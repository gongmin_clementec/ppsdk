//
//  PPConfig.h
//  ppsdk
//
//  Created by clementec on 2016/05/12.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_PPConfig__)
#define __h_PPConfig__

#include <string>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic pop
#include "BaseSystem.h"

class PPConfig : public Singleton<PPConfig>
{
friend class Singleton<PPConfig>;
    
private:
    json_t m_json;
    
            PPConfig();
    virtual ~PPConfig() = default;
    
    mutable boost::recursive_mutex  m_mtx;
    
protected:
public:
    void    clear();
    void    serialize(std::ostream& os, bool bEncryption) const;
    void    serialize(const boost::filesystem::path& path, bool bEncryption) const;
    void    deserialize(std::istream& is, bool bEncryption);
    void    deserialize(const boost::filesystem::path& path, bool bEncryption);
    
    void    updateConfigure(const std::string& desc, const std::string& json);
    
    void    updateConnectedCardReaderInformation();

    template<class T> T get(const std::string& key) const
    {
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        return m_json.get<T>(key);
    }
    
    json_t getChild(const std::string& key) const
    {
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        return m_json.get(key);
    }

    template<class T> boost::optional<T> getOptional(const std::string& key) const
    {
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        return m_json.get_optional<T>(key);
    }
    
    template<class T> void set(const std::string& key, T&& value)
    {
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        m_json.put<T>(key, std::forward<T>(value));
    }
};


#endif /* !defined(__h_PPConfig__) */

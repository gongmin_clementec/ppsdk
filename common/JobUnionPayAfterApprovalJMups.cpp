//
//  JobUnionPayAfterApprovalJMups.cpp
//  ppsdk
//
//  Created by tel on 2018/12/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobUnionPayAfterApprovalJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"

JobUnionPayAfterApprovalJMups::JobUnionPayAfterApprovalJMups(const __secret& s) :
    JobUnionPaySalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobUnionPayAfterApprovalJMups::~JobUnionPayAfterApprovalJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    m_bIsMSFallback = false;
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.unionpay").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging(m_taggingSelector).as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()       
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobUnionPayAfterApprovalJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::AfterApproval;
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    LogManager_Function();

    return JobUnionPaySalesJMups::buildDispatchRequestParamForTradeInfo()
    .map([=](TradeInfoBuilder builder){
        builder.addApprovalNo   (TradeInfoBuilder::attribute::required, boost::none);
        builder.addCupNo        (TradeInfoBuilder::attribute::required, boost::none);
        builder.addCupSendDate  (TradeInfoBuilder::attribute::required, boost::none);
        return builder;
    });
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::sendTradeConfirm(prmUnionPay::doubleTrade selector, boost::optional<std::string> authenticationNo)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto cupSendDate= m_tradeData.get<std::string>("cupSendDate"); //format: MMDDhhmmss

    return hpsUnionPayAfterApproval_Confirm
    (
     selector,
     m_tradeData.get<std::string>("approvalNo"),
     m_tradeData.get<std::string>("cupNo"),
     cupSendDate,
     m_tradeData.get<int>("amount"),
     false,
     authenticationNo,
     authenticationNo ? true : false,                           /* pinRetryFlg */
     m_tradeData.get_optional<std::string>("firstGACCmdRes")    /* firstGACCmdRes */
    );
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData, boost::optional<std::string> authenticationNo)
-> pprx::observable<prmUnionPay::doubleTrade>
{
    LogManager_Function();
    
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(prmUnionPay::doubleTrade::No).as_dynamic();
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmUnionPay::doubleTrade::Yes, authenticationNo);
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return prmUnionPay::doubleTrade::Yes;
    })
    .as_dynamic();
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::sendTagging(prmUnionPay::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsUnionPayAfterApproval_Tagging( selector, slipNo, prmUnionPay::printResultDiv::Success, boost::none, false );
    }).as_dynamic();
}

/* virtual */
void JobUnionPayAfterApprovalJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto loop = pprx::subjects::behavior<int>(0);
    m_tradeConfirmFinished.clear();
    LogManager_AddDebug(data.str());
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    boost::shared_ptr<boost::optional<std::string>> pin = boost::make_shared<boost::optional<std::string>>();
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::UnionPay,
     getOperationDiv(),
     m_bIsMSFallback,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_cardInfoData = resp.body.clone();
        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY")).as_dynamic()
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
                .as_dynamic();
            }).as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            throw_error_internal("UnionPay does not support KID.");
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTradeInfo("Confirm");
    }).as_dynamic()
    .flat_map([=](auto){
        json_t jparam;
        jparam.put("amount", m_tradeData.get<int>("amount"));
        jparam.put("application.label", "556E696F6E506179");
        jparam.put("uiMessage.message1", "暗証番号を入力してください。");
        jparam.put("uiMessage.message2", "");
        return loop.get_observable()
        .flat_map([=](auto nEnterPinCnt) {
            return emvOnCardInputEncryptedOnlinePin(jparam)
            .flat_map([=](const_json_t pinResult){
                auto result = pinResult.get<std::string>("result");
                if(result == "retry"){
                    loop.get_subscriber().on_next(nEnterPinCnt);
                    return pprx::never<JMupsAccess::response_t>().as_dynamic();
                }
                if(result == "success"){
                    *pin = pinResult.get<std::string>("data");
                }
                return sendTradeConfirm(prmUnionPay::doubleTrade::No, *pin)
                .flat_map([=](JMupsAccess::response_t confirmResult) mutable {
                    auto pinCheckResult = confirmResult.body.get_optional<std::string>("normalObject.pinCheckResult");
                    if(pinCheckResult && (*pinCheckResult == "1")){ /* PINチェック失敗 */
                        m_tradeConfirmFinished.put("hps", addMediaDivToCardNo(confirmResult.body));
                        m_pinParam.put("pinResult", "PinFail-deny");
                        m_pinParam.put("bOnetimeRemain", false);
                        if(result == "bypass"){ // PINバイパス失敗です。
                            m_pinParam.put("pinResult", "PinFail-bypass");
                        }
                        return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", m_pinParam).as_dynamic()
                        .flat_map([=](const_json_t resp){
                            const bool bContinue = resp.get<bool>("bContinue");
                            if(bContinue){
                                loop.get_subscriber().on_next(nEnterPinCnt + 1);
                                return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished).as_dynamic()
                                .flat_map([=](auto){
                                    return pprx::never<JMupsAccess::response_t>().as_dynamic();
                                }).as_dynamic();
                                
                            }
                            else{
                                return pprx::error<JMupsAccess::response_t>(make_error_aborted("aborted")).as_dynamic();
                            }
                        }).as_dynamic();
                    }
                    else{
                        auto bIsFallback = confirmResult.body.get<bool>("normalObject.isFallback");
                        if(bIsFallback){
                            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_SKIP_FAILURE")).as_dynamic()
                            .flat_map([=](auto) {
                                json_t json;
                                json.put("errorCode", "PP_E3001");
                                json.put("description", "このカードはICでのお取り引きはできません。/n 磁気カードリーダを使用してください。");
                                return pprx::error<JMupsAccess::response_t>(make_error_pinreleted(json)).as_dynamic();
                            }).as_dynamic();
                        }
                        else{
                            // 取引が不成立かどうか
                            auto bIsDecline = confirmResult.body.get_optional<std::string>("normalObject.printInfo.isDecline");
                            if(bIsDecline && (*pinCheckResult == "true")){ /* 取引が不成立 */
                                m_pinParam.put("pinResult", "PinFail-deny");
                            }
                            else{
                                m_pinParam.put("pinResult", "success");
                            }
                            m_pinParam.put("bOnetimeRemain", false);
                            
                            return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", m_pinParam).as_dynamic()
                            .flat_map([=](const_json_t resp){
                                const bool bContinue = resp.get<bool>("bContinue");
                                if(bContinue){
                                    m_tradeConfirmFinished.put("hps", confirmResult.body.clone());
                                    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
                                    .map([=](auto) {
                                        return confirmResult;
                                    }).as_dynamic();
                                }
                                else{
                                    return pprx::error<JMupsAccess::response_t>(make_error_aborted("aborted")).as_dynamic();
                                }
                            }).as_dynamic();
                        }
                    }
                }).as_dynamic();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForConfirmDoubleTrade(jmups, *pin).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto selector){
        m_taggingSelector = selector;
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    });
}

/* virtual */
auto JobUnionPayAfterApprovalJMups::emvOnUiAmountConfirm(const_json_t emvSession)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    m_tradeData.put("firstGACCmdRes", emvSession.get<std::string>("transaction.firstGACCmdRes"));
    
    return sendTradeConfirm(prmUnionPay::doubleTrade::No, boost::none)
    .flat_map([=](auto resp){
        return waitForConfirmDoubleTrade(resp, boost::none);
    }).as_dynamic()
    .map([=](prmUnionPay::doubleTrade taggingSelector) -> const_json_t{
        m_taggingSelector = taggingSelector;
        return getJMupsStorage()->getTradeResultData();
    });
}


/* virtual */
auto JobUnionPayAfterApprovalJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "4"); // 承認後売上
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}


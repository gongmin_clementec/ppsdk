//
//  JobNFCIntermediateTotalJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCIntermediateTotalJMups.h"

JobNFCIntermediateTotalJMups::JobNFCIntermediateTotalJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobNFCIntermediateTotalJMups::~JobNFCIntermediateTotalJMups()
{
    
}

/* virtual */
auto JobNFCIntermediateTotalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    return callOnDispatchWithCommand("waitForOptions")
    .flat_map([=](const_json_t json){
        return hpsImJournal
        (
         JMupsAccessJournal::prmJournalCommon::paymentType::NFC,
         JMupsAccessJournal::prmJournalCommon::stringToDetailOption(json.get<std::string>("detailOption"))
         );
    }).as_dynamic()
    .map([=](auto resp){
        json_t json;
        json.put("status", "success");
        json.put("result.hps", resp.body.clone());
        return json.as_readonly();
    });
}

/* virtual */
auto JobNFCIntermediateTotalJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


//
//  JobOpenTerminalJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobOpenTerminalJMups.h"
#include "TMSAccess.h"
#include "JMupsJobStorage.h"

JobOpenTerminalJMups::JobOpenTerminalJMups(const __secret& s) :
    PaymentJob(s)
{
    LogManager_Function();
}

/* virtual */
JobOpenTerminalJMups::~JobOpenTerminalJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobOpenTerminalJMups::getObservableSelf()
 -> pprx::observable<const_json_t>
{
    if(!getStorage()){
        setStorage(boost::make_shared<JMupsJobStorage>());
    }
    
    return hpsOpenTerminal_Request(JMupsAccess::prmOpenTerminal_Request::nfcDeviceType::Others)
    .observe_on(pprx::observe_on_thread_pool())
    .map([=](auto resp){
        LogManager_AddFullInformation(resp.body.str());
        auto js = getJMupsStorage();
        js->setTerminalInfo(resp.body);
        BaseSystem::instance().setSharedAreaText("shared", resp.body.str());
        json_t json;
        json.put("status", "success");
        json.put("result.hps", resp.body.clone());
        return json.as_readonly();
    })
    .flat_map([=](auto resp){
        if(PaymentJob::isTrainingMode()){
            return pprx::just(resp).as_dynamic();
        }
        else{
            auto tms = TMSAccess::create();
            return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit>s){
                s.on_next(pprx::unit());
            })
            .timeout(std::chrono::seconds(5))
            .flat_map([=](auto){
                return tms->initializeAndLoginAndUpdate();
            }).as_dynamic()
            .observe_on(pprx::observe_on_thread_pool())
            .flat_map([=](const_json_t tmsResp){
                auto RWFirmwareDownloadUrl = tmsResp.get_optional<std::string>("RWFirmwareDownloadUrl");
                auto AppUpdateAvailable = tmsResp.get_optional<bool>("AppUpdateAvailable");
                json_t j;
                if(RWFirmwareDownloadUrl){
                    j.put("bFirmwareUpdate", true);
                }
                else{
                    j.put("bFirmwareUpdate", false);
                }
                if(AppUpdateAvailable && (*AppUpdateAvailable)){
                    j.put("bAvailableNewApp", true);
                }
                else{
                    j.put("bAvailableNewApp", false);
                }
                return callOnDispatchWithMessageAndSubParameter("tmsUpdates", "", j);
            }).as_dynamic()
            .on_error_resume_next([=](std::exception_ptr err){
                try{std::rethrow_exception(err);}
                catch(std::exception& ex){
                    LogManager_AddWarningF("TMS Error: %s", ex.what());
                }
                catch(...){
                    LogManager_AddWarning("TMS Error: unknown exception");
                }
                return pprx::error<pprx::unit>(err).as_dynamic();
            }).as_dynamic()
            .take(1)
            .map([resp, tms](auto){
                return resp;
            }).as_dynamic();
        }
    });
}

/* virtual */
auto JobOpenTerminalJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


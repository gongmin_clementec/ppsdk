//
//  JMupsEmvKernel.cpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsEmvKernel.h"
#include "AbstructFactory.h"
#include <boost/regex.hpp>
#include <fstream>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <boost/algorithm/hex.hpp>
#include "PPConfig.h"
#include "PaymentJob.h"
#include "JMupsJobStorage.h"
#include "JavascriptEnvironment.h"

JMupsEmvKernel::JMupsEmvKernel(Type type) :
 m_type(type),
 m_bOfflinePinConfirmed(true),
 m_bOnlinePinReinput(false),
 m_kernelParam(kernelparam_t{})
{
    LogManager_Function();
}

/* virtual */
JMupsEmvKernel::~JMupsEmvKernel()
{
    LogManager_Function();
    
}

auto JMupsEmvKernel::initialize(JMupsJobStorage::sp storage)
  -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    //## jsenvにパスするので抱える必要がない asyncPrepareCallbacks(onSuccess, onError);
    m_jsenv = AbstructFactory::instance().createJavascriptEnvironment();
    
    auto&& termInfo = storage->getTerminalInfo();
    m_initialParameter.put("transaction", json_t::map_t());
    
    json_t property;
    property.put("aidList", termInfo.get("sessionObject.aidList"));
    property.put("pinUnknownFbOnOffDiv", termInfo.get<std::string>("sessionObject.pinUnknownFbOnOffDiv"));
    m_initialParameter.put("property", property);
    
    auto resourceDirectory = BaseSystem::instance().getResourceDirectoryPath();
    
    auto docPath = resourceDirectory / "EMVKernel";
    if(m_type == Type::Credit){
        docPath /= "credit";
    }
    else if(m_type == Type::UnionPay){
        docPath /= "unionpay";
    }
    else{
        throw_error_internal("bad type.");
    }
    
    auto indexHtml = docPath / "index.html";
    std::ifstream   shtml(indexHtml.string());
    std::string     _html((std::istreambuf_iterator<char>(shtml)), std::istreambuf_iterator<char>());
    
    return m_jsenv->load(_html, docPath.string())
    .map([=](auto){
        return pprx::unit();
    });
}

auto JMupsEmvKernel::kernelInit()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    LogManager_AddDebug(m_initialParameter.str());
    return kernelSetSession(m_initialParameter)
    .flat_map([=](auto){
        return m_jsenv->call("EMVKernel.init(session);");
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    });
}

auto JMupsEmvKernel::kernelExecute(const kernelparam_t& kernelparam)
  -> pprx::observable<const_json_t>
{
    LogManager_Function();

    json_t param;
    param.put("method", kernelparam.methods);
    param.put("arg", kernelparam.args);
    
    auto sparam = param.str();
    auto js = (boost::format("JSON.stringify(EMVKernel.execute(%s, session));") % sparam).str();
    return m_jsenv->call(js)
    .map([=](auto&& str){
        auto json = json_t::from_str(str);
        LogManager_AddDebugF("call :\n%s", js);
        LogManager_AddDebugF("response :\n%s", str);
        return json.as_readonly();
    });
}

auto JMupsEmvKernel::kernelGetCurrentNode()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    auto js = "JSON.stringify(EMVKernel.getCurrentNode(session));";
    return m_jsenv->call(js)
    .map([=](auto&& str){
        auto json = json_t::from_str(str);
        LogManager_AddDebugF("call :\n%s", js);
        LogManager_AddDebugF("response :\n%s", str);
        return json.as_readonly();
    });
}

auto JMupsEmvKernel::kernelGetSession()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    auto js = "JSON.stringify(session);";
    return m_jsenv->call(js)
    .map([=](auto&& str){
        auto json = json_t::from_str(str);
        LogManager_AddDebugF("call :\n%s", js);
        LogManager_AddDebugF("response :\n%s", json.str());
        return json.as_readonly();
    });
}

auto JMupsEmvKernel::kernelSetSession(const_json_t json)
  -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto sess = json.str();
    auto js = (boost::format("session = %s;") % sess).str();
    return m_jsenv->call(js)
    .map([=](auto){
        LogManager_AddDebugF("call :\n%s", js);
        return pprx::unit();
    });
}

auto JMupsEmvKernel::emvRun(JMupsJobStorage::sp storage, const_json_t tradeStartResponse)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    if(isTrainingMode()){
        LogManager_AddInformation("emv training mode");
        return trainingEmvRun(storage);
    }

    m_encryptKeyModulus  = tradeStartResponse.get<std::string>("normalObject.keyModulus");
    m_encryptKeyExponent = tradeStartResponse.get<std::string>("normalObject.keyExponent");
    
    return initialize(storage)
    .flat_map([=](auto){
        return kernelInit();
    })
    .flat_map([=](auto){
        return kernelGetSession();
    })
    .flat_map([=](const_json_t session){
        auto modsession = session.clone();
        /* カード後挿し設定 */
        modsession.put("transaction.isInsertedFirst", "false");
        modsession.put("transaction.isEmvProcess", "true");
        return kernelSetSession(modsession)
        .map([=](auto){
            return pprx::unit();
        });
    })
    .flat_map([=](auto){
        setNextKernelParam({
            "DNX", "iccard_input", "cardinsert"
        }, const_json_t());

        /*
         * 同一スレッドで　setNextKernelParam() を経由して
         * m_kernelParam.on_next() を呼び出しても、
         * 次の値は発行を取りこぼすため、
         * 値の発行時のスレッドと、以降のobservableを切り離す。
         */
        return m_kernelParam.get_observable()
        .observe_on(pprx::observe_on_thread_pool())
        .flat_map([=](auto kernelParam){
            return kernelExecuteAndProcessEvent(kernelParam).as_dynamic();
        }).as_dynamic()
        .skip_until(m_emvProcessing.get_observable())
        .flat_map([=](auto){
            if(!m_bOfflinePinConfirmed){ // offlinePinではない場合
                return emvOnCardPinResultConfirm(m_jsPinResultConfirm)
                .flat_map([=](auto){
                    return pprx::just(const_json_t()).as_dynamic();
                }).as_dynamic();
            }
            else{
                return pprx::just(const_json_t()).as_dynamic();
            }
        }).as_dynamic()
        .flat_map([=](auto){
            m_jsTradeConfirmFinish.put("hps", addMediaDivToCardNo(m_tradeResultData.as_readonly()));
            return emvOnTradeConfirmFinish(m_jsTradeConfirmFinish.clone()).as_dynamic()
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }).as_dynamic()
        .map([=](auto){
            m_kernelParam.get_subscriber().on_completed();
            return m_tradeResultData.as_readonly();
        });
    }).as_dynamic();
}

auto JMupsEmvKernel::emvRunLevel1(JMupsJobStorage::sp storage)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    if(isTrainingMode()){
        LogManager_AddInformation("emv training mode");
        return trainingEmvRunLevel1(storage);
    }
    
    return initialize(storage)
    .flat_map([=](auto){
        return kernelInit();
    })
    .flat_map([=](auto){
        return kernelGetSession();
    })
    .flat_map([=](const_json_t session){
        auto modsession = session.clone();
        /* カード後挿し設定 */
        modsession.put("transaction.isInsertedFirst", "false");
        modsession.put("transaction.isEmvProcess", "true");
        return kernelSetSession(modsession)
        .map([=](auto){
            return pprx::unit();
        });
    })
    .flat_map([=](auto){
        setNextKernelParam({
            "DNX", "iccard_input", "cardinsert"
        }, const_json_t());
        
        /*
         * 同一スレッドで　setNextKernelParam() を経由して
         * m_kernelParam.on_next() を呼び出しても、
         * 次の値は発行を取りこぼすため、
         * 値の発行時のスレッドと、以降のobservableを切り離す。
         */
        return m_kernelParam.get_observable()
        .observe_on(pprx::observe_on_thread_pool())
        .flat_map([=](auto kernelParam){
            return kernelExecute(kernelParam).as_dynamic();
        }).as_dynamic()
        .flat_map([=](const_json_t kernelResponse){
            return kernelGetCurrentNode()
            .flat_map([=](const_json_t node){
                /* 判定を"EMV34"で正しいかは仕様を確認する必要がある */
                auto flow = node.get_optional<std::string>("flow");
                if(flow && (*flow == "EMV34")){
                    /* kernelProcessEventは行うがm_kernelParamによるイベントループは終了する */
                    m_kernelParam.get_subscriber().on_completed();
                }
                return kernelProcessEvent(kernelResponse).as_dynamic();
            }).as_dynamic();
        })
        .last();
    }).as_dynamic();
}


void JMupsEmvKernel::setNextKernelParam(const std::vector<std::string>& methods, const_json_t arg)
{
    LogManager_Function();

    std::vector<json_t> args;
    if(!arg.empty()) args.push_back(arg.clone());
    m_kernelParam.get_subscriber().on_next(kernelparam_t{
        json_t::from_array(methods),
        json_t::from_array(args)
    });
}

auto JMupsEmvKernel::kernelExecuteAndProcessEvent(kernelparam_t kernelParam)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return kernelExecute(kernelParam)
    .flat_map([=](const_json_t kernelResponse){
        return kernelProcessEvent(kernelResponse);
    });
}

auto JMupsEmvKernel::kernelProcessEvent(const_json_t kernelResponse) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto device = kernelResponse.get_optional("device");
    if(device){
        return dispatchDevice(device->clone()).as_dynamic();
    }
    
    auto waiting = kernelResponse.get_optional("waiting");
    if(waiting){
        waiting->clone();
        return checkBlockCard(waiting->clone())
        .flat_map([=](pprx::unit){
            return dispatchWaiting(waiting->clone());
        }).as_dynamic();
    }
    
    auto parentControl = kernelResponse.get_optional("parentControl");
    if(parentControl){
        return checkSessionEventCode()
        .flat_map([=](auto){
            return dispatchParentControl(parentControl->clone()).as_dynamic();
        });
    }
    
    LogManager_AddInformation("call nothing");
    return pprx::just(pprx::unit())
    .map([=](auto){
        setNextKernelParam
        (
         {"RELEASE", "release", "iccard"},
         json_t()
         );
        return pprx::unit();
    }).as_dynamic();
}

/*
 カード内にアプリケーションが存在するが、存在するアプリケーションが全て閉塞している場合には、
 磁気フォールバックではなく、使用できないカードとして判定しなければならない。
 JCB Test Case 05
 */
auto JMupsEmvKernel::checkBlockCard(const_json_t waiting) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    const auto name = waiting.get<std::string>("name");
    const auto obj  = waiting.get<std::string>("value.obj");

    if(name != "wait-NODEIN") return pprx::just(pprx::unit()).as_dynamic();
    
    return kernelGetSession()
    .flat_map([=](const_json_t session){
        do{
            auto modsession = session.clone();
            auto isMSFallback = modsession.get_optional<std::string>("transaction.isMSFallback");
            if(!isMSFallback) break;
            if(*isMSFallback != "true") break;

            auto applicationCount = modsession.get_optional<int>("transaction.applicationCount");
            if(!applicationCount) break;
            if(*applicationCount != 1) break;
            
            auto applicationBlockFlg = modsession.get_optional<std::string>("transaction.applicationBlockFlg");
            if(!applicationBlockFlg) break;
            if(*applicationBlockFlg != "true") break;
            
            LogManager_AddWarning("no valid applications with block -> Change Card");
            json_t errresp;
            errresp.put("resultCode", "1");
            errresp.put("errorObject.errorCode", "PP_W0001");
            errresp.put("errorObject.errorLevel", "WARN");
            errresp.put("errorObject.errorType", "WARN");
            errresp.put("errorObject.errorMessage", "お取り扱いできません。↑カードを抜いて、別の取引方法に切替えてください。");
            errresp.put("errorObject.printBugReport", "false");
            return pprx::error<pprx::unit>(make_error_paymentserver(errresp)).as_dynamic();
        } while(false);
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic();
}

auto JMupsEmvKernel::checkSessionEventCode() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return kernelGetSession()
    .flat_map([=](const_json_t session){
        auto eventcode = session.get_optional<std::string>("transaction.eventcode");
        if(eventcode){
            LogManager_AddWarning("EMVKernel event code detected.");
            json_t errorJson;
            errorJson.put("description", "ICCardError");
            return pprx::error<pprx::unit>(make_error_cardrw(errorJson)).as_dynamic();
            
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic();
}

auto JMupsEmvKernel::dispatchParentControl(const_json_t parentControl)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto state_name = parentControl.get<std::string>("state.name");
    if(state_name == "release"){
        m_emvProcessing.get_subscriber().on_next(pprx::unit());
        return pprx::just(pprx::unit())
        .as_dynamic();
    }
    return pprx::error<pprx::unit>(make_error_internalf("unhandled dispatchParentControl -> %s", state_name))
    .as_dynamic();
}

auto JMupsEmvKernel::dispatchDevice(const_json_t device)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto name = device.get<std::string>("name");
    if(name == "iccard"){
        return dispatchDeviceICCard(device);
    }
    else if(name == "network"){
        return dispatchDeviceNetwork(device);
    }
    else{
        return pprx::error<pprx::unit>(make_error_internalf("unknown device name. %s", name));
    }
}

auto JMupsEmvKernel::dispatchDeviceICCard(const_json_t device)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    LogManager_AddDebugF("%s", device.str());
    
    const auto name = device.get<std::string>("name");
    const auto obj  = device.get<std::string>("value.obj");
    if((name == "iccard") && (obj == "apduresponse")){
        auto apdu = device.get<std::string>("value.param.in_apdu_data");

        bool bEncrypted = false;
        bool bVerify = false;
        bool bCheckSelectResult = false;
        bool bCheckGetDataResult = false;
        
        {
            auto expr_1 = boost::regex("^0[0-9A-F]B2.*$", boost::regex::icase);
            auto expr_2 = boost::regex("^0084000000$", boost::regex::icase);
            auto expr_3 = boost::regex("^002000.*$", boost::regex::icase);
            auto expr_select  = boost::regex("^0[0-9A-F]A4.*$", boost::regex::icase);
            auto expr_getData = boost::regex("^80CA.*$", boost::regex::icase);
            
            if(boost::regex_match(apdu, expr_1)){
                bEncrypted = true;
            }
            if(boost::regex_match(apdu, expr_2)){
                bEncrypted = true;
            }
            if(boost::regex_match(apdu, expr_3)){
                bVerify = true;
            }
            if(boost::regex_match(apdu, expr_select)){
                bCheckSelectResult = true;
            }
            if(boost::regex_match(apdu, expr_getData)){
                bCheckGetDataResult = true;
            }
        }
        
        auto _prepareNextKernel = [=](const_json_t json)
        {
            setNextKernelParam
            (
             {"DNX", "iccard_output", "apduresponse"},
             json
             );
            return pprx::just(pprx::unit())
            .as_dynamic();
        };

        if(bEncrypted){
            return emvOnCardTxEncryptedApdu(apdu)
            .flat_map([=](auto json){
                return _prepareNextKernel(json);
            }).as_dynamic();
        }
        else{
            return emvOnCardTxPlainApdu(apdu)
            .flat_map([=](const_json_t json){
                /*
                 checkBlockCard() で使用するための情報を収集し、
                 下記の２つのパラメータをJS内に埋め込む
                    "transaction.applicationCount"
                    "transaction.applicationBlockFlg"
                 JCB Test Case 05 で閉塞カードは磁気フォールバックしてはならない対応はKernelではなくsdk側で行う仕様のため。
                 SW1, SW2 が 6283 は"Selected file invalidated"
                 */
                auto expr = boost::regex(".*?6283$", boost::regex::icase);
                auto apdure = json.get<std::string>("result");
                auto preprocess = pprx::just(pprx::unit()).as_dynamic();
                if(boost::regex_match(apdure, expr)){   /* 閉塞カード処理 */
                    preprocess = kernelGetSession()
                    .flat_map([=](const_json_t session){
                        do{
                            auto modsession = session.clone();
                            auto applicationCount = modsession.get_optional<int>("transaction.applicationCount");
                            if(!applicationCount) break;
                            modsession.put("transaction.applicationCount", *applicationCount + 1);
                            modsession.put("transaction.applicationBlockFlg", "true");
                            return kernelSetSession(modsession).as_dynamic();
                        } while(false);
                        return pprx::just(pprx::unit()).as_dynamic();
                    }).as_dynamic();
                }
                // リクルート銀聯対応:
                // MiuraのファームウェアやMPIが古い場合に対応してないAIDにSELECTコマンドを発行した際、
                // 不正パケット(SW=6F00)を返すことがある。
                // その場合、次のAID検索には行かずに即エラーにするため、ここでエラーを発行するようにした。
                if(bCheckSelectResult) {
                    auto exprSelect = boost::regex(".*?6F00$", boost::regex::icase);
                    if(boost::regex_match(apdure, exprSelect)){
                        LogManager_AddInformation("SELECT COMMAND error. maybe non-adaptive Firmware or MPI version.");
                        json_t errresp;
                        std::string sw;
                        sw.assign(apdure.end() - 4, apdure.end());
                        errresp.put("description", "disableCardInsertedEMVKernel+sw:" + sw);
                        errresp.put("code", "PP_E2003");
                        return pprx::error<pprx::unit>(make_error_cardrw(errresp)).as_dynamic();
                    }
                }
                // リクルート銀聯対応:
                // GET DATAコマンドは正常系でもReferenced data (data objects) not found(SW=6A88)を返すことがある。
                // リクルートの要望でGET DATAコマンドでSW=9000、6A88でない場合は全てエラーにするようにした。
                if(bCheckGetDataResult) {
                    auto exprGetData = boost::regex(".*?(9000|6A88)$", boost::regex::icase);
                    while(!boost::regex_match(apdure, exprGetData)){
                        LogManager_AddInformationF("GET DATA COMMAND error. response [%s]", apdure);
                        json_t errresp;
                        std::string sw;
                        sw.assign(apdure.end() - 4, apdure.end());
                        if(sw == "6A81"){
                            break;
                        }
                        errresp.put("description", "disableCardInsertedEMVKernel+sw:" + sw);
                        errresp.put("code", "PP_E2003");
                        return pprx::error<pprx::unit>(make_error_cardrw(errresp)).as_dynamic();
                    }
                }
                return preprocess
                .flat_map([=](pprx::unit){
                    return _prepareNextKernel(json);
                }).as_dynamic();
            }).as_dynamic();
        }
    }
    else if((name == "iccard") && (obj == "ifdSerialNo")) {
        return emvOnCardGetIfdSerialNo()
        .flat_map([=](auto json){
            setNextKernelParam
            (
             {"DNX", "iccard_output", "ifdSerialNo"},
             json
             );
            return pprx::just(pprx::unit());
        }).as_dynamic();
    }
    
    return pprx::error<pprx::unit>(make_error_internalf("invalid request type, device.name=\"%s\", device.value.obj=\"%s\"", name % obj))
    .as_dynamic();
}

auto JMupsEmvKernel::dispatchDeviceNetwork(const_json_t device)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    std::map<std::string, std::string> lutUrl =
    {
        {"serviceMenu_ma0770_1" , "ma0770.do?mA0770_1=aaa"},
        {"serviceMenu_ma0770_2" , "ma0770.do?mA0770_2=aaa"},
        {"serviceMenu_ma0770_3" , "ma0770.do?mA0770_3=aaa"},
        {"serviceMenu_ma0770_4" , "ma0770.do?mA0770_4=aaa"},
        {"serviceMenu_ma0770_5" , "ma0770.do?mA0770_5=aaa"}, /* TODO: 要確認 */

        {"CreditSales_SA1770_1" , "sa1770.do?sA1770_1=aaa"},
        {"CreditSales_SA1770_2" , "sa1770.do?sA1770_2=aaa"},
        {"CreditSales_SA1770_3" , "sa1770.do?sA1770_3=aaa"},
        {"CreditSales_SA1770_4" , "sa1770.do?sA1770_4=aaa"},
        {"CreditSales_SA1770_5" , "sa1770.do?sA1770_5=aaa"},
        
        {"CreditSales_SA1719"   , "sa1719.do?sA1719=aaa"},
        {"CreditSales_SA1901_1" , "sa1901.do?sA1901_1=aaa"},
        {"CreditSales_SA1901_2" , "sa1901.do?sA1901_2=aaa"}
    };
    
    std::map<std::string, std::vector<std::string>> lutParams =
    {
        /* カード読取画面で IC を挿入した場合のリクエスト */
        {"serviceMenu_ma0770_1",
            {"fci", "aid", "brandApJudgeNo", "brandApVersion", "ifdSerialNumber", "operationDiv", "amount", "taxOtherAmount"}},
        {"serviceMenu_ma0770_2", {"gpoCmdRes"}},
        {"serviceMenu_ma0770_3", {"rdrcdCmdResList", "operationDiv"}},
        {"serviceMenu_ma0770_4", {"gdCmdResList"}},
        {"serviceMenu_ma0770_5" , {"intAuthCmdRes"}}, /* TODO: 要確認 */

        /* 金額フラグが true の場合のリクエスト */
        {"CreditSales_SA1770_1",
            {"fci", "aid", "brandApJudgeNo", "brandApVersion", "ifdSerialNumber", "operationDiv", "amount", "taxOtherAmount"}},
        {"CreditSales_SA1770_2", {"gpoCmdRes"}},
        {"CreditSales_SA1770_3", {"rdrcdCmdResList", "operationDiv"}},
        {"CreditSales_SA1770_4", {"gdCmdResList"}},
        {"CreditSales_SA1770_5" , {"intAuthCmdRes"}}, /* TODO: 要確認 */
        
        {"CreditSales_SA1719", {"amount", "taxOtherAmount", "payWayDiv", "pinpadResult", "resultMsg", "onEncipheredPin", "offEncipheredPin", "pinBypassFlg"}},
        {"CreditSales_SA1901_1", {}},
        {"CreditSales_SA1901_2", {"secondGACCmdRes"}}
    };
    
    std::map<std::string, boost::function<pprx::observable<const_json_t>(const_json_t)>> lutPostprocess =
    {
        {"serviceMenu_ma0770_1", [=](const_json_t body){
            /* 日立の指示に基づく対応（fciのsw1,sw2が削除される） */
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();
                auto fci = session.get_optional<std::string>("transaction.selectCmdRes");
                modsession.put("transaction.fci", *fci);
                return kernelSetSession(modsession);
            })
            .map([=](auto){
                return body;
            });
        }},
        {"CreditSales_SA1901_2", [=](const_json_t body){
            m_tradeResultData = body.clone();
            return pprx::just(body);
        }}
    };

    auto&& type     = device.get<std::string>("value.type");
    auto&& url      = lutUrl[type];
    auto&& params   = lutParams[type];
    
    json_t postParam;
    
    for(auto param : params){
        auto v = device.get_optional("value.session.transaction." + param);
        if(v) postParam.put(param, *v);
    }
    
    /* 【別紙】PIN入力待ちについて.xls に基づく実装 */
    if(type == "CreditSales_SA1719"){
        auto sinspection = device.get_optional<std::string>("value.session.transaction.inspectionMethod");
        if(sinspection){
            auto inspection = boost::lexical_cast<int>(*sinspection);
            if((inspection == 5) || (inspection == 6)){
                LogManager_AddInformation("オフライン暗号PIN -> offEncipheredPinに0000を追加");
                postParam.put("offEncipheredPin", "0000");
            }
        }
    }
    
    return emvOnNetwork(url, postParam)
    .flat_map([=](const_json_t body){
        auto it = lutPostprocess.find(type);
        if( it != lutPostprocess.end()){
            return it->second(body).as_dynamic();
        }
        return pprx::just(body).as_dynamic();
    })
    .flat_map([=](const_json_t body){
        json_t json;
        auto normalObject = body.get_optional("normalObject");
        if(normalObject){
            json = normalObject->clone();
        }
        
        /* javascript へはbool, intは文字列として渡すらしい */
        {
            auto amountFlg = json.get_optional<bool>("amountFlg");
            if(amountFlg){
                json.put("amountFlg", *amountFlg ? "true" : "false");
            }
            
            auto result = json.get_optional<int>("result");
            if(result){
                json.put("result", boost::lexical_cast<std::string>(*result));
            }
        }
        
        return kernelGetCurrentNode()
        .flat_map([=](const_json_t node){
            std::vector<std::string> method;
            const auto context = node.get<std::string>("context");
            if(context == "pre"){
                method = {"ICC", "wait", "icProcess"};
            }
            else if(context == "main"){
                method = {"EMV", "wait", "icProcess"};
            }
            else{
                return pprx::error<pprx::unit>(make_error_internalf("unknown context. %d", context))
                .as_dynamic();
            }
            setNextKernelParam(method, json);
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

void JMupsEmvKernel::encryptReadRecords(json_t ioJson)
{
    LogManager_Function();

    auto _encrypt = [](const std::string& src, const std::string& modulus, const std::string& exponent)
    {
        std::stringstream srcs(src);
        std::stringstream dsts;
        
        RSA* rsa = ::RSA_new();
        ::BN_hex2bn(&rsa->n, modulus.c_str());
        ::BN_hex2bn(&rsa->e, exponent.c_str());
        const auto blockSize = ::RSA_size(rsa);
        /*
         RSA_PKCS1_PADDINGの場合、ソースの最大バイト数は RSA_size() -11 にしなければならない。
         https://www.openssl.org/docs/man1.0.2/crypto/RSA_public_encrypt.html
         */
        std::vector<uint8_t> raw(blockSize - 11);
        std::vector<uint8_t> enc(blockSize);
        
        while(!srcs.eof()){
            srcs.read(reinterpret_cast<char*>(raw.data()), raw.size());
            const auto readBytes = srcs.gcount();
            
            const auto writtenBytes = ::RSA_public_encrypt(
                                                           static_cast<int>(readBytes),
                                                           raw.data(),
                                                           enc.data(),
                                                           rsa,
                                                           RSA_PKCS1_PADDING);
            if(writtenBytes == -1){
                LogManager_AddErrorF("%s", ::ERR_error_string(::ERR_get_error(), nullptr));
                break;
            }
            boost::algorithm::hex
            (
             enc.begin(),
             enc.begin() + writtenBytes,
             std::ostream_iterator<char>(dsts));
        }
        ::RSA_free(rsa);
        
        dsts << "9000";
        return dsts.str();
    };
    
    {
        auto rdrcdCmdResList = ioJson.get_optional("rdrcdCmdResList");
        if(rdrcdCmdResList){
            auto&& list = rdrcdCmdResList->get_array<std::string>();
            std::vector<std::string> enc;
            for(auto src: list){
                enc.push_back(_encrypt(src, m_encryptKeyModulus, m_encryptKeyExponent));
            }
            ioJson.put("rdrcdCmdResList", json_t::from_array(enc));
        }
    }
}


auto JMupsEmvKernel::dispatchWaiting(const_json_t waiting)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    LogManager_AddDebugF("dispatchWaiting\n%s", waiting.str());
    
    const auto name = waiting.get<std::string>("name");
    const auto obj  = waiting.get<std::string>("value.obj");
    if((name == "wait") && (obj == "selectDeal")){
        return emvOnUiSelectDeal()
        .flat_map([=](const_json_t json){
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();
                auto operationDiv = json.get<std::string>("operationDiv");
                modsession.put("transaction.operationDiv", operationDiv);
                if(operationDiv != "0"){
                    modsession.put("transaction.amount", "0");
                    modsession.put("transaction.taxOtherAmount", "0");
                }
                return kernelSetSession(modsession);
            }).as_dynamic()
            .map([=](auto){
                setNextKernelParam
                (
                 {"PDL", "select", ""},
                 json_t()
                 );
                return pprx::unit();
            });
        }).as_dynamic();
    }
    else if((name == "wait") && (obj == "applicationSelect")){
        return kernelGetSession()
        .flat_map([=](const_json_t session){
            return emvOnUiSelectApplication(session);
        }).as_dynamic()
        .flat_map([=](const_json_t json){
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();
                modsession.put("transaction.application", json.get<std::string>("aid"));
                return kernelSetSession(modsession);
            }).as_dynamic()
            .map([=](auto){
                setNextKernelParam
                (
                 {"BVR", "select", "application"},
                 json_t()
                 );
                return pprx::unit();
            });
        }).as_dynamic();
    }
    else if((name == "wait") && (obj == "inputKid")){
        return emvOnUiInputKid()
        .flat_map([=](const_json_t json){
            setNextKernelParam
            (
             {"NODEIN", "proceed", "inputKidComplete"},
             json
             );
            return pprx::just(pprx::unit());
        }).as_dynamic();
    }
    else if((name == "wait-NODEIN") && (obj == "amount")){
        /* 磁気フォールバック判定は wait-NODEIN, amount で行う */
        return kernelGetSession()
        .flat_map([=](const_json_t session){
            auto isMSFallback = session.get_optional<std::string>("transaction.isMSFallback");
            if(isMSFallback && (*isMSFallback == "true")){
                LogManager_AddWarning("no valid applications -> MS Fallback");
                json_t errresp;
                errresp.put("resultCode", "1");
                errresp.put("errorObject.errorCode", "PP_W0000");
                errresp.put("errorObject.errorLevel", "WARN");
                errresp.put("errorObject.errorType", "WARN");
                errresp.put("errorObject.errorMessage", "このカードはICでのお取引はできません。↑カードを抜いて、磁気カードリーダーを使用してください。");
                errresp.put("errorObject.printBugReport", "false");
                return pprx::error<pprx::unit>(make_error_paymentserver(errresp)).as_dynamic();
            }
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic()
        .flat_map([=](pprx::unit){
            return emvOnUiAmount();
        }).as_dynamic()
        .flat_map([=](const_json_t json){
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();

                auto amount = json.get<int>("amount");
                modsession.put("transaction.amount", boost::lexical_cast<std::string>(amount));
                
                auto taxOtherAmount = json.get_optional<int>("taxOtherAmount");
                modsession.put("transaction.taxOtherAmount", taxOtherAmount ? boost::lexical_cast<std::string>(*taxOtherAmount) : "0");
                
                return kernelSetSession(modsession);
            })
            .map([=](auto){
                setNextKernelParam
                (
                 {"NODEIN", "input", "amount"},
                 json_t()
                 );
                return pprx::unit();
            });
        }).as_dynamic();
    }
    else if(obj == "inputAmount"){
        return emvWaitInputPreAmount()
        .flat_map([=](const_json_t preInputData){
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();
                auto amount = preInputData.get<int>("amount");
                auto taxOtherAmount = preInputData.get_optional<int>("taxOtherAmount");
                modsession.put("transaction.amount", boost::lexical_cast<std::string>(amount));
                modsession.put("transaction.taxOtherAmount", taxOtherAmount ? boost::lexical_cast<std::string>(*taxOtherAmount) : "0");
                return kernelSetSession(modsession);
            }).as_dynamic();
        }).as_dynamic()
        .map([=](auto){
            setNextKernelParam
            (
             {"NODEIN", "wait", "icProcess"},
             json_t()
             );
            return pprx::unit();
        }).as_dynamic();
    }
    else if((name == "wait") && (obj == "inputPayWay")){
        return kernelGetSession()
        .flat_map([=](const_json_t session){
            return emvOnUiInputPayWay(session);
        })
        .flat_map([=](const_json_t json){
            return kernelGetSession()
            .flat_map([=](const_json_t session){
                auto modsession = session.clone();
                auto productCode = json.get_optional<std::string>("productCode");
                if(productCode){
                    modsession.put("transaction.productCode", *productCode);
                }
                auto payWayDiv = json.get_optional<std::string>("payWayDiv");
                if(payWayDiv){
                    modsession.put("transaction.payWayDiv", *payWayDiv);
                }
                else{
                    /* オーソリ予約時は"payWayDiv"が存在しないので"0"（一括）を指定する（仕様に明記なし） */
                    modsession.put("transaction.payWayDiv", "0");
                }
                return kernelSetSession(modsession);
            })
            .map([=](auto){
                setNextKernelParam
                (
                 {"NODEIN", "wait", "icProcess"},
                 json_t()
                 );
                return pprx::unit();
            });
        }).as_dynamic();
    }
    else if((name == "wait") && (obj == "amountConfirm")){
        auto _prepareNextKernel = [=]()
        {
            setNextKernelParam
            (
             {"NODEIN","wait","inputPin"},
             const_json_t()
             );
            return pprx::unit();
        };

        if(m_bOnlinePinReinput){
            m_jsPinResultConfirm.put("pinResult", "PinFail-deny");
            m_jsPinResultConfirm.put("bOnetimeRemain", false);
            return emvOnCardPinResultConfirm(m_jsPinResultConfirm).as_dynamic()
            .flat_map([=](auto){
                return hanldeInputPinProc(m_emvSession, true)
                .map([=](auto){
                    m_bOnlinePinReinput = false;
                    return pprx::unit();
                }).as_dynamic();
            }).as_dynamic();
        }

        return kernelGetSession()
        .flat_map([=](const_json_t session){
            m_emvSession = session.clone();
            return emvOnUiAmountConfirm(session);
        })
        .map([=](const_json_t body){
            auto pinCheckResult = body.get_optional<std::string>("normalObject.pinCheckResult");
            if(pinCheckResult && (*pinCheckResult == "1")){ /* PINチェック失敗 */
                m_bOnlinePinReinput = true;
                m_jsPinResultConfirm.put("pinResult", "PinFail-deny");
                return _prepareNextKernel();
            }
            // Correct PIN or Bypass success
            m_jsPinResultConfirm.put("pinResult", "success");
            m_jsPinResultConfirm.put("bOnetimeRemain", false);
            LogManager_AddDebugF("trade response :\n%s", body.str());
            
            auto onOffDiv = body.get_optional<std::string>("normalObject.onOffDiv");
            if((!onOffDiv) || (*onOffDiv == "0")){
                m_emvProcessing.get_subscriber().on_next(pprx::unit());
                m_tradeResultData = body.clone();
            }
            else{
                json_t json = body.get("normalObject");
                /* javascript へはbool, intは文字列として渡すらしい */
                {
                    auto amountFlg = json.get_optional<bool>("amountFlg");
                    if(amountFlg){
                        json.put("amountFlg", *amountFlg ? "true" : "false");
                    }
                    
                    auto result = json.get_optional<int>("result");
                    if(result){
                        json.put("result", boost::lexical_cast<std::string>(*result));
                    }
                }
                
                setNextKernelParam
                (
                 {"NODEIN","wait","icProcess"},
                 json
                 );
            }
            return pprx::unit();
        })
        .as_dynamic();
    }
    else if((name == "wait") && (obj == "inputPin")){
        // 取り敢えず、失敗とする。成功なら、amountConfirmのところで値を変更する。
        return kernelGetSession()
        .flat_map([=](const_json_t session){
            return hanldeInputPinProc(session).as_dynamic();
        });
    }
    else if((obj == "endOfEmv") || (obj == "endOfIcCardError")){
        return pprx::just(pprx::unit()).as_dynamic();
    }
    return pprx::error<pprx::unit>(make_error_internalf("invalid request type, device.name=\"%s\", device.value.obj=\"%s\"", name % obj))
    .as_dynamic();
}

auto JMupsEmvKernel::hanldeInputPinProc(const_json_t session, bool bReinput /*= false*/)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto kernelProcess = bReinput ? "inputPin" : "icProcess";   
    auto _prepareNextKernel = [=]()
    {
        setNextKernelParam
        (
         {"NODEIN","wait",kernelProcess },
         const_json_t()
         );
        return pprx::just(pprx::unit()).as_dynamic();
    };
    
    auto _prepareNextKernelInputPin = [=]()
    {
        setNextKernelParam
        (
         {"NODEIN","wait", "inputPin" },
         const_json_t()
         );
        return pprx::just(pprx::unit()).as_dynamic();
    };
    
    auto pinMessages    = PPConfig::instance().getChild("hps.pinMessages");
    auto samount        = session.get<std::string>("transaction.amount");
    auto amount         = boost::lexical_cast<int>(samount);
    auto commaAmount    = BaseSystem::commaSeparatedString(amount);
    auto messageId1     = session.get<std::string>("transaction.messageId1");
    auto messageId2     = session.get<std::string>("transaction.messageId2");
    auto msgFlg         = session.get<bool>("transaction.msgFlg");
    auto _getAndReplaceUiMessage = [=](const std::string& messageId) -> std::string {
        if(messageId.empty()) return "";
        auto msg = pinMessages.get_optional<std::string>(messageId);
        if(!msg){
            LogManager_AddWarningF("pin messageId = %s not found", messageId);
            return "";
        }
        auto d = boost::algorithm::replace_all_copy(*msg, "##AMOUNT##", commaAmount);
        return d;
    };
    json_t param;
    param.put("uiMessage.message1", _getAndReplaceUiMessage(messageId1));
    param.put("uiMessage.message2", _getAndReplaceUiMessage(messageId2));
    
    LogManager_AddInformationF("msgFlg: %s, messageId1: \"%s\", messageId2: \"%s\"", (msgFlg ? "true":"false") % messageId1 % messageId2);
    
    m_jsPinResultConfirm.put("bOnetimeRemain", false);
    if(messageId1=="14" || messageId1=="4"){ // Correct PIN
        m_jsPinResultConfirm.put("pinResult", "success");
    }
    else if( messageId1=="13" || messageId1=="3"){ // Incorrect PIN
        m_jsPinResultConfirm.put("pinResult", "PinFail-deny");
    }
    else if( messageId1=="15" || messageId1=="5" ){ // Call Issuer
        m_jsPinResultConfirm.put("pinResult", "PinFail-over");
    }
    
    if((!m_bOfflinePinConfirmed&&!messageId2.empty())
       || msgFlg ){
        if(messageId2=="12" || messageId2=="2"){
            m_jsPinResultConfirm.put("bOnetimeRemain", true);
        }
        
        return emvOnCardPinResultConfirm(m_jsPinResultConfirm)
        .flat_map([=](auto){
            m_bOfflinePinConfirmed = true;
            const auto pinResult = m_jsPinResultConfirm.get_optional<std::string>("pinResult");
            if(pinResult && ((*pinResult) == "PinFail-deny")){
                return _prepareNextKernelInputPin().as_dynamic();
            }
            else{
                return _prepareNextKernel().as_dynamic();
            }
        }).as_dynamic();
    }
    
    m_bOfflinePinConfirmed = false;
    if(!msgFlg){
        auto sinspection = session.get_optional<std::string>("transaction.inspectionMethod");
        if(sinspection){
            auto applicationList = session.get<json_t::array_t>("transaction.applicationList");
            auto applicationListIndex = session.get<int>("transaction.applicationListIndex");
            bool bOnEncipheredPin = false;
            
            param.put("amount"      , amount);
            param.put("application" , applicationList[applicationListIndex]);
            
            auto inspection = boost::lexical_cast<int>(*sinspection);
            LogManager_AddInformationF("inspectoin Method: %d", inspection);
            LogManager_AddInformationF("------ session ---------\n %s", session.str());
            pprx::observable<const_json_t> verifyPin;
            if ((inspection == 2) || (inspection == 4)) {
                verifyPin = emvOnCardInputPlainOfflinePin(param);
            }
            else if ((inspection == 5) || (inspection == 6)) {
                verifyPin = emvOnCardInputEncryptedOfflinePin(param);
            }
            else if (inspection == 3) {
                /* オンライン暗号PIN（銀聯のみ） */
                bOnEncipheredPin = true;
                verifyPin = emvOnCardInputEncryptedOnlinePin(param);
            }
            else{
                return pprx::error<pprx::unit>(make_error_internalf("unknown inspectoin Method. %d", inspection)).as_dynamic();
            }
            return verifyPin
            .flat_map([=](const_json_t json) mutable{
                auto modsession = session.clone();
                auto pinResult = json.get<std::string>("result");
                if(pinResult == "success"){
                    modsession.put("transaction.resultMsg", json.get<std::string>("data"));
                    modsession.put("transaction.pinBypassFlg", "false");
                    modsession.put("transaction.pinpadResult", "0");
                    if(bOnEncipheredPin){
                        modsession.put("transaction.onEncipheredPin", json.get<std::string>("data"));
                    }
                    return kernelSetSession(modsession)
                    .flat_map([=](auto){
                        return pprx::maybe::success(pprx::unit());
                    }).as_dynamic();
                }
                else if(pinResult == "bypass"){
                    modsession.put("transaction.pinBypassFlg", "true");
                    modsession.put("transaction.pinpadResult", "0");
                    return kernelSetSession(modsession)
                    .flat_map([=](auto){
                        return pprx::maybe::success(pprx::unit());
                    }).as_dynamic();
                }
                else if(pinResult == "retry"){
                    json_t errorJson;
                    errorJson.put("description", "pinTimeOut");
                    return pprx::maybe::error(make_error_cardrw(errorJson)).as_dynamic();
                }
                
                return pprx::maybe::error(make_error_internalf("unknown pinResult : %s", pinResult))
                .as_dynamic();
            }).as_dynamic()
            .on_error_resume_next([=](auto err){
                LogManager_AddError(pprx::exceptionToString(err));
                return pprx::maybe::error(err);
            })
            .retry()
            .flat_map([=](pprx::maybe flow){
                return flow.observableForContinue<pprx::unit>();
            }).as_dynamic()
            .flat_map([=](auto){
                return _prepareNextKernel();
            }).as_dynamic();
        }
    }
    
    return _prepareNextKernel().as_dynamic();
}

auto JMupsEmvKernel::isTrainingMode() const -> bool
{
    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj == nullptr) return false;
    return pj->isTrainingMode();
}

auto JMupsEmvKernel::isUseDigiSignInTrainingMode() const -> bool
{
    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj == nullptr) return false;
    return pj->isUseDigiSignInTrainingMode();
}

auto JMupsEmvKernel::trainingEmvRun(JMupsJobStorage::sp storage)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    storage->setTrainingModePinInputted(false);
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        return emvOnUiSelectDeal();
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        auto mA0770_1 = [](){
            json_t j;
            j.put("aid", "a0000000031010");
            j.put("brandApJudgeNo","00");
            j.put("brandApVersion","0096");
            j.put("fci"
                  ,"6f248407a0000000031010a51950095649534144454249548701019f38039f1a025f2d026a61");
            j.put("ifdSerialNumber", "11217076");
            j.put("isTraining", "true");
            j.put("mposDeviceType", "1");
            j.put("operationDiv", "0");
            j.put("supportCupIc", "1");
            return j.as_readonly();
        };
        /* 金額の先行入力を開始するため */
        return emvOnNetwork("ma0770.do?mA0770_1=aaa", mA0770_1());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        auto mA0770_3 = [](){
            json_t j;
            j.put("isTraining", "true");
            j.put("mposDeviceType","1");
            j.put("rdrcdCmdResList",nullptr);
            j.put("supportCupIc", "1");
            return j.as_readonly();
        };
        /* カード情報を確定させるため */
        return emvOnNetwork("ma0770.do?mA0770_3=aaa", mA0770_3());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        return emvOnUiAmount();
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        return emvOnUiInputPayWay(const_json_t());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        return emvOnTrainingCardReaderDisplayData();
    }).as_dynamic()
    .flat_map([=](auto displayData){
        BaseSystem::sleep(500);
        return emvOnCardInputEncryptedOnlinePin(displayData);
    }).as_dynamic()
    .flat_map([=](const_json_t resp){
        LogManager_AddDebugF("PIN input result %s", resp.str());
        auto pinResult = resp.get_optional<std::string>("result");
        if(*pinResult == "success"){
            storage->setTrainingModePinInputted(true);
        }
        else if(*pinResult == "bypass"){
            storage->setTrainingModePinInputted(false);
        }
        else if(*pinResult == "retry"){
            storage->setTrainingModePinInputted(false);
        }
        
        BaseSystem::sleep(500);
        auto param = [](){
            json_t j;
            j.put("bOnetimeRemain", false);
            j.put("pinResult", "success");
            return j.as_readonly();
        };
        return emvOnCardPinResultConfirm(param());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        auto sA1027_1 = [](){
            json_t j;
            j.put("firstGACCmdRes"  , "801280033d1f3c8964c2d08f1906010a03a0a0009000");
            j.put("isRePrint"       , "false");
            j.put("isTraining"      , "true");
            return j.as_readonly();
        };
        return emvOnNetwork("sa1027.do?sA1027_1=aaa", sA1027_1());
    }).as_dynamic()
    .flat_map([=](const_json_t result){
        BaseSystem::sleep(500);
        m_tradeResultData = addMediaDivToCardNo(result);
        m_jsTradeConfirmFinish.put("hps", m_tradeResultData);
        return emvOnTradeConfirmFinish(m_jsTradeConfirmFinish.clone()).as_dynamic()
        .map([=](auto){
            return pprx::unit();
        }).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        LogManager_AddFullInformationF("ICで取引結果：\n %s", m_tradeResultData.str());
        return m_tradeResultData.as_readonly();
    });
}

auto JMupsEmvKernel::trainingEmvRunLevel1(JMupsJobStorage::sp storage)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    storage->setTrainingModePinInputted(false);
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        return emvOnUiSelectDeal();
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        auto mA0770_1 = [](){
            json_t j;
            j.put("aid", "a0000000031010");
            j.put("brandApJudgeNo","00");
            j.put("brandApVersion","0096");
            j.put("fci"
                  ,"6f248407a0000000031010a51950095649534144454249548701019f38039f1a025f2d026a61");
            j.put("ifdSerialNumber", "11217076");
            j.put("isTraining", "true");
            j.put("mposDeviceType", "1");
            j.put("operationDiv", "0");
            j.put("supportCupIc", "1");
            return j.as_readonly();
        };
        /* 金額の先行入力を開始するため */
        return emvOnNetwork("ma0770.do?mA0770_1=aaa", mA0770_1());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        auto mA0770_3 = [](){
            json_t j;
            j.put("isTraining", "true");
            j.put("mposDeviceType","1");
            j.put("rdrcdCmdResList",nullptr);
            j.put("supportCupIc", "1");
            return j.as_readonly();
        };
        /* カード情報を確定させるため */
        return emvOnNetwork("ma0770.do?mA0770_3=aaa", mA0770_3());
    }).as_dynamic()
    .flat_map([=](auto){
        BaseSystem::sleep(500);
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

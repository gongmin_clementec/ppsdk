//
//  JMupsAccessReprint.h
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//



#if !defined(__h_JMupsAccessReprint__)
#define __h_JMupsAccessReprint__

#include "JMupsAccess.h"

class JMupsAccessReprint :
    virtual public JMupsAccess
{
private:
    
protected:
    struct prmReprintCommon
    {
        enum class paymentType
        {
            Bad,
            Credit,
            NFC,
            UnionPay
        };
        static paymentType stringToPaymentType(const std::string& str);
    };
    
    auto hpsReprintLastSlip(prmReprintCommon::paymentType paymentType) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsReprintWithSlipNo(prmReprintCommon::paymentType paymentType, const std::string& slipNo) -> pprx::observable<JMupsAccess::response_t>;

public:
            JMupsAccessReprint() = default;
    virtual ~JMupsAccessReprint() = default;
    
};

#endif /* !defined(__h_JMupsAccessReprint__) */

//
//  JavascriptEnvironment.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JavascriptEnvironment__)
#define __h_JavascriptEnvironment__

#include "LazyObject.h"
#include "PPReactive.h"

class JavascriptEnvironment :
    public LazyObject
{
public:
    using sp = boost::shared_ptr<JavascriptEnvironment>;
    
private:
    
protected:
    
public:
            JavascriptEnvironment(const __secret& s);
    virtual ~JavascriptEnvironment();

    virtual auto load
    (
     const std::string& htmlText,
     const std::string& documentBasePath
     ) -> pprx::observable<pprx::unit> = 0;
    
    virtual auto call(const std::string& jsText) -> pprx::observable<std::string> = 0;
};

#endif /* !defined(__h_JavascriptEnvironment__) */

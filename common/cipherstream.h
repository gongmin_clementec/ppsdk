#if ! defined( __h_cipherstream__ )
#define __h_cipherstream__

#include <streambuf>
#include <iostream>
#include <vector>
#include <memory>

#include <openssl/evp.h>

namespace ORANGEWERKS {
    
    class cipherstream_exception :
    public std::exception
    {
    private:
        std::string m_strError;
        
    protected:
        
    public:
        cipherstream_exception( const char* strError ) : m_strError( strError ) {}
        virtual const char* what() const noexcept { return( m_strError.c_str() ); }
    };
    
    /*
     ・flush() や sync() では全データは出力されず、デストラクタで全データが出力される
     ・_Elem は 1バイトでないと動作しない
     */
    
    enum class ECipherMethod
    {
          Bad		= 0
        , NONE      = 1
        , DES
        , DES_EDE
        , DES_EDE3
        , DESX
        , IDEA
        , RC2
        , RC2_40
        , RC2_64
        , BF
        , CAT5
        , AES128
        , AES192
        , AES256
        , CAMELLIA128
        , CAMELLIA192
        , CAMELLIA256
        , SEED
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_cipherstreambuf : public std::basic_streambuf<_Elem,_Traits>
    {
    public:
        
    private:
        using superclass_t = std::basic_streambuf<_Elem,_Traits>;
        
        std::basic_ostream<_Elem,_Traits>*		m_out;
        std::basic_istream<_Elem,_Traits>*		m_in;
        
        std::vector<_Elem>		m_srcBuff;		/* バッファ実体 */
        std::vector<_Elem>		m_dstBuff;		/* バッファ実体 */
        
        bool							m_bFinished;
        std::auto_ptr<EVP_CIPHER_CTX>	m_ctx;
        
    protected:
        /* 外部からの出力バッファ指定 */
        virtual superclass_t* setbuf( _Elem* /* b */, std::streamsize /* s */ )
        {
            return( NULL );
        }
        
        /* 書き込みバッファが一杯になった時に呼び出される */
        virtual typename superclass_t::int_type overflow( typename superclass_t::int_type c = _Traits::eof() )
        {
            overflow_self();
            if( c == _Traits::eof() ){
                return( _Traits::eof() );
            }
            /* c はバッファから溢れたオブジェクトなので、ここで追加する */
            *superclass_t::pptr() = _Traits::to_char_type( c );
            superclass_t::pbump( 1 );
            return( _Traits::not_eof( c ) );
        }
        
        /* 読み込みバッファが空になった時に呼び出される */
        virtual typename superclass_t::int_type underflow()
        {
            if( superclass_t::egptr() <= superclass_t::gptr() ){
                underflow_self();
                if( m_bFinished && ( superclass_t::egptr() <= superclass_t::gptr() ) ){
                    return( _Traits::eof() );
                }
            }
            return( _Traits::to_int_type( *superclass_t::gptr() ) );
        }
        
        /* flush() で呼び出される */
        virtual int sync()
        {
            if( m_out ){
                encrypt();
                m_out->flush();
            }
            return( 0 );
        }
        
        
        void overflow_self()
        {
            encrypt();
        }
        
        void encrypt( bool bFinish = false )
        {
            if( m_bFinished ){
                this->setp( &m_srcBuff.front(), &m_srcBuff.back() + 1 );	/* buffer ポインタリセット */
                return;
            }
            
            const ptrdiff_t length = superclass_t::pptr() - &m_srcBuff.front();	/* 使用バッファサイズ */
            
            if( length > 0 ){
                int	encryptedBytes = 0;
                ::EVP_EncryptUpdate(	m_ctx.get(),
                                    reinterpret_cast<unsigned char*>( &m_dstBuff.front() ),
                                    &encryptedBytes,
                                    reinterpret_cast<const unsigned char*>( &m_srcBuff.front() ),
                                    static_cast<int>( length ) );
                if( encryptedBytes > 0 ){
                    m_out->write( &m_dstBuff.front(), encryptedBytes );		/* stream に出力 */
                }
            }
            
            if( bFinish ){
                m_bFinished = true;
                int	encryptedBytes = 0;
                ::EVP_EncryptFinal( m_ctx.get(), reinterpret_cast<unsigned char*>( &m_dstBuff.front() ), &encryptedBytes );
                if( encryptedBytes > 0 ){
                    m_out->write( &m_dstBuff.front(), encryptedBytes );		/* stream に出力 */
                }
            }
            
            this->setp( &m_srcBuff.front(), &m_srcBuff.back() + 1 );	/* buffer ポインタリセット */
        }
        
        
        
        void underflow_self()
        {
            if( m_bFinished ){
                this->setg( &m_dstBuff.front(), &m_dstBuff.front(), &m_dstBuff.front() );
                return;
            }
            
            
            m_in->read( &m_srcBuff.front(), m_srcBuff.size() );	/* stream から入力してバッファに展開 */
            const std::streamsize sz = m_in->gcount();			/* stream から読み込んだバイト数 */
            
            int decryptedBytes = 0;
            
            if( sz == 0 ){
                m_bFinished = true;
                ::EVP_DecryptFinal( m_ctx.get(), reinterpret_cast<unsigned char*>( &m_dstBuff.front() ), &decryptedBytes );
            }
            else{
                ::EVP_DecryptUpdate(	m_ctx.get(),
                                    reinterpret_cast<unsigned char*>( &m_dstBuff.front() ),
                                    &decryptedBytes,
                                    reinterpret_cast<const unsigned char*>( &m_srcBuff.front() ),
                                    static_cast<int>( sz ) );
            }
            
            this->setg( &m_dstBuff.front(), &m_dstBuff.front(), &m_dstBuff.front() + decryptedBytes );	/* buffer ポインタリセット */
        }
        
        
        static const EVP_CIPHER* MethodToEVPCipher( ECipherMethod method )
        {
            const EVP_CIPHER* cipher = NULL;
            
            switch( method ){
                case ECipherMethod::NONE:           { cipher = ::EVP_enc_null();			break; }
                case ECipherMethod::DES:			{ cipher = ::EVP_des_cbc();				break; }
                case ECipherMethod::DES_EDE:		{ cipher = ::EVP_des_ede_cbc();			break; }
                case ECipherMethod::DES_EDE3:       { cipher = ::EVP_des_ede3_cbc();		break; }
                case ECipherMethod::DESX:           { cipher = ::EVP_desx_cbc();			break; }
                case ECipherMethod::IDEA:           { cipher = ::EVP_idea_cbc();			break; }
                case ECipherMethod::RC2:			{ cipher = ::EVP_rc2_cbc();				break; }
                case ECipherMethod::RC2_40:         { cipher = ::EVP_rc2_40_cbc();			break; }
                case ECipherMethod::RC2_64:         { cipher = ::EVP_rc2_64_cbc();			break; }
                case ECipherMethod::BF:             { cipher = ::EVP_bf_cbc();				break; }
                case ECipherMethod::CAT5:           { cipher = ::EVP_cast5_cbc();			break; }
                case ECipherMethod::AES128:         { cipher = ::EVP_aes_128_cbc();			break; }
                case ECipherMethod::AES192:         { cipher = ::EVP_aes_192_cbc();			break; }
                case ECipherMethod::AES256:         { cipher = ::EVP_aes_256_cbc();			break; }
                case ECipherMethod::CAMELLIA128:	{ cipher = ::EVP_camellia_128_cbc();	break; }
                case ECipherMethod::CAMELLIA192:	{ cipher = ::EVP_camellia_192_cbc();	break; }
                case ECipherMethod::CAMELLIA256:	{ cipher = ::EVP_camellia_256_cbc();	break; }
                case ECipherMethod::SEED:           { cipher = ::EVP_seed_cbc();			break; }
                case ECipherMethod::Bad:
                default:
                    break;
            }
            
            return( cipher );
        }
        
        
    public:
        basic_cipherstreambuf( std::basic_ostream<_Elem,_Traits>& os ) :
        m_out( &os ), m_in( NULL ), m_bFinished( false )
        {
        }
        
        basic_cipherstreambuf( std::basic_istream<_Elem,_Traits>& is ) :
        m_out( NULL ), m_in( &is ), m_bFinished( false )
        {
        }
        
        static size_t get_block_bytes( ECipherMethod method )
        {
            const EVP_CIPHER* cipher = MethodToEVPCipher( method );
            if( cipher == NULL ) return( 0 );
            return( ::EVP_CIPHER_key_length( cipher ) );
        }
        
        /* iv ... Initialization Vector
         暗号化は CBC (Chipher Block Chaining) モードで行われる。
         このモードは、nブロック目の暗号化データを、n+1ブロック目にxorすることで、
         辞書攻撃を回避するものだが、そのn=0の時の初期ブロック（仮想の-1ブロック目）を
         指定するためのバイト列。
         したがって、key（暗号ブロック長）と同じ長さである必要がある。
         */
        void initialize( ECipherMethod method, const std::vector<unsigned char>& key, const std::vector<unsigned char>& iv )
        {
            const EVP_CIPHER* cipher = MethodToEVPCipher( method );
            if( cipher == NULL ) throw( cipherstream_exception( "basic_cipherstreambuf: bad method" ) );
            
            const size_t blockBytes = get_block_bytes( method );
            
            /* ブロック長より大きい場合 */
            if( blockBytes < key.size()	) throw( cipherstream_exception( "basic_cipherstreambuf: bad key block bytes" ) );
            if( blockBytes < iv.size()	) throw( cipherstream_exception( "basic_cipherstreambuf: bad iv block bytes" ) );
            
            /* key と iv をコピーする */
            std::vector<unsigned char>	_key(key );
            std::vector<unsigned char>	_iv( iv );
            
            /* ブロック長より小さい場合残りをゼロで埋める */
            while( blockBytes > _key.size()	) _key.push_back( 0 );
            while( blockBytes > _iv.size()	) _iv.push_back( 0 );
            
            m_ctx.reset( new EVP_CIPHER_CTX );
            ::EVP_CIPHER_CTX_init( m_ctx.get() );
            
            m_srcBuff.resize( _key.size() * 16 );
            m_dstBuff.resize( m_srcBuff.size() + blockBytes );	/* EVP_EncryptUpdate() で出力されるデータは±ブロック長－１ */
            
            if( m_out ){
                ::EVP_EncryptInit( m_ctx.get(), cipher, &_key.front(), &_iv.front() );
                this->setp( &m_srcBuff.front(), &m_srcBuff.back() + 1 );
            }
            if( m_in ){
                ::EVP_DecryptInit( m_ctx.get(), cipher, &_key.front(), &_iv.front() );
                this->setg( &m_dstBuff.front(), &m_dstBuff.back() + 1, &m_dstBuff.back() + 1 );
            }
        }
        
        void initialize( ECipherMethod method, const std::vector<unsigned char>& key )
        {
            std::vector<unsigned char>	iv;
            std::copy(std::begin(key), std::end(key), std::begin(iv));
            initialize( method, key, iv );
        }
        
        virtual ~basic_cipherstreambuf()
        {
            if( m_out ){
                encrypt( true );
            }
            sync();
            if( m_ctx.get() ){
                ::EVP_CIPHER_CTX_cleanup( m_ctx.get() );
            }
        }
        
        
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_ocipherstream : public std::basic_ostream<_Elem,_Traits>
    {
    private:
        using superclass_t      = std::basic_ostream<_Elem,_Traits>;
        using cipherstreambuf_t = basic_cipherstreambuf<_Elem,_Traits>;
    protected:
    public:
        basic_ocipherstream( std::ostream& os ) :
        std::basic_ostream<_Elem,_Traits>( new cipherstreambuf_t( os ) )
        {
        }
        
        virtual ~basic_ocipherstream()
        {
            superclass_t::flush();
            delete superclass_t::rdbuf();
        }
        
        void initialize( ECipherMethod method, const std::vector<unsigned char>& key, const std::vector<unsigned char>& iv )
        {
            cipherstreambuf_t* buff = dynamic_cast<cipherstreambuf_t*>( superclass_t::rdbuf() );
            buff->initialize( method, key, iv );
        }
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_icipherstream : public std::basic_istream<_Elem,_Traits>
    {
    private:
        using superclass_t      = std::basic_istream<_Elem,_Traits>;
        using cipherstreambuf_t = basic_cipherstreambuf<_Elem,_Traits>;
        
    protected:
    public:
        basic_icipherstream( std::istream& is ) :
        std::basic_istream<_Elem,_Traits>( new cipherstreambuf_t( is ) )
        {
        }
        
        virtual ~basic_icipherstream()
        {
            delete superclass_t::rdbuf();
        }
        
        void initialize( ECipherMethod method, const std::vector<unsigned char>& key, const std::vector<unsigned char>& iv )
        {
            cipherstreambuf_t* buff = dynamic_cast<cipherstreambuf_t*>( superclass_t::rdbuf() );
            buff->initialize( method, key, iv );
        }
    };
    
    using icipherstream = basic_icipherstream<char>;
    using ocipherstream = basic_ocipherstream<char>;
    
}	/* namespace ORANGEWERKS */

#endif /* ! defined( __h_cipherstream__ ) */
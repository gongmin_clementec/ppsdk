//
//  SerialDevice.h
//  ppsdk
//
//  Created by tel on 2019/02/18.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_SerialDevice__)
#define __h_SerialDevice__

#include "PPReactive.h"
#include <boost/enable_shared_from_this.hpp>

class SerialDevice : virtual public boost::enable_shared_from_this<SerialDevice>
{
public:
    class error{
    public:
        class open : public std::exception
        {
        private:
        protected:
        public:
            open() = default;
            virtual ~open() = default;
            virtual const char* what() const noexcept { return "open"; }
        };


        class disconnected : public std::exception
        {
        private:
        protected:
        public:
            disconnected() = default;
            virtual ~disconnected() = default;
            virtual const char* what() const noexcept { return "disconnected"; }
        };

        class data : public std::exception
        {
        private:
        protected:
        public:
            data() = default;
            virtual ~data() = default;
            virtual const char* what() const noexcept { return "data"; }
        };

        class unknown : public std::exception
        {
        private:
        protected:
        public:
            unknown() = default;
            virtual ~unknown() = default;
            virtual const char* what() const noexcept { return "unknown"; }
        };
    };

    
    using sp = boost::shared_ptr<SerialDevice>;
    using wp = boost::weak_ptr<SerialDevice>;

private:
    
protected:
    std::exception_ptr makeOpenError()
    {
        return std::make_exception_ptr(error::open());
    }

    std::exception_ptr makeDisconnectedError()
    {
        return std::make_exception_ptr(error::disconnected());
    }

    std::exception_ptr makeDataError()
    {
        return std::make_exception_ptr(error::data());
    }

    std::exception_ptr makeUnknownError()
    {
        return std::make_exception_ptr(error::unknown());
    }

public:
            SerialDevice() = default;
    virtual ~SerialDevice() = default;

    virtual auto open(const_json_t param) -> pprx::observable<pprx::unit> = 0;
    virtual auto close() -> pprx::observable<pprx::unit> = 0;
    virtual auto getReadObservable() -> pprx::observable<bytes_sp> = 0;
    virtual void writeAsync(bytes_sp data) = 0;
    virtual const_json_t getDeviceInfo() = 0;
};



#endif /* !defined(__h_SerialDevice__) */

//
//  JMupsAccessCoupon.h
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//



#if !defined(__h_JMupsAccessCoupon__)
#define __h_JMupsAccessCoupon__

#include "JMupsAccess.h"

class JMupsAccessCoupon :
    virtual public JMupsAccess
{
private:
    
protected:
    struct prmCouponSalesAutoDiscount_communication
    {
        enum class serviceDiv
        {
            Bad,
            Credit,
            UnionPay
        };
    };
    
    
    pprx::observable<JMupsAccess::response_t> hpsCouponSalesAutoDiscount_communication
    (
     int                amount,
     prmCouponSalesAutoDiscount_communication::serviceDiv serviceDiv);
    
    
    const_json_t findCoupon(const_json_t json, const std::string& couponId) const;
    boost::optional<int> getCouponSelectListNum(const_json_t json, const std::string& couponId) const;

public:
            JMupsAccessCoupon() = default;
    virtual ~JMupsAccessCoupon() = default;
    
};

#endif /* !defined(__h_JMupsAccessCoupon__) */

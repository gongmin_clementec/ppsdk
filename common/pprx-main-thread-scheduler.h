//
//  pprx-main-thread-scheduler.h
//  ppsdk
//
//  Created by tel on 2017/11/15.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_pprx_main_thread_scheduler__)
#define __h_pprx_main_thread_scheduler__

#include <rxcpp/rx-scheduler.hpp>
#include <boost/asio.hpp>
#include "BaseSystem.h"


namespace pprx {
    using namespace rxcpp;
    using namespace rxcpp::schedulers;

    class main_thread : public scheduler_interface
    {
    private:
        main_thread(const main_thread&) = delete;
        
        struct main_thread_worker : public worker_interface
        {
        private:
            composite_subscription m_lifetime;

        public:
            explicit main_thread_worker(composite_subscription cs)
            : m_lifetime(cs)
            {
            }
            
            virtual ~main_thread_worker()
            {
                m_lifetime.unsubscribe();
            }
            
            virtual clock_type::time_point now() const override { return clock_type::now(); }
            
            virtual void schedule(const schedulable& scbl) const override
            {
                if (scbl.is_subscribed()) {
                    auto keep_alive = shared_from_this();
                    BaseSystem::instance().invokeMainThread([=](){
                        auto _ = keep_alive;
                        (void)_;
                        scbl(recursion(true).get_recurse());
                    });
                }
            }
            
            virtual void schedule(clock_type::time_point when, const schedulable& scbl) const override
            {
                if (scbl.is_subscribed()) {
                    printf("scheduled on %p with timeout\n", this);
                    auto keep_alive = shared_from_this();
                    auto timer = std::make_shared<boost::asio::basic_waitable_timer<clock_type>>
                    (BaseSystem::instance().ioService(), when);
                    timer->async_wait([=](const boost::system::error_code&) {
                        BaseSystem::instance().invokeMainThread([=](){
                            auto _ = keep_alive;
                            auto __ = timer;
                            (void)_;
                            (void)__;
                            scbl(recursion(true).get_recurse());
                        });
                    });
                }
            }
            
        };
        
    public:
        main_thread(){}
        
        virtual ~main_thread() { }
        
        virtual clock_type::time_point now() const { return clock_type::now(); }
        
        virtual worker create_worker(composite_subscription cs) const
        {
            return worker(cs, std::make_shared<main_thread_worker>(cs));
        }
    };
    
    inline scheduler make_main_thread()
    {
        return make_scheduler<main_thread>();
    }

    inline observe_on_one_worker observe_on_main_thread()
    {
        return observe_on_one_worker(make_main_thread());
    }
    
    inline synchronize_in_one_worker synchronize_in_main_thread()
    {
        return synchronize_in_one_worker(make_main_thread());
    }
    
    inline identity_one_worker identitiy_main_thread()
    {
        return identity_one_worker(make_main_thread());
    }
    
    inline serialize_one_worker serialize_main_thread()
    {
        return serialize_one_worker(make_main_thread());
    }
}

#endif /* !defined(__h_pprx_main_thread_scheduler__) */

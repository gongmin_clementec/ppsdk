//
//  JobCreditCardCheckJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobCreditCardCheckJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"

JobCreditCardCheckJMups::JobCreditCardCheckJMups(const __secret& s) :
    JobCreditSalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobCreditCardCheckJMups::~JobCreditCardCheckJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobCreditCardCheckJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    m_bIsMSFallback = false;
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.credit").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return waitForCardCheckResultConfirm().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging().as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobCreditCardCheckJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::CardCheck;
}


/* virtual */
auto JobCreditCardCheckJMups::sendTradeConfirm()
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    return hpsCreditCardCheck_Confirm();
}

/* virtual */
auto JobCreditCardCheckJMups::sendTagging()
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsCreditCardCheck_Tagging
        (
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmCredit::printResultDiv::Success
         );
    }).as_dynamic();
}

/* virtual */
void JobCreditCardCheckJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobCreditCardCheckJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "5");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

/* virtual */
auto JobCreditCardCheckJMups::emvOnNetwork(const std::string& url, const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    return hpsAccessFromEmvKernel(url, param)
    .map([=](auto resp){
        if(url == "ma0770.do?mA0770_1=aaa"){
            beginAsyncPreInputAmount();
        }
        else if(url == "ma0770.do?mA0770_3=aaa"){
            /*
             売上以外 EMVKernel は Level 1 相当で動作する。
             会社判定に失敗しても、磁気と同じ処理を走行させる。
             */
            m_cardInfoData = resp.body.clone();
        }
        else if(url == "sa1770.do?sA1770_3=aaa"){
            m_cardInfoData = resp.body.clone();
        }
        return resp.body;
    });
}

/* virtual */
auto JobCreditCardCheckJMups::waitForTradeInfo(const std::string&  strTradeOp)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return applyDispatchResponseParamForTradeInfo()
    .flat_map([=](bool bApply){
        if(bApply) return pprx::maybe::success(pprx::unit());
        else return pprx::maybe::retry();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::maybe::error(err);
    })
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    });
}


auto JobCreditCardCheckJMups::waitForStoreTradeResultData(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmCredit::doubleTrade>
{
    LogManager_Function();
    
    auto tradeData = addMediaDivToCardNo(jmupsData.body);
    m_tradeConfirmFinished.put("hps", tradeData);
    tradeData.put("ppsdk.bTradeCompletionFlag", false);
    getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
    return pprx::just(prmCredit::doubleTrade::No).as_dynamic();
}

auto JobCreditCardCheckJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    LogManager_AddDebug(data.str());
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::Credit,
     getOperationDiv(),
     m_bIsMSFallback,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_cardInfoData = resp.body.clone();
        return processCommon();
    }).as_dynamic();
}

/* virtual */
auto JobCreditCardCheckJMups::proceedContact(const_json_t data)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return getCardReader()->beginICCardTransaction()
    .flat_map([=](auto){
        return emvRunLevel1(getJMupsStorage());
    }).as_dynamic()
    .flat_map([=](auto){
        return processCommon();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->endICCardTransaction();
    });
}

auto JobCreditCardCheckJMups::processCommon() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
            .as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            return waitForKidInput()
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTradeInfo("Confirm").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTradeConfirm().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForStoreTradeResultData(jmups).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    });
}

auto JobCreditCardCheckJMups::applyDispatchResponseParamForTradeInfo()
-> pprx::observable<bool>
{
    LogManager_Function();
    
    // IC カード保有者検証必須パラメーター
    m_tradeData.put("amount",          100);
    m_tradeData.put("taxOtherAmount",    0);
    m_tradeData.put("paymentWay",   "lump");

    return pprx::just(true);
}

auto JobCreditCardCheckJMups::treatSlipDocumentJson()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto pinfo = tradeResultData.get("normalObject.printInfo.device.value").clone();
    return pprx::just(pinfo.as_readonly());
}

auto JobCreditCardCheckJMups::waitForCardCheckResultConfirm()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    
    json_t param;
    param.put("CARD_NO", tradeResultData.get<std::string>("normalObject.tradeResult.CARD_NO"));
    param.put("SLIP_NO", tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"));
    param.put("A_NO", tradeResultData.get<std::string>("normalObject.tradeResult.A_NO"));
    param.put("checkResult", tradeResultData.get<std::string>("normalObject.checkResult"));
    
    return callOnDispatchWithCommandAndSubParameter("waitForCardCheckResultConfirm", "", param)
    .map([=](auto){
        return pprx::unit();
    });
}

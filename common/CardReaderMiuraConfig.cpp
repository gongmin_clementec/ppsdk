//
//  CardReaderMiuraConfig.cpp
//  ppsdk
//
//  Created by tel on 2020/01/23.
//  Copyright © 2020年 Clementec Co., Ltd. All rights reserved.
//

#include "CardReaderMiuraConfig.h"
#include "CardReaderUtil.h"
#include <boost/algorithm/hex.hpp>
#include <openssl/md5.h>
#include "LogManager.h"


CardReaderMiuraConfig::CardReaderMiuraConfig()
{
    LogManager_Function();
}

CardReaderMiuraConfig::~CardReaderMiuraConfig()
{
    LogManager_Function();
}

auto CardReaderMiuraConfig::createContactlessConfigData(const_json_t rootJson) -> std::string
{
    LogManager_Function();
    LogManager_AddInformationF("rootJson = %s.", rootJson.str());
    std::string ret;
    // header
    ret += createContactlessConfigHeader(rootJson);
    // Reader Combinations
    auto masterJson = getValue(rootJson, "normalObject.masterData");
    ret += createContactlessConfigCOMB(masterJson);
    // Terminal configuration
    ret += createContactlessConfigTERM(masterJson);
    // Applications specific configuration
    ret += createContactlessConfigAPP(masterJson);
    // Dynamic Reader Limits specific configuration for Visa kernel
    ret += createContactlessConfigDRL(masterJson);
    LogManager_AddDebugF("[createContactlessConfigData] result. ret = %s", ret);
    return ret;
}

auto CardReaderMiuraConfig::createContactlessPromptsData(const_json_t prompts) -> std::string
{
    std::string ret;
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_APPROVED") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_DECLINED") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_PLEASE_ENTER_PIN") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_ERROR_PROCESSING") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_REMOVE_CARD") + "\n";
    ret += "no_prompt\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_PRESENT_CARD") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_PROCESSING") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_CARD_READ_OK_REMOVE") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_INSERT_OR_SWIPE_CARD") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_CARD_COLLISION") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_PLEASE_SIGN") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_ONLINE_AUTHORISATION") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_TRY_OTHER_CARD") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_INSERT_CARD") + "\n";
    ret += "clear_screen\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_SEE_PHONE") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_PRESENT_CARD_AGAIN") + "\n";
    ret += prompts.get<std::string>("MIURA_CONTACTLESS_UI_CALL_YOUR_BANK") + "\n";
    ret += (boost::format("$Revision: %s $\n") % jsonToMd5String(prompts)).str();
    LogManager_AddDebugF("[createContactlessPromptsData] result. ret = %s", ret);
    return ret;
}              

auto CardReaderMiuraConfig::retrieveConfigRevision(const bytes_t apdu, const std::string filename) -> std::string
{
    LogManager_AddDebugF("GET CONFIGURATION apdu = \"%s\".", CardReaderUtil::binaryToHextext(apdu));
    LogManager_AddDebugF("GET CONFIGURATION filename = %s.", filename);
    int32_t pos = 0;
    std::string revision;
    bytes_t tag_ED = {0xED};
    int32_t totalLength = 0;
    // E1 Template tag Skip
    pos += 1;
    // E1 Template length check
    if((apdu[pos] & 0x80) == 0) {
        totalLength = apdu[pos];
        pos += 1;
    } else {
        // MSBがONの場合、下位7ビットが次以降のbyteに続く長さフィールド長を表す。
        int32_t length_L = apdu[pos] & 0x7F;
        pos += + 1;
        for(int i = length_L; i > 0; i--) {
            totalLength |= apdu[pos++] << 8 * (i - 1);
        }
    }
    // E1 Template Value was list of identifiers wraped ED template
    for(; pos < totalLength;) {
        auto taglength = CardReaderUtil::getTagLength(apdu.begin() + pos);
        bytes_t tag = bytes_t(apdu.begin() + pos, apdu.begin() + pos + taglength);
        int32_t datalength = apdu.operator[](pos + taglength);
        if(tag != tag_ED) {
            pos += taglength + 1 + datalength;
            continue;
        }
        bytes_t indata = bytes_t(apdu.begin() + pos, apdu.end());
        bytes_t configInformation = CardReaderUtil::getTLVDataFromRAPDU(indata, {0xED});
        bytes_t identifier = CardReaderUtil::getTLVDataFromRAPDU(configInformation, {0xDF, 0x0D});
        std::string searchFilename = std::string(identifier.begin(), identifier.end());
        if(searchFilename == filename) {
            bytes_t version = CardReaderUtil::getTLVDataFromRAPDU(configInformation, {0xDF, 0x7F});
            revision = std::string(version.begin(), version.end());
            LogManager_AddDebugF("searchFilename = %s.", searchFilename);
            LogManager_AddDebugF("match filename exist. return revision = %s.\n", revision);
            break;
        }
        pos += taglength + 1 + configInformation.size();
    }
    return revision;
}

auto CardReaderMiuraConfig::createContactlessConfigHeader(const_json_t rootJson) -> std::string
{
    LogManager_Function();
    std::string date = "";
    std::string updateDate = getValue<std::string>(rootJson, "normalObject.updateDate");
    if(updateDate.size() != 0) {
        date = updateDate.substr(2, 8);
    }
    std::string revisionStr = jsonToMd5String(rootJson.get("normalObject.masterData"));
    LogManager_AddDebugF("createContactlessConfigHeader revisionStr = %s.", revisionStr);
    return (boost::format("\n# Config Date %s\n# Author Ivan Kvesic\n# $Revision: %s $\n\n") % date % revisionStr).str();
}

auto CardReaderMiuraConfig::createContactlessConfigCOMB(const_json_t masterJson) -> std::string
{
    LogManager_Function();
    std::string ret = "";
    auto kernelVisa = getValue(masterJson, "kernel3File");
    if(!kernelVisa.empty() && kernelVisa.str() != "null") {
        ret += createContactlessConfigCOMBVisa(kernelVisa);
    }
    auto kernelMaster = getValue(masterJson, "kernel2File");
    if(!kernelMaster.empty() && kernelMaster.str() != "null") {
        ret += createContactlessConfigCOMBMaster(kernelMaster);
    }
    auto kernelAmex = getValue(masterJson, "kernel4File");
    if(!kernelAmex.empty() && kernelAmex.str() != "null") {
        ret += createContactlessConfigCOMBAmex(kernelAmex);
    }
    auto kernelJCB = getValue(masterJson, "kernel5File");
    if(!kernelJCB.empty() && kernelJCB.str() != "null") {
        ret += createContactlessConfigCOMBJCB(kernelJCB);
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigCOMBVisa(const_json_t kernel) -> std::string
{
    LogManager_Function();
    std::string ret;
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(auto aidInfo : aidInfoArray) {
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Entry Point Settings: Visa Reader Combination - %s\n") % getTransactionTypeString(transactionType)).str();
        ret += (boost::format("COMB=%s\n") % getAidFromAidInfo(aidInfo, AidOutSpec::RID)).str();
        ret += (boost::format("9F2A=%s\n") % getKernelIdFromAidInfo(aidInfo)).str();
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += "DF01=00\n";
        ret += "DF02=01\n";
/*　TODO : ダミーデータここから　*/
        ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "109")).str();
        if( getTransactionTypeString(transactionType) != "Refund"){
            ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "110")).str();
            ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "111")).str();
        }
        else{
            ret += "DF04=000000000001\n";
            ret += "DF05=000000000001\n"; 
        }
        ret += "9F1B=00003A98\n";
        ret += "9F66=32504000\n";
/*　TODO : ダミーデータここまで　*/
        ret += "9F29=00\n";
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::getAidFromAidInfo(const_json_t aidInfo, AidOutSpec spec /* = AidOutSpec::AID */) -> std::string
{
    std::string aidLv = getValue<std::string>(aidInfo, "100");
    if(aidLv.size() == 0) {
        return std::string("");
    }
    std::string aidAsciiLen;
    boost::algorithm::unhex(aidLv.begin(), aidLv.begin() + 4, std::back_inserter(aidAsciiLen));
    // 場所によって欲しいものがRIDだったりAIDだったりしている。
    // RIDを指定した場合は長さ5バイト指定で値を取り出して返す。
    int aidLen = (spec == AidOutSpec::AID) ? std::stoi(aidAsciiLen) : 5;
    std::string aid = aidLv.substr(4, (aidLen * 2));
    boost::algorithm::to_upper(aid);
    return aid;
}

auto CardReaderMiuraConfig::getKernelIdFromAidInfo(const_json_t aidInfo) -> std::string
{
    std::string kernelLv = getValue<std::string>(aidInfo, "101");
    if(kernelLv.size() == 0) {
        return std::string("");
    }
    // 「通貨指定時」はKernel IDに長いデータが設定されてくるが1バイトしか取得しない
    return kernelLv.substr(4, (1 * 2));
}

auto CardReaderMiuraConfig::getTransactionTypeString(const std::string transactionType) -> std::string
{
    std::string value_9C = "";
    if(transactionType == "00") {
        // Visaの場合は本来「Purchase without Cashback」が設定されるべきだが、
        // この文字列はコメントに記載するだけで動作に影響はないので他のブランドと統一して問題ない。
        value_9C = "Purchase";
    } else if(transactionType == "01") {
        value_9C = "Purchase with Cash";
    } else if(transactionType == "09") {
        value_9C = "Purchase with Cashback";
    } else if(transactionType == "20") {
        value_9C = "Refund";
    }
    return value_9C;
}

auto CardReaderMiuraConfig::decimalStrToHexStr(const_json_t json, const std::string key) -> std::string
{
    std::string value = getValue<std::string>(json, key);
    if(value.size() == 0) {
        return std::string("");
    }
    auto retValue = std::stoi(value);
    return (boost::format("%08x") % retValue).str();
}

auto CardReaderMiuraConfig::getTlvValueString(const_json_t json, const std::string key) -> std::string
{
    std::string tlvStr = getValue<std::string>(json, key);
    if(tlvStr.size() == 0) {
        return std::string("");
    }
    bytes_t binTlv;
    boost::algorithm::unhex(tlvStr.begin(), tlvStr.end(), std::back_inserter(binTlv));
    int tagLength = CardReaderUtil::getTagLength(binTlv.begin());
    bytes_t tag = bytes_t(binTlv.begin(), binTlv.begin() + tagLength);
    auto retValue = CardReaderUtil::getTLVDataFromRAPDU(binTlv, tag);
    std::string retValueStr;
    boost::algorithm::hex(retValue.begin(), retValue.end(), back_inserter(retValueStr));
    return retValueStr;
}

template <typename T> auto CardReaderMiuraConfig::getValue(const_json_t json, const std::string key) -> T
{
    auto opt = json.get_optional<T>(key);
    return  opt ? *opt : T();
}

auto CardReaderMiuraConfig::getValue(const_json_t json, const std::string key) -> json_t
{
    auto opt = json.get_optional(key);
    return  opt ? *opt : json_t();
}

auto CardReaderMiuraConfig::createContactlessConfigCOMBMaster(const_json_t kernel) -> std::string
{
    LogManager_Function();
    std::string ret;
    json_t::array_t temp = json_t::array_t();
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(auto aidInfo : aidInfoArray) {
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Entry Point Settings: Mastercard Reader Combination - %s\n") % getTransactionTypeString(transactionType)).str();
        ret += (boost::format("COMB=%s\n") % getAidFromAidInfo(aidInfo, AidOutSpec::RID)).str();
        ret += (boost::format("9F2A=%s\n") % getKernelIdFromAidInfo(aidInfo)).str();
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += "DF01=00\n";
        ret += "DF02=01\n";
/*　TODO : ダミーデータここから　*/
        ret += "DF03=000000000000\n";
/*　TODO : ダミーデータここまで　*/
        ret += (boost::format("9F1B=%s\n") % decimalStrToHexStr(aidInfo, "105")).str();
        if( getTransactionTypeString(transactionType) != "Refund"){
            ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "108")).str();
            ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "109")).str();
        }
        else{
            ret += "DF04=000000000001\n";
            ret += "DF05=000000000001\n"; 
        }
        ret += "9F29=00\n";
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigCOMBAmex(const_json_t kernel) -> std::string
{
    LogManager_Function();
    std::string ret;
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(auto aidInfo : aidInfoArray) {
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Entry Point Settings: Amex Reader Combination - %s\n") % getTransactionTypeString(transactionType)).str();
        ret += (boost::format("COMB=%s\n") % getAidFromAidInfo(aidInfo)).str();
        ret += (boost::format("9F2A=%s\n") % getKernelIdFromAidInfo(aidInfo)).str();
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += (boost::format("9F1B=%s\n") % decimalStrToHexStr(aidInfo, "105")).str();
        ret += "DF01=01\n";
        ret += "DF02=01\n";
        ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "106")).str();
        if( getTransactionTypeString(transactionType) != "Refund"){
            ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "107")).str();
            ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "108")).str();
        }
        else{
            ret += "DF04=000000000001\n";
            ret += "DF05=000000000001\n"; 
        }
        ret += "9F29=01\n";
        ret += "DF06=01\n";
        ret += "DF08=01\n";
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigCOMBJCB(const_json_t kernel) -> std::string
{
    LogManager_Function();
    std::string ret;
    std::string merchantCategoryCode = getTlvValueString(kernel, "2");
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(auto aidInfo : aidInfoArray) {
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Entry Point Settings: JCB Reader Combination - %s\n") % getTransactionTypeString(transactionType)).str();
        ret += (boost::format("COMB=%s\n") % getAidFromAidInfo(aidInfo)).str();
        ret += (boost::format("9F2A=%s\n") % getKernelIdFromAidInfo(aidInfo)).str();
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += (boost::format("9F15=%s\n") % merchantCategoryCode).str();
        ret += (boost::format("9F1B=%s\n") % decimalStrToHexStr(aidInfo, "105")).str();
        ret += "DF01=00\n";
        ret += "DF02=01\n";
        ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "113")).str();
        if( getTransactionTypeString(transactionType) != "Refund"){
            ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "112")).str();
            ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "114")).str();
        }
        else{
            ret += "DF04=000000000001\n";
            ret += "DF05=000000000001\n"; 
        }

        ret += "DF06=00\n";
        ret += "DF08=02\n";
        ret += "9F29=01\n";
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigTERM(const_json_t masterJson) -> std::string
{
    LogManager_Function();
    std::string ret = "# Terminal Settings\n";
    ret += "TERM=\n";
    ret += "5F2A=0978\n";
    ret += "5F57=00\n";
/*　TODO : ダミーデータここから　*/
    ret += "9C00=0C\n";
    ret += "9C01=2C\n";
    ret += "9C09=0C\n";
    ret += "9C20=00\n";
/*　TODO : ダミーデータここまで　*/
/*　TODO : 要確認ここから　*/
    std::string merchantIdentifierStr = "";
    auto kernel4aidInfo = getValue<json_t::array_t>(masterJson, "kernel4File.AIDINFO");
    if(kernel4aidInfo.size() != 0) {
        merchantIdentifierStr = getTlvValueString(kernel4aidInfo[0], "110");
    }
    ret += (boost::format("9F16=%s\n") % (merchantIdentifierStr.size() != 0 ? merchantIdentifierStr : "00")).str();
/*　TODO : 要確認ここまで　*/
    ret += (boost::format("9F1A=%s\n") % getTlvValueString(masterJson, "kernelCommonFile.1")).str();
    ret += "9F1C=3132333435363738\n";
    ret += "9F1E=00\n";
/*　TODO : 要確認ここから　*/
    auto kernel2aidInfo = getValue<json_t::array_t>(masterJson, "kernel2File.AIDINFO");
    std::string terminalCapabilities = "";
    if(kernel2aidInfo.size() != 0) {
        terminalCapabilities = getTlvValueString(kernel2aidInfo[0], "110");
    }
    if(terminalCapabilities.size() == 0 && kernel4aidInfo.size() != 0) {
        terminalCapabilities = getTlvValueString(kernel4aidInfo[0], "113");
    }
//    ret += (boost::format("9F33=%s\n") % terminalCapabilities).str();
    ret += "9F33=E0B8C8\n";
    ret += (boost::format("9F4E=%s\n") % getValue<std::string>(masterJson, "merchantTable.1")).str();
    ret += "FF0D=FC50808800\n";
    ret += "FF0E=0000000000\n";
    ret += "FF0F=FC60ACF800\n";
    ret += "DF8104=\n";
    ret += "DF8105=\n";
    ret += "DF811C=003C\n";
    ret += "DF811D=01\n";
/*　TODO : 要確認ここまで　*/
    ret += "DFDF13=00\n";
    ret += "DFDF14=A00000002501\n";
    ret += "DFDF16=00000080\n";
    ret += "DFDF20=00\n";
    ret += "DFDF21=03\n";
    auto unpredictableNumberRange = getValue<std::string>(masterJson, "kernel4File.AIDINFO[0].117");
    ret += (boost::format("DFDF23=%s\n") % (unpredictableNumberRange.size() != 0 ? unpredictableNumberRange : "3C")).str();
    ret += "DFDF51=00\n";
    ret += "DFDF57=9F359F339F1A5F2A9F029F039A9F21575A5F345F249F26829F369F37959B9C9F109F069F099F279F34\n";
    ret += "DFDF58=9F359F339F1A5F2A9F029F039A9F215F24829F369F37959B9C9F069F099F279F34DFDF41DFDF42\n";
    ret += "DFDF01=A00000002501001000A00000002501110001A00000002501000010A00000002501000011A00000002501000101A00000002501000110A00000002501000111A00000002501001001A00000002501001010A00000002501001011A00000002501001100A00000002501001101A00000002501001110A00000002501001111A00000002501010000A00000002501010001A00000002501010010A00000002501010011A00000002501010100A00000002501010101A00000002501010111A00000002501011000A00000002501011001A00000002501011010A00000002501011011A00000002501011100A00000002501011101A00000002501011110A00000002501011111A00000002501100000\n";
    ret += "\n";
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigAPP(const_json_t masterJson) -> std::string
{
    LogManager_Function();
    std::string ret;
    std::string sectionAPP = "# Application Settings: ";
    if(masterJson.empty()) {
        return std::string("");
    }
    ret += createContactlessConfigAPPMaster(masterJson);
    ret += createContactlessConfigAPPVisa(masterJson);
    ret += createContactlessConfigAPPAmex(masterJson);
    ret += createContactlessConfigAPPJCB(masterJson);
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigAPPMaster(const_json_t master) -> std::string
{
    LogManager_Function();
    boost::function<std::string(const json_t::array_t)> getTacFromIcTable = [=](const json_t::array_t icCardArray) {
        std::string ret = "";
        for(json_t icCardInfo : icCardArray) {
            std::string rid = getValue<std::string>(icCardInfo, "2");
            if(boost::iequals(rid, "a000000004")) {
                ret += (boost::format("DF8120=%s\n") % getValue<std::string>(icCardInfo, "5")).str();
                ret += (boost::format("DF8121=%s\n") % getValue<std::string>(icCardInfo, "6")).str();
                ret += (boost::format("DF8122=%s\n") % getValue<std::string>(icCardInfo, "7")).str();
                break;
            }
        }
        return ret;
    };
    std::string ret;
    const_json_t kernel = getValue(master, "kernel2File");
    if(kernel.empty() || kernel.str() == "null") {
        return std::string("");
    }
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(json_t aidInfo : aidInfoArray) {
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Application Settings: Mastercard - %s\n") % getTransactionTypeString(transactionType)).str();
        ret += "APP=A000000004\n";
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += (boost::format("9F01=%s\n") % getTlvValueString(kernel, "1")).str();
        ret += (boost::format("9F06=%s\n") % getAidFromAidInfo(aidInfo)).str();
        ret += (boost::format("9F09=%s\n") % getTlvValueString(aidInfo, "113")).str();
        ret += (boost::format("9F6D=%s\n") % getTlvValueString(kernel, "2")).str();
        ret += (boost::format("9F15=%s\n") % getTlvValueString(kernel, "8")).str();
        ret += (boost::format("9F35=%s\n") % getTlvValueString(aidInfo, "112")).str();
        ret += (boost::format("9F40=%s\n") % getTlvValueString(aidInfo, "111")).str();
        ret += "9F7E=00\n";
        ret += (boost::format("DF810C=%s\n") % getKernelIdFromAidInfo(aidInfo)).str();
        ret += "DF8117=00\n";
        ret += (boost::format("DF8118=%s\n") % getTlvValueString(aidInfo, "114")).str();
        ret += (boost::format("DF8119=%s\n") % getTlvValueString(aidInfo, "115")).str();
        ret += (boost::format("DF811A=%s\n") % getTlvValueString(kernel, "11")).str();
        ret += (boost::format("DF811B=%s\n") % getTlvValueString(aidInfo, "118")).str();
        ret += (boost::format("DF811E=%s\n") % getTlvValueString(aidInfo, "116")).str();
        ret += "DF811F=08\n";
        ret += getTacFromIcTable(getValue<json_t::array_t>(master, "icCardTable"));
        ret += (boost::format("DF8123=%s\n") % getValue<std::string>(aidInfo, "108")).str();
        ret += (boost::format("DF8124=%s\n") % getValue<std::string>(aidInfo, "106")).str();
        ret += (boost::format("DF8125=%s\n") % getValue<std::string>(aidInfo, "107")).str();
        ret += (boost::format("DF8126=%s\n") % getValue<std::string>(aidInfo, "109")).str();
        ret += (boost::format("DF812C=%s\n") % getTlvValueString(aidInfo, "117")).str();
        ret += (boost::format("DF812D=%s\n") % getTlvValueString(kernel, "3")).str();
        if(boost::iequals(transactionType, "20")) {
            ret += "70=00\n";
        }
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigAPPVisa(const_json_t master) -> std::string
{
    LogManager_Function();
    std::string ret;
    const_json_t kernel = getValue(master, "kernel3File");
    if(kernel.empty() || kernel.str() == "null") {
        return std::string("");
    }
    json_t aidInfo = json_t();
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() != 0) {
        aidInfo = aidInfoArray[0];
    }
    ret += "# Application Settings: Visa\n";
    ret += "APP=A000000003\n";
    ret += (boost::format("9F01=%s\n") % getTlvValueString(kernel, "1")).str();
    ret += (boost::format("9F06=%s\n") % getAidFromAidInfo(aidInfo, AidOutSpec::RID)).str();
/*　TODO : ダミーデータここから　*/
    ret += "9F09=0001\n";
    ret += "9F15=0001\n";
/*　TODO : ダミーデータここまで　*/
    ret += (boost::format("9F35=%s\n") % getTlvValueString(aidInfo, "106")).str();
    ret += "\n";
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigAPPAmex(const_json_t master) -> std::string
{
    LogManager_Function();
    std::string ret;
    const_json_t kernel = getValue(master, "kernel4File");
    if(kernel.empty() || kernel.str() == "null") {
        return std::string("");
    }
    json_t aidInfo = json_t();
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() != 0) {
        aidInfo = aidInfoArray[0];
    }
    ret += "# Application Settings: Amex\n";
    ret += "APP=A00000002501\n";
    ret += (boost::format("9C=%s\n") % getValue<std::string>(aidInfo, "102")).str();
/*　TODO : ダミーデータここから　*/
    ret += "9F01=00\n";
/*　TODO : ダミーデータここまで　*/
    ret += (boost::format("9F06=%s\n") % getAidFromAidInfo(aidInfo)).str();
    ret += (boost::format("9F09=%s\n") % getTlvValueString(aidInfo, "109")).str();
    ret += (boost::format("9F6D=%s\n") % getTlvValueString(aidInfo, "115")).str();
    ret += (boost::format("9F15=%s\n") % getTlvValueString(kernel, "3")).str();
    ret += (boost::format("9F35=%s\n") % getTlvValueString(aidInfo, "112")).str();
    ret += "9F6E=D8F00000\n";
    ret += "\n";
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigAPPJCB(const_json_t master) -> std::string
{
    LogManager_Function();
    std::string ret;
    const_json_t kernel = getValue(master, "kernel5File");
    if(kernel.empty() || kernel.str() == "null") {
        return std::string("");
    }
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() == 0) {
        return std::string("");
    }
    for(json_t aidInfo : aidInfoArray) {
        std::string aid = getAidFromAidInfo(aidInfo);
        std::string transactionType = getValue<std::string>(aidInfo, "102");
        ret += (boost::format("# Application Settings: %s - %s\n") % aid % getTransactionTypeString(transactionType)).str();
        ret += "APP=A0000000651010\n";
        ret += (boost::format("9C=%s\n") % transactionType).str();
        ret += (boost::format("9F01=%s\n") % getTlvValueString(kernel, "1")).str();
        ret += (boost::format("9F06=%s\n") % aid).str();
/*　TODO : ダミーデータここから　*/
        ret += "9F09=0001\n";
/*　TODO : ダミーデータここまで　*/
        ret += (boost::format("9F6D=%s\n") % getValue<std::string>(aidInfo, "108")).str();
        ret += (boost::format("9F15=%s\n") % getTlvValueString(kernel, "2")).str();
        ret += (boost::format("9F35=%s\n") % getTlvValueString(aidInfo, "111")).str();
/*　TODO : ダミーデータここから　*/
        ret += "70=9F5303708000DFDF1006000000002000DFDF110100DFDF120100\n";
/*　TODO : ダミーデータここまで　*/
        ret += "\n";
    }
    return ret;
}

auto CardReaderMiuraConfig::createContactlessConfigDRL(const_json_t masterJson) -> std::string
{
    LogManager_Function();
    std::string ret;
    const_json_t kernel = getValue(masterJson, "kernel3File");
    if(kernel.empty() || kernel.str() == "null") {
        return std::string("");
    }
    json_t aidInfo = json_t();
    auto aidInfoArray = getValue<json_t::array_t>(kernel, "AIDINFO");
    if(aidInfoArray.size() != 0) {
        aidInfo = aidInfoArray[0];
    }
    ret += "# Visa Dynamic Reader Limit Settings: Set 1\n";
    ret += "DRL=A000000003\n";
    ret += (boost::format("9F5A=%s\n") % getTlvValueString(aidInfo, "107")).str();
    ret += "DF01=00\n";
    ret += "DF02=01\n";
    ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "109")).str();
    ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "110")).str();
    ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "111")).str();
    ret += "\n";
    ret += "# Visa Dynamic Reader Limit Settings: Set 2\n";
    ret += "DRL=A000000003\n";
    ret += (boost::format("9F5A=%s\n") % getTlvValueString(aidInfo, "112")).str();
    ret += "DF01=00\n";
    ret += "DF02=01\n";
    ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "114")).str();
    ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "115")).str();
    ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "116")).str();
    ret += "\n";
    ret += "# Visa Dynamic Reader Limit Settings: Set 3\n";
    ret += "DRL=A000000003\n";
    ret += (boost::format("9F5A=%s\n") % getTlvValueString(aidInfo, "117")).str();
    ret += "DF01=00\n";
    ret += "DF02=01\n";
    ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "119")).str();
    ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "120")).str();
    ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "121")).str();
    ret += "\n";
    ret += "# Visa Dynamic Reader Limit Settings: Set 4\n";
    ret += "DRL=A000000003\n";
    ret += (boost::format("9F5A=%s\n") % getTlvValueString(aidInfo, "122")).str();
    ret += "DF01=00\n";
    ret += "DF02=01\n";
    ret += (boost::format("DF03=%s\n") % getValue<std::string>(aidInfo, "124")).str();
    ret += (boost::format("DF04=%s\n") % getValue<std::string>(aidInfo, "125")).str();
    ret += (boost::format("DF05=%s\n") % getValue<std::string>(aidInfo, "126")).str();
    ret += "\n";
    return ret;
}

auto CardReaderMiuraConfig::jsonToMd5String(const_json_t json) -> std::string
{
    MD5_CTX ctx;
    std::string serialStr = json.str();
    unsigned char buf[MD5_DIGEST_LENGTH] = {0};
    MD5_Init(&ctx);
    MD5_Update(&ctx, serialStr.c_str(), serialStr.size());
    MD5_Final(buf, &ctx);
    bytes_t md5Data(buf, std::end(buf));
    return CardReaderUtil::binaryToHextext(md5Data);;
}

auto CardReaderMiuraConfig::writeMasterData(std::string filename, std::string writeData) -> void
{
    auto masterDir = BaseSystem::instance().getDocumentDirectoryPath() / "MasterData";
    if(!boost::filesystem::exists(masterDir)) {
        boost::filesystem::create_directories(masterDir);
    }
    std::ofstream cfgOStream(masterDir.string() + "/" + filename);
    cfgOStream << writeData;
    cfgOStream.close();
    return;
}


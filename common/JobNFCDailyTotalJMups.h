//
//  JobNFCDailyTotalJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobNFCDailyTotalJMups__)
#define __h_JobNFCDailyTotalJMups__

#include "PaymentJob.h"
#include "JMupsAccessJournal.h"

class JobNFCDailyTotalJMups :
    public PaymentJob,
    public JMupsAccessJournal
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobNFCDailyTotalJMups(const __secret& s);
    virtual ~JobNFCDailyTotalJMups();
    
    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCDailyTotalJMups__) */

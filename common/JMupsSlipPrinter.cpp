//
//  JMupsSlipPrinter.cpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsSlipPrinter.h"
#include "AbstructFactory.h"
#include "SlipDocumentParser.h"
#include "SlipPrinter.h"

JMupsSlipPrinter::JMupsSlipPrinter() :
    m_printer(AbstructFactory::instance().createSlipPrinter())
{
    LogManager_Function();
    m_documentMap = {
        /////////////クレジット印字/////////////////
        /* クレジット売上（DCCなし） */
        { "R005", "CreditSales" },   // 金融機関
        { "R001", "CreditSales" },   // お客様　英語なし
        { "R002", "CreditSales.en"}, // お客様　英語付き
        { "R003", "CreditSales" },   // 加盟店
        { "R004", "CreditSales" },  // 加盟店本部
  
        /* クレジット取消返品（DCCなし） */
        { "R010", "CreditSales" },   // 金融機関
        { "R006", "CreditSales" },   // お客様　英語なし
        { "R007", "CreditSales.en"}, // お客様　英語付き
        { "R008", "CreditSales" },   // 加盟店
        { "R009", "CreditSales" },  // 加盟店本部
        
        /* クレジット売上（DCCあり） */
        { "R321", "DCCSelect" },    // DCC
        { "R326", "CreditSales"},   // 金融機関
        { "R322", "CreditSales.en"},// お客様
        { "R324", "CreditSales" },  // 加盟店
        { "R325", "CreditSales" },  // 加盟店本部

        /* クレジットオーソリ予約 */
        { "R015", "CreditPreApproval" }, // 金融機関
        
        /* クレジットオーソリ予約（DCCあり） */
        { "R327", "CreditPreApproval" }, // 金融機関

        /* クレジット承認後売上（DCCなし） */
        { "R020", "CreditSales" },    // 金融機関
        { "R016", "CreditSales" },    // お客様　英語なし
        { "R017", "CreditSales.en" }, // お客様　英語付き
        { "R018", "CreditSales" },    // 加盟店
        { "R019", "CreditSales" },    // 加盟店本部

        /* クレジット再印字（売上、取消返品、オーソリ予約、承認後売上） */
        { "R025", "CreditSales" },   // 金融機関
        { "R021", "CreditSales" },   // お客様　英語なし
        { "R022", "CreditSales.en"}, // お客様　英語付き
        { "R023", "CreditSales" },   // 加盟店
        { "R024", "CreditSales" },  // 加盟店本部
        
        /* クレジットチェック */
        { "R081", "CreditCheck"},  // 加盟店
        
        /* クレジットカード取引不成立*/
        { "R263", "Failure" },    // 印字
        { "R288", "Failure" },    // 再印字
        
        /* クレジット中間計 */
        { "R235", "CreditJournalSummary"},
        { "R236", "CreditJournalDetail"},

        /* クレジット日計 */
        { "R227", "CreditJournalSummary"},
        { "R228", "CreditJournalDetail"},

        /* クレジット日計再印字（前回・前々回） */
        { "R229", "CreditJournalSummary"},
        { "R230", "CreditJournalDetail"},
        
        /////////////NFC印字/////////////////
        /* NFC売上  */
        { "R405", "NFCSales" },   // 金融機関
        { "R402", "NFCSales.en"}, // お客様　英語付き
        { "R401", "NFCSales" },   // お客様　英語なし
        { "R403", "NFCSales" },   // 加盟店
        { "R404", "NFCSales" },  // 加盟店本部
        
        /* NFC中間計  */
        { "RI21", "NFCJournalSummary"},
        { "RI22", "NFCJournalDetail"},
        { "RI23", "NFCJournalSummary"},
        { "RI24", "NFCJournalDetail"},
        
        /* NFC日計  */
        { "RI11", "NFCJournalSummary"},
        { "RI12", "NFCJournalDetail"},
        { "RI15", "NFCJournalSummary"},
        { "RI16", "NFCJournalDetail"},
        
        /* NFC日計再印字 (前回・前々回)  */
        { "RI13", "NFCJournalSummary"},
        { "RI14", "NFCJournalDetail"},
        { "RI17", "NFCJournalSummary"},
        { "RI18", "NFCJournalDetail"},
 
        /////////////銀聯印字/////////////////
        /* 銀聯売上  */
        { "R086", "UnionPaySales" }, // 金融機関
        { "R083", "UnionPaySales" }, // お客様
        { "R084", "UnionPaySales" }, // 加盟店
        { "R085", "UnionPaySales" }, // 加盟店本部

        /* 銀聯取消返品  */
        { "R090", "UnionPayRefund" }, // 金融機関
        { "R087", "UnionPayRefund" }, // お客様
        { "R088", "UnionPayRefund" }, // 加盟店
        { "R089", "UnionPayRefund" }, // 加盟店本部
        
        /* 銀聯オーソリ予約  */
        { "R094", "UnionPaySales" }, // 金融機関
        { "R091", "UnionPaySales" }, // お客様
        { "R092", "UnionPaySales" }, // 加盟店
        { "R093", "UnionPaySales" }, // 加盟店本部
        
        /* 銀聯承認後売上  */
        { "R098", "UnionPaySales" }, // 金融機関
        { "R095", "UnionPaySales" }, // お客様
        { "R096", "UnionPaySales" }, // 加盟店
        { "R097", "UnionPaySales" }, // 加盟店本部
 
        /* 銀聯オーソリ予約取消  */
        { "R102", "UnionPayRefund" }, // 金融機関
        { "R099", "UnionPayRefund" }, // お客様
        { "R100", "UnionPayRefund" }, // 加盟店
        { "R101", "UnionPayRefund" }, // 加盟店本部
        
        /* 銀聯承認後売上取消  */
        { "R106", "UnionPayRefund" }, // 金融機関
        { "R103", "UnionPayRefund" }, // お客様
        { "R104", "UnionPayRefund" }, // 加盟店
        { "R105", "UnionPayRefund" }, // 加盟店本部
        
        /////////////銀聯再印字/////////////////
        /* 銀聯売上再印字  */
        { "R110", "UnionPaySales" }, // 金融機関
        { "R107", "UnionPaySales" }, // お客様
        { "R108", "UnionPaySales" }, // 加盟店
        { "R109", "UnionPaySales" }, // 加盟店本部
        
        /* 銀聯取消返品再印字  */
        { "R114", "UnionPayRefund" }, // 金融機関
        { "R111", "UnionPayRefund" }, // お客様
        { "R112", "UnionPayRefund" }, // 加盟店
        { "R113", "UnionPayRefund" }, // 加盟店本部
        
        /* 銀聯オーソリ予約再印字  */
        { "R118", "UnionPaySales" }, // 金融機関
        { "R115", "UnionPaySales" }, // お客様
        { "R116", "UnionPaySales" }, // 加盟店
        { "R117", "UnionPaySales" }, // 加盟店本部
        
        /* 銀聯承認後売上再印字  */
        { "R122", "UnionPaySales" }, // 金融機関
        { "R119", "UnionPaySales" }, // お客様
        { "R120", "UnionPaySales" }, // 加盟店
        { "R121", "UnionPaySales" }, // 加盟店本部
        
        /* 銀聯オーソリ予約取消再印字  */
        { "R126", "UnionPayRefund" }, // 金融機関
        { "R123", "UnionPayRefund" }, // お客様
        { "R124", "UnionPayRefund" }, // 加盟店
        { "R125", "UnionPayRefund" }, // 加盟店本部
        
        /* 銀聯承認後売上取消再印字  */
        { "R130", "UnionPayRefund" }, // 金融機関
        { "R127", "UnionPayRefund" }, // お客様
        { "R128", "UnionPayRefund" }, // 加盟店
        { "R129", "UnionPayRefund" }, // 加盟店本部
        
        /* 銀聯中間計 */
        { "R247", "UnionPayJournalSummary"},
        { "R248", "UnionPayJournalDetail"},
        
        /* 銀聯日計 */
        { "R239", "UnionPayJournalSummary"},
        { "R240", "UnionPayJournalDetail"},
        
        /* 銀聯日計再印字（前回・前々回） */
        { "R241", "UnionPayJournalSummary"},
        { "R242", "UnionPayJournalDetail"},
        
        /////////////銀聯取引不成立/////////////////
        /* 銀聯カード取引不成立*/
        { "R265", "UnionPayFailure" }, // 印字
        { "R289", "UnionPayFailure" },  // 再印字
        
        /////////////設定印字/////////////////
        { "R277", "SettingIssuers" },  //カード会社一覧
        { "R283", "TerminalService" },  //端末サービス情報
    };
}

/* virtual */
JMupsSlipPrinter::~JMupsSlipPrinter()
{
    LogManager_Function();
}

auto JMupsSlipPrinter::slipPrinterExecute(const_json_t printInfo)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return slipPrinterDispatch("waitForStartingSlipPrint", json_t())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](bool bExecute){
        if(bExecute){
            return printLoop(printInfo)
            .flat_map([=](auto){
                json_t json;
                json.put("status", "success");
                return slipPrinterMessage("printFinished", json);
            }).as_dynamic()
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }
        else{
            return pprx::just(pprx::unit()).as_dynamic();
        }
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddDebugF("slipPrinterExecute Error : \t %s", errotToStatusText(err));
        auto errorMsg = errotToStatusText(err);
        if(errorMsg == "error"){
            return pprx::error<pprx::unit>(make_error_internal("bExecute is nil")).as_dynamic();
        }
        else{
            json_t json;
            json.put("status", errorMsg);
            return slipPrinterMessage("printFinished", json)
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }
    }).as_dynamic();
}

std::string JMupsSlipPrinter::errotToStatusText(std::exception_ptr err) const
{
    boost::optional<std::string> result;
    
    try{ std::rethrow_exception(err); }
    catch(pprx::ppexception& pex){
        result = pex.json().get_optional<std::string>("result.detail.reason");
    }
    catch(...){
    }
    return result ? *result : "error";
}

auto JMupsSlipPrinter::printLoop(const_json_t printInfo) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto pageTrigger = pprx::subjects::behavior<int>(0);

    return pprx::just(pprx::unit(), pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        auto types = printInfo.get_optional<json_t::array_t>("type");
        if(!types){
            throw_error_internal("type not found.");
        }
        
        auto values = printInfo.get_optional<json_t::array_t>("values");
        if(!values){
            throw_error_internal("values not found.");
        }
        
        if(types->size() != values->size()){
            throw_error_internal("types->size() != values->size()");
        }
        
        return pageTrigger.get_observable()
        //.observe_on(pprx::observe_on_thread_pool())
        .flat_map([=](int nPage){
            json_t json;
            json.put("pageTotal", static_cast<int>(types->size()));
            json.put("pageCurrent", nPage + 1);
            return slipPrinterDispatch("waitForPrintingSlipPage", json)
            .observe_on(pprx::observe_on_thread_pool())
            .flat_map([=](bool bExecute){
                if(!bExecute){
                    throw_error_aborted("aborted");
                }
                return pprx::just(pprx::unit())
                .flat_map([=](auto){
                    return m_printer->openDocument();
                }).as_dynamic()
                .flat_map([=](auto){
                    return m_printer->openPage();
                }).as_dynamic()
                .flat_map([=](auto){
                    auto type = (*types)[nPage].get<std::string>();
                    return printOne(type, (*values)[nPage].clone());
                }).as_dynamic()
                .flat_map([=](auto){
                    return m_printer->closePage();
                }).as_dynamic()
                .flat_map([=](auto r){
                    return pprx::maybe::success(r);
                })
                .on_error_resume_next([=](auto err){
                    json_t perr;
                    perr.put("status", errotToStatusText(err));
                    return slipPrinterDispatch("waitForRecoverablePrinterError", perr)
                    .observe_on(pprx::observe_on_thread_pool())
                    .flat_map([=](bool bRetry){
                        if(bRetry){
                            return pprx::maybe::retry();
                        }
                        return pprx::maybe::error(err);
                    }).as_dynamic();
                })
                .retry()
                .flat_map([=](pprx::maybe m){
                    return m.observableForContinue<pprx::unit>();
                }).as_dynamic()
                .flat_map([=](auto){
                    BaseSystem::instance().post([=]{
                        const auto nextPage = nPage + 1;
                        if(nextPage >= types->size()){
                            pageTrigger.get_subscriber().on_completed();
                        }
                        else{
                            pageTrigger.get_subscriber().on_next(nextPage);
                        }
                    });
                    return pprx::just(pprx::unit());
                }).as_dynamic();
            }).as_dynamic();
        })
        .last();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        return m_printer->closeDocument()
        .flat_map([=](auto){
            return pprx::error<pprx::unit>(err);
        });
    })
    .flat_map([=](auto){
        return m_printer->closeDocument();
    }).as_dynamic();
}

auto JMupsSlipPrinter::printOne(const std::string& type, json_t json)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto it = m_documentMap.find(type);
    if(it == m_documentMap.end()){
        throw_error_internalf("file not found for template [\"%s\"]", type);
    }

    const std::string _INFORM = [&](){
        std::string r;
        for(auto i = 1; i <= 3; i++){
            auto INFORM = json.get_optional<std::string>((boost::format("INFORM%d") % i).str());
            if(INFORM){
                if(!r.empty()) r += '\n';
                r += (*INFORM);
            }
        }
        return r;
    }();
    if(!_INFORM.empty()){
        json.put("_INFORM", _INFORM);
    }

    const std::string _FAILURE_INFO = [&](){
        std::string r;
        auto arr = json.get_optional<json_t::array_t>("FAILURE_INFO");
        if(arr){
            for(auto s : *arr){
                if(!r.empty()) r += "\n";
                r += s.get<std::string>();
            }
        }
        return r;
    }();
    if(!_FAILURE_INFO.empty()){
        json.put("_FAILURE_INFO", _FAILURE_INFO);
    }
    
    {
        const bool bDCC = json.get_optional("EXCHANGE_RATE_SOURCE_NAME") ? true : false;
        if(bDCC){
            json.put("_DCC", true);
            auto CARD_BRAND_CODE = json.get_optional<std::string>("CARD_BRAND_CODE");
            if(CARD_BRAND_CODE){
                if((*CARD_BRAND_CODE) == "M") json.put("_MASTER", true);
                else if((*CARD_BRAND_CODE) == "V") json.put("_VISA", true);
            }
        }
    }
    
    {
        const bool bPAYMENTDETAIL = [=](){
            auto INSTALLMENT = json.get_optional<std::string>("INSTALLMENT");
            auto FIRSTTIME_CLAIM_MONTH = json.get_optional<std::string>("FIRSTTIME_CLAIM_MONTH");
            auto BONUS = json.get_optional<std::string>("BONUS");
            if(INSTALLMENT && !INSTALLMENT->empty()) return true;
            if(FIRSTTIME_CLAIM_MONTH && !FIRSTTIME_CLAIM_MONTH->empty()) return true;
            if(BONUS && !BONUS->empty()) return true;
            return false;
        }();
        if(bPAYMENTDETAIL) json.put("_PAYMENT_DETAIL", bPAYMENTDETAIL);
    }
    
    {
        auto is_TRAINING = json.get_optional<std::string>("is_TRAINING");
        if(is_TRAINING && ((*is_TRAINING) == "false")){
            json.remove_key("is_TRAINING");
        }
    }

    {
        auto is_REPRINT = json.get_optional<std::string>("is_REPRINT");
        if(is_REPRINT && ((*is_REPRINT) == "false")){
            json.remove_key("is_REPRINT");
        }
    }

    SlipDocumentParser parser(json);
    auto fname = it->second + ".txt";
    auto fpath = BaseSystem::instance().getResourceDirectoryPath() / "JMups" / "SlipTemplate" / fname;

    auto is = std::ifstream(fpath.string());
    auto doc = parser.execute(is);
    
    return m_printer->printPage(doc);
}


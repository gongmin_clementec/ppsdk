//
//  ZipFileCompressor.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "ZipFileCompressor.h"
#include <fstream>
#include <vector>
extern "C"{
#include "contrib/minizip/zip.h"
}

ZipFileCompressor::ZipFileCompressor() : m_zipfile(nullptr)
{
    
}

/* virtual */
ZipFileCompressor::~ZipFileCompressor()
{
    close();
}

bool ZipFileCompressor::open(const std::string& pathname)
{
    m_zipfile = ::zipOpen(pathname.c_str(), APPEND_STATUS_CREATE);
    return m_zipfile != nullptr;
}

void ZipFileCompressor::close()
{
    if(m_zipfile != nullptr){
        ::zipClose(m_zipfile, nullptr);
        m_zipfile = nullptr;
    }
}

uint32_t ZipFileCompressor::getStreamCrc32(std::istream& is) const
{
    unsigned long result = 0;
    
    auto pos = is.tellg();
    
    std::vector<char> buff(4 * 1024);
    
    while(!is.eof()){
        is.read(buff.data(), buff.size());
        if(is.gcount() > 0){
            result = ::crc32(result, reinterpret_cast<const unsigned char*>(buff.data()), static_cast<uInt>(is.gcount()));
        }
    }
    
    is.clear();
    is.seekg(pos, std::ios_base::beg);
    
    return static_cast<uint32_t>(result);
}


bool ZipFileCompressor::add(const std::string& pathInLocal, const std::string& pathInZipFile)
{
    std::ifstream fs;
    fs.open(pathInLocal.c_str());
    return add(fs, pathInZipFile);
}

bool ZipFileCompressor::add(std::istream& is, const std::string& pathInZipFile)
{
    bool bSuccess = false;
    do{
        const uLong crcFile = getStreamCrc32(is);
        const char* password = nullptr;
        const int   opt_compress_level = Z_DEFAULT_COMPRESSION;
        const int   zip64 = 0;
        
        zip_fileinfo zi;
        ::memset(&zi, 0, sizeof(zi));
        
        int err;
        err = ::zipOpenNewFileInZip3_64(    m_zipfile,              /* zipFile */
                                        pathInZipFile.c_str(),  /* filename */
                                        &zi,                    /* zip_fileinfo */
                                        nullptr, 0,             /* extrafield_local */
                                        nullptr, 0,             /* extrafield_global */
                                        nullptr,                /* comment */
                                        (opt_compress_level != 0) ? Z_DEFLATED : 0, /* method */
                                        opt_compress_level,     /* level */
                                        0,                      /* raw */
                                        -MAX_WBITS,             /* windowBits */
                                        DEF_MEM_LEVEL,          /* memLevel */
                                        Z_DEFAULT_STRATEGY,     /* strategy */
                                        password,               /* password */
                                        crcFile,                /* crcForCrypting */
                                        zip64);                 /* zip64 */
        if(err != ZIP_OK) break;
        
        std::vector<char> buff(4 * 1024);
        
        while(!is.eof()){
            is.read(buff.data(), buff.size());
            if(is.gcount() > 0){
                err = ::zipWriteInFileInZip(m_zipfile, buff.data(), static_cast<uInt>(is.gcount()));
                if(err != ZIP_OK) break;
            }
        }
        
        if(err != ZIP_OK){
            ::zipCloseFileInZip(m_zipfile);
        }
        else{
            err = ::zipCloseFileInZip(m_zipfile);
        }
        
        bSuccess = (err != ZIP_OK);
    }while(false);
    
    
    return bSuccess;
}

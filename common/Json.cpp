//
//  Json.cpp
//  ppsdk
//
//  Created by tel on 2017/07/13.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if defined(DEBUG)

#include <string>
#include <iostream>
#include "Json.h"

class basic_json_tester
{
    template <typename T> static void testinternal(std::basic_ostream<T>& output)
    {
        using jt = basic_json_t<T>;
        using st = std::basic_string<T>;
        using sst = std::basic_stringstream<T>;
        
        class conv
        {
        public:
            st operator () (const char* s) const
            {
                sst ss;
                while(*s != 0){
                    ss << static_cast<T>(*s);
                    s++;
                }
                return ss.str();
            }
        };
        conv c;
        
        auto&& sample = boost::lexical_cast<st>
        (
         R"--->(
         {
             "map":{
                 "string":"str\/ing",
                 "boolean_true":true,
                 "boolean_false":false,
                 "null":null,
                 "int":123,
                 "float":1.23,
                 "float2":.123,
                 "float3":123.,
                 "map":{
                     "string":"string",
                     "boolean_true":true,
                     "boolean_false":false,
                     "null":null,
                     "int":123,
                     "float":1.23,
                     "float2":.123,
                     "float3":123.
                 },
                 "arr":[
                        "string\u003A",
                        true,
                        false,
                        null,
                        123,
                        1.23,
                        .123,
                        123.,
                        [
                         "string\u003A",
                         true,
                         false,
                         null,
                         123,
                         1.23,
                         .123,
                         123.,
                         {
                         "string":"string",
                         "boolean_true":true,
                         "boolean_false":false,
                         "null":null,
                         "int":123,
                         "float":1.23,
                         "float2":.123,
                         "float3":123.
                         }
                         ]
                        ]
             },
             "arr":[
                    "string\u003A",
                    true,
                    false,
                    null,
                    123,
                    1.23,
                    .123,
                    123.,
                    [
                     "string\u003A",
                     true,
                     false,
                     null,
                     123,
                     1.23,
                     .123,
                     123.,
                     {
                     "string":"string",
                     "boolean_true":true,
                     "boolean_false":false,
                     "null":null,
                     "int":123,
                     "float":1.23,
                     "float2":.123,
                     "float3":123.
                     }
                     ]
                    ]
         }
         )--->"
        );
        
        
        jt json;
        
        json.deserialize(sample);
        
        output << std::endl;
        json.serialize(output, true);
        
        output << std::endl << json.template get_optional<st>(c("map.map.string")).get();
        output << std::endl << json.template get_optional<bool>(c("map.boolean_true")).get();
        
        /* get_optional() が内包するオブジェクトは参照ではなくコピーなので、変更してもオリジナルのjsonには影響を及ぼさない */
        {
            auto opt_i = json.template get_optional<int>(c("map.map.int"));
            if(opt_i){
                opt_i = 789;
            }
            
            auto&& opt_f = json.get_optional(c("map.map.float3"));
            if(opt_f){
                opt_f = 5.67;
            }
        }
        
        /* 非constなget()を使用すれば変更可能 */
#if defined(ENABLE_NON_CONST_GETTER)
//        {
//            auto& iv = json.template get(c("map.int"));
//            iv = 9012;
//            (json / boost::lexical_cast<st>("arr") / 1) = c("mod_string");
//        }
#endif
        
        
        /* put() を使用した値の変更（ノードが無ければ追加）*/
        json.put(c("map.string"), c("***** abc ****"));
        json.put(c("map2.string"), c("***** abc2 ****"));
        
        /* str() を使用すれば文字列展んかいする（内部的にはserialize()が動作している） */
        output << std::endl << c("json/map/string = ") << ( json / c("map") / c("string") ).str();
        output << std::endl << c("json/map/arr/1 = ") << ( json / c("map") / c("arr") / 1 ).str();
        
        output << std::endl;
        json.serialize(output, true);
    }
    
public:
    basic_json_tester()
    {
        testinternal<char>(std::cerr);
        testinternal<wchar_t>(std::wcerr);
    }
};

//static basic_json_tester test;


#endif

//
//  SlipPrinterESCP.cpp
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipPrinterESCP.h"
#include "TextUtils.h"
#include "Image.h"

SlipPrinterESCP::SlipPrinterESCP(const __secret& s) :
    SlipPrinter(s),
    m_currentFont(SlipDocument::Font::Normal)
{
}

/* virtual */
SlipPrinterESCP::~SlipPrinterESCP()
{
}

/* virtual */
auto SlipPrinterESCP::printPageSelf(SlipDocument::sp slip)
-> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit())
    .flat_map([=](auto){

        writeBytes({0x1B, 0x40});       /* ESC @ : コマンド初期化 */
        writeBytes({0x1B, 0x52, 0x08}); /* ESC R n : 国際文字の選択（日本） */
        writeBytes({0x1B, 0x74, 0x01}); /* ESC t n : 文字コードテーブルの選択（カタカナ） */
        writeBytes({0x1C, 0x43, 0x01}); /* FS C n : 漢字モード指定（シフトJIS漢字モード指定） */
        writeBytes({0x1B, 0x33, 0x1C}); /* ESC 3 n : 改行量の設定（28ドット） */

        auto&& doc = slip->getDocument();
        for(auto table : doc.tables){
            
            if(table.border == SlipDocument::Table::Border::On){
                writeBytes({0x1B, 0x33, 0x00}); /* ESC 3 n : 改行量の設定（0ドット） */
                doTableWithBorder(table);
                writeBytes({0x1B, 0x33, 0x1C}); /* ESC 3 n : 改行量の設定（28ドット） */
                continue;
            }
            
            for(auto tr : table.trs){
                setFont(tr.font);
                zhstr_t buff(getRowChars());
                
                if(tr.tds.size() == 1){
                    if(tr.tds[0].type == SlipDocument::Td::Type::WrappedText){
                        /* 折り返し文字列については、ここで特殊処理を行う */
                        writeString(zhstrToUtf8(doWrappedText(tr.tds[0])));
                        writeBytes({0x0A});
                        continue;
                    }
                    else if(tr.tds[0].type == SlipDocument::Td::Type::LogoImage){
                        writeBytes(doLogoImage());
                        continue;
                    }
                    else{
                        zhPutLeft(buff, tr.tds[0]);
                    }
                }
                else if(tr.tds.size() == 2){
                    zhPutLeft(buff, tr.tds[0]);
                    zhPutRight(buff, tr.tds[1]);
                }
                else if(tr.tds.size() == 3){
                    zhPutLeft(buff, tr.tds[0]);
                    zhPutRight(buff, tr.tds[2]);
                    zhPutCenter(buff, tr.tds[1]);
                }
                writeString(zhstrToUtf8(buff));
                writeBytes({0x0A});
            }
        }
        
        return pprx::just(pprx::unit());
    });
}

bytes_t SlipPrinterESCP::doLogoImage()
{
    auto image = logoImage();
    
    const int bwImageRowBytes = (image->width() + 7) / 8;
    bytes_t bwImage(bwImageRowBytes * image->height());
    
    const int start_x = (getRowPixels() - image->width()) / 2;
    const int actual_width = (getRowPixels() >= image->width()) ? image->width() : getRowPixels();
    
    for(int y = 0; y < image->height(); y++){
        const uint8_t*  src = image->data() + (y * image->rowBytes()) - start_x;
        uint8_t*        dst = bwImage.data() + (y * bwImageRowBytes);
        int     bits = 0;
        uint8_t bwbits = 0;
        for(int x = 0; x < actual_width; x++){
            bwbits <<= 1;
            if(*src == 0x00) bwbits |= 1;
            src++;
            bits++;
            if(bits == 8){
                *dst = bwbits;
                dst++;
                bits = 0;
                bwbits = 0;
            }
        }
        if(bits != 0){
            *dst = bwbits;
        }
    }
    
    bytes_t cmd;
    cmd.insert(cmd.end(), {0x1B, 0x58, 0x34, static_cast<uint8_t>(bwImageRowBytes), static_cast<uint8_t>(image->height())});
    cmd.insert(cmd.end(), bwImage.cbegin(), bwImage.cend());
    cmd.insert(cmd.end(), {0x1B, 0x58, 0x32, static_cast<uint8_t>(image->height())});
    return cmd;
}

void SlipPrinterESCP::doTableWithBorder(const SlipDocument::Table& table)
{
    if(table.trs.size() == 0) return;
    
    auto&& tds1st = table.trs[0].tds;
    
    /* 文字幅の割合の分母 */
    const int textRatioTotal = [&](){
        int r = 0;
        /* 文字 | 文字 | 文字 ... | 文字 */
        for(int i = 0; i < tds1st.size(); i += 2){
            r += tds1st[i].ratio;
        }
        return r;
    }();
    
    /* セパレータの数 */
    const auto countSeparator = (tds1st.size() / 2);    /* 奇数の切り捨てを利用 */

    const auto& colchars = [&](){
        std::vector<uint32_t> r;
        const auto printerTotalChars = getRowChars();
        const auto printerTotalTextChars = printerTotalChars - (countSeparator * 2);

        uint32_t charsTotal = 0;
        for(int i = 0; i < tds1st.size(); i++){
            if((i % 2) == 0){
                if(i == (tds1st.size() - 1)){
                    /* 最後は端数を含め印刷領域の最大まで確保する */
                    r.push_back(printerTotalChars - charsTotal);
                }
                else{
                    const auto chars = ((((printerTotalTextChars * tds1st[i].ratio) / textRatioTotal) + 1) / 2) * 2;
                    r.push_back(static_cast<uint32_t>(chars));
                    charsTotal += chars;
                }
            }
            else{
                r.push_back(2);  /* 罫線を全角としてカウント */
                charsTotal += 2;
            }
        }
        return r;
    }();

    for(auto tr : table.trs){
        setFont(tr.font);
        for(int i = 0; i < tr.tds.size(); i++){
            auto&& td = tr.tds[i];
            zhstr_t buff(colchars[i]);
            switch(td.alignment){
                case SlipDocument::Alignment::Left:
                {
                    zhPutLeft(buff, td);
                    break;
                }
                case SlipDocument::Alignment::Right:
                {
                    zhPutRight(buff, td);
                    break;
                }
                case SlipDocument::Alignment::Center:
                {
                    zhPutCenter(buff, td);
                    break;
                }
                default:
                {
                    break;
                }
            }
            writeString(zhstrToUtf8(buff));
        }
        writeBytes({0x0A});
    }
}

void SlipPrinterESCP::setFont(SlipDocument::Font font)
{
    m_currentFont = font;
    switch(font){
        case SlipDocument::Font::Normal:
        {
            writeBytes({0x1D, 0x21, 0x00}); /* GS ! n : 文字サイズの指定 */
            break;
        }
        case SlipDocument::Font::DoubleSize_V:
        {
            writeBytes({0x1D, 0x21, 0x01}); /* GS ! n : 文字サイズの指定 */
            break;
        }
        case SlipDocument::Font::DoubleSize_H:
        {
            writeBytes({0x1D, 0x21, 0x10}); /* GS ! n : 文字サイズの指定 */
            break;
        }
        case SlipDocument::Font::DoubleSize_HV:
        {
            writeBytes({0x1D, 0x21, 0x11}); /* GS ! n : 文字サイズの指定 */
            break;
        }
        default:
        {
            LogManager_AddError("不正な文字指定");
            break;
        }
    }
}

int SlipPrinterESCP::bytesPerChar() const
{
    switch(m_currentFont){
        case SlipDocument::Font::Normal:
        {
            break;
        }
        case SlipDocument::Font::DoubleSize_V:
        {
            break;
        }
        case SlipDocument::Font::DoubleSize_H:
        {
            return 2;
        }
        case SlipDocument::Font::DoubleSize_HV:
        {
            return 2;
        }
        default:
        {
            break;
        }
    }
    return 1;
}

void SlipPrinterESCP::zhPut(zhstr_t& ioBuffer, const zhstr_t& src, int bufferStartOffset) const
{
    /*
     *  下記のコードでは、「全角１バイト目が欠損していて全角２バイト目のみが存在する状況」が発生しないように
     *  バイト列を処理することで、配列の境界チェックを省いている。
     */
    bool bFirst = true;
    const int ioBuffer_size = static_cast<int>(ioBuffer.size());
    const int src_size = static_cast<int>(src.size());
    for(int bi = bufferStartOffset, si = 0; (bi < ioBuffer_size) && (si < src_size); bi++, si++){
        if(bi < 0) continue;

        if(bFirst){
            /* 初めて ioBerrer に置こうとしたsrcの文字が倍角のNバイト目 */
            if(src[si].isMultiple() && src[si].isHalf()){
                while(src[si].isHalf()) si--;
            }
            /* 初めて ioBuffer に置こうとした場所に既に倍角文字が配置されている　*/
            if(ioBuffer[bi].isMultiple()){
                /* 前後を含めてクリアする　*/
                auto iob = &ioBuffer[bi - ioBuffer[bi].nth];
                int len = ioBuffer[bi].length;
                for(int iobn = 0; iobn < len; iobn++) iob->clear();
            }
            bFirst = false;
        }
        
        const bool bSrcFinal    = si == (src_size - 1);
        const bool bBufferFinal = bi == (ioBuffer_size - 1);

        if(bSrcFinal){
            /* 最後に ioBuffer に置こうとした場所に既に倍角文字が配置されている */
            if(ioBuffer[bi].isMultiple()){
                /* 前後を含めてクリアする　*/
                auto iob = &ioBuffer[bi - ioBuffer[bi].nth];
                int len = ioBuffer[bi].length;
                for(int iobn = 0; iobn < len; iobn++) iob->clear();
            }
        }
        if(bBufferFinal){
            /* ioBufferの最後に置こうとしている文字が　--->　*/
            if(src[si].isSingle()){ /* 半角文字の場合 */
                /* OK */
            }
            else if(src[si].isMultiple() && src[si].isLast()){  /* 倍角文字の最終バイトの場合 */
                /* OK */
            }
            else{
                /* 中途半端に配置された ioBuffer をクリアする */
                ioBuffer[bi].clear();
                bi--;
                bool bContinue = true;
                while(bContinue){
                    if(ioBuffer[bi].isFirst()) bContinue = false;
                    ioBuffer[bi].clear();
                    bi--;
                }
                break;  /* 配置終了 */
            }
        }
        ioBuffer[bi] = src[si];
    }
}

SlipPrinterESCP::zhstr_t SlipPrinterESCP::tdToZhstr(const SlipDocument::Td& src) const
{
    switch(src.type){
        case SlipDocument::Td::Type::Text:
        {
            return utf8ToZhstr(src.body);
        }
        case SlipDocument::Td::Type::WrappedText:
        {
            return doWrappedText(src);
        }
        case SlipDocument::Td::Type::Separator:
        {
            std::string s;
            for(int i = 0; i < (getRowChars() / 2); i++){
                s += "─";
            }
            return utf8ToZhstr(s);
        }
        case SlipDocument::Td::Type::Separator_T:
        {
            return utf8ToZhstr("┬");
        }
        case SlipDocument::Td::Type::Separator_rT:
        {
            return utf8ToZhstr("┴");
        }
        case SlipDocument::Td::Type::Separator_X:
        {
            return utf8ToZhstr("┼");
        }
        case SlipDocument::Td::Type::Separator_I:
        {
            return utf8ToZhstr("│");
        }
        default:
        {
            break;
        }
    }
    return zhstr_t();
}


/*
 * WrappedText は　$WRAPPED_TEXT() にて生成され、必ず下記の設定となる。
 * (1) Tr 中に Td は１個のみで設定される。
 * (2) TextAlign は必ず Left で設定される。
 */
SlipPrinterESCP::zhstr_t SlipPrinterESCP::doWrappedText(const SlipDocument::Td& td) const
{
    zhstr_t result;
    
    /* １文字ずつに分解する　*/
    const std::vector<zhstr_t> src = [&](){
        std::vector<zhstr_t> r;
        const zhstr_t src = utf8ToZhstr(td.body);
        zhstr_t ss;
        for(auto s : src){
            ss.push_back(s);
            if(s.isLast()){
                r.push_back(ss);
                ss.clear();
            }
        }
        return r;
    }();

    const auto kinsokuChars = TextUtils::utf8To32("、。");
    auto isKinsokuChar = [&](char32_t ch){
        for(auto c : kinsokuChars){
            if(c == ch) return true;
        }
        return false;
    };
    
    const int lbuff_length = getRowChars();
    zhstr_t lbuff;
    for(int si = 0; si < src.size(); si++){
        if(src[si].front().c32 == 0x0A){
            if(result.size() != 0){
                result.push_back(zhchar_t(0x0A, 1, 0));
            }
            result.insert(result.end(), lbuff.cbegin(), lbuff.cend());
            lbuff.clear();
            continue;   /* 改行文字は　lbuff.insert() を行わないので、ここで continue */
        }
        
        const char32_t nextch = ((si + 1) >= src.size()) ? 0 : src[si + 1].front().c32;
        const int putbytes = [&](){
            if(isKinsokuChar(nextch)){  /* 行頭禁止文字を含めて計算する */
                return src[si].front().length + src[si + 1].front().length;
            }
            return src[si].front().length;
        }();
        if((lbuff.size() + putbytes) > lbuff_length){
            if(result.size() != 0){
                result.push_back(zhchar_t(0x0A, 1, 0));
            }
            result.insert(result.end(), lbuff.cbegin(), lbuff.cend());
            lbuff.clear();
        }
        
        lbuff.insert(lbuff.end(), src[si].cbegin(), src[si].cend());
    }
    /* 最終行処理 */
    if(lbuff.size() > 0){
        if(result.size() != 0){
            result.push_back(zhchar_t(0x0A, 1, 0));
        }
        result.insert(result.end(), lbuff.cbegin(), lbuff.cend());
    }
    return result;   /* 空白を返却する */
}

void SlipPrinterESCP::zhPutLeft(zhstr_t& ioBuffer, const SlipDocument::Td& src) const
{
    auto&& zstr = tdToZhstr(src);
    zhPut(ioBuffer, zstr, 0);
}

void SlipPrinterESCP::zhPutRight(zhstr_t& ioBuffer, const SlipDocument::Td& src) const
{
    auto&& zstr = tdToZhstr(src);
    const int offset = static_cast<int>(ioBuffer.size()) - static_cast<int>(zstr.size());
    zhPut(ioBuffer, zstr, offset);
}

void SlipPrinterESCP::zhPutCenter(zhstr_t& ioBuffer, const SlipDocument::Td& src) const
{
    auto&& zstr = tdToZhstr(src);
    const int offset = (static_cast<int>(ioBuffer.size()) - static_cast<int>(zstr.size())) / 2;
    zhPut(ioBuffer, zstr, offset);
}

SlipPrinterESCP::zhstr_t SlipPrinterESCP::utf8ToZhstr(const std::string& src) const
{
    zhstr_t result;
    auto u32s = TextUtils::utf8To32(src);
    for(auto u32 : u32s){
        if(TextUtils::isHalfwidthChar(u32)){
            const auto&& bpc = bytesPerChar();
            result.push_back(zhchar_t(u32, bpc, 0));
            for(int i = 1; i < bpc; i++){
                result.push_back(zhchar_t(0, bpc, i));
            }
        }
        else{
            const auto&& bpc = bytesPerChar() * 2;
            result.push_back(zhchar_t(u32, bpc, 0));
            for(int i = 1; i < bpc; i++){
                result.push_back(zhchar_t(0, bpc, i));
            }
        }
    }
    return result;
}

std::string SlipPrinterESCP::zhstrToUtf8(const SlipPrinterESCP::zhstr_t& src) const
{
    TextUtils::utf32_t u32;
    for(auto zhc : src){
        if(zhc.length == 1){
            u32.push_back(zhc.c32 == 0 ? 0x20 : zhc.c32);
        }
        else if((zhc.length > 1) && (zhc.nth == 0)){
            u32.push_back(zhc.c32);
        }
    }
    return TextUtils::utf32To8(u32);
}


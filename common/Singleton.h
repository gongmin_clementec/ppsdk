//
//  Singleton.hpp
//  ppsdk
//
//  Created by clementec on 2016/03/22.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#ifndef Singleton_h
#define Singleton_h

#include <boost/noncopyable.hpp>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/thread.hpp>
#pragma GCC diagnostic pop

template <class T> class Singleton
: public boost::noncopyable
{
private:
    static T*   s_instance;
    static boost::recursive_mutex s_instance_mtx;
    
protected:
public:
    static void initialize()
    {
        boost::unique_lock<boost::recursive_mutex> lock(s_instance_mtx);
        if(s_instance != nullptr) return;
        s_instance = new T();
    }
    
    static void finalize()
    {
        boost::unique_lock<boost::recursive_mutex> lock(s_instance_mtx);
        if(s_instance == nullptr) return;
        delete s_instance;
        s_instance = nullptr;
    }
    
    static T& instance()
    {
        if(__builtin_expect(s_instance != nullptr, 1)){
            return *s_instance;
        }
        initialize();
        return *s_instance;
    }
};

template <class T> T* Singleton<T>::s_instance = nullptr;
template <class T> boost::recursive_mutex Singleton<T>::s_instance_mtx;

#endif /* Singleton_h */

//
//  CardReaderMiura.h
//  ppsdk
//
//  Created by tel on 2018/10/24.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_CardReaderMiura__)
#define __h_CardReaderMiura__

#include "CardReader.h"
#include "CardReaderUtil.h"


class SerialDevice;

class CardReaderMiura : public CardReader
{
public:
    enum class APIERROR: uint32_t
    {
         SUCCESS         = 0x00     // 成功
        ,COMMAND_ACCEPT  = 0x40     // コマンド受付(ある意味成功な気はするけど)
        ,INTERNAL_ERROR  = 0x41     // 不明なエラー発生時、MPIログで内容確認
        ,NOT_STARTED     = 0x42     // NFCモードになっていない。
        ,INVALID_TLV     = 0x43     // TLVフォーマットになっていない
        ,INVALID_LENGTH  = 0x44     // データ長さがまちがっている。
        ,INVALID_TIMEOUT = 0x45     // タイムアウト指定がまちがっています。
        ,TIMEOUT         = 0x46     // 取得待ちがタイムアウトした場合
        ,CARD_DETECTION_TIMEOUT         = 0x47    // カード検出のタイムアウト
        ,INVALID_REPEAT_INTERVAL        = 0x48    // リピート間隔の設定が不正
        ,INVALID_REPEAT_COUNT           = 0x49    // リピート回数の設定が不正
        ,INVALID_RF_CONTROL_VALUE       = 0x50    // RF control valueの設定が不正
        ,FELICA_REPEAT_IS_IN_PROGRESS   = 0x51    // 端末側ループ処理中

        ,TRANSACTION_ABORT = 0x0000100            // 既に取引が終了している(業務キャンセルなど)

        ,READ_TIMEOUT      = 0x1000001
        ,TLV_BAD_LENGTH    = 0x1000002
        ,TLV_UNKNOWN_TAG   = 0x1000003
        ,NO_DEVICE         = 0x1000004
        
        ,UNKOWN            = 0xFFFFFFFE
        ,BAD               = 0xFFFFFFFF
    };

    class Exception : public std::exception
    {
    private:
        std::string m_reason;
        APIERROR    m_apiError;
    protected:
    public:
        Exception(const char* reason, APIERROR apiError) _NOEXCEPT :
        m_reason(reason),
        m_apiError(apiError)
        {}
        virtual ~Exception() _NOEXCEPT {};
        virtual const char* what() const _NOEXCEPT { return m_reason.c_str(); }
        APIERROR    apiError() const { return m_apiError; }
    };
    
    
private:
//    struct ReceiveData
//    {
//        bytes_sp    header;           /* tlv header | Prologue */
//        bytes_sp    extLength;        /* headerのレングスのMSBが立っている時 */
//        bytes_sp    body;
//        bytes_sp    lrc;              /* bodyがAPDUの場合　*/
//        void clear()
//        {
//            header.reset();
//            extLength.reset();
//            body.reset();
//            lrc.reset();
//        }
//    };
    
    struct PrivateTLV
    {
        bytes_sp    header;
        bytes_sp    extLength;
        bytes_sp    value;
        void clear()
        {
            header.reset();
            extLength.reset();
            value.reset();
        }
    };

    enum class ERows
    {
        Bad,
        Row3,
        Row4
    };
    
    enum class EBackLight
    {
        Bad,
        On,
        Off
    };
    
    enum class ReadTimeoutHandling
    {
        None,
        Disconnect
    };

    enum class CardType
    {
        NotDicided,
        Magstripe,
        IC
    };

    enum class OfflinePINType
    {
        Plaintext,
        Encrypt
    };

    enum class ReadUnsolicited
    {
        Yes,
        No
    };

    enum class PredicateResult
    {
        Go,
        Continue,
        Reject,
    };

    enum class ContactlessState
    {
        Bad,
        Touch,
        EnterPIN,
        SeePhone
    };

    enum class UpdateKind
    {
        FirmWare,
        MasterData
    };

    boost::shared_ptr<SerialDevice>   m_ios;
    bytes_t m_tag_dfae22;
    bytes_t m_tag_9f1e;
    CardType m_cardType;
    UpdateKind m_updateKind;
    bytes_t m_RWInfo;
    boost::shared_ptr<pprx::subject<pprx::unit>>   m_abortRWcommunication = boost::make_shared<pprx::subject<pprx::unit>>();
    boost::shared_ptr<pprx::subjects::behavior<bool>>         m_lockTransactionState;
    boost::shared_ptr<pprx::subject<bytes_sp>>                m_recv;
    boost::shared_ptr<pprx::subject<pprx::unit>>              m_waitStartContactlessState;
    std::vector<std::string>                                  m_updateMasterFileList;
    bool                                                      m_isCardReadTerminate;
    json_t                                                    m_cardReaderInfo;

    void _readNBytesSync(size_t n);
    auto observableReaderPacket() -> pprx::observable<bytes_sp>;
    void _sendPrologueAndAPDU(const bytes_t& prologue, const bytes_t& header, const bytes_t& trailer);
    void _sendRawData(bytes_sp data);

    auto _displayText(ERows rows, EBackLight backLight, const std::string& str) -> pprx::observable<pprx::unit>;
    void _controlLed(bool led1, bool led2, bool led3, bool led4, bool blink);
    auto _sendAndReceiveCommonApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    auto RWcommunication
    (
     const std::string               debugId,
     boost::function<void()>         action = boost::function<void()>(),
     const std::vector<bytes_t>&     retrieveTags = {},
     ReadUnsolicited                 isUnsolicitedRead = ReadUnsolicited::Yes,
     boost::function<bool(bytes_sp)> predicate = boost::function<bool(bytes_sp)>(),
     boost::function<void(void)>     cleanup = boost::function<void(void)>()
     )  -> pprx::observable<bytes_sp>;
    auto standardPredicate(const bytes_sp& rawBuffer, const bytes_sp& readBuffer, const std::vector<bytes_t>& retrieveTags, ReadUnsolicited isUnsolicitedRead) -> PredicateResult;
    auto beginRecvTrafficController() -> pprx::observable<pprx::unit>;

    auto send(const bytes_t& prologue,const bytes_t& header, const bytes_t& trailer) -> void;
    auto offlinePinCommonTransaction(const OfflinePINType type, const_json_t paramjson) -> pprx::observable<const_json_t>;
    auto cleanUp() -> pprx::observable<pprx::unit>;
    auto update1KindInitialize() -> pprx::observable<pprx::unit>;
    auto updateInitialize() -> pprx::observable<pprx::unit>;
    auto updateWriteAll(const std::string& dirname, const json_t& infojson) -> pprx::observable<pprx::unit>;
    auto updateWrite1Kind(const std::string& dirname, const boost::shared_ptr<std::vector<std::string>>& filelist) -> pprx::observable<bool>;
    auto updateWrite1File(const std::string& dirname, const std::string& filename) -> pprx::observable<bool>;
    auto update1KindFinalize(int rebootWaitSecond, bool bSendSuccess) -> pprx::observable<bool>;
    auto updateFinalize() -> pprx::observable<pprx::unit>;
    auto beginWatchingCardReaderState() -> void;

    auto connectWithOption(int retryMS) -> pprx::observable<pprx::unit>;
    auto waitForMagOrIcCardDetect(const_json_t param) -> pprx::observable<pprx::maybe>;
    auto waitForNFCCardDetect(const_json_t param) -> pprx::observable<pprx::maybe>;
    auto waitForStartContactlessTransaction(const_json_t param, bool isWait = false) -> pprx::observable<pprx::maybe>;
    auto getRwConfigurationVersion(const std::string filename) -> pprx::observable<std::string>;
    auto createMasterFile(std::string filename, const_json_t srcData, boost::function<std::string(const_json_t)> createDataFunc) -> pprx::observable<pprx::unit>;
    auto beginCardReadCleanUp(const_json_t param, const_json_t cardInfo) -> pprx::observable<pprx::unit>;
    auto generateTransactionResultBit(const bytes_sp) -> std::string;
    auto isDebugMode() -> bool;

protected:
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;
    virtual void prepareObservablesSelf();

public:
            CardReaderMiura(const __secret& s);
    virtual ~CardReaderMiura();

    virtual auto displayText(const std::string& str) -> pprx::observable<pprx::unit>;
    virtual auto displayTextNoLf(const std::string& str) -> pprx::observable<pprx::unit>;
    virtual auto displayIdleImage() -> pprx::observable<pprx::unit>;
    virtual auto connect() -> pprx::observable<pprx::unit>;
    virtual auto disconnect() -> pprx::observable<pprx::unit>;
    virtual auto beginCardRead(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto beginICCardTransaction() -> pprx::observable<pprx::unit>;
    virtual auto endICCardTransaction() -> pprx::observable<pprx::unit>;
    virtual auto setICCardRemoved() -> pprx::observable<pprx::unit>;
    
    virtual auto checkMasterData(const_json_t data) -> pprx::observable<const_json_t>;
    virtual auto updateMasterData(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto readContactless(const_json_t param) -> pprx::observable<const_json_t>;
    
    virtual auto adjustDateTime(const boost::posix_time::ptime& dtime)-> pprx::observable<pprx::unit>;
    
    virtual auto sendApdu(const_json_t data) -> pprx::observable<const_json_t>;
    
    virtual auto verifyPlainOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto verifyEncryptedOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto verifyEncryptedOnlinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto abortTransaction() -> pprx::observable<pprx::unit>;
    virtual auto txPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto txEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    
    virtual auto getIfdSerialNo() -> pprx::observable<const_json_t>;

    virtual auto onlineProccessingResult(bool bApproved) -> pprx::observable<pprx::unit>;

    virtual auto getReaderInfo() -> pprx::observable<const_json_t>;
    virtual auto isCardInserted() -> pprx::observable<bool>;
    virtual auto updateFirmware(const boost::filesystem::path& basedir) -> pprx::observable<pprx::unit>;
    virtual auto getDisplayMsgForReader(const std::string& msgId, const bool bContactless = false) -> std::string;
    virtual void setLockTransactionState(bool blockd = false);
    virtual auto getCurrentReaderInfo()  -> const_json_t;
    virtual auto clearCardreaderDisplay() -> pprx::observable<pprx::unit>;
};

#endif /* !defnied(__h_CardReaderMiura__) */

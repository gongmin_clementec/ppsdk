//
//  ZipFileDecompressor.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_ZipFileDecompressor__)
#define __h_ZipFileDecompressor__

#include <string>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <iostream>

class ZipFileDecompressor
{
public:
    using enumerate_callback_t = boost::function<bool(const std::string&,boost::shared_ptr<std::ostream>&)>;
    
private:
    using unzFile_t = void*;
    unzFile_t       m_unzFile;
    
    
protected:
    
public:
            ZipFileDecompressor();
    virtual ~ZipFileDecompressor();
    
    bool    open(const std::string& filepath);
    void    close();
    
    bool    enumerate(enumerate_callback_t callback);
    
    bool    decompress(const std::string& pathInZipFile, const std::string& pathInLocal);
    bool    decompress(const std::string& pathInZipFile, std::ostream& os);
    bool    decompressAll(const std::string& basePathInLocal);
};

#endif /* !defined(__h_ZipFileDecompressor__) */

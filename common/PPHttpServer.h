//
//  PPHttpServer.hpp
//  ppsdk
//
//  Created by tel on 2018/08/06.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h__PPHttpServer__)
#define __h__PPHttpServer__

#include <boost/enable_shared_from_this.hpp>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic pop
#include <memory>
#include "HttpServer.h"
#include "JMupsJobStorage.h"


class PPHttpServer;
class PaymentJob;

class PPWebSocketSession : public boost::enable_shared_from_this<PPWebSocketSession>
{
public:
    using sp = boost::shared_ptr<PPWebSocketSession>;
    json_t          m_dispatch_result;
    boost::mutex    m_dispatch_sem;
    bool            m_dispath_bContinue;
    boost::recursive_mutex  m_dispath_bNoMore_mtx;
    bool                    m_dispath_bNoMore;
    
private:
    struct message_t{
        int     sessionIndex;
        int     messageIndex;
        json_t  messageJson;
        bool    bUnsolicitedMessage;
        bool    bUnsolicitedResponse;
        using sp = boost::shared_ptr<message_t>;
        
        static message_t::sp fromString(const std::string& s)
        {
            auto j = json_t::from_str(s);
            return fromJson(j);
        }

        static message_t::sp fromJson(json_t::readonly j)
        {
            return boost::shared_ptr<message_t>(new message_t{
                j.get<int>("sessionIndex"),
                j.get<int>("messageIndex"),
                j.get("messageJson"),
                j.get<bool>("bUnsolicitedMessage"),
                j.get<bool>("bUnsolicitedResponse")
                
            });
        }

        json_t toJson() const
        {
            json_t j;
            j.put("sessionIndex", sessionIndex);
            j.put("messageIndex", messageIndex);
            j.put("messageJson", messageJson);
            j.put("bUnsolicitedMessage", bUnsolicitedMessage);
            j.put("bUnsolicitedResponse", bUnsolicitedResponse);
            return j;
        }
    };
    
    boost::shared_ptr<PPHttpServer> m_http;
    boost::recursive_mutex          m_socket_mtx;
    HttpServer::WebSocket::sp       m_socket;
    pprx::subject<json_t::readonly> m_wsRead;
    pprx::subject<pprx::unit>       m_finish;
    boost::shared_ptr<PaymentJob>   m_job;
    pprx::subscription              m_subscription;
    boost::asio::deadline_timer     m_jobTimer;
    
    HttpServer::WebSocket::sp   safeSocket();
    const int               m_sessionIndex;

    boost::recursive_mutex  m_messageIndex_mtx;
    int                     m_messageIndex;
    
    boost::recursive_mutex  m_unsolicited_messageIndex_mtx;
    int                     m_unsolicited_messageIndex;
    
    boost::recursive_mutex  m_unsolicited_lastReceiveMessageIndex_mtx;
    boost::optional<int>    m_unsolicited_lastReceiveMessageIndex;

    boost::recursive_mutex      m_sendMessages_mtx;
    std::vector<message_t::sp>  m_sendMessages;
    
    void    sendToSafeSocketAsync(message_t::sp json);
    void    send(boost::optional<json_t> json);
    void    sendUnsolicitedMessage(boost::optional<json_t> json);
    auto    process(json_t::readonly j) -> pprx::observable<pprx::unit>;
    
    void    startJobTimer(int timeoutSecond);
    void    stopJobTimer();

protected:
public:
            PPWebSocketSession(boost::shared_ptr<PPHttpServer> http, int sessionIndex);
    virtual ~PPWebSocketSession();
    
    void    initialize();
    void    attach(HttpServer::WebSocket::sp, boost::optional<json_t> message);
    void    detach();
    
    void    abort(PaymentJob::AbortReason reason);
};


class PPHttpServer : public boost::enable_shared_from_this<PPHttpServer>
{
private:
    boost::shared_ptr<HttpServer>           m_server;
    
    boost::recursive_mutex                  m_session_mtx;
    std::map<int,PPWebSocketSession::sp>    m_sessionMap;
    int                                     m_sessionIndex;
    
    boost::shared_ptr<CardReader>           m_cardReader;
    PaymentJob::Storage::sp                 m_storage;

    pprx::subjects::behavior<int>           m_jobCountSubject;

    void onWebSocket(HttpServer::WebSocket::sp ws);
    static std::string getSelfIpv4Address(const std::string& ifName);
    
    
    pprx::subscription m_serverSubscription;
    void beginServerSbuscribe();
    void endServerSubscribe();
    
protected:
    
public:
    PPHttpServer();
    virtual ~PPHttpServer();
    
    void initialize();
    void begin(const std::string& documentRoot, int portNumber, bool bUseLocalhost);

    void suspend();
    void resume();
    void forceAbortJobs();
    
    void addJobCount(int add);
    
    void waitUntilServerRunning(boost::function<void(bool)> completionHandler);
    void waitUntilNoJobs(boost::function<void(bool)> completionHandler);
    void waitUntilJobWakeup(boost::function<void(bool)> completionHandler);

    auto cardReader() { return m_cardReader; }
    auto storage() { return m_storage; }

    void sessionDetachManageMap(int sessionIndex);
};



#endif /* !defined(__h__PPHttpServer__) */

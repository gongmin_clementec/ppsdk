//
//  SlipPrinter.cpp
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipPrinter.h"
#include "SlipDocument.h"
#include "Image.h"

SlipPrinter::SlipPrinter(const __secret& s) :
    LazyObject(s)
{
}

/* virtual */
SlipPrinter::~SlipPrinter()
{
}

auto SlipPrinter::openDocument()
-> pprx::observable<pprx::unit>
{
    return openDocumentSelf();
}

auto SlipPrinter::openPage()
-> pprx::observable<pprx::unit>
{
    return openPageSelf();
}

auto SlipPrinter::printPage(SlipDocument::sp slip)
-> pprx::observable<pprx::unit>
{
    return printPageSelf(slip);
}

auto SlipPrinter::closePage()
-> pprx::observable<pprx::unit>
{
    return closePageSelf();
}

auto SlipPrinter::closeDocument()
-> pprx::observable<pprx::unit>
{
    return closeDocumentSelf();
}


//
//  JobSettingService.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingService.h"
#include "BaseSystem.h"

JobSettingService::JobSettingService(const __secret& s) :
    PaymentJob(s)
{
    LogManager_Function();
}

/* virtual */
JobSettingService::~JobSettingService()
{
    LogManager_Function();
    
}

/* virtual */
auto JobSettingService::getObservableSelf() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return hpsPrintService()
    .map([=](auto resp){
        LogManager_AddInformationF("TerminalService \n%s",resp.body.str());
        m_settingServiceResp = resp.body.clone();
        return pprx::unit();
    })


    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()

    .flat_map([=](auto){
        return waitForServicePrintConfirm().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        json_t json;
        json.put("status", "success");
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobSettingService::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

auto JobSettingService::treatSlipDocumentJson()
-> pprx::observable<const_json_t>
{
    auto pinfo = m_settingServiceResp.get("normalObject.printInfo.device.value").clone();
    
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobSettingService::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobSettingService::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

auto JobSettingService::waitForServicePrintConfirm()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto values = m_settingServiceResp.get<json_t::array_t>("normalObject.printInfo.device.value.values");
    auto COMPANY_LIST = values[0].get("COMPANY_LIST");
    
    json_t param;
    param.put("COMPANY_LIST", COMPANY_LIST);
    
    return callOnDispatchWithCommandAndSubParameter("waitForServicePrintConfirm", "hps", param)
    .flat_map([=](auto){
        return pprx::observable<>::just(pprx::unit());
    });
}

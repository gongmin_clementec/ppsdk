//
//  TradeInfoBuilder.h
//  ppsdk
//
//  Created by tel on 2017/12/20.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_TradeInfoBuilder__)
#define __h_TradeInfoBuilder__

#include "BaseSystem.h"

class TradeInfoBuilder
{
public:
    enum class attribute { required, optional, readonly };
    enum class updatable { yes, no };
    
private:
protected:
    std::map<std::string, json_t> m_values;
    
    template <typename T> void addInternal(const std::string& name, attribute attr, boost::optional<T> value, const_json_t config = const_json_t())
    {
        json_t j;
        j.put("type", name);
        switch(attr){
            case attribute::required: { j.put("attribute", "required"); break; }
            case attribute::optional: { j.put("attribute", "optional"); break; }
            case attribute::readonly: { j.put("attribute", "readonly"); break; }
        }
        if(value) j.put("current", *value);
        else j.put("current", nullptr);
        if(!config.empty()) {
            j.put("config", config.clone());
        }
        m_values[name] = j;
    }
    
public:
    virtual ~TradeInfoBuilder() = default;
    
    TradeInfoBuilder asBaseClass() const
    {
        TradeInfoBuilder tib;
        tib.m_values = m_values;
        return tib;
    }
    
    void addAmount(attribute attr, boost::optional<int> value)
    {
        addInternal<int>("amount", attr, value);
    }
    
    void addTaxOtherAmount(attribute attr, boost::optional<int> value)
    {
        addInternal<int>("taxOtherAmount", attr, value);
    }

    void addSlipNo(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("slipNo", attr, value);
    }

    void addApprovalNo(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("approvalNo", attr, value);
    }

    void addProductCode(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("productCode", attr, value);
    }
    
    void addCoupon(attribute attr, boost::optional<std::string> value, const_json_t config)
    {
        addInternal<std::string>("coupon", attr, value, config);
    }
    
    void addOtherTermJudgeNo(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("otherTermJudgeNo", attr, value);
    }

    void addTradeDate(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("tradeDate", attr, value);
    }

    void addPAN(attribute attr, boost::optional<std::string> value)
    {
        /* PAN .. Primary Account Number -> カード番号 */
        addInternal<std::string>("pan", attr, value);
    }
    
    virtual void addCupSendDate(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("cupSendDate", attr, value);
    }
    
    virtual void addCupNo(attribute attr, boost::optional<std::string> value)
    {
        addInternal<std::string>("cupNo", attr, value);
    }

    json_t build(updatable updatable, std::string strOperation) const
    {
        json_t j;
        std::vector<json_t> jarr;
        for(auto it : m_values){
            jarr.push_back(it.second.clone());
        }
        j.put("values", json_t::from_array(jarr));
        j.put("operation", strOperation);
        j.put("updatable", (updatable == updatable::yes));
        return j;
    }
};

#endif /* !defined(__h_TradeInfoBuilder__) */

//
//  JobNFCMasterDataCheckJMups.h
//  ppsdk
//
//  Created by codianz on 2020/02/26.
//  Copyright © 2020年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobNFCMasterDataCheckJMups__)
#define __h_JobNFCMasterDataCheckJMups__

#include "PaymentJob.h"
#include "JMupsAccessNFCWithMasterDataTool.h"

class JobNFCMasterDataCheckJMups :
    public PaymentJob,
    public JMupsAccessNFCWithMasterDataTool
{
private:
    bool    m_bMasterDataChecked;
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;
    virtual auto mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto isNeedWaitForShowingJobResult() -> bool;

public:
            JobNFCMasterDataCheckJMups(const __secret& s);
    virtual ~JobNFCMasterDataCheckJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCMasterDataCheckJMups__) */


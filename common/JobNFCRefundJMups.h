//
//  JobNFCRefundJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobNFCRefundJMups__)
#define __h_JobNFCRefundJMups__

#include "JobNFCSalesJMups.h"

class JobNFCRefundJMups :
public JobNFCSalesJMups
{
private:
    json_t m_tradeData;
    json_t m_cardData;
    json_t m_tradeStartResponseHeader;
        
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

    auto tradeStart() -> pprx::observable<pprx::unit>;
    auto processCardJob() -> pprx::observable<pprx::unit>;
    
    auto waitForRefundInput() -> pprx::observable<pprx::unit>;

    auto waitForTouching() -> pprx::observable<pprx::unit>;
    auto onCardReadFinished() -> pprx::observable<pprx::unit>;
    auto searchTrade() -> pprx::observable<const_json_t>;
    auto requestRefund() -> pprx::observable<pprx::unit>;
    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;

    auto processSendDigiSign() -> pprx::observable<pprx::unit>;
    
    auto waitForDigiSign() -> pprx::observable<boost::optional<std::string>>;
    auto waitForStoreTradeResultData(JMupsAccess::response_t jmupsData) -> pprx::observable<pprx::unit>;
    
public:
            JobNFCRefundJMups(const __secret& s);
    virtual ~JobNFCRefundJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCRefundJMups__) */


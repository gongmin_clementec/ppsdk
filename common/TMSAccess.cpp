//
//  TMSAccess.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "TMSAccess.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "WebClient.h"

/* static */
boost::recursive_mutex   TMSAccess::s_file_mtx;


TMSAccess::TMSAccess(const __secret& s):
    LazyObject(s)
{
    LogManager_Function();
}

/* virtual */
TMSAccess::~TMSAccess()
{
    LogManager_Function();
}

auto TMSAccess::initializeAndLoginAndUpdate() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](pprx::unit){
        auto&& url = PPConfig::instance().get<std::string>("tms.url");
        auto&& si = BaseSystem::instance().getSystemInformation();
        auto&& user = si.get<std::string>("UserName");
        auto&& pass = BaseSystem::instance().getSecureElementText("TmsPassword");
        initialize(url);
        if(!pass){
            return registerUser(user)
            .flat_map([=](const_json_t resp){
                auto&& password = resp.get<std::string>("Password");
                BaseSystem::instance().setSecureElementText("TmsPassword", password);
                return login(user, password);
            }).as_dynamic();
        }
        else {
            return login(user, *pass).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](const_json_t){
        json_t j = BaseSystem::instance().getSystemInformation().clone();
        
        json_t cr = PPConfig::instance().getChild("volatile.connectedCardReaderInformation");

        auto _merge = [](json_t& d, json_t s, const std::string& key){
            auto v = s.get_optional<std::string>(key);
            if(v) d.put(key, *v);
        };
        
        _merge(j, cr, "RWName");
        _merge(j, cr, "RWSerial");
        _merge(j, cr, "RWFirmwareVersion");

        j.put("PrinterName"         , "start sm");
        j.put("PrinterSerial"       , "n/a");
        LogManager_AddDebug(j.str(true));
        return update(j);
    }).as_dynamic();
}

void TMSAccess::initialize(const std::string& serverBaseUrl)
{
    m_serverBaseUrl = serverBaseUrl;
}

auto TMSAccess::execute(const std::string& path, const_json_t json) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto wc = AbstructFactory::instance().createWebClient();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        wc->initialize();

        auto&& js = json.str();
        auto postData = boost::make_shared<bytes_t>(std::begin(js), std::end(js));

        auto cookieData = const_json_t();

        json_t extraHeader;
        extraHeader.put("Content-Type", "application/json");
        if((!m_token.empty()) && (!m_tokenType.empty())){
            extraHeader.put("Authorization", m_tokenType + " " +m_token);
        }

        return wc->postAsync(m_serverBaseUrl + path, postData, cookieData, extraHeader);
    }).as_dynamic()
    .flat_map([=](WebClient::response_t resp){
        wc->explicitCapture();
        std::stringstream ss;
        ss << std::string(resp.body->begin(), resp.body->end());
        LogManager_AddDebugF("receive body : %s", ss.str());
        LogManager_AddDebugF("receive cookie : %s", resp.cookies.str());
        json_t responseJson;
        responseJson.deserialize(ss);
        return pprx::just(responseJson.as_readonly());
    }).as_dynamic();
}

auto TMSAccess::registerUser(const std::string& userName) -> pprx::observable<const_json_t>
{
    json_t json;
    json.put("UserName", userName);
    
    return execute("api/app/register", json);
}

auto TMSAccess::login(const std::string& userName, const std::string& password) -> pprx::observable<const_json_t>
{
    json_t json;
    json.put("UserName", userName);
    json.put("Password", password);
    LogManager_AddDebug(json.str());

    return execute("api/app/login", json)
    .map([=](const_json_t resp){
        auto access_token   = resp.get_optional<std::string>("access_token");
        auto token_type     = resp.get_optional<std::string>("token_type");
        m_token     = access_token  ? *access_token : "";
        m_tokenType = token_type    ? *token_type   : "";
        return resp;
    }).as_dynamic();
}

auto TMSAccess::update(const_json_t json) -> pprx::observable<const_json_t>
{
    return execute("api/app/update", json);
}

auto TMSAccess::logBegin() -> pprx::observable<const_json_t>
{
    return execute("api/app/logbegin", const_json_t());
}

auto TMSAccess::logEnd(const std::string& LogId) -> pprx::observable<const_json_t>
{
    json_t j;
    j.put("LogId", LogId);
    return execute("api/app/logend", j);
}

auto TMSAccess::blobDownload(const std::string& url, const boost::filesystem::path& path) ->  pprx::observable<pprx::unit>
{
    auto wc = AbstructFactory::instance().createWebClient();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        wc->initialize();
        return wc->downloadFileWithGet(url, path);
    }).as_dynamic();
}


auto TMSAccess::blobUpload(const std::string& url, const boost::filesystem::path& path) ->  pprx::observable<pprx::unit>
{
    auto wc = AbstructFactory::instance().createWebClient();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        wc->initialize();
        return wc->putFile(url, path);
    }).as_dynamic();
}

/* static */
void TMSAccess::setConnectedCardReaderInfo(const_json_t info)
{
    LogManager_Function();
    
    boost::unique_lock<boost::recursive_mutex> lock(s_file_mtx);
    
    auto path = BaseSystem::instance().getDocumentDirectoryPath() / "cardReader.json";
    std::ofstream os(path.string());
    info.serialize(os, false);
}

/* static */
json_t TMSAccess::getConnectedCardReaderSerial()
{
    LogManager_Function();
    boost::unique_lock<boost::recursive_mutex> lock(s_file_mtx);
    
    json_t j;
    auto path = BaseSystem::instance().getDocumentDirectoryPath() / "cardReader.json";
    if(boost::filesystem::exists(path)){
        json_t existRWInfo;
        std::ifstream is(path.string());
        existRWInfo.deserialize(is);
        
        auto rwSerial = existRWInfo.get_optional<std::string>("RWSerial");
        j.put("RWSerial", rwSerial.get());
    }
    
    return j;
}

/* static */
json_t  TMSAccess::getMasterDataCheckInfo()
{
    LogManager_Function();
    boost::unique_lock<boost::recursive_mutex> lock(s_file_mtx);
    
    json_t checkInfo;
    auto path = BaseSystem::instance().getDocumentDirectoryPath() / "masterDataCheck.json";
    if(boost::filesystem::exists(path)){
        std::ifstream is(path.string());
        checkInfo.deserialize(is);
    }
    
    return checkInfo;
}

/* static */
void TMSAccess::setMasterDataCheckInfo(const_json_t checkInfo)
{
    LogManager_Function();
    LogManager_AddInformationF("masterData check :\n%s", checkInfo.str());
    
    boost::unique_lock<boost::recursive_mutex> lock(s_file_mtx);
    
    json_t j;
    auto rwSerial = checkInfo.get_optional<std::string>("RWSerial");
    if (rwSerial) {
        j.put("RWSerial", rwSerial.get());
    }
    else{
        j.put("RWSerial", "");
    }
    
    auto bNeedUpdate = checkInfo.get_optional<bool>("bNeedUpdate");
    if(bNeedUpdate){
        j.put("bNeedUpdate", bNeedUpdate.get());
    }

    auto path = BaseSystem::instance().getDocumentDirectoryPath() / "masterDataCheck.json";
    std::ofstream os(path.string());
    j.serialize(os, false);
}

//
//  WebClient.h
//  ppsdk
//
//  Created by Clementec on 2017/05/25.
//
//

#if !defined(__h_WebClient__)
#define __h_WebClient__

#include <string>
#include "LazyObject.h"
#include "PPReactive.h"

class WebClient :
    public LazyObject
{
public:
    using sp = boost::shared_ptr<WebClient>;
    
    struct response_t
    {
        const_json_t    headers;
        const_json_t    cookies;
        const_bytes_sp  body;
    };

private:
    
protected:

public:
    WebClient(const __secret& s) : LazyObject(s) {}
    virtual ~WebClient() = default;
    
    virtual void initialize() = 0;
    virtual void finalize() = 0;
    
    virtual auto postAsync
    (
     const std::string& url,
     const_bytes_sp     postData,
     const_json_t       cookieData,
     const_json_t       extraHeaderData = const_json_t()
     ) -> pprx::observable<response_t> = 0;

    
    virtual auto getAsync
    (
     const std::string& url,
     const_json_t      cookieData
     )  -> pprx::observable<response_t> = 0;
    
    
    virtual auto downloadFileWithGet
    (
     const std::string&             url,
     const boost::filesystem::path  file
     ) -> pprx::observable<pprx::unit> = 0;

    virtual auto downloadFileWithPost
    (
     const std::string&             url,
     const_bytes_sp                 postData,
     const boost::filesystem::path  file,
     const_json_t                   extraHeaderData = const_json_t()
     ) -> pprx::observable<pprx::unit> = 0;

    virtual auto putFile
    (
     const std::string&             url,
     const boost::filesystem::path  file
     ) -> pprx::observable<pprx::unit> = 0;
};


#endif /* !defined(__h_WebClient__) */

//
//  PPReactive.cpp
//  ppsdk
//
//  Created by tel on 2017/11/16.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "PPReactive.h"

std::string pprx::ppexception::where() const noexcept
{
    return (boost::format("%s(%d):%s") % m_sourceFile % m_sourceLine % m_function).str();
}

void pprx::ppexception::setJson(const_json_t json)
{
    m_json = json.clone();
    m_json_text = json.str();
    LogManager_AddErrorF("error at %s -> %s", where() % what());
}

pprx::ppex_aborted::ppex_aborted(const std::string& reason, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", reason);
    setJson(json);
}

pprx::ppex_network::ppex_network(const_json_t detail, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "network");
    json.put("result.detail", detail.clone());
    setJson(json);
}

pprx::ppex_paymentserver::ppex_paymentserver(const_json_t detail, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "hps");
    json.put("result.detail", detail.clone());
    setJson(json);
}

pprx::ppex_cardrw::ppex_cardrw(const_json_t detail, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "r/w");
    json.put("result.detail", detail.clone());
    setJson(json);
}

pprx::ppex_printer::ppex_printer(const_json_t detail, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "printer");
    json.put("result.detail", detail.clone());
    setJson(json);
}

pprx::ppex_internal::ppex_internal(const std::string& description, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "internal");
    json.put("result.detail.description", description);
    setJson(json);
}

pprx::ppex_pinrelated::ppex_pinrelated(const_json_t detail, const char* sf, int sl, const char* f)
: pprx::ppexception(sf, sl, f)
{
    json_t json;
    json.put("status", "error");
    json.put("result.where", "pin");
    json.put("result.detail", detail.clone());
    setJson(json);
}

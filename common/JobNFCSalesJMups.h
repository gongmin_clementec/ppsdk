//
//  JobNFCSalesJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobNFCSalesJMups__)
#define __h_JobNFCSalesJMups__

#include "PaymentJob.h"
#include "JMupsAccessNFCWithMasterDataTool.h"
#include "JMupsAccessCoupon.h"
#include "JMupsSlipPrinter.h"

class JobNFCSalesJMups :
    public PaymentJob,
    public JMupsAccessNFCWithMasterDataTool,
    public JMupsAccessCoupon,
    public JMupsSlipPrinter
{
private:
    json_t m_tradeData;
    json_t m_couponData;
    json_t m_tradeConfirmFinished; // 取引確認処理終了《通知》json
    auto analyseReadResult(const_json_t resultData) -> pprx::observable<pprx::unit>;
protected:
    bool   m_onlineProcessing;
protected:
    json_t m_cardData;
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;
    virtual auto initializeChildJob() -> pprx::observable<pprx::unit>;
    virtual auto mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto getChildSlipDocument() -> pprx::observable<const_json_t>;
    virtual auto execChildJobDigiSign() -> pprx::observable<pprx::unit>;
    virtual auto execChildJobSendTagging() -> pprx::observable<JMupsAccess::response_t>;
    virtual auto processMasterData() -> pprx::observable<pprx::unit>;
    virtual auto processIsNeedUpdateMasterData() -> pprx::observable<const_json_t>;
    
    auto tradeStart() -> pprx::observable<pprx::unit>;
    auto processCardJob() -> pprx::observable<pprx::unit>;

    auto waitForAmountInput() -> pprx::observable<pprx::unit>;

    auto waitForTouching() -> pprx::observable<pprx::unit>;
    auto waitForCardActive() -> pprx::observable<pprx::unit>;
    auto onCardReadFinished() -> pprx::observable<pprx::unit>;
    auto sendCardResultData() -> pprx::observable<pprx::unit>;

    auto waitForTradeInfo(const std::string& strTradeOp="") -> pprx::observable<pprx::unit>;
    auto sendTradeConfirm(prmNFCSales_Confirm::selector selector)  -> pprx::observable<JMupsAccess::response_t>;
    auto waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData) -> pprx::observable<pprx::unit>;
    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;
    
    auto processSendDigiSign() -> pprx::observable<pprx::unit>;
    auto waitForDigiSign() -> pprx::observable<boost::optional<std::string>>;
    virtual auto sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)  -> pprx::observable<pprx::unit>;
    virtual auto addMediaDivToCardNo(const_json_t tradeData) -> json_t;
    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    
protected:
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;

public:
            JobNFCSalesJMups(const __secret& s);
    virtual ~JobNFCSalesJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCSalesJMups__) */


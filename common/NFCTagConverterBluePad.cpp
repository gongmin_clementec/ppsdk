//
//  NFCTagConverterBluePad.cpp
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "NFCTagConverterBluePad.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/crc.hpp>
#include "LogManager.h"
#include <sstream>


NFCTagConverterBluePad::NFCTagConverterBluePad()
{
    
}


/* virtual */
NFCTagConverterBluePad::~NFCTagConverterBluePad()
{
    
}

void NFCTagConverterBluePad::loadJMupsData(const_json_t jmups)
{
#if defined(DEBUG)
//    LogManager_AddDebug("**** use sanei fixed data ****");
//    m_masterData = saneiFixedData();
//    LogManager_AddDebug(m_masterData.str());
//    return;
#endif
    std::string hash;
    {
        std::stringstream ss;
        jmups.get("normalObject.masterData").serialize(ss, false);
        auto&& jstr = ss.str();
        boost::crc_32_type crc32;
        crc32.process_bytes(jstr.data(), jstr.size());
        hash = (boost::format("%08X") % crc32.checksum()).str();
    }
    m_masterData.clear();
    m_masterData.put("capk", jmupsBuildCAPublicKey(jmups, hash));
    m_masterData.put("config", jmupsBuildConfigure(jmups, hash));
    LogManager_AddDebug(m_masterData.str());
}


json_t NFCTagConverterBluePad::jmupsBuildCAPublicKey(const_json_t jmups, const std::string& hash) const
{
    json_t certs;

    auto cakeyFile = jmups.get("normalObject.masterData.cakeyFile");
    
    if(!cakeyFile.is_null()){
        json_t::array_t e2s;
        
        for (auto src : cakeyFile.get<json_t::array_t>()){
            json_t dst;
            translator _(dst, src);
            
            _.tr("DFC316"   , "1");
            _.tr("9F22"     , "2");
            _.tr("DFC319"   , "3");
            _.tr("DFC31A"   , "8");
            
            /* エクスポーネントは、DTLIBに渡す際に前半の０を削除 */
            _.tr("DFC318"   , "7", translator::trimLeft00);
            
            /* モジュラスはレングス分だけ送信 */
            auto modlen = src.get_optional<std::string>("5");
            if(modlen){
                auto slen = translator::asciiHexTextToAsciiText(modlen.get());
                auto len = boost::lexical_cast<int>(slen);
                auto mod = src.get<std::string>("6");
                dst.put("DFC317", mod.substr(0, len * 2));
            }
            e2s.push_back(dst);
        }
        certs.put("E2", e2s);
    }
    {
        json_t e4;
        e4.put("C1", hash); /* version は hash とする */
        certs.put("E4", e4);
    }

    return certs;
}

json_t NFCTagConverterBluePad::jmupsBuildConfigure(const_json_t jmups, const std::string& hash) const
{
    json_t config;
    
    /*********** E0 ***********/
    {
        auto src = jmups.get("normalObject.masterData.kernelCommonFile");
        json_t e0;
        translator _(e0, src);
        
        _.tr("9F1A", "1", boost::bind(&translator::fetchTlv, _1, "9F1A"));
        
        e0.put("9F1D", "2CB8000000000000");
        config.put("E0", e0);
    }
    
    /*********** E1 ***********/
    {
        json_t::array_t e1s;
        auto&& master   = jmupsBuildMaster(jmups);
        auto&& visa     = jmupsBuildVisa(jmups);
        auto&& amex     = jmupsBuildAmex(jmups);

        if(!master.empty()) e1s.push_back(master);
        if(!visa.empty()) e1s.push_back(visa);
        if(!amex.empty()) e1s.push_back(amex);
        
        if(!e1s.empty()) config.put("E1", e1s);
    }
    
    /*********** E4 ***********/
    {
        json_t e4;
        e4.put("C1", hash); /* version は hash とする */
        config.put("E4", e4);
    }
    
    /*********** E5 ***********/
    {
        json_t e5;
        e5.put("C1", "9FDF124F50DF810CDF81295F24");
        e5.put("C2", "9FDF124F50DF810CDF81295F24");
        config.put("E5", e5);
    }
    
    return config;
}

json_t NFCTagConverterBluePad::jmupsBuildMaster(const_json_t jmups) const
{
    json_t dst;
    
    auto src = jmups.get_optional("normalObject.masterData.kernel2File");
    
    if(!src){
        LogManager_AddWarning("empty");
        return dst;
    }
    
    if(src->is_null()){
        LogManager_AddWarning("null");
        return dst;
    }
    
    {
        translator _(dst, *src);
        
        _.tr("9F01"     ,  "1", boost::bind(&translator::fetchTlv, _1, "9F01"));
        _.tr("DF812D"   ,  "3", boost::bind(&translator::fetchTlv, _1, "DF812D"));
        _.tr("DF811C"   ,  "4", boost::bind(&translator::fetchTlv, _1, "DF811C"));
        _.tr("DF811D"   ,  "5", boost::bind(&translator::fetchTlv, _1, "DF811D"));
        _.tr("5F2A"     ,  "6", boost::bind(&translator::currencyCode, _1, "5F2A"));
        _.tr("5F36"     ,  "7", boost::bind(&translator::fetchTlv, _1, "5F36"));
        _.tr("9F15"     ,  "8", boost::bind(&translator::fetchTlv, _1, "9F15"));
        _.tr("9F7C"     , "10", boost::bind(&translator::fetchTlv, _1, "9F7C"));
        _.tr("DF811A"   , "11", boost::bind(&translator::fetchTlv, _1, "DF811A"));
    }
    
    dst.put("DF8120"    , "FC50808800");
    dst.put("DF8121"    , "0000000000");    /* refund -> "FFFFFFFFFF" */
    dst.put("DF8122"    , "FC50808800");
    
    {
        auto aidinfos = src->get<json_t::array_t>("AIDINFO");
        translator _(dst, aidinfos[0]);
        
        _.tr("9F06"     , "100", boost::bind(&translator::fetchLv, _1, 4));
        _.tr("DF810C"   , "101", boost::bind(&translator::subString, _1, 4, 2));
        _.tr("9F1B"     , "105", boost::bind(&translator::bcdTextToHexText, _1, 8));
        _.tr("DF8124"   , "106");
        _.tr("DF8125"   , "107");
        _.tr("DF8123"   , "108");
        _.tr("DF8126"   , "109");
        
        dst.put("9F33"  , "E02808");
        dst.put("9F40"  , "7000B0A000");
        
        _.tr("9F35"     , "112", boost::bind(&translator::fetchTlv, _1, "9F35"));
        _.tr("9F09"     , "113", boost::bind(&translator::fetchTlv, _1, "9F09"));
        
        _.tr("DF8118"   , "114", boost::bind(&translator::fetchTlv, _1, "DF8118"));
        _.tr("DF8119"   , "115", boost::bind(&translator::fetchTlv, _1, "DF8119"));
        _.tr("DF811E"   , "116", boost::bind(&translator::fetchTlv, _1, "DF811E"));
        _.tr("DF812C"   , "117", boost::bind(&translator::fetchTlv, _1, "DF812C"));
        _.tr("DF811B"   , "118", boost::bind(&translator::fetchTlv, _1, "DF811B"));
        
        dst.put("DF811F", "08");  /* 9F33の3バイト目 */
        dst.put("9F7E"  , "01");
        
    }
    return dst;
}

json_t NFCTagConverterBluePad::jmupsBuildVisa(const_json_t jmups) const
{
    json_t dst;

    auto src = jmups.get_optional("normalObject.masterData.kernel3File");

    if(!src){
        LogManager_AddWarning("empty");
        return dst;
    }

    if(src->is_null()){
        LogManager_AddWarning("null");
        return dst;
    }

    {
        translator _(dst, *src);
        
        _.tr("9F01"     ,  "1", boost::bind(&translator::fetchTlv,     _1, "9F01"));
        _.tr("5F2A"     ,  "4", boost::bind(&translator::currencyCode, _1, "5F2A"));
        _.tr("5F36"     ,  "5", boost::bind(&translator::fetchTlv,     _1, "5F36"));
    }
    
    {
        auto aidinfos = src->get<json_t::array_t>("AIDINFO");
        {
            translator _(dst, aidinfos[0]);
            
            _.tr("9F06"     , "100", boost::bind(&translator::fetchLv,   _1, 4));
            _.tr("DF810C"   , "101", boost::bind(&translator::subString, _1, 4, 2));
            _.tr("9F66"     , "104", boost::bind(&translator::fetchTlv,  _1, "9F66"));
            _.tr("9F1B"     , "105");
            _.tr("9F35"     , "106", boost::bind(&translator::fetchTlv,  _1, "9F35"));
            
            _.tr("DFC30C"   , "109", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC30D"   , "110", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC30E"   , "111", boost::bind(&translator::bcdTextToHexText, _1, 8));
            
            dst.put("DFC309", "01");
            dst.put("DFC315", "02");
            dst.put("DFC30A", "01");
        }
        
        json_t::array_t e7s;
        {
            json_t e7;
            translator _(e7, aidinfos[0]);
            
            _.tr("9F5A"     , "112", boost::bind(&translator::asciiHexTextToAsciiText,
                             boost::bind(&translator::fetchTlv, _1, "9F5A")
                            ));
            _.tr("DFC304"   , "114", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC305"   , "115", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC306"   , "116", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.visa_checkFlag_to_DFC303_DFC308_DFC307("113");
            
            e7s.push_back(e7);
        }
        
        {
            json_t e7;
            translator _(e7, aidinfos[0]);
            
            _.tr("9F5A"     , "117", boost::bind(&translator::asciiHexTextToAsciiText,
                                                 boost::bind(&translator::fetchTlv, _1, "9F5A")
                                                 ));
            _.tr("DFC304"   , "119", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC305"   , "120", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC306"   , "121", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.visa_checkFlag_to_DFC303_DFC308_DFC307("118");
            
            e7s.push_back(e7);
        }
        
        {
            json_t e7;
            translator _(e7, aidinfos[0]);
            
            _.tr("9F5A"     , "122", boost::bind(&translator::asciiHexTextToAsciiText,
                                                 boost::bind(&translator::fetchTlv, _1, "9F5A")
                                                 ));
            _.tr("DFC304"   , "124", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC305"   , "125", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC306"   , "126", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.visa_checkFlag_to_DFC303_DFC308_DFC307("123");
            
            e7s.push_back(e7);
        }
        
        {
            json_t e7;
            translator _(e7, aidinfos[0]);
            
            _.tr("9F5A"     , "127", boost::bind(&translator::asciiHexTextToAsciiText,
                                                 boost::bind(&translator::fetchTlv, _1, "9F5A")
                                                 ));
            _.tr("DFC304"   , "129", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC305"   , "130", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.tr("DFC306"   , "131", boost::bind(&translator::bcdTextToHexText, _1, 8));
            _.visa_checkFlag_to_DFC303_DFC308_DFC307("128");
            
            e7s.push_back(e7);
        }
        dst.put("E7", e7s);
    }
    
    return dst;
}

json_t NFCTagConverterBluePad::jmupsBuildAmex(const_json_t jmups) const
{
    json_t dst;

    auto src = jmups.get_optional("normalObject.masterData.kernel4File");

    if(!src){
        LogManager_AddWarning("empty");
        return dst;
    }
    
    if(src->is_null()){
        LogManager_AddWarning("null");
        return dst;
    }

    {
        translator _(dst, *src);
        
        _.tr("5F2A"     ,  "1", boost::bind(&translator::currencyCode, _1, "5F2A"));
        _.tr("5F36"     ,  "2", boost::bind(&translator::fetchTlv,     _1, "5F36"));
        _.tr("9F15"     ,  "3", boost::bind(&translator::fetchTlv,     _1, "9F15"));
    }
    
    {
        auto aidinfos = src->get<json_t::array_t>("AIDINFO");
        translator _(dst, aidinfos[0]);
        
        _.tr("9F06"     , "100", boost::bind(&translator::fetchLv,   _1, 4));
        _.tr("DF810C"   , "101", boost::bind(&translator::subString, _1, 4, 2));
        _.tr("9F1B"     , "105");
        
        _.tr("DFC30C"   , "106", boost::bind(&translator::bcdTextToHexText, _1, 8));
        _.tr("DFC30D"   , "107", boost::bind(&translator::bcdTextToHexText, _1, 8));
        _.tr("DFC30E"   , "108", boost::bind(&translator::bcdTextToHexText, _1, 8));
        
        _.tr("9F09"     , "109", boost::bind(&translator::fetchTlv, _1, "9F09"));
        _.tr("9F16"     , "110", boost::bind(&translator::fetchTlv, _1, "9F16"));
        _.tr("9F35"     , "112", boost::bind(&translator::fetchTlv, _1, "9F35"));
        _.tr("9F6D"     , "115", boost::bind(&translator::fetchTlv, _1, "9F6D"));
        _.tr("9F6E"     , "116", boost::bind(&translator::fetchTlv, _1, "9F6E"));
        _.tr("DFFFDF4A" , "118");
        
        dst.put("9F33"  , "E02888");
        dst.put("9F40"  , "700B0A000");
        
        dst.put("DF8120", "DC50840000");
        dst.put("DF8121", "0000000000");
        dst.put("DF8122", "C400000000");
    }
    
    return dst;
}



/* ASCIIのHEXテキストをASCII文字列に変換 */
/* static */
std::string NFCTagConverterBluePad::translator::asciiHexTextToAsciiText(const std::string& src)
{
    std::stringstream ss;
    char ch = 0;
    for(auto i = 0; i < src.length(); i++){
        switch(src[i]){
            case '0': { ch |= 0; break; }
            case '1': { ch |= 1; break; }
            case '2': { ch |= 2; break; }
            case '3': { ch |= 3; break; }
            case '4': { ch |= 4; break; }
            case '5': { ch |= 5; break; }
            case '6': { ch |= 6; break; }
            case '7': { ch |= 7; break; }
            case '8': { ch |= 8; break; }
            case '9': { ch |= 9; break; }
            case 'A': case 'a': { ch |= 10; break; }
            case 'B': case 'b': { ch |= 11; break; }
            case 'C': case 'c': { ch |= 12; break; }
            case 'D': case 'd': { ch |= 13; break; }
            case 'E': case 'e': { ch |= 14; break; }
            case 'F': case 'f': { ch |= 15; break; }
        }
        if((i & 1) == 1){
            ss.put(ch);
            ch = 0;
        }
        else{
            ch <<= 4;
        }
    }
    return ss.str();
}
                                 
/* static */
std::string NFCTagConverterBluePad::translator::trimLeft00(const std::string& src)
{
    std::string s = src;
    while(s.size()>2){
        if((s[0] == '0') && (s[1] == '0')){
            s = s.substr(2, s.size() - 2);
        }
        else{
            break;
        }
    }
    return s;
}


/* static */
std::string NFCTagConverterBluePad::translator::fetchTlv(const std::string& src, const std::string& tag)
{
    auto _src = boost::to_upper_copy<std::string>(src);
    auto _tag = boost::to_upper_copy<std::string>(tag);
    
    std::string result;
    do{
        if(_src.find(_tag) != 0) break;
        auto slen = _src.substr(_tag.length(), 2);
        auto len = std::stoul(slen, nullptr, 16);
        result = _src.substr(_tag.length() + 2, len * 2);
    } while(false);
    return result;
}


/* TLV且つ、HEXテキストで入ってくるので、10進テキストに変換　*/
/* static */
std::string NFCTagConverterBluePad::translator::currencyCode(const std::string& src, const std::string& tag)
{
    auto _src = boost::to_upper_copy<std::string>(src);
    auto _tag = boost::to_upper_copy<std::string>(tag);
    
    std::string result;
    do{
        if(_src.find(_tag) != 0) break;
        auto slen = _src.substr(_tag.length(), 2);
        auto len = std::stoul(slen, nullptr, 16);
        auto hexText = _src.substr(_tag.length() + 2, len * 2);
        auto value = std::stoul(hexText, nullptr, 16);
        result = (boost::format("%d") % value).str();
        if((result.size() % 2) != 0){
            result = "0" + result;
        }
    } while(false);
    return result;
}

/* static */
std::string NFCTagConverterBluePad::translator::fetchLv(const std::string& src, int lenchars)
{
    auto slen = asciiHexTextToAsciiText(src.substr(0, lenchars));
    auto len = boost::lexical_cast<int>(slen);
    return src.substr(lenchars, len * 2);
}

/* static */
std::string NFCTagConverterBluePad::translator::subString(const std::string& src, int start, int count)
{
    return src.substr(start, count);
}

/* static */
std::string NFCTagConverterBluePad::translator::bcdTextToHexText(const std::string& src, int outputLength)
{
    uint64_t r = 0;
    for(auto i = 0; i < src.length(); i++){
        r *= 10;
        switch(src[i]){
            case '0': { r += 0; break; }
            case '1': { r += 1; break; }
            case '2': { r += 2; break; }
            case '3': { r += 3; break; }
            case '4': { r += 4; break; }
            case '5': { r += 5; break; }
            case '6': { r += 6; break; }
            case '7': { r += 7; break; }
            case '8': { r += 8; break; }
            case '9': { r += 9; break; }
        }
    }
    
    std::stringstream ss;
    ss << std::setfill('0') << std::setw(outputLength) << std::hex << std::uppercase << r;
    
    return ss.str();
}

void NFCTagConverterBluePad::translator::visa_checkFlag_to_DFC303_DFC308_DFC307(const std::string& tag)
{
    auto ovalue = m_src.get_optional<std::string>(tag);
    if(!ovalue) return;
    auto value = ovalue.get();
    
    const auto checkflag = std::stoul(value, nullptr, 16);
    
    constexpr uint8_t bit_1 = 0x01;
    constexpr uint8_t bit_2 = 0x02;
    constexpr uint8_t bit_3 = 0x04;
    constexpr uint8_t bit_4 = 0x08;
    constexpr uint8_t bit_5 = 0x10;
    constexpr uint8_t bit_6 = 0x20;
    constexpr uint8_t bit_7 = 0x40;
    constexpr uint8_t bit_8 = 0x80;
    
    const uint8_t DFC303 =
        (((checkflag & bit_8) != 0) ? 0x01 : 0x00);
    
    const uint8_t DFC308 =
        (((checkflag & bit_8) != 0) ? bit_1 : 0x00) |
        (((checkflag & bit_6) != 0) ? bit_2 : 0x00) |
        (((checkflag & bit_4) != 0) ? bit_3 : 0x00) |
        (((checkflag & bit_5) != 0) ? bit_4 : 0x00) |
        (((checkflag & bit_7) != 0) ? bit_5 : 0x00);
    
    const uint8_t DFC307 =
    (((checkflag & bit_7) != 0) ? 0x01 : 0x00);
    
    m_dst.put("DFC303", (boost::format("%02X") % static_cast<int>(DFC303)).str());
    m_dst.put("DFC308", (boost::format("%02X") % static_cast<int>(DFC308)).str());
    m_dst.put("DFC307", (boost::format("%02X") % static_cast<int>(DFC307)).str());
}

bytes_sp NFCTagConverterBluePad::getCAPublicKeyBytes() const
{
    return buildBytesAt("capk");
}

bytes_sp NFCTagConverterBluePad::getConfigureBytes() const
{
    return buildBytesAt("config");
}

bytes_sp NFCTagConverterBluePad::buildBytesAt(const std::string& key) const
{
    auto json = m_masterData.get(key);
    return buildBytes(json);
}

bytes_sp NFCTagConverterBluePad::buildBytes(const_json_t json) const
{
    auto result = boost::make_shared<bytes_t>();
    if(json.type() != typeid(json_t::map_t)){
        return result; /* error */
    }
    
    auto map = json.get<json_t::map_t>();
    for(auto it : map){
        auto key    = it.first;
        auto value  = it.second;
        auto keyBytes = hexTextToBin(key);
        
        if(value.type() == typeid(json_t::array_t)){
            for(auto arrit : value.get<json_t::array_t>()){
                auto arritBytes = buildBytes(arrit);
                auto lenBytes   = lengthBytes(arritBytes);
                result->insert(result->end(), keyBytes->begin(), keyBytes->end());
                result->insert(result->end(), lenBytes->begin(), lenBytes->end());
                result->insert(result->end(), arritBytes->begin(), arritBytes->end());
            }
        }
        else if(value.type() == typeid(json_t::map_t)){
            auto mapBytes = buildBytes(value);
            auto lenBytes   = lengthBytes(mapBytes);
            result->insert(result->end(), keyBytes->begin(), keyBytes->end());
            result->insert(result->end(), lenBytes->begin(), lenBytes->end());
            result->insert(result->end(), mapBytes->begin(), mapBytes->end());
        }
        else if(value.type() == typeid(json_t::string_t)){
            auto valueBytes = hexTextToBin(value.get<json_t::string_t>());
            auto lenBytes   = lengthBytes(valueBytes);
            result->insert(result->end(), keyBytes->begin(), keyBytes->end());
            result->insert(result->end(), lenBytes->begin(), lenBytes->end());
            result->insert(result->end(), valueBytes->begin(), valueBytes->end());
        }
    }
    return result;
}

bytes_sp NFCTagConverterBluePad::lengthBytes(bytes_sp data) const
{
    auto result = boost::make_shared<bytes_t>();

    const auto vlen = data->size();
    if(vlen <= 127){
        result->push_back(static_cast<uint8_t>(vlen));
    }
    else{
        result->push_back(0x80 + 2);    /* 0x80 + レングスのバイト長（２） */
        result->push_back(static_cast<uint8_t>((vlen >> 8) & 0xFF));
        result->push_back(static_cast<uint8_t>((vlen     ) & 0xFF));
    }
    return result;
}


bytes_sp NFCTagConverterBluePad::hexTextToBin(const std::string& text) const
{
    auto result = boost::make_shared<bytes_t>();
    
    uint8_t ch = 0;
    for(auto i = 0; i < text.length(); i++){
        switch(text[i]){
            case '0': { ch |= 0; break; }
            case '1': { ch |= 1; break; }
            case '2': { ch |= 2; break; }
            case '3': { ch |= 3; break; }
            case '4': { ch |= 4; break; }
            case '5': { ch |= 5; break; }
            case '6': { ch |= 6; break; }
            case '7': { ch |= 7; break; }
            case '8': { ch |= 8; break; }
            case '9': { ch |= 9; break; }
            case 'A': case 'a': { ch |= 10; break; }
            case 'B': case 'b': { ch |= 11; break; }
            case 'C': case 'c': { ch |= 12; break; }
            case 'D': case 'd': { ch |= 13; break; }
            case 'E': case 'e': { ch |= 14; break; }
            case 'F': case 'f': { ch |= 15; break; }
        }
        if((i & 1) == 1){
            result->push_back(ch);
            ch = 0;
        }
        else{
            ch <<= 4;
        }
    }
    
    return result;
}

json_t NFCTagConverterBluePad::saneiFixedData() const
{
    auto path = BaseSystem::instance().getResourceDirectoryPath() / "sanei.fixed.json";
    std::ifstream is(path.string());
    //return std::string(std::istreambuf_iterator<char>(is), {});   /* テキストファイル一括読み込み */
    json_t json;
    json.deserialize(is);
    return json;
}



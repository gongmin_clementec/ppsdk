//
//  JobUnionPayAfterApprovalCancelJMups.cpp
//  ppsdk
//
//  Created by tel on 2018/12/11.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "JobUnionPayAfterApprovalCancelJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "JMupsJobStorage.h"

JobUnionPayAfterApprovalCancelJMups::JobUnionPayAfterApprovalCancelJMups(const __secret& s) :
    JobUnionPaySalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobUnionPayAfterApprovalCancelJMups::~JobUnionPayAfterApprovalCancelJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.unionpay").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging().as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobUnionPayAfterApprovalCancelJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::CancelAfterApproval;
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    LogManager_Function();

    auto slipNo = boost::make_shared<std::string>();
    return pprx::start_async<TradeInfoBuilder>([=](){
        TradeInfoBuilder builder;
        builder.addSlipNo(TradeInfoBuilder::attribute::required, *slipNo);
        return builder;
    }).as_dynamic()
    .flat_map([=](TradeInfoBuilder builder){
        return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "Search").as_dynamic();
    }).as_dynamic()
    .flat_map([=](const_json_t uiResp){
        *slipNo = uiResp.get<std::string>("results.slipNo");
        return hpsUnionPayAfterApprovalCancel_SearchTrade(*slipNo).as_dynamic();
    })
    .flat_map([=](JMupsAccess::response_t searchResp){
        auto&& searchResultDiv  = searchResp.body.get<std::string>("normalObject.searchResultDiv");
        auto&& cupTradeDiv      = searchResp.body.get<std::string>("normalObject.cupTradeDiv");
        LogManager_AddInformationF("searchResultDiv = %s, cupTradeDiv = %s ", searchResultDiv % cupTradeDiv );
        
        auto&& strSlipNo = *slipNo.get();
        m_tradeData.put("slipNo"            , strSlipNo);
        m_tradeData.put("cupTradeDiv"       , cupTradeDiv);
        
        if(searchResultDiv == "1"){
            /* 検索成功 */
            auto&& amount               = searchResp.body.get<std::string>("normalObject.amount");
            auto&& slipNo               = searchResp.body.get<std::string>("normalObject.slipNo");
            auto&& cupSendDateArr       = searchResp.body.get<json_t::array_t>("normalObject.cupSendDate");
            auto&& approvalNo           = searchResp.body.get<std::string>("normalObject.approvalNo");
            auto&& cupNo                = searchResp.body.get<std::string>("normalObject.cupNo");
            std::string cupSendDate("");
            for(auto v : cupSendDateArr){
                cupSendDate += v.get<std::string>();
            }
            
            m_tradeData.put("approvalNo"        , approvalNo);
            m_tradeData.put("cupSendDate"       , cupSendDate);
            m_tradeData.put("cupNo"             , cupNo);

            TradeInfoBuilderJMupsUnionPay builder;
            /* 通常取引 */
            builder.addAmount               (TradeInfoBuilder::attribute::required, ::atoi(amount.c_str()));
            builder.addSlipNo               (TradeInfoBuilder::attribute::readonly, slipNo);
            builder.addCupSendDate          (TradeInfoBuilder::attribute::readonly, cupSendDate);
            builder.addApprovalNo           (TradeInfoBuilder::attribute::readonly, approvalNo);
            builder.addCupNo                (TradeInfoBuilder::attribute::readonly, cupNo);
            builder.addCupTradeDiv          (TradeInfoBuilder::attribute::readonly, cupTradeDiv);
            return pprx::maybe::success(builder);
        }
        else{
            /* 通常カード */
            TradeInfoBuilderJMupsUnionPay builder;
            builder.addAmount               (TradeInfoBuilder::attribute::required, boost::none);
            builder.addSlipNo               (TradeInfoBuilder::attribute::readonly, *slipNo);
            builder.addCupSendDate          (TradeInfoBuilder::attribute::required, boost::none);
            builder.addApprovalNo           (TradeInfoBuilder::attribute::required, boost::none);
            builder.addCupNo                (TradeInfoBuilder::attribute::required, boost::none);
            builder.addCupTradeDiv          (TradeInfoBuilder::attribute::readonly, cupTradeDiv);
            return pprx::maybe::success(builder);
        }
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_paymentserver& ex){
            return callOnDispatchWithMessageAndSubParameter("refundSearchError", "", ex.json())
            .on_error_resume_next([=](auto errorOnDispath){
                try{ std::rethrow_exception(err); }
                catch(pprx::ppex_aborted&){
                    return pprx::error<pprx::unit>(err);
                }
                return pprx::error<pprx::unit>(errorOnDispath);
            }).as_dynamic()
            .flat_map([=](auto){
                return pprx::maybe::retry().as_dynamic();
            }).as_dynamic()
            .on_error_resume_next([=](auto uiErr){
                return pprx::maybe::error(uiErr).as_dynamic();
            }).as_dynamic();
        }
        catch(...){
        }
        return pprx::maybe::error(err).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<TradeInfoBuilderJMupsUnionPay>();
    }).as_dynamic()
    .map([=](TradeInfoBuilderJMupsUnionPay builder){
        return builder.asBaseClass();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::sendTradeConfirm(boost::optional<std::string> authenticationNo)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    auto cupSendDate = m_tradeData.get<std::string>("cupSendDate"); //format: MMDDhhmmss
    
    return hpsUnionPayAfterApprovalCancel_Confirm
    (
     m_tradeData.get<int>("amount"),
     m_tradeData.get<std::string>("slipNo"),
     m_tradeData.get<std::string>("approvalNo"),
     m_tradeData.get<std::string>("cupNo"),
     cupSendDate,
     m_tradeData.get<std::string>("cupTradeDiv"),
     authenticationNo,
     authenticationNo ? true : false,                           /* pinRetryFlg */
     m_tradeData.get_optional<std::string>("firstGACCmdRes")    /* firstGACCmdRes */
     );
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::sendTagging()
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsUnionPayAfterApprovalCancel_Tagging( slipNo, prmUnionPay::printResultDiv::Success, boost::none );
    }).as_dynamic();
}

/* virtual */
void JobUnionPayAfterApprovalCancelJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::emvOnUiAmountConfirm(const_json_t emvSession)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    m_tradeData.put("firstGACCmdRes", emvSession.get<std::string>("transaction.firstGACCmdRes"));
    
    return sendTradeConfirm(boost::none)
    .flat_map([=](auto resp){
        return waitForStoreTradeResultData(resp);
    }).as_dynamic()
    .map([=](prmUnionPay::doubleTrade taggingSelector) -> const_json_t{
        m_taggingSelector = taggingSelector;
        return getJMupsStorage()->getTradeResultData();
    });
}

auto JobUnionPayAfterApprovalCancelJMups::waitForStoreTradeResultData(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmUnionPay::doubleTrade>
{ 
    LogManager_Function();
      
    auto tradeData = addMediaDivToCardNo(jmupsData.body);
    tradeData.put("ppsdk.bTradeCompletionFlag", false);
    getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
    return pprx::just(prmUnionPay::doubleTrade::No).as_dynamic();
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddDebug(data.str());
    
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::UnionPay,
     getOperationDiv(),
     false,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_cardInfoData = resp.body.clone();
        
        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
            .as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            throw_error_internal("UnionPay does not support KID.");
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTradeInfo("Confirm")
        .flat_map([=](auto){
            return sendTradeConfirm(boost::none).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups) mutable {
        json_t tradeConfirmFinished; // 取引確認処理終了《通知》json
        tradeConfirmFinished.put("hps", addMediaDivToCardNo(jmups.body));
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", tradeConfirmFinished).as_dynamic()
        .flat_map([=](auto){
            return waitForStoreTradeResultData(jmups).as_dynamic(); // 取消返品の時、二重確認不要
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto selector){
        m_taggingSelector = selector;
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPayAfterApprovalCancelJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "6"); // 承認後売上取消返品
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

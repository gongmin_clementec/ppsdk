//
//  JMupsAccessSetting.cpp
//  ppsdk
//
//  Created by clementec on 2018/12/11.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessSetting.h"


/* アクティベーション */
auto JMupsAccessSetting::hpsAcivateAuth
(
 const std::string& subscribeSequence,
 const std::string& activateID,
 const std::string& oneTimePassword
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "subscribeSequence"  , subscribeSequence);
    setJsonParam(json, "activateID"         , activateID);
    setJsonParam(json, "oneTimePassword"    , oneTimePassword);

    std::string url = "op0619.do?oP0619_1=aaa";
    
    return callHps(url, json);
}

auto JMupsAccessSetting::hpsAcivateDownload
(
 const std::string& subscribeSequence,
 const std::string& activateID,
 const std::string& oneTimePassword,
 const std::string& termPrimaryNo,
 const boost::filesystem::path& filepath
 ) -> pprx::observable<pprx::unit>
{
    json_t json;
    setJsonParam(json, "subscribeSequence"  , subscribeSequence);
    setJsonParam(json, "activateID"         , activateID);
    setJsonParam(json, "oneTimePassword"    , oneTimePassword);
    setJsonParam(json, "termPrimaryNo"      , termPrimaryNo);
    
    std::string url = "op0619.do?oP0619_3=aaa";
    
    return downloadHps(url, json, filepath);
}

auto JMupsAccessSetting::hpsAcivateComplete
(
 const std::string& uniqueId
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "uniqueId"  , uniqueId);

    std::string url = "op0619.do?oP0619_2=aaa";
    
    return callHps(url, json);
}

auto JMupsAccessSetting::hpsPrintCardCope()
-> pprx::observable<JMupsAccess::response_t>
{
    const auto url = "pp0701.do?pp0701=aaa";
    
    json_t json;
    
//    setJsonParam(json, "isRePrint", true);
    setJsonParam(json, "isTraining", isTrainingMode());
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessSetting::hpsPrintService()
-> pprx::observable<JMupsAccess::response_t>
{
    const auto url = "pp1101.do?pp1101=aaa";
    
    json_t json;
    
    //    setJsonParam(json, "isRePrint", true);
    setJsonParam(json, "isTraining", isTrainingMode());
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}


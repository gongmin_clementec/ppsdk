//
//  pprx-observable-from.h
//  ppsdk
//
//  Created by tel on 2017/11/15.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_pprx_observable_from__)
#define __h_pprx_observable_from__

#include "PPReactive.h"


namespace pprx {
    template <typename T> auto observable_from(const T& list) -> pprx::observable<typename T::value_type>
    {
        return pprx::observable<>::create<typename T::value_type>([=](pprx::subscriber<typename T::value_type> s){
            for(auto&& item : list){
                s.on_next(item);
            }
            s.on_completed();
        });
    }
}

#endif /* !defined(__h_pprx_observable_from__) */

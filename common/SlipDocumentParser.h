//
//  SlipDocumentParser.h
//  ppsdk
//
//  Created by clementec on 2018/02/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_SlipDocumentParser__)
#define __h_SlipDocumentParser__

#include <string>
#include <iostream>
#include <boost/regex.hpp>
#include "BaseSystem.h"
#include "SlipDocument.h"

class SlipDocumentParser
{
public:
    class error : public std::exception
    {
    private:
        const std::string m_what;
    protected:
    public:
                error(const std::string& what) : m_what(what) {}
        virtual ~error() = default;
        virtual const char* what() const noexcept
        {
            return m_what.c_str();
        }
    };
    
private:
    
    struct Token
    {
        enum class Type {
            Bad,
            Variable,   // $(FOO)
            Opcode,     // $FOO(xxx)
            ScopeIn,    // {
            ScopeOut,   // }
            Text,       // "FOO"
            Separator,  // __
            EmptyText,  // _ as ""
            LineFeed,   // ;
            Comment     // # FOO <-- tokenには入れない
        };
        Type        type;
        std::string text;
        
        Token() : type(Type::Bad) {}
        
        void clear()
        {
            type = Type::Bad;
            text.clear();
        }
        
        using list_t = std::vector<Token>;
        using iter_t = list_t::const_iterator;
    };
    
    Token::list_t       m_tokens;
    
    int                 m_processLine;
    json_t              m_replaces;
    boost::regex        m_exprVariable;
    SlipDocument::Font  m_currentFont;
    
    std::string     extractVariable(const std::string& src) const;
    void            tokenize(const std::string& src);
    SlipDocument::Document  compile();
    
    void throwErrorInvalidChar(char ch);
    void throwError(const std::string& src);
    void throwError(const boost::format& fmt);
    
    SlipDocument::Td    doVariable(Token::iter_t& token_it);
    SlipDocument::Td    doText(Token::iter_t& token_it);
    SlipDocument::Td    doSeparator(Token::iter_t& token_it);
    SlipDocument::Td    doEmptyText(Token::iter_t& token_it);

    SlipDocument::Table::list_t doOpcode(Token::iter_t& token_it);

    SlipDocument::Table doOpcode_FONT(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table doOpcode_LOGO_IMAGE(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table doOpcode_TABLE(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table::list_t doOpcode_FOREACH(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table::list_t doOpcode_IF_EXIST(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table::list_t doOpcode_IF_NOT_EXIST(Token::iter_t& token_it, const std::string& param);
    SlipDocument::Table doOpcode_WRAPPED_TEXT(Token::iter_t& token_it, const std::string& param);

    SlipDocument::Table::list_t doOpcode_scope_execute_or_skip(Token::iter_t& token_it, bool bExecute);

    void treatForSingleTable(SlipDocument::Tr& tr) const;
    
protected:
public:
            SlipDocumentParser(const_json_t replaces);
    virtual ~SlipDocumentParser();
    
    SlipDocument::sp execute(std::istream& src);
    
#if defined(DEBUG)
    static void test();
#endif
};


#endif /* !defined(__h_LazyObject__) */

//
//  JMupsAccessJournal.cpp
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessJournal.h"

/* static */
JMupsAccessJournal::prmJournalCommon::detailOption
JMupsAccessJournal::prmJournalCommon::stringToDetailOption(const std::string& str)
{
    const std::map<std::string, prmJournalCommon::detailOption> map
    {
        {"Summary"   , prmJournalCommon::detailOption::Summary},
        {"Detail"    , prmJournalCommon::detailOption::Detail}
    };
    auto it = map.find(str);
    if(it == map.end()){
        return prmJournalCommon::detailOption::Bad;
    }
    return it->second;
}

/* static */
JMupsAccessJournal::prmJournalCommon::paymentType
JMupsAccessJournal::prmJournalCommon::stringToPaymentType(const std::string& str)
{
    const std::map<std::string, prmJournalCommon::paymentType> map
    {
        {"Credit"   , prmJournalCommon::paymentType::Credit},
        {"NFC"      , prmJournalCommon::paymentType::NFC},
        {"UnionPay" , prmJournalCommon::paymentType::UnionPay},
        {"All"      , prmJournalCommon::paymentType::All}
    };
    auto it = map.find(str);
    if(it == map.end()){
        return prmJournalCommon::paymentType::Bad;
    }
    return it->second;
}

/* static */
JMupsAccessJournal::prmJournalCommon::reprint
JMupsAccessJournal::prmJournalCommon::stringToReprint(const std::string& str)
{
    const std::map<std::string, prmJournalCommon::reprint> map
    {
        {"No"           , prmJournalCommon::reprint::No},
        {"Last"         , prmJournalCommon::reprint::Last},
        {"BeforeLast"   , prmJournalCommon::reprint::BeforeLast}
    };
    auto it = map.find(str);
    if(it == map.end()){
        return prmJournalCommon::reprint::Bad;
    }
    return it->second;
}

auto JMupsAccessJournal::hpsDailyJournal
(
 prmJournalCommon::paymentType  paymentType,
 prmJournalCommon::reprint      reprint,
 prmJournalCommon::detailOption detailOption
 )
 -> pprx::observable<JMupsAccess::response_t>
{
    const std::map<prmJournalCommon::paymentType, std::string> urls
    {
        {prmJournalCommon::paymentType::Credit,  "dt0127.do?dT0127_1=aaa"},
        {prmJournalCommon::paymentType::NFC,     "dt5027.do?dT5127_1=aaa"},
        {prmJournalCommon::paymentType::UnionPay,"dt0927.do?dT0927_1=aaa"},
        {prmJournalCommon::paymentType::All,     "dt1327.do?dT1327_1=aaa"},
    };
    const std::map<prmJournalCommon::paymentType, std::string> urls_reprint
    {
        {prmJournalCommon::paymentType::Credit,  "rp0134.do?rP0134=aaa"},
        {prmJournalCommon::paymentType::NFC,     "rpb634.do?rPB634=aaa"},
        {prmJournalCommon::paymentType::UnionPay,"rp1934.do?rP1934=aaa"},
        {prmJournalCommon::paymentType::All,     "rp3134.do?rP3134=aaa"},
    };
    auto&& _urls = (reprint == prmJournalCommon::reprint::No) ? urls : urls_reprint;
    auto it = _urls.find(paymentType);
    if(it == _urls.end()){
        throw_error_internal("unknown typeOption");
    }
    const auto url = it->second;
    
    json_t json;
    
    if(reprint == prmJournalCommon::reprint::No){
        setJsonParam(json, "isRePrint", "false");
    }
    else if(reprint == prmJournalCommon::reprint::Last){
        setJsonParam(json, "isRePrint", "true");
        setJsonParam(json, "journalDiv", "0");
    }
    else if(reprint == prmJournalCommon::reprint::BeforeLast){
        setJsonParam(json, "isRePrint", "true");
        setJsonParam(json, "journalDiv", "1");
    }
    setJsonParam(json, "detailOption", detailOption,
                 {
                     {prmJournalCommon::detailOption::Summary , "0"},
                     {prmJournalCommon::detailOption::Detail  , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    if(isTrainingMode()){
        if(detailOption == prmJournalCommon::detailOption::Detail){
            return callTraining(url + "&detail", keyvalue_t());
        }
        else if(detailOption == prmJournalCommon::detailOption::Summary){
            return callTraining(url + "&summary", keyvalue_t());
        }
        else{
            return pprx::error<JMupsAccess::response_t>(make_error_internal("parameter error"));
        }
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessJournal::hpsDailyJournalApply
(
 prmJournalCommon::paymentType  paymentType,
 const std::string&             outputDate
 )
 -> pprx::observable<JMupsAccess::response_t>
{
    const std::map<prmJournalCommon::paymentType, std::string> urls
    {
        {prmJournalCommon::paymentType::Credit,  "dt0127.do?dT0127_2=aaa"},
        {prmJournalCommon::paymentType::NFC,     "dt5027.do?dT5127_2=aaa"},
        {prmJournalCommon::paymentType::UnionPay,"dt0927.do?dT0927_2=aaa"},
        {prmJournalCommon::paymentType::All,     "dt1327.do?dT1327_2=aaa"},
    };
    auto it = urls.find(paymentType);
    if(it == urls.end()){
        throw_error_internal("unknown typeOption");
    }
    const auto url = it->second;

    json_t json;
    
    setJsonParam(json, "outputDate", outputDate);
    setJsonParam(json, "isTraining", isTrainingMode());
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessJournal::hpsImJournal
(
 prmJournalCommon::paymentType  paymentType,
 prmJournalCommon::detailOption detailOption
 )
-> pprx::observable<JMupsAccess::response_t>
{
    const std::map<prmJournalCommon::paymentType, std::string> urls
    {
        {prmJournalCommon::paymentType::Credit,  "pt0127.do?pT0127=aaa"},
        {prmJournalCommon::paymentType::NFC,     "pt6127.do?pT6127=aaa"},
        {prmJournalCommon::paymentType::UnionPay,"pt0927.do?pT0927=aaa"},
        {prmJournalCommon::paymentType::All,     "pt1327.do?pT1327=aaa"},
    };
    auto it = urls.find(paymentType);
    if(it == urls.end()){
        throw_error_internal("unknown typeOption");
    }
    const auto url = it->second;
    
    json_t json;
    
    setJsonParam(json, "isRePrint", "false");
    setJsonParam(json, "detailOption", detailOption,
                 {
                     {prmJournalCommon::detailOption::Summary , "0"},
                     {prmJournalCommon::detailOption::Detail  , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    if(isTrainingMode()){
        if(detailOption == prmJournalCommon::detailOption::Detail){
            return callTraining(url + "&detail", keyvalue_t());
        }
        else if(detailOption == prmJournalCommon::detailOption::Summary){
            return callTraining(url + "&summary", keyvalue_t());
        }
        else{
            return pprx::error<JMupsAccess::response_t>(make_error_internal("parameter error"));
        }
    }
    else{
        return callHps(url, json);
    }
}

//
//  JobNFCRefundJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCRefundJMups.h"
#include "PPConfig.h"
#include "JMupsJobStorage.h"
#include "CardReader.h"
#include <time.h>

JobNFCRefundJMups::JobNFCRefundJMups(const __secret& s) :
    JobNFCSalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobNFCRefundJMups::~JobNFCRefundJMups()
{
    LogManager_Function();
}

/* virtual */
auto JobNFCRefundJMups::getObservableSelf()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return waitForReaderConnect().as_dynamic()
    .flat_map([=](auto){
        return processMasterData().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()

    .flat_map([=](auto){
        return processCardJob();
    }).as_dynamic()
    .flat_map([=](auto success){
        // ここまできたらNFC決済成功とみなし成功音を鳴らす
        BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_APPROVED");
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        // NFC決済失敗とみなし失敗音を鳴らす
        try{std::rethrow_exception(err);}
        catch(pprx::ppex_cardrw& perrRw){
            auto code = perrRw.json().get_optional<std::string>("result.detail.code");
            if(!code || *code == "PP_E4001") {
                BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_DECLINED");
                return pprx::maybe::error(err);
            }
        }
        BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_DECLINED");
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()

    .flat_map([=](auto){
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()    
    .flat_map([=](auto){
        return sendTagging().as_dynamic();
    }).as_dynamic()   
    .map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return json.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobNFCRefundJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}

auto JobNFCRefundJMups::tradeStart()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return hpsTradeStart_Request
    (
     JMupsAccess::prmTradeStart_Request::serviceDiv::Nfc,
     boost::none,
     JMupsAccess::prmTradeStart_Request::nfcDeviceType::Miura
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_tradeStartResponseHeader = resp.header.clone();
        if(PaymentJob::isTrainingMode()){
            LogManager_AddDebug("skip adjusting time. (training)");
            time_t rawtime;
            time (&rawtime);
            LogManager_AddDebugF("The current local time is: %s", ctime (&rawtime));
            m_tradeStartResponseHeader.put("Date", ctime (&rawtime));
        }
        else{
            auto dtime = BaseSystem::parseRFC2616DateTimeText(resp.header.get<std::string>("Date"));
            LogManager_AddInformationF("adjust time to %s", boost::posix_time::to_iso_extended_string(dtime));
            return getCardReader()->adjustDateTime(dtime).as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic();
}

auto JobNFCRefundJMups::waitForRefundInput()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    json_t param;
    auto&& termInfo = getTerminalInfo();
    auto&& tradeInfo = getTradeStartInfo();
    
    const bool isNeedProductCode = tradeInfo.get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
    const bool isNeedTaxOther    = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
    
    param.put("isNeedProductCode", isNeedProductCode);
    param.put("isNeedTaxOther",    isNeedTaxOther);
    
    return callOnDispatchWithCommandAndSubParameter("waitForRefundInput", "hps", param)
    .map([=](const_json_t cjson){
        auto json = cjson.clone();
        if(!isNeedProductCode){
            const auto productCode = termInfo.get<std::string>("sessionObject.productCode");
            LogManager_AddInformationF("no need 'productCode' -> productCode = '%s'", productCode);
            json.put("productCode", productCode);
        }
        if(!isNeedTaxOther){
            LogManager_AddInformation("no need 'taxOtherAmount' -> taxOtherAmount = 0");
            json.put("taxOtherAmount", 0);
        }
        
        m_tradeData = json;
        auto&& currencyCode = termInfo.get_optional<std::string>("sessionObject.transactionCurrencyCode");
        if(currencyCode) {
            m_tradeData.put("currencyCode", boost::lexical_cast<int>(*currencyCode));
        } else {
            // トレーニングモードなどで開局情報を取得できない場合は固定値で392を設定する。
            m_tradeData.put("currencyCode", 392);
        }
        
        return pprx::unit();
    });
}
auto JobNFCRefundJMups::waitForTouching()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    {
        int totalAmount = 0;
        {
            auto amount = m_tradeData.get_optional<int>("amount");
            if(!amount){
                LogManager_AddError("amount not found.");
            }
            else{
                totalAmount = amount.get();
                auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
                if(taxOtherAmount){
                    totalAmount += taxOtherAmount.get();
                }
            }
        }
        const auto messageStr = (boost::format("タッチしてください\nNFC　取消\n￥%d") % totalAmount).str();
        
        json_t::array_t jarr;
        json_t message;
        message.put("id"    , "EMV_UI_PRESENT_CARD");
        message.put("text"  , messageStr);
        message.put("font"  , "FONT_10X10_JPN");
        jarr.push_back(message);
        m_tradeData.put("optionalMessage.table", jarr);
    }
    
    /* R/W側で動作を変更するために、isTrainingを記録する */
    m_tradeData.put("isTraining", PaymentJob::isTrainingMode());
    auto dateOpt = m_tradeStartResponseHeader.get_optional<std::string>("Date");
    auto dateJson = PaymentJob::getTradeDateTime((dateOpt ? *dateOpt : ""));
    m_tradeData.put("dateTime", dateJson.clone());
    m_tradeData.put("tradeType", "refund");
    
    return callOnDispatchWithMessage("waitForCardRead").as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->readContactless(m_tradeData);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        json_t dispatchParam;
        dispatchParam.put("cardType", "nfc");
        return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
        .flat_map([json,this](auto){
            if(json.empty()){
                return pprx::maybe::error(make_error_internal("json is null or empty.")).as_dynamic();
            }
            auto status = json.get_optional<std::string>("status");
            if(!status){
                return pprx::maybe::error(make_error_internal("status not found.")).as_dynamic();
            }
            else if(status.get() == "approved"){
                m_cardData = json.clone();
                return pprx::maybe::success(pprx::unit()).as_dynamic();
            }
            else if(status.get() == "onlineProcessing"){
                m_cardData = json.clone();
                return pprx::maybe::success(pprx::unit()).as_dynamic();
            }
            else if(status.get() == "declined"){
                json_t j;
                m_cardData = json.clone();
                j.put("description", "declined");
                return pprx::maybe::error(make_error_cardrw(j)).as_dynamic();
            }
            else if(status.get() == "seePhone"){
                return waitForCardActive()
                .flat_map([=](auto){
                    return pprx::maybe::retry().as_dynamic();
                }).as_dynamic();
            }
            else if(status.get() == "timeout"){
                return pprx::maybe::error(make_error_internal("timeout")).as_dynamic();
            }
            else {
                return pprx::maybe::error(make_error_internal("unknown")).as_dynamic();
            }
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_cardrw& perrRw){
            auto desc = perrRw.json().get_optional<std::string>("result.detail.description");
            if(desc){
                LogManager_AddDebugF("waitForTouching. error desc = %s", *desc);
                if(*desc == "SeePhone"){
                    return waitForCardActive()
                    .flat_map([=](auto) {
                        return pprx::maybe::retry();
                    }).as_dynamic();
                }
            }
        }
        catch(...){
            LogManager_AddError("waitForTouching rethrow_exception");
        }
        
        return pprx::maybe::error(err).as_dynamic();
    })
    .retry()
    .flat_map([=](pprx::maybe obj){
        return obj.observableForContinue<pprx::unit>();
    }).as_dynamic();
}

auto JobNFCRefundJMups::onCardReadFinished()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    json_t param;
    param.put("type", "online");
    return callOnDispatchWithMessageAndSubParameter("onCardReadFinished", "", param)
    .map([=](auto){
        return pprx::unit();
    });
}

auto JobNFCRefundJMups::searchTrade()
  -> pprx::observable<const_json_t>
{
    LogManager_Function();

    const auto cardBrandIdentifier = JMupsAccessNFC::prmNFCCommon::stringToCardBrandIdentifier(m_cardData.get<std::string>("cardBrand"));
    
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    auto taxAmount      = taxOtherAmount ? (*taxOtherAmount) : 0;
    
    return hpsNFCSearchTradeWithCard
    (
     prmNFCCommon::nfcDeviceType::Miura,
     prmNFCCommon::resultCode::Success,
     m_cardData.get<std::string>("resultCodeExtended"),
     m_tradeData.get<std::string>("slipNo"),
     m_tradeData.get<int>("amount"),
     taxAmount,
     m_tradeData.get<std::string>("productCode"),
     prmNFCCommon::payWayDiv::Lump,
     false,
     m_cardData.get<std::string>("transactionResultBit"),
     cardBrandIdentifier,
     m_cardData.get<std::string>("nfcTradeResult"),
     m_cardData.get<std::string>("outcomeParameter"),
     m_cardData.get_optional<std::string>("nfcTradeResultExtended"),
     m_cardData.get_optional<std::string>("keySerialNoForResult"),
     m_cardData.get_optional<std::string>("keySerialNoForExtended")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        const auto searchResultDiv = resp.body.get_optional<std::string>("normalObject.searchResultDiv");
        if(searchResultDiv && (searchResultDiv.get() == "1")){
            /* 取引結果が見つかった */
            return pprx::just(const_json_t()).as_dynamic();
        }
        else{
            /* 取引結果が無かったけど、requestRefund()を呼び出せば「返品」扱いで成功する */
            LogManager_AddInformationF("refund target not found. -> slipNo = %s", m_tradeData.get<std::string>("slipNo"));
            return pprx::error<const_json_t>(make_error_internal("No Refund SLIPNO")).as_dynamic();
        }
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

auto JobNFCRefundJMups::requestRefund()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    const auto cardBrandIdentifier = JMupsAccessNFC::prmNFCCommon::stringToCardBrandIdentifier(m_cardData.get<std::string>("cardBrand"));
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    auto taxAmount      = taxOtherAmount ? (*taxOtherAmount) : 0;
    return hpsNFCRefund
    (
     prmNFCCommon::nfcDeviceType::Miura,
     prmNFCRefund::undoDiv::Undo,
     prmNFCCommon::resultCode::Success,
     m_cardData.get<std::string>("resultCodeExtended"),
     m_tradeData.get<std::string>("slipNo"),
     m_tradeData.get<int>("amount"),
     taxAmount,
     m_tradeData.get<std::string>("productCode"),
     prmNFCCommon::payWayDiv::Lump,
     false,
     m_cardData.get<std::string>("transactionResultBit"),
     cardBrandIdentifier,
     m_cardData.get<std::string>("nfcTradeResult"),
     m_cardData.get<std::string>("outcomeParameter"),
     m_cardData.get_optional<std::string>("nfcTradeResultExtended"),
     m_cardData.get_optional<std::string>("keySerialNoForResult"),
     m_cardData.get_optional<std::string>("keySerialNoForExtended")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        json_t tradeConfirmFinished; // 取引確認処理終了《通知》json
        auto tradeData = addMediaDivToCardNo(resp.body);
        tradeConfirmFinished.put("hps", tradeData.clone());
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", tradeConfirmFinished).as_dynamic()
        .flat_map([=](auto){
            return waitForStoreTradeResultData(resp).as_dynamic(); 
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto JobNFCRefundJMups::sendTagging()
  -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsNFCRefund_Tagging
        (
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         false,
         prmNFCCommon::printResultDiv::Success,
         boost::none   /* isSendingDesiSlip */
         );
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<JMupsAccess::response_t>(err).as_dynamic();
    }).as_dynamic();
}

auto JobNFCRefundJMups::processSendDigiSign()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    const auto digiReceiptDiv = tradeResultData.get_optional<std::string>("normalObject.digiReceiptDiv");
    if( digiReceiptDiv && (*digiReceiptDiv == "1")){
        LogManager_AddInformation("電子伝票保管対象（サイン要）");        
        return waitForDigiSign()
        .flat_map([=](boost::optional<std::string> s){
            if(s){
                std::string sign = *s;
                return sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip, sign);
            }
            else{
                return pprx::error<pprx::unit>(make_error_internal("sign data is empty")).as_dynamic();;
            }
        }).as_dynamic()
        .flat_map([=](auto){
            return pprx::observable<>::just(pprx::unit());
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError(pprx::exceptionToString(err));
            return pprx::error<pprx::unit>(err).as_dynamic();
        }).as_dynamic();
    }
    else if(digiReceiptDiv && (*digiReceiptDiv == "2")){
        LogManager_AddInformation("電子伝票保管対象（サイン不要）");
        return pprx::observable<>::just(pprx::unit());
    }
    else{
        LogManager_AddInformation("電子伝票保管対象外");
        tradeResultData.put("ppsdk.isNeedAquirerSlip", true);
        getJMupsStorage()->setTradeResultData(tradeResultData);
        return pprx::observable<>::just(pprx::unit());
    }
}


auto JobNFCRefundJMups::waitForDigiSign()
-> pprx::observable<boost::optional<std::string>>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();

    json_t param;
    param.put("modulus", tradeResultData.get<std::string>("normalObject.digiSignPubKeyModulus"));
    param.put("exponent", tradeResultData.get<std::string>("normalObject.digiSignPubKeyExponent"));
    const auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    auto totalAmount = m_tradeData.get<int>("amount") + (taxOtherAmount ? *taxOtherAmount : 0);
    param.put("amount", totalAmount);
    auto strAmount = (boost::format("合計金額：%s 円") % BaseSystem::commaSeparatedString(totalAmount)).str();
    param.put("strAmount", strAmount );
    
    return callOnDispatchWithCommandAndSubParameter("waitForDigiSign", "hps", param)
    .map([=](const_json_t json){
         return json.get_optional<std::string>("encryptedDigiSignData");
     });
}

auto JobNFCRefundJMups::processCardJob()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit()).as_dynamic()
    .flat_map([=](auto){
        return masterDataSendUpdateDate(getTradeStartInfo()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForRefundInput().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTouching().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return onCardReadFinished().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return searchTrade().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return requestRefund().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto status = m_cardData.get<std::string>("status");
        if(status == "onlineProcessing"){
            bool bSuccess = false;
            auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
            auto values = tradeResultData.get_optional<json_t::array_t>("normalObject.printInfo.device.value.values");
            if(values && (values->size() > 0)){
                auto failure_info = (*values)[0].get_optional<json_t::array_t>("FAILURE_INFO");
                if(!failure_info){
                    bSuccess = true;
                }
            }
            return getCardReader()->onlineProccessingResult(bSuccess);
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddInformation(pprx::exceptionToString(err));
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_internal& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_paymentserver& ppex){
            auto errjson = ppex.json().get("result");
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_cardrw& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_aborted& ppex){
            LogManager_AddInformationF("exception\t%s", ppex.json().str());
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppexception& ppex){
            LogManager_AddInformationF("ppexception\t%s", ppex.json().str());
        }
        catch(...){
        }
        
        auto cardStatus = m_cardData.get_optional<std::string>("status");
        if(cardStatus && (*cardStatus == "onlineProcessing")){
            return getCardReader()->onlineProccessingResult(false)
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(err).as_dynamic();
            }).as_dynamic();
        }
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto JobNFCRefundJMups::waitForStoreTradeResultData(JMupsAccess::response_t jmupsData)
-> pprx::observable<pprx::unit>
{ 
    LogManager_Function();
    
    json_t tradeData = addMediaDivToCardNo(jmupsData.body);
    tradeData.put("ppsdk.bTradeCompletionFlag", false);
    getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
    return pprx::just(pprx::unit()).as_dynamic();
}

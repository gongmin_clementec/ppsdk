//
//  JobUnionPayPreApprovalCancelJMups.cpp
//  ppsdk
//
//  Created by tel on 2018/12/11.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobUnionPayPreApprovalCancelJMups__)
#define __h_JobUnionPayPreApprovalCancelJMups__

#include "JobUnionPaySalesJMups.h"

class JobUnionPayPreApprovalCancelJMups :
    public JobUnionPaySalesJMups
{
private:

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual void beginAsyncPreInputAmount();

    auto sendTradeConfirm(boost::optional<std::string> authenticationNo)  -> pprx::observable<JMupsAccess::response_t>;
    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;
    auto waitForStoreTradeResultData(JMupsAccess::response_t jmupsData) -> pprx::observable<prmUnionPay::doubleTrade>;
    
protected:
    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    
public:
            JobUnionPayPreApprovalCancelJMups(const __secret& s);
    virtual ~JobUnionPayPreApprovalCancelJMups();
};



#endif /* !defined(__h_JobUnionPayPreApprovalCancelJMups__) */

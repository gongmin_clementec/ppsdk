//
//  JobCreditCardCheckJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobCreditCardCheckJMups__)
#define __h_JobCreditCardCheckJMups__

#include "JobCreditSalesJMups.h"

class JobCreditCardCheckJMups :
    public JobCreditSalesJMups
{
private:
    auto applyDispatchResponseParamForTradeInfo() -> pprx::observable<bool>;
    auto sendTradeConfirm()  -> pprx::observable<JMupsAccess::response_t>;
    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;
    auto waitForStoreTradeResultData(JMupsAccess::response_t jmupsData) -> pprx::observable<prmCredit::doubleTrade>;
    auto waitForCardCheckResultConfirm()->pprx::observable<pprx::unit>;

    auto processCommon() -> pprx::observable<pprx::unit>;
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto waitForTradeInfo(const std::string& strTradeOp="")-> pprx::observable<pprx::unit>;
    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto proceedContact(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    virtual auto emvOnNetwork(const std::string& url, const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    virtual void beginAsyncPreInputAmount();

public:
            JobCreditCardCheckJMups(const __secret& s);
    virtual ~JobCreditCardCheckJMups();
};



#endif /* !defined(__h_JobCreditCardCheckJMups__) */

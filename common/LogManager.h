//
//  LogManager.hpp
//  ppsdk
//
//  Created by clementec on 2016/05/11.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//
#if !defined(__h_LogManager__)
#define __h_LogManager__

#include "Singleton.h"
#include <string>
#include <vector>
#include <queue>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/date_time/posix_time/posix_time.hpp>
#pragma GCC diagnostic pop
#include <boost/format.hpp>
#include <boost/filesystem.hpp>

class LogManager : public Singleton<LogManager>
{
    friend class Singleton<LogManager>;
    
public:
    enum class EType
    {
        Bad = 0,
        Error = 1,
        Warning = 2,
        Information = 3,
        Debug = 4,
        ScopeIn = 5,
        ScopeOut = 6
    };

private:
    
    struct item_t
    {
        boost::recursive_mutex      mtx;
        EType                       type;
        boost::posix_time::ptime    when;
        int                         threadid;
        std::string                 file;
        std::string                 function;
        int                         line;
        std::string                 what;  /* error code */
        
        std::string toString(bool bOmitDate);
    };
    
    using item_sp = boost::shared_ptr<item_t>;
    
    std::vector<item_sp>    m_items;
    size_t                  m_items_wp;
    bool                    m_items_wrapped;
    boost::recursive_mutex  m_items_mtx;
    
    std::queue<item_sp>     m_display_queue;
    boost::recursive_mutex  m_display_queue_mtx;
    
    boost::recursive_mutex  m_display_mtx;
    
    bool                    m_opmode_bOutputToConsole;
    bool                    m_opmode_bRecordLog;
    boost::recursive_mutex  m_opmode_mtx;

    boost::recursive_mutex  m_file_queue_mtx;
    std::queue<item_sp>     m_file_queue;
    
    boost::recursive_mutex  m_file_mtx;
    boost::filesystem::path m_file_path;
    boost::filesystem::path m_log_directory;
    int                     m_log_max_files;
    
    LogManager(size_t maxLogItem = 1000);
    virtual ~LogManager();
    
    void                    deleteOldFiles();
    
protected:
public:
    std::vector<std::string>    collectLogFiles();
    void                        flushLogFile();
    void                        deleteLogFileAll();
    
    void clear();
    void add(EType type, const std::string& file, const std::string& func, int line, const std::string& what);
    void dump(std::ostream& os);
    void archive(const boost::filesystem::path& dest);
    
    void updateOperationMode();
    
    class scoped
    {
    private:
        const std::string m_file;
        const std::string m_func;
        const int m_line;
        const std::string m_what;
        
    protected:
    public:
        scoped(const std::string& file, const std::string& func, int line, const std::string& what) :
            m_file(file),
            m_func(func),
            m_line(line),
            m_what(what)
        {
            LogManager::instance().add(LogManager::EType::ScopeIn, m_file, m_func, m_line, "{{ " + m_what);
        }
        ~scoped()
        {
            LogManager::instance().add(LogManager::EType::ScopeOut, m_file, m_func, m_line, m_what + " }}");
        }
    };
};

#define LogManager_AddError(w)              LogManager::instance().add(LogManager::EType::Error,__FILE__,__FUNCTION__,__LINE__,(w))
#define LogManager_AddWarning(w)            LogManager::instance().add(LogManager::EType::Warning,__FILE__,__FUNCTION__,__LINE__,(w))
#define LogManager_AddInformation(w)        LogManager::instance().add(LogManager::EType::Information,__FILE__,__FUNCTION__,__LINE__,(w))
#define LogManager_AddFullInformation(w)    LogManager::instance().add(LogManager::EType::Information,__FILE__,__FUNCTION__,__LINE__,(std::string("\t")+(w)))
#define LogManager_AddDebug(w)              LogManager::instance().add(LogManager::EType::Debug,__FILE__,__FUNCTION__,__LINE__,(w))

#define LogManager_AddErrorF(f,expr)            LogManager::instance().add(LogManager::EType::Error,__FILE__,__FUNCTION__,__LINE__,(boost::format(f)%expr).str())
#define LogManager_AddWarningF(f,expr)          LogManager::instance().add(LogManager::EType::Warning,__FILE__,__FUNCTION__,__LINE__,(boost::format(f)%expr).str())
#define LogManager_AddInformationF(f,expr)      LogManager::instance().add(LogManager::EType::Information,__FILE__,__FUNCTION__,__LINE__,(boost::format(f)%expr).str())
#define LogManager_AddFullInformationF(f,expr)  LogManager::instance().add(LogManager::EType::Information,__FILE__,__FUNCTION__,__LINE__,(boost::format(std::string("\t")+(f))%expr).str())
#define LogManager_AddDebugF(f,expr)            LogManager::instance().add(LogManager::EType::Debug,__FILE__,__FUNCTION__,__LINE__,(boost::format(f)%expr).str())


#define LogManager_Scoped(w)            LogManager::scoped ____(__FILE__,__FUNCTION__,__LINE__,(w))
#define LogManager_ScopedF(f,expr)      LogManager::scoped ____(__FILE__,__FUNCTION__,__LINE__,(boost::format(f)%expr).str())
#define LogManager_Function()           LogManager_Scoped("")
#define LogManager_Lambda()             LogManager::scoped ____(__FILE__,__PRETTY_FUNCTION__,__LINE__,"")

#endif /* !defined(__h_LogManager__) */

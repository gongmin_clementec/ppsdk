//
//  JobNFCReprintSlipJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCReprintSlipJMups.h"

JobNFCReprintSlipJMups::JobNFCReprintSlipJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobNFCReprintSlipJMups::~JobNFCReprintSlipJMups()
{
    
}

/* virtual */
auto JobNFCReprintSlipJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    return callOnDispatchWithCommand("waitForOptions")
    .flat_map([=](const_json_t json){
        auto slipNo = json.get_optional<std::string>("slipNo");
        if(slipNo){
            return hpsReprintWithSlipNo(prmReprintCommon::paymentType::NFC, *slipNo)
            .as_dynamic();
        }
        else{
            return hpsReprintLastSlip(prmReprintCommon::paymentType::NFC)
            .as_dynamic();
        }
    }).as_dynamic()
    .map([=](auto resp){
        json_t json;
        json.put("status", "success");
        json.put("result.hps", resp.body.clone());
        return json.as_readonly();
    });
}

/* virtual */
auto JobNFCReprintSlipJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


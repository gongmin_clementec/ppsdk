//
//  Image.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_Image__)
#define __h_Image__


#include "BaseSystem.h"

class Image
{
private:
    std::vector<uint8_t>    m_bits;
    int m_width;
    int m_height;
    int m_rowBytes;
    int m_bytesPerPixel;
    
protected:
    
public:
    Image(const void* image, int width, int height, int rowBytes, int bytesPerPixel) :
        m_bits(rowBytes * height),
        m_width(width),
        m_height(height),
        m_rowBytes(rowBytes),
        m_bytesPerPixel(bytesPerPixel)
    {
        if(image != nullptr){
            ::memcpy(m_bits.data(), image, m_bits.size());
        }
    }
    
    virtual ~Image()
    {
    }

    int width() const           { return m_width; }
    int height() const          { return m_height; }
    int rowBytes() const        { return m_rowBytes; }
    int bytesPerPixel() const   { return m_bytesPerPixel; }
    
    const uint8_t* data() const { return m_bits.data(); }
    
    using sp = boost::shared_ptr<Image>;
};

#endif /* !defined(__h_Image__) */

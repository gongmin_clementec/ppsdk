//
//  TextUtils.h
//  ppsdk
//
//  Created by tel on 2017/08/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_TextUtils__)
#define __h_TextUtils__

#include <vector>
#include <string>

class TextUtils
{
private:
    
protected:

public:
    using utf32_t = std::vector<uint32_t>;
    enum class TextAlignment
    {
        Bad,
        None,
        Left,
        Right,
        Center
    };
    
    static utf32_t utf8To32(const std::string& src);
    static std::string utf32To8(const utf32_t& src);
    
    
    static int countAsHalfWidthCharacter(const utf32_t& src);
    
    static std::vector<std::string> split
    (
     const std::string& src,
     int rowCountWithHalfwidth,
     TextAlignment textAlignment = TextAlignment::None,
     int maxLine = 0
     );
    static bool isHalfwidthChar(uint32_t ch);
    
    
};




#endif /* !defined(__h_TextUtils__) */

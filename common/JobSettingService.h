//
//  JobSettingService.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingService__)
#define __h_JobSettingService__

#include "PaymentJob.h"
#include "JMupsAccessSetting.h"
#include "JMupsSlipPrinter.h"

class JobSettingService :
    public PaymentJob,
    public JMupsAccessSetting,
    public JMupsSlipPrinter
{
private:
    json_t m_settingServiceResp;

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;
    
    auto waitForServicePrintConfirm() -> pprx::observable<pprx::unit>;
    
public:
            JobSettingService(const __secret& s);
    virtual ~JobSettingService();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobSettingService__) */

//
//  JMupsAccessReprint.cpp
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessReprint.h"

/* static */
JMupsAccessReprint::prmReprintCommon::paymentType
JMupsAccessReprint::prmReprintCommon::stringToPaymentType(const std::string& str)
{
    const std::map<std::string, prmReprintCommon::paymentType> map
    {
        {"Credit"   , prmReprintCommon::paymentType::Credit},
        {"NFC"      , prmReprintCommon::paymentType::NFC},
        {"UnionPay" , prmReprintCommon::paymentType::UnionPay}
    };
    auto it = map.find(str);
    if(it == map.end()){
        return prmReprintCommon::paymentType::Bad;
    }
    return it->second;
}

auto JMupsAccessReprint::hpsReprintLastSlip(prmReprintCommon::paymentType paymentType)
 -> pprx::observable<JMupsAccess::response_t>
{
    const std::map<prmReprintCommon::paymentType, std::string> urls
    {
        {prmReprintCommon::paymentType::Credit,  "rp0534.do?rP0534=aaa"},
        {prmReprintCommon::paymentType::NFC,     "rpb934.do?rPB934=aaa"},
        {prmReprintCommon::paymentType::UnionPay,"rp2334.do?rP2334=aaa"}
    };
    auto it = urls.find(paymentType);
    if(it == urls.end()){
        throw_error_internal("unknown typeOption");
    }
    const auto url = it->second;
    
    json_t json;
    
    setJsonParam(json, "isRePrint", true);
    setJsonParam(json, "isTraining", isTrainingMode());
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessReprint::hpsReprintWithSlipNo(prmReprintCommon::paymentType paymentType, const std::string& slipNo)
 -> pprx::observable<JMupsAccess::response_t>
{
    const std::map<prmReprintCommon::paymentType, std::string> urls
    {
        {prmReprintCommon::paymentType::Credit,  "rp0619.do?rP0619=aaa"},
        {prmReprintCommon::paymentType::NFC,     "rpba19.do?rPBA19=aaa"},
        {prmReprintCommon::paymentType::UnionPay,"rp2419.do?rP2419=aaa"}
    };
    auto it = urls.find(paymentType);
    if(it == urls.end()){
        throw_error_internal("unknown typeOption");
    }
    const auto url = it->second;

    json_t json;
    
    setJsonParam(json, "isRePrint", true);
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();
        reps["##SLIP_NO##"] = slipNo;
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}



//
//  JMupsEmvKernel.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JMupsEmvKernel__)
#define __h_JMupsEmvKernel__

#include "PPReactive.h"

class JavascriptEnvironment;
class JMupsJobStorage;

class JMupsEmvKernel
{
public:
    enum class Type
    {
        Bad, Credit, UnionPay
    };
    
private:
    const Type                  m_type;
    boost::shared_ptr<JavascriptEnvironment> m_jsenv;
    json_t                      m_initialParameter;
    std::string                 m_encryptKeyModulus;
    std::string                 m_encryptKeyExponent;
    
    json_t                      m_tradeResultData;
    json_t                      m_jsTradeConfirmFinish;
    json_t                      m_jsPinResultConfirm;
    json_t                      m_emvSession;
    
    pprx::subject<pprx::unit>   m_emvProcessing;
    bool                        m_bOfflinePinConfirmed;
    bool                        m_bOnlinePinReinput;

    struct kernelparam_t
    {
        json_t   methods;
        json_t   args;
    };
    pprx::subjects::behavior<kernelparam_t> m_kernelParam;
    void setNextKernelParam(const std::vector<std::string>& methods, const_json_t arg);
    auto initialize(boost::shared_ptr<JMupsJobStorage> storage) -> pprx::observable<pprx::unit>;
    auto kernelInit() -> pprx::observable<pprx::unit>;
    auto kernelExecute(const kernelparam_t& kernelparam) -> pprx::observable<const_json_t>;
    auto kernelGetCurrentNode() -> pprx::observable<const_json_t>;
    auto kernelSetSession(const_json_t json) -> pprx::observable<pprx::unit>;
    auto kernelGetSession() -> pprx::observable<const_json_t>;
    auto kernelExecuteAndProcessEvent(kernelparam_t kernelParam) -> pprx::observable<pprx::unit>;
    auto kernelProcessEvent(const_json_t kernelResponse) -> pprx::observable<pprx::unit>;
    auto dispatchDevice(const_json_t device) -> pprx::observable<pprx::unit>;
    auto dispatchDeviceICCard(const_json_t device) -> pprx::observable<pprx::unit>;
    auto dispatchDeviceNetwork(const_json_t device) -> pprx::observable<pprx::unit>;
    auto dispatchWaiting(const_json_t waiting) -> pprx::observable<pprx::unit>;
    auto dispatchParentControl(const_json_t parentControl) -> pprx::observable<pprx::unit>;

    auto checkBlockCard(const_json_t waiting) -> pprx::observable<pprx::unit>;
    auto checkSessionEventCode() -> pprx::observable<pprx::unit>;
    void encryptReadRecords(json_t ioJson);

    auto isTrainingMode() const -> bool;
    auto isUseDigiSignInTrainingMode() const -> bool;
    auto trainingEmvRun(boost::shared_ptr<JMupsJobStorage> storage) -> pprx::observable<const_json_t>;
    auto trainingEmvRunLevel1(boost::shared_ptr<JMupsJobStorage> storage) -> pprx::observable<pprx::unit>;
    auto hanldeInputPinProc(const_json_t emvSession, bool bReinput=false) -> pprx::observable<pprx::unit>;

protected:
    auto emvRun(boost::shared_ptr<JMupsJobStorage> storage, const_json_t tradeStartResponse) -> pprx::observable<const_json_t>;
    auto emvRunLevel1(boost::shared_ptr<JMupsJobStorage> storage) -> pprx::observable<pprx::unit>;

    virtual auto emvOnCardPinMessage(const_json_t param)  -> pprx::observable<pprx::unit> = 0;
    virtual auto emvOnCardInputPlainOfflinePin(const_json_t param) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardInputEncryptedOfflinePin(const_json_t param) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardInputEncryptedOnlinePin(const_json_t param) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardTxPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardTxEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardGetIfdSerialNo() -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnNetwork(const std::string& url, const_json_t param) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiSelectApplication(const_json_t emvSession) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t> = 0;
    virtual auto emvWaitInputPreAmount() -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiAmount() -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiInputKid() -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiInputPayWay(const_json_t emvSession) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnTradeConfirmFinish(const_json_t emvTradeConfirm)  -> pprx::observable<pprx::unit> = 0;
    virtual auto emvOnTrainingCardReaderDisplayData() -> pprx::observable<const_json_t> = 0;
    virtual auto emvOnCardPinResultConfirm(const_json_t param)  -> pprx::observable<pprx::unit> = 0;
    
    virtual auto checkTradeFailureErrorCode(const_json_t tradeData, const std::string errorCode) -> bool = 0;
    virtual auto addMediaDivToCardNo(const_json_t tradeData) -> json_t = 0;
    
public:
            JMupsEmvKernel(Type type);
    virtual ~JMupsEmvKernel();
};

#endif /* !defined(__h_JMupsEmvKernel__) */

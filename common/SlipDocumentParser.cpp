//
//  SlipDocumentParser.cpp
//  ppsdk
//
//  Created by clementec on 2018/02/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipDocumentParser.h"
#include "LogManager.h"
#include "SlipPrinter.h"

SlipDocumentParser::SlipDocumentParser(const_json_t replaces) :
    m_processLine(0),
    m_replaces(replaces.clone()),
    m_exprVariable(R"**(\$\((\w+)\))**"),
    m_currentFont(SlipDocument::Font::Normal)
{
}

/* virtual */
SlipDocumentParser::~SlipDocumentParser()
{
}

SlipDocument::sp SlipDocumentParser::execute(std::istream& src)
{
    std::string lineText;
    while(std::getline(src, lineText)){
        m_processLine++;
        boost::algorithm::trim(lineText);
        tokenize(lineText);
    }
    
    auto sdoc = boost::make_shared<SlipDocument>();
    sdoc->setDocument(compile());
    return sdoc;
}

void SlipDocumentParser::tokenize(const std::string& src)
{
    auto _isspace = [](char ch) -> bool
    {
        return (ch == ' ') || (ch == '\t') || (ch == 0);
    };

    std::string::const_iterator it_begin;
    Token token;
    
    for(auto it = src.cbegin(); it != src.cend(); it++){
        const bool bLast = (it + 1) == src.cend();
        const char ch = *it;
        const char ch2 = bLast ? 0 : *(it + 1);
        switch(token.type){
            case Token::Type::Bad:
            {
                if((ch == '$') && (ch2 == '(')){            /* $FOO(xx) */
                    token.type = Token::Type::Variable;
                    it_begin = it;
                }
                else if((ch == '$') && (!_isspace(ch2))){   /* $FOO */
                    token.type = Token::Type::Opcode;
                    it_begin = it;
                }
                else if(ch == '{'){                         /* { */
                    token.type = Token::Type::ScopeIn;
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == '}'){                         /* } */
                    token.type = Token::Type::ScopeOut;
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == '\"'){                        /* "FOO" */
                    token.type = Token::Type::Text;
                    it_begin = it;
                }
                else if((ch == '_') && (ch2 == '_')){       /* __ */
                    token.type = Token::Type::Separator;
                    it_begin = it;
                }
                else if((ch == '_') && (ch2 != '_')){    /* _ */
                    token.type = Token::Type::EmptyText;
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == ';'){                        /* ; */
                    token.type = Token::Type::LineFeed;
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == '#'){                        /* #FOO */
                    token.type = Token::Type::Comment;
                    it_begin = it;
                }
                else if(_isspace(ch)){
                }
                else {
                    throwErrorInvalidChar(ch);
                }
                break;
            }
            case Token::Type::Variable:
            {
                if(ch == ')'){
                    token.text = std::string(it_begin, it + 1);
                    m_tokens.push_back(token);
                    token.clear();
                }
                break;
            }
            case Token::Type::Opcode:
            {
                if(ch == ')'){
                    token.text = std::string(it_begin, it + 1);
                    m_tokens.push_back(token);
                    token.clear();
                }
                break;
            }
            case Token::Type::ScopeIn:  { break; }
            case Token::Type::ScopeOut: { break; }
            case Token::Type::Text:
            {
                if(ch == '\"'){
                    token.text = std::string(it_begin + 1, it);
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == '\\'){    /* エスケープに対応　*/
                    it++;
                }
                break;
            }
            case Token::Type::Separator:
            {
                if(ch != '_'){
                    it--;   /* セパレータは終端文字がない為、'_'以外の文字で終端判定を行う。したがって、セパレータ終端はit--である。 */
                    token.text = std::string(it_begin, it + 1);
                    m_tokens.push_back(token);
                    token.clear();
                }
                else if(ch == '_'){
                }
                else{
                    throwErrorInvalidChar(ch);
                }
                break;
            }
            case Token::Type::EmptyText: { break; }
            case Token::Type::LineFeed: { break; }
            case Token::Type::Comment: { break; }
        }
    }
    if(token.type == Token::Type::Comment){
        /* success */
    }
    else if(token.type != Token::Type::Bad){
        token.text = std::string(it_begin, src.cend());
        m_tokens.push_back(token);
    }
}

void SlipDocumentParser::throwErrorInvalidChar(char ch)
{
    throwError(boost::format("invalid char '%c' at line %d") % ch % m_processLine);
}

SlipDocument::Document SlipDocumentParser::compile()
{
    m_currentFont = SlipDocument::Font::Normal;
    SlipDocument::Document  doc;
    SlipDocument::Tr        tr(SlipDocument::Font::Normal); /* 正しい値は trs.push_back時に指定する　*/
    auto token_it = m_tokens.cbegin();
    while(token_it != m_tokens.cend()){
        switch(token_it->type){
            case Token::Type::Text:         { tr.tds.push_back(doText(token_it)); break; }
            case Token::Type::Variable:     { tr.tds.push_back(doVariable(token_it)); break; }
            case Token::Type::Separator:    { tr.tds.push_back(doSeparator(token_it)); break; }
            case Token::Type::EmptyText:    { tr.tds.push_back(doEmptyText(token_it)); break; }
            case Token::Type::LineFeed:
            {
                treatForSingleTable(tr);
                SlipDocument::Table table;
                tr.font = m_currentFont;
                table.trs.push_back(tr);
                doc.tables.push_back(table);
                tr.tds.clear();
                token_it++;
                break;
            }
            case Token::Type::Opcode:
            {
                auto&& tables = doOpcode(token_it);
                if(!tables.empty()){
                    doc.tables.insert(doc.tables.end(), tables.begin(), tables.end());
                }
                break;
            }
            default:
            {
                throwError("compile() : 不正なトークンを検出");
                break;
            }
        }
    }
    
    return doc;
}

SlipDocument::Td SlipDocumentParser::doVariable(Token::iter_t& token_it)
{
    SlipDocument::Td td;
    td.type = SlipDocument::Td::Type::Text;
    td.body = extractVariable(token_it->text);
    token_it++;
    return td;
}

SlipDocument::Td SlipDocumentParser::doText(Token::iter_t& token_it)
{
    SlipDocument::Td td;
    td.type = SlipDocument::Td::Type::Text;
    td.body = extractVariable(token_it->text);
    token_it++;
    return td;
}

SlipDocument::Td SlipDocumentParser::doSeparator(Token::iter_t& token_it)
{
    SlipDocument::Td td;
    td.type = SlipDocument::Td::Type::Separator;
    token_it++;
    return td;
}

SlipDocument::Td SlipDocumentParser::doEmptyText(Token::iter_t& token_it)
{
    SlipDocument::Td td;
    td.type = SlipDocument::Td::Type::Text;
    token_it++;
    return td;
}


SlipDocument::Table::list_t SlipDocumentParser::doOpcode(Token::iter_t& token_it)
{
    SlipDocument::Table::list_t tables;
    
    boost::regex expr_FONT(R"**(\$FONT\((\w+)\))**");
    boost::regex expr_LOGO_IMAGE(R"**(\$LOGO_IMAGE\(\))**");
    boost::regex expr_TABLE(R"**(\$TABLE\(([A-Z0-9,:]+)\))**");
    boost::regex expr_FOREACH(R"**(\$FOREACH\((\w+)\))**");
    boost::regex expr_IF_EXIST(R"**(\$IF_EXIST\((\w+)\))**");
    boost::regex expr_IF_NOT_EXIST(R"**(\$IF_NOT_EXIST\((\w+)\))**");
    boost::regex expr_WRAPPED_TEXT(R"**(\$WRAPPED_TEXT\((\w+)\))**");

    boost::smatch m;
    if(boost::regex_match(token_it->text, m, expr_FONT)){
        tables.push_back(doOpcode_FONT(token_it, m.str(1)));
    }
    else if(boost::regex_match(token_it->text, m, expr_LOGO_IMAGE)){
        tables.push_back(doOpcode_LOGO_IMAGE(token_it, m.str(1)));
    }
    else if(boost::regex_match(token_it->text, m, expr_TABLE)){
        tables.push_back(doOpcode_TABLE(token_it, m.str(1)));
    }
    else if(boost::regex_match(token_it->text, m, expr_FOREACH)){
        tables = doOpcode_FOREACH(token_it, m.str(1));
    }
    else if(boost::regex_match(token_it->text, m, expr_IF_EXIST)){
        tables = doOpcode_IF_EXIST(token_it, m.str(1));
    }
    else if(boost::regex_match(token_it->text, m, expr_IF_NOT_EXIST)){
        tables = doOpcode_IF_NOT_EXIST(token_it, m.str(1));
    }
    else if(boost::regex_match(token_it->text, m, expr_WRAPPED_TEXT)){
        tables.push_back(doOpcode_WRAPPED_TEXT(token_it, m.str(1)));
    }
    else{
        throwError("不明なオペコードです。");
    }
    return tables;
}

SlipDocument::Table SlipDocumentParser::doOpcode_FONT(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $FONT(xxx) */
    
    if(param == "Normal"){
        m_currentFont = SlipDocument::Font::Normal;
    }
    else if(param == "DoubleSize_V"){
        m_currentFont = SlipDocument::Font::DoubleSize_V;
    }
    else if(param == "DoubleSize_H"){
        m_currentFont = SlipDocument::Font::DoubleSize_H;
    }
    else if(param == "DoubleSize_HV"){
        m_currentFont = SlipDocument::Font::DoubleSize_HV;
    }
    else{
        throwError("不明な文字設定です。");
    }

    return SlipDocument::Table();
}

SlipDocument::Table SlipDocumentParser::doOpcode_LOGO_IMAGE(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $LOGO_IMAGE() */
    
    SlipDocument::Table table;
    SlipDocument::Tr tr(m_currentFont);
    tr.tds.push_back(SlipDocument::Td::create(SlipDocument::Td::Type::LogoImage));
    table.trs.push_back(tr);
    return table;
}

SlipDocument::Table SlipDocumentParser::doOpcode_TABLE(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $TABLE(x:y,y,y:z,z,z) */
    
    /* $TABLEの次は必ずスコープが開いていなければならない */
    if(token_it->type != Token::Type::ScopeIn){
        throwError("'{'が見つかりません。");
    }
    token_it++;

    /* パラメータ処理 */
    std::vector<int>                        ratios;
    std::vector<SlipDocument::Alignment>    alignments;
    {
        std::vector<std::string> blocks;
        boost::split(blocks, param, boost::is_any_of(":"));
        
        if(blocks.size() != 3){
            throwError("パラメータが異常です。");
        }
        
        const int numCols = ::atoi(blocks[0].c_str());
        
        std::list<std::string> ratios_raw;
        std::list<std::string> alignments_raw;
        boost::split(ratios_raw     , blocks[1], boost::is_any_of(","));
        boost::split(alignments_raw , blocks[2], boost::is_any_of(","));

        if(numCols != ratios_raw.size()){
            throwError("サイズ記述子の個数が間違っています。");
        }
        if(numCols != alignments_raw.size()){
            throwError("アライメント記述子の個数が間違っています。");
        }
        
        for(auto d : ratios_raw){
            ratios.push_back(::atoi(d.c_str()));
        }

        for(auto d : alignments_raw){
            if(d == "C") alignments.push_back(SlipDocument::Alignment::Center);
            else if(d == "L") alignments.push_back(SlipDocument::Alignment::Left);
            else if(d == "R") alignments.push_back(SlipDocument::Alignment::Right);
            else{
                throwError("フォーマット記述子でサポートされていない文字が指定されています。");
            }
        }
    }
    const auto numColumns = ratios.size();
    
    SlipDocument::Table     table(SlipDocument::Table::Border::On);
    SlipDocument::Tr        tr(m_currentFont);
    bool bFinish = false;
    while(token_it != m_tokens.cend()){
        switch(token_it->type){
            case Token::Type::Text:         { tr.tds.push_back(doText(token_it)); break; }
            case Token::Type::Variable:     { tr.tds.push_back(doVariable(token_it)); break; }
            case Token::Type::Separator:    { tr.tds.push_back(doSeparator(token_it)); break; }
            case Token::Type::EmptyText:    { tr.tds.push_back(doEmptyText(token_it)); break; }
            case Token::Type::LineFeed:
            {
                if(tr.tds.size() != numColumns){
                    throwError("TRに含まれるtdの要素数が指定されたものと異なります。");
                }
                for(auto i = 0; i < tr.tds.size(); i++){
                    tr.tds[i].alignment = alignments[i];
                    tr.tds[i].ratio = ratios[i];
                }
                tr.font = m_currentFont;
                table.trs.push_back(tr);
                tr.tds.clear();
                token_it++;
                break;
            }
            case Token::Type::ScopeOut:
            {
                token_it++;
                bFinish = true;
                break;
            }
            default:
            {
                throwError("不正なトークンです。");
                break;
            }
        }
        if(bFinish) break;
    }
    
    /* ボーダ処理 */
    for(auto it = table.trs.begin(); it != table.trs.end(); it++){
        for(auto it2 = it->tds.begin(); it2 != it->tds.end(); it2++){
            if(it2 != it->tds.begin()){
                if((it2 - 1)->type == SlipDocument::Td::Type::Separator){
                    it2 = it->tds.insert(it2,SlipDocument::Td::create(SlipDocument::Td::Type::Separator_X));
                }
                else{
                    it2 = it->tds.insert(it2,SlipDocument::Td::create(SlipDocument::Td::Type::Separator_I));
                }
                it2++;
            }
        }
    }
    
    SlipDocument::Tr        tr_top(m_currentFont);
    SlipDocument::Tr        tr_bottom(m_currentFont);
    
    for(int i = 0; i < numColumns; i++){
        if(i != 0){
            tr_top.tds.push_back(SlipDocument::Td::create(SlipDocument::Td::Type::Separator_T));
            tr_bottom.tds.push_back(SlipDocument::Td::create(SlipDocument::Td::Type::Separator_rT));
        }
        auto hsep = SlipDocument::Td::create(SlipDocument::Td::Type::Separator);
        hsep.ratio = ratios[i];
        tr_top.tds.push_back(hsep);
        tr_bottom.tds.push_back(hsep);
    }

    table.trs.insert(table.trs.begin(), tr_top);
    table.trs.insert(table.trs.end(), tr_bottom);


    return table;
}


SlipDocument::Table::list_t SlipDocumentParser::doOpcode_FOREACH(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $FOREACH(foo) */

    /* $FOREACHの次は必ずスコープが開いていなければならない */
    if(token_it->type != Token::Type::ScopeIn){
        throwError("'{'が見つかりません。");
    }
    
    auto jarr = m_replaces.get_optional<json_t::array_t>(param);
    if(!jarr){
        throwError("配列が見つかりません。");
    }
    
    SlipDocument::Table::list_t result;

    if(jarr->size() == 0){
        doOpcode_scope_execute_or_skip(token_it, false);
    }
    else{
        token_it++;
        auto it_begin = token_it;   /* ループ用に保存する */
        auto org_replaces = m_replaces;
        for(auto arr_it = jarr->begin(); arr_it != jarr->end(); arr_it++){
            token_it = it_begin;
            m_replaces = *arr_it;
            SlipDocument::Tr        tr(m_currentFont);
            while(token_it != m_tokens.cend()){
                bool bFinish = false;
                switch(token_it->type){
                    case Token::Type::Text:         { tr.tds.push_back(doText(token_it)); break; }
                    case Token::Type::Variable:     { tr.tds.push_back(doVariable(token_it)); break; }
                    case Token::Type::Separator:    { tr.tds.push_back(doSeparator(token_it)); break; }
                    case Token::Type::EmptyText:    { tr.tds.push_back(doEmptyText(token_it)); break; }
                    case Token::Type::LineFeed:
                    {
                        treatForSingleTable(tr);
                        SlipDocument::Table table;
                        tr.font = m_currentFont;
                        table.trs.push_back(tr);
                        result.push_back(table);
                        tr.tds.clear();
                        token_it++;
                        break;
                    }
                    case Token::Type::Opcode:
                    {
                        auto&& tables = doOpcode(token_it);
                        if(!tables.empty()){
                            result.insert(result.end(), tables.begin(), tables.end());
                        }
                        break;
                    }
                    case Token::Type::ScopeOut:
                    {
                        token_it++;
                        bFinish = true;
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                if(bFinish) break;
            }
        }
        m_replaces = org_replaces;
    }
    
    return result;
}

SlipDocument::Table::list_t SlipDocumentParser::doOpcode_IF_EXIST(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $IF_EXIST(foo) */
    
    const bool bExist = [=](){
        auto value = m_replaces.get_optional(param);
        if(!value) return false;                    /* キーが存在しない */
        if(value->is_null_or_empty()) return false; /* キーが存在するがnull */
        if(value->type() == typeid(json_t::string_t)){
            /* 文字列の場合には、その文字列が空かどうかを判定する （これをやるかどうか悩むがJMupsの仕様であると便利なので） */
            return !(value->get<std::string>().empty());
        }
        return true;
    }();
    return doOpcode_scope_execute_or_skip(token_it, bExist);
}

SlipDocument::Table::list_t SlipDocumentParser::doOpcode_IF_NOT_EXIST(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $IF_EXIST(foo) */
    
    const bool bExist = [=](){
        auto value = m_replaces.get_optional(param);
        if(!value) return false;                    /* キーが存在しない */
        if(value->is_null_or_empty()) return false; /* キーが存在するがnull */
        if(value->type() == typeid(json_t::string_t)){
            /* 文字列の場合には、その文字列が空かどうかを判定する （これをやるかどうか悩むがJMupsの仕様であると便利なので） */
            return !(value->get<std::string>().empty());
        }
        return true;
    }();
    return doOpcode_scope_execute_or_skip(token_it, !bExist);
}

SlipDocument::Table::list_t SlipDocumentParser::doOpcode_scope_execute_or_skip(Token::iter_t& token_it, bool bExecute)
{
    /* この処理の最初はスコープが開いていなければならない */
    if(token_it->type != Token::Type::ScopeIn){
        throwError("'{'が見つかりません。");
    }
    token_it++;
    
    if(!bExecute){
        /* ScopeOutまでスキップして終了 */
        int nScope = 1;
        while(token_it != m_tokens.cend()){
            if(token_it->type == Token::Type::ScopeOut){
                nScope--;
            }
            else if(token_it->type == Token::Type::ScopeIn){
                nScope++;
            }
            token_it++;
            if(nScope == 0) break;
        }
        return SlipDocument::Table::list_t();
    }
    
    SlipDocument::Table::list_t result;
    SlipDocument::Tr        tr(m_currentFont);
    while(token_it != m_tokens.cend()){
        bool bFinish = false;
        switch(token_it->type){
            case Token::Type::Text:         { tr.tds.push_back(doText(token_it)); break; }
            case Token::Type::Variable:     { tr.tds.push_back(doVariable(token_it)); break; }
            case Token::Type::Separator:    { tr.tds.push_back(doSeparator(token_it)); break; }
            case Token::Type::EmptyText:    { tr.tds.push_back(doEmptyText(token_it)); break; }
            case Token::Type::LineFeed:
            {
                treatForSingleTable(tr);
                SlipDocument::Table table;
                tr.font = m_currentFont;
                table.trs.push_back(tr);
                result.push_back(table);
                tr.tds.clear();
                token_it++;
                break;
            }
            case Token::Type::Opcode:
            {
                auto&& tables = doOpcode(token_it);
                if(!tables.empty()){
                    result.insert(result.end(), tables.begin(), tables.end());
                }
                break;
            }
            case Token::Type::ScopeOut:
            {
                token_it++;
                bFinish = true;
                break;
            }
            default:
            {
                throwError("doOpcode_IF_EXIST() : 不正なトークンを検出");
                break;
            }
        }
        if(bFinish) break;
    }
    
    return result;
}



SlipDocument::Table SlipDocumentParser::doOpcode_WRAPPED_TEXT(Token::iter_t& token_it, const std::string& param)
{
    token_it++; /* $WRAPPED_TEXT(foo) */

    auto jstr = m_replaces.get_optional<json_t::string_t>(param);
    if(!jstr){
        /* 置き換え文字列と同様で、「あえて空白」を考慮してエラーではなく警告レベルとする */
        LogManager_AddWarningF("keyword not found \"%s\"", param);
        return SlipDocument::Table();
    }

    SlipDocument::Td td;
    td.body = *jstr;
    td.alignment = SlipDocument::Alignment::Left;
    td.type = SlipDocument::Td::Type::WrappedText;
    td.ratio = 1;
    
    SlipDocument::Tr tr(m_currentFont);
    tr.tds.push_back(td);
    
    SlipDocument::Table table;
    table.trs.push_back(tr);
    
    return table;
}


void SlipDocumentParser::treatForSingleTable(SlipDocument::Tr& tr) const
{
    if(tr.tds.empty()){
        SlipDocument::Td td;
        td.type         = SlipDocument::Td::Type::Text;
        td.alignment    = SlipDocument::Alignment::Left;
        tr.tds.push_back(td);
    }
    else{
        if(tr.tds.size() == 1){
            tr.tds.front().alignment = SlipDocument::Alignment::Left;
        }
        else if(tr.tds.size() == 2){
            tr.tds.front().alignment = SlipDocument::Alignment::Left;
            tr.tds.back().alignment = SlipDocument::Alignment::Right;
        }
        else if(tr.tds.size() >= 3){
            tr.tds.front().alignment = SlipDocument::Alignment::Left;
            tr.tds.back().alignment = SlipDocument::Alignment::Right;
            for(auto i = 1; i < tr.tds.size() - 1; i++){
                tr.tds[i].alignment = SlipDocument::Alignment::Center;
            }
        }
    }
    for(auto it = tr.tds.begin(); it != tr.tds.end(); it++){
        it->ratio = 0;
    }
}



void SlipDocumentParser::throwError(const std::string& src)
{
    LogManager_AddError(src);
    throw error(src);
}

void SlipDocumentParser::throwError(const boost::format& fmt)
{
    throwError(fmt.str());
}

std::string SlipDocumentParser::extractVariable(const std::string& src) const
{
    std::stringstream ss;
    auto s_begin = src.cbegin();
    auto s_end = src.cend();
    while(true){
        boost::smatch m;
        if(boost::regex_search(s_begin, s_end, m, m_exprVariable)){
            auto k = m.str(1);
            auto v = m_replaces.get_optional<std::string>(k);
            if(v){
                ss << std::string(s_begin, s_begin + m.position());
                ss << *v;
            }
            else{
                /* あえて空白」を考慮してエラーではなく警告レベルとする */
                LogManager_AddWarningF("keyword not found \"%s\" at line %d", k % m_processLine);
            }
            s_begin += m.position() + m.length();
        }
        else{
            ss << std::string(s_begin, s_end);
            break;
        }
    }
    return ss.str();
}

#if defined(DEBUG)

#include "AbstructFactory.h"

/* static */
void SlipDocumentParser::test()
{
#if 0

    std::string test = u8R"***(
    $TABLE(3:1,1,1:C,C,C){
        "伝票番号"   "有効期限"     "承認番号";
        $(SLIP_NO)  $(EXP_DATE)  $(A_NO);
        __ __ __;
        "取引区分"   "支払区分"     "商品区分";
        $(TRADE)  $(PAY_DIVISION)  $(PRODUCT_DIVISION);
    }
    )***";
    
#elif 1
    std::string test = u8R"***(
# クレジット売上
    $IF_EXIST(_DCC){
        $FONT(DoubleSize_HV)
        "DOUBLE SIZE?";
        $FONT(Normal)
    }
    )***";

#else
    std::string test = u8R"***(
    # クレジット売上
    $FONT(DoubleSize_HV)
    $LOGO_IMAGE()#comment
    _ $(TITLE) _;
    $FONT(Normal)
    __;
    "加盟店"              $(MERCHANT_NAME);
    _                    $(MERCHANT_TELNO);
    __;
    "ｶｰﾄﾞ会社"            $(CARD_CO);
    "ｶｰﾄﾞ番号"            $(CARD_NO);
    "端末番号"            $(TERM_NO);
    $TABLE(3:1,1,1:C,C,C){
        "伝票番号"   "有効期限"     "承認番号";
        $(SLIP_NO)  $(EXP_DATE)  $(A_NO);
        __ __ __;
        "取引区分"   "支払区分"     "商品区分";
        $(TRADE)  $(PAY_DIVISION)  $(PRODUCT_DIVISION);
    }
    $FOREACH(TEST_ARRAY){
        "AA" $(AA);
        "BB" $(BB);
        _ $(CC);
    }
    $IF_EXIST(TEST_KEY){
        __;
        $WRAPPED_TEXT(LONGTEXT)
        ;
         $(TEST_KEY) $(TEST_KEY) $(TEST_KEY);
        ;
        __;
    }
    "金額"    $(AMOUNT);
    _______________;
    $FONT(DoubleSize_V)
    "合計金額"  $(TOTAL_YEN);
    $FONT(Normal)
    ___ ;
    $IF_EXIST(_DCC){
        $IF_EXIST(_VISA){
            
        }
        $FONT(DoubleSize_V)
        "DOUBLE FONT";
        $FONT(Normal)
    }
    
    
    $WRAPPED_TEXT(LONGTEXT)
    ___ ;
    "ARC:$(ARC)"    $(RECEIPT);
    )***";
#endif
    
    json_t j;
    j.put("TITLE", "クレジットカード売上票");
    j.put("MERCHANT_NAME", "ｽﾏﾎﾃｽﾄ");
    j.put("MERCHANT_TELNO", "78-901-23456");
    j.put("CARD_CO", "JCB GROUP");
    j.put("CARD_NO", "IC 374245XXXXX0001");
    j.put("TERM_NO", "49693-808-00178");
    j.put("SLIP_NO", "03140");
    j.put("EXP_DATE", "XX/XX");
    j.put("A_NO", "010981");
    j.put("TRADE", "売上");
    j.put("PAY_DIVISION", "一括");
    j.put("PRODUCT_DIVISION", "0990");
    j.put("AMOUNT", "\\3,333");
    j.put("TOTAL_YEN", "\\3,344");
    j.put("ARC", "00");
    j.put("RECEIPT", "カード会社控");
    
    
    json_t::array_t arr;
    json_t jj1;
    jj1.put("AA", "AA1");
    jj1.put("BB", "BB1");
    jj1.put("CC", "CC1");
    json_t jj2;
    jj2.put("AA", "AA2");
    jj2.put("BB", "BB2");
    jj2.put("CC", "CC2");
    arr.push_back(jj1);
    arr.push_back(jj2);
    j.put("TEST_ARRAY", arr);
    
    j.put("TEST_KEY", "てすと〜");
    j.put("LONGTEXT", "ご利用ありがとうございまし〜〜た。またのご来店をお待ちしております。");

    j.put("_DCC", true);
    
    std::stringstream ss(test);

    SlipDocumentParser parser(j);

    auto doc = parser.execute(ss);
    
    std::stringstream html;
    doc->getDocument().serialize(html);
    LogManager_AddDebug(html.str());
    
    auto printer = AbstructFactory::instance().createSlipPrinter();
    printer->openDocument()
    .flat_map([=](auto){
        return printer->openPage();
    }).as_dynamic()
    .flat_map([=](auto){
        return printer->printPage(doc);
    }).as_dynamic()
    .flat_map([=](auto){
        return printer->closePage();
    }).as_dynamic()
    .flat_map([=](auto){
        return printer->closeDocument();
    }).as_dynamic()
    .publish()
    .connect();}

#endif


//
//  JobNFCMasterDataUpdateJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCMasterDataUpdateJMups.h"

JobNFCMasterDataUpdateJMups::JobNFCMasterDataUpdateJMups(const __secret& s) :
    PaymentJob(s)
{
    LogManager_Function();
}

/* virtual */
JobNFCMasterDataUpdateJMups::~JobNFCMasterDataUpdateJMups()
{
    LogManager_Function();
}

/* virtual */
auto JobNFCMasterDataUpdateJMups::getObservableSelf()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return waitForReaderConnect().as_dynamic()
    .flat_map([=](auto){
        return masterDataDownload(true).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return masterDataUpdate(getCardReader()).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        json_t j;
        j.put("status", "success");
        return j.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobNFCMasterDataUpdateJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}


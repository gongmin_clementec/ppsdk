//
//  JobSettingTraining.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingTraining__)
#define __h_JobSettingTraining__

#include "PaymentJob.h"

class JobSettingTraining :
    public PaymentJob
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;

public:
            JobSettingTraining(const __secret& s);
    virtual ~JobSettingTraining();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobSettingTraining__) */

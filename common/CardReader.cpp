//
//  CardReader.cpp
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "CardReader.h"
#include "LogManager.h"

CardReader::CardReader(const __secret& s) :
    LazyObject(s),
    m_subjectDeviceConnectState(DeviceConnectState::Bad),
    m_subjectICCardSlotState(ICCardSlotState::Bad)
{
    LogManager_Function();
}

/* virtual */
CardReader::~CardReader()
{
    LogManager_Function();
}

auto CardReader::finalize() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    subscriberICCardInserted().on_completed();
    return finalizeSelf();
}

void CardReader::prepareObservables()
{
    LogManager_Function();

    m_subjectICCardInserted     = pprx::subject<const_json_t>();

    prepareObservablesSelf();
}

std::string CardReader::deviceConnectStateToString(DeviceConnectState state) const
{
    switch(state){
        case DeviceConnectState::Connecting:     return "Connecting";
        case DeviceConnectState::Connected:      return "Connected";
        case DeviceConnectState::Disconnecting:  return "Disconnecting";
        case DeviceConnectState::Disconnected:   return "Disconnected";
        case DeviceConnectState::Bad:            return "Bad";
        default:
            break;
    }
    return "(n/a)";
}

void CardReader::setDeviceConnectState(DeviceConnectState state)
{
    LogManager_AddInformationF("connection state change -> %s", deviceConnectStateToString(state));
    m_subjectDeviceConnectState.get_subscriber().on_next(state);
}

void CardReader::setICCardSlotState(ICCardSlotState state)
{
    auto _state = [](ICCardSlotState s){
        switch(s){
            case ICCardSlotState::Inserted:
                return "Inserted";
            case ICCardSlotState::Removed:
                return "Removed";
            case ICCardSlotState::Bad:
                return "Bad";
            default:
                break;
        }
        return "unknown";
    };
    LogManager_AddInformationF("slot state changed -> %s", _state(state));
    m_subjectICCardSlotState.get_subscriber().on_next(state);
}



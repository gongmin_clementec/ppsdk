//
//  JobUnionPayRefundJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobUnionPayRefundJMups__)
#define __h_JobUnionPayRefundJMups__

#include "JobUnionPaySalesJMups.h"

class JobUnionPayRefundJMups :
    public JobUnionPaySalesJMups
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual void beginAsyncPreInputAmount();

    auto sendTradeConfirm(boost::optional<std::string> authenticationNo)  -> pprx::observable<JMupsAccess::response_t>;
    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;
    auto waitForStoreTradeResultData(JMupsAccess::response_t jmupsData) -> pprx::observable<prmUnionPay::doubleTrade>;
    auto processCommon() -> pprx::observable<pprx::unit>;
    
protected:
    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto proceedContact(const_json_t data)  -> pprx::observable<pprx::unit>;
    virtual auto emvWaitInputPreAmount() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    
public:
            JobUnionPayRefundJMups(const __secret& s);
    virtual ~JobUnionPayRefundJMups();
};

#endif /* !defined(__h_JobUnionPayRefundJMups__) */

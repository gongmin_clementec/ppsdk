//
//  JobNFCMasterDataUpdateJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobNFCMasterDataUpdateJMups__)
#define __h_JobNFCMasterDataUpdateJMups__

#include "PaymentJob.h"
#include "JMupsAccessNFCWithMasterDataTool.h"

class JobNFCMasterDataUpdateJMups :
    public PaymentJob,
    public JMupsAccessNFCWithMasterDataTool
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobNFCMasterDataUpdateJMups(const __secret& s);
    virtual ~JobNFCMasterDataUpdateJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCMasterDataUpdateJMups__) */


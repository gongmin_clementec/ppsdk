//
//  JMupsAAccessNFC.cpp
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessNFC.h"

JMupsAccessNFC::JMupsAccessNFC()
{
    
}

/* virtual */
JMupsAccessNFC::~JMupsAccessNFC()
{
    
}


/* static */
JMupsAccessNFC::prmNFCCommon::cardBrandIdentifier
    JMupsAccessNFC::prmNFCCommon::stringToCardBrandIdentifier(const std::string& str)
{
    const std::map<std::string, prmNFCCommon::cardBrandIdentifier> cardBrandMap
    {
        {"VISA"     , JMupsAccessNFC::prmNFCCommon::cardBrandIdentifier::VISA},
        {"MASTER"   , JMupsAccessNFC::prmNFCCommon::cardBrandIdentifier::MASTER},
        {"AMEX"     , JMupsAccessNFC::prmNFCCommon::cardBrandIdentifier::AMEX},
        {"JCB"      , JMupsAccessNFC::prmNFCCommon::cardBrandIdentifier::JCB}
    };
    auto it = cardBrandMap.find(str);
    if(it == cardBrandMap.end()){
        return prmNFCCommon::cardBrandIdentifier::Bad;
    }
    return it->second;
}

auto JMupsAccessNFC::hpsMasterDataDownload
(
 prmNFCCommon::nfcDeviceType            nfcDeviceType,
 prmMasterDataDownload::downloadType    downloadType
) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });
    
    setJsonParam
    (
     json, "downloadType", downloadType,
     {
         {prmMasterDataDownload::downloadType::Full         , "1"},
         {prmMasterDataDownload::downloadType::Difference   , "2"}
     });
    
    if(isTrainingMode()){
        return callTraining("xAA01.do?xAA0101=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA01.do?xAA0101=aaa", json);
    }
}


auto JMupsAccessNFC::hpsUpdateMasterDataDate
(
 prmNFCCommon::nfcDeviceType    nfcDeviceType,
 boost::optional<std::string>   kernelCommonUpdateDate,
 boost::optional<std::string>   kernelC1UpdateDate,
 boost::optional<std::string>   kernelC2UpdateDate,
 boost::optional<std::string>   kernelC3UpdateDate,
 boost::optional<std::string>   kernelC4UpdateDate,
 boost::optional<std::string>   kernelC5UpdateDate,
 boost::optional<std::string>   icCardTableUpdateDate,
 boost::optional<std::string>   caKeyFile1UpdateDate,
 boost::optional<std::string>   merchantTableUpdateDate
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });
    
    setJsonParam(json, "kernelCommonUpdateDate" , kernelCommonUpdateDate);
    setJsonParam(json, "kernelC1UpdateDate"     , kernelC1UpdateDate);
    setJsonParam(json, "kernelC2UpdateDate"     , kernelC2UpdateDate);
    setJsonParam(json, "kernelC3UpdateDate"     , kernelC3UpdateDate);
    setJsonParam(json, "kernelC4UpdateDate"     , kernelC4UpdateDate);
    setJsonParam(json, "icCardTableUpdateDate"  , icCardTableUpdateDate);
    setJsonParam(json, "caKeyFile1UpdateDate"   , caKeyFile1UpdateDate);
    setJsonParam(json, "merchantTableUpdateDate", merchantTableUpdateDate);
    
    if(isTrainingMode()){
        return callTraining("xAA01.do?xAA0102=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA01.do?xAA0102=aaa", json);
    }
}

auto JMupsAccessNFC::hpsNFCRead
(
 prmNFCCommon::nfcDeviceType        nfcDeviceType,
 prmNFCCommon::resultCode           resultCode,
 const std::string&                 resultCodeExtended,
 const std::string&                 transactionResultBit,
 prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
 const std::string&                 nfcTradeResult,
 const std::string&                 outcomeParameter,
 boost::optional<std::string>       nfcTradeResultExtended,
 boost::optional<std::string>       keySerialNoForResult,
 boost::optional<std::string>       keySerialNoForExtended
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });
    
    setJsonParam
    (
     json, "resultCode", resultCode,
     {
         {prmNFCCommon::resultCode::Success     , "00"}
     });
    
    setJsonParam(json, "resultCodeExtended"     , resultCodeExtended);
    setJsonParam(json, "transactionResultBit"   , transactionResultBit);
    
    setJsonParam
    (
     json, "cardBrandIdentifier", cardBrandIdentifier,
     {
         {prmNFCCommon::cardBrandIdentifier::VISA     , "00"},
         {prmNFCCommon::cardBrandIdentifier::MASTER   , "01"},
         {prmNFCCommon::cardBrandIdentifier::AMEX     , "02"},
         {prmNFCCommon::cardBrandIdentifier::JCB      , "03"}
     });
    
    setJsonParam(json, "nfcTradeResult"         , nfcTradeResult);
    setJsonParam(json, "outcomeParameter"       , outcomeParameter);
    setJsonParam(json, "nfcTradeResultExtended" , nfcTradeResultExtended);
    setJsonParam(json, "keySerialNoForResult"   , keySerialNoForResult);
    setJsonParam(json, "keySerialNoForExtended" , keySerialNoForExtended);
    
    setCookie("tradeState", "1");
    setCookie("printResult", "false");

    if(isTrainingMode()){
        return callTraining("xAA04.do?xAA0401=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA04.do?xAA0401=aaa", json);
    }
}

auto JMupsAccessNFC::hpsNFCSales_Confirm
(
 prmNFCSales_Confirm::selector      selector,
 prmNFCCommon::nfcDeviceType        nfcDeviceType,
 prmNFCCommon::resultCode           resultCode,
 const std::string&                 resultCodeExtended,
 int                                amount,
 int                                taxOtherAmount,
 const std::string&                 productCode,
 prmNFCCommon::payWayDiv            payWayDiv,
 bool                               isRePrint,
 const std::string&                 transactionResultBit,
 prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
 const std::string&                 nfcTradeResult,
 const std::string&                 outcomeParameter,
 boost::optional<std::string>       nfcTradeResultExtended,
 boost::optional<std::string>       keySerialNoForResult,
 boost::optional<std::string>       keySerialNoForExtended,
 boost::optional<int>               couponSelectListNum,
 boost::optional<int>               couponApplyPreviousAmount,
 boost::optional<int>               point,
 boost::optional<int>               exchangeablePoint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });
    
    setJsonParam
    (
     json, "resultCode", resultCode,
     {
         {prmNFCCommon::resultCode::Success     , "00"}
     });
    
    setJsonParam(json, "resultCodeExtended"     , resultCodeExtended);
    setJsonParam(json, "amount"                 , amount);
    setJsonParam(json, "taxOtherAmount"         , taxOtherAmount);
    setJsonParam(json, "productCode"            , productCode);

    setJsonParam
    (
     json, "payWayDiv", payWayDiv,
     {
         {prmNFCCommon::payWayDiv::Lump     , "0"}
     });


    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "transactionResultBit", transactionResultBit);
    setJsonParam
    (
     json, "cardBrandIdentifier", cardBrandIdentifier,
     {
         {prmNFCCommon::cardBrandIdentifier::VISA     , "00"},
         {prmNFCCommon::cardBrandIdentifier::MASTER   , "01"},
         {prmNFCCommon::cardBrandIdentifier::AMEX     , "02"},
         {prmNFCCommon::cardBrandIdentifier::JCB      , "03"}
     });

    setJsonParam(json, "nfcTradeResult"             , nfcTradeResult);
    setJsonParam(json, "outcomeParameter"           , outcomeParameter);
    setJsonParam(json, "nfcTradeResultExtended"     , nfcTradeResultExtended);
    setJsonParam(json, "keySerialNoForResult"       , keySerialNoForResult);
    setJsonParam(json, "keySerialNoForExtended"     , keySerialNoForExtended);
    setJsonParam(json, "couponSelectListNum"        , couponSelectListNum);
    setJsonParam(json, "couponApplyPreviousAmount"  , couponApplyPreviousAmount);
    setJsonParam(json, "point"                      , point);
    setJsonParam(json, "exchangeablePoint"          , exchangeablePoint);
    
    std::string url;
    switch(selector){
        case prmNFCSales_Confirm::selector::Normal:
            url = "xAA0201.do?xAA020101=aaa";
            break;
        case prmNFCSales_Confirm::selector::Double:
            url = "xAA0201.do?xAA020103=aaa";
            break;
        default:
            break;
    }
    
    setCookie("tradeState", "2");
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();
        
        reps["##AMOUNT##"]       = integerToYenStringInJson(amount);
        reps["##TAX##"]          = integerToYenStringInJson(taxOtherAmount);
        reps["##TOTAL_AMOUNT##"] = integerToYenStringInJson(amount + taxOtherAmount);
        reps["##PRODUCT_CODE##"] = productCode;
        reps["##digiReceiptDiv##"] = isUseDigiSignInTrainingMode() ? "1" : "0";
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessNFC::hpsNFCSales_Tagging
(
 const std::string&                     slipNo,
 prmNFCCommon::printResultDiv           printResultDiv,
 bool                                   isRePrint,
 boost::optional<prmNFCCommon::isSendingDegiSlip>               isSendingDegiSlip,
 boost::optional<prmNFCSales_Tagging::issureScriptResultDiv>    issureScriptResultDiv,
 boost::optional<prmNFCCommon::nfcDeviceType>                   nfcDeviceType,
 boost::optional<std::string>       nfcTradeResult,
 boost::optional<std::string>       nfcTradeResultExtended,
 boost::optional<std::string>       keySerialNoForResult,
 boost::optional<std::string>       keySerialNoForExtended
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmNFCCommon::printResultDiv::Success , "2"},
                     {prmNFCCommon::printResultDiv::Failure , "3"}
                 });
    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "isSendingDegiSlip", isSendingDegiSlip,
                 {
                     {prmNFCCommon::isSendingDegiSlip::ShoudBeSending   , "2"},
                     {prmNFCCommon::isSendingDegiSlip::Unnecessarily    , "3"}
                 });
    setJsonParam(json, "issureScriptResultDiv", issureScriptResultDiv,
                 {
                     {prmNFCSales_Tagging::issureScriptResultDiv::Unexecuted    , "0"},
                     {prmNFCSales_Tagging::issureScriptResultDiv::Success       , "1"},
                     {prmNFCSales_Tagging::issureScriptResultDiv::Failure       , "2"},
                 });
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });

    
    setJsonParam(json, "nfcTradeResult", nfcTradeResult);
    setJsonParam(json, "nfcTradeResultExtended", nfcTradeResultExtended);
    setJsonParam(json, "keySerialNoForResult", keySerialNoForResult);
    setJsonParam(json, "keySerialNoForExtended", keySerialNoForExtended);
    
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    if(isTrainingMode()){
        return callTraining("xAA0201.do?xAA020102=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA0201.do?xAA020102=aaa", json);
    }
}

auto JMupsAccessNFC::hpsNFCSearchTrade
(
 const std::string& slipNo,
 bool               isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", isRePrint);
    
    return callHps("xAA03.do?xAA0301=aaa", json);
}

auto JMupsAccessNFC::hpsNFCSearchTradeWithCard
(
 prmNFCCommon::nfcDeviceType        nfcDeviceType,
 prmNFCCommon::resultCode           resultCode,
 const std::string&                 resultCodeExtended,
 const std::string&                 slipNo,
 int                                amount,
 int                                taxOtherAmount,
 const std::string&                 productCode,
 prmNFCCommon::payWayDiv            payWayDiv,
 bool                               isRePrint,
 const std::string&                 transactionResultBit,
 prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
 const std::string&                 nfcTradeResult,
 const std::string&                 outcomeParameter,
 boost::optional<std::string>       nfcTradeResultExtended,
 boost::optional<std::string>       keySerialNoForResult,
 boost::optional<std::string>       keySerialNoForExtended
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });
    
    setJsonParam
    (
     json, "resultCode", resultCode,
     {
         {prmNFCCommon::resultCode::Success     , "00"}
     });
    
    setJsonParam(json, "resultCodeExtended"     , resultCodeExtended);
    setJsonParam(json, "slipNo"                 , slipNo);
    setJsonParam(json, "amount"                 , amount);
    setJsonParam(json, "taxOtherAmount"         , taxOtherAmount);
    setJsonParam(json, "productCode"            , productCode);
    
    setJsonParam
    (
     json, "payWayDiv", payWayDiv,
     {
         {prmNFCCommon::payWayDiv::Lump     , "0"}
     });
    
    
    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "transactionResultBit", transactionResultBit);
    setJsonParam
    (
     json, "cardBrandIdentifier", cardBrandIdentifier,
     {
         {prmNFCCommon::cardBrandIdentifier::VISA     , "00"},
         {prmNFCCommon::cardBrandIdentifier::MASTER   , "01"},
         {prmNFCCommon::cardBrandIdentifier::AMEX     , "02"},
         {prmNFCCommon::cardBrandIdentifier::JCB      , "03"}
     });
    
    setJsonParam(json, "nfcTradeResult"             , nfcTradeResult);
    setJsonParam(json, "outcomeParameter"           , outcomeParameter);
    setJsonParam(json, "nfcTradeResultExtended"     , nfcTradeResultExtended);
    setJsonParam(json, "keySerialNoForResult"       , keySerialNoForResult);
    setJsonParam(json, "keySerialNoForExtended"     , keySerialNoForExtended);
    
    if(isTrainingMode()){
        return callTraining("xAA03.do?xAA0302=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA03.do?xAA0302=aaa", json);
    }
}



auto JMupsAccessNFC::hpsNFCRefund
(
 prmNFCCommon::nfcDeviceType        nfcDeviceType,
 prmNFCRefund::undoDiv              undoDiv,
 prmNFCCommon::resultCode           resultCode,
 const std::string&                 resultCodeExtended,
 const std::string&                 slipNo,
 int                                amount,
 int                                taxOtherAmount,
 const std::string&                 productCode,
 prmNFCCommon::payWayDiv            payWayDiv,
 bool                               isRePrint,
 const std::string&                 transactionResultBit,
 prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
 const std::string&                 nfcTradeResult,
 const std::string&                 outcomeParameter,
 boost::optional<std::string>       nfcTradeResultExtended,
 boost::optional<std::string>       keySerialNoForResult,
 boost::optional<std::string>       keySerialNoForExtended
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam
    (
     json, "nfcDeviceType", nfcDeviceType,
     {
         {prmNFCCommon::nfcDeviceType::Panasonic   , "1"},
         {prmNFCCommon::nfcDeviceType::Miura       , "2"}
     });

    setJsonParam
    (
     json, "undoDiv", undoDiv,
     {
         {prmNFCRefund::undoDiv::Undo           , "0"},
         {prmNFCRefund::undoDiv::ReturningGoods , "1"}
     });

    setJsonParam
    (
     json, "resultCode", resultCode,
     {
         {prmNFCCommon::resultCode::Success     , "00"}
     });
    
    setJsonParam(json, "resultCodeExtended"     , resultCodeExtended);
    setJsonParam(json, "slipNo"                 , slipNo);
    setJsonParam(json, "amount"                 , amount);
    setJsonParam(json, "taxOtherAmount"         , taxOtherAmount);
    setJsonParam(json, "productCode"            , productCode);
    
    setJsonParam
    (
     json, "payWayDiv", payWayDiv,
     {
         {prmNFCCommon::payWayDiv::Lump     , "0"}
     });
    
    
    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "transactionResultBit", transactionResultBit);
    setJsonParam
    (
     json, "cardBrandIdentifier", cardBrandIdentifier,
     {
         {prmNFCCommon::cardBrandIdentifier::VISA     , "00"},
         {prmNFCCommon::cardBrandIdentifier::MASTER   , "01"},
         {prmNFCCommon::cardBrandIdentifier::AMEX     , "02"},
         {prmNFCCommon::cardBrandIdentifier::JCB      , "03"}
     });
    
    setJsonParam(json, "nfcTradeResult"             , nfcTradeResult);
    setJsonParam(json, "outcomeParameter"           , outcomeParameter);
    setJsonParam(json, "nfcTradeResultExtended"     , nfcTradeResultExtended);
    setJsonParam(json, "keySerialNoForResult"       , keySerialNoForResult);
    setJsonParam(json, "keySerialNoForExtended"     , keySerialNoForExtended);
    
    setCookie("tradeState", "2");
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();

        reps["##AMOUNT##"]       = integerToYenStringInJson(amount);
        reps["##TAX##"]          = integerToYenStringInJson(taxOtherAmount);
        reps["##TOTAL_AMOUNT##"] = integerToYenStringInJson(amount + taxOtherAmount);
        reps["##PRODUCT_CODE##"] = productCode;
        reps["##digiReceiptDiv##"] = isUseDigiSignInTrainingMode() ? "1" : "0";
        return callTraining("xAA0304.do?xAA030401=aaa", reps);
    }
    else{
        return callHps("xAA0304.do?xAA030401=aaa", json);
    }
}



auto JMupsAccessNFC::hpsNFCRefund_Tagging
(
 const std::string&             slipNo,
 bool                           isRePrint,
 prmNFCCommon::printResultDiv   printResultDiv,
 boost::optional<prmNFCCommon::isSendingDegiSlip>   isSendingDegiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmNFCCommon::printResultDiv::Success , "2"},
                     {prmNFCCommon::printResultDiv::Failure , "3"}
                 });
    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "isSendingDegiSlip", isSendingDegiSlip,
                 {
                     {prmNFCCommon::isSendingDegiSlip::ShoudBeSending   , "2"},
                     {prmNFCCommon::isSendingDegiSlip::Unnecessarily    , "3"}
                 });
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    if(isTrainingMode()){
        return callTraining("xAA0304.do?xAA030402=aaa", keyvalue_t());
    }
    else{
        return callHps("xAA0304.do?xAA030402=aaa", json);
    }
}






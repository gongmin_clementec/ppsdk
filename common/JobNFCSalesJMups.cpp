//
//  JobNFCSalesJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCSalesJMups.h"
#include "PPConfig.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"
#include "TMSAccess.h"

JobNFCSalesJMups::JobNFCSalesJMups(const __secret& s) :
    PaymentJob(s)
    ,m_onlineProcessing(false)
{
    LogManager_Function();
}

/* virtual */
JobNFCSalesJMups::~JobNFCSalesJMups()
{
    LogManager_Function();
}

/* virtual */
auto JobNFCSalesJMups::getObservableSelf()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return waitForReaderConnect().as_dynamic()
    .flat_map([=](auto){
        return processMasterData().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return processCardJob();
    }).as_dynamic()
     
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
   
    .flat_map([=](auto){
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging().as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return json.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobNFCSalesJMups::addMediaDivToCardNo(const_json_t tradeData) -> json_t
{
    LogManager_Function();
    
    if(PaymentJob::isTrainingMode()){
        json_t resp = tradeData.clone();
        auto pinfo = tradeData.get("normalObject.printInfo.device.value").clone();
        auto values = pinfo.get<json_t::array_t>("values");
        /* カード番号CLを追加*/
        for(auto v : values){
            auto cardNo   = v.get_optional<std::string>("CARD_NO");
            auto cardNoWithMediaDiv = "CL " + *cardNo;
            v.put("CARD_NO", cardNoWithMediaDiv);
        }

        /* printInfoのvalueを再構築 */
        pinfo.put("values", values);
        resp.put("normalObject.printInfo.device.value", pinfo);
        
        getJMupsStorage()->setTradeResultData(resp.as_readonly());
        return resp;
    }
    
    getJMupsStorage()->setTradeResultData(tradeData);
    return tradeData.clone();
}

auto JobNFCSalesJMups::treatSlipDocumentJson()
-> pprx::observable<const_json_t>
{
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto pinfo = tradeResultData.get("normalObject.printInfo.device.value").clone();
    auto values = pinfo.get<json_t::array_t>("values");
    auto types = pinfo.get<json_t::array_t>("type");
    
    /* 税・その他が不要な場合削除する　*/
    {
        auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
        if(!taxOtherAmount){
            for(auto v : values){
                v.remove_key_if_exist("TAX");
            }
        }
    }
    
    /* カード会社控えが不要な場合削除する。必要なら、サインする */
    {
        auto isNeedAquirerSlip = tradeResultData.get_optional<bool>("ppsdk.isNeedAquirerSlip");
        if(isNeedAquirerSlip){
            if(!(*isNeedAquirerSlip)){
                for(int n = 0; n < values.size(); n++){
                    if(values[n].get<std::string>("RECEIPT_DIV") == "4"){
                        values.erase(values.begin() + n);
                        types.erase(types.begin() + n);
                        break;
                    }
                }
            }
            else{ // 手書きサイン
                for(auto v : values){
                    auto sign = v.get_optional<std::string>("SIGN");
                    if(sign){
                        v.put("SIGN", "1");
                    }
                }
            }
        }
    }
    
    /* クーポン情報の埋め込み */
    {
        auto&& tradeResult = tradeResultData.get("normalObject.tradeResult");
        auto COUPON_ID = tradeResult.get_optional<std::string>("COUPON_ID");
        if(COUPON_ID && (!COUPON_ID->empty())){
            auto SETTLEDAMOUNT = ::atoi(tradeResult.get<std::string>("SETTLEDAMOUNT").c_str());
            auto DISCOUNT_PRICE = tradeResult.get<int>("DISCOUNT_PRICE");
            auto AMOUNT = std::string("\\") + BaseSystem::commaSeparatedString(SETTLEDAMOUNT + DISCOUNT_PRICE);
            auto strDISCOUNT_PRICE = std::string("-\\") + BaseSystem::commaSeparatedString(::abs(DISCOUNT_PRICE));
            
            auto COUPON_MEMBER_ID       = tradeResult.get_optional("COUPON_MEMBER_ID");
            auto COUPON_POSS_TIMES      = tradeResult.get_optional("MAXIMUM_USE_POSS_TIMES");
            auto COUPON_DONE_TIMES      = tradeResult.get_optional("USE_DONE_TIMES");
            auto COUPON_1               = tradeResult.get_optional<std::string>("COUPON_1");
            auto COUPON_2               = tradeResult.get_optional<std::string>("COUPON_2");
            
            auto COUPON_TEXT = [=](){
                std::stringstream ss;
                if(COUPON_1) ss << (*COUPON_1);
                if(COUPON_2){
                    if(COUPON_1) ss << "\n";
                    ss << (*COUPON_2);
                }
                return ss.str();
            }();
            
            for(auto v : values){
                v.put("_COUPON", true);
                v.put("_COUPON_ID"       , (*COUPON_ID));
                v.put("AMOUNT"           , AMOUNT);    /* 元のAMOUNTを上書き */
                v.put("_DISCOUNT_PRICE"  , strDISCOUNT_PRICE);
                if(COUPON_MEMBER_ID && (!COUPON_MEMBER_ID->is_null_or_empty())){
                    auto member_id = COUPON_MEMBER_ID->get<std::string>();
                    if(!member_id.empty()){
                        v.put("_COUPON_MEMBER_ID", member_id);
                    }
                }
                if(COUPON_POSS_TIMES && (!COUPON_POSS_TIMES->is_null_or_empty())){
                    auto poss_times = COUPON_POSS_TIMES->get<int>();
                    if(poss_times > 0){
                        v.put("_COUPON_POSS_TIMES", boost::lexical_cast<std::string>(poss_times));
                        if(COUPON_DONE_TIMES && (!COUPON_DONE_TIMES->is_null_or_empty())){
                            auto done_times = COUPON_DONE_TIMES->get<int>();
                            v.put("_COUPON_DONE_TIMES", boost::lexical_cast<std::string>(done_times));
                        }
                    }
                }
                if(!COUPON_TEXT.empty()){
                    v.put("_COUPON_TEXT", COUPON_TEXT);
                }
            }
        }
    }
    
    /* printInfoのvalueを再構築 */
    pinfo.put("values", values);
    pinfo.put("type", types);
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobNFCSalesJMups::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobNFCSalesJMups::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

/* virtual */
auto JobNFCSalesJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}

/* virtual */
auto JobNFCSalesJMups::processIsNeedUpdateMasterData() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    auto bExistRWSerial = false;
    
    auto existRWSerial   = TMSAccess::getMasterDataCheckInfo().get_optional<std::string>("RWSerial");
    auto currentRWSerial = getCardReader()->getCurrentReaderInfo().get_optional<std::string>("RWSerial");
    if(existRWSerial && currentRWSerial){
        if(existRWSerial.get() == currentRWSerial.get()){
            bExistRWSerial = true;
        }
    }
    
    if(!bExistRWSerial){ // 新しいCardReader
        return callOnDispatchWithMessage("checkingMasterData").as_dynamic()
        .flat_map([=](auto) {
            return masterDataDownload(true).as_dynamic()
            .flat_map([=](auto){
                return masterDataUpdateCheck(getCardReader()).as_dynamic();
            }).as_dynamic()
            .flat_map([=](const_json_t resut){
                return pprx::just(resut).as_dynamic();;
            }).as_dynamic();
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError(pprx::exceptionToString(err));
            return pprx::error<const_json_t>(err).as_dynamic();
        }).as_dynamic();
    }
    else{ //　既存なCardReader
        // 更新有無値
        auto bNeedUpdate = TMSAccess::getMasterDataCheckInfo().get_optional<bool>("bNeedUpdate");
        json_t result;
        result.put("bNeedUpdate", bNeedUpdate ? bNeedUpdate.get() : false);
        return pprx::just(result.as_readonly());
    }
}

auto JobNFCSalesJMups::processMasterData()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    if(PaymentJob::isTrainingMode()){
        return pprx::just(pprx::unit()).as_dynamic();
    }
    
    return processIsNeedUpdateMasterData().as_dynamic()
    .flat_map([=](const_json_t resultJson){
        const auto bNeedUpdate = resultJson.get_optional<bool>("bNeedUpdate");
        if(bNeedUpdate && (bNeedUpdate.get()) ){
            return callOnDispatchWithMessage("masterDataUpdating")
            .flat_map([=](auto) {
                // マスター更新有無の判定はカードリーダー側で実施する。
                return masterDataUpdate(getCardReader());
            }).as_dynamic()
            .on_error_resume_next([=](auto err){
                return pprx::error<pprx::unit>(err).as_dynamic();
            }).as_dynamic();
        }
        else{
            return pprx::just(pprx::unit()).as_dynamic();
        }
    }).as_dynamic();
}

auto JobNFCSalesJMups::tradeStart()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    return hpsTradeStart_Request
    (
     JMupsAccess::prmTradeStart_Request::serviceDiv::Nfc,
     boost::none,
     JMupsAccess::prmTradeStart_Request::nfcDeviceType::Miura
     )
    .flat_map([=](JMupsAccess::response_t resp){
        if(PaymentJob::isTrainingMode()){
            LogManager_AddDebug("skip adjusting time. (training)");
        }
        else{
            auto dtime = BaseSystem::parseRFC2616DateTimeText(resp.header.get<std::string>("Date"));
            LogManager_AddInformationF("adjust time to %s", boost::posix_time::to_iso_extended_string(dtime));
            return getCardReader()->adjustDateTime(dtime);
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic();
}


auto JobNFCSalesJMups::waitForAmountInput()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto&& termInfo = getTerminalInfo();
    auto&& tradeInfo = getTradeStartInfo();
    
    bool&& isNeedProductCode = tradeInfo.get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
    bool&& isNeedTaxOther    = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";

    TradeInfoBuilder builder;
    builder.addAmount(TradeInfoBuilder::attribute::required, boost::none);
    if(isNeedProductCode){
        auto&& productCode = termInfo.get<std::string>("sessionObject.productCode");
        builder.addProductCode(TradeInfoBuilder::attribute::required, productCode);
    }
    if(isNeedTaxOther){
        builder.addTaxOtherAmount(TradeInfoBuilder::attribute::required, boost::none);
    }
    
    return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "Input")
    .flat_map([=](const_json_t resp){
        auto results = resp.get("results").clone();
        m_tradeData.put("amount", results.get<int>("amount"));
        auto productCode = results.get_optional<std::string>("productCode");
        if(productCode) m_tradeData.put("productCode", *productCode);
        auto taxOtherAmount = results.get_optional<int>("taxOtherAmount");
        if(taxOtherAmount) m_tradeData.put("taxOtherAmount", *taxOtherAmount);
        auto&& currencyCode = termInfo.get_optional<std::string>("sessionObject.transactionCurrencyCode");
        if(currencyCode) {
            m_tradeData.put("currencyCode", boost::lexical_cast<int>(*currencyCode));
        }
        return pprx::just(pprx::unit());
    });
}

auto JobNFCSalesJMups::waitForTouching()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    {
        int totalAmount = 0;
        {
            auto amount = m_tradeData.get_optional<int>("amount");
            if(!amount){
                LogManager_AddError("amount not found.");
            }
            else{
                totalAmount = amount.get();
                auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
                if(taxOtherAmount){
                    totalAmount += taxOtherAmount.get();
                }
            }
        }
        const auto messageStr = (boost::format("タッチしてください\nNFC　売上\n￥%d") % totalAmount).str();
        
        json_t::array_t jarr;
        json_t message;
        message.put("id"    , "EMV_UI_PRESENT_CARD");
        message.put("text"  , messageStr);
        message.put("font"  , "FONT_10X10_JPN");
        jarr.push_back(message);
        m_tradeData.put("optionalMessage.table", jarr);
    }
    
    /* R/W側で動作を変更するために、isTrainingを記録する */
    m_tradeData.put("isTraining", PaymentJob::isTrainingMode());

    return getCardReader()->readContactless(m_tradeData)
    .flat_map([=](const_json_t json){
         if(json.empty()){
             return pprx::maybe::error(make_error_internal("json is null or empty.")).as_dynamic();
         }
         auto status = json.get_optional<std::string>("status");
         if(!status){
             return pprx::maybe::error(make_error_internal("status not found.")).as_dynamic();
         }
         else if(status.get() == "approved"){
             m_cardData = json.clone();
             return pprx::maybe::success(pprx::unit()).as_dynamic();
         }
         else if(status.get() == "onlineProcessing"){
             m_cardData = json.clone();
             m_onlineProcessing = true;
             return pprx::maybe::success(pprx::unit()).as_dynamic();
         }
         else if(status.get() == "declined"){
             json_t j;
             j.put("description", "declined");
             return pprx::maybe::error(make_error_cardrw(j)).as_dynamic();
         }
         else if(status.get() == "seePhone"){
             return waitForCardActive()
             .flat_map([=](auto){
                 return pprx::maybe::retry().as_dynamic();
             }).as_dynamic();
         }
         else if(status.get() == "timeout"){
             return pprx::maybe::error(make_error_internal("timeout")).as_dynamic();
         }
         else {
             return pprx::maybe::error(make_error_internal("unknown")).as_dynamic();
         }
     })
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::maybe::error(err).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe obj){
        return obj.observableForContinue<pprx::unit>();
    });
}

auto JobNFCSalesJMups::waitForCardActive()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return callOnDispatchWithCommand("waitForCardActive")
    .map([=](auto){
        return pprx::unit();
    });
}

auto JobNFCSalesJMups::onCardReadFinished()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    json_t param;
    param.put("type", "online");
    return callOnDispatchWithMessageAndSubParameter("onCardReadFinished", "", param)
    .map([=](auto){
        return pprx::unit();
    });
}

auto JobNFCSalesJMups::sendCardResultData()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    m_tradeConfirmFinished.clear();
    const auto cardBrandIdentifier = JMupsAccessNFC::prmNFCCommon::stringToCardBrandIdentifier(m_cardData.get<std::string>("cardBrand"));
    
    return pprx::just(JMupsAccess::response_t()).as_dynamic()
    .flat_map([=](auto){
        if(m_onlineProcessing){
            return callOnDispatchWithMessage("centerTransaction").as_dynamic();
        }
        else{
            return pprx::just(pprx::unit()).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](auto){
        return hpsNFCRead
        (
         prmNFCCommon::nfcDeviceType::Miura,
         prmNFCCommon::resultCode::Success,
         m_cardData.get<std::string>("resultCodeExtended"),
         m_cardData.get<std::string>("transactionResultBit"),
         cardBrandIdentifier,
         m_cardData.get<std::string>("nfcTradeResult"),
         m_cardData.get<std::string>("outcomeParameter"),
         m_cardData.get_optional<std::string>("nfcTradeResultExtended"),
         m_cardData.get_optional<std::string>("keySerialNoForResult"),
         m_cardData.get_optional<std::string>("keySerialNoForExtended")
         ).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

class TradeInfoBuilderJMupsNFC : public TradeInfoBuilder
{
public:
    virtual ~TradeInfoBuilderJMupsNFC() = default;
    void addPaymentWay()
    {
        json_t j;
        j.put("type", "paymentWay");
        j.put("attribute", "readonly");
        j.put("current.paymentWay", "lump");
        j.put("config", nullptr);
        m_values["paymentWay"] = j;
    }
};

auto JobNFCSalesJMups::waitForTradeInfo(const std::string& strTradeOp)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    const auto bCouponEnabled = getJMupsStorage()->getTerminalInfo().get<std::string>("sessionObject.couponServiceOnoffDiv") == "1";
    if(!bCouponEnabled){
        return pprx::just(pprx::unit());
    }
    
    return hpsCouponSalesAutoDiscount_communication
    (
     m_tradeData.get<int>("amount"),
     prmCouponSalesAutoDiscount_communication::serviceDiv::Credit
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_couponData = resp.body.clone();
        auto arr = m_couponData.get_optional<json_t::array_t>("normalObject.printInfoList");
        if(!arr || (arr->size() == 0)){
            LogManager_AddInformation("normalObject.printInfoList is null or empty -> skip coupon");
            return pprx::just(pprx::unit())
            .as_dynamic();
        }
        else{
            TradeInfoBuilderJMupsNFC builder;
            builder.addAmount(TradeInfoBuilder::attribute::readonly, m_tradeData.get<int>("amount"));
            auto productCode = m_tradeData.get_optional<std::string>("productCode");
            if(productCode) builder.addProductCode(TradeInfoBuilder::attribute::readonly, *productCode);
            auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
            if(taxOtherAmount) builder.addTaxOtherAmount(TradeInfoBuilder::attribute::readonly, *taxOtherAmount);
            builder.addCoupon(TradeInfoBuilder::attribute::optional, boost::none, m_couponData.as_readonly());
            
            builder.addPaymentWay();
            
            return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, strTradeOp)
            .flat_map([=](const_json_t json){
                const auto couponId = json.get_optional<std::string>("results.coupon");
                if(couponId){
                    auto couponInfo = findCoupon(m_couponData, *couponId);
                    auto couponSelectListNum = getCouponSelectListNum(m_couponData, *couponId);
                    auto orgAmount = m_tradeData.get<int>("amount");
                    auto discountValue = couponInfo.get<int>("DISCOUNT_PRICE");
                    m_tradeData.put("couponSelectListNum", *couponSelectListNum);
                    m_tradeData.put("couponApplyPreviousAmount", orgAmount);
                    m_tradeData.put("amount", orgAmount - discountValue);
                    return waitForReaderConnect()
                    .flat_map([=](auto){
                        return waitForTouching().as_dynamic();
                    })
                    .flat_map([=](auto){
                        return onCardReadFinished().as_dynamic();
                    }).as_dynamic();
                }
                else{
                    LogManager_AddInformation("selected 'couponIndex' is null -> skip coupon");
                    return pprx::just(pprx::unit()).as_dynamic();
                }
            })
            .as_dynamic();
        }
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto JobNFCSalesJMups::sendTradeConfirm(prmNFCSales_Confirm::selector selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();

    LogManager_AddDebug(m_tradeData.str());
    const auto cardBrandIdentifier = JMupsAccessNFC::prmNFCCommon::stringToCardBrandIdentifier(m_cardData.get<std::string>("cardBrand"));
    
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    if(!taxOtherAmount) taxOtherAmount = 0;
    auto productCode = m_tradeData.get_optional<std::string>("productCode");
    if(!productCode) productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
    
    return pprx::just(JMupsAccess::response_t()).as_dynamic()
    .flat_map([=](auto){
        if(m_onlineProcessing){
            return callOnDispatchWithMessage("centerTransaction").as_dynamic();
        }
        else{
            return pprx::just(pprx::unit()).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](auto){
        return hpsNFCSales_Confirm
        (
         selector,
         prmNFCCommon::nfcDeviceType::Miura,
         prmNFCCommon::resultCode::Success,
         m_cardData.get<std::string>("resultCodeExtended"),
         m_tradeData.get<int>("amount"),
         *taxOtherAmount,
         *productCode,
         JMupsAccessNFC::prmNFCCommon::payWayDiv::Lump,
         false, /* isRePrint */
         m_cardData.get<std::string>("transactionResultBit"),
         cardBrandIdentifier,
         m_cardData.get<std::string>("nfcTradeResult"),
         m_cardData.get<std::string>("outcomeParameter"),
         m_cardData.get_optional<std::string>("nfcTradeResultExtended"),
         m_cardData.get_optional<std::string>("keySerialNoForResult"),
         m_cardData.get_optional<std::string>("keySerialNoForExtended"),
         m_tradeData.get_optional<int>("couponSelectListNum"), /* couponSelectListNum */
         m_tradeData.get_optional<int>("couponApplyPreviousAmount"), /* couponApplyPreviousAmount */
         boost::none, /* point */
         boost::none  /* exchangeablePoint */
         ).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<JMupsAccess::response_t>(err).as_dynamic();
    });
}

auto JobNFCSalesJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(pprx::unit());
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmNFCSales_Confirm::selector::Double);
    })
    .as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    });
}

auto JobNFCSalesJMups::sendTagging()
 -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsNFCSales_Tagging
        (
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmNFCCommon::printResultDiv::Success,
         false,
         boost::none,   /* isSendingDesiSlip */
         boost::none,   /* issureScriptResultDiv */
         boost::none,   /* nfcDeviceType */
         boost::none,   /* nfcTradeResult */
         boost::none,   /* nfcTradeResultExtended */
         boost::none,   /* keySerialNoForResult */
         boost::none    /* keySerialNoForExtended */
         );
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        return pprx::error<JMupsAccess::response_t>(err).as_dynamic();
    }).as_dynamic();
 }

auto JobNFCSalesJMups::processSendDigiSign()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    const auto digiReceiptDiv = tradeResultData.get_optional<std::string>("normalObject.digiReceiptDiv");
    if( digiReceiptDiv && (*digiReceiptDiv == "1")){
        LogManager_AddInformation("電子伝票保管対象（サイン要）");
        do{ // トレモで、1万以下サイン不要と要望
            if(!PaymentJob::isTrainingMode()) {
                break;
            }
            const auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
            const auto amount = m_tradeData.get<int>("amount") + (taxOtherAmount ? *taxOtherAmount : 0);
            if(amount>10000) {
                break;
            }
            
            tradeResultData.put("ppsdk.isNeedAquirerSlip", false);
            getJMupsStorage()->setTradeResultData(tradeResultData);
            return pprx::observable<>::just(pprx::unit());
        } while(0);
    
        return waitForDigiSign()
        .flat_map([=](boost::optional<std::string> s){
            if(s){
                std::string sign = *s;
                return sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip, sign);
            }
            else{
                return pprx::error<pprx::unit>(make_error_internal("sign data is empty")).as_dynamic();
            }
        })
        .as_dynamic()
        .flat_map([=](auto){
            return pprx::observable<>::just(pprx::unit());
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError(pprx::exceptionToString(err));
            return pprx::error<pprx::unit>(err).as_dynamic();
        }).as_dynamic();
    }
    else if(digiReceiptDiv && (*digiReceiptDiv == "2")){
        LogManager_AddInformation("電子伝票保管対象（サイン不要）");
        return pprx::observable<>::just(pprx::unit());
    }
    else{
        LogManager_AddInformation("電子伝票保管対象外");
        tradeResultData.put("ppsdk.isNeedAquirerSlip", true);
        getJMupsStorage()->setTradeResultData(tradeResultData);
        return pprx::observable<>::just(pprx::unit());
    }
}


auto JobNFCSalesJMups::waitForDigiSign()
  -> pprx::observable<boost::optional<std::string>>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();

    json_t param;
    param.put("modulus", tradeResultData.get<std::string>("normalObject.digiSignPubKeyModulus"));
    param.put("exponent", tradeResultData.get<std::string>("normalObject.digiSignPubKeyExponent"));
    const auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    auto totalAmount = m_tradeData.get<int>("amount") + (taxOtherAmount ? *taxOtherAmount : 0);
    param.put("amount", totalAmount);
    auto strAmount = (boost::format("合計金額：%s 円") % BaseSystem::commaSeparatedString(totalAmount)).str();
    param.put("strAmount", strAmount );
    
    return callOnDispatchWithCommandAndSubParameter("waitForDigiSign", "hps", param)
    .map([=](const_json_t json){
         return json.get_optional<std::string>("encryptedDigiSignData");
     });
}

auto JobNFCSalesJMups::sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    
    return hpsStoreDigiSignForCredit( slipNo, digiReceiptDiv, digiSign )
    .flat_map([=](auto resp){
        return pprx::maybe::success(resp);
    }).as_dynamic()
    .on_error_resume_next([=](std::exception_ptr err){
        LogManager_AddError(pprx::exceptionToString(err));
        return callOnDispatchWithCommand("waitForRetrySendingDigiSign")
        .flat_map([=](const_json_t json){
            const auto bRetry = json.get_optional<bool>("bRetry");
            if(bRetry){
                return *bRetry ? pprx::maybe::retry() : pprx::maybe::error(err);
            }
            else{
                return pprx::maybe::error(make_error_internal("bRetry is null")).as_dynamic();
            }
        }).as_dynamic();
    })
    .retry()
    .flat_map([=](pprx::maybe ctrl){
        return ctrl.observableForContinue<JMupsAccess::response_t>();
    }).as_dynamic()
    .map([=](auto resp){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
        switch(digiReceiptDiv){
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip:
                tradeResultData.put("ppsdk.isNeedAquirerSlip", false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlipWithoutSign:
                tradeResultData.put("ppsdk.isNeedAquirerSlip", false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::PaperSlip:
                tradeResultData.put("ppsdk.isNeedAquirerSlip", true);
                break;
            default:
                LogManager_AddError("invalid digiReceiptDiv");
                break;
        }
        getJMupsStorage()->setTradeResultData(tradeResultData);
        return pprx::unit();
    })
    .on_error_resume_next([=](auto err){
        LogManager_AddWarning("sign failed.");
        
        json_t result;
        bool bOutjsonRetryNullFlg = false;
        try{std::rethrow_exception(err);}
        catch(pprx::ppexception& ex){
            result = json_t::from_str(ex.what());
            auto errorDesc = result.get_optional<std::string>("result.detail.description");
            if(errorDesc&& (*errorDesc == "bRetry is null")){
                bOutjsonRetryNullFlg = true;
            }
            else{
                result.put("resultCode", "1");
                result.put("errorObject.errorCode", "PP_W0002");
                result.put("errorObject.errorLevel", "ERROR");
                result.put("errorObject.errorType", "ERROR");
                result.put("errorObject.errorMessage", "電子サイン送信失敗");
            }
        }
        catch(...){
            LogManager_AddDebug("what??");
        }
        
        return bOutjsonRetryNullFlg ? pprx::error<pprx::unit>(make_error_internal("bRetry is nil")) : pprx::error<pprx::unit>(make_error_paymentserver(result));
    });
}


auto JobNFCSalesJMups::processCardJob()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit()).as_dynamic()
    .flat_map([=](auto){
        return masterDataSendUpdateDate(getTradeStartInfo()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForAmountInput().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTouching().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return onCardReadFinished().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendCardResultData().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTradeConfirm(prmNFCSales_Confirm::selector::Normal).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForConfirmDoubleTrade(jmups).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        if(m_cardData.get<std::string>("status") == "onlineProcessing"){
            bool bSuccess = false;
            auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
            auto values = tradeResultData.get_optional<json_t::array_t>("normalObject.printInfo.device.value.values");
            if(values && (values->size() > 0)){
                auto failure_info = (*values)[0].get_optional<json_t::array_t>("FAILURE_INFO");
                if(!failure_info){
                    bSuccess = true;
                }
            }
            return getCardReader()->onlineProccessingResult(bSuccess);
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_internal& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_cardrw& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_aborted& ppex){
            LogManager_AddInformationF("exception\t%s", ppex.json().str());
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppexception& ppex){
            LogManager_AddInformationF("ppexception\t%s", ppex.json().str());
        }
        
        auto cardStatus = m_cardData.get_optional<std::string>("status");
        if(cardStatus && (*cardStatus == "onlineProcessing")){
            return getCardReader()->onlineProccessingResult(false)
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(err).as_dynamic();
            }).as_dynamic();
        }
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto JobNFCSalesJMups::initializeChildJob() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return processMasterData().as_dynamic();
}

/* virtual */
auto JobNFCSalesJMups::mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddDebugF("data = %s", data.str());
    return analyseReadResult(data).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return masterDataSendUpdateDate(getTradeStartInfo()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return onCardReadFinished().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendCardResultData().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTradeConfirm(prmNFCSales_Confirm::selector::Normal).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForConfirmDoubleTrade(jmups).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto status = m_cardData.get<std::string>("status");
        if(status == "onlineProcessing"){
            bool bSuccess = false;
            auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
            auto values = tradeResultData.get_optional<json_t::array_t>("normalObject.printInfo.device.value.values");
            if(values && (values->size() > 0)){
                auto failure_info = (*values)[0].get_optional<json_t::array_t>("FAILURE_INFO");
                if(!failure_info){
                    bSuccess = true;
                }
            }
            return getCardReader()->onlineProccessingResult(bSuccess);
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_internal& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_paymentserver& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_cardrw& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_network& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_aborted& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppexception& ppex){
            LogManager_AddInformationF("ppexception\t%s", ppex.json().str());
        }
        auto cardStatus = m_cardData.get_optional<std::string>("status");
        if(cardStatus && (*cardStatus == "onlineProcessing")){
            return getCardReader()->onlineProccessingResult(false)
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(err).as_dynamic();
            }).as_dynamic();
        }
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto JobNFCSalesJMups::analyseReadResult(const_json_t resultData) -> pprx::observable<pprx::unit>
{
    if(resultData.empty()){
        return pprx::error<pprx::unit>(make_error_internal("json is null or empty."));
    }
    auto status = resultData.get_optional<std::string>("status");
    if(!status) {
        return pprx::error<pprx::unit>(make_error_internal("status not found.")).as_dynamic();
    }
    std::string statusStr = status.get();
    LogManager_AddInformationF("resultData = %s", resultData.str());
    LogManager_AddDebugF("in analyseReadResult. %s case.", statusStr);
    if(statusStr == "approved") {
        m_tradeData.put("amount", resultData.get<int>("amount"));
        auto productCode = resultData.get_optional<std::string>("productCode");
        if(productCode) m_tradeData.put("productCode", *productCode);
        auto taxOtherAmount = resultData.get_optional<int>("taxOtherAmount");
        if(taxOtherAmount) m_tradeData.put("taxOtherAmount", *taxOtherAmount);
        auto currencyCode = resultData.get_optional<int>("currencyCode");
        if(currencyCode) m_tradeData.put("currencyCode", *currencyCode);
        m_cardData = resultData.clone();
        return pprx::just(pprx::unit()).as_dynamic();
    } else if(statusStr == "onlineProcessing") {
        m_onlineProcessing = true;
        m_tradeData.put("amount", resultData.get<int>("amount"));
        auto productCode = resultData.get_optional<std::string>("productCode");
        if(productCode) m_tradeData.put("productCode", *productCode);
        auto taxOtherAmount = resultData.get_optional<int>("taxOtherAmount");
        if(taxOtherAmount) m_tradeData.put("taxOtherAmount", *taxOtherAmount);
        auto currencyCode = resultData.get_optional<int>("currencyCode");
        if(currencyCode) m_tradeData.put("currencyCode", *currencyCode);
        m_cardData = resultData.clone();
        return pprx::just(pprx::unit()).as_dynamic();
    } else if(statusStr == "declined") {
        m_cardData = resultData.clone();
        json_t j;
        j.put("description", statusStr);
        return pprx::error<pprx::unit>(make_error_cardrw(j)).as_dynamic();
    } else if(statusStr == "seePhone") {
        json_t j;
        j.put("description", statusStr);
        return pprx::error<pprx::unit>(make_error_cardrw(j)).as_dynamic();
    } else if(statusStr == "timeout") {
        return pprx::error<pprx::unit>(make_error_internal(statusStr)).as_dynamic();
    } else {
        return pprx::error<pprx::unit>(make_error_internal("unknown")).as_dynamic();
    }
}

/* virtual */
auto JobNFCSalesJMups::getChildSlipDocument() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return treatSlipDocumentJson();
}

/* virtual */
auto JobNFCSalesJMups::execChildJobDigiSign() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return processSendDigiSign();
}

/* virtual */
auto JobNFCSalesJMups::execChildJobSendTagging() -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    return sendTagging();
}



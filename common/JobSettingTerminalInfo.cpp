//
//  JobSettingTerminalInfo.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingTerminalInfo.h"
#include "JMupsJobStorage.h"

JobSettingTerminalInfo::JobSettingTerminalInfo(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobSettingTerminalInfo::~JobSettingTerminalInfo()
{
    
}

/* virtual */
auto JobSettingTerminalInfo::getObservableSelf() -> pprx::observable<const_json_t>
{
    auto&& terminalInfo = boost::dynamic_pointer_cast<JMupsJobStorage>(dynamic_cast<PaymentJob*>(this)->getStorage())->getTerminalInfo();
    auto&& sysInfo = BaseSystem::instance().getSystemInformation().clone();
    LogManager_AddDebug(sysInfo.str(true));
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto&& termJudgeNo = terminalInfo.get<std::string>("sessionObject.termJudgeNo");
        auto&& appVersion = sysInfo.get<std::string>("AppVersion");
        auto&& oSVersion = sysInfo.get<std::string>("PlatformOSVersion");
        auto&& UID = sysInfo.get<std::string>("UserName");
        
        json_t json;
        json.put("status",               "success");
        json.put("result.UID",            UID);
        json.put("result.termJudgeNo",    termJudgeNo);
        json.put("result.oSVersion",      oSVersion);
        json.put("result.appVersion",     appVersion);
        return pprx::just(json.as_readonly());
    }).as_dynamic();
}

/* virtual */
auto JobSettingTerminalInfo::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

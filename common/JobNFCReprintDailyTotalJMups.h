//
//  JobNFCReprintDailyTotalJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobNFCReprintDailyTotalJMups__)
#define __h_JobNFCReprintDailyTotalJMups__

#include "PaymentJob.h"
#include "JMupsAccessReprint.h"

class JobNFCReprintDailyTotalJMups :
    public PaymentJob,
    public JMupsAccessReprint
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobNFCReprintDailyTotalJMups(const __secret& s);
    virtual ~JobNFCReprintDailyTotalJMups();
    
    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCReprintDailyTotalJMups__) */

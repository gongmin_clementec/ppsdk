//
//  BaseSystem.cpp
//  ppsdk
//
//  Created by clementec on 2016/04/14.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#include <boost/regex.hpp>
#include <boost/algorithm/hex.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "PPReactive.h"

static constexpr int kNumThreads = 8;


BaseSystem::BaseSystem()
{
    m_imp.reset(AbstructFactory::instance().createBaseSystemImp());
    m_ioService = boost::make_shared<boost::asio::io_service>();
    m_iosWork   = boost::make_shared<boost::asio::io_service::work>(*m_ioService.get());
    m_threads   = boost::make_shared< boost::thread_group>();
    for(int i = 0; i < kNumThreads; i++){
        m_threads->create_thread(
            boost::bind(
                &boost::asio::io_service::run,
                m_ioService.get()
            )
        );
    }
}

/* virtual */
BaseSystem::~BaseSystem()
{
    m_iosWork.reset();
    if(m_threads.get() != nullptr){
        m_threads->join_all();
        m_threads.reset();
    }
    m_ioService.reset();
}

void BaseSystem::post(boost::function<void()> func)
{
    m_ioService->post([=](){
        m_imp->callFunctionWithCatchException(func);
    });
}

timer_sp BaseSystem::postWithTimeout(int second, boost::function<void()> func)
{
    auto timer = boost::make_shared<boost::asio::deadline_timer>(*m_ioService);
    timer->expires_from_now(boost::posix_time::seconds(second));
    timer->async_wait([timer, func](const boost::system::error_code& error){
        if(!error){
            func();
        }
    });
    return timer;
}

void BaseSystem::invokeMainThread(boost::function<void()> func)
{
    m_imp->invokeMainThread(func);
}

void BaseSystem::invokeMainThreadAsync(boost::function<void()> func)
{
    m_imp->invokeMainThreadAsync(func);
}

bool BaseSystem::isMainThread()
{
    return m_imp->isMainThread();
}

boost::filesystem::path BaseSystem::getResourceDirectoryPath()
{
    return m_imp->getResourceDirectoryPath();
}

boost::filesystem::path BaseSystem::getDocumentDirectoryPath()
{
    return m_imp->getDocumentDirectoryPath();
}

const_json_t BaseSystem::getSystemInformation()
{
    return m_imp->getSystemInformation();
}

std::string BaseSystem::getDeviceUuid()
{
    return m_imp->getDeviceUuid();
}

boost::optional<std::string> BaseSystem::getSecureElementText(const std::string& name)
{
    return m_imp->getSecureElementText(name);
}

bool BaseSystem::setSecureElementText(const std::string& name, const std::string& value)
{
    return m_imp->setSecureElementText(name, value);
}


boost::optional<std::string> BaseSystem::getSharedAreaText(const std::string& name)
{
    return m_imp->getSharedAreaText(name);
}

bool BaseSystem::setSharedAreaText(const std::string& name, const std::string& value)
{
    return m_imp->setSharedAreaText(name, value);
}

bool BaseSystem::certificateIsInstalled()
{
    return m_imp->certificateIsInstalled();
}

bool BaseSystem::certificateInstall(const boost::filesystem::path& path, const std::string& password)
{
    return m_imp->certificateInstall(path, password);
}

bool BaseSystem::certificateUninstall()
{
    return m_imp->certificateUninstall();
}

auto BaseSystem::canSendTextToAnotherApplication(const std::string& text)
-> pprx::observable<bool>
{
    return m_imp->canSendTextToAnotherApplication(text);
}

auto BaseSystem::applicationState()
-> rxcpp::observable<BaseSystem::ApplicationState>
{
    return m_imp->applicationState();
}

BaseSystem::OSType BaseSystem::getOSType()
{
    return m_imp->getOSType();
}

auto BaseSystem::soundPlay(const std::string& soundFile) -> void
{
    m_imp->soundPlay(soundFile);
}

auto BaseSystem::sendTextToAnotherApplication(const std::string& text)
 -> pprx::observable<bool>
{
    return m_imp->sendTextToAnotherApplication(text);
}

/* static */
std::string BaseSystem::jis8ToUtf8(const std::string& src)
{
    constexpr const char* const jis8_table[256] =
    {
        "\x00","\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09","\x0a","\x0b","\x0c","\x0d","\x0e","\x0f",
        "\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19","\x1a","\x1b","\x1c","\x1d","\x1e","\x1f",
        " ","!","\"","#","$","%","&","\'","(",")","*","+",",","-",".","/",
        "0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?",
        "@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O",
        "P","Q","R","S","T","U","V","W","X","Y","Z","[","\\","]","^","_",
        "`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
        "p","q","r","s","t","u","v","w","x","y","z","{","|","}","~","\x7f",
        "\x80","\x81","\x82","\x83","\x84","\x85","\x86","\x87","\x88","\x89","\x8a","\x8b","\x8c","\x8d","\x8e","\x8f",
        "\x90","\x91","\x92","\x93","\x94","\x95","\x96","\x97","\x98","\x99","\x9a","\x9b","\x9c","\x9d","\x9e","\x9f",
        "\xa0","｡","｢","｣","､","･","ｦ","ｧ","ｨ","ｩ","ｪ","ｫ","ｬ","ｭ","ｮ","ｯ",
        "ｰ","ｱ","ｲ","ｳ","ｴ","ｵ","ｶ","ｷ","ｸ","ｹ","ｺ","ｻ","ｼ","ｽ","ｾ","ｿ",
        "ﾀ","ﾁ","ﾂ","ﾃ","ﾄ","ﾅ","ﾆ","ﾇ","ﾈ","ﾉ","ﾊ","ﾋ","ﾌ","ﾍ","ﾎ","ﾏ",
        "ﾐ","ﾑ","ﾒ","ﾓ","ﾔ","ﾕ","ﾖ","ﾗ","ﾘ","ﾙ","ﾚ","ﾛ","ﾜ","ﾝ","ﾞ","ﾟ",
        "\xe0","\xe1","\xe2","\xe3","\xe4","\xe5","\xe6","\xe7","\xe8","\xe9","\xea","\xeb","\xec","\xed","\xee","\xef",
        "\xf0","\xf1","\xf2","\xf3","\xf4","\xf5","\xf6","\xf7","\xf8","\xf9","\xfa","\xfb","円","\xfd","\xfe","\xff"
    };
    
    std::stringstream ss;
    for(auto it = std::begin(src); it != std::end(src); it++){
        ss << jis8_table[static_cast<unsigned char>(*it)];
    }
    return ss.str();
}

std::string BaseSystem::sjisToUtf8(const std::string& src)
{
    return m_imp->sjisToUtf8(src);
}

/* static */
std::string BaseSystem::replaceTextAlignRight(const std::string& src, const std::string& append)
{
    if(append.length() > src.length()) return append;
    
    std::string result = src.substr(0, src.length() - append.length());
    return result + append;
}


/* static */
std::string BaseSystem::urlEncode(const std::string& src)
{
    const boost::regex  esc("[.^$|()\\[\\]{}*+?\\\\]");
    const std::string   rep("\\\\&");
    return boost::regex_replace(src, esc, rep, boost::match_default | boost::format_sed);
}

/* static */
std::string BaseSystem::dumpHexText(const bytes_t& bytes)
{
    std::string result;
    boost::algorithm::hex(std::begin(bytes), std::end(bytes), back_inserter(result));
    return result;
}

/* static */
boost::posix_time::ptime BaseSystem::parseRFC2616DateTimeText(const std::string& src)
{
    /* RFC2616, sec 3.3
        Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
        Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
        Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
    */
    constexpr const char* const formats[] =
    {
        "%a, %d %b %Y %H:%M:%S GMT",
        "%A, %d-%b-%y %H:%M:%S GMT",
        "%a %b %e %H:%M:%S %Y",
        "%a  %b %e %H:%M:%S %Y"
    };
    
    for(auto format : formats){
        /* facet はstreamで削除される */
        try{
            auto loc = std::locale(std::locale::classic(), new boost::posix_time::time_input_facet(format));
            std::stringstream ss(src);
            ss.imbue(loc);
            boost::posix_time::ptime pt;
            ss >> pt;
            if(pt != boost::posix_time::ptime()){
                return boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(pt);
            }
        }
        catch(...){
        }
    }
    LogManager_AddErrorF("Cannot convert from \"%s\"", src);
    return boost::posix_time::ptime();
}

/* static */
std::string BaseSystem::commaSeparatedString(int value)
{
    if(value == 0) return std::string("0");
    std::stringstream ss;
    const bool bWithMinus = (value < 0);
    value = ::abs(value);
    for(int nComma = 0; value != 0; value /= 10, nComma++){
        if(nComma == 3){
            ss << ',';
            nComma = 0;
        }
        ss << static_cast<char>(('0' + (value % 10)));
    }
    if(bWithMinus) ss << '-';
    auto&& rstr = ss.str();
    return std::string(rstr.rbegin(), rstr.rend());
}

/* static */
int BaseSystem::commaSeparatedStringToInt(std::string value)
{
    auto strIntValue = value;
    int nPos = value.find("\\");
    // delete "\\"
    if(nPos!=std::string::npos){
        strIntValue = value.substr(nPos+1, value.length()-1);
    }
    
    auto tempStr = strIntValue;
    nPos = tempStr.find(",");
    while(nPos!=std::string::npos){  // delete ,
        auto temStr1 =  tempStr.substr(0, nPos) + tempStr.substr(nPos+1, tempStr.length());
        tempStr = temStr1;
        std::cout <<temStr1<< "\t"<<temStr1.length()<< "\n";
        nPos = tempStr.find(",");
    }
    
    auto result = ::atoi(tempStr.c_str());
    return  result;
}

boost::shared_ptr<SerialDevice> BaseSystem::createSerialDevice()
{
    return m_imp->createSerialDevice();
}


//
//  JobNFCReprintDailyTotalJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCReprintDailyTotalJMups.h"

JobNFCReprintDailyTotalJMups::JobNFCReprintDailyTotalJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobNFCReprintDailyTotalJMups::~JobNFCReprintDailyTotalJMups()
{
    
}

/* virtual */
auto JobNFCReprintDailyTotalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    return pprx::error<const_json_t>(make_error_internal("obsoleted."));
}

/* virtual */
auto JobNFCReprintDailyTotalJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


//
//  JobSettingReaderFWUpdate.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingReaderFWUpdate.h"
#include "ZipFileDecompressor.h"
#include "PPConfig.h"
#include "TMSAccess.h"
#include "CardReader.h"

JobSettingReaderFWUpdate::JobSettingReaderFWUpdate(const __secret& s) :
    PaymentJob(s),
    m_tms(TMSAccess::create())
{
    LogManager_Function();
}

/* virtual */
JobSettingReaderFWUpdate::~JobSettingReaderFWUpdate()
{
    LogManager_Function();
}

/* virtual */
auto JobSettingReaderFWUpdate::getObservableSelf() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    const auto zipfp  = BaseSystem::instance().getDocumentDirectoryPath() / "fw.zip";
    const auto zipdir = BaseSystem::instance().getDocumentDirectoryPath() / "fw";
    
    if(boost::filesystem::exists(zipfp)){
        boost::filesystem::remove(zipfp);
    }

    return m_tms->initializeAndLoginAndUpdate()
    .flat_map([=](const_json_t resp){
        auto RWFirmwareDownloadUrl = resp.get_optional<std::string>("RWFirmwareDownloadUrl");
        
        json_t j;
        j.put("bAvailable", RWFirmwareDownloadUrl != boost::none);
        return callOnDispatchWithCommandAndSubParameter("waitForStart", "", j)
        .flat_map([=](auto){
            if(!RWFirmwareDownloadUrl){
                throw_error_internal("RWFirmwareDownloadUrl == null")
            }
            return m_tms->blobDownload(*RWFirmwareDownloadUrl, zipfp);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        ZipFileDecompressor zip;
        zip.open(zipfp.string());
        zip.decompressAll(zipdir.string());
        return waitForReaderConnect();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->updateFirmware(zipdir);
    }).as_dynamic()
    .flat_map([=](auto){
        json_t j;
        j.put("status", "success");
        return pprx::just(j.as_readonly());
    }).as_dynamic();
}

/* virtual */
bool JobSettingReaderFWUpdate::isJobTimeoutRequired() const
{
    LogManager_Function();
    
    return false;
}

/* virtual */
auto JobSettingReaderFWUpdate::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}

auto JobSettingReaderFWUpdate::doUpdate(const std::string& url) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    const auto zipfp  = BaseSystem::instance().getDocumentDirectoryPath() / "fw.zip";
    const auto zipdir = BaseSystem::instance().getDocumentDirectoryPath() / "fw";

    if(boost::filesystem::exists(zipfp)){
        boost::filesystem::remove(zipfp);
    }

    return m_tms->blobDownload(url, zipfp)
    .flat_map([=](auto){
        ZipFileDecompressor zip;
        zip.open(zipfp.string());
        zip.decompressAll(zipdir.string());
        return waitForReaderConnect();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->updateFirmware(zipdir);
    }).as_dynamic();
}

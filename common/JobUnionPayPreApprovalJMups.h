//
//  JobUnionPayPreApprovalJMups.cpp
//  ppsdk
//
//  Created by tel on 2018/12/07.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobUnionPayPreApprovalJMups__)
#define __h_JobUnionPayPreApprovalJMups__

#include "JobUnionPaySalesJMups.h"

class JobUnionPayPreApprovalJMups :
    public JobUnionPaySalesJMups
{
private:

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;

    virtual prmCommon::operationDiv getOperationDiv() const;

    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual auto sendTradeConfirm(prmUnionPay::doubleTrade selector, boost::optional<std::string> authenticationNo)  -> pprx::observable<JMupsAccess::response_t>;
    virtual auto waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData, boost::optional<std::string> authenticationNo) -> pprx::observable<prmUnionPay::doubleTrade>;
    virtual auto sendTagging(prmUnionPay::doubleTrade selector) -> pprx::observable<JMupsAccess::response_t>;
    virtual void beginAsyncPreInputAmount();
    
protected:
    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;

public:
            JobUnionPayPreApprovalJMups(const __secret& s);
    virtual ~JobUnionPayPreApprovalJMups();
};



#endif /* !defined(__h_JobUnionPayPreApprovalJMups__) */

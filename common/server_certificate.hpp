//
// Copyright (c) 2016-2017 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

#ifndef BOOST_BEAST_EXAMPLE_COMMON_SERVER_CERTIFICATE_HPP
#define BOOST_BEAST_EXAMPLE_COMMON_SERVER_CERTIFICATE_HPP

#include <boost/asio/buffer.hpp>
#include <boost/asio/ssl/context.hpp>
#include <cstddef>
#include <memory>

/*  Load a signed certificate into the ssl context, and configure
 the context for use with a server.
 
 For this to work with the browser or operating system, it is
 necessary to import the "Beast Test CA" certificate into
 the local certificate store, browser, or operating system
 depending on your environment Please see the documentation
 accompanying the Beast certificate for more details.
 */
inline
void
load_server_certificate(boost::asio::ssl::context& ctx)
{
    /*
     The certificate was generated from CMD.EXE on Windows 10 using:
     
     winpty openssl dhparam -out dh.pem 2048
     winpty openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -subj "//C=US\ST=CA\L=Los Angeles\O=Beast\CN=www.example.com"
     */
    
    std::string const cert =
R"***(-----BEGIN CERTIFICATE-----
MIIDbDCCAlQCCQCjcbLrFxPmSDANBgkqhkiG9w0BAQsFADB4MQswCQYDVQQGEwJK
UDEOMAwGA1UECAwFVE9LWU8xDjAMBgNVBAcMBVRPS1lPMRwwGgYDVQQKDBNDbGVt
ZW50ZWMgQ28uLCBMdGQuMQ4wDAYDVQQLDAVwcHNkazEbMBkGA1UEAwwScHBzZGsu
Y2xlbWVudGVjLmpwMB4XDTE4MDcwNDA5MTEyMFoXDTI4MDcwMTA5MTEyMFoweDEL
MAkGA1UEBhMCSlAxDjAMBgNVBAgMBVRPS1lPMQ4wDAYDVQQHDAVUT0tZTzEcMBoG
A1UECgwTQ2xlbWVudGVjIENvLiwgTHRkLjEOMAwGA1UECwwFcHBzZGsxGzAZBgNV
BAMMEnBwc2RrLmNsZW1lbnRlYy5qcDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC
AQoCggEBAOdJDgtsjgLWtHffrbosai6xkdJzs+uqb//BxFmGbX+VaCzmw6jKAJlG
071ny4IaoWw/BH65rieBVFXhNEggzXH3bBHBuykfWUAPcCkWh5swkokNRcsyb9QX
LGtyC/bZCKIHE/p8Q7M0CST+PkYYZT1MGfM7vl0/T2asKRtcMz6BEhGeUPDm2YT5
cmNvOP/cENHrBs+tgCJxU6hc6s9j2KZXOM1/oC9Zhf+Th7har1kX9IDl2TNgX8pd
EB9DQ3lCyLevUI1cY7GwcPoHhGlXYLpEdz8/C3Y0EG292D2sUlS0MlpT8BckQxPF
6K974s3v2O2jowqdRnyQLqB3hT5tXMMCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEA
e2oi/5IF1pnL17BTm4JgHWat+4JZxSHlmfKOvKExXRM2pnOIZgOhhT1pyuJsoBl6
rqyVSAWool5oB4HhQy1vVS3JYxlRJYnUAYsYS06FShATRPA3AUAWMCCvwJoJzHoP
ynK8XFHqdtSRpGX8tPxn7MsJUG7TLkP4YHaBE7J6e4dozTJVDpclxf3BejXwhtWq
lutcx2J8vhxzJTZtXxbh7AcjGgkRQRCS+J6Bd/7Dc1VkyZcdaGwfHj30QjOmcCEY
fu7VnahAAY+20hNkoD0k4GE4VerkUM4pqpQFkn6T+N+BMYbdDM73kqmZleU9ktNt
8odUOYK2y+GInAjtA7uwEQ==
-----END CERTIFICATE-----)***";
    
    
    std::string const key =
R"***(-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDnSQ4LbI4C1rR3
3626LGousZHSc7Prqm//wcRZhm1/lWgs5sOoygCZRtO9Z8uCGqFsPwR+ua4ngVRV
4TRIIM1x92wRwbspH1lAD3ApFoebMJKJDUXLMm/UFyxrcgv22QiiBxP6fEOzNAkk
/j5GGGU9TBnzO75dP09mrCkbXDM+gRIRnlDw5tmE+XJjbzj/3BDR6wbPrYAicVOo
XOrPY9imVzjNf6AvWYX/k4e4Wq9ZF/SA5dkzYF/KXRAfQ0N5Qsi3r1CNXGOxsHD6
B4RpV2C6RHc/Pwt2NBBtvdg9rFJUtDJaU/AXJEMTxeive+LN79jto6MKnUZ8kC6g
d4U+bVzDAgMBAAECggEBAIWg7u1ozfAT7vcnlC9xw/OwgqkOUpDBy64AXRrAkXla
cHMo7TkTZR8qDeJsGufTzRpGmqlBTL7keT6ZPGTEFw9WvejQfvt2clGR2eLTcowW
l0N6GK4bOQNYVEDMBt+ba+J5pnBd5ZufX/0wq/hB1hSBaKjhBs7Ham/O3n1BWxYr
0ThJ//Cb+ErZtWP7x/UM6CmzlYDYOSC4MGiF/Xq4TpSHLh6uxb6d28q3tXAR+eHa
8lEsZfsxEDdmMP+SCJD7l6nY5RyNnvc2D+bfSr2XTUYFE0uAqChlmkwhbSyfyZue
USFKa1QM3qsURMZAJ5xvs/cAX3/eXYAcHNMNTqgjJmECgYEA/nCcJTj8OlNKZ6XU
zIu59As0YxRLJt/jilg9o7qwF2aSoJRaj6mAAVG5FqarlkH1xRHQOr4lWOyd7Bwi
ku4vbv5m12DfYZeCXmTiIBnDdb4U08/MGmYuTaDkX3xcBbjIHVxIIOz5NzaqyKW1
hLPKecLrt9EkY96+Cgge4qqo7zECgYEA6LQZg7T4KGL9ny8mlV1gim7rKc96s5Qm
Bs4RS9J7IXGY3GoIhijBW+TJ5ZSa8bdcPxPnE3960BKzo0RJ5WnADx8XVDPOLPQo
HZHHILW1A2O/IPlPXkiv2JJsi5/Vim15wDNHBf+3Ih7sYa0F6fJQzQF9CP2uGPNH
4ITVX/rPljMCgYAZde9KEj+7HmaNpJx8s2ayMwTU/Uzf4PuF9nqEjcPdM5hslSvS
wGVcvG7HA3qcGJGdXMD3f9uA4HyVCeOzzD3W34Iu/zSbMWwMQhAWoj1a5OB0qZCE
+8CaaAUyQOF8eRcdSQS2tr/hdIj2dw6I7uzN7mFkDXuUus9Nda6Rg/5PoQKBgAad
cAg9cbx8K+eFiJxAXkkxJAA5RKysLnSNERLNtfBacVT6sqlVqUF6fH7JPapXYwjs
AOqd+hjGH1Mp0svxuKL1aE1dd6PxgN5uUXHNmIqzIDDIxYfRmyYV634AmipoOEjN
bFxwS31t0ZxpNbzTxZY+T5ac14EgEVABlqs2zwXfAoGBAPvZNOS83/PRmV3sx62D
DIgZljH/aOazVXLrZeLyO4WczFTJu36cdMEQBQZesV0PZuXPC5e/vmV5bvaq5O8m
T5XOsL0NqQ1AZWCtEU1kW9+w3V4bfCbrXtZScmV88qDLUQqUxJi7SNAs97eXO1Pz
aiItdb41vSJQoGMll3zFTaxC
-----END PRIVATE KEY-----)***";
    
    
    std::string const dh =
R"***(-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA0KoLKLFQbYcjUlGzg2Q+NwNQwHQ8JxwrdAwCxJdP6YhL6vlJTDG4
i4dXd5cXaPnvCb7v/tYJogRzFxkut+C4w2VHWkM2NLN43nxmRMmFW8I5DZtTJPve
7zTu9stnQrJqHLTZHogL0FQ04gn+hp2KT1mo91/2M5l60V0ruyEik5FHWEaN1uZL
yTrg6IDdjDUeEI1sWedLZUmDCtpEpVlkKhm6nqNtNjn6v9DEPbc9OoCUrFTpEmTn
v5LA0MU3+tsCC3FST9wrh9YiWQFHw81P7oRgPDgxB1PBFvOzrMvZHHHdas0FiQZT
o5mRfza8wFklH4ntH2C4fmFkgaQSThWuMwIBAg==
-----END DH PARAMETERS-----)***";
    
    ctx.set_password_callback(
                              [](std::size_t,
                                 boost::asio::ssl::context_base::password_purpose)
                              {
                                  return "test";
                              });
    
    ctx.set_options(
                    boost::asio::ssl::context::default_workarounds |
                    boost::asio::ssl::context::no_sslv2 |
                    boost::asio::ssl::context::single_dh_use);
    
    ctx.use_certificate_chain(
                              boost::asio::buffer(cert.data(), cert.size()));
    
    ctx.use_private_key(
                        boost::asio::buffer(key.data(), key.size()),
                        boost::asio::ssl::context::file_format::pem);
    
    ctx.use_tmp_dh(
                   boost::asio::buffer(dh.data(), dh.size()));
}

#endif

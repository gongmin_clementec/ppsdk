//
//  JobCreditAfterApprovalJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobCreditAfterApprovalJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "JMupsJobStorage.h"

JobCreditAfterApprovalJMups::JobCreditAfterApprovalJMups(const __secret& s) :
    JobCreditSalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobCreditAfterApprovalJMups::~JobCreditAfterApprovalJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobCreditAfterApprovalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    m_bIsMSFallback = false;
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.credit").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging(m_taggingSelector).as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobCreditAfterApprovalJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::AfterApproval;
}

/* virtual */
auto JobCreditAfterApprovalJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    return JobCreditSalesJMups::buildDispatchRequestParamForTradeInfo()
    .map([=](TradeInfoBuilder builder){
        builder.addApprovalNo(TradeInfoBuilder::attribute::required, boost::none);
        return builder;
    });
}


auto JobCreditAfterApprovalJMups::processDcc() -> pprx::observable<pprx::unit>
{
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");

    auto productCode = m_tradeData.get_optional<std::string>("productCode");
    if(!productCode) productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");

    auto slipNo = boost::make_shared<std::string>();
    auto terminalId = boost::make_shared<std::string>(getTerminalInfo().get<std::string>("sessionObject.termJudgeNo"));

        
    return hpsDHSLookup
    (
     m_tradeData.get<int>("amount"),
     taxOtherAmount ? (*taxOtherAmount) : 0,
     prmCommon::operationDiv::AfterApproval,
     false).as_dynamic()
    .flat_map([=](JMupsAccess::response_t dhsResp){
        auto dccTradeTargetJudgeResult = dhsResp.body.get<std::string>("normalObject.dccTradeTargetJudgeResult");
        if(dccTradeTargetJudgeResult == "0"){
            return pprx::just(pprx::unit()).as_dynamic();
        }

        return callOnDispatchWithCommandAndSubParameter("detectDcc", "hps", dhsResp.body).as_dynamic()
        .flat_map([=](const_json_t uiResp){
            const bool bDccTrade = uiResp.get<bool>("bDccTrade");
            if(!bDccTrade){
                return pprx::just(pprx::unit()).as_dynamic();
            }
            return pprx::start<TradeInfoBuilder>([=](){
                TradeInfoBuilder builder;
                builder.addOtherTermJudgeNo(TradeInfoBuilder::attribute::required, *terminalId);
                builder.addSlipNo(TradeInfoBuilder::attribute::required, *slipNo);
                return builder;
            }).as_dynamic()
            .flat_map([=](TradeInfoBuilder builder){
                return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "Search").as_dynamic();
            })
            .flat_map([=](const_json_t uiResp){
                *slipNo = uiResp.get<std::string>("results.slipNo");
                *terminalId = uiResp.get<std::string>("results.otherTermJudgeNo");
                return hpsCreditAfterApproval_DCCSearchTrade
                (
                 m_tradeData.get<std::string>("approvalNo"),
                 m_tradeData.get<int>("amount"),
                 taxOtherAmount ? (*taxOtherAmount) : 0,
                 *productCode,
                 *slipNo,
                 *terminalId
                 ).as_dynamic();
            }).as_dynamic()
            .flat_map([=](JMupsAccess::response_t hpsResp){
                auto searchResultDiv = hpsResp.body.get_optional<std::string>("normalObject.searchResultDiv");
                if(searchResultDiv && (*searchResultDiv == "1")){
                    return pprx::maybe::success(pprx::unit());
                }
                else{
                    return pprx::maybe::retry();
                }
            })
            .on_error_resume_next([=](auto err){
                return pprx::maybe::error(err);
            })
            .retry()
            .flat_map([=](pprx::maybe m){
                return m.observableForContinue<pprx::unit>().as_dynamic();
            }).as_dynamic()
            .flat_map([=](pprx::unit){
                return callOnDispatchWithMessage("beforeDccDescriptionPrint").as_dynamic();
            }).as_dynamic()
            .flat_map([=](pprx::unit){
                return slipPrinterExecute(dhsResp.body.get("normalObject.printInfo.device.value")).as_dynamic();
            }).as_dynamic()
            .flat_map([=](pprx::unit){
                return callOnDispatchWithCommandAndSubParameter("waitForDccSelect", "hps", dhsResp.body).as_dynamic();
            }).as_dynamic()
            .map([=](const_json_t json){
                const bool bDccTrade = json.get<bool>("bDccTrade");
                if(bDccTrade){
                    m_tradeData.put<bool>("bDccTrade", true);
                    std::string strDccAmount =
                    (
                     boost::format("%s %s") %
                     dhsResp.body.get<std::string>("normalObject.currentlyCode") %
                     dhsResp.body.get<std::string>("normalObject.fgnAmount")
                     ).str();
                    m_tradeData.put("strDccAmount", strDccAmount);
                }
                return pprx::unit();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditAfterApprovalJMups::sendTradeConfirm(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    const std::map<std::string, prmCredit::payWayDiv> lutPayWayDiv =
    {
        { "lump"        , prmCredit::payWayDiv::Lump },
        { "installment" , prmCredit::payWayDiv::Installment },
        { "bonus"       , prmCredit::payWayDiv::Bonus },
        { "bonusCombine", prmCredit::payWayDiv::BonusCombine },
        { "revolving"   , prmCredit::payWayDiv::Revolving }
    };
    
    auto it = lutPayWayDiv.find(m_tradeData.get<std::string>("paymentWay"));
    auto payWayDiv = it->second;
    
    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    if(!taxOtherAmount) taxOtherAmount = 0;
    auto productCode = m_tradeData.get_optional<std::string>("productCode");
    if(!productCode) productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
        
    return hpsCreditAfterApproval_Confirm
    (
     selector,
     (bDccTrade && (*bDccTrade)) ?
     prmCredit::dcc::Yes :
     prmCredit::dcc::No,
     m_tradeData.get<std::string>("approvalNo"),
     m_tradeData.get<int>("amount"),
     *taxOtherAmount,
     *productCode,
     payWayDiv,
     m_tradeData.get_optional<int>("partitionTimes"),
     m_tradeData.get_optional<int>("firsttimeClaimMonth"),
     false,         /* isRePrint */
     m_tradeData.get_optional<int>("couponSelectListNum"), /* couponSelectListNum */
     m_tradeData.get_optional<int>("couponApplyPreviousAmount"), /* couponApplyPreviousAmount */
     m_tradeData.get_optional<int>("couponApplyAfterAmount"), /* couponApplyAfterAmount */
     m_tradeData.get_optional<std::string>("firstGACCmdRes"),   /* firstGACCmdRes */
     m_tradeData.get_optional<int>("bonusTimes"),
     m_tradeData.get_optional<int>("bonusMonth1"),
     m_tradeData.get_optional<int>("bonusAmount1"),
     m_tradeData.get_optional<int>("bonusMonth2"),
     m_tradeData.get_optional<int>("bonusAmount2"),
     m_tradeData.get_optional<int>("bonusMonth3"),
     m_tradeData.get_optional<int>("bonusAmount3"),
     m_tradeData.get_optional<int>("bonusMonth4"),
     m_tradeData.get_optional<int>("bonusAmount4"),
     m_tradeData.get_optional<int>("bonusMonth5"),
     m_tradeData.get_optional<int>("bonusAmount5"),
     m_tradeData.get_optional<int>("bonusMonth6"),
     m_tradeData.get_optional<int>("bonusAmount6"),
     boost::none,   /* point */
     boost::none    /* exchangeablePoint */
    );
}

/* virtual */
auto JobCreditAfterApprovalJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmCredit::doubleTrade>
{
    LogManager_Function();
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(prmCredit::doubleTrade::No)
        .as_dynamic();
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmCredit::doubleTrade::Yes);
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return prmCredit::doubleTrade::Yes;
    })
    .as_dynamic();
}

/* virtual */
auto JobCreditAfterApprovalJMups::sendTagging(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsCreditAfterApproval_Tagging
        (
         selector,
         (bDccTrade && (*bDccTrade)) ?
         prmCredit::dcc::Yes :
         prmCredit::dcc::No,
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmCredit::printResultDiv::Success,
         boost::none,
         false
         );
    }).as_dynamic();
}

/* virtual */
void JobCreditAfterApprovalJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobCreditAfterApprovalJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "4");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

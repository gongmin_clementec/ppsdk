//
//  JMupsAccessNFCWithMasterDataTool.cpp
//  ppsdk
//
//  Created by tel on 2017/06/19.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessNFCWithMasterDataTool.h"
#include "JMupsJobStorage.h"
#include "CardReader.h"

auto JMupsAccessNFCWithMasterDataTool::masterDataDownload(bool bForceDownload)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    /* TerminalInfoから更新の必要性を取得する */
    auto ti = getTerminalInfo();
    if(ti.empty()){
        /* TerminalInfoが無ければ続行不能 */
        LogManager_AddError("terminal info not exists.");
        return pprx::error<pprx::unit>(make_error_internal("terminal info not exists"));
    }
    m_mdUpdateFlag.loadFromTerminalInfo(ti);
    
    /* TerminalInfoの情報を基本とするのだが、そもそもMasterDataが無ければ更新とする */
    auto md = getNfcMasterData();
    if(md.empty()){
        LogManager_AddInformation("master data not exists.");
        m_mdUpdateFlag.setAll(mdUpateFlag_t::State::Update);
    }
    else if(bForceDownload){
        LogManager_AddInformation("bForceDownload = true");
        m_mdUpdateFlag.setAll(mdUpateFlag_t::State::Update);
    }
    else{
        auto mdDateText = md.get_optional<std::string>("normalObject.updateDate");
        do{
            if(!mdDateText){
                LogManager_AddInformation("mdDateText == null");
                break;
            }
            if(!m_mdUpdateFlag.isNeedUpdate()){
                LogManager_AddInformation("m_mdUpdateFlag.isNeedUpdate == false");
                break;
            }
            if(m_mdUpdateFlag.UpdateDate.empty()){
                LogManager_AddInformation("m_mdUpdateFlag.UpdateDate is empty");
                break;
            }
            if(!m_mdUpdateFlag.isNeedUpdate()){
                LogManager_AddInformation("m_mdUpdateFlag.isNeedUpdate == false");
                break;
            }
            if(mdDateText->empty()){
                LogManager_AddInformation("mdDateText is empty");
                break;
            }
            if(*mdDateText == m_mdUpdateFlag.UpdateDate){
                /* 更新フラグは立っているものの、既にダウンロード済みの場合、更新日時が同じとなる */
                LogManager_AddInformation("mdDateText == m_mdUpdateFlag.UpdateDate -> State::None");
                m_mdUpdateFlag.setAll(mdUpateFlag_t::State::None);
                break;
            }
            LogManager_AddInformation("obey m_mdUpdateFlag.isNeedUpdate");
        } while(false);
    }

    if(m_mdUpdateFlag.isNeedUpdate()){
        LogManager_AddInformation("download & update master data.");
        
        const auto downloadType = m_mdUpdateFlag.isUpdateAll() ?
            prmMasterDataDownload::downloadType::Full :
            prmMasterDataDownload::downloadType::Difference;

        return hpsMasterDataDownload
        (
         prmNFCCommon::nfcDeviceType::Miura,
         downloadType
         )
        .flat_map([=](JMupsAccess::response_t resp){
            if(downloadType == prmMasterDataDownload::downloadType::Full){
                LogManager_AddInformation("downloadType::Full");
                getJMupsStorage()->setNfcMasterData(resp.body);
            }
            else{
                LogManager_AddInformation("downloadType::Difference");
                auto master = getJMupsStorage()->getNfcMasterData().clone();
                for(auto flag : m_mdUpdateFlag.Flags){
                    const std::string jpath = "normalObject.masterData." + m_mdUpdateFlag.whereToString(flag.first);
                    if(flag.second == mdUpateFlag_t::State::None){
                        LogManager_AddInformationF("%s -> State::None", jpath);
                    }
                    else if(flag.second == mdUpateFlag_t::State::Update){
                        LogManager_AddInformationF("%s -> State::Update", jpath);
                        auto data = resp.body.get_optional(jpath);
                        if(data){
                            master.put(jpath, *data);
                        }
                        else{
                            LogManager_AddErrorF("invalid download data -> %s not found", jpath);
                        }
                    }
                    else if(flag.second == mdUpateFlag_t::State::Delete){
                        LogManager_AddInformationF("%s -> State::Delete", jpath);
                        master.remove_key(jpath);
                    }
                    else{
                        LogManager_AddError("%s -> Bad state");
                    }
                }
                auto updateDate = resp.body.get_optional<std::string>("normalObject.updateDate");
                if(updateDate){
                    LogManager_AddInformationF("update normalObject.updateDate %s", *updateDate);
                    master.put("normalObject.updateDate", *updateDate);
                }
                else{
                    LogManager_AddError("updateDate not found");
                }
                LogManager_AddDebug(master.str());
                getJMupsStorage()->setNfcMasterData(master);
            }
            return pprx::just(pprx::unit());
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError(pprx::exceptionToString(err));
            return pprx::error<pprx::unit>(err).as_dynamic();
        }).as_dynamic();
    }
    else{
        return pprx::just(pprx::unit());
    }
}

auto JMupsAccessNFCWithMasterDataTool::masterDataUpdateCheck(CardReader::sp cardReader)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return cardReader->checkMasterData(getNfcMasterData())
    .flat_map([=](auto res){
        return pprx::just(res).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}


auto JMupsAccessNFCWithMasterDataTool::masterDataUpdate(CardReader::sp cardReader)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return cardReader->updateMasterData(getNfcMasterData())
    .flat_map([=](auto){
       return pprx::just(pprx::unit());
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto JMupsAccessNFCWithMasterDataTool::masterDataSendUpdateDate(const_json_t tradeStartInfo)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj&&(pj->isTrainingMode())){
        return pprx::just(pprx::unit());
    }
    
    if(m_mdUpdateFlag.isNeedUpdate()){
        auto info = tradeStartInfo.get("normalObject.nfcMasterDataUpdateInfo");
        return hpsUpdateMasterDataDate
        (
         prmNFCCommon::nfcDeviceType::Miura,
         info.get_optional<std::string>("kernelCommonFileUpdateDate"),
         info.get_optional<std::string>("kernel1FileUpdateDate"),
         info.get_optional<std::string>("kernel2FileUpdateDate"),
         info.get_optional<std::string>("kernel3FileUpdateDate"),
         info.get_optional<std::string>("kernel4FileUpdateDate"),
         info.get_optional<std::string>("kernel5FileUpdateDate"),
         info.get_optional<std::string>("icCardTableUpdateDate"),
         info.get_optional<std::string>("cakeyFile1UpdateDate"),
         info.get_optional<std::string>("merchantTableUpdateDate")
         )
        .flat_map([=](auto){
           return pprx::just(pprx::unit());
        }).as_dynamic();
    }
    else{
        return pprx::just(pprx::unit());
    }
}





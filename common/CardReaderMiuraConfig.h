//
//  CardReaderMiuraConfig.h
//  ppsdk
//
//  Created by tel on 2020/01/23.
//  Copyright © 2020年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_CardReaderMiuraConfig__)
#define __h_CardReaderMiuraConfig__

#include "BaseSystem.h"

class CardReaderMiuraConfig
{
public:
    CardReaderMiuraConfig();
    virtual ~CardReaderMiuraConfig();
    static auto createContactlessConfigData(const_json_t rootJson) -> std::string;
    static auto createContactlessPromptsData(const_json_t prompts) -> std::string;
    static auto retrieveConfigRevision(const bytes_t apdu, const std::string filename) -> std::string;
    static auto jsonToMd5String(const_json_t json) -> std::string;
    static auto writeMasterData(std::string filename, std::string writeData) -> void;

private:
    enum class AidOutSpec
    {
        AID,
        RID
    };

    static auto createContactlessConfigHeader(const_json_t rootJson) -> std::string;
    static auto createContactlessConfigCOMB(const_json_t masterJson) -> std::string;
    static auto createContactlessConfigCOMBVisa(const_json_t kernel) -> std::string;
    static auto createContactlessConfigCOMBMaster(const_json_t kernel) -> std::string;
    static auto createContactlessConfigCOMBAmex(const_json_t kernel) -> std::string;
    static auto createContactlessConfigCOMBJCB(const_json_t kernel) -> std::string;
    static auto createContactlessConfigTERM(const_json_t masterJson) -> std::string;
    static auto createContactlessConfigAPP(const_json_t masterJson) -> std::string;
    static auto createContactlessConfigAPPMaster(const_json_t master) -> std::string;
    static auto createContactlessConfigAPPVisa(const_json_t master) -> std::string;
    static auto createContactlessConfigAPPAmex(const_json_t master) -> std::string;
    static auto createContactlessConfigAPPJCB(const_json_t master) -> std::string;
    static auto createContactlessConfigDRL(const_json_t masterJson) -> std::string;
    static auto getAidFromAidInfo(const_json_t aidInfo, AidOutSpec spec = AidOutSpec::AID) -> std::string;
    static auto getKernelIdFromAidInfo(const_json_t aidInfo) -> std::string;
    static auto getTransactionTypeString(const std::string transactionType) -> std::string;
    static auto decimalStrToHexStr(const_json_t json, const std::string key) -> std::string;
    static auto getTlvValueString(const_json_t json, const std::string key) -> std::string;
    template <typename T> static auto getValue(const_json_t json, const std::string key) -> T;
    static auto getValue(const_json_t json, const std::string key) -> json_t;

protected:
};


#endif /* !defined(__h_CardReaderMiuraConfig__) */

//
//  JobSettingIssuers.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingIssuers__)
#define __h_JobSettingIssuers__

#include "PaymentJob.h"
#include "JMupsAccessSetting.h"
#include "JMupsSlipPrinter.h"

class JobSettingIssuers :
    public PaymentJob,
    public JMupsAccessSetting,
    public JMupsSlipPrinter
{
private:
    json_t m_settingIssuers;
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;
    
    auto waitForSettingIssuersConfirm() -> pprx::observable<pprx::unit>;
    
public:
            JobSettingIssuers(const __secret& s);
    virtual ~JobSettingIssuers();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobSettingIssuers__) */

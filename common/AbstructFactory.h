//
//  PPAbstructFactory.h
//  ppsdk
//
//  Created by Clementec on 2017/05/25.
//
//

#if !defined(__h_AbstructFactory__)
#define __h_AbstructFactory__

#include "BaseSystem.h"

class WebClient;
class CardReader;
class SlipPrinter;
class JavascriptEnvironment;

class AbstructFactory : public Singleton<AbstructFactory>
{
friend class Singleton<AbstructFactory>;

private:
            AbstructFactory() = default;
    virtual ~AbstructFactory() = default;
    
protected:
    
public:
    boost::function<boost::shared_ptr<WebClient>()> createWebClient;
    boost::function<boost::shared_ptr<WebClient>()> createWebClientForJMups;
    boost::function<BaseSystem::imp*()> createBaseSystemImp;
    boost::function<boost::shared_ptr<CardReader>()> createCardReader;
    boost::function<boost::shared_ptr<SlipPrinter>()> createSlipPrinter;
    boost::function<boost::shared_ptr<JavascriptEnvironment>()> createJavascriptEnvironment;
};



#endif /* !defined(__h_AbstructFactory__) */

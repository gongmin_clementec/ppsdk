//
//  TMSAccess.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_TMSAccess__)
#define __h_TMSAccess__

#include "WebClient.h"

class TMSAccess :
    public LazyObject
{
public:
    using sp = boost::shared_ptr<TMSAccess>;
    
private:
    std::string m_serverBaseUrl;

    std::string m_token;
    std::string m_tokenType;
    
    static boost::recursive_mutex   s_file_mtx;

    auto execute(const std::string& path, const_json_t json) -> pprx::observable<const_json_t>;

    void initialize(const std::string& serverBaseUrl);
    auto registerUser(const std::string& userName) -> pprx::observable<const_json_t>;
    auto login(const std::string& userName, const std::string& password) -> pprx::observable<const_json_t>;

protected:
    
public:
            TMSAccess(const __secret& s);
    virtual ~TMSAccess();

    static sp create(){
        return LazyObject::create<TMSAccess>();
    }

    auto initializeAndLoginAndUpdate() -> pprx::observable<const_json_t>;

    auto update(const_json_t json) -> pprx::observable<const_json_t>;

    auto logBegin() -> pprx::observable<const_json_t>;
    auto logEnd(const std::string& LogId) -> pprx::observable<const_json_t>;

    auto blobDownload(const std::string& url, const boost::filesystem::path& path) ->  pprx::observable<pprx::unit>;
    auto blobUpload(const std::string& url, const boost::filesystem::path& path) ->  pprx::observable<pprx::unit>;
    
    static void setConnectedCardReaderInfo(const_json_t info);
    static json_t getConnectedCardReaderSerial();
    static void setMasterDataCheckInfo(const_json_t checkInfo);
    static json_t getMasterDataCheckInfo();
};

#endif /* !defined(__h_TMSAccess__) */

//
//  CardReaderMiura.cpp
//  ppsdk
//
//  Created by tel on 2018/10/24.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "CardReaderMiura.h"
#include "PPConfig.h"
#include <boost/timer/timer.hpp>
#include <boost/algorithm/hex.hpp>
#include <boost/format.hpp>
#include <boost/regex.hpp>
#include "TMSAccess.h"
#include "SerialDevice.h"
#include "CardReaderMiuraConfig.h"

class TLV
{
private:
    bytes_t m_data;
protected:
public:
    void appendBytes(const bytes_t& tag, const bytes_t& value)
    {
        m_data.insert(m_data.end(), tag.begin(), tag.end());
        m_data.push_back(value.size());
        m_data.insert(m_data.end(), value.begin(), value.end());
    }
    
    void appendString(const bytes_t& tag, const std::string& value)
    {
        m_data.insert(m_data.end(), tag.begin(), tag.end());
        m_data.push_back(value.size());
        m_data.insert(m_data.end(), value.begin(), value.end());
    }
    
    /* M0000-MPI-V1-36-API-V1 */
    /* 5.2.1 Command APDU */
    bytes_t buildCommandApduBody(uint8_t command) const
    {
        bytes_t dataExtLen;
        pushLength(m_data.size(), dataExtLen);
        
        bytes_t result;
        result.push_back(m_data.size() + 1 + dataExtLen.size());
        result.push_back(command);
        result.insert(result.end(), dataExtLen.begin(), dataExtLen.end());
        result.insert(result.end(), m_data.begin(), m_data.end());
        result.push_back(0);
        return result;
    }
    
    static void pushLength(size_t length, bytes_t& ioData)
    {
        int lenBytes = 0;
        if(length >= 128) lenBytes++;
        if(length >= 256) lenBytes++;
        if(lenBytes > 0){
            ioData.push_back(0x80 + lenBytes);
            for(int i = lenBytes - 1; i >= 0; i--){
                uint8_t d = static_cast<uint8_t>((length >> (8 * i)) & 0xFF);
                ioData.push_back(d);
            }
        }
        else{
            ioData.push_back(length);
        }
    }
    
    const bytes_t& data() const  { return m_data; }
};

CardReaderMiura::CardReaderMiura(const __secret& s) :
    CardReader(s)
{
    LogManager_Function();
}

/* virtual */
CardReaderMiura::~CardReaderMiura()
{
    LogManager_Function();
    
    m_abortRWcommunication->get_subscriber().on_completed();
}


auto CardReaderMiura::RWcommunication
(
 const std::string               debugId,
 boost::function<void()>         action /* = action = boost::function<void()>() */,
 const std::vector<bytes_t>&     retrieveTags /* = {} */,
 ReadUnsolicited                 isUnsolicitedRead /* = ReadUnsolicited::Yes */,
 boost::function<bool(bytes_sp)> predicate /* = boost::function<bool(bytes_sp)>() */,
 boost::function<void(void)>     cleanup   /* = boost::function<void(void)>() */
 ) -> pprx::observable<bytes_sp>
{
    auto ios = m_ios;
    auto packet = boost::make_shared<bytes_t>();
    auto bEmited = boost::make_shared<bool>(false);
    auto blocking = boost::make_shared<boost::recursive_mutex>();
    
    static int debugIdN = 0;
    debugIdN++;

    return pprx::observable<>::create<bytes_sp>([=](pprx::subscriber<bytes_sp> s){
        if(!ios){
            LogManager_AddWarning("eastream has been deleted.");
            s.on_error(make_error_cardrw(makeErrorJson("disconnected")));
            return;
        }

        pprx::subject<pprx::unit> until;

        m_recv->get_observable()
        .observe_on(pprx::observe_on_thread_pool())
        .take_until(until.get_observable())
        .take_until(m_abortRWcommunication->get_observable())
        .subscribe
        (
         [=](bytes_sp data){
             LogManager_ScopedF("RWcommunication [%d:%s] subscribe next", debugIdN % debugId);
             boost::unique_lock<boost::recursive_mutex> lock(*blocking);
             packet->insert(packet->end(), data->begin() + 3, data->end() - 1);
             switch(standardPredicate(data, packet, retrieveTags, isUnsolicitedRead)) {
                 case (PredicateResult::Continue):
                     // 後続データ待ち
                     break;
                 case (PredicateResult::Reject):
                     // 欲しくないデータ、次のデータを待つ
                     packet->clear();
                     break;
                 case (PredicateResult::Go):
                     // Response Messages
                 {
                     if(*bEmited){
                         LogManager_AddInformationF("RWcommunication [%d:%s] Emited", debugIdN % debugId);
                         break;
                     }
                     LogManager_AddInformationF("RWcommunication [%d:%s] PredicateResult::Go", debugIdN % debugId);
                     bool bEmitAndFinish = false;
                     {
                         if(predicate) {
                             bEmitAndFinish = predicate(packet);
                         }
                         else{
                             bEmitAndFinish = true;
                         }
                     }
                     if(bEmitAndFinish){
                         LogManager_AddInformationF("RWcommunication [%d:%s] emit and finish ---- begin", debugIdN % debugId);
                         *bEmited = true;
                         /*
                            ここはblockingの保護区間なので、ここで s.on_next() をやると、
                            flat_mapで接続している後続処理を「長時間」同期待ちすることになるため、
                            このsubscribeのcompletedで s.on_next() をすることとした。
                          */
                         until.get_subscriber().on_next(pprx::unit());
                         until.get_subscriber().on_completed();
                         LogManager_AddInformationF("RWcommunication [%d:%s] emit and finish ---- end", debugIdN % debugId);
                     }
                     else{
                         LogManager_AddInformationF("RWcommunication [%d:%s] retry", debugIdN % debugId);
                         packet->clear();
                     }
                     break;
                 }
             }
         },
         [=](auto err){
             LogManager_ScopedF("RWcommunication [%d:%s] subscribe error", debugIdN % debugId);
             try{ std::rethrow_exception(err); }
             catch(SerialDevice::error::disconnected&){
                 s.on_error(make_error_cardrw(makeErrorJson("disconnected")));
             }
             catch(SerialDevice::error::open&){
                 s.on_error(make_error_cardrw(makeErrorJson("deviceOpenError")));
             }
             catch(pprx::ppex_cardrw&){
                 s.on_error(err);
             }
             catch(...){
                 s.on_error(make_error_cardrw(makeErrorJson("general")));
             }
         },
         [=](){
             LogManager_ScopedF("RWcommunication [%d:%s] subscribe completed", debugIdN % debugId);
             BaseSystem::instance().post([=](){
                 if(*bEmited){
                     LogManager_ScopedF("RWcommunication [%d:%s] emit data", debugIdN % debugId);
                     s.on_next(packet);
                     s.on_completed();
                 }
                 else{
                     LogManager_AddErrorF("RWcommunication [%d:%s] emit data empty -> do nothing", debugIdN % debugId);
                     if(cleanup) {
                         LogManager_AddInformationF("RWcommunication [%d:%s] call cleanup method", debugIdN % debugId);
                         cleanup();
                     }
                     s.on_completed();
                 }
             });
         });
        
        if(action) action();
    });
}

auto CardReaderMiura::standardPredicate(const bytes_sp& rawBuffer, const bytes_sp& readBuffer, const std::vector<bytes_t>& retrieveTags, ReadUnsolicited isUnsolicitedRead) -> PredicateResult
{
    LogManager_Function();
    auto header = bytes_t(rawBuffer->begin(), rawBuffer->begin() + 3);
    /* Prologue LRC strip */
    if((header[0] & 0xFF) == 0) {
        LogManager_AddInformation("standardPredicate. NDA 0xFF case. Reject : wait for next emit.");
        return PredicateResult::Reject;
    } else if((header[1] & 0x01) != 0) {
        LogManager_AddInformation("standardPredicate. LSB of PCB not 0. Continue : wait for continue emit.");
        /* PCBのLSBが0でない場合、後続データがあるので次を待つ */
        return PredicateResult::Continue;
    } else if((header[1] & 0x40) == 0) {
        LogManager_AddInformation("standardPredicate. PCB 0x40. Response Message Detedted. Go : Go emit.");
        return PredicateResult::Go;
    }
    // リクルート銀聯対応:
    // SW=6F00はMiuraの「不正パケット」エラーなのでReadUnsolicited::Noであっても呼び出し元に返し、エラーに流れるよう仕向ける
    auto sw = CardReaderUtil::retrieveSW1SW2(*readBuffer);
    if(sw == "6F00") {
        LogManager_AddInformation("standardPredicate. invalid packet error(0x6F00).");
        return PredicateResult::Go;
    }
    // Unsolicited Response Messages
    if(isUnsolicitedRead == ReadUnsolicited::No) {
        LogManager_AddInformation("standardPredicate. ReadUnsolicited::No.");
        return PredicateResult::Reject;
    } else if(retrieveTags.size() == 0) {
        // タグ指定なしの場合はなんでも返す
        LogManager_AddInformation("standardPredicate. ReadUnsolicited::Yes.");
        return PredicateResult::Go;
    }
    // タグ指定ありの場合、指定されたタグを含むデータなら返す
    bytes_t tagdata;
    for(bytes_t tag : retrieveTags) {
        std::string text;
        tagdata = CardReaderUtil::getTLVDataFromRAPDU(*readBuffer, tag);
        if(tagdata.size() != 0) {
            LogManager_AddInformation("tagCheckPredicate. PredicateResult::Go.");
            return PredicateResult::Go;
        }
    }
    // 欲しいタグを含むデータを取得できなかったら次を待つ
    LogManager_AddInformation("tagCheckPredicate. PredicateResult::Reject.");
    return PredicateResult::Reject;
}

// カードリーダーから複数の応答が混ざって返ってくることがある。
// プロローグの長さを元にこの関数で整理して値を発行し直す。
auto CardReaderMiura::beginRecvTrafficController() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto ios = m_ios;
    auto restbuf = boost::make_shared<bytes_t>();
    auto blocking = boost::make_shared<boost::recursive_mutex>();

    m_recv = boost::make_shared<pprx::subject<bytes_sp>>();
    if(!ios){
        LogManager_AddWarning("eastream has been deleted.");
        m_recv->get_subscriber().on_error(make_error_cardrw(makeErrorJson("disconnected")));
        return pprx::just(pprx::unit());
    }
    
    ios->getReadObservable()
    .observe_on(pprx::observe_on_thread_pool())
    .subscribe
    (
        [=](bytes_sp data){
            LogManager_Scoped("beginRecvTrafficController subscribe next");
            boost::unique_lock<boost::recursive_mutex> lock(*blocking);
            // 今回使用するバッファ
            bytes_t usebuf;
            if(restbuf->size() != 0) {
                usebuf.insert(usebuf.end(), restbuf->begin(), restbuf->end());
                restbuf->clear();
                LogManager_AddInformation("蓄積データ取り出し");
            }
            usebuf.insert(usebuf.end(), data->begin(), data->end());
            if(usebuf.size() < 4) {
                // データサイズが3未満だと長さがわからないので次に回す
                restbuf->insert(restbuf->end(), usebuf.begin(), usebuf.end());
                LogManager_AddInformationF("短すぎるデータ \"%s\" ", CardReaderUtil::binaryToHextext(*restbuf));
                return;
            }
            // Prologue + APDU + LRC
            std::uint32_t firstpacketlen = usebuf.operator[](2) + 3 + 1;
            LogManager_AddInformationF("firstpacketlen = %d", firstpacketlen);
            if(usebuf.size() == firstpacketlen) {
                LogManager_Scoped("OK");
                BaseSystem::instance().post([=]() {
                    auto p = boost::make_shared<bytes_t>(usebuf.size());
                    std::copy(usebuf.begin(), usebuf.end(), p->begin());
                    LogManager_AddInformationF("emit \"%s\" ", CardReaderUtil::binaryToHextext(*p));
                    m_recv->get_subscriber().on_next(p);
                });
            } else if(usebuf.size() > firstpacketlen) {
                LogManager_AddInformation("複数パケット同時に受けた");
                std::uint32_t i = 0;
                for(i = 0; i < usebuf.size(); ) {
                    if((usebuf.size() - i) < 4) {
                        LogManager_AddInformation("短すぎのため積み残し");
                        break;
                    }
                    std::uint32_t packetlen = usebuf[i + 2] + 3 + 1;
                    auto p = boost::make_shared<bytes_t>(packetlen);
                    std::copy(usebuf.begin() + i, usebuf.begin() + i + packetlen, p->begin());
                    LogManager_AddInformationF("emit \"%s\" ", CardReaderUtil::binaryToHextext(*p));
                    m_recv->get_subscriber().on_next(p);
                    i += packetlen;
                }
                if(usebuf.size() > i) {
                    // 積み残しがあったら取っておく
                    restbuf->insert(restbuf->end(), usebuf.begin() + i, usebuf.end());
                    LogManager_AddInformationF("積み残しあり = [%s}", CardReaderUtil::binaryToHextext(*restbuf));
                }
            } else if(usebuf.size() < firstpacketlen) {
                LogManager_AddInformation("不完全なバッファ受信、次へ");
                // 不完全なバッファの場合は貯めておき、次のデータと連結して使う
                restbuf->insert(restbuf->end(), usebuf.begin(), usebuf.end());
                LogManager_AddInformationF("現状バッファ \"%s\" ", CardReaderUtil::binaryToHextext(*restbuf));
            }
        },
        [=](auto err) {
            LogManager_Scoped("beginRecvTrafficController subscribe error");
            m_recv->get_subscriber().on_error(err);
        },
        [=]() {
            LogManager_Scoped("beginRecvTrafficController subscribe completed");
            m_recv->get_subscriber().on_completed();
        }
    );
    return pprx::just(pprx::unit());
}

void CardReaderMiura::_sendPrologueAndAPDU(const bytes_t& prologue, const bytes_t& header, const bytes_t& trailer)
{
    auto iosession = m_ios;
    if(!iosession) {
        throw Exception("disconnected", APIERROR::NO_DEVICE);
    }
    std::stringstream ss;
    
    ss.clear();
    ss.put(prologue[0]);
    ss.put(prologue[1]);
    // LENは自動計算
    ss.put(header.size() + trailer.size());
    
    ss.write(reinterpret_cast<const char*>(header.data()), header.size());
    if(trailer.size() > 0){
        ss.write(reinterpret_cast<const char*>(trailer.data()), trailer.size());
    }
    
    // LRC 計算
    uint8_t lrc = 0x00;
    
    lrc ^= prologue[0];
    lrc ^= prologue[1];
    lrc ^= static_cast<uint8_t>(header.size() + trailer.size());
    
    for(auto ch : header){
        lrc ^= ch;
    }
    for(auto ch : trailer){
        lrc ^= ch;
    }
    
    ss.put(lrc);
    ss.seekp(0, std::ios::end);
    const auto length = ss.tellp();
    
    auto p = boost::make_shared<bytes_t>(length);
    ss.read(reinterpret_cast<char*>(p->data()), length);
    iosession->writeAsync(p);
}

void CardReaderMiura::_sendRawData(bytes_sp data)
{
    auto iosession = m_ios;
    if(!iosession) {
        throw Exception("disconnected", APIERROR::NO_DEVICE);
    }
    iosession->writeAsync(data);
}

auto CardReaderMiura::_displayText(ERows rows, EBackLight backLight, const std::string& str) -> pprx::observable<pprx::unit>
{
    return RWcommunication(
       "_displayText#1",
        [=] {
            //const uint8_t p1 = (rows == ERows_3) ? 0x01 : 0x00;
            const uint8_t p1 = 0x00;
            const uint8_t p2 = (backLight == EBackLight::On) ? 0x81 : 0x80;
            
            const bytes_t header{ 0xD2, 0x01, p1, p2};
            
            bytes_t body;
            body.push_back(str.length() + 1);
            body.push_back('\n');
            body.insert(body.end(), str.begin(), str.end());

            send({0x01, 0x00}, header, body); 
        },
       {{}},
       ReadUnsolicited::No
    )
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
void CardReaderMiura::prepareObservablesSelf()
{
    LogManager_Function();
    // 取引に使用するカード種をクリア
    m_cardType = CardType::NotDicided;
    m_lockTransactionState = boost::make_shared<pprx::subjects::behavior<bool>>(false);
}

/* virtual */
auto CardReaderMiura::connect() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return connectWithOption(500)
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::connectWithOption(int retryMS) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        BaseSystem::instance().postWithTimeout(PPConfig::instance().get<int>("miura.connectTimeoutSecond"), [=](){
            s.on_error(std::make_exception_ptr(pprx::timeout_error("timeout")));
        });
        s.on_next(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto){
        return observableDeviceConnectState();
    }).as_dynamic()
    .observe_on(pprx::observe_on_thread_pool())
    .distinct_until_changed()
    .flat_map([=](DeviceConnectState state){
        LogManager_AddInformationF("[RW] get serial device status %s", deviceConnectStateToString(state));
        if(state == DeviceConnectState::Connected){
            LogManager_AddInformation("[RW] emit connected -> proceed next");
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else if(state == DeviceConnectState::Connecting){
            LogManager_AddInformation("[RW] connecting -> stay");
            return pprx::never<pprx::unit>().as_dynamic();
        }
        else{
            LogManager_AddInformation("[RW] do connect");
            auto protocol   = PPConfig::instance().get<std::string>("miura.ea.protocol");
            auto name       = PPConfig::instance().get<std::string>("miura.ea.name");
            auto serial     = PPConfig::instance().get<std::string>("miura.ea.serial");
            
            return pprx::just(pprx::unit())
            .flat_map([=](auto){
                auto old_ios = m_ios;
                if(old_ios){
                    LogManager_AddInformation("[RW] close serial device before create");
                    return old_ios->close().as_dynamic();
                }
                else{
                    return pprx::just(pprx::unit()).as_dynamic();
                }
            }).as_dynamic()
            .flat_map([=](auto){
                m_ios.reset();
                LogManager_AddInformation("[RW] create serial device");
                m_ios = BaseSystem::instance().createSerialDevice();
                /* ここでのsetDeviceConnectState()によって上にあるobservableDeviceConnectState()が
                 反応して再入する */
                LogManager_AddInformation("[RW] state -> DeviceConnectState::Connecting");
                setDeviceConnectState(DeviceConnectState::Connecting);
                return pprx::just(pprx::unit());
            }).as_dynamic()
            .flat_map([=](auto){
                LogManager_AddInformation("[RW] beginRecvTrafficController");
                return beginRecvTrafficController();
            }).as_dynamic()
            .flat_map([=](auto) {
                json_t param;
                param.put("protocol", protocol);
                param.put("name", name);
                param.put("serial", serial);
                LogManager_AddInformationF("[RW] open serial device - protocol: \"%s\", name: \"%s\", serial: \"%s\"", protocol % name % serial);
                return m_ios->open(param);
            }).as_dynamic()
            .flat_map([=](auto x) {
                LogManager_AddInformation("[RW] beginWatchingCardReaderState");
                // これ以降のカード抜き検知処理を起動
                beginWatchingCardReaderState();
                LogManager_AddInformation("[RW] state -> DeviceConnectState::Connected");
                setDeviceConnectState(DeviceConnectState::Connected);
                return pprx::never<pprx::unit>().as_dynamic();
            }).as_dynamic()
            .on_error_resume_next([=](auto err){
                LogManager_AddWarning("[RW] error at inner -> next emit (retry)");
                return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
                    LogManager_AddInformationF("[RW] wait %dms", retryMS);
                    BaseSystem::sleep(retryMS);
                    LogManager_AddInformation("[RW] state -> DeviceConnectState::Bad");
                    setDeviceConnectState(DeviceConnectState::Bad);
                    s.on_next(pprx::unit());
                }).as_dynamic()
                .flat_map([=](auto){
                    return pprx::never<pprx::unit>().as_dynamic();
                });
            }).as_dynamic();
        }
        return pprx::never<pprx::unit>().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return pprx::maybe::success(pprx::unit());
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddInformation("[RW] error at outer");
        return pprx::maybe::error(err);
    }).as_dynamic()
    .take(1)
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddInformation("[RW] state -> DeviceConnectState::Bad");
        setDeviceConnectState(DeviceConnectState::Bad);
        try{ std::rethrow_exception(err); }
        catch(pprx::timeout_error&){
            LogManager_AddError("[RW] timeout error");
            return pprx::error<pprx::unit>(make_error_cardrw(makeErrorJson("connetTimeout"))).as_dynamic();
        }
        catch(...) {}
        return pprx::never<pprx::unit>().as_dynamic();
    }).as_dynamic();
}


/* virtual */
auto CardReaderMiura::disconnect() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto eas = m_ios;
    return pprx::just(pprx::unit())
    .flat_map([=](pprx::unit){
        if(eas){
            LogManager_AddInformation("close eastream");
            eas->close()
            .subscribe
            (
             [=](auto) {},
             [=](auto) {},
             [=]() {}
             );
            setDeviceConnectState(DeviceConnectState::Disconnected);
        }
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto){
        return observableDeviceConnectState()
        .flat_map([=](DeviceConnectState state){
            LogManager_AddInformationF("DeviceConnectState is %s", deviceConnectStateToString(state));
            if((state == DeviceConnectState::Disconnected) || (state == DeviceConnectState::Bad)){
                LogManager_AddInformation("detect disconnected or bad -> loop finish");
                return pprx::just(pprx::unit()).as_dynamic();
            }
            return pprx::never<pprx::unit>().as_dynamic();
        });
    }).as_dynamic()
    .take(1)
    .map([=](auto){
        LogManager_AddInformation("delete m_ios");
        m_ios.reset();
        return pprx::unit();
    });
}

auto CardReaderMiura::send(const bytes_t& prologue,const bytes_t& header, const bytes_t& trailer) -> void
{
    _sendPrologueAndAPDU(prologue, header, trailer);
}

/* virtual */
auto CardReaderMiura::beginCardRead(const_json_t param) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    m_waitStartContactlessState = boost::make_shared<pprx::subject<pprx::unit>>();
    uint8_t waitMedia = [=]() -> uint8_t {
        uint8_t       r  = 0x01;
        auto&& bEnableIC = param.get<bool>("media.ic");
        auto&& bEnableMS = param.get<bool>("media.ms");
        if(bEnableIC) r |= 0x02;
        if(bEnableMS) r |= 0x20;
        return r;
    }();
    m_isCardReadTerminate = false;
    auto waitParam = param.clone();
    LogManager_AddInformation("get media kind.");
    waitParam.put("waitMedia", CardReaderUtil::binaryToHextext({waitMedia}));
    return RWcommunication(
        "beginCardRead#1",
        [=] { send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {}); },
        {{}},
        ReadUnsolicited::No
    )
    .flat_map([=](bytes_sp resetrapdu) {
        m_tag_9f1e = CardReaderUtil::getTLVDataFromRAPDU(*resetrapdu, {0x9F, 0x1E});
        m_RWInfo = bytes_t(resetrapdu->begin(), resetrapdu->end());
        LogManager_AddInformationF("Interface Device Serial Number = \"%s\".", CardReaderUtil::binaryToHextext(m_tag_9f1e));
        // CARD STATUS 設定クリア
        return RWcommunication(
            "beginCardRead#2",
            [=] { send({0x01, 0x00}, {0xD0, 0x60, 0x00, 0x00}, {}); }
        ).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        // 磁気、IC、NFCの3面待ち
        return waitForNFCCardDetect(waitParam.as_readonly()).as_dynamic()
        .amb(pprx::observe_on_thread_pool(), waitForMagOrIcCardDetect(waitParam).as_dynamic()).as_dynamic()
        .flat_map([=](pprx::maybe m) {
            return m.observableForContinue<const_json_t>();
        }).as_dynamic()
        .flat_map([=](const_json_t cardInfo) {
            return beginCardReadCleanUp(param, cardInfo)
            .flat_map([=](auto) {
                return pprx::just(cardInfo).as_dynamic();
            }).as_dynamic();
        }).as_dynamic()
        .on_error_resume_next([=](auto err) {
            return beginCardReadCleanUp(param, json_t())
            .flat_map([=](auto) {
                try{std::rethrow_exception(err);}
                catch(pprx::ppex_internal& ppex){
                    return pprx::error<const_json_t>(ppex).as_dynamic();
                }
                catch(pprx::ppex_aborted& ppex){
                    return pprx::error<const_json_t>(ppex).as_dynamic();
                }
                catch(pprx::ppex_cardrw& rwerr){
                    return pprx::error<const_json_t>(rwerr).as_dynamic();
                }
                catch(...){
                }
                return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general"))).as_dynamic();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::beginCardReadCleanUp(const_json_t param, const_json_t cardInfo) -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        m_isCardReadTerminate = true;
        auto cardType = cardInfo.get_optional<std::string>("cardType");
        auto isTraining = param.get_optional<bool>("childParam.isTraining");
        if(cardType && (*cardType == "magneticStripe" || *cardType == "contact")) {
            auto&& bEnableNFC = param.get<bool>("media.nfc");
            if(!bEnableNFC) {
                // NFC無効の場合はSTART CONTACTLESS TRANSACTIONを発行しないので何もせずに終了する
                return pprx::just(bytes_sp()).as_dynamic();
            }
            // 磁気、ICを検出した場合はNFCの処理を止めるためにABORTを発行する
            return RWcommunication(
                "beginCardReadCleanUp#2",
                [=] {
                    LogManager_AddInformation("exec ABORT command.");
                    send({0x01, 0x00}, {0xD0, 0xFF, 0x00, 0x00}, {});
                }
            ).as_dynamic();
        } else {
            return RWcommunication(
                "beginCardReadCleanUp#3",
                [=] {
                    // CARD STATUS 設定クリア
                    send({0x01, 0x00}, {0xD0, 0x60, 0x00, 0x00}, {}); 
                }, {{0x48}}
            ).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto CardReaderMiura::waitForMagOrIcCardDetect(const_json_t param) -> pprx::observable<pprx::maybe>
{
    LogManager_Function();
    auto waitMediaStr = param.get<std::string>("waitMedia");
    bytes_t temp;
    boost::algorithm::unhex(waitMediaStr.begin(), waitMediaStr.end(), std::back_inserter(temp));
    auto waitMedia = temp[0];
    LogManager_AddDebugF("param. %s", param.str());
    LogManager_AddInformationF("waitMedia \"%s\".", CardReaderUtil::binaryToHextext({waitMedia}));
    auto retJsonsp = boost::make_shared<json_t>();
    auto detect48 = [=](bytes_sp data) -> bool {
        const bytes_t tag_48 = CardReaderUtil::getTLVDataFromRAPDU(*data, {0x48});
        LogManager_AddInformationF("tag_48 \"%s\" ", CardReaderUtil::binaryToHextext(tag_48));
        if(m_isCardReadTerminate) {
            return true;
        }
        if(tag_48.size() >= 2){
            if(((tag_48[0] & 0x01) == 0x01) && ((waitMedia & 0x02) != 0)) {
                // Byte 1 Bit 0 Card Present : ICC
                if((tag_48[0] & 0x02) == 0x02) {    /* #3983 カードへ検出の判断を変更(EMV Compatible追加) */
                    // Byte 1 Bit 1 EMV Compatible : ICC
                    LogManager_AddInformation("IC card inserted and enable.");
                    retJsonsp->put("cardType", "contact");
                    retJsonsp->put("status", "approved");
                    setICCardSlotState(ICCardSlotState::Inserted);
                    m_cardType = CardType::IC;
                }
                return true;
            } else if((tag_48[1] & 0x04) && ((waitMedia & 0x20) != 0)) {
                // Byte 2 Bit 2 Track 2 available : Mag
                LogManager_AddInformation("mag stripe swiped and enable.");
                m_tag_dfae22 = CardReaderUtil::getTLVDataFromRAPDU(*data, {0xDF, 0xAE, 0x22});
                retJsonsp->put("cardType", "magneticStripe");
                retJsonsp->put("status", "approved");
                retJsonsp->put("keySerialNumber",
                    CardReaderUtil::binaryToHextext(CardReaderUtil::getTLVDataFromRAPDU(*data, {0xDF, 0xAE, 0x03}))
                );
                retJsonsp->put("encryptedTrackData",
                    CardReaderUtil::binaryToHextext(CardReaderUtil::getTLVDataFromRAPDU(*data, {0xDF, 0xAE, 0x02}))
                );
                m_cardType = CardType::Magstripe;
                return true;
            }
            BaseSystem::instance().post([=](){
                LogManager_AddInformation("emit go contactless.");
                m_waitStartContactlessState->get_subscriber().on_next(pprx::unit());
            });
        }
        return false;
    };
    // ICカード挿入、または磁気カードスワイプ検知用のCARD STATUS 設定
    return RWcommunication(
        "waitForMagOrIcCardDetect#1",
        [=] {
            send({0x01, 0x00}, {0xD0, 0x60, waitMedia, 0x00}, {});
        },
        {{0x48}}, ReadUnsolicited::Yes,
        [=](bytes_sp buf) {
            return detect48(buf);
        }
    )
    .flat_map([=](auto) {
        // detect48()で応答の解析は済んでいるので解析時に取得したJSONを返すだけ
        if(m_cardType == CardType::NotDicided) {
            // ここまできてカードタイプが決まってない場合はエラーとする
            // ICカードの裏挿などの場合に発生する。
            return pprx::maybe::error(make_error_cardrw(makeErrorJson("disableCardInserted"))).as_dynamic();
        } else if(m_cardType == CardType::Magstripe) {
            // Magstripeの場合、用が済んだらCARD STATUS 設定クリア
            return RWcommunication(
                "waitForMagOrIcCardDetect#2",
                [=] { send({0x01, 0x00}, {0xD0, 0x60, 0x00, 0x00}, {}); },
                {{0x48}}
            )
            .flat_map([=](auto) {
                return pprx::maybe::success(retJsonsp->as_readonly());
            }).as_dynamic();
        }
        return pprx::maybe::success(retJsonsp->as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::setICCardRemoved()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    setICCardSlotState(ICCardSlotState::Removed);
    return pprx::just(pprx::unit());
}

auto CardReaderMiura::waitForNFCCardDetect(const_json_t param) -> pprx::observable<pprx::maybe>
{
    LogManager_Function();
    LogManager_AddDebugF("in waitForNFCCardDetect. param = %s", param.str());
    do {
        auto&& bEnableNFC = param.get<bool>("media.nfc");
        if(!bEnableNFC) {
            // NFC無効の場合はSTART CONTACTLESS TRANSACTION不要
            LogManager_AddInformation("nfc transaction disable.");
            return pprx::never<pprx::maybe>().as_dynamic();
        }
        auto&& childParam = param.get_optional("childParam");
        if(!childParam) {
            LogManager_AddInformation("childParam none.");
            return pprx::never<pprx::maybe>().as_dynamic();
        }
        auto&& bValidChildJob = childParam->get<bool>("validChildJob");
        if(!bValidChildJob) {
            LogManager_AddInformation("child job not valid.");
            return pprx::never<pprx::maybe>().as_dynamic();
        }
    } while(0);
    return waitForStartContactlessTransaction(param.get("childParam"), true);
}

/* virtual */
auto CardReaderMiura::beginICCardTransaction() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    m_lockTransactionState->get_subscriber().on_next(false);
    return pprx::just(pprx::unit()).as_dynamic();
}

/* virtual */
auto CardReaderMiura::endICCardTransaction() -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .flat_map([=](pprx::unit x) {
        if(!m_ios){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        // カード抜き検知設定
        return RWcommunication(
            "endICCardTransaction#1",
            [=] {
                send({0x01, 0x00}, {0xD0, 0x60, 0x03, 0x00}, {}); 
            }
        )
        .flat_map([=](auto) {
            return pprx::just(pprx::unit());
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::checkMasterData(const_json_t data) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    LogManager_AddInformationF("in checkMasterData. data = %s", data.str());
    
    m_updateMasterFileList.clear();
    boost::function<pprx::observable<pprx::unit>(std::string,const_json_t,const_json_t,boost::function<std::string(const_json_t)>)> checkAndCreateMasterFile = [=](
        std::string filename,
        const_json_t srcData,
        const_json_t md5SrcData,
        boost::function<std::string(const_json_t)> createDataFunc
    ) {
        if(isDebugMode()) {
            // デバッグモードの動作
            // ファイルがある場合 : 置いてあるファイルをそのまま使う
            // ファイルがない場合 : MD5の値にかかわらずファイルを新規に作成する
            if(boost::filesystem::exists(BaseSystem::instance().getDocumentDirectoryPath() / "MasterData" / filename)) {
                LogManager_AddInformationF("[debug mode] file exist. not create file [%s]", filename);
                return pprx::just(pprx::unit()).as_dynamic();
            } else {
                LogManager_AddInformationF("[debug mode] file not exist. create file [%s]", filename);
                return createMasterFile(filename, srcData, createDataFunc).as_dynamic();
            }
        }
        return getRwConfigurationVersion(filename)
        .flat_map([=](std::string rwMd5) {
            std::string srcMd5 = CardReaderMiuraConfig::jsonToMd5String(md5SrcData);
            LogManager_AddInformationF("%s srcMd5 = %s, rwMd5  = %s", filename % srcMd5 % rwMd5);
            LogManager_AddInformationF("\t[update config test] in checkMasterData. srcData = %s", srcData.str());
            LogManager_AddInformationF("\t[update config test] in checkMasterData. md5SrcData = %s", md5SrcData.str());
            if(srcMd5 == rwMd5) {
                return pprx::just(pprx::unit()).as_dynamic();
            } else {
                m_updateMasterFileList.push_back(filename);
                return createMasterFile(filename, srcData, createDataFunc).as_dynamic();
            }
        }).as_dynamic();
    };

    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        return checkAndCreateMasterFile(
            "contactless.cfg",
            data,
            data.get("normalObject.masterData"),
            CardReaderMiuraConfig::createContactlessConfigData
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        auto src =  PPConfig::instance().getChild("miura.messages.contactless.prompts");
        return checkAndCreateMasterFile(
            "ctls-prompts.txt",
            src,
            src,
            CardReaderMiuraConfig::createContactlessPromptsData
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        auto bNeedupdate = (m_updateMasterFileList.size() != 0 ? true : false);
        json_t checkInfo;
        auto rwSerial = m_cardReaderInfo.get_optional<std::string>("RWSerial");
        checkInfo.put("RWSerial"    , rwSerial.get());
        checkInfo.put("bNeedUpdate" , bNeedupdate);
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        
        json_t result;
        result.put("bNeedUpdate", bNeedupdate);
        LogManager_AddInformationF("update check result = %s", result.str());
        return pprx::just(result.as_readonly());
    }).as_dynamic()
    .on_error_resume_next([=](auto err) {
        LogManager_AddError(pprx::exceptionToString(err));
        json_t checkInfo;
        checkInfo.put("RWSerial"    , "");
        // bNeedUpdate項目 なし
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::createMasterFile(std::string filename, const_json_t srcData, boost::function<std::string(const_json_t)> createDataFunc) -> pprx::observable<pprx::unit>
{
    return pprx::just(createDataFunc(srcData))
    .flat_map([=](std::string configData) {
        CardReaderMiuraConfig::writeMasterData(filename, configData);
        return pprx::just(pprx::unit());
    }).as_dynamic();
};

/* virtual */
auto CardReaderMiura::updateMasterData(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    boost::function<pprx::observable<pprx::unit>(boost::filesystem::path,std::string,const_json_t,boost::function<std::string(const_json_t)>)> confirmAndcreateMasterFile = [=](
        boost::filesystem::path masterDir,
        std::string filename,
        const_json_t srcData,
        boost::function<std::string(const_json_t)> createDataFunc
    ) {
        LogManager_AddInformationF("\t[update config test] in updateMasterData. srcData = %s", srcData.str());
        auto ite = std::find(m_updateMasterFileList.begin(), m_updateMasterFileList.end(), filename);
        if(ite == m_updateMasterFileList.end()) {
            LogManager_AddInformationF("%s not need update", filename);
            return pprx::just(pprx::unit()).as_dynamic();
        }
        auto cfgFileExist = boost::filesystem::exists(masterDir / filename);
        LogManager_AddInformationF("cfgFileExist = %s", (cfgFileExist ? "true" : "false"));
        if(cfgFileExist) {
            return pprx::just(pprx::unit()).as_dynamic();
        }
        return createMasterFile(filename, srcData, createDataFunc).as_dynamic();
    };
    // checkMasterData()で更新なしとなった場合、この関数を呼び出してもマスターデータの更新は実施されない。
    // 動作検証用に強制的にマスターデータ更新を実施するモードを儲ける。
    if(isDebugMode()) {
        LogManager_AddInformation("notice!!! Debug Mode Force Update Eexecute Now.");
        m_updateMasterFileList = std::vector<std::string>({"contactless.cfg", "ctls-prompts.txt"});
    }
    if(m_updateMasterFileList.size() == 0) {
        LogManager_AddInformation("update master file none.");
        json_t checkInfo;
        auto rwSerial = m_cardReaderInfo.get_optional<std::string>("RWSerial");
        checkInfo.put("RWSerial"    , rwSerial.get());
        checkInfo.put("bNeedUpdate" , false);
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        return pprx::just(pprx::unit());
    }
    m_updateKind = UpdateKind::MasterData;
    auto masterDir = boost::make_shared<boost::filesystem::path>(BaseSystem::instance().getDocumentDirectoryPath() / "MasterData");
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        return confirmAndcreateMasterFile(
            *masterDir,
            "contactless.cfg",
            data,
            CardReaderMiuraConfig::createContactlessConfigData
        ).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return confirmAndcreateMasterFile(
            *masterDir,
            "ctls-prompts.txt",
            PPConfig::instance().getChild("miura.messages.contactless.prompts"),
            CardReaderMiuraConfig::createContactlessPromptsData
        ).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return getReaderInfo()
        .flat_map([=](const_json_t readerInfoJson) {
            // ファームウェアアップデートと同じ仕組みを使うため、info.jsonを生成する
            LogManager_AddInformationF("readerInfoJson = %s", readerInfoJson.str());
            std::string verStr = readerInfoJson.get<std::string>("RWFirmwareVersion");
            auto pos = verStr.find("/") + 1;
            std::string osVer = std::string(verStr.begin(), verStr.begin() + pos - 1);
            std::string mpiVer = std::string(verStr.begin() + pos, verStr.end());
            json_t flagmentJson;
            json_t::array_t files;
            for(std::string filename : m_updateMasterFileList) {
                files.push_back(json_t(filename));;
            }
            flagmentJson.put("files", files);
            flagmentJson.put("type", "MPI");
            flagmentJson.put("version", mpiVer);
            flagmentJson.put("rebootWaitSecond", 0);
            json_t infoJson;
            infoJson.put("fragments", json_t::array_t({flagmentJson}));
            boost::algorithm::replace_all(osVer, ".", "-");
            boost::algorithm::replace_all(mpiVer, ".", "-");
            infoJson.put("description", (boost::format("Miura M010, OS %s, MPI %s, Master Data Update.") % osVer % mpiVer).str());
            LogManager_AddInformationF("info.json = %s", infoJson.str());
            return updateWriteAll(masterDir->string(), infoJson);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        m_updateMasterFileList.clear();
        json_t checkInfo;
        auto rwSerial = m_cardReaderInfo.get_optional<std::string>("RWSerial");
        checkInfo.put("RWSerial"    , rwSerial.get());
        checkInfo.put("bNeedUpdate" , false);
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .on_error_resume_next([=](auto err) {
        LogManager_AddError(pprx::exceptionToString(err));
        m_updateMasterFileList.clear();
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::isDebugMode() -> bool
{
    return boost::filesystem::exists(BaseSystem::instance().getDocumentDirectoryPath() / "debugMode");
}

/* virtual */
auto CardReaderMiura::readContactless(const_json_t param) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return waitForStartContactlessTransaction(param)
    .flat_map([=](pprx::maybe m) {
        return m.observableForContinue<const_json_t>();
    });
}

auto CardReaderMiura::waitForStartContactlessTransaction(const_json_t param, bool isWait/*= false*/) -> pprx::observable<pprx::maybe>
{
    LogManager_AddDebugF("in waitForStartContactlessTransaction. param = %s", param.str());
    boost::shared_ptr<json_t> json = boost::make_shared<json_t>();
    boost::shared_ptr<ContactlessState> state = boost::make_shared<ContactlessState>(ContactlessState::Bad);
    boost::shared_ptr<bool> wait = boost::make_shared<bool>(isWait);
    boost::function<void(void)> sendStartContactlessTransaction = [=]() {
        LogManager_AddInformation("in waitForStartContactlessTransaction. in sendStartContactlessTransaction.");
        m_lockTransactionState->get_subscriber().on_next(true);
        auto dateTime = param.get("dateTime");
        auto amountOpt = param.get_optional<int>("amount");
        auto amount = amountOpt ? *amountOpt : 0;
        auto amountOther = 0;
        auto transactionDate = dateTime.get<std::string>("date");
        auto transactionTime = dateTime.get<std::string>("time");
        auto currencyCodeOpt = param.get_optional<int>("currencyCode");
        auto currencyCode = currencyCodeOpt ? *currencyCodeOpt : 0;
        std::vector<std::string> datas = {
            // 9F02 : Amount, Authorised (Numeric)
            (boost::format("9f0206%012d") % amount).str(),
            // 9F03 : Amount, Other (Numeric)
            (boost::format("9f0306%012d") % amountOther).str(),
            // 9A : Transaction date
            (boost::format("9a03%s") % transactionDate).str(),
            // 9F21 : Transaction time
            (boost::format("9f2103%s") % transactionTime).str(),
            // 9C : Transaction Type、固定値
            boost::format("9c0100").str(),
            // 9C : Transaction Type、固定値
            boost::format("9c0120").str(),
            // 5F2A : Transaction Currency Code
            (boost::format("5f2a02%04d") % currencyCode).str(),
            // 5F36 : Transaction Currency Exponent、固定値
            boost::format("5f360100").str()
        };
        bytes_t bodybin = CardReaderUtil::createTemplateData("e0", datas);
        send({0x01, 0x00}, {0xDE, 0xC1, 0x07, 0x01}, bodybin);
    };
    boost::function<bool(bytes_sp)> predicate = [=](bytes_sp data) {
        auto tag_c4 = CardReaderUtil::getTLVDataFromRAPDU(*data, {0xC4});
        if(tag_c4.size() != 0) {
            bool ret = false;
            // C4がきている場合はstateの変更のみでコマンドの応答待ちは継続する
            auto tag_c4Str = std::string(tag_c4.begin(), tag_c4.end());
            LogManager_AddInformationF("C4 Tag text [%s]", tag_c4Str);
            if(tag_c4Str == "Enter PIN") {
                *state = ContactlessState::Touch;
            } else if(tag_c4Str == "Success BEEP") {
                *state = ContactlessState::EnterPIN;
            } else if(tag_c4Str == "SEE PHONE") {
                *state = ContactlessState::SeePhone;
                ret = true;
            }
            LogManager_AddInformation("in waitForStartContactlessTransaction in predicate. return false.");
            return ret;
        }
        LogManager_AddInformation("in waitForStartContactlessTransaction in predicate. return true.");
        return true;
    };
    // ABORT発行から処理を返すまでの処理
    boost::function<pprx::observable<bytes_sp>(void)> abortProcess = [=]() {
        return RWcommunication(
            "waitForStartContactlessTransaction#1",
            [=] {
                LogManager_AddInformation("exec ABORT command.");
                send({0x01, 0x00}, {0xD0, 0xFF, 0x00, 0x00}, {});
            },
            {{}},
            ReadUnsolicited::Yes,
            [=](bytes_sp data) {
                LogManager_AddInformationF("abort recv data = %s", CardReaderUtil::binaryToHextext(*data));
                if(CardReaderUtil::retrieveSW1SW2(*data) == "9F41") {
                    LogManager_AddInformation("startContactlessTransaction abort pick up 9F41.");
                    m_lockTransactionState->get_subscriber().on_next(false);
                    return true;
                }
                return false;
            }
        );
    };
    // キャンセル時の中断ハンドラ
    boost::function<void(void)> cleanup = [=]() {
        abortProcess()
        .subscribe([](auto){}, [](auto){}, [](){});
    };
    boost::function<pprx::observable<bytes_sp>(void)> execReadContactless = [=]() {
        return RWcommunication(
            "waitForStartContactlessTransaction#2",
            sendStartContactlessTransaction,
            {{0xC4}},
            ReadUnsolicited::Yes,
            predicate,
            cleanup
        );
    };
    boost::function<pprx::observable<pprx::maybe>(bytes_sp)> analyseResponse = [=](bytes_sp response) {
        LogManager_AddDebugF("in analyseResponse. response = %s", CardReaderUtil::binaryToHextext(*response));
        return pprx::just(response)
        .flat_map([=](bytes_sp response) {
            if(*state == ContactlessState::SeePhone) {
                LogManager_AddInformation("startContactlessTransaction receive SeePhone. DEVICE STATUS CHANGE receive status -> SEE PHONE.");
                return abortProcess()
                .flat_map([=](auto) {
                    json_t errResp;
                    errResp.put("description","SeePhone");
                    return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
                }).as_dynamic();
            }
            auto sw = CardReaderUtil::retrieveSW1SW2(*response);
            if(sw == "9000") {
                return pprx::maybe::success(response);
            } else if(sw == "9F41") {
                if(*state == ContactlessState::Bad) {
                    // 9F41はいろんな状況で返される可能性があるが、カードタッチ前の場合(キャンセル押下で発生)のみリトライする
                    LogManager_AddInformation("startContactlessTransaction receive 9F41. before card touch -> retry.");
                    return pprx::maybe::retry();
                } else {
                    LogManager_AddInformation("startContactlessTransaction receive 9F41. after card touch -> never.");
                    return pprx::never<pprx::maybe>().as_dynamic();
                }
            } else if(sw == "9F42") {
                LogManager_AddInformation("startContactlessTransaction receive 9F42. Transaction Timed Out.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Transaction timed out. SW:" + sw);
                errResp.put("code", "PP_E4001");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9F43") {
                LogManager_AddInformation("startContactlessTransaction receive 9F43. card being inserted.");
                auto tradeType = param.get_optional<std::string>("tradeType");
                if(tradeType&&(tradeType.get() == "refund")){
                    json_t errResp;
                    errResp.put("description","NFCCardError");
                    errResp.put("type", "Inserted card. SW:" + sw);
                    errResp.put("code", "PP_E4012");
                    return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
                }
                return pprx::never<pprx::maybe>().as_dynamic();
            } else if(sw == "9F44") {
                LogManager_AddInformation("startContactlessTransaction receive 9F44. card being swiped.");
                return pprx::never<pprx::maybe>().as_dynamic();
            } else if(sw == "9FC2") {
                LogManager_AddInformation("startContactlessTransaction receive 9FC2. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Insert, swipe or try another card. SW:" + sw);
                errResp.put("code", "PP_E4004");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9FCF") {
                LogManager_AddInformation("startContactlessTransaction receive 9FCF. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Hardware error. SW:" + sw);
                errResp.put("code", "PP_E4005");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9FC3") {
                LogManager_AddInformation("startContactlessTransaction receive 9FC3. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "The card asked for Chip interface: Transaction should be performed using ICC. SW:" + sw);
                errResp.put("code", "PP_E4006");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9F14") {
                LogManager_AddInformation("startContactlessTransaction receive 9F14. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Invalid data received as input parameter. SW:" + sw);
                errResp.put("code", "PP_E4007");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9F15") {
                LogManager_AddInformation("startContactlessTransaction receive 9F15. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Invalid configuration file received inside tag 80. SW:" + sw);
                errResp.put("code", "PP_E4008");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9F26") {
                LogManager_AddInformation("startContactlessTransaction receive 9F26. Transaction not possible.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "Command not allowed at this state. SW:" + sw);
                errResp.put("code", "PP_E4009");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            } else if(sw == "9FC1") {
                LogManager_AddInformation("startContactlessTransaction receive 9FC1. No applications for the transaction.");
                json_t errResp;
                errResp.put("description","NFCCardError");
                errResp.put("type", "No applications for the transaction conditions: Transaction should be performed using ICC or MSR SW:" + sw);
                errResp.put("code", "PP_E4010");
                return pprx::maybe::error(make_error_cardrw(errResp)).as_dynamic();
            }

            LogManager_AddInformationF("startContactlessTransaction receive unknown response [%s]. emit error.", sw);
            return pprx::maybe::error(make_error_cardrw(makeErrorJson("general")));
        }).as_dynamic()
        .map([=](pprx::maybe m) {
            m_lockTransactionState->get_subscriber().on_next(false);
            return m;
        }).as_dynamic();
    };
    boost::function<void(bytes_t, std::string)> setBrandData = [=](bytes_t tag_9f06, std::string status) {
        std::string tagstr = CardReaderUtil::binaryToHextext(tag_9f06);
        std::string resultCodeExtended;
        // ブランドはAIDで判別する。
        // フルサイズのAIDではなくRID(前5バイト)の一致で判別する。
        if(boost::regex_match(tagstr, boost::regex("^A000000003.*"))) {
            json->put("cardBrand", "VISA");
            json->put("cardBrandIdentifier", "03");
            resultCodeExtended = "3";
        } else if(boost::regex_match(tagstr, boost::regex("^A000000004.*")) ||
                boost::regex_match(tagstr, boost::regex("^A000000005.*"))) {
            json->put("cardBrand", "MASTER");
            resultCodeExtended = "2";
        } else if(boost::regex_match(tagstr, boost::regex("^A000000025.*"))) {
            json->put("cardBrand", "AMEX");
            resultCodeExtended = "4";
        } else if(boost::regex_match(tagstr, boost::regex("^A000000065.*"))) {
            json->put("cardBrand", "JCB");
            resultCodeExtended = "5";
            json->put("cardBrandIdentifier", "03");
        }
        if(status == "approved") {
            resultCodeExtended += "1000000";
        } else if(status == "declined") {
            resultCodeExtended += "2110000";
        } else if(status == "onlineProcessing") {
            resultCodeExtended += "3000000";
        }
        json->put("resultCodeExtended", resultCodeExtended);
    };
    boost::function<pprx::observable<pprx::maybe>(bytes_sp)> createPostData = [=](bytes_sp response) {
        auto responseStr = CardReaderUtil::binaryToHextext(*response);
        LogManager_AddDebugF("in waitForStartContactlessTransaction. START CONTACTLESS TRANSACTION data = %s", responseStr);
        CardReaderUtil::debugTlvDump(*response);
        json->put("cardType", "nfc");
        if(boost::regex_match(responseStr, boost::regex("^E3.*"))) {
            LogManager_AddDebug("[status] approved");
            json->put("status", "approved");
            auto tag_dfae03 = CardReaderUtil::getTLVDataFromRAPDU(*response, {0xDF, 0xAE, 0x03});
            json->put("keySerialNoForResult", CardReaderUtil::binaryToHextext(tag_dfae03));
            json->put("rapdu", responseStr);
            setBrandData(CardReaderUtil::getTLVDataFromRAPDU(*response, {0x9F, 0x06}), "approved");
            json->put("transactionResultBit", generateTransactionResultBit(response));
            auto tag_dfae02 = CardReaderUtil::getTLVDataFromRAPDU(*response, {0xDF, 0xAE, 0x02});
            json->put("nfcTradeResult", CardReaderUtil::binaryToHextext(tag_dfae02));
            json->put("outcomeParameter", "30F0F010A0F1FF00");
            json->put("amount", param.get<int>("amount"));
            json->put("isRePrint", "false");
            json->put("payWayDiv", "0");
            json->put("productCode", "0990");
            json->put("taxOtherAmount", 0);
            return pprx::maybe::success(json->as_readonly());
        } else if(boost::regex_match(responseStr, boost::regex("^E5.*"))) {
            LogManager_AddDebug("[status] declined");
            json->put("status", "declined");
            json->put("rapdu", responseStr);
            setBrandData(CardReaderUtil::getTLVDataFromRAPDU(*response, {0x9F, 0x06}), "declined");
            return pprx::maybe::success(json->as_readonly());
        } else if(boost::regex_match(responseStr, boost::regex("^E4.*"))) {
            LogManager_AddDebug("[status] onlineProcessing");
            json->put("status", "onlineProcessing");
            auto tag_dfae03 = CardReaderUtil::getTLVDataFromRAPDU(*response, {0xDF, 0xAE, 0x03});
            json->put("keySerialNoForResult", CardReaderUtil::binaryToHextext(tag_dfae03));
            auto tag_dfae02 = CardReaderUtil::getTLVDataFromRAPDU(*response, {0xDF, 0xAE, 0x02});
            json->put("nfcTradeResult", CardReaderUtil::binaryToHextext(tag_dfae02));
            setBrandData(CardReaderUtil::getTLVDataFromRAPDU(*response, {0x9F, 0x06}), "onlineProcessing");
            // TODO : outcomeParameterに設定されるデータは実装方法未決定、とりあえず固定値を使用
            // json->put("outcomeParameter", "10F0F0F010F0FF00");
            json->put("outcomeParameter", "30F0F010A0F2FF00");
            json->put("amount", param.get<int>("amount"));
            json->put("transactionResultBit", generateTransactionResultBit(response));
            return pprx::maybe::success(json->as_readonly());
        } else {
            LogManager_AddInformation("illegal response. received.");
            return pprx::maybe::error(make_error_cardrw(makeErrorJson("general")));
        }
    };

    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        if(*wait) {
            *wait = false;
            return m_waitStartContactlessState->get_observable().as_dynamic();
        } else {
            return pprx::just(pprx::unit()).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](auto) {
        return execReadContactless()
        .flat_map([=](bytes_sp response) {
            return analyseResponse(response);
        }).as_dynamic()
        .retry()
        .flat_map([=](pprx::maybe result) {
            return result.observableForContinue<bytes_sp>();
        }).as_dynamic()
        .flat_map([=](bytes_sp response) {
            return createPostData(response);
        }).as_dynamic();
    }).as_dynamic()
    .take(1);
}

auto CardReaderMiura::generateTransactionResultBit(const bytes_sp data) -> std::string
{
    std::string ret;
    bytes_t transactionResultBit(5, 0);

    // Byte 1. MPI specification V1-53-API-V1 9.4 Contactless Kernel Type and Transaction Mode used
    bytes_t tag_df30 = CardReaderUtil::getTLVDataFromRAPDU(*data, {0xDF, 0x30});
    if((tag_df30[1] & 0x01) == 0) {
        transactionResultBit[0] |= (1 << 6);
    }
    // Byte 2. MPI specification V1-53-API-V1 9.3 Acquirer mandated CVM
    bytes_t tag_df28 = CardReaderUtil::getTLVDataFromRAPDU(*data, {0xDF, 0x28});
    if(tag_df28.size() == 0) {
        transactionResultBit[1] |= 1 << 2;
    } else if(tag_df28[0] == 0x00 ||
              tag_df28[0] == 0x1F) {
        transactionResultBit[1] |= 1 << 0;
    } else if(tag_df28[0] != 0x02 &&
              tag_df28[0] != 0x03 &&
              tag_df28[0] != 0xFF) {
        transactionResultBit[1] |= 1 << 2;
    }
    return CardReaderUtil::binaryToHextext(transactionResultBit);
}

auto CardReaderMiura::getRwConfigurationVersion(const std::string filename) -> pprx::observable<std::string>
{
    LogManager_Function();
    return RWcommunication(
        "getRwConfigurationVersion#1",
        [=] { send({0x01, 0x00}, {0xD0, 0x01, 0x00, 0x00}, {}); }
    ).as_dynamic()
    .flat_map([=](bytes_sp response) {
        return pprx::just(CardReaderMiuraConfig::retrieveConfigRevision(*response, filename));
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::adjustDateTime(const boost::posix_time::ptime& dtime)-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderMiura::sendApdu(const_json_t data) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return pprx::just(json_t().as_readonly());
}

/* virtual */
auto CardReaderMiura::verifyPlainOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return offlinePinCommonTransaction(OfflinePINType::Plaintext, paramjson).as_dynamic()
    .flat_map([=](const_json_t pinresult) {
        auto result = pinresult.get<std::string>("result");
        LogManager_AddInformationF("offline PIN Entry result. \"%s\"", result);
        if(result != "success") { 
            return pprx::just(pinresult).as_dynamic();
        }
        LogManager_AddInformation("exec VERIFY command.");
        return pprx::just(pprx::unit())
        .flat_map([=](auto x) {
            // VERIFY
            // 平文PINの場合、Offline PINコマンド後のVERIFYコマンドを実施した結果を返す。
            /*
            * EMVのBookに記載の仕様ではbody部にPINの長さやPIN自体などを設定する必要があるが、
            * JMupsの仕組みではPIN認証のOK/NGだけ取得できればよい作りになっている。
            * そのため、body部にはダミーの値を設定してVERIFYコマンドを発行している。
            */
            return RWcommunication(
                "verifyPlainOfflinePin#1",
                [=] {
                    send(
                        {0x11, 0x00},
                        {0x00, 0x20, 0x00, 0x80},
                        {0x08, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});
                }
            );
        })
        .flat_map([=](bytes_sp response) {
            json_t resultjson;
            resultjson.put("result", "success");
            resultjson.put("data", CardReaderUtil::binaryToHextext(*response));
            LogManager_AddInformationF("VEIRFY result. %s", resultjson.str());
            return pprx::just(resultjson.as_readonly()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::verifyEncryptedOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    // 暗号PINの場合、Offline PINコマンドの結果を返す。
    return offlinePinCommonTransaction(OfflinePINType::Encrypt, paramjson).as_dynamic();
}

// offline PIN、平文/暗号共通処理
auto CardReaderMiura::offlinePinCommonTransaction(const OfflinePINType type, const_json_t paramjson) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    LogManager_AddInformationF("paramjson = %s", paramjson.str());
    // RESET DEVICE
    return RWcommunication(
        "offlinePinCommonTransaction#1",
        [=] {
            send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {});
        },
        {{}},
        ReadUnsolicited::No
    )
    .flat_map([=](auto) {
        return RWcommunication(
            "offlinePinCommonTransaction#2",
            [=] {
                // 9F02 : Amount, Authorised
                std::string tag_9f02 = (boost::format("9f0206%012d") %
                    paramjson.get<int>("amount")).str();
                // 50 : Application Label
                std::string label = paramjson.get<std::string>("application.label");
                std::string tag_50   = (boost::format("50%02x%s") %
                    (label.size() / 2) %
                    label).str();
                // 5F2A : Transaction Currency Code、固定値
                std::string tag_5f2a = (boost::format("5f2a020392")).str();
                // 5F36 : Transaction Currency Exponent、固定値
                std::string tag_5f36 = (boost::format("5f360100")).str();
                // MPI Offline PINコマンドの発行
                const auto datalen = ((tag_9f02.size() + tag_50.size() + tag_5f2a.size() + tag_5f36.size()) / 2);
                std::string bodystr = (boost::format("%02xe0%02x%s") % 
                    (datalen + 2) % // データ長(1byte) + E0 tag(1byte) + E0 template内のデータ長
                    (datalen) %
                    (tag_9f02 + tag_50 + tag_5f2a + tag_5f36)).str();
                bytes_t bodybin;
                boost::algorithm::unhex(bodystr.begin(), bodystr.end(), std::back_inserter(bodybin));
                send({0x01, 0x00}, {0xDE, 0xD7, 0x00, 0x01}, bodybin);
            }
        );
    }).as_dynamic()
    .flat_map([=](bytes_sp pinResult) {
        // リクルート銀聯対応:
        // MiuraのファームウェアやMPIが古い場合にOnline PINコマンドを発行した際、
        // Referenced data (data objects) not found(SW=6A88)を返すことがある。
        // Online PINに合わせてoffline PINにも同様のケアを入れる。
        // Offline PINの場合はSWを見て以降の振る舞いを変更する処理がすでにあるので
        // 6A88に対象を絞ってエラーを発行して終了させる。
        std::string sw = CardReaderUtil::retrieveSW1SW2(*pinResult);
        if(sw == "6A88") {
            LogManager_AddInformationF("Offline PIN COMMAND error. response [%s]", sw);
            json_t errresp;
            errresp.put("description", "disableCardInsertedEMVKernel+sw:" + sw);
            errresp.put("code", "PP_E2003");
            return pprx::error<const_json_t>(make_error_cardrw(errresp)).as_dynamic();
        }
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            m_lockTransactionState->get_subscriber().on_next(true);
            // キャンセル時の中断ハンドラ
            boost::function<void(void)> cleanup = [=]() {
                RWcommunication(
                    "offlinePinCommonTransaction#4",
                    [=] {
                        LogManager_AddInformation("exec ABORT command.");
                        send({0x01, 0x00}, {0xD0, 0xFF, 0x00, 0x00}, {});
                    },
                    {{}}, ReadUnsolicited::No,
                    [=](bytes_sp data) {
                        // PINの入力完了によるRWの応答とアプリからキャンセルしたことによるABORTコマンドの発行、
                        // これがほぼ同時に実行された場合はどちらの応答が先にくるかわからない。
                        // そのため、ABORTの応答とPIN入力の応答の両方を待ってから処理が先に進むようにする。
                        // Offline PINコマンドはPIN入力を何もしない状態でもABORTコマンド発行によりキャンセル応答を返すので、
                        // 何もしていない場合のために特別な処理を作る必要はない。
                        LogManager_AddInformationF("abort recv data = %s", CardReaderUtil::binaryToHextext(*data));
                        static int successCount = 0;
                        static bool abortReturn = false;
                        static bool pinReturn = false;
                        std::string abortsw = CardReaderUtil::retrieveSW1SW2(*data);
                        if(abortsw == "9F41") {
                            // Offline PINコマンドの入力待ち中にABORTコマンドを受けた場合はSW=9F41を返す。
                            // ×ボタン押下についても同様。
                            LogManager_AddInformation("offlinePinCommonTransaction abort pick up 9F41.");
                            pinReturn = true;
                        } else if(abortsw == "9F0C") {
                            // Offline PINコマンドの入力待ち中にPINバイパスを実施した場合はSW=9F0Cを返す。
                            LogManager_AddInformation("offlinePinCommonTransaction abort and bypass exec.");
                            pinReturn = true;
                        } else if(abortsw == "9000") {
                            // PINの入力が完了した場合、PIN入力完了のSW=9000を返す。
                            // ABORTコマンドの応答もSW=9000を返す。
                            // 両者共にデータなしSWのみの応答なので見分けがつかない。
                            // そのため1度目をABORT応答、2度目をPIN入力完了の応答とみなす。
                            abortReturn = true;
                            successCount += 1;
                            if(successCount > 1) {
                                LogManager_AddInformation("offlinePinCommonTransaction abort and PIN entry exec.");
                                pinReturn = true;
                            }
                        }
                        if(pinReturn && abortReturn) {
                            // PIN入力とABORTコマンドの応答両方を回収してから先の処理に進める
                            LogManager_AddError("offlinePinCommonTransaction abort return.");
                            pinReturn = false;
                            abortReturn = false;
                            successCount = 0;
                            m_lockTransactionState->get_subscriber().on_next(false);
                            return true;
                        }
                        return false;
                    }
                )
                .subscribe([](auto){}, [](auto){}, [](){});
            };
            // PIN入力の完了待ち or キャンセル受付待ち
            return RWcommunication(
                "offlinePinCommonTransaction#3",
                [=] { return; },
                {{}},
                ReadUnsolicited::No,
                [=](bytes_sp sp) { return true; },
                cleanup
            );
        }).as_dynamic()
        .flat_map([=](bytes_sp response) {
            LogManager_AddInformation("PIN Entry success.");
            m_lockTransactionState->get_subscriber().on_next(false);
            return pprx::just(CardReaderUtil::retrieveSW1SW2(*response));
        }).as_dynamic()
        .flat_map([=](std::string sw) {
            LogManager_AddInformationF("Offline PIN Entry result. sw = \"%s\"", sw);
            json_t result;
            if(sw == "9000") {
                // この段階では何をいれても数字4桁入力 + ✔︎ボタン押下で9000が返る
                result.put("result", "success");
                result.put("data", CardReaderUtil::binaryToHextext(*pinResult));
            } else if(sw == "9F0C") {
                // 数字0桁入力 + ✔︎ボタン押下
                // 尚、数字1〜3桁入力時の✔︎ボタン押下はカードリーダーから何も返されない
                result.put("result", "bypass");
            } else if(sw == "9F41") {
                // 数字0桁入力 + ×ボタン押下
                // 尚、数字1〜4桁入力時の×ボタン押下は「全桁クリア」の挙動をする
                result.put("result", "retry");
            }
            // 該当するSWがない場合は呼び出し元でエラーを発行している
            LogManager_AddInformationF("result = %s", result.str());
            return pprx::just(result.as_readonly()).as_dynamic();            
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::abortTransaction() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return m_lockTransactionState->get_observable().take(1)
    .flat_map([=](bool state) {
        LogManager_AddInformationF("lockstate = \"%s\".", (state ? "true" : "false"));
        if(state == false) {
            LogManager_AddInformation("no wait.");
            return pprx::just(pprx::unit()).as_dynamic();
        }
        return m_lockTransactionState->get_observable()
        .flat_map([=](bool lockstate) {
            if(lockstate) {
                LogManager_AddInformation("emit on_error.");
                m_abortRWcommunication->get_subscriber().on_next(pprx::unit());
            } else {
                LogManager_AddInformation("abort finish detected.");
                return pprx::just(pprx::unit()).as_dynamic();
            }
            return pprx::never<pprx::unit>().as_dynamic();
        }).as_dynamic()
        .take(1)
        .flat_map([=](auto) {
            LogManager_AddInformation("abort end.");
            return pprx::just(pprx::unit());
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
void CardReaderMiura::setLockTransactionState(bool blockd )
{
    LogManager_Function();
    
    m_lockTransactionState->get_subscriber().on_next(blockd);
}

/* virtual */
auto CardReaderMiura::displayText(const std::string& str) -> pprx::observable<pprx::unit>
{
    return _displayText(ERows::Row3, EBackLight::On, str);
}

auto CardReaderMiura::displayTextNoLf(const std::string& str) -> pprx::observable<pprx::unit>
{
    return RWcommunication(
                           "_displayText#1",
                           [=] {
                               const uint8_t p1 = 0x01;
                               const uint8_t p2 = 0x01;
                               const bytes_t header{ 0xD2, 0x01, p1, p2};
                               bytes_t body;
                               body.push_back(str.length());
                               body.insert(body.end(), str.begin(), str.end());
                               send({0x01, 0x00}, header, body);
                           },
                           {{}},
                           ReadUnsolicited::No
                           )
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::displayIdleImage() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return RWcommunication(
        "displayIdleImage#1",
        [=] {
            // idle画面の表示
            send(
                {0x01, 0x00}, 
                {0xD2, 0xD2, 0x01, 0x00}, 
                {0x0F, 0x69, 0x64, 0x6C, 0x65, 0x2D, 0x73, 0x63, 0x72, 0x65, 0x65, 0x6E, 0x2E, 0x62, 0x6D, 0x70, 0x6F}); 
        }, {{}}, ReadUnsolicited::No
    )
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::clearCardreaderDisplay() -> pprx::observable<pprx::unit>
{
    // ABORT発行から処理を返すまでの処理
    boost::function<pprx::observable<bytes_sp>(void)> abortProcess = [=]() {
        return RWcommunication
        (
         "clearCardreaderDisplay#",
         [=] {
             LogManager_AddInformation("exec ABORT command.");
             send({0x01, 0x00}, {0xD0, 0xFF, 0x00, 0x00}, {});
         },
         {{}},
         ReadUnsolicited::No,
         [=](bytes_sp data) {
             LogManager_AddInformationF("abort recv data = %s", CardReaderUtil::binaryToHextext(*data));
             return true;
         });
    };
    return abortProcess().as_dynamic()
    .flat_map([=](auto) {
        return cleanUp().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        return displayIdleImage().as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::cleanUp() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return RWcommunication(
        "cleanUp#1",
        [=] {
            // ソフトリセット
            send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {});
        }, {{}}, ReadUnsolicited::No
    )
    .flat_map([=](auto) {
        return RWcommunication(
            "cleanUp#2",
            [=] {
                // CARD STATUS 設定クリア
                send({0x01, 0x00}, {0xD0, 0x60, 0x00, 0x00}, {}); 
            }, {{0x48}}
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        m_lockTransactionState->get_subscriber().on_completed();
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto CardReaderMiura::observableReaderPacket() -> pprx::observable<bytes_sp>
{
    LogManager_Function();

    return pprx::observable<>::create<bytes_sp>([=](pprx::subscriber<bytes_sp> s){
        auto packet = boost::make_shared<bytes_t>();
        auto eas = m_ios;
        if(!eas){
            LogManager_AddWarning("eastream has been deleted.");
            s.on_error(make_error_cardrw(makeErrorJson("disconnected")));
            return;
        }
        
        m_recv->get_observable()
        .subscribe
        (
         [=](bytes_sp data){
             LogManager_Scoped("subscribe get_observable : next");
             auto header = bytes_t(data->begin(), data->begin() + 3);
             /* Prologue LRC strip */
             packet->insert(packet->end(), data->begin() + 3, data->end() - 1);
             if((header[0] & 0xFF) == 0) {
                 return;
             }
             /* PCBのLSBが0の場合、後続データがなしなので値を発行する */
             if((header[1] & 0x01) == 0) {
                 auto _p = boost::make_shared<bytes_t>();
                 *_p = *packet;
                 packet->clear();
                 BaseSystem::instance().post([=](){
                     s.on_next(_p);
                 });
             }
         },
         [=](auto err) {
             LogManager_Scoped("subscribe get_observable : error");
             s.on_error(err);
         },
         [=]() {
             LogManager_Scoped("subscribe get_observable : completed");
             s.on_completed();
         }
        );
    });
}

auto CardReaderMiura::beginWatchingCardReaderState() -> void
{
    LogManager_Function();

    observableReaderPacket()
    .subscribe(
        [=](bytes_sp data) {
            LogManager_Scoped("subscribe cardstate : next");
            if(data == nullptr) {
                LogManager_AddInformation("in beginWatchingCardReaderState. emited data nullptr.");
                return;
            }
            bytes_t tag_48 = CardReaderUtil::get48Data(*data, {0x48});
            if(tag_48.size() >= 2) {
                if((tag_48[0] & 0x01) == 0) {
                    LogManager_AddInformation("in peek subscriber. card remove detect. emit message.");
                    setICCardSlotState(ICCardSlotState::Removed);
                }
            }
        },
        [=](auto err) {
            LogManager_Scoped("subscribe cardstate : error");
            setDeviceConnectState(DeviceConnectState::Disconnected);
        },
       [=]() {
           LogManager_Scoped("subscribe cardstate : completed");
           setDeviceConnectState(DeviceConnectState::Disconnected);
       }
    );
}

/* virtual */
auto CardReaderMiura::verifyEncryptedOnlinePin(const_json_t paramjson) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return RWcommunication(
        // Online PINコマンド
        "verifyEncryptedOnlinePin#1",
        [=] {
            // 9F02 : Amount, Authorised
            std::string tag_9f02 = (boost::format("9f0206%012d") %
                paramjson.get<int>("amount")).str();
            // 50 : Application Label
            std::string label = paramjson.get<std::string>("application.label");
            std::string tag_50   = (boost::format("50%02x%s") %
                (label.size() / 2) %
                label).str();
            // 5F2A : Transaction Currency Code、固定値
            std::string tag_5f2a = (boost::format("5f2a020392")).str();
            // 5F36 : Transaction Currency Exponent、固定値
            std::string tag_5f36 = (boost::format("5f360100")).str();
            // Masked Track 2
            std::string track2;
            if(m_cardType == CardType::Magstripe) {
                // Magの場合のみ、Masked Track2を付加する
                // DFAE22 : Masked Track 2 (if Track 2 requested and available)
                std::string tag_dfae22 = CardReaderUtil::binaryToHextext(m_tag_dfae22);
                track2 = (boost::format("DFAE22%02x%s") %
                    (tag_dfae22.size() / 2) %
                    tag_dfae22).str();
            }
            // Online PIN
            auto datalen = ((tag_9f02.size() + tag_50.size() + tag_5f2a.size() + tag_5f36.size() + track2.size()) / 2);
            std::string bodystr = (boost::format("%02xe0%02x%s") % 
                (datalen + 2) % // データ長(1byte) + E0 tag(1byte) + E0 template内のデータ長
                (datalen) %
                (tag_9f02 + tag_50 + tag_5f2a + tag_5f36 + track2)).str();
            bytes_t bodybin;
            boost::algorithm::unhex(bodystr.begin(), bodystr.end(), std::back_inserter(bodybin));
            send({0x01, 0x00}, {0xDE, 0xD6, 0x11, 0x01}, bodybin);
        }
    )
    .flat_map([=](bytes_sp response) {
        // リクルート銀聯対応:
        // MiuraのファームウェアやMPIが古い場合にOnline PINコマンドを発行した際、
        // Referenced data (data objects) not found(SW=6A88)を返すことがある。
        // 6A88を返した場合はPIN入力の待機が必要ないのでここでエラーを発行して終了させる。
        std::string sw = CardReaderUtil::retrieveSW1SW2(*response);
        if(sw != "9000") {
            LogManager_AddInformationF("Online PIN COMMAND error. response [%s]", sw);
            json_t errresp;
            errresp.put("description", "disableCardInsertedEMVKernel+sw:" + sw);
            errresp.put("code", "PP_E2003");
            return pprx::error<bytes_sp>(make_error_cardrw(errresp)).as_dynamic();
        }
        m_lockTransactionState->get_subscriber().on_next(true);
        // キャンセル時の中断ハンドラ
        boost::function<void(void)> cleanup = [=]() {
            LogManager_AddInformation("PIN Entry error.");
            RWcommunication(
                "verifyEncryptedOnlinePin#3",
                [=] {
                    LogManager_AddInformation("exec ABORT command.");
                    send({0x01, 0x00}, {0xD0, 0xFF, 0x00, 0x00}, {});
                },
                {{}}, ReadUnsolicited::No,
                [=](bytes_sp data) {
                    // PINの入力完了によるRWの応答とアプリからキャンセルしたことによるABORTコマンドの発行、
                    // これがほぼ同時に実行された場合はどちらの応答が先にくるかわからない。
                    // そのため、ABORTの応答とPIN入力の応答の両方を待ってから処理が先に進むようにする。
                    // Online PINコマンドはPIN入力を何もしない状態でもABORTコマンド発行によりキャンセル応答を返すので、
                    // 何もしていない場合のために特別な処理を作る必要はない。
                    LogManager_AddInformationF("abort recv data = %s", CardReaderUtil::binaryToHextext(*data));
                    static bool abortReturn = false;
                    static bool pinReturn = false;
                    std::string abortsw = CardReaderUtil::retrieveSW1SW2(*data);
                    if(data->size() == 3 && (int)data->operator[](0) == 0x08) {
                        // Online PINコマンドの入力待ち中にABORTコマンドを受けた場合は応答データに0x08のみ返す。
                        // ×ボタン押下についても同様。
                        LogManager_AddError("verifyEncryptedOnlinePin abort pick up 0x08(cancel).");
                        pinReturn = true;
                    } else if(data->size() == 3 && (int)data->operator[](0) == 0x0C) {
                        // Online PINコマンドの入力待ち中にPINバイパスを実施した場合は応答データに0x0Cのみ返す。
                        LogManager_AddError("verifyEncryptedOnlinePin abort pick up 0x0C(PIN Bypass).");
                        pinReturn = true;
                    } else if(data->size() > 3) {
                        // 3バイトより長いデータの場合はPIN入力が正常に完了しているとみなす
                        LogManager_AddError("verifyEncryptedOnlinePin abort pick up sw=9000 and normal data.");
                        pinReturn = true;
                    } else if(data->size() == 2 && abortsw == "9000") {
                        // SWのみの9000はABORTの応答とみなす
                        LogManager_AddError("verifyEncryptedOnlinePin abort pick up sw=9000 and no data. It's ABORT response.");
                        abortReturn = true;
                    }
                    if(pinReturn && abortReturn) {
                        // PIN入力とABORTコマンドの応答両方を回収してから先の処理に進める
                        LogManager_AddError("verifyEncryptedOnlinePin abort return.");
                        pinReturn = false;
                        abortReturn = false;
                        m_lockTransactionState->get_subscriber().on_next(false);
                        return true;
                    }
                    return false;
                }
            )
            .subscribe([](auto){}, [](auto){}, [](){});
        };
        // PIN入力の完了待ち or キャンセル受付待ち
        return RWcommunication(
            "verifyEncryptedOnlinePin#2",
            [=] { return; },
            {{}},
            ReadUnsolicited::No,
            [=](bytes_sp sp) { return true; },
            cleanup
        ).as_dynamic();
    }).as_dynamic()
    .flat_map([=](bytes_sp response) {
        LogManager_AddInformationF("PIN Entry success. result \"%s\"", CardReaderUtil::binaryToHextext(*response));
        m_lockTransactionState->get_subscriber().on_next(false);
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            json_t resultjson;
            std::string pinresult = CardReaderUtil::retrieveSW1SW2(*response);
            if(pinresult == "9000") {
                if(response->size() == 3) {
                    // 1バイト + SW1SW2の場合はエラー
                    switch(response->front()) {
                    case 0x08: // PIN入力なしで×ボタン押下
                        // 尚、数字1〜4桁入力時の×ボタン押下は「全桁クリア」の挙動をする
                        LogManager_AddInformation("online PIN Entry command retry.");
                        resultjson.put("result", "retry");
                        break;
                    case 0x0C: // PIN入力なしで✔ボタン押下
                        // 尚、数字1〜3桁入力時の✔︎ボタン押下はカードリーダーから何も返されない
                        // Online PINだが、6桁ではなく4桁以上入力していた場合は正常終了が返される
                        LogManager_AddInformation("online PIN Entry command bypass.");
                        resultjson.put("result", "bypass");
                        break;
                    default:  // internal error
                        // MPI仕様書によると0x09〜0x0Bが内部エラーとして返される可能性があるが、それ以外の値もエラーにまとめる
                        LogManager_AddError("online PIN Entry command internal error.");
                        return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general"))).as_dynamic();
                    }
                } else {
                    // PINを4〜12桁入力した状態で✔ボタン押下で正常終了
                    LogManager_AddInformation("online PIN Entry command success.");
                    resultjson.put("result", "success");
                    resultjson.put("data", CardReaderUtil::binaryToHextext(*response));
                }
            } else {
                LogManager_AddErrorF("online PIN Entry command response. receive undefined SW = \"%s\".", pinresult);
                return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general"))).as_dynamic();
            }
            LogManager_AddInformation(resultjson.str());
            return pprx::just(resultjson.as_readonly()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::txPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    LogManager_AddInformation("[RW]EMV Plaintext COMMAND");
    return _sendAndReceiveCommonApdu(apdu);
}

/* virtual */
auto CardReaderMiura::txEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    LogManager_AddInformation("[RW]EMV Encriptedd COMMAND");
    return _sendAndReceiveCommonApdu(apdu);
}

auto CardReaderMiura::_sendAndReceiveCommonApdu(const std::string& apdu) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    LogManager_AddInformationF("parameter apdu = %s", apdu);
    return RWcommunication(
        "_sendAndReceiveCommonApdu#1",
        [=] {
            bytes_t byteline, header, trailer;
            boost::algorithm::unhex(apdu.begin(), apdu.end(), std::back_inserter(byteline));
            header.assign(&byteline[0], &byteline[4]);
            trailer.assign(&byteline[4], &byteline[byteline.size()]);
            send({0x11, 0x00}, header, trailer);
        },
        {{}},
        ReadUnsolicited::No
    )
    .flat_map([=](bytes_sp response) {
        json_t result;
        result.put("result", CardReaderUtil::binaryToHextext(*response));
        return pprx::just(result.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::getIfdSerialNo() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        json_t result;
        auto str = std::string(m_tag_9f1e.begin(), m_tag_9f1e.end());
        LogManager_AddInformationF("str = %s", str);
        result.put("result", str);
        LogManager_AddInformationF("getIfdSerialNo serial No. = %s", result.str());
        return pprx::just(result.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::onlineProccessingResult(bool bApproved) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddInformationF("parameter bApproved = %s", (bApproved ? "true": "false"));
    return RWcommunication(
        "onlineProccessingResult#1",
        [=] {
            LogManager_AddInformation("exec CONTINUE TRANSACTION command.");
            uint8_t arc = (bApproved ? 0x30 : 0x31);
            send({0x01, 0x00}, {0xDE, 0xD2, 0x00, 0x01}, {0x06, 0xE0, 0x04, 0x8A, 0x02, 0x30, arc});
        },
        {{}},
        ReadUnsolicited::No
    )
    .flat_map([=](bytes_sp response) {
        LogManager_AddDebugF("response CONTINUE TRANSACTION. %s", CardReaderUtil::binaryToHextext(*response));
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .on_error_resume_next([=](auto){
        LogManager_AddInformation("response CONTINUE TRANSACTION. error");
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto) {
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::getReaderInfo() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        if(m_RWInfo.size() != 0) {
            LogManager_AddInformation("already m_RWInfo exist.");
            return pprx::just(pprx::unit()).as_dynamic();
        }
        // RESET DEVICEの応答を取得していなかった場合、ここで取得する
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            if(m_ios) {
                LogManager_AddInformation("already conneced.");
                return pprx::just(pprx::unit()).as_dynamic();
            }
            // カードリーダーに接続していなかった場合、ここで接続する
            return connect()
            .flat_map([=](auto) {
                LogManager_AddInformation("connect success.");
                return pprx::maybe::success(pprx::unit());
            }).as_dynamic()
            .on_error_resume_next([=](auto err) {
                LogManager_AddError("connect error.");
                LogManager_AddError(pprx::exceptionToString(err));
                try{std::rethrow_exception(err);}
                catch(pprx::ppex_cardrw& rwerr){
                    auto&& desc = rwerr.json().get_optional<std::string>("result.detail.description");
                    LogManager_AddInformationF("rwerr.json().str(). %s", rwerr.json().str());
                    if(*desc == "connetTimeout"){
                        return pprx::maybe::retry();
                    } else {
                        return pprx::maybe::error(make_error_cardrw(makeErrorJson("general")));
                    }
                }
                catch(...){}
                return pprx::maybe::error(err);
            }).as_dynamic()
            .retry()
            .flat_map([=](pprx::maybe result) {
                LogManager_AddInformation("connect observableForContinue.");
                return result.observableForContinue<pprx::unit>();
            }).as_dynamic();
        }).as_dynamic()
        .flat_map([=](auto) {
            return RWcommunication(
                "getReaderInfo#1",
                [=] {
                    LogManager_AddInformation("exec RESET DEVICE soft reset.");
                    send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {}); 
                },
                {{}},
                ReadUnsolicited::No
            )
            .flat_map([=](bytes_sp resetrapdu) {
                m_RWInfo = bytes_t(resetrapdu->begin(), resetrapdu->end());
                return pprx::just(pprx::unit());
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        // TMSに渡すカードリーダー情報を返す
        m_cardReaderInfo.put("RWName", "MIURA/M010");
        std::string serial;
        std::string id;
        std::string osver;
        std::string mpiver;
        LogManager_AddInformationF("m_RWInfo \"%s\"", CardReaderUtil::binaryToHextext(m_RWInfo));
        for(std::uint32_t i = 0; i < m_RWInfo.size(); ) {
            if((m_RWInfo[i] == 0xe1) || (m_RWInfo[i] == 0xef)) {
                // E1もしくはEFテンプレート飛ばし
                i += 2;
            }
            if((m_RWInfo[i] == 0x9f) && (m_RWInfo[i + 1] == 0x1e)) {
                // Interface Device Serial Number
                const auto seriallen = m_RWInfo[i + 2];
                i += 3;
                serial = std::string(m_RWInfo.begin() + i, m_RWInfo.begin() + i + seriallen);
                i += seriallen;
            }
            if((m_RWInfo[i] == 0xdf) && (m_RWInfo[i + 1] == 0x0d)) {
                // Identifier
                const auto idlen = m_RWInfo[i + 2];
                i += 3;
                id = std::string(m_RWInfo.begin() + i, m_RWInfo.begin() + i + idlen);
                i += idlen;
            }
            if((m_RWInfo[i] == 0xdf) && (m_RWInfo[i + 1] == 0x7f)) {
                // Version
                const auto verlen = m_RWInfo[i + 2];
                i += 3;
                if(boost::regex_match(id, boost::regex(".*OS$", boost::regex::icase))) {
                    osver = std::string(m_RWInfo.begin() + i, m_RWInfo.begin() + i + verlen);
                } else {
                    mpiver = std::string(m_RWInfo.begin() + i, m_RWInfo.begin() + i + verlen);
                }
                i += verlen;
            }
            if((m_RWInfo[i] == 0x90) && (m_RWInfo[i + 1] == 0x00)) {
                break;
            }
        }
        boost::algorithm::replace_all(osver, "-", ".");
        boost::algorithm::replace_all(mpiver, "-", ".");
        m_cardReaderInfo.put("RWSerial", serial);
        m_cardReaderInfo.put("RWFirmwareVersion", osver + "/" + mpiver);
        LogManager_AddInformationF("json %s", m_cardReaderInfo.str());
        return pprx::just(m_cardReaderInfo.as_readonly());
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::isCardInserted() -> pprx::observable<bool>
{
    LogManager_Function();
    return observableDeviceConnectState()
    .flat_map([=](DeviceConnectState state){
        if(state != DeviceConnectState::Connected){
            LogManager_AddInformation("CardReader not connect.");
            return pprx::error<pprx::unit>(make_error_cardrw(makeErrorJson("disconnected"))).as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    })
    .flat_map([=](pprx::unit unit) {
        return RWcommunication(
           "isCardInserted#1",
            [=] {
                send({0x01, 0x00}, {0xD0, 0x60, 0x03, 0x00}, {});
            },
            {{0x48}}, 
            ReadUnsolicited::Yes
        );
    })
    .flat_map([=](bytes_sp resultsp) {
        // 先にCARD STATUS 設定クリア
        return RWcommunication(
            "isCardInserted#2",
            [=] {
                send({0x01, 0x00}, {0xD0, 0x60, 0x00, 0x00}, {}); 
            },
            {{0x48}},
            ReadUnsolicited::Yes
        )
        .flat_map([=](bytes_sp p) {
            const bytes_t tag_48 = CardReaderUtil::getTLVDataFromRAPDU(*resultsp, {0x48});
            LogManager_AddInformationF("tag_48 \"%s\" ", CardReaderUtil::binaryToHextext(tag_48));
            LogManager_AddInformationF("resultsp \"%s\" ", CardReaderUtil::binaryToHextext(*resultsp));
            if(tag_48.size() >= 2 && tag_48[0] & 0x01) {
                // Byte 1 Bit 0 Card Present : ICC
                LogManager_AddInformation("IC card slot inserted.");
                return pprx::error<bool>(make_error_cardrw(makeErrorJson("cardExist"))).as_dynamic();
            }
            return pprx::just(false).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::updateFirmware(const boost::filesystem::path& basedir) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddInformationF("filepath \"%s\"", basedir.string());
    m_updateKind = UpdateKind::FirmWare;

    return updateInitialize()
    .flat_map([=](pprx::unit){
        LogManager_AddInformation("update start.");
        std::ifstream is((basedir / "info.json").string());
        json_t j;
        j.deserialize(is);
        return updateWriteAll(basedir.string(), j);
    }).as_dynamic()
    .flat_map([=](auto){
        return updateFinalize();
    }).as_dynamic();
}

auto CardReaderMiura::getDisplayMsgForReader(const std::string& msgId, const bool bContactless/*= false*/) -> std::string
{
    std::string msg = "";
    std::string tablePath = bContactless ? "miura.messages.contactless.table" : "miura.messages.credit.table";
    auto table = PPConfig::instance().get<json_t::array_t>(tablePath);
    for(auto record : table) {
        auto value = record.get_optional<std::string>(msgId);
        if(value) {
            msg = value.value();
            break;
        }
    }
    return msg;
}

auto CardReaderMiura::updateInitialize() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

auto CardReaderMiura::update1KindInitialize() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddInformation("update1KindInitialize start.");
    return RWcommunication(
        "updateInitialize#1",
        [=] {
            LogManager_AddInformation("RESET DEVICE - Soft Reset");
            send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {});
        }, {{}}, ReadUnsolicited::No
    )
    .flat_map([=](bytes_sp data) {
        m_RWInfo = bytes_t(data->begin(), data->end());
        return RWcommunication(
            "updateInitialize#2",
            [=] {
                LogManager_AddInformation("BATTERY STATUS");
                send({0x01, 0x00}, {0xD0, 0x62, 0x00, 0x00}, {});
            }, {{}}, ReadUnsolicited::No
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        return RWcommunication(
            "updateInitialize#3",
            [=] {
                LogManager_AddInformation("SYSTEM LOG - remove mpi.log");
                send({0x01, 0x00}, {0xD0, 0xE1, 0x00, 0x00}, {});
            }, {{}}, ReadUnsolicited::No
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        return RWcommunication(
            "updateInitialize#4",
            [=] {
                LogManager_AddInformation("RESET DEVICE - Soft Reset");
                send({0x01, 0x00}, {0xD0, 0x00, 0x00, 0x00}, {});
            }, {{}}, ReadUnsolicited::No
        );
    }).as_dynamic()
    .flat_map([=](auto) {
        LogManager_AddInformation("update1KindInitialize finish.");
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto CardReaderMiura::updateWriteAll(const std::string& dirname, const json_t& infojson) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddInformationF("infojson.str() = %s", infojson.str());
    auto fragments = boost::make_shared<std::vector<json_t>>(infojson.get<json_t::array_t>("fragments"));
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        if(fragments->size() == 0) {
            LogManager_AddInformation("kindlist empty. All finish.");
            return pprx::maybe::success(true).as_dynamic();
        }
        
        auto&& type               = fragments->front().get<std::string>("type");
        auto&& version            = fragments->front().get<std::string>("version");
        auto&& rebootWaitSecond   = fragments->front().get<int>("rebootWaitSecond");
        auto&& fl                 = fragments->front().get("files").get_array<std::string>();
        auto filelist             = boost::make_shared<std::vector<std::string>>(fl);
        LogManager_AddInformationF("type = %s", type);
        LogManager_AddInformationF("version = %s", version);
        LogManager_AddInformationF("rebootWaitSecond = %d", rebootWaitSecond);
        LogManager_AddInformationF("filelist = %d", filelist->size());
        return update1KindInitialize()
        .flat_map([=](auto) {
            return updateWrite1Kind(dirname, filelist);
        }).as_dynamic()
        .flat_map([=](bool result) {
            return update1KindFinalize(rebootWaitSecond, result);
        }).as_dynamic()
        .flat_map([=](bool result) {
            if(result) {
                LogManager_AddInformation("erase kindlist record.");
                fragments->erase(fragments->begin());
                return pprx::maybe::retry().as_dynamic();
            } else {
                LogManager_AddError("update1KindFinalize result error.");
                // 下位の応答のエラーをmaybe::errorで包み直す
                return pprx::maybe::error(make_error_cardrw(makeErrorJson("firmwareUpdateError"))).as_dynamic();
            }
        }).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe result) {
        return result.observableForContinue<bool>();
    }).as_dynamic()
    .flat_map([=](bool result) {
        LogManager_AddInformation("write all file complete.");
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto CardReaderMiura::updateWrite1Kind(const std::string& dirname, const boost::shared_ptr<std::vector<std::string>>& filelist) -> pprx::observable<bool>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        if(filelist->size() == 0) {
            LogManager_AddInformation("filelist empty. Kind finish.");
            return pprx::maybe::success(true).as_dynamic();
        }
        /* 1ファイル転送 */
        auto filename = filelist->operator[](0);
        LogManager_AddInformationF("update file write start. filename \"%s\"", filename);
        //return pprx::just(true)
        return updateWrite1File(dirname, filename)
        .flat_map([=](bool result) {
            if(result) {
                LogManager_AddInformation("erase filelist record.");
                filelist->erase(filelist->begin());
                return pprx::maybe::retry();
            } else {
                LogManager_AddError("updateWrite1File error.");
                return pprx::maybe::error(make_error_cardrw(makeErrorJson("firmwareUpdateError")));
            }
        }).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe result) {
        LogManager_AddInformation("observableForContinue");
        return result.observableForContinue<bool>();
    }).as_dynamic()
    .flat_map([=](auto) {
        LogManager_AddInformation("write 1 kind success.");
        return pprx::just(true);
    }).as_dynamic()
    .on_error_resume_next([=](auto) {
        LogManager_AddError("write 1 kind error.");
        return pprx::just(false);
    }).as_dynamic();
}

auto CardReaderMiura::updateWrite1File(const std::string& dname, const std::string& fname) -> pprx::observable<bool>
{
    LogManager_Function();
    std::string dirname = dname;
    std::string filename = fname;
    return RWcommunication(
        "updateWrite1File#1",
        [=] {
            LogManager_AddInformation("SELECT FILE - Truncate mode");
            auto fn = boost::filesystem::path(filename).leaf().string();
            std::vector<uint8_t> d;
            d.push_back(fn.size());
            d.insert(d.end(), fn.begin(), fn.end());
            send({0x01, 0x00}, {0x00, 0xa4, 0x01, 0x00}, d);
        }, {{}},  ReadUnsolicited::No
    )
    .flat_map([=](auto) {
        /* 1ファイル転送 */
        auto packet = boost::make_shared<std::vector<char>>(4 * 1024 * 1024);
        auto s = boost::make_shared<std::ifstream>(std::ifstream(dirname + "/" + filename));
        auto offset = boost::make_shared<std::uint32_t>(0);
        auto streamSize = boost::make_shared<std::streamsize>(0);
        LogManager_AddInformation("write start");
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            s->read(packet->data(), packet->size());
            *streamSize = s->gcount();
            LogManager_AddInformationF("streamSize = %d.", *streamSize);
            if(*streamSize == 0) {
                LogManager_AddInformation("return write loop.");
                return pprx::maybe::success(true).as_dynamic();
            }
            return pprx::just(pprx::unit())
            .flat_map([=](auto) {
                LogManager_AddInformationF("read packet length = [%d]", *streamSize);
                LogManager_AddInformationF("read data front 50 byte %s", CardReaderUtil::binaryToHextext(bytes_t(packet->begin(), packet->begin() + 50)));
                /* STREAM BINARY */
                LogManager_AddInformationF("STREAM BINARY %d - %d", *offset % *streamSize);
                #define _d3_(d) static_cast<uint8_t>((d >> 16) & 0xFF), \
                                static_cast<uint8_t>((d >>  8) & 0xFF), \
                                static_cast<uint8_t>( d        & 0xFF)
                send({0x01, 0x00},
                    {0x00, 0xd7, 0x00, 0x00}, {
                    0x15,
                    0xe0, 0x13,                         /* E0 template */
                    0xdf, 0xa3, 0x01, 0x03, _d3_(*offset),     /* Offset */
                    0xdf, 0xa3, 0x02, 0x03, _d3_(*streamSize), /* Stream size */
                    0xdf, 0xa3, 0x03, 0x01, 0x64              /* Timeout value */
                });
                #undef _d3_
                return RWcommunication(
                    "updateWrite1File#2",
                    [=] {
                        LogManager_AddInformation("sendRawData");
                        /* 実データ送信 */
                        auto p = boost::make_shared<bytes_t>(*streamSize);
                        ::memcpy(p->data(), packet->data(), *streamSize);
                        _sendRawData(p);
                    }, {{}},  ReadUnsolicited::No
                );
            }).as_dynamic()
            .flat_map([=](bytes_sp data) {
                /* 実データ送信 */
                LogManager_AddInformationF("R-APDU \"%s\"", CardReaderUtil::binaryToHextext(*data));
                if(CardReaderUtil::retrieveSW1SW2(*data) == "9000") {
                    LogManager_AddInformation("write data success.");
                    *offset += *streamSize;
                    return pprx::maybe::retry();
                } else {
                    return pprx::maybe::error(make_error_cardrw(makeErrorJson("firmwareUpdateError")));
                }
            }).as_dynamic();
        }).as_dynamic()
        .retry()
        .flat_map([=](pprx::maybe result) {
            LogManager_AddInformation("observableForContinue");
            return result.observableForContinue<bool>();
        }).as_dynamic()
        .flat_map([=](auto) {
            LogManager_AddInformation("write 1 file success.");
            return pprx::just(true);
        }).as_dynamic()
        .on_error_resume_next([=](auto) {
            LogManager_AddError("write 1 file error.");
            return pprx::just(false);
        }).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::updateFinalize() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    // 後始末
    m_RWInfo.clear();
    return getReaderInfo()
    .flat_map([=](const_json_t info){
        // cardReader.jsonとPPConfigの更新
        LogManager_AddInformationF("updateFinalize %s.", info.str());
        TMSAccess::setConnectedCardReaderInfo(info);
        PPConfig::instance().updateConnectedCardReaderInformation();
        json_t checkInfo;
        auto rwSerial = info.get_optional<std::string>("RWSerial");
        checkInfo.put("bNeedUpdate", false);
        checkInfo.put("RWSerial", rwSerial.get());
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto){
        return observableDeviceConnectState();
    }).as_dynamic()
    .flat_map([=](DeviceConnectState state){
        if(state == DeviceConnectState::Connected) {
            return disconnect().as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic();
}

auto CardReaderMiura::update1KindFinalize(int rebootWaitSecond, bool bSendSuccess) -> pprx::observable<bool>
{
    LogManager_Function();
    auto ios = m_ios;
    return pprx::just(bSendSuccess)
    .flat_map([=](bool bSendSuccess){
        LogManager_AddInformation("update1KindFinalize start.");
        if(!bSendSuccess){
            LogManager_AddError("error. write files failed.");
            return pprx::just(false).as_dynamic();
        }
        if(m_updateKind == UpdateKind::MasterData) {
            // MasterData更新の場合、HARD RESETしても電源断しないので待機せずにそのまま応答を返して終わる
            return RWcommunication(
                "update1KindFinalize#1",
                [=] {
                    LogManager_AddInformation("RESET DEVICE - Hard Reset from Master Data Update.");
                    send({0x01, 0x00}, {0xd0, 0x00, 0x01, 0x00}, {});
                }, {{}},  ReadUnsolicited::No
            )
            .flat_map([=](auto) {
                return pprx::just(true);
            }).as_dynamic();
        }
        pprx::subject<pprx::unit> reconnect;
        m_recv->get_observable()
        .subscribe(
            [](auto) {},
            [=](auto) {
                LogManager_AddInformation("on error. eastream completed detect.");
                // 同じスレッドでemitしてしまうとconnectループにハマり、発行元の終了処理が終わらなくなってしまう。
                // そのため、ここで再接続処理の起動トリガーは別スレッドで実施する。
                BaseSystem::instance().post([=]() {
                    LogManager_AddInformation("on error. old session close.");
                    disconnect()
                    .flat_map([=](auto) {
                        LogManager_AddInformation("on error. emit reconnect trigger.");
                        reconnect.get_subscriber().on_next(pprx::unit());
                        reconnect.get_subscriber().on_completed();
                        return pprx::just(pprx::unit());
                    })
                    .subscribe(
                        [](auto) {},
                        [](auto) {},
                        []() {}
                    );
                });
            },
            [=]() {
                LogManager_AddInformation("on completed. eastream completed detect.");
                BaseSystem::instance().post([=]() {
                    LogManager_AddInformation("on completed. old session close.");
                    disconnect()
                    .flat_map([=](auto) {
                        LogManager_AddInformation("on completed. emit reconnect trigger.");
                        reconnect.get_subscriber().on_next(pprx::unit());
                        reconnect.get_subscriber().on_completed();
                        return pprx::just(pprx::unit());
                    })
                    .subscribe(
                        [](auto) {},
                        [](auto) {},
                        []() {}
                    );
                });
            }
        );
        /*
         * カードリーダーシャットダウン時にcompletedが発行されるので、
         * 一旦流れを切ってから以降につなぎ直す。
         */
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            LogManager_AddInformation("RESET DEVICE - Hard Reset from Firmware Update.");
            send({0x01, 0x00}, {0xd0, 0x00, 0x01, 0x00}, {});
            // eastreamの終了を待って、再接続しにいく
            return reconnect.get_observable();
        }).as_dynamic()
        .flat_map([=](auto){
            /* 再起動する場合、一定時間は応答が見込めないので、無駄なconnectを行わないようにウエイトを入れる */
            return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
                LogManager_AddInformationF("wait %dsec start", rebootWaitSecond);
                BaseSystem::instance().postWithTimeout(rebootWaitSecond, [=](){
                    LogManager_AddInformationF("wait %dsec finish", rebootWaitSecond);
                    s.on_next(pprx::unit());
                    s.on_completed();
                });
            });
        })
        .flat_map([=](auto) {
            /* 応答なしで再起動が行われる */
            LogManager_AddInformation("eastream closed. receive reconnect trigger.");
            return connectWithOption(10 * 1000) /* retryMS = 10,000ms */
            .flat_map([=](auto) {
                LogManager_AddInformation("connect success.");
                return pprx::maybe::success(pprx::unit());
            }).as_dynamic()
            .on_error_resume_next([=](auto err) {
                LogManager_AddWarning("connect error.");
                LogManager_AddWarning(pprx::exceptionToString(err));
                try{std::rethrow_exception(err);}
                catch(pprx::ppex_cardrw& rwerr){
                    LogManager_AddWarning("connect error. CardReader exp.");
                    auto&& desc = rwerr.json().get_optional<std::string>("result.detail.description");
                    LogManager_AddWarningF("rwerr.json().str(). %s", rwerr.json().str());
                    if(*desc == "connetTimeout"){
                        return pprx::maybe::retry();
                    } else {
                        return pprx::maybe::error(make_error_cardrw(makeErrorJson("firmwareUpdateError")));
                    }
                }
                catch(...){}
                return pprx::maybe::error(err);
            }).as_dynamic()
            .retry()
            .flat_map([=](pprx::maybe result) {
                LogManager_AddInformation("connect observableForContinue.");
                return result.observableForContinue<pprx::unit>();
            }).as_dynamic();
        }).as_dynamic()
        .flat_map([=](auto){
            LogManager_AddInformation("reconnect success.");
            return pprx::just(true);
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError("reconnect error.");
            return pprx::just(false);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](bool state){
        LogManager_AddInformation("before observableDeviceConnectState{).");
        return observableDeviceConnectState();
    }).as_dynamic()
    .flat_map([=](DeviceConnectState state){
        LogManager_AddInformation("update1KindFinalize finish.");
        return pprx::just(bSendSuccess);
    }).as_dynamic();
}

/* virtual */
auto CardReaderMiura::getCurrentReaderInfo()  -> const_json_t
{
    return m_cardReaderInfo.as_readonly();
}

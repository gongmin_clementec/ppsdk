//
//  PaymentSystem.cpp
//  ppsdk
//
//  Created by Clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "PaymentJobCreator.h"

#include "JobIdle.h"

#include "JobOpenTerminalJMups.h"
#include "JobActivationJMups.h"

#include "JobCreditSalesJMups.h"
#include "JobCreditPreApprovalJMups.h"
#include "JobCreditAfterApprovalJMups.h"
#include "JobCreditRefundJMups.h"
#include "JobCreditCardCheckJMups.h"

#include "JobUnionPaySalesJMups.h"
#include "JobUnionPayRefundJMups.h"
#include "JobUnionPayPreApprovalJMups.h"
#include "JobUnionPayAfterApprovalJMups.h"
#include "JobUnionPayPreApprovalCancelJMups.h"
#include "JobUnionPayAfterApprovalCancelJMups.h"

#include "JobNFCSalesJMups.h"
#include "JobNFCRefundJMups.h"
#include "JobNFCDailyTotalJMups.h"
#include "JobNFCIntermediateTotalJMups.h"
#include "JobNFCReprintDailyTotalJMups.h"
#include "JobNFCReprintSlipJMups.h"
#include "JobNFCMasterDataUpdateJMups.h"
#include "JobNFCMasterDataCheckJMups.h"

#include "JobJournalJMups.h"
#include "JobReprintSlipJMups.h"

#include "JobSettingTraining.h"
#include "JobSettingTerminalInfo.h"
#include "JobSettingChangeUrl.h"
#include "JobSettingIssuers.h"
#include "JobSettingService.h"

#include "JobSettingReaderFWUpdate.h"
#include "JobSettingSendLog.h"

PaymentJobCreator::PaymentJobCreator()
{
    m_map = map_t{
         { "JobIdle"                   , creator<JobIdle> }
        ,{ "JMups.OpenTerminal"        , creator<JobOpenTerminalJMups> }
        ,{ "JMups.Activation"          , creator<JobActivationJMups> }

        ,{ "JMups.CreditSales"         , creator<JobCreditSalesJMups> }
        ,{ "JMups.CreditPreApproval"   , creator<JobCreditPreApprovalJMups> }
        ,{ "JMups.CreditAfterApproval" , creator<JobCreditAfterApprovalJMups> }
        ,{ "JMups.CreditRefund"        , creator<JobCreditRefundJMups> }
        ,{ "JMups.CreditCardCheck"     , creator<JobCreditCardCheckJMups> }

        ,{ "JMups.UnionPaySales"                , creator<JobUnionPaySalesJMups> }
        ,{ "JMups.UnionPayRefund"               , creator<JobUnionPayRefundJMups> }
        ,{ "JMups.UnionPayPreApproval"          , creator<JobUnionPayPreApprovalJMups> }
        ,{ "JMups.UnionPayAfterApproval"        , creator<JobUnionPayAfterApprovalJMups> }
        ,{ "JMups.UnionPayPreApprovalCancel"    , creator<JobUnionPayPreApprovalCancelJMups> }
        ,{ "JMups.UnionPayAfterApprovalRefund"  , creator<JobUnionPayAfterApprovalCancelJMups> }
        
        ,{ "JMups.NFCSales"            , creator<JobNFCSalesJMups> }
        ,{ "JMups.NFCRefund"           , creator<JobNFCRefundJMups> }
        ,{ "JMups.NFCUpdateMasterData" , creator<JobNFCMasterDataUpdateJMups> }
        ,{ "JMups.NFCCheckMasterData"  , creator<JobNFCMasterDataCheckJMups> }
        ,{ "JMups.Journal"             , creator<JobJournalJMups> }
        ,{ "JMups.ReprintSlip"         , creator<JobReprintSlipJMups> }
        
        ,{ "Setting.ReaderFWUpdate"    , creator<JobSettingReaderFWUpdate> }
        ,{ "Setting.SendLog"           , creator<JobSettingSendLog> }
        ,{ "Setting.Training"          , creator<JobSettingTraining> }
        ,{ "Setting.TerminalInfo"      , creator<JobSettingTerminalInfo> }
        ,{ "Setting.ChangeUrl"         , creator<JobSettingChangeUrl> }
        ,{ "Setting.Issuers"           , creator<JobSettingIssuers> }  // カード会社一覧
        ,{ "Setting.Service"           , creator<JobSettingService> }  // 端末サービス

        /* will be obsolete */
        ,{ "JMups.NFCDailyTotal"        , creator<JobNFCDailyTotalJMups> }
        ,{ "JMups.NFCIntermediateTotal" , creator<JobNFCIntermediateTotalJMups> }
        ,{ "JMups.NFCReprintDailyTotal" , creator<JobNFCReprintDailyTotalJMups> }
        ,{ "JMups.NFCReprintSlip"       , creator<JobNFCReprintSlipJMups> }
    };
}



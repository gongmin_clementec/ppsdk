//
//  LazyObject.h
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_LazyObject__)
#define __h_LazyObject__

#include "StrictShared.h"
#include <map>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/thread.hpp>
#pragma GCC diagnostic pop
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


/*
 遅延削除機能を提供する。

 ネットワークのセッションクラスなど、インスタンスを即時削除できない場合に、
 このクラスを派生することにより、遅延削除が可能になる。

 boost::enable_shared_from_this をベースとし、
 遅延実行する際に下記のように実装を行い、
 遅延削除中でもインスタンスが保持されることを保証するコードとすること。
 
 void foo::exec()
 {
     auto hold = holdThis();
     async([=](){
        hold->explicitCapture();
        遅延実行処理
     });
 }
 
 ここでexplicitCapture()が登場するが、
 これは、C++のLambda式で[=]とした場合に、C++11の規定では全オブジェクトのコピーを行うはずだが、
 実際には、Lambda式内部で使用しないオブジェクトはコピーされない。
 holdThisで自身のスマートポインタを生成しても、Lambda式内部で使用されない場合、
 holdはコピーされないため、foo::exec()を抜けると遅延処理が行われる前に、
 自身が削除される可能性がある。
 そこで、explicitCapture()を明示的に呼び出すことで、hostのコピーを明示的に行う。
 
 ちなみに、下記の場合には、explicitCapture()は不要であるが、最適化による問題も考えられるので、
 explicitCapture() を呼び出した方が無難だと思われる。（コンパイラ依存）

 void foo::exec()
 {
     auto hold = holdThis();
     async([hold](){
        hold->explicitCapture(); // 不要かも知れない
        遅延実行処理
     });
 }
 
 なお、explicitCapture()は自身のポインタを返却するが、
 これは、volatileがlvalueである必要があるためであって、
 返却されるポインタを使用してはならない。
*/

class LazyObject :
    public StrictShared,
    public boost::enable_shared_from_this<LazyObject>
{
public:
    using holder_t = boost::shared_ptr<LazyObject>;
    
private:
    
protected:
    
public:
    LazyObject(const __secret& s) : StrictShared(s) {}
    virtual ~LazyObject() = default;
    
    template<class T> static boost::shared_ptr<T> create()
    {
        auto instance = StrictShared::create<T>();
        return instance;
    }

    holder_t holdThis()
    {
        return shared_from_this();
    }
    
    volatile void* explicitCapture()
    {
        return reinterpret_cast<void*>(this);
    }
};


#endif /* !defined(__h_LazyObject__) */

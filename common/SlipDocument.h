//
//  SlipDocument.h
//  ppsdk
//
//  Created by clementec on 2018/02/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_SlipDocument__)
#define __h_SlipDocument__

#include <string>
#include "BaseSystem.h"

class SlipDocument
{
friend class SlipDocumentParser;
public:
    enum class Alignment
    {
        Bad,
        Default,
        Left,
        Center,
        Right
    };
    
    enum class Font
    {
        Bad,
        Normal,
        DoubleSize_V,
        DoubleSize_H,
        DoubleSize_HV
    };

    struct Element
    {
        virtual         ~Element() = default;
        virtual void    serialize(std::ostream& os) const = 0;
    };
    
    struct Td : public Element
    {
        enum class Type {
            Bad,
            Text,
            WrappedText,    /* 折り返し文字列　*/
            Separator,      /* ─ */
            Separator_T,    /* ┬ */
            Separator_rT,   /* ┴ */
            Separator_X,    /* ┼ */
            Separator_I,    /* │ */
            LogoImage
        };
        int             ratio;
        Type            type;
        Alignment       alignment;
        std::string     body;
        virtual         ~Td() = default;
        virtual void    serialize(std::ostream& os) const;
        
        static Td create(Type t)
        {
            Td td;
            td.ratio = 0;
            td.type = t;
            td.alignment = Alignment::Center;
            return td;
        }
    };

    struct Tr : public Element
    {
        Font                font;
        std::vector<Td>     tds;
                            Tr(Font f) : font(f) {}
        virtual             ~Tr() = default;
        virtual void        serialize(std::ostream& os) const;
    };

    struct Table : public Element
    {
        enum class Border { Off, On };
        
        Border              border;
        std::vector<Tr>     trs;
                            Table(Border b = Border::Off) : border(b) {}
        virtual             ~Table() = default;
        virtual void        serialize(std::ostream& os) const;
        
        using list_t = std::vector<Table>;
    };
    
    struct Document
    {
        std::vector<Table>  tables;
        void                serialize(std::ostream& os) const;
    };
    
    using sp = boost::shared_ptr<SlipDocument>;

private:
    Document m_document;
    void setDocument(Document&& document) { m_document = document; }
    
protected:
public:
            SlipDocument();
    virtual ~SlipDocument();
    
    const Document& getDocument() const { return m_document; }
};


#endif /* !defined(__h_LazyObject__) */

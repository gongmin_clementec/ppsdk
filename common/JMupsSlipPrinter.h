//
//  JMupsSlipPrinter.h
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JMupsSlipPrinter__)
#define __h_JMupsSlipPrinter__

#include "PPReactive.h"
#include <unordered_map>

class SlipPrinter;

class JMupsSlipPrinter
{
private:
    boost::shared_ptr<SlipPrinter>               m_printer;
    std::unordered_map<std::string,std::string>  m_documentMap;

    auto printLoop(const_json_t printInfo) -> pprx::observable<pprx::unit>;
    auto printOne(const std::string& type, json_t json) -> pprx::observable<pprx::unit>;
    
    std::string errotToStatusText(std::exception_ptr err) const;
    
protected:
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool> = 0;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit> = 0;

public:
            JMupsSlipPrinter();
    virtual ~JMupsSlipPrinter();
    
    auto slipPrinterExecute(const_json_t printInfo) -> pprx::observable<pprx::unit>;
};

#endif /* !defined(__h_JMupsSlipPrinter__) */

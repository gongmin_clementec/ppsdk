//
//  SlipDocument.cpp
//  ppsdk
//
//  Created by clementec on 2018/02/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipDocument.h"
#include "LogManager.h"

SlipDocument::SlipDocument()
{
}

/* virtual */
SlipDocument::~SlipDocument()
{
}

/* virtual */
void SlipDocument::Td::serialize(std::ostream& os) const
{
    os << "<td";
    if(alignment == Alignment::Left){
        os << " align=left";
    }
    else if(alignment == Alignment::Right){
        os << " align=right";
    }
    else if(alignment == Alignment::Center){
        os << " align=center";
    }

    if(ratio == 0){
        os << " width=300";
    }
    if(ratio != 0){
        os << " width=" << ratio * 300;
    }
    os << ">";

    if(type == Type::Text){
        os << body;
    }
    else if(type == Type::WrappedText){
        os << body;
    }
    else if(type == Type::Separator){
        os << "<hr />";
    }
    else if(type == Type::Separator_T){
        os << "┬";
    }
    else if(type == Type::Separator_rT){
        os << "┴";
    }
    else if(type == Type::Separator_X){
        os << "┼";
    }
    else if(type == Type::Separator_I){
        os << "│";
    }

    os << "</td>";
}

/* virtual */
void SlipDocument::Tr::serialize(std::ostream& os) const
{
    os << "<tr>";
    for(auto it = tds.cbegin(); it != tds.cend(); it++){
        it->serialize(os);
    }
    os << "</tr>";
}

/* virtual */
void SlipDocument::Table::serialize(std::ostream& os) const
{
    os << "<table>";
    for(auto it = trs.cbegin(); it != trs.cend(); it++){
        it->serialize(os);
    }
    os << "</table>";
}

void SlipDocument::Document::serialize(std::ostream& os) const
{
    os << "<?xml version='1.0' encoding='utf-8' ?>" << std::endl;
    os << "<html>";
    for(auto it = tables.cbegin(); it != tables.cend(); it++){
        it->serialize(os);
    }
    os << "</html>";
}


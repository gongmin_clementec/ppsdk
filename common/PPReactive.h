//
//  PPReactive.h
//  ppsdk
//
//  Created by tel on 2017/11/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_PPReactive__)
#define __h_PPReactive__

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <rxcpp/rx.hpp>
#pragma GCC diagnostic pop
#include <boost/any.hpp>
#include "BaseSystem.h"
#include "LogManager.h"
#include "pprx-thread-pool-scheduler.h"
#include "pprx-main-thread-scheduler.h"

namespace pprx {
    using namespace rxcpp;
    using namespace rxcpp::subjects;
    using namespace rxcpp::sources;
    using namespace rxcpp::operators;
    using namespace rxcpp::util;

    /*
     *  rxcpp には retry_when が存在していないため、
     *  retry をスルーするerrorとretryするエラーを選択的に動作させることができない。
     *  そこで、retry をスルーさせるため
     *  maybe::success() を使用して値を、
     *  maybe::error() を使用してエラーを
     *  maybe オブジェクトでラップして on_next に乗せる。
     *  もし、retry させるためには maybe::retry() を使用して
     *  後段の retry() が反応するように仕向ける。
     */
    class maybe
    {
    private:
        boost::any m_object;
        class retry_error : public std::exception {};
    protected:
    public:
        maybe(boost::any obj) : m_object(obj) {}

        template<typename T> observable<T> observableForContinue()
        {
            if(m_object.type() == typeid(std::exception_ptr)){
                return observable<>::error<T>(boost::any_cast<std::exception_ptr>(m_object)).as_dynamic();
            }
            return observable<>::just(boost::any_cast<T>(m_object)).as_dynamic();
        }
        
        static observable<maybe> success(boost::any obj)
        {
            return observable<>::just(maybe(obj));
        }
        
        static observable<maybe> error(std::exception_ptr ex)
        {
            /* on_error_resume_next() でもretryについては
             * maybeでラップせずにそのままエラーを発行する
             */
            try{std::rethrow_exception(ex);}
            catch(retry_error&){
                return retry();
            }
            catch(...){}
            return observable<>::just(maybe(ex));
        }

        static observable<maybe> retry()
        {
            return observable<>::error<maybe>(std::make_exception_ptr(retry_error()));
        }
    };
    
    class ppexception : public std::exception
    {
    private:
        const std::string   m_sourceFile;
        const int           m_sourceLine;
        const std::string   m_function;
        json_t              m_json;
        std::string         m_json_text;

        
    protected:
        void                setJson(const_json_t json);
        
    public:
        ppexception
        (
         const char*        sourceFile,
         int                sourceLine,
         const char*        function
         ) :
        m_sourceFile(sourceFile),
        m_sourceLine(sourceLine),
        m_function(function)
        {
            LogManager_AddErrorF("exception thrown at %s", where());
        }
        
        virtual ~ppexception() {}
        
        virtual const char* what() const noexcept { return m_json_text.c_str(); }
        const_json_t json() const noexcept { return m_json; }
        std::string where() const noexcept;
    };

    class ppex_aborted : public ppexception
    {
    public:
        ppex_aborted(const std::string& reason, const char* sf, int sl, const char* f);
        virtual ~ppex_aborted() {}
    };
    
    class ppex_network : public ppexception
    {
    public:
        ppex_network(const_json_t detail, const char* sf, int sl, const char* f);
        virtual ~ppex_network() {}
    };

    class ppex_paymentserver : public ppexception
    {
    public:
        ppex_paymentserver(const_json_t detail, const char* sf, int sl, const char* f);
        virtual ~ppex_paymentserver() {}
    };
    
    class ppex_cardrw : public ppexception
    {
    public:
        ppex_cardrw(const_json_t detail, const char* sf, int sl, const char* f);
        virtual ~ppex_cardrw() {}
    };

    class ppex_printer : public ppexception
    {
    public:
        ppex_printer(const_json_t detail, const char* sf, int sl, const char* f);
        virtual ~ppex_printer() {}
    };

    class ppex_internal : public ppexception
    {
    public:
        ppex_internal(const std::string& description, const char* sf, int sl, const char* f);
        virtual ~ppex_internal() {}
    };
    
    class ppex_pinrelated : public ppexception
    {
    public:
        ppex_pinrelated(const_json_t detail, const char* sf, int sl, const char* f);
        virtual ~ppex_pinrelated() {}
    };
    
    struct unit {};
    
    inline std::string exceptionToString(std::exception_ptr ex)
    {
        std::string s;
        try{std::rethrow_exception(ex);}
        catch(ppexception& ppex){
            s = (boost::format("ppexception\n%s\n%s") % ppex.where() % ppex.what()).str();
        }
        catch(std::exception& stdex){
            s = (boost::format("std::exception\n%s") % stdex.what()).str();
        }
        catch(...){
            s = "unknown exception";
        }
        return s;
    }
    
    template <class T> pprx::observable<T> start(boost::function<T()> f)
    {
        return pprx::observable<>::create<T>([f](pprx::subscriber<T> s){
            try{
                s.on_next(f());
                s.on_completed();
            }
            catch(std::exception& ex){
                s.on_error(std::make_exception_ptr(ex));
            }
        });
    }

    template <class T> pprx::observable<T> start_async(boost::function<T()> f)
    {
        return start<T>(f)
        .subscribe_on(pprx::observe_on_thread_pool());
    }
}

#define make_error_aborted(m)       std::make_exception_ptr(pprx::ppex_aborted(m,__FILE__,__LINE__,__FUNCTION__))
#define make_error_network(j)       std::make_exception_ptr(pprx::ppex_network(j,__FILE__,__LINE__,__FUNCTION__))
#define make_error_paymentserver(j) std::make_exception_ptr(pprx::ppex_paymentserver(j,__FILE__,__LINE__,__FUNCTION__))
#define make_error_cardrw(j)        std::make_exception_ptr(pprx::ppex_cardrw(j,__FILE__,__LINE__,__FUNCTION__))
#define make_error_printer(j)       std::make_exception_ptr(pprx::ppex_printer(j,__FILE__,__LINE__,__FUNCTION__))
#define make_error_internal(m)      std::make_exception_ptr(pprx::ppex_internal(m,__FILE__,__LINE__,__FUNCTION__))
#define make_error_internalf(m,p)   std::make_exception_ptr(pprx::ppex_internal((boost::format(m)%p).str(),__FILE__,__LINE__,__FUNCTION__))
#define make_error_pinreleted(j)      std::make_exception_ptr(pprx::ppex_pinrelated(j,__FILE__,__LINE__,__FUNCTION__))


#define throw_error_aborted(m)       { throw pprx::ppex_aborted(m,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_network(j)       { throw pprx::ppex_network(j,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_paymentserver(j) { throw pprx::ppex_paymentserver(j,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_cardrw(j)        { throw pprx::ppex_cardrw(j,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_printer(j)       { throw pprx::ppex_printer(j,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_internal(m)      { throw pprx::ppex_internal(m,__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_internalf(m,p)   { throw pprx::ppex_internal((boost::format(m)%p).str(),__FILE__,__LINE__,__FUNCTION__); }
#define throw_error_pinreleted(j)      { throw pprx::ppex_pinrelated(j,__FILE__,__LINE__,__FUNCTION__); }

#endif /* !defined(__h_PPReactive__) */

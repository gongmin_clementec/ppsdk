//
//  HttpServer.h
//  ppsdk
//
//  Created by tel on 2018/07/03.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_HttpServer__)
#define __h_HttpServer__

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/asio/ssl/stream.hpp>
#pragma GCC diagnostic pop
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/ip/tcp.hpp>
#include "PPReactive.h"

class HttpServer : public boost::enable_shared_from_this<HttpServer>
{
friend class boost::enable_shared_from_this<HttpServer>;

public:
    using sp = boost::shared_ptr<HttpServer>;
    
    class WebSocket
    {
    public:
        using sp = boost::shared_ptr<WebSocket>;
        
    private:
        pprx::subject<std::string>    m_subject;

    protected:
        void emitReceiveData(const std::string& data)
        {
            m_subject.get_subscriber().on_next(data);
        }
        
        
    public:
                WebSocket() = default;
        virtual ~WebSocket() = default;
        
        virtual void send(const std::string& data) = 0;
        virtual void close() = 0;
        
        auto getObservable() { return m_subject.get_observable(); }
    };
    
    enum class State {
        Stopped, Prepare, Running, Unprepare
    };
    
    std::string stateToString(State s) const
    {
        switch(s){
            case State::Stopped:    return "State::Stopped";
            case State::Prepare:    return "State::Prepare";
            case State::Running:    return "State::Running";
            case State::Unprepare:  return "State::Unprepare";
        }
        return "State::(unknown)";
    }
    
private:
    std::string             m_address;
    uint16_t                m_port;
    boost::shared_ptr<boost::filesystem::path>      m_docroot;
    boost::shared_ptr<boost::asio::io_context>      m_ioc;
    boost::shared_ptr<boost::thread_group>          m_threads;
    boost::shared_ptr<boost::asio::ssl::context>    m_sslctx;
    pprx::subject<WebSocket::sp>                    m_subject;
    boost::recursive_mutex                          m_stateChange_mtx;

    pprx::subjects::behavior<State>     m_stateSubject;
    State                               m_currentState;
    
    enum class Request {
        Start, Stop, Nothing
    };
    
    std::string requestToString(Request r) const
    {
        switch(r){
            case Request::Start:   return "Request::Start";
            case Request::Stop:    return "Request::Stop";
            case Request::Nothing: return "Request::Nothing";
        }
        return "Request::(unknown)";
    }

    Request        m_nextRequest;
    
    void    doPrepare();
    void    doUnprepare();

    
    auto startListener() -> pprx::observable<pprx::unit>;
    auto stopListener() -> pprx::observable<pprx::unit>;

    void trySetRequest(Request request);

protected:
    HttpServer();

public:
    virtual ~HttpServer();

    static auto create()
    {
        return boost::shared_ptr<HttpServer>(new HttpServer());
    }
    
    void suspend();
    void resume();
    
    void begin(const std::string& address, uint16_t port, const boost::filesystem::path& doc_root);
    auto getObservable() { return m_subject.get_observable(); }


    auto ioc() { return m_ioc; }
    auto sslctx() { return m_sslctx; }
    auto docroot() { return m_docroot; }
    
    template <typename T> void emitWebSocket(boost::shared_ptr<T> ws)
    {
        m_subject.get_subscriber().on_next(boost::static_pointer_cast<WebSocket>(ws));
    }

    pprx::observable<State> getState() { return m_stateSubject.get_observable(); }

private:
    class listener : public boost::enable_shared_from_this<listener>
    {
    public:
        using sp = boost::shared_ptr<listener>;
        
    private:
        HttpServer::sp  m_server;
        boost::asio::ip::tcp::endpoint  m_endpoint;
        boost::asio::ip::tcp::acceptor  m_acceptor;
        boost::asio::ip::tcp::socket    m_socket;
        
    public:
        listener(
                 HttpServer::sp  server,
                 boost::asio::ip::tcp::endpoint   endpoint
                 );
        virtual ~listener();
        
        bool prepare();
        void run();
        void stop();
        void do_accept();
        void on_accept(boost::system::error_code ec);
    };
    
    listener::sp m_listener;
};

#endif /* HttpServer_hpp */

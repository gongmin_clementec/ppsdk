//
//  PaymentSystem.cpp
//  ppsdk
//
//  Created by Clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "PaymentJob.h"
#include "BaseSystem.h"
#include "TMSAccess.h"
#include "CardReader.h"
#include "pprx-thread-pool-scheduler.h"
#include "JMupsJobStorage.h"
#include <boost/xpressive/xpressive.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

PaymentJob::PaymentJob(const __secret& s) :
    LazyObject(s),
    m_bTrainingMode(false),
    m_bUseDigiSignInTrainingMode(false),
    m_abortRequest(pprx::unit()),
    m_abortBySystemRequest(pprx::unit()),
    m_abortReason(AbortReason::Bad),
    m_bValidChildJob(false),
    m_bUseChildJob(false)
{
    LogManager_Function();
}

/* virtual */
PaymentJob::~PaymentJob()
{
    LogManager_Function();
}

auto PaymentJob::getObservable(dispatch_t onDispatch, dispatchCancel_t onDispatchCancel)
 -> pprx::observable<const_json_t>
{
    m_funcDispatch          = onDispatch;
    m_funcDispatchCancel    = onDispatchCancel;
    
    auto removeCardReaderProcess = [=](){
        auto reader = getCardReader();
        if(reader){
            return reader->setICCardRemoved()
            .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
            .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
            .take(1)
            .map([=](pprx::maybe){ return pprx::unit(); })
            .as_dynamic();
        }
        else{
            return pprx::just(pprx::unit()).as_dynamic();
        }
    };
    
    auto holdThis = this->holdThis();
    return m_abortRequest.get_observable()
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        auto reader = getCardReader();
        if(reader){
            reader->prepareObservables();
        }
        return getObservableSelf();
    }).as_dynamic()
    /*
     ここで take(1) をしても、上流でエラーが複数発生すると error が複数流れてしまう。
     （tap() を使って確認済み）
     全体の流れが、unsubscribe（またはerrorやcompletedで終了）すると全ての観測が終了し、
     値もエラーも発生せず、さらに、completeにもならない。
     （※このcompleteにもならない点にも注意）
     後続の処理を行なっている場合には、依然として観測は続く。
     したがって、エラーも値（boost::anyでラッピング）として下流に流し、take(1)する対応とした。
    */
    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<const_json_t>();
    }).as_dynamic()
    .flat_map([=](const_json_t result){
        LogManager_AddInformation("PaymentJob emit success");
        return pprx::just(pprx::unit())
        .flat_map([=](auto) {
            // リクルートNFC対応:
            // リクルートのアプリではマスターデータ更新チェックはバックグラウンドで実行するらしい。
            // そのため、画面遷移などでリクルートアプリ側のsubscriberがいなくなったあとに
            // waitForShowingJobResultを発行するとシーケンスが崩れてしまう。
            // それを回避するためにwaitForShowingJobResultを呼び出さない条件を追加している。
            if(!isNeedWaitForShowingJobResult()) {
                return pprx::maybe::success(pprx::unit()).as_dynamic();
            } else {
                return callOnDispatchWithCommandAndSubParameter("waitForShowingJobResult", "", result)
                .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
                .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); }).as_dynamic();
            }
        }).as_dynamic()
        .take(1)
        .map([=](pprx::maybe){ return pprx::unit(); })
        .flat_map([=](pprx::unit){
            return removeCardReaderProcess()
            .flat_map([=](auto){
                return finalize();
            }).as_dynamic()
            .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
            .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
            .take(1)
            .map([=](pprx::maybe){ return pprx::unit(); })
            .as_dynamic();
        }).as_dynamic()
        .map([=](pprx::unit){
            return result;
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddInformationF("PaymentJob emit error\n%s", pprx::exceptionToString(err));
        auto abortReason = [=]{
            boost::unique_lock<boost::recursive_mutex> lock(m_abortReason_mtx);
            return m_abortReason;
        }();

        if(abortReason == AbortReason::ForceAnyway){
            LogManager_AddInformation("Abort Force Anyway");
            /* 唯一、緊急終了の場合にはエラーを発行する */
            auto cardReaderProcess = [=](){
                auto reader = getCardReader();
                if(reader){
                    reader->setLockTransactionState();
                    return reader->abortTransaction()
                    .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
                    .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
                    .take(1)
                    .map([=](pprx::maybe){ return pprx::unit(); })
                    .as_dynamic();
                }
                else{
                    return pprx::just(pprx::unit()).as_dynamic();
                }
            };
            
            return cardReaderProcess()
            .flat_map([=](auto){
                return finalize();
            }).as_dynamic()
            .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
            .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
            .take(1)
            .map([=](pprx::maybe){ return pprx::unit(); })
            .flat_map([=](pprx::unit) {
                return pprx::error<const_json_t>(err);
            }).as_dynamic();
        }

        /* 緊急エラー以外の終了については、エラー情報を含んだ値を発行する */
        json_t result;
        try{std::rethrow_exception(err);}
        catch(pprx::ppexception& ex){
            result = json_t::from_str(ex.what());
        }
        catch(std::exception& ex){
            result.put("status", "error");
            result.put("result.where", "internal");
            result.put("result.detail.description", ex.what());
        }
        catch(...){
            LogManager_AddDebug("what??");
        }
        
        return callOnDispatchCancel()
        .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
        .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
        .take(1)
        .map([=](pprx::maybe){ return pprx::unit(); })
        .flat_map([=](auto)mutable{
            handleDispatchCancelResult(result);
            return pprx::just(pprx::unit())
            .flat_map([=](auto) {
                // リクルートNFC対応:
                // リクルートのアプリではマスターデータ更新チェックはバックグラウンドで実行するらしい。
                // そのため、画面遷移などでリクルートアプリ側のsubscriberがいなくなったあとに
                // waitForShowingJobResultを発行するとシーケンスが崩れてしまう。
                // それを回避するためにwaitForShowingJobResultを呼び出さない条件を追加している。
                if(!isNeedWaitForShowingJobResult()) {
                    return pprx::maybe::success(pprx::unit()).as_dynamic();
                } else {
                    return callOnDispatchWithCommandAndSubParameter("waitForShowingJobResult", "", result)
                    .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
                    .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); }).as_dynamic();
                }
            }).as_dynamic()
            .take(1)
            .map([=](pprx::maybe){ return pprx::unit(); })
            .flat_map([=](auto){
                auto reader = getCardReader();
                if(reader){
                    reader->setLockTransactionState();
                }
                return waitForRemovingContact()
                .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
                .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
                .take(1)
                .map([=](pprx::maybe){ return pprx::unit(); });
            }).as_dynamic();
        }).as_dynamic()
        .flat_map([=](pprx::unit) {
            return pprx::just(pprx::unit())
            .timeout(std::chrono::seconds(3))
            .flat_map([=](auto){
                LogManager_AddInformation("Cancel setICCardRemoved()");
                return removeCardReaderProcess();
            }).as_dynamic()
            .flat_map([=](auto){
                return finalize();
            }).as_dynamic()
            .flat_map([=](auto){ return pprx::maybe::success(pprx::unit()); })
            .on_error_resume_next([=](auto err){ return pprx::maybe::error(err); })
            .take(1)
            .map([=](pprx::maybe){ return pprx::unit(); });
        }).as_dynamic()
        .map([=](pprx::unit) {
            return result.as_readonly();
        }).as_dynamic();
    })
    .finally([=](){
        LogManager_AddDebug("PaymentJob finally.");
        holdThis->explicitCapture();
    });
}

auto PaymentJob::retrieveJobDispatch() -> dispatch_t
{
    return m_funcDispatch.referenceBlockWithResult<dispatch_t>([&](auto&& _value){ return _value; });
}

auto PaymentJob::retrieveJobDispatchCancel() -> dispatchCancel_t
{
    return m_funcDispatchCancel;
}

/* virtual */
auto PaymentJob::isUsedChildJob() -> bool const
{
    return m_bUseChildJob;
}

/* virtual */
auto PaymentJob::createChildJobParam() -> const_json_t
{
    return const_json_t();
}

/* virtual */
auto PaymentJob::isNeedWaitForShowingJobResult() -> bool
{
    return true;
}

auto PaymentJob::registDispatches(dispatch_t dispatch, dispatchCancel_t dispatchCancel) -> pprx::observable<pprx::unit>
{
    m_funcDispatch          = dispatch;
    m_funcDispatchCancel    = dispatchCancel;
    return pprx::just(pprx::unit());
}

auto PaymentJob::getTradeDateTime(std::string dateTimeStr) -> const_json_t
{
    json_t json;
    boost::posix_time::ptime dtime;
    if(dateTimeStr.size() == 0) {
        dtime = boost::posix_time::second_clock::local_time();
    } else {
        dtime = BaseSystem::parseRFC2616DateTimeText(dateTimeStr);
    }
    auto isoDateTimeStr = boost::posix_time::to_iso_string(dtime);
    json.put("date", isoDateTimeStr.substr(2,6));
    json.put("time", isoDateTimeStr.substr(9));
    return json;
}

/* virtual */
bool PaymentJob::isJobTimeoutRequired() const
{
    return true;
}

/* virtual */
auto PaymentJob::initializeChildJob() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

/* virtual */
auto PaymentJob::mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

/* virtual */
auto PaymentJob::getChildSlipDocument() -> pprx::observable<const_json_t>
{
    return pprx::just(const_json_t());
}

/* virtual */
auto PaymentJob::execChildJobDigiSign() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

/* virtual */
auto PaymentJob::execChildJobSendTagging() -> pprx::observable<JMupsAccess::response_t>
{
    return pprx::just(JMupsAccess::response_t());
}

void PaymentJob::abort(AbortReason reason)
{
    LogManager_Function();
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_abortReason_mtx);
        m_abortReason = reason;
    }
    auto status = [reason](){
        switch(reason){
            case AbortReason::UserRequest:  return "aborted";
            case AbortReason::Background:   return "abortedByBackground";
            case AbortReason::JobTimer:     return "abortedByJobTimer";
            case AbortReason::ForceAnyway:  return "abortForceAnyway";
            default: break;
        }
        return "unknown";
    };
    m_abortRequest.get_subscriber().on_error(make_error_aborted(status()));
    if(reason == AbortReason::ForceAnyway){
        m_abortBySystemRequest.get_subscriber().on_error(make_error_aborted(status()));
    }
}

auto PaymentJob::finalize() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    // clear saved url
    std::string url("");
    boost::dynamic_pointer_cast<JMupsJobStorage>(getStorage())->getCurrentHpsUrl(url);
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        if(getCardReader()){
            LogManager_AddInformation("getCardReader()->finalize()");
            return getCardReader()->finalize().as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        m_abortRequest.get_subscriber().on_completed();
        return finalizeSelf();
    }).as_dynamic();
}

bool PaymentJob::internalCallOnDispatch(const std::string& inParam, std::string& outParam)
{
    LogManager_Function();

    auto hold = holdThis();

    auto abortReason = [=]{
        boost::unique_lock<boost::recursive_mutex> lock(m_abortReason_mtx);
        return m_abortReason;
    }();
    if(abortReason == AbortReason::ForceAnyway){
        LogManager_AddInformation("now force aborting -> dispatch should be cancel");
        outParam = "{}";
        return false;
    }
    
    bool bContinue = false;
    m_funcDispatch.referenceBlock([&](auto funcDispatch) mutable {
        LogManager_ScopedF("internalCallOnDispatch: %s", inParam);
        if(funcDispatch){
            bContinue = funcDispatch(inParam, outParam);
            LogManager_AddInformationF("bContinue: %s", (bContinue ? "true" : "false"));
            LogManager_AddInformationF("outParam: %s", outParam);
        }
        else{
            LogManager_AddInformation("dispatch not execute");
        }
    });
    
    return bContinue;
}

auto PaymentJob::callOnDispatchCancel() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        m_funcDispatchCancel();
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

bool PaymentJob::internalCallOnDispatchWithCommand(const std::string& command, json_t& outParam)
{
    LogManager_Function();
    json_t json;
    json.put("command", command);

    const auto in = json.str();
    std::string out;
    const bool bContinue = internalCallOnDispatch(in, out);
    
    outParam = json_t::from_str(out);
    
    return bContinue;
}

bool PaymentJob::internalCallOnDispatchWithMessage(const std::string& message)
{
    LogManager_Function();
    json_t json;
    json.put("command", "notice");
    json.put("message", message);
    
    const auto in = json.str();
    std::string out;
    const bool bContinue = internalCallOnDispatch(in, out);
    
    return bContinue;
}

bool PaymentJob::internalCallOnDispatchWithCommandAndSubParameter(const std::string& command, const std::string& sub, const_json_t subParam, json_t& outParam)
{
    LogManager_Function();
    json_t json;
    json.put("command", command);
    if(sub.empty()){
        json.put("parameter", subParam.clone());
    }
    else{
        json.put("parameter." + sub, subParam.clone());
    }
    
    const auto in = json.str();
    std::string out;
    const bool bContinue = internalCallOnDispatch(in, out);
    
    outParam = json_t::from_str(out);
    
    return bContinue;
}

bool PaymentJob::internalCallOnDispatchWithMessageAndSubParameter(const std::string& message, const std::string& sub, const_json_t subParam)
{
    LogManager_Function();
    json_t json;
    json.put("command", "notice");
    json.put("message", message);
    if(sub.empty()){
        json.put("parameter", subParam.clone());
    }
    else{
        json.put("parameter." + sub, subParam.clone());
    }
    
    const auto in = json.str();
    std::string out;
    const bool bContinue = internalCallOnDispatch(in, out);
    
    return bContinue;
}


auto PaymentJob::callOnDispatchWithCommand(const std::string& command)
-> pprx::observable<const_json_t>
{
    auto hold = shared_from_this();
    return pprx::observable<>::create<const_json_t>
    (
     [=](pprx::subscriber<const_json_t> s)
     {
         hold->explicitCapture();
         json_t json;
         const bool bContinue = internalCallOnDispatchWithCommand(command, json);
         if(!bContinue){
             s.on_error(make_error_aborted("aborted"));
             return;
         }
         s.on_next(json);
         s.on_completed();
     });
}

auto PaymentJob::callOnDispatchWithMessage(const std::string& message)
 -> pprx::observable<pprx::unit>
{
    auto hold = shared_from_this();
    return pprx::observable<>::create<pprx::unit>
    (
     [=](pprx::subscriber<pprx::unit> s)
     {
         hold->explicitCapture();
         const bool bContinue = internalCallOnDispatchWithMessage(message);
         if(!bContinue){
             s.on_error(make_error_aborted("aborted"));
             return;
         }
         s.on_next(pprx::unit());
         s.on_completed();
     });
}

auto PaymentJob::callOnDispatchWithCommandAndSubParameter(const std::string& command, const std::string& sub, const_json_t subParam)
-> pprx::observable<const_json_t>
{
    auto hold = shared_from_this();
    return pprx::observable<>::create<const_json_t>
    (
     [=](pprx::subscriber<const_json_t> s)
     {
         hold->explicitCapture();
         json_t json;
         const bool bContinue = internalCallOnDispatchWithCommandAndSubParameter(command, sub, subParam, json);
         if(!bContinue){
             s.on_error(make_error_aborted("aborted"));
             return;
         }
         s.on_next(json);
         s.on_completed();
     });
}

auto PaymentJob::callOnDispatchWithMessageAndSubParameter(const std::string& message, const std::string& sub, const_json_t subParam)
 -> pprx::observable<pprx::unit>
{
    auto hold = shared_from_this();
    return pprx::observable<>::create<pprx::unit>
    (
     [=](pprx::subscriber<pprx::unit> s)
     {
         hold->explicitCapture();
         const bool bContinue = internalCallOnDispatchWithMessageAndSubParameter(message, sub, subParam);
         if(!bContinue){
             s.on_error(make_error_aborted("aborted"));
             return;
         }
         s.on_next(pprx::unit());
         s.on_completed();
     });
}

auto PaymentJob::callOnDispatchTradeInfo(const TradeInfoBuilder& builder, TradeInfoBuilder::updatable updatable, std::string strOp)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithCommandAndSubParameter("waitForTradeInfo", "", builder.build(updatable, strOp));
}

auto PaymentJob::waitForReaderConnect() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        getCardReader()->prepareObservables();  /* 再接続の可能性があるので、再度observableを準備しなおす必要がある */
        return getCardReader()->connect();
    })
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        return pprx::maybe::success(pprx::unit());
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        try{std::rethrow_exception(err);}
        catch(pprx::ppex_cardrw& rwerr){
            auto&& desc = rwerr.json().get_optional<std::string>("result.detail.description");
            if(!desc){
                return pprx::maybe::error(make_error_internal("bad json format")).as_dynamic();
            }
            else{
                if((*desc == "connetTimeout") || (*desc == "deviceOpenError")){
                    return callOnDispatchWithCommand("waitForRetryConnectingReader")
                    .flat_map([=](const_json_t json){
                        const auto bRetry = json.get_optional<bool>("bRetry");
                        if(bRetry && *bRetry == true ){
                            return pprx::maybe::retry();
                        }
                        else{
                            return pprx::maybe::error(make_error_internal("CardReader connection failure")).as_dynamic();
                        }
                    })
                    .as_dynamic();
                }
            }
        }
        catch(...){}
        return pprx::maybe::error(err).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe ctrl){
        return ctrl.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](pprx::unit){
        return getCardReader()->getReaderInfo();
    }).as_dynamic()
    .map([=](const_json_t info){
        TMSAccess::setConnectedCardReaderInfo(info);
        return pprx::unit();
    }).as_dynamic();
}

auto PaymentJob::waitForReaderConnectAndReadMedia(const_json_t readOption, const std::string& displayTxt) -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        return waitForReaderConnect().as_dynamic()
        .flat_map([=](auto){
            return getCardReader()->errorOnDeviceDisconnected(pprx::unit());
        }).as_dynamic()
        .flat_map([=](auto){
            return getCardReader()->isCardInserted();
        }).as_dynamic()
        .flat_map([=](auto){
            return callOnDispatchWithMessage("waitForCardRead").as_dynamic();
        }).as_dynamic()
        .flat_map([=](auto){
            if(displayTxt.empty()){
                return pprx::just(pprx::unit()).as_dynamic();
            }
            else{
                return getCardReader()->displayTextNoLf(displayTxt);
            }
        }).as_dynamic()
        .flat_map([=](auto){
            return getCardReader()->beginCardRead(readOption);
        }).as_dynamic()
        .flat_map([=](const_json_t data) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING"))
            .map([=](auto){
                return data;
            }).as_dynamic();
        }).as_dynamic()
        .flat_map([=](const_json_t data){
            return pprx::maybe::success(data);
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        try{std::rethrow_exception(err);}
        catch(pprx::ppex_cardrw& rwerr){
            LogManager_AddDebugF("card r/w exception:\n%s",rwerr.json().str());
            auto descrip = rwerr.json().get_optional<std::string>("result.detail.description");
            if(descrip && ((*descrip) == "cardExist")){
                auto display = getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_REMOVE_TRY");
                return getCardReader()->displayTextNoLf(display)
                .as_dynamic()
                .flat_map([=](auto){
                    return pprx::maybe::error(err);
                }).as_dynamic();
            }
            else if(descrip && ((*descrip) == "disableCardInserted")){
                auto display = getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_CHECK_ERROR");
                return getCardReader()->displayTextNoLf(display)
                .as_dynamic()
                .flat_map([=](auto){
                    return pprx::maybe::error(err);
                }).as_dynamic();
            }
            else if(descrip && ((*descrip) == "disconnected")){
                return pprx::maybe::error(err);
            }
            else{
                auto display = getDisplayMsgForReader("MIURA_CREDIT_UI_ERROR_PROCESSING");
                if(display.empty()){
                    return pprx::maybe::error(err);
                }
                else{
                    return getCardReader()->displayTextNoLf(display).as_dynamic()
                    .flat_map([=](auto){
                        return pprx::maybe::error(err);
                    }).as_dynamic();
                }
            }
        }
        catch(...){
            LogManager_AddDebug("other exception -> thru");
        }
        return pprx::maybe::error(err);
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<const_json_t>();
    }).as_dynamic()
    .take(1);
}

auto PaymentJob::getDisplayMsgForReader(const std::string& msgId, const bool bContactless/*= false*/)
 -> std::string
{
    return getCardReader()->getDisplayMsgForReader(msgId, bContactless);
}

auto PaymentJob::waitForRemovingContact()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    if(getCardReader() == nullptr){
        LogManager_AddDebug("PaymentJob-カードリーダ不要の業務");
        return pprx::just(pprx::unit());
    }
       
    auto buseChildjob = isUsedChildJob();
    if(buseChildjob){
        return pprx::just(pprx::unit()).as_dynamic();
    }
    
    return getCardReader()->observableDeviceConnectState().take(1)
    .flat_map([=](CardReader::DeviceConnectState connState){
        if(connState != CardReader::DeviceConnectState::Connected){
            LogManager_AddDebug("PaymentJob-デバイスが接続されていない");
            /* R/Wが切断状態の場合、カード抜き待ち処理は存在しない */
            return pprx::just(pprx::unit()).as_dynamic();
        }
        
        LogManager_AddDebug("paymentjobデバイスが接続されている");
        return getCardReader()->abortTransaction()
        .flat_map([=](auto) {
            return getCardReader()->observableICCardSlotState().take(1);
        }).as_dynamic()
        .flat_map([=](CardReader::ICCardSlotState slotState){
            if(slotState != CardReader::ICCardSlotState::Inserted){
                LogManager_AddDebug("PaymentJob-カードが挿入されている状態ではなくなった");
                /* カードが挿入されている状態でない */
                return pprx::just(pprx::unit()).as_dynamic();
            }
            else{
                return pprx::just(pprx::unit()).as_dynamic()
                .flat_map([=](auto){
                    auto display = getDisplayMsgForReader("MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD");
                    if(display.empty()){
                        return pprx::just(pprx::unit()).as_dynamic();
                    }
                    else{
                        return getCardReader()->displayTextNoLf(display);
                    }
                }).as_dynamic()
                .flat_map([=](auto){
                    LogManager_AddDebug("PaymentJob-observableICCardSlotState");
                    return getCardReader()->observableICCardSlotState();
                }).as_dynamic()
                .timeout(std::chrono::seconds(5))
                .flat_map([=](CardReader::ICCardSlotState slotState){
                    if(slotState != CardReader::ICCardSlotState::Inserted){
                        return pprx::just(pprx::unit()).as_dynamic();
                    }
                    return pprx::never<pprx::unit>().as_dynamic();
                }).as_dynamic()
                .take(1)
                .on_error_resume_next([=](auto err){
                    return pprx::just(pprx::unit()).as_dynamic();
                })
                .map([=](auto){
                    return pprx::unit();
                }).as_dynamic();
            }
        }).as_dynamic();
    }).as_dynamic();
}


#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <boost/algorithm/hex.hpp>
#include <boost/iostreams/copy.hpp>
#include "WindowsBitmapFileBuilder.h"

std::string PaymentJob::encryptHtml5RawCanvasDigiSignData(const_json_t html5, const std::string& modulus, const std::string& exponent) const
{
    std::stringstream srcs;
    {
        const auto width        = html5.get<int>("width");
        const auto height       = html5.get<int>("height");
        const auto depth        = html5.get<int>("depth");
        const auto pixelType    = html5.get<std::string>("pixelType");
        const auto pixelData    = html5.get<std::string>("pixelData");
        
        LogManager_AddInformationF("width:%d, height:%d, depth:%d, pixelType:%s", width % height % depth % pixelType);
        
        if(depth != 8){
            throw_error_internal("depth != 8");
        }
        if(pixelType != "RGBA"){
            throw_error_internal("pixelType != RGBA");
        }
        
        const auto pixelBytes = 4;
        const auto rowBytes   = width * pixelBytes;

        const auto destWidth  = 178;
        const auto destHeight = 44;

        {
            auto charsExpected = height * rowBytes * 2;
            if(charsExpected != pixelData.size()){
                auto err = (boost::format("wrong pixDataSize %d expected %d") % pixelData.size() % charsExpected).str();
                throw_error_internal(err);
            }
        }

        /* 拡大処理は未実装であるため例外とする */
        if(destWidth >= width){
            throw_error_internal("dstWidth >= width");
        }
        if(destHeight >= height){
            throw_error_internal("destHeight >= height");
        }

#if defined(DEBUG)
        {
            auto html5jsonfile = BaseSystem::instance().getDocumentDirectoryPath() / "html5.json";
            std::ofstream opix(html5jsonfile.string());
            opix << html5.str();
        }

        {
            auto pixeldatafile = BaseSystem::instance().getDocumentDirectoryPath() / "pixeldata.txt";
            std::ofstream opix(pixeldatafile.string());
            opix << pixelData;
        }
#endif
        
        bytes_t srcpix;
        boost::algorithm::unhex(pixelData.begin(), pixelData.end(), std::back_inserter(srcpix));
        
        bytes_t dstpix(destWidth * destHeight);
        {
            const double ratio_y = static_cast<double>(destHeight) / static_cast<double>(height);
            const double ratio_x = static_cast<double>(destWidth)  / static_cast<double>(width);

            for(auto y = 0; y < height; y++){
                const uint8_t* sptr = srcpix.data() + (y * rowBytes);
                for(auto x = 0; x < width; x++, sptr += pixelBytes){
                    auto&& r = sptr[0];
                    auto&& g = sptr[1];
                    auto&& b = sptr[2];
                    auto&& a = sptr[3];
                    auto&& pix = ( (255 * (255 - a)) + (((r + g + b) / 3) * a) ) / 255;
                    uint8_t&& pixel = (pix == 0) ? 0xFF : 0x00; /* dstpix に OR 演算するので反転 */
                    auto&& dy = static_cast<int>(ratio_y * static_cast<double>(y));
                    auto&& dx = static_cast<int>(ratio_x * static_cast<double>(x));
                    dstpix[ (dy * destWidth) + dx ] |= pixel;
                }
            }
        }
        
        WindowsBitmapFileBuilder<uint8_t>().grayImageToBinBitmap
        (
         srcs,
         dstpix.data(),
         destWidth, destHeight, destWidth,
         [](auto pix){
             return pix != 0x00;
         });
    }
    
#if defined(DEBUG)
    {
        auto bmpfile = BaseSystem::instance().getDocumentDirectoryPath() / "sign.bmp";
        std::ofstream obmp(bmpfile.string());
        boost::iostreams::copy(srcs, obmp);
        srcs.seekg(0);
    }
#endif
    
    std::stringstream dsts;
    {
        RSA* rsa = ::RSA_new();
        ::BN_hex2bn(&rsa->n, modulus.c_str());
        ::BN_hex2bn(&rsa->e, exponent.c_str());
        const auto blockSize = ::RSA_size(rsa);
        /*
         RSA_PKCS1_PADDINGの場合、ソースの最大バイト数は RSA_size() -11 にしなければならない。
         https://www.openssl.org/docs/man1.0.2/crypto/RSA_public_encrypt.html
         */
        std::vector<uint8_t> raw(blockSize - 11);
        std::vector<uint8_t> enc(blockSize);
        
        while(!srcs.eof()){
            srcs.read(reinterpret_cast<char*>(raw.data()), raw.size());
            const auto readBytes = srcs.gcount();
            
            const auto writtenBytes = ::RSA_public_encrypt(
                                                           static_cast<int>(readBytes),
                                                           raw.data(),
                                                           enc.data(),
                                                           rsa,
                                                           RSA_PKCS1_PADDING);
            if(writtenBytes == -1){
                LogManager_AddError("writtenBytes == -1");
                throw_error_internal(::ERR_error_string(::ERR_get_error(), nullptr));
            }
            boost::algorithm::hex
            (
             enc.begin(),
             enc.begin() + writtenBytes,
             std::ostream_iterator<char>(dsts));
        }
        ::RSA_free(rsa);
    }
    
    return dsts.str();
}

void PaymentJob::handleDispatchCancelResult(json_t& dispatchJson)
{
    LogManager_AddDebugF("\n%s", dispatchJson.str());
    
    auto status = dispatchJson.get<std::string>("status");
    if(status == "error"){
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(getStorage());
        auto bTradeCompletionFlag = js->getTradeResultData().get_optional<bool>("ppsdk.bTradeCompletionFlag");
        if(bTradeCompletionFlag&&(*bTradeCompletionFlag)){
            dispatchJson.put("result.recruit.bTradeCompletionFlag", true);
        }
        else{
            dispatchJson.put("result.recruit.bTradeCompletionFlag", false);
        }
        
        std::string url("");
        js->getCurrentHpsUrl(url);
        dispatchJson.put("result.recruit.hpsUrl", url);
        
        auto where = dispatchJson.get<std::string>("result.where");
        if(where == "r/w"){
            auto descrip = dispatchJson.get_optional<std::string>("result.detail.description");
            LogManager_AddInformationF("result.detail.description:\n%s",*descrip);
            dispatchJson.put("result.recruit.detail", *descrip);
            if(descrip->find("disconnected")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E2001");
            }
            else if(descrip->find("icCardRemoved")!= std::string::npos ){
                dispatchJson.put("result.recruit.errorCode", "PP_E2002");
            }
            else if(descrip->find("general")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E2003");
            }
            else if(descrip->find("pinTimeOut")!= std::string::npos){
                dispatchJson.put("result.recruit.detail", "PIN入力タイムアウト");
                dispatchJson.put("result.recruit.errorCode", "PP_E2003");
            }
            else if(descrip->find("ICCardError")!= std::string::npos){
                dispatchJson.put("result.recruit.detail", "ICカードエラー。取引を中断します。");
                dispatchJson.put("result.recruit.errorCode", "PP_E2003");
                
                dispatchJson.put("result.recruit.errorCode", "PP_E2003");
                dispatchJson.put("result.detail.errorObject.errorCode", "PP_E2003");
                dispatchJson.put("result.detail.errorObject.errorLevel", "ERROR");
                dispatchJson.put("result.detail.errorObject.errorType", "ERROR");
                dispatchJson.put("result.detail.errorObject.printBugReport", "false");
                dispatchJson.put("result.detail.errorObject.errorMessage", "ICカードエラー。取引を中断します。");
                dispatchJson.put("result.detail.resultCode", "1");
                dispatchJson.remove_key("result.detail.description");
            }
            else if(descrip->find("disableCardInsertedEMVKernel")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E2003");
                auto swpos = descrip->find("sw:");
                std::string sw;
                if(swpos != std::string::npos) {
                    sw.assign(descrip->begin() + swpos, descrip->end());
                    dispatchJson.put("result.recruit.detail", sw);
                }
                // 対外的にはdisableCardInsertedと同じ動作にするため、書き戻す
                dispatchJson.put("result.detail.description", "disableCardInserted");
            }
            else if(descrip->find("cardExist")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E2004");
            }
            else if(descrip->find("disableCardInserted")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E2005");
            }
            else if(descrip->find("NFCCardError")!= std::string::npos){
                auto descrip = dispatchJson.get_optional<std::string>("result.detail.type");
                auto code    = dispatchJson.get_optional<std::string>("result.detail.code");
                if(descrip){
                    dispatchJson.remove_key_if_exist("result.detail.type");
                    dispatchJson.put("result.detail.description", descrip.get());
                }
                dispatchJson.put("result.recruit.detail",descrip ? descrip.get():"NFC Card Error");
                dispatchJson.put("result.recruit.errorCode", code.get());
            }
            else if(descrip->find("declined")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_E4011");
            }
            else{
                dispatchJson.put("result.recruit.errorCode", "undefined");
            }
        }
        else if(where == "network"){
            auto code = dispatchJson.get<int>("result.detail.code");
            auto networkErrorCode = "N_E" + std::to_string(code);
            dispatchJson.put("result.recruit.errorCode", networkErrorCode);
            
            auto descrip = dispatchJson.get_optional<std::string>("result.detail.description");
            if(descrip){
                dispatchJson.put("result.recruit.detail", *descrip);
            }
            else{
                dispatchJson.put("result.recruit.detail", nullptr);
            }
        }
        else if(where == "hps"){
            auto errorCode = dispatchJson.get<std::string>("result.detail.errorObject.errorCode");
            std::string hpsErrorCode("");
            hpsErrorCode.append(errorCode.c_str());
            dispatchJson.put("result.recruit.errorCode", hpsErrorCode);
            dispatchJson.put("result.recruit.detail", dispatchJson.get<std::string>("result.detail.errorObject.errorMessage"));
        }
        else if(where == "internal"){
            auto descrip = dispatchJson.get_optional<std::string>("result.detail.description");
            LogManager_AddInformationF("result.detail.description:\n%s",*descrip);
            dispatchJson.put("result.recruit.detail", *descrip);
            
            if(descrip->find("CardReader connection failure")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_W0003");
                dispatchJson.put("result.detail.errorObject.errorCode", "PP_W0003");
                dispatchJson.put("result.detail.errorObject.errorLevel", "WARN");
                dispatchJson.put("result.detail.errorObject.errorType", "WARN");
                dispatchJson.put("result.detail.errorObject.errorMessage", *descrip);
                dispatchJson.put("result.detail.resultCode", "1");
                dispatchJson.remove_key("result.detail.description");
            }
            else if(descrip->find("No Refund SLIPNO")!= std::string::npos){
                dispatchJson.put("result.recruit.errorCode", "PP_W0004");
                dispatchJson.put("result.detail.errorObject.errorCode", "PP_W0004");
                dispatchJson.put("result.detail.errorObject.errorLevel", "WARN");
                dispatchJson.put("result.detail.errorObject.errorType", "WARN");
                dispatchJson.put("result.detail.errorObject.errorMessage", *descrip);
                dispatchJson.put("result.detail.resultCode", "1");
                dispatchJson.remove_key("result.detail.description");
            }
            else{
                dispatchJson.put("result.recruit.errorCode", "PP_E9999");
            }
        }
        else{
            auto errorCode = dispatchJson.get_optional<std::string>("result.detail.errorCode");
            if(errorCode){
                dispatchJson.put("result.recruit.errorCode", *errorCode);
            }
            else{
                dispatchJson.put("result.recruit.errorCode", nullptr);
            }
            auto descrip = dispatchJson.get_optional<std::string>("result.detail.description");
            if(descrip){
                dispatchJson.put("result.recruit.detail", *descrip);
            }
            else{
                dispatchJson.put("result.recruit.detail", nullptr);
            }
        }
    }
}

//
//  Json.h
//  ppsdk
//
//  Created by tel on 2017/07/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_Json__)
#define __h_Json__

#include <map>
#include <vector>
#include <list>
#include <sstream>
#include <cctype>
#include <algorithm>
#include <typeinfo>
#include <utility>
#include <iomanip>
#include <typeindex>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex/pending/unicode_iterator.hpp>

#define ENABLE_NON_CONST_GETTER

template <typename CHAR_TYPE> class basic_json_t
{
public:
    using char_t        = CHAR_TYPE;
    using self_t        = basic_json_t<char_t>;
    using string_t      = std::basic_string<char_t>;
    using sstream_t     = std::basic_stringstream<char_t>;
    using istream_t     = std::basic_istream<char_t>;
    using ostream_t     = std::basic_ostream<char_t>;
    using array_t       = std::vector<self_t>;
    using map_t         = std::map<string_t,self_t>;
    
private:
    class placeholder_t
    {
    private:
    protected:
    public:
        placeholder_t() = default;
        virtual ~placeholder_t() = default;
        virtual const std::type_info& type() const = 0;
        
        template <typename T> T& get()
        {
            auto _this = dynamic_cast< tplaceholder_t<T>* >(this);
            return _this->get();
        }
        
        template <typename T> const T& get() const
        {
            auto _this = dynamic_cast< const tplaceholder_t<T>* >(this);
            return _this->get();
        }
    };
    
    template<typename T> class tplaceholder_t : public placeholder_t
    {
    private:
        T       m_value;
    protected:
    public:
        tplaceholder_t() = default;
        tplaceholder_t(const T& src) : m_value(src) {}
        tplaceholder_t(T&& src) : m_value(src) {}
        virtual const std::type_info& type() const { return typeid(T); }
        
        T&          get()               { return m_value; }
        const T&    get() const         { return m_value; }
        void        set(const T& value) { m_value = value; }
        void        set(T&& value)      { m_value = value; }
        
    };
    
    boost::shared_ptr<placeholder_t> m_value;
    
    class exception_t : public std::exception
    {
    private:
        const std::string   m_what;
        static std::string generateWhat(const std::string& what, const char* src, int line, const char* func)
        {
            return (boost::format("%s\nat %s (%d) -> %s") % what % src % line % func).str();
        }
        
    protected:
    public:
        exception_t(const std::string& what, const char* s, int l, const char* f) : m_what(generateWhat(what, s, l, f)) {}
        exception_t(const boost::format& fmt, const char* s, int l, const char* f) : m_what(generateWhat(fmt.str(), s, l, f)) {}
        virtual ~exception_t() = default;
        virtual const char* what() const noexcept { return m_what.c_str(); }
    };
    #define __throw_basic_json_exception__(s) throw exception_t((s), __FILE__,__LINE__,__FUNCTION__)
    
    
    static std::string u8(const std::string& src)
    {
        return src;
    }

    static std::string u8(const std::wstring& src)
    {
        auto it_begin   = boost::u32_to_u8_iterator<std::wstring::const_iterator>(std::begin(src));
        auto it_end     = boost::u32_to_u8_iterator<std::wstring::const_iterator>(std::end(src));
        return std::string(it_begin, it_end);
    }
    
    bool unescape(istream_t& src, string_t& dst) const
    {
        char_t c;
        if(!src.get(c)) return false;
        bool bSuccess = false;
        bool bUnicode = false;
        switch(c){
            case char_t('"'):
            case char_t('\\'):
            case char_t('/'):
            {
                dst += c;
                bSuccess = true;
                break;
            }
            case char_t('b'):
            {
                dst += char_t('\b');
                bSuccess = true;
                break;
            }
            case char_t('f'):
            {
                dst += char_t('\f');
                bSuccess = true;
                break;
            }
            case char_t('n'):
            {
                dst += char_t('\n');
                bSuccess = true;
                break;
            }
            case char_t('r'):
            {
                dst += char_t('\r');
                bSuccess = true;
                break;
            }
            case char_t('t'):
            {
                dst += char_t('\t');
                bSuccess = true;
                break;
            }
            case char_t('u'):
            {
                bUnicode = true;
                break;
            }
        }
        if(bSuccess) return true;
        if(!bUnicode) return false;
        
        uint16_t    unicode = 0;
        for(auto i = 0; i < 4; i++){
            if(!src.get(c)) return false;
            unicode <<= 4;
            switch(c){
                case char_t('0'): { unicode |= 0x0; break; }
                case char_t('1'): { unicode |= 0x1; break; }
                case char_t('2'): { unicode |= 0x2; break; }
                case char_t('3'): { unicode |= 0x3; break; }
                case char_t('4'): { unicode |= 0x4; break; }
                case char_t('5'): { unicode |= 0x5; break; }
                case char_t('6'): { unicode |= 0x6; break; }
                case char_t('7'): { unicode |= 0x7; break; }
                case char_t('8'): { unicode |= 0x8; break; }
                case char_t('9'): { unicode |= 0x9; break; }
                case char_t('a'):
                case char_t('A'): { unicode |= 0xA; break; }
                case char_t('b'):
                case char_t('B'): { unicode |= 0xB; break; }
                case char_t('c'):
                case char_t('C'): { unicode |= 0xC; break; }
                case char_t('d'):
                case char_t('D'): { unicode |= 0xD; break; }
                case char_t('e'):
                case char_t('E'): { unicode |= 0xE; break; }
                case char_t('f'):
                case char_t('F'): { unicode |= 0xF; break; }
                default: return false;
            }
        }
        
        if(sizeof(char_t) == 1){
            if(unicode <= 0x007F){
                dst += char_t(unicode);
            }
            else if(unicode <= 0x07FF){
                dst += char_t(0b11000000 + ((unicode >>  6) & 0b00011111));
                dst += char_t(0b10000000 + ( unicode        & 0b00111111));
            }
            else{
                dst += char_t(0b11100000 + ((unicode >> 12) & 0b00001111));
                dst += char_t(0b10000000 + ((unicode >>  6) & 0b00111111));
                dst += char_t(0b10000000 + ( unicode        & 0b00111111));
            }
        }
        else if(sizeof(char_t) == 2){
            dst += char_t(unicode);
        }
        return true;
    }
    
    string_t escape(const string_t& src) const
    {
        sstream_t ss;
        for(auto c : src){
            switch(c){
                case char_t('"' ): { ss << char_t('\\') << char_t('"');  break; }
                case char_t('\b'): { ss << char_t('\\') << char_t('b');  break; }
                case char_t('\f'): { ss << char_t('\\') << char_t('f');  break; }
                case char_t('\n'): { ss << char_t('\\') << char_t('n');  break; }
                case char_t('\r'): { ss << char_t('\\') << char_t('r');  break; }
                case char_t('\t'): { ss << char_t('\\') << char_t('t');  break; }
                case char_t('\\'): { ss << char_t('\\') << char_t('\\'); break; }
                case char_t('/' ): { ss << char_t('\\') << char_t('/');  break; }
                default:
                {
                    if((0x00 <= c) && (c <= 0x1F)){
                        ss << char_t('\\') << char_t('u') << std::hex << std::setw(4) << std::setfill(char_t('0')) << static_cast<int>(c);
                    }
                    else{
                        ss << c;
                    }
                }
            }
        }
        return ss.str();
    }
    
    
    void serialize_map(ostream_t& os, bool bWithIndent, int indent) const
    {
        bool bFirst = true;
        auto&& map = get<map_t>();
        for(auto it: map){
            if(bFirst){
                bFirst = false;
            }
            else{
                os << char_t(',');
                if(bWithIndent) os << std::endl;
            }
            
            if(bWithIndent){
                os << string_t(indent, char_t(' '));
            }
            
            os  << '"'
            << it.first
            << '"'
            << ':';
            it.second.serialize(os, bWithIndent, indent);
        }
    }
    
    void serialize_array(ostream_t& os, bool bWithIndent, int indent) const
    {
        bool bFirst = true;
        auto&& arr = get<array_t>();
        for(auto it: arr){
            if(bFirst){
                bFirst = false;
            }
            else{
                os << char_t(',');
                if(bWithIndent){
                    os << std::endl;
                }
            }
            if(bWithIndent){
                os << string_t(indent, char_t(' '));
            }
            it.serialize(os, bWithIndent, indent);
        }
    }
    
    bool deserialize_map(istream_t& is, map_t& map) const
    {
        enum class mode
        {
            find_map_enter,
            find_key_or_map_leave,
            proceed_key,
            find_colon,
            find_value,
            proceed_value,
            proceed_value_string,
            proceed_value_boolean,
            proceed_value_integer,
            proceed_value_float,
            proceed_value_null,
            find_comma_or_map_leave,
            find_key
        };
        
        mode mode = mode::find_map_enter;
        string_t    key;
        string_t    svalue;
        char_t      c;
        while(is.get(c)){
            switch(mode){
                case mode::find_map_enter:
                {
                    if(c == char_t('{')){
                        mode = mode::find_key_or_map_leave;
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::find_key_or_map_leave:
                {
                    if(c == char_t('"')){
                        mode = mode::proceed_key;
                    }
                    else if(c == char_t('}')){
                        return true;     /* load 終了 */
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::find_comma_or_map_leave:
                {
                    if(c == char_t(',')){
                        mode = mode::find_key;
                    }
                    else if(c == char_t('}')){
                        return true;     /* load 終了 */
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                    
                }
                    
                case mode::find_key:
                {
                    if(c == char_t('"')){
                        mode = mode::proceed_key;
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::proceed_key:
                {
                    if(c == char_t('"')){
                        mode = mode::find_colon;
                    }
                    else{
                        key += c;
                    }
                    break;
                }
                    
                case mode::find_colon:
                {
                    if(c == char_t(':')){
                        mode = mode::find_value;
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::find_value:
                {
                    if(!std::isspace(c)){
                        is.unget();
                        mode = mode::proceed_value;
                    }
                    break;
                }
                    
                case mode::proceed_value:
                {
                    switch(c){
                        case char_t('['):
                        {
                            is.unget();
                            auto sub = self_t();
                            sub.deserialize(is);
                            map[key] = std::move(sub);
                            key.clear();
                            svalue.clear();
                            mode = mode::find_comma_or_map_leave;
                            break;
                        }
                        case char_t('{'):
                        {
                            is.unget();
                            auto sub = self_t();
                            sub.deserialize(is);
                            map[key] = std::move(sub);
                            key.clear();
                            svalue.clear();
                            mode = mode::find_comma_or_map_leave;
                            break;
                        }
                            
                        case char_t('"'):   /* strinng */
                        {
                            mode = mode::proceed_value_string;
                            break;
                        }
                        case char_t('t'):   /* true */
                        case char_t('T'):   /* TRUE */
                        case char_t('f'):   /* false */
                        case char_t('F'):   /* FALSE */
                        {
                            is.unget();
                            mode = mode::proceed_value_boolean;
                            break;
                        }
                            
                        case char_t('n'):   /* null */
                        case char_t('N'):   /* NULL */
                        {
                            is.unget();
                            mode = mode::proceed_value_null;
                            break;
                        }
                            
                        case char_t('-'):
                        case char_t('0'):
                        case char_t('1'):
                        case char_t('2'):
                        case char_t('3'):
                        case char_t('4'):
                        case char_t('5'):
                        case char_t('6'):
                        case char_t('7'):
                        case char_t('8'):
                        case char_t('9'):
                        case char_t('.'):
                        {
                            is.unget();
                            mode = mode::proceed_value_integer;
                            break;
                        }
                            
                        default:
                        {
                            return false;   /* error */
                        }
                    }
                    break;
                }
                    
                case mode::proceed_value_string:
                {
                    if(c == char_t('"')){
                        map[key] = std::move(svalue);
                        key.clear();
                        svalue.clear();
                        mode = mode::find_comma_or_map_leave;
                    }
                    else if(c == char_t('\\')){
                        if(!unescape(is,svalue)){
                            return false;
                        }
                    }
                    else{
                        svalue += c;
                    }
                    break;
                }
                    
                case mode::proceed_value_boolean:
                {
                    constexpr char_t _true[]     = {'t','r','u','e',0};
                    constexpr char_t _false[]    = {'f','a','l','s','e',0};
                    
                    if(!std::isalpha(c)){
                        is.unget();
                        if(std::equal(svalue.begin(), svalue.end(), _true)){
                            map[key] = true;
                            key.clear();
                            svalue.clear();
                        }
                        else if(std::equal(svalue.begin(), svalue.end(), _false)){
                            map[key] = false;
                            key.clear();
                            svalue.clear();
                        }
                        else{
                            return false;   /* error */
                        }
                        mode = mode::find_comma_or_map_leave;
                    }
                    else{
                        svalue += std::tolower(c);
                    }
                    break;
                }
                case mode::proceed_value_null:
                {
                    constexpr char_t _null[]     = {'n','u','l','l',0};
                    
                    if(!std::isalpha(c)){
                        is.unget();
                        if(std::equal(svalue.begin(), svalue.end(), _null)){
                            map[key] = nullptr;
                            key.clear();
                            svalue.clear();
                        }
                        else{
                            return false;   /* error */
                        }
                        mode = mode::find_comma_or_map_leave;
                    }
                    else{
                        svalue += std::tolower(c);
                    }
                    break;
                }
                    
                case mode::proceed_value_integer:
                case mode::proceed_value_float:
                {
                    if(c == char_t('.')){
                        if(mode == mode::proceed_value_float){
                            return false;   /* error */
                        }
                        mode = mode::proceed_value_float;
                        svalue += c;
                    }
                    else if(c == char_t('-')){
                        svalue += c;
                    }
                    else if(!std::isdigit(c)){
                        is.unget();
                        if(mode == mode::proceed_value_integer){
                            map[key] = _atoi(svalue);
                            key.clear();
                            svalue.clear();
                        }
                        else{
                            map[key] = _atof(svalue);
                            key.clear();
                            svalue.clear();
                        }
                        mode = mode::find_comma_or_map_leave;
                    }
                    else{
                        svalue += c;
                    }
                    break;
                }
            }
        }
        return false;
    }
    
    int _atoi(const string_t& s) const
    {
        return boost::lexical_cast<int>(s);
    }
    
    double _atof(const string_t& s) const
    {
        return boost::lexical_cast<double>(s);
    }
    
    bool deserialize_array(istream_t& is, array_t& arr) const
    {
        enum class mode
        {
            find_array_enter,
            find_value_or_array_leave,
            proceed_value,
            proceed_value_string,
            proceed_value_boolean,
            proceed_value_integer,
            proceed_value_float,
            proceed_value_null,
            find_commna_or_array_leave,
            find_value
        };
        
        mode        mode = mode::find_array_enter;
        char_t      c;
        string_t    svalue;
        
        while(is.get(c)){
            switch(mode){
                case mode::find_array_enter:
                {
                    if(c == char_t('[')){
                        mode = mode::find_value_or_array_leave;
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::find_value_or_array_leave:
                {
                    if(c == char_t(']')){
                        return true;    /* 正常終了 */
                    }
                    else if(!std::isspace(c)){
                        is.unget();
                        mode = mode::proceed_value;
                    }
                    
                    break;
                }
                    
                case mode::find_commna_or_array_leave:
                {
                    if(c == char_t(']')){
                        return true;    /* 正常終了 */
                    }
                    else if(c == char_t(',')){
                        mode = mode::find_value;
                    }
                    else if(!std::isspace(c)){
                        return false;   /* error */
                    }
                    break;
                }
                    
                case mode::find_value:
                {
                    if(!std::isspace(c)){
                        is.unget();
                        mode = mode::proceed_value;
                    }
                    break;
                }
                    
                case mode::proceed_value:
                {
                    switch(c){
                        case char_t('['):
                        {
                            is.unget();
                            auto sub = self_t();
                            sub.deserialize(is);
                            arr.push_back(std::move(sub));
                            svalue.clear();
                            mode = mode::find_commna_or_array_leave;
                            break;
                        }
                        case char_t('{'):
                        {
                            is.unget();
                            auto sub = self_t();
                            sub.deserialize(is);
                            arr.push_back(std::move(sub));
                            svalue.clear();
                            mode = mode::find_commna_or_array_leave;
                            break;
                        }
                            
                        case char_t('"'):   /* strinng */
                        {
                            mode = mode::proceed_value_string;
                            break;
                        }
                        case char_t('t'):   /* true */
                        case char_t('T'):   /* TRUE */
                        case char_t('f'):   /* false */
                        case char_t('F'):   /* FALSE */
                        {
                            is.unget();
                            mode = mode::proceed_value_boolean;
                            break;
                        }
                            
                        case char_t('n'):   /* null */
                        case char_t('N'):   /* NULL */
                        {
                            is.unget();
                            mode = mode::proceed_value_null;
                            break;
                        }
                            
                        case char_t('-'):
                        case char_t('0'):
                        case char_t('1'):
                        case char_t('2'):
                        case char_t('3'):
                        case char_t('4'):
                        case char_t('5'):
                        case char_t('6'):
                        case char_t('7'):
                        case char_t('8'):
                        case char_t('9'):
                        case char_t('.'):
                        {
                            is.unget();
                            mode = mode::proceed_value_integer;
                            break;
                        }
                        default:
                        {
                            return false;   /* error */
                        }
                    }
                    break;
                }
                    
                case mode::proceed_value_string:
                {
                    if(c == char_t('"')){
                        arr.push_back(std::move(svalue));
                        svalue.clear();
                        mode = mode::find_commna_or_array_leave;
                    }
                    else if(c == char_t('\\')){
                        if(!unescape(is,svalue)){
                            return false;
                        }
                    }
                    else{
                        svalue += c;
                    }
                    break;
                }
                    
                case mode::proceed_value_boolean:
                {
                    constexpr char_t _true[]     = {'t','r','u','e',0};
                    constexpr char_t _false[]    = {'f','a','l','s','e',0};
                    
                    if(!std::isalpha(c)){
                        is.unget();
                        if(std::equal(svalue.begin(), svalue.end(), _true)){
                            arr.push_back(true);
                            svalue.clear();
                        }
                        else if(std::equal(svalue.begin(), svalue.end(), _false)){
                            arr.push_back(false);
                            svalue.clear();
                        }
                        else{
                            return false;   /* error */
                        }
                        mode = mode::find_commna_or_array_leave;
                    }
                    else{
                        svalue += std::tolower(c);
                    }
                    break;
                }
                case mode::proceed_value_null:
                {
                    constexpr char_t _null[]     = {'n','u','l','l',0};
                    
                    if(!std::isalpha(c)){
                        is.unget();
                        if(std::equal(svalue.begin(), svalue.end(), _null)){
                            arr.push_back(nullptr);
                            svalue.clear();
                        }
                        else{
                            return false;   /* error */
                        }
                        mode = mode::find_commna_or_array_leave;
                    }
                    else{
                        svalue += std::tolower(c);
                    }
                    break;
                }
                    
                case mode::proceed_value_integer:
                case mode::proceed_value_float:
                {
                    if(c == char_t('.')){
                        if(mode == mode::proceed_value_float){
                            return false;   /* error */
                        }
                        mode = mode::proceed_value_float;
                        svalue += c;
                    }
                    else if(c == char_t('-')){
                        svalue += c;
                    }
                    else if(!std::isdigit(c)){
                        is.unget();
                        if(mode == mode::proceed_value_integer){
                            arr.push_back(_atoi(svalue));
                            svalue.clear();
                        }
                        else{
                            arr.push_back(_atof(svalue));
                            svalue.clear();
                        }
                        mode = mode::find_commna_or_array_leave;
                    }
                    else{
                        svalue += c;
                    }
                    break;
                }
            }
        }
        return false;
    }
protected:
public:
    basic_json_t() = default;
    basic_json_t(const char_t*   src) : m_value(boost::make_shared<tplaceholder_t<string_t      >>(src)) {}
    basic_json_t(const string_t& src) : m_value(boost::make_shared<tplaceholder_t<string_t      >>(src)) {}
    basic_json_t(const array_t&  src) : m_value(boost::make_shared<tplaceholder_t<array_t       >>(src)) {}
    basic_json_t(const map_t&    src) : m_value(boost::make_shared<tplaceholder_t<map_t         >>(src)) {}
    basic_json_t(bool            src) : m_value(boost::make_shared<tplaceholder_t<bool          >>(src)) {}
    basic_json_t(int             src) : m_value(boost::make_shared<tplaceholder_t<int           >>(src)) {}
    basic_json_t(double          src) : m_value(boost::make_shared<tplaceholder_t<double        >>(src)) {}
    basic_json_t(std::nullptr_t  src) : m_value(boost::make_shared<tplaceholder_t<std::nullptr_t>>(src)) {}

    basic_json_t(string_t&&      src) : m_value(boost::make_shared<tplaceholder_t<string_t      >>(src)) {}
    basic_json_t(array_t&&       src) : m_value(boost::make_shared<tplaceholder_t<array_t       >>(src)) {}
    basic_json_t(map_t&&         src) : m_value(boost::make_shared<tplaceholder_t<map_t         >>(src)) {}

    virtual ~basic_json_t() = default;
    
    const std::type_info& type() const
    {
        if(!m_value){
            __throw_basic_json_exception__("value is null");
        }
        return m_value->type();
    }
    
    std::string type_name(const std::type_info& tinfo) const
    {
        const auto tbl = std::map<std::type_index,std::string>
        {
            {typeid(int)            , "int"},
            {typeid(double)         , "double"},
            {typeid(bool)           , "bool"},
            {typeid(std::nullptr_t) , "nullptr_t"},
            {typeid(string_t)       , "string_t"},
            {typeid(array_t)        , "array_t"},
            {typeid(map_t)          , "map_t"}
        };
        auto it = tbl.find(tinfo);
        if(it == tbl.end()){
            return tinfo.name();
        }
        return it->second;
    }
    
    template <typename T> std::string type_name() const
    {
        return type_name(typeid(T));
    }
    
    std::string type_name() const
    {
        return type_name(type());
    }
    
    bool empty() const
    {
        return m_value.get() == nullptr;
    }
    
    bool is_null() const
    {
        return type() == typeid(std::nullptr_t);
    }
    
    bool is_null_or_empty() const
    {
        return empty() || is_null();
    }
    
    string_t str(bool bWithIndent = false) const
    {
        sstream_t ss;
        serialize(ss, bWithIndent);
        return ss.str();
    }
    
    static self_t from_str(const string_t& s)
    {
        std::stringstream ss;
        ss << s;
        self_t json;
        json.deserialize(ss);
        return json;
    }
    
    template<typename T> static self_t from_array(const T& array)
    {
        self_t result;
        self_t::array_t jarr;
        for(auto item: array){
            jarr.push_back(item);
        }
        return self_t(std::move(jarr));
    }
    
    template <typename T> std::vector<T> get_array() const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("array is empty."));
        }
        if(type() != typeid(array_t)){
            __throw_basic_json_exception__(boost::format("not array."));
        }
        std::vector<T> arr;
        for (auto item : get<self_t::array_t>()){
            arr.push_back(item.template get<T>());
        }
        return arr;
    }

    template <typename T> boost::optional<std::vector<T>> get_array_optional() const
    {
        if(empty()){
            return boost::none;
        }
        if(type() != typeid(array_t)){
            return boost::none;
        }
        return get_array();
    }

    self_t& operator = (const char_t*   src) { m_value = boost::make_shared<tplaceholder_t<string_t      >>(src); return *this; }
    self_t& operator = (const string_t& src) { m_value = boost::make_shared<tplaceholder_t<string_t      >>(src); return *this; }
    self_t& operator = (const array_t&  src) { m_value = boost::make_shared<tplaceholder_t<array_t       >>(src); return *this; }
    self_t& operator = (const map_t&    src) { m_value = boost::make_shared<tplaceholder_t<map_t         >>(src); return *this; }
    self_t& operator = (bool            src) { m_value = boost::make_shared<tplaceholder_t<bool          >>(src); return *this; }
    self_t& operator = (int             src) { m_value = boost::make_shared<tplaceholder_t<int           >>(src); return *this; }
    self_t& operator = (double          src) { m_value = boost::make_shared<tplaceholder_t<double        >>(src); return *this; }
    self_t& operator = (std::nullptr_t  src) { m_value = boost::make_shared<tplaceholder_t<std::nullptr_t>>(src); return *this; }

    self_t& operator = (string_t&&      src) { m_value = boost::make_shared<tplaceholder_t<string_t      >>(src); return *this; }
    self_t& operator = (array_t&&       src) { m_value = boost::make_shared<tplaceholder_t<array_t       >>(src); return *this; }
    self_t& operator = (map_t&&         src) { m_value = boost::make_shared<tplaceholder_t<map_t         >>(src); return *this; }

#if defined(ENABLE_NON_CONST_GETTER)
    
    self_t& get(const string_t& path)
    {
        const auto idx = path.find(char_t('.'));
        const auto key = path.substr(0, idx);
        
        if(empty()){
            __throw_basic_json_exception__(boost::format("'%s' is empty.") % u8(path));
        }

        if(type() != typeid(map_t)){
            __throw_basic_json_exception__(boost::format("'%s' type is not map_t. (%s)") % u8(path) % type_name());
        }
        
        auto&& map = get<map_t>();
        auto it_map = map.find(key);
        if(it_map == map.end()){
            __throw_basic_json_exception__(boost::format("key '%s' not found.") % u8(key));
        }
        
        if(idx == string_t::npos){
            return it_map->second;
        }
        else{
            return it_map->second.get(path.substr(idx + 1));
        }
    }
    
    self_t& operator [] (const string_t& key)
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("map is empty."));
        }
        if(type() != typeid(map_t)){
            __throw_basic_json_exception__(boost::format("'%s' is not map.") % u8(key));
        }
        auto&& map = get<map_t>();
        return map[key];
    }
    
    self_t& operator [] (const char_t* key)
    {
        return operator [] (string_t(key));
    }
    
    
    self_t& operator [] (int index)
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("array is empty."));
        }
        if(type() != typeid(array_t)){
            __throw_basic_json_exception__(boost::format("not array."));
        }
        auto&& arr = get<array_t>();
        return arr[index];
    }
    
    self_t& operator / (const string_t& key)
    {
        return operator [] (key);
    }
    
    self_t& operator / (const char_t* key)
    {
        return operator [] (key);
    }
    
    self_t& operator / (int index)
    {
        return operator [] (index);
    }
    
#endif
    
    const self_t& get(const string_t& path) const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("'%s' is empty.") % u8(path));
        }

        const auto idx = path.find(char_t('.'));
        const auto key = path.substr(0, idx);
        
        if(type() != typeid(map_t)){
            __throw_basic_json_exception__(boost::format("'%s' type is not map_t. (%s)") % u8(key) % type_name());
        }
        
        /*
         参照返却関数に対してautoで受けるとコピーコンストラクタが呼び出されてしまう。
         つまり別インスタンスとなり、且つ、スタック上の一時変数なので、スコープ（または関数）を抜けるとオブジェクトは破棄される。
         その参照を受ける関数は、スタック状況によっては落ちることとなる。
         auto 変数の定義では「後置の返却型を利用する」（§6.3.1）はずなのだが、コンパイラの問題かも知れない。
         そこで、auto&& のUniversal Referenceを使用して、右辺値参照or左辺値参照で値を受け、参照を維持する修正を行った。
         関数返却値が const& なので、結果、&& const & となるが、重複している場合には左辺値参照が採用される（参照崩壊につき§7.7.3）。
         */
        auto&& map = get<map_t>();
        const auto it_map = map.find(key);
        if(it_map == map.end()){
            __throw_basic_json_exception__(boost::format("key '%s' not found.") % u8(key));
        }
        
        if(idx == string_t::npos){
            return it_map->second;
        }
        else{
            return it_map->second.get(path.substr(idx + 1));
        }
    }
    
    template<typename T> const T& get() const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("value is empty."));
        }
        if(typeid(T) != type()){
            __throw_basic_json_exception__(boost::format("cannot cast. (%s) -> (%s)") % type_name() % type_name<T>());
        }
        return m_value->template get<T>();
    }
    
    template<typename T> T& get()
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("value is empty."));
        }
        if(typeid(T) != type()){
            __throw_basic_json_exception__(boost::format("cannot cast. (%s) -> (%s)") % type_name() % type_name<T>());
        }
        return m_value->template get<T>();
    }
    
    template <typename T> const T& get(const string_t& path) const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("'%s' is empty.") % u8(path));
        }
        auto&& value = get(path);
        if(value.type() != typeid(T)){
            __throw_basic_json_exception__(boost::format("'%s' type is not %s. (%s)") % u8(path) % type_name<T>() % type_name());
        }
        return value.template get<T>();
    }
    
    template <typename T> T& get(const string_t& path)
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("'%s' is empty.") % u8(path));
        }
        auto&& value = get(path);
        if(value.type() != typeid(T)){
            __throw_basic_json_exception__(boost::format("'%s' type is not %s. (%s)") % u8(path) % type_name<T>() % type_name());
        }
        return value.template get<T>();
    }
    
    const self_t& operator [] (const string_t& key) const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("map['%s'] is empty.") % u8(key));
        }
        auto&& map = get<map_t>();
        return map[key];
    }
    
    const self_t& operator [] (const char_t* key) const
    {
        return operator [] (string_t(key));
    }
    
    
    const self_t& operator [] (int index) const
    {
        if(empty()){
            __throw_basic_json_exception__(boost::format("array[%d] is empty.") % index);
        }
        if(type() != typeid(array_t)){
            __throw_basic_json_exception__(boost::format("not array"));
        }
        auto&& arr = get<array_t>();
        return arr[index];
    }
    
    const self_t& operator / (const string_t& key) const
    {
        return operator [] (key);
    }
    
    
    const self_t& operator / (const char_t* key) const
    {
        return operator [] (key);
    }
    
    const self_t& operator / (int index) const
    {
        return operator [] (index);
    }
    
    
    /* boost::optional<>の特性上、内包されているオブジェクトはオリジナルではなくコピーなので変更しても無意がない */
    /* self_tの場合はshared_ptrを内包しているのが、oparator = () で変更を行っても、オリジナルself_tと別のインスタンスをshared_ptrで管理するだけで、 */
    /* 前shared_ptrの所有権は放棄することになる。（よくよく考えればメモリ破壊の原因となる） */
    /* 従って、get_optional() は全て const とし、変更を許さないこととした */
    boost::optional<self_t> get_optional(const string_t& path) const
    {
        const auto idx = path.find(char_t('.'));
        const auto key = path.substr(0, idx);
        
        if(empty()){
            return boost::optional<self_t>();
        }
        else if(type() != typeid(map_t)){
            return boost::optional<self_t>();
        }
        
        const auto map = get<map_t>();
        const auto it_map = map.find(key);
        if(it_map == map.end()){
            return boost::optional<self_t>();
        }
        
        if(idx == string_t::npos){
            return boost::optional<self_t>(it_map->second);
        }
        else{
            return it_map->second.get_optional(path.substr(idx + 1));
        }
    }
    
    template <typename T> boost::optional<T> get_optional(const string_t& path) const
    {
        auto opt = get_optional(path);
        if(!opt){
            return boost::optional<T>();
        }
        else if(opt->type() == typeid(std::nullptr_t)){
            return boost::optional<T>();
        }
        return boost::optional<T>(opt->template get<T>());
    }
    
    
    /* nodeが存在しない場合には作成する */
    template <typename T> void put(const string_t& path, const T& value)
    {
        const auto idx = path.find(char_t('.'));
        const auto key = path.substr(0, idx);
        
        if(empty()){
            (*this) = map_t();
        }
        else if(type() != typeid(map_t)){
            (*this) = map_t();
        }
        
        auto&& map = get<map_t>();
        const auto it_map = map.find(key);
        if(it_map == map.end()){
            if(idx == string_t::npos){
                map[key] = value;
            }
            else{
                self_t subnode = map_t();
                subnode.put(path.substr(idx + 1), value);
                map[key] = subnode;
            }
        }
        else{
            if(idx == string_t::npos){
                map.erase(it_map);     /* キーが重複している場合、上書きされない（§31.4.3.1） */
                map[key] = value;
            }
            else{
                map[key].put(path.substr(idx + 1), value);
            }
        }
    }
    
    void remove_key(const string_t& path)
    {
        const auto idx = path.find(char_t('.'));
        const auto key = path.substr(0, idx);
        
        if(empty()){
            __throw_basic_json_exception__(boost::format("'%s' is empty.") % u8(path));
        }
        
        if(type() != typeid(map_t)){
            __throw_basic_json_exception__(boost::format("'%s' type is not map_t. (%s)") % u8(path) % type_name());
        }
        
        auto&& map = get<map_t>();
        auto it_map = map.find(key);
        if(it_map == map.end()){
            __throw_basic_json_exception__(boost::format("key '%s' not found.") % u8(key));
        }
        
        if(idx == string_t::npos){
            map.erase(it_map);
        }
        else{
            it_map->second.remove_key(path.substr(idx + 1));
        }
    }
    
    void remove_key_if_exist(const string_t& path)
    {
        if(get_optional(path)){
            remove_key(path);
        }
    }
    
    void serialize(ostream_t& os, bool bWithIndent, int indent = 0) const
    {
        if(empty()){
            /* nothing to do */
            constexpr char_t _empty[]     = {'{','}',0};
            os << _empty;
        }
        else if(type() == typeid(bool)){
            constexpr char_t _true[]     = {'t','r','u','e',0};
            constexpr char_t _false[]    = {'f','a','l','s','e',0};
            os << (get<bool>() ? _true : _false);
        }
        else if(type() == typeid(string_t)){
            os << '"' << escape(get<string_t>()) << '"';
        }
        else if(type() == typeid(array_t)){
            os << '[';
            
            if(bWithIndent) os << std::endl;
            serialize_array(os, bWithIndent, indent + 1);
            if(bWithIndent) os << std::endl;
            
            if(bWithIndent){
                os << string_t(indent, char_t(' '));
            }
            os << ']';
        }
        else if(type() == typeid(map_t)){
            os << '{';
            
            if(bWithIndent) os << std::endl;
            serialize_map(os, bWithIndent, indent + 1);
            if(bWithIndent) os << std::endl;
            
            if(bWithIndent){
                os << string_t(indent, char_t(' '));
            }
            os << '}';
        }
        else if(type() == typeid(std::nullptr_t)){
            constexpr char_t _null[]     = {'n','u','l','l',0};
            os << _null;
        }
        else if(type() == typeid(int)){
            os << get<int>();
        }
        else if(type() == typeid(double)){
            os << get<double>();
        }
        else{
            __throw_basic_json_exception__(boost::format("invalid value. (%s)") % type_name());
        }
    }
    
    bool deserialize(const string_t& src)
    {
        sstream_t ss;
        ss.str(src);
        return deserialize(ss);
    }
    
    bool deserialize(istream_t& is)
    {
        char_t  c;
        while(is.get(c)){
            if(c == char_t('{')){
                is.unget();
                auto map = map_t();
                if(!deserialize_map(is, map)){
                    return false; /* error */
                }
                (*this) = std::move(map);
                return true;    /* 正常終了 */
                break;
            }
            else if(c == char_t('[')){
                is.unget();
                auto arr = array_t();
                if(!deserialize_array(is, arr)){
                    return false; /* error */
                }
                (*this) = std::move(arr);
                return true;    /* 正常終了 */
            }
            else if(!std::isspace(c)){
                return false;   /* error */
            }
        }
        return true;
    }
    
    self_t clone() const
    {
        sstream_t ss;
        serialize(ss, false);
        self_t json;
        json.deserialize(ss);
        return json;
    }
    
    void clear()
    {
        m_value.reset();
    }
    
    class readonly
    {
    friend class basic_json_t<CHAR_TYPE>;
    private:
        const self_t json;

    public:
        readonly(const self_t& j) : json(j) {}  /* self_t からの暗黙の型変換　*/
        readonly() : json(self_t()) {}

        const std::type_info& type() const                      { return json.type(); }
        std::string type_name(const std::type_info& tinfo) const{ return json.type_name(tinfo); }
        template <typename T> std::string type_name() const     { return json.type_name<T>(); }
        
        std::string type_name() const                           { return json.type_name(); }
        bool empty() const                                      { return json.empty(); }
        bool is_null() const                                    { return json.is_null(); }
        bool is_null_or_empty() const                           { return json.is_null_or_empty(); }
        string_t str() const                                    { return json.str(); }
        template <typename T> std::vector<T> get_array() const  { return json.get_array<T>(); }
        template <typename T> boost::optional<std::vector<T>> get_array_optional() const { return json.get_array_optional<T>(); }

        const self_t& get(const string_t& path) const           { return json.get(path); }
        template<typename T> const T& get() const               { return json.get<T>(); }
        template <typename T> const T& get(const string_t& path) const
                                                                { return json.get<T>(path); }
        const self_t& operator [] (const string_t& key) const   { return json.operator [] (key); }
        const self_t& operator [] (const char_t* key) const     { return json.operator [] (key); }
        const self_t& operator [] (int index) const             { return json.operator [] (index); }
        const self_t& operator / (const string_t& key) const    { return json.operator / (key); }
        const self_t& operator / (const char_t* key) const      { return json.operator / (key); }
        const self_t& operator / (int index) const              { return json.operator / (index); }
        boost::optional<self_t> get_optional(const string_t& path) const
                                                                { return json.get_optional(path); }
        template <typename T> boost::optional<T> get_optional(const string_t& path) const
                                                                { return json.get_optional<T>(path); }
        void serialize(ostream_t& os, bool bWithIndent, int indent = 0) const
                                                                { json.serialize(os, bWithIndent, indent); }
        self_t clone() const                                    { return json.clone(); }
    };
    
    readonly as_readonly() const
    {
        return readonly(*this);
    }

    #undef __throw_basic_json_exception__
};


#endif /* !defined(__h_Json__) */

//
//  JMupsAccessSetting.h
//  ppsdk
//
//  Created by clementec on 2018/12/11.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JMupsAccessSetting__)
#define __h_JMupsAccessSetting__

#include "JMupsAccess.h"

class JMupsAccessSetting :
virtual public JMupsAccess
{
private:
    
protected:    
    /* アクティベーション */
    auto hpsAcivateAuth
    (
     const std::string& subscribeSequence,
     const std::string& activateID,
     const std::string& oneTimePassword
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsAcivateDownload
    (
     const std::string& subscribeSequence,
     const std::string& activateID,
     const std::string& oneTimePassword,
     const std::string& termPrimaryNo,
     const boost::filesystem::path& filepath
     ) -> pprx::observable<pprx::unit>;

    auto hpsAcivateComplete
    (
     const std::string& uniqueId
     ) -> pprx::observable<JMupsAccess::response_t>;

    /// カード会社一覧
    auto hpsPrintCardCope() -> pprx::observable<JMupsAccess::response_t>;
    
    /// 端末サービス設定情報
    auto hpsPrintService() -> pprx::observable<JMupsAccess::response_t>;
    
public:
    JMupsAccessSetting() = default;
    virtual ~JMupsAccessSetting() = default;
    
};

#endif /* !defined(__h_JMupsAccessSetting__) */

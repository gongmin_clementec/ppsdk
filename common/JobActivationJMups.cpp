//
//  JobActivationJMups.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobActivationJMups.h"
#include "PPConfig.h"

JobActivationJMups::JobActivationJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobActivationJMups::~JobActivationJMups()
{
    
}

/* virtual */
auto JobActivationJMups::getObservableSelf() -> pprx::observable<const_json_t>
{
    auto certfile = BaseSystem::instance().getDocumentDirectoryPath() / "cert.p12";

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        setUrlForActivation();

        if(BaseSystem::instance().certificateIsInstalled()){
            LogManager_AddInformation("cert is installed");
            return pprx::just(pprx::unit()).as_dynamic();
        }

        return callOnDispatchWithCommand("waitForActivationOptions")
        .flat_map([=](const_json_t json){
            auto subscribeSequence  = json.get<std::string>("subscribeSequence");
            auto activateID         = json.get<std::string>("activateID");
            auto oneTimePassword    = json.get<std::string>("oneTimePassword");
            auto termPrimaryNo      = BaseSystem::instance().getDeviceUuid();
            
            pprx::observable<pprx::unit> preprocess;
            if(!boost::filesystem::exists(certfile)){
                LogManager_AddInformation("cert.p12 file not exists");
                /* 証明書ファイルが存在しない場合、認証とダウンロードを行う */
                preprocess = hpsAcivateAuth(subscribeSequence, activateID, oneTimePassword)
                .on_error_resume_next([=](auto err){
                    auto bDebug = PPConfig::instance().getOptional<bool>("debug.bActivationSkipError");
                    if((!bDebug) || (!bDebug.get())){
                        std::rethrow_exception(err);
                    }
                    return pprx::just(JMupsAccess::response_t());
                })
                .flat_map([=](JMupsAccess::response_t){
                    LogManager_AddInformationF("termPrimaryNo : %s", termPrimaryNo);
                    return hpsAcivateDownload(subscribeSequence, activateID, oneTimePassword, termPrimaryNo, certfile);
                }).as_dynamic();
            }
            else{
                LogManager_AddInformation("cert.p12 file exists");
                /*
                    証明書ファイルが存在する場合、認証でエラーになるので、何もしない。
                    でも、証明書のインストールのためには oneTimePsswordが必要なので、
                    あえて、ユーザに入力させる必要がある。（subscribeSequenceとactivateIDの検証は行わない）
                 */
                preprocess = pprx::just(pprx::unit());
            }
            
            return preprocess
            .flat_map([=](pprx::unit){
                if(!BaseSystem::instance().certificateInstall(certfile, oneTimePassword)){
                    throw_error_internal("certificateInstall() failed");
                }
                return pprx::just(BaseSystem::instance().getSystemInformation());
            })
            .flat_map([=](const_json_t si){
                auto uniqueId = si.get<std::string>("UserName");
                return hpsAcivateComplete(uniqueId);
            }).as_dynamic()
            .map([=](JMupsAccess::response_t){
                return pprx::unit();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](pprx::unit){
        LogManager_AddInformation("success -> remove certfile");
        boost::filesystem::remove(certfile);    /* 成功しない限り certfile は削除しない */
        json_t j;
        j.put("status", "success");
        return pprx::just(j.as_readonly());
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError("error -> certificateUninstall()");
        BaseSystem::instance().certificateUninstall();
        return pprx::error<const_json_t>(err);
    }).as_dynamic();
}

/* virtual */
auto JobActivationJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

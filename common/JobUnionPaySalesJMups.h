//
//  JobUnionPaySalesJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobUnionPaySalesJMups__)
#define __h_JobUnionPaySalesJMups__

#include "PaymentJob.h"
#include "JMupsAccessUnionPay.h"
#include "JMupsAccessCoupon.h"
#include "JMupsEmvKernel.h"
#include "JMupsSlipPrinter.h"

class JobUnionPaySalesJMups :
    public PaymentJob,
    public JMupsAccessUnionPay,
    public JMupsAccessCoupon,
    public JMupsEmvKernel,
    public JMupsSlipPrinter
{
protected:
    class TradeInfoBuilderJMupsUnionPay : public TradeInfoBuilder
    {
    public:    
        virtual ~TradeInfoBuilderJMupsUnionPay() = default;
        
        virtual void addCupTradeDiv(attribute attr, boost::optional<std::string> value)
        {
            addInternal<std::string>("cupTradeDiv", attr, value);
        }
    };
    
    json_t m_tradeStartResponse;
    json_t m_cardStartData;
    json_t m_cardInfoData;
    json_t m_tradeData;
    json_t m_couponData;
    json_t m_tradeConfirmFinished; // 取引確認処理終了《通知》json
    json_t m_pinParam;
    prmUnionPay::doubleTrade    m_taggingSelector;
    
    bool   m_bICCardType;             // true:ICカード　false:磁気カード
    bool   m_bIsMSFallback;
    
    enum class UIBehaviorState
    {
        Bad,
        InProgress,
        Finished,
        Cancelled
    };
    
    pprx::subjects::behavior<UIBehaviorState> m_emvAmountPreInputUI;
    
protected:
    auto syncEmvAmountPreInputUI() -> pprx::observable<pprx::unit>;
    auto tradeStart() -> pprx::observable<pprx::unit>;
    auto processCardJob(const_json_t cardReaderOption) -> pprx::observable<pprx::unit>;
    auto processMagneticStripeOrContact(const_json_t data) -> pprx::observable<pprx::unit>;
    auto getCouponData() -> pprx::observable<const_json_t>;
    auto applyDispatchResponseParamForTradeInfo(const_json_t resp) -> pprx::observable<bool>;
    auto waitForTradeInfo(const std::string& strTradeOp="") -> pprx::observable<pprx::unit>;
    auto processSendDigiSign() -> pprx::observable<pprx::unit>;
    auto waitForDigiSign() -> pprx::observable<boost::optional<std::string>>;
    auto isNeedAquirerSlip(bool bNeedAquirer = true) -> pprx::observable<pprx::unit>;
    auto sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)  -> pprx::observable<pprx::unit>;

    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto proceedContact(const_json_t data)  -> pprx::observable<pprx::unit>;
    virtual void beginAsyncPreInputAmount();
    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual auto sendTradeConfirm(prmUnionPay::doubleTrade selector, boost::optional<std::string> authenticationNo)  -> pprx::observable<JMupsAccess::response_t>;
    virtual auto waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData, boost::optional<std::string> authenticationNo) -> pprx::observable<prmUnionPay::doubleTrade>;
    virtual auto sendTagging(prmUnionPay::doubleTrade selector) -> pprx::observable<JMupsAccess::response_t>;
    virtual auto processRemovingContact(bool bTradeIsDecline = false) -> pprx::observable<pprx::unit>;
    virtual auto beginAsyncRemovingContact() -> void;
    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;
    virtual auto addMediaDivToCardNo(const_json_t tradeData) -> json_t;
    virtual auto checkTradeFailureErrorCode(const_json_t tradeData, const std::string errorCode) -> bool;	
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;
    
    virtual auto emvOnCardPinMessage(const_json_t param)  -> pprx::observable<pprx::unit>;
    virtual auto emvOnCardInputPlainOfflinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardInputEncryptedOfflinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardInputEncryptedOnlinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardTxPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardTxEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardGetIfdSerialNo() -> pprx::observable<const_json_t>;
    virtual auto emvOnNetwork(const std::string& url, const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectApplication(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    virtual auto emvWaitInputPreAmount() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiAmount() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiInputKid() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiInputPayWay(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnTradeConfirmFinish(const_json_t emvTradeConfirm)  -> pprx::observable<pprx::unit>;
    virtual auto emvOnTrainingCardReaderDisplayData() -> pprx::observable<const_json_t>;
    virtual auto emvOnCardPinResultConfirm(const_json_t param)  -> pprx::observable<pprx::unit>;
    
public:
    JobUnionPaySalesJMups(const __secret& s);
    virtual ~JobUnionPaySalesJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobUnionPaySalesJMups__) */

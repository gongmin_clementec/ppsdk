//
//  AtomicObject.h
//  ppsdk
//
//  Created by tel on 2017/08/28.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_AtomicObjecct__)
#define __h_AtomicObjecct__

#include <boost/thread.hpp>
#include <boost/function.hpp>
#include <boost/core/noncopyable.hpp>

template <typename T> class AtomicObject : boost::noncopyable
{
private:
    T                               m_value;
    mutable boost::recursive_mutex  m_value_mtx;
    
protected:
    
public:
    AtomicObject() = default;
    AtomicObject(const T& value) : m_value(value) {}
    AtomicObject(T&& value) : m_value(value) {}
    virtual ~AtomicObject() = default;
    
    void operator = (const T& value)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        m_value = value;
    }

    void operator = (T&& value)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        m_value = value;
    }

    void referenceBlock(boost::function<void(const T&)> f) const
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        f(m_value);
    }

    template <typename R> R referenceBlockWithResult(boost::function<R(const T&)> f) const
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        return f(m_value);
    }

    void modifyBlock(boost::function<void(T&)> f)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        f(m_value);
    }

    template <typename R> R modifyBlockWithResult(boost::function<R(T&)> f)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_value_mtx);
        return f(m_value);
    }
};



#endif /* !defined(__h_AtomicObjecct__) */

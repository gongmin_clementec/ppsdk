//
//  JobSettingSendLog.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingSendLog.h"
#include "BaseSystem.h"
#include "PPConfig.h"
#include "TMSAccess.h"

JobSettingSendLog::JobSettingSendLog(const __secret& s) :
    PaymentJob(s),
    m_tms(TMSAccess::create())
{
}

/* virtual */
JobSettingSendLog::~JobSettingSendLog()
{
    
}

/* virtual */
auto JobSettingSendLog::getObservableSelf() -> pprx::observable<const_json_t>
{
    return m_tms->initializeAndLoginAndUpdate()
    .flat_map([=](const_json_t resp){
        auto SendingLogExpireDateTime = resp.get_optional<std::string>("SendingLogExpireDateTime");
        json_t j;
        j.put("bAvailable", SendingLogExpireDateTime != boost::none);
        return callOnDispatchWithCommandAndSubParameter("waitForStart", "", j)
        .flat_map([=](auto){
            if(!SendingLogExpireDateTime){
                throw_error_internal("SendingLogExpireDateTime == null")
            }
            return pprx::just(resp);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](const_json_t resp){
        return m_tms->logBegin();
    }).as_dynamic()
    .flat_map([=](const_json_t resp){
        auto&& LogId = resp.get<std::string>("LogId");
        auto&& UploadUrl = resp.get<std::string>("UploadUrl");
        auto zip = BaseSystem::instance().getDocumentDirectoryPath() / "log.zip";
        if(boost::filesystem::exists(zip)){
            boost::filesystem::remove(zip);
        }
        LogManager::instance().archive(zip);
        return m_tms->blobUpload(UploadUrl, zip)
        .flat_map([=](auto){
            return m_tms->logEnd(LogId);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        json_t j;
        j.put("status", "success");
        return pprx::just(j.as_readonly());
    }).as_dynamic();}

/* virtual */
auto JobSettingSendLog::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

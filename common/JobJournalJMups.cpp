//
//  JobJournalJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobJournalJMups.h"

JobJournalJMups::JobJournalJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobJournalJMups::~JobJournalJMups()
{
}

/* virtual */
auto JobJournalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    auto paymentType    = boost::make_shared<prmJournalCommon::paymentType>();
    auto bIntermediate  = boost::make_shared<bool>();
    auto reprint        = boost::make_shared<prmJournalCommon::reprint>();
    return callOnDispatchWithCommand("waitForJournalOptions")
    .flat_map([=](const_json_t json){
        *paymentType = prmJournalCommon::stringToPaymentType(json.get<std::string>("paymentType"));
        *bIntermediate = json.get<bool>("bIntermediate");
        *reprint = prmJournalCommon::stringToReprint(json.get<std::string>("reprint"));
        
        if((*bIntermediate) && (*reprint != prmJournalCommon::reprint::No)){
            /* 中間計はreprintが存在しない */
            throw_error_internal("parameter error : intermediate can't be reprinted.")
        }
        
        auto detailOption = prmJournalCommon::stringToDetailOption(json.get<std::string>("detailOption"));
        if(*bIntermediate){
            return hpsImJournal(*paymentType, detailOption);
        }
        else{
            return hpsDailyJournal(*paymentType, *reprint, detailOption);
        }
    }).as_dynamic()
    .flat_map([=](response_t resp){
        if((*reprint == prmJournalCommon::reprint::No) && !(*bIntermediate)){
            return hpsDailyJournalApply(*paymentType, resp.body.get<std::string>("normalObject.outputDate"))
            .flat_map([=](auto){
                return pprx::just(resp).as_dynamic();
            }).as_dynamic();
        }
        else{
            return pprx::just(resp).as_dynamic();
        }
    }).as_dynamic()
    .flat_map([=](response_t resp){
        if((*reprint == prmJournalCommon::reprint::Last) || (*reprint == prmJournalCommon::reprint::BeforeLast)){
            return callOnDispatchWithCommandAndSubParameter("waitForJournalReprintConfirm", "hps", resp.body).as_dynamic()
            .map([=](auto){
                return resp;
            }).as_dynamic();
        }
        return pprx::just(resp).as_dynamic();
    }).as_dynamic()
    .flat_map([=](response_t resp){
        return treatSlipDocumentJson(*paymentType, resp, *bIntermediate)
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic()
        .map([=](auto){
            return resp;
        }).as_dynamic();
    }).as_dynamic()
    .map([=](response_t resp){
        json_t result;
        result.put("status", "success");
        result.put("result.hps", resp.body.clone());
        return result.as_readonly();
    });
}

auto JobJournalJMups::treatSlipDocumentJson(prmJournalCommon::paymentType paymentType, JMupsAccess::response_t resp, bool bIntermediate)
-> pprx::observable<const_json_t>
{
    json_t pinfo;
    if(paymentType == prmJournalCommon::paymentType::All){
        json_t::array_t values;
        json_t::array_t type;
        for(auto brand : {"creditPrintInfo", "cupPrintInfo", "nfcPrintInfo"}){
            auto jkey = (boost::format("normalObject.%s.device.value") % brand).str();
            auto value = resp.body.get_optional(jkey);
            if(!value){
                LogManager_AddInformationF("skip %s", brand);
                continue;
            }

            LogManager_AddInformationF("print %s", brand);

            auto org_values = value->get<json_t::array_t>("values");
            auto org_type = value->get<json_t::array_t>("type");
            if(org_values.size() == 0){
                LogManager_AddError("org_values.size() == 0");
                continue;
            }
            if(org_type.size() == 0){
                LogManager_AddError("org_type.size() == 0");
                continue;
            }

            values.push_back(org_values[0]);
            type.push_back(org_type[0]);
        }
        pinfo.put("values", values);
        pinfo.put("type", type);
    }
    else{
        pinfo = resp.body.get("normalObject.printInfo.device.value").clone();
    }

    {
        auto values = pinfo.get<json_t::array_t>("values");
        
        for(auto v : values){
            if(bIntermediate){
                v.put("_INTERMEDIATE", true);
            }
            else{
                v.put("_NOT_INTERMEDIATE", true);
            }
        }
        pinfo.put("values", values);
    }

    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobJournalJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


/* virtual */
auto JobJournalJMups::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobJournalJMups::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}


//
//  CardReader.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_CardReader__)
#define __h_CardReader__

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic pop
#include "LazyObject.h"
#include "PPReactive.h"
#include "AtomicObject.h"

class CardReader :
    public LazyObject
{
public:
    enum class DeviceConnectState
    {
        Bad,
        Connecting,
        Connected,
        Disconnecting,
        Disconnected
    };

    enum class ICCardSlotState
    {
        Bad,
        Inserted,
        Removed
    };

    using sp = boost::shared_ptr<CardReader>;
    
private:
    
    pprx::subjects::behavior<DeviceConnectState>    m_subjectDeviceConnectState;
    pprx::subject<const_json_t>                     m_subjectICCardInserted;
    pprx::subjects::behavior<ICCardSlotState>       m_subjectICCardSlotState;

protected:
    auto subscriberICCardInserted()     { return m_subjectICCardInserted.get_subscriber(); }

    std::string deviceConnectStateToString(DeviceConnectState state) const;
    void setDeviceConnectState(DeviceConnectState state);
    void setICCardSlotState(ICCardSlotState state);
    
    json_t makeErrorJson(const std::string& description, boost::optional<int> code = boost::none) const
    {
        json_t json;
        json.put("description", description);
        if(code){
            json.put("code", *code);
        }
        return json;
    }
    
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit> = 0;
    virtual void prepareObservablesSelf() = 0;

public:
            CardReader(const __secret& s);
    virtual ~CardReader();

    auto    finalize() -> pprx::observable<pprx::unit>;
    void    prepareObservables();

    auto observableDeviceConnectState() { return m_subjectDeviceConnectState.get_observable(); }
    auto observableICCardInserted()     { return m_subjectICCardInserted.get_observable(); }
    auto observableICCardSlotState()    { return m_subjectICCardSlotState.get_observable(); }

    
    virtual auto displayText(const std::string& str) -> pprx::observable<pprx::unit> = 0;
    virtual auto displayTextNoLf(const std::string& str) -> pprx::observable<pprx::unit> = 0;
    virtual auto displayIdleImage() -> pprx::observable<pprx::unit> = 0;
    virtual auto connect() -> pprx::observable<pprx::unit> = 0;
    virtual auto disconnect() -> pprx::observable<pprx::unit> = 0;

    virtual auto beginCardRead(const_json_t param) -> pprx::observable<const_json_t> = 0;
    virtual auto beginICCardTransaction() -> pprx::observable<pprx::unit> = 0;
    virtual auto endICCardTransaction() -> pprx::observable<pprx::unit> = 0;
    virtual auto setICCardRemoved() -> pprx::observable<pprx::unit> = 0;

    virtual auto checkMasterData(const_json_t data) -> pprx::observable<const_json_t> = 0;
    virtual auto updateMasterData(const_json_t data) -> pprx::observable<pprx::unit> = 0;
    virtual auto readContactless(const_json_t param) -> pprx::observable<const_json_t> = 0;

    virtual auto adjustDateTime(const boost::posix_time::ptime& dtime)-> pprx::observable<pprx::unit> = 0;
    
    virtual auto sendApdu(const_json_t data) -> pprx::observable<const_json_t> = 0;

    virtual auto verifyPlainOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t> = 0;
    virtual auto verifyEncryptedOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t> = 0;
    virtual auto verifyEncryptedOnlinePin(const_json_t paramjson) -> pprx::observable<const_json_t> = 0;
    virtual auto abortTransaction() -> pprx::observable<pprx::unit> = 0;
    virtual auto txPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t> = 0;
    virtual auto txEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t> = 0;

    virtual auto getIfdSerialNo() -> pprx::observable<const_json_t> = 0;

    virtual auto onlineProccessingResult(bool bApproved) -> pprx::observable<pprx::unit> = 0;

    virtual auto getReaderInfo() -> pprx::observable<const_json_t> = 0;
    virtual auto isCardInserted() -> pprx::observable<bool> = 0;
    virtual auto updateFirmware(const boost::filesystem::path& basedir) -> pprx::observable<pprx::unit> = 0;

    virtual void setLockTransactionState(bool blockd = false) = 0;
    virtual auto getDisplayMsgForReader(const std::string& msgId, const bool bContactless = false) -> std::string = 0;
    virtual auto getCurrentReaderInfo()  -> const_json_t = 0;
    virtual auto clearCardreaderDisplay() -> pprx::observable<pprx::unit> = 0;
    
    template<class T> auto errorOnDeviceDisconnected(T value)
    {
        return pprx::just(pprx::unit())
        .flat_map([=](auto){
            return observableDeviceConnectState()
            .observe_on(pprx::observe_on_thread_pool())
            .flat_map([=](DeviceConnectState state){
                LogManager_AddDebug("errorOnDeviceDisconnected : receive state change");
                if((state != DeviceConnectState::Connected) && (state != DeviceConnectState::Connecting)){
                    throw_error_cardrw(makeErrorJson("disconnected"));
                }
                return pprx::never<T>();
            }).as_dynamic();
        }).as_dynamic()
        .merge(pprx::observe_on_thread_pool(), pprx::just(value))
        .tap
        (
         [=](auto){
             LogManager_AddDebug("errorOnDeviceDisconnected : next");
         },
         [=](auto){
             LogManager_AddDebug("errorOnDeviceDisconnected : error");
         },
         [=](){
             LogManager_AddDebug("errorOnDeviceDisconnected : completed");
         }
        );
    }
    
    template<class T> auto errorOnICCardRemoved(T value)
    {
        return pprx::just(pprx::unit())
        .flat_map([=](auto){
            return observableICCardSlotState()
            .observe_on(pprx::observe_on_thread_pool())
            .flat_map([=](ICCardSlotState state){
                LogManager_AddDebug("errorOnICCardRemoved : receive state change");
                if(state != ICCardSlotState::Inserted){
                    throw_error_cardrw(makeErrorJson("icCardRemoved"));
                }
                return pprx::never<T>();
            }).as_dynamic();
        }).as_dynamic()
        .merge(pprx::observe_on_thread_pool(), pprx::just(value))
        .tap
        (
         [=](auto){
             LogManager_AddDebug("errorOnICCardRemoved : next");
         },
         [=](auto){
             LogManager_AddDebug("errorOnICCardRemoved : error");
         },
         [=](){
             LogManager_AddDebug("errorOnICCardRemoved : completed");
         }
         );
    }
};


#endif /* !defined(__h_CardReader__) */

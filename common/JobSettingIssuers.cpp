//
//  JobSettingIssuers.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingIssuers.h"
#include "BaseSystem.h"

JobSettingIssuers::JobSettingIssuers(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobSettingIssuers::~JobSettingIssuers()
{
    
}

/* virtual */
auto JobSettingIssuers::getObservableSelf() -> pprx::observable<const_json_t>
{
    return hpsPrintCardCope()
    .map([=](auto resp){
        LogManager_AddInformationF("settingIssuers \n%s",resp.body.str());
        m_settingIssuers = resp.body.clone();
        return pprx::unit();
    })

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()

    .flat_map([=](auto){
        return waitForSettingIssuersConfirm().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        json_t json;
        json.put("status", "success");
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobSettingIssuers::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

auto JobSettingIssuers::treatSlipDocumentJson()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto pinfo = m_settingIssuers.get("normalObject.printInfo.device.value").clone();
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobSettingIssuers::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobSettingIssuers::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

auto JobSettingIssuers::waitForSettingIssuersConfirm()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto values = m_settingIssuers.get<json_t::array_t>("normalObject.printInfo.device.value.values");
    auto COMPANY_LIST = values[0].get("COMPANY_LIST");
    
    json_t param;
    param.put("COMPANY_LIST", COMPANY_LIST);
    
    return callOnDispatchWithCommandAndSubParameter("waitForSettingIssuersConfirm", "", param)
    .flat_map([=](auto){
        return pprx::observable<>::just(pprx::unit());
    });
}

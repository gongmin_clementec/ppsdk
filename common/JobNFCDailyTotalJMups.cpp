//
//  JobNFCDailyTotalJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCDailyTotalJMups.h"

JobNFCDailyTotalJMups::JobNFCDailyTotalJMups(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobNFCDailyTotalJMups::~JobNFCDailyTotalJMups()
{
    
}

/* virtual */
auto JobNFCDailyTotalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    auto result = boost::make_shared<json_t>();
    return callOnDispatchWithCommand("waitForOptions")
    .flat_map([=](const_json_t json){
        return hpsDailyJournal
        (
         JMupsAccessJournal::prmJournalCommon::paymentType::NFC,
         JMupsAccessJournal::prmJournalCommon::reprint::No,
         JMupsAccessJournal::prmJournalCommon::stringToDetailOption(json.get<std::string>("detailOption"))
         );
    }).as_dynamic()
    .flat_map([=](JMupsAccess::response_t resp){
        result->put("status", "success");
        result->put("result.hps", resp.body.clone());
        return hpsDailyJournalApply
        (
         JMupsAccessJournal::prmJournalCommon::paymentType::NFC,
         resp.body.get<std::string>("normalObject.outputDate")
         );
    })
    .map([=](auto){
        return result->as_readonly();
    });
}

/* virtual */
auto JobNFCDailyTotalJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}


//
//  JobSettingSendLog.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingSendLog__)
#define __h_JobSettingSendLog__

#include "PaymentJob.h"

class TMSAccess;

class JobSettingSendLog :
    public PaymentJob
{
private:
    boost::shared_ptr<TMSAccess> m_tms;

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobSettingSendLog(const __secret& s);
    virtual ~JobSettingSendLog();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobSettingSendLog__) */

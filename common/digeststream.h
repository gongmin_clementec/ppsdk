#if ! defined( __h_digeststream__ )
#define __h_digeststream__

#include <streambuf>
#include <iostream>
#include <vector>

#include <openssl/evp.h>

namespace ORANGEWERKS {
    
    class digeststream_exception :
    public std::exception
    {
    private:
        std::string m_strError;
        
    protected:
        
    public:
        digeststream_exception( const char* strError ) : m_strError( strError ) {}
        virtual const char* what() const noexcept { return( m_strError.c_str() ); }
    };
    
    /*
     ・flush() や sync() では全データは出力されず、デストラクタで全データが出力される
     ・_Elem は 1バイトでないと動作しない
     */
    
    enum class EDigestMethod
    {
          Bad		= 0
        , NONE      = 1
        , MD4
        , MD5
        , MDC2
        , SHA1
        , SHA224
        , SHA256
        , SHA384
        , SHA512
        , DSS
        , DSS1
        , ECDSA
        , RIPEMD160
        , WHIRLPOOL
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_digeststreambuf : public std::basic_streambuf<_Elem,_Traits>
    {
    public:
        
    private:
        using superclass_t = std::basic_streambuf<_Elem,_Traits>;
        
        std::basic_ostream<_Elem,_Traits>*		m_out;
        
        std::vector<_Elem>		m_srcBuff;		/* バッファ実体 */
        
        bool					m_bFinished;
        EVP_MD_CTX				m_ctx;
        
    protected:
        /* 外部からの出力バッファ指定 */
        virtual superclass_t* setbuf( _Elem* /* b */, std::streamsize /* s */ )
        {
            return( NULL );
        }
        
        /* 書き込みバッファが一杯になった時に呼び出される */
        virtual typename superclass_t::int_type overflow( typename superclass_t::int_type c = _Traits::eof() )
        {
            overflow_self();
            if( c == _Traits::eof() ){
                return( _Traits::eof() );
            }
            /* c はバッファから溢れたオブジェクトなので、ここで追加する */
            *superclass_t::pptr() = _Traits::to_char_type( c );
            superclass_t::pbump( 1 );
            return( _Traits::not_eof( c ) );
        }
        
        
        /* flush() で呼び出される */
        virtual int sync()
        {
            if( m_out ){
                digest();
                m_out->flush();
            }
            return( 0 );
        }
        
        
        void overflow_self()
        {
            digest();
        }
        
        void digest( bool bFinish = false )
        {
            if( m_bFinished ){
                setp( &m_srcBuff.front(), &m_srcBuff.front(), &m_srcBuff.back() + 1 );	/* buffer ポインタリセット */
                return;
            }
            
            const ptrdiff_t length = superclass_t::pptr() - &m_srcBuff.front();	/* 使用バッファサイズ */
            
            if( length > 0 ){
                ::EVP_DigestUpdate(	&m_ctx,
                                   reinterpret_cast<const unsigned char*>( &m_srcBuff.front() ),
                                   length );
            }
            
            if( bFinish ){
                m_bFinished = true;
                unsigned int	digestBytes = 0;
                _Elem			digest[ EVP_MAX_MD_SIZE ];
                ::EVP_DigestFinal( &m_ctx, reinterpret_cast<unsigned char*>( digest ), &digestBytes );
                m_out->write( digest, digestBytes );
            }
            
            setp( &m_srcBuff.front(), &m_srcBuff.front(), &m_srcBuff.back() + 1 );	/* buffer ポインタリセット */
        }
        
        static const EVP_MD* MethodToEVPMD( EDigestMethod method )
        {
            const EVP_MD* md = NULL;
            
            switch( method ){
                case EDigestMethod::NONE:		{ md = ::EVP_md_null();		break; }
                case EDigestMethod::MD4:		{ md = ::EVP_md4();			break; }
                case EDigestMethod::MD5:		{ md = ::EVP_md5();			break; }
                case EDigestMethod::MDC2:		{ md = ::EVP_mdc2();		break; }
                case EDigestMethod::SHA1:		{ md = ::EVP_sha1();		break; }
                case EDigestMethod::SHA224:		{ md = ::EVP_sha224();		break; }
                case EDigestMethod::SHA256:		{ md = ::EVP_sha256();		break; }
                case EDigestMethod::SHA384:		{ md = ::EVP_sha384();		break; }
                case EDigestMethod::SHA512:		{ md = ::EVP_sha512();		break; }
                case EDigestMethod::DSS:		{ md = ::EVP_dss();			break; }
                case EDigestMethod::DSS1:		{ md = ::EVP_dss1();		break; }
                case EDigestMethod::ECDSA:		{ md = ::EVP_ecdsa();		break; }
                case EDigestMethod::RIPEMD160:	{ md = ::EVP_ripemd160();	break; }
                case EDigestMethod::WHIRLPOOL:	{ md = ::EVP_whirlpool();	break; }
                case EDigestMethod::Bad:
                default:
                    break;
            }
            
            return( md );
        }
        
        
    public:
        basic_digeststreambuf( std::basic_ostream<_Elem,_Traits>& os ) :
        m_out( &os ), m_bFinished( false )
        {
        }
        
        
        void initialize( EDigestMethod method )
        {
            const EVP_MD* md = MethodToEVPMD( method );
            if( md == NULL ) throw( digeststream_exception( "basic_digeststreambuf: bad method" ) );
            
            ::EVP_MD_CTX_init( &m_ctx );
            
            if( m_out ){
                m_srcBuff.resize( 4096 );
                ::EVP_DigestInit( &m_ctx, md );
                setp( &m_srcBuff.front(), &m_srcBuff.front(), &m_srcBuff.back() + 1 );
            }
        }
        
        virtual ~basic_digeststreambuf()
        {
            if( m_out ){
                digest( true );
            }
            sync();
        }
        
        
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_odigeststream : public std::basic_ostream<_Elem,_Traits>
    {
    private:
        using superclass_t      = std::basic_ostream<_Elem,_Traits>;
        using digeststreambuf_t = basic_digeststreambuf<_Elem,_Traits>;
    protected:
    public:
        basic_odigeststream( std::ostream& os ) :
        std::basic_ostream<_Elem,_Traits>( new digeststreambuf_t( os ) )
        {
        }
        
        virtual ~basic_odigeststream()
        {
            superclass_t::flush();
            delete superclass_t::rdbuf();
        }
        
        void initialize( EDigestMethod method )
        {
            digeststreambuf_t* buff = dynamic_cast<digeststreambuf_t*>( superclass_t::rdbuf() );
            buff->initialize( method );
        }
    };
    
    
    using odigeststream = basic_odigeststream<char>;
    
}	/* namespace ORANGEWERKS */

#endif /* ! defined( __h_digeststream__ ) */
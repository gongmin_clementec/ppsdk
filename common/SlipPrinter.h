//
//  SlipPrinter.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_SlipPrinter__)
#define __h_SlipPrinter__


#include "LazyObject.h"
#include "PPReactive.h"
#include "SlipDocument.h"
#include "Image.h"

class Image;

class SlipPrinter :
    public LazyObject
{
private:
    boost::shared_ptr<Image> m_logoImage;
    
protected:
    virtual auto openDocumentSelf() -> pprx::observable<pprx::unit> = 0;
    virtual auto openPageSelf() -> pprx::observable<pprx::unit> = 0;
    virtual auto printPageSelf(SlipDocument::sp slip)  -> pprx::observable<pprx::unit> = 0;
    virtual auto closePageSelf() -> pprx::observable<pprx::unit> = 0;
    virtual auto closeDocumentSelf() -> pprx::observable<pprx::unit> = 0;

    virtual bool hasAutoCutter() const = 0;

    boost::shared_ptr<Image> logoImage() { return m_logoImage; }
    
public:
            SlipPrinter(const __secret& s);
    virtual ~SlipPrinter();
    
    void    setLogoImage(boost::shared_ptr<Image> image) { m_logoImage = image; }
    
    auto    openDocument() -> pprx::observable<pprx::unit>;
    auto    openPage() -> pprx::observable<pprx::unit>;
    auto    printPage(SlipDocument::sp slip)  -> pprx::observable<pprx::unit>;
    auto    closePage() -> pprx::observable<pprx::unit>;
    auto    closeDocument() -> pprx::observable<pprx::unit>;
    
    using sp = boost::shared_ptr<SlipPrinter>;
};


#endif /* !defined(__h_SlipPrinter__) */

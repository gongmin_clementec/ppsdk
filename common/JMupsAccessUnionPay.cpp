//
//  JMupsAccessUnionPay.cpp
//  ppsdk
//
//  Created by tel on 2017/06/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessUnionPay.h"
#include "JMupsJobStorage.h"

JMupsAccessUnionPay::JMupsAccessUnionPay()
{
    LogManager_Function();
}

/* virtual */
JMupsAccessUnionPay::~JMupsAccessUnionPay()
{
    LogManager_Function();
}

/* 取引確認 */
auto JMupsAccessUnionPay::hpsUnionPaySales_Confirm
(
 prmUnionPay::doubleTrade selector,
 int                    amount,
 boost::optional<std::string>   authenticationNo,
 bool                   isRePrint,
 boost::optional<int>   couponSelectListNum,
 boost::optional<int>   couponApplyPreviousAmount,
 bool                   pinRetryFlg,
 boost::optional<std::string> firstGACCmdRes
 )
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount"             , amount);
    setJsonParam(json, "authenticationNo"   , authenticationNo);
    setJsonParam(json, "isRePrint"          , isRePrint);
    setJsonParam(json, "firstGACCmdRes"     , firstGACCmdRes);
    setJsonParam(json, "couponSelectListNum", couponSelectListNum);
    setJsonParam(json, "couponApplyPreviousAmount"  , couponApplyPreviousAmount);
    setJsonParam(json, "pinRetryFlg"        , pinRetryFlg? "1" : "0");
    
    setCookie("tradeState", "2");
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cb0274.do?cB0274_1=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cb0777.do?cB0777_1=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();
        auto AMOUNT = integerToYenStringInJson(amount);
        reps["##AMOUNT##"]           = AMOUNT;
        reps["##SETTLEDAMOUNT##"]    = AMOUNT;
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}


/* 印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPaySales_Tagging
(
 prmUnionPay::doubleTrade       selector,
 const std::string&             slipNo,
 prmUnionPay::printResultDiv    printResultDiv,
 boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip,
 bool                       isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmUnionPay::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmUnionPay::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint"              , isRePrint);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cb0274.do?cB0274_2=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cb0777.do?cB0777_2=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 取引確認 */
auto JMupsAccessUnionPay::hpsUnionPayPreApproval_Confirm
(
 prmUnionPay::doubleTrade       selector,
 int                            amount,
 boost::optional<std::string>   authenticationNo,
 bool                           pinRetryFlg,
 boost::optional<std::string>   firstGACCmdRes
 )
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "amount"             , amount);
    setJsonParam(json, "authenticationNo"   , authenticationNo);
    setJsonParam(json, "firstGACCmdRes"     , firstGACCmdRes);
    setJsonParam(json, "pinRetryFlg"        , pinRetryFlg);
    setJsonParam(json, "isRePrint"          , false);
    
    setCookie("tradeState", "2");
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cf0274.do?cF0274_1=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cf0777.do?cF0777_1=aaa";
            break;
        default:
            break;
    }

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPayPreApproval_Tagging
(
 prmUnionPay::doubleTrade   selector,
 const std::string&         slipNo,
 prmUnionPay::printResultDiv  printResultDiv
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isRePrint"              , false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cf0274.do?cF0274_2=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cf0777.do?cF0777_2=aaa";
            break;
        default:
            break;
    }

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 取引確認 */
auto JMupsAccessUnionPay::hpsUnionPayAfterApproval_Confirm
(
 prmUnionPay::doubleTrade       selector,
 const std::string&             approvalNo,
 const std::string&             cupNo,
 const std::string&             cupSendDate,
 int                            amount,
 bool                           isRePrint,
 boost::optional<std::string>   authenticationNo,
 bool                   pinRetryFlg,
 boost::optional<std::string> firstGACCmdRes
)
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining"         , isTrainingMode());
    
    setJsonParam(json, "approvalNo"         , approvalNo);
    setJsonParam(json, "cupNo"              , cupNo);
    setJsonParam(json, "cupSendDateValue"   , cupSendDate);
    setJsonParam(json, "amount"             , amount);
    setJsonParam(json, "isRePrint"          , isRePrint);
    setJsonParam(json, "authenticationNo"   , authenticationNo);
    setJsonParam(json, "firstGACCmdRes"     , firstGACCmdRes);
    setJsonParam(json, "pinRetryFlg"        , pinRetryFlg);
    
    setCookie("tradeState", "2");
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cg0527.do?cG0527_1=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cg0977.do?cG0977_1=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPayAfterApproval_Tagging
(
 prmUnionPay::doubleTrade   selector,
 const std::string&         slipNo,
 prmUnionPay::printResultDiv  printResultDiv,
 boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip,
 bool                       isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo"     , slipNo);

    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmUnionPay::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmUnionPay::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint"              , isRePrint);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    std::string url;
    switch(selector){
        case prmUnionPay::doubleTrade::No:
            url = "cg0527.do?cG0527_2=aaa";
            break;
        case prmUnionPay::doubleTrade::Yes:
            url = "cg0977.do?cG0977_2=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessUnionPay::hpsUnionPayRefund_SearchTrade
(
 const std::string&     slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    
    std::string url = "cd0119.do?cD0119=aaa";
    
    if(isTrainingMode()){
        auto s           = boost::posix_time::to_iso_extended_string(boost::posix_time::second_clock::local_time());
        auto cupSendDate = (boost::format("[\"%s\", \"%s\", \"%s\", \"%s\", \"%s\"]")
                    % s.substr(5, 2) % s.substr(8, 2) % s.substr(11, 2) % s.substr(14, 2) % s.substr(17, 2)).str(); //["MM", "DD", "HH", "MM", "SS"]
        auto reps = keyvalue_t();
        reps["##SLIP_NO##"] = slipNo;
        reps["##CUPSENDDATE##"] = cupSendDate;
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

/* 取消返品取引確認 */
auto JMupsAccessUnionPay::hpsUnionPayRefund_Confirm
(
 int                    amount,
 const std::string&     slipNo,
 const std::string&     approvalNo,
 const std::string&     cupNo,
 const std::string&     cupSendDate,
 const std::string&     cupTradeDiv,
 boost::optional<std::string>   authenticationNo,
 bool                   pinRetryFlg,
 boost::optional<std::string> firstGACCmdRes
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount",            amount);
    setJsonParam(json, "slipNo",            slipNo);
    setJsonParam(json, "approvalNo",        approvalNo);
    setJsonParam(json, "cupNo",             cupNo);
    setJsonParam(json, "cupSendDateValue",  cupSendDate);
    setJsonParam(json, "authenticationNo",  authenticationNo);
    setJsonParam(json, "cupTradeDiv",       cupTradeDiv);
    setJsonParam(json, "isRePrint",         false);
    setJsonParam(json, "firstGACCmdRes",    firstGACCmdRes);
    setJsonParam(json, "pinRetryFlg",       pinRetryFlg);
    
    setCookie("tradeState", "2");
    

    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    std::string url = "cd0974.do?cD0974_1=aaa";
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();
        auto AMOUNT = integerToYenStringInJson(amount);
        reps["##AMOUNT##"]           = AMOUNT;
        reps["##SETTLEDAMOUNT##"]    = AMOUNT;
        reps["##SLIP_NO##"]          = slipNo;
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

/* 取消返品印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPayRefund_Tagging
(
 const std::string&         slipNo,
 prmUnionPay::printResultDiv  printResultDiv,
 boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmUnionPay::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmUnionPay::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint", false);
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto cupTradeDiv = tradeResultData.get<std::string>("normalObject.tradeResult.TRADE");
    setJsonParam(json, "cupTradeDiv", cupTradeDiv);      //印字結果は　取消か返品か
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = "cd0974.do?cD0974_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* オーソリ予約-取消対象取引検索 */
auto JMupsAccessUnionPay::hpsUnionPayPreApprovalCancel_SearchTrade
(
 const std::string&     slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    
    std::string url = "ci0119.do?cI0119=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* オーソリ予約取引確認 */
auto JMupsAccessUnionPay::hpsUnionPayPreApprovalCancel_Confirm
(
 int                    amount,
 const std::string&     slipNo,
 const std::string&     approvalNo,
 const std::string&     cupNo,
 const std::string&     cupSendDate,
 boost::optional<std::string>   authenticationNo,
 bool                   pinRetryFlg,
 boost::optional<std::string> firstGACCmdRes
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount",            amount);
    setJsonParam(json, "slipNo",            slipNo);
    setJsonParam(json, "approvalNo",        approvalNo);
    setJsonParam(json, "cupNo",             cupNo);
    setJsonParam(json, "cupSendDateValue",  cupSendDate);
    setJsonParam(json, "authenticationNo",  authenticationNo);
    setJsonParam(json, "cupTradeDiv",       "5"); // 銀聯オーソリ予約取消
    setJsonParam(json, "isRePrint",         false);
    setJsonParam(json, "firstGACCmdRes"     , firstGACCmdRes);
    setJsonParam(json, "pinRetryFlg"        , pinRetryFlg);
    
    setCookie("tradeState", "2");

    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    std::string url = "ci0874.do?cI0874_1=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* オーソリ予約印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPayPreApprovalCancel_Tagging
(
 const std::string&         slipNo,
 prmUnionPay::printResultDiv  printResultDiv,
 boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmUnionPay::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmUnionPay::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint", false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = "ci0874.do?cI0874_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 承認後売上取消返品-取消対象取引検索 */
auto JMupsAccessUnionPay::hpsUnionPayAfterApprovalCancel_SearchTrade
(
 const std::string&     slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    
    std::string url = "cj0119.do?cJ0119=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 承認後売上取消返品取引確認 */
auto JMupsAccessUnionPay::hpsUnionPayAfterApprovalCancel_Confirm
(
 int                    amount,
 const std::string&     slipNo,
 const std::string&     approvalNo,
 const std::string&     cupNo,
 const std::string&     cupSendDate,
 const std::string&     cupTradeDiv,
 boost::optional<std::string>   authenticationNo,
 bool                   pinRetryFlg,
 boost::optional<std::string> firstGACCmdRes
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount",            amount);
    setJsonParam(json, "slipNo",            slipNo);
    setJsonParam(json, "approvalNo",        approvalNo);
    setJsonParam(json, "cupNo",             cupNo);
    setJsonParam(json, "cupSendDateValue",  cupSendDate);
    setJsonParam(json, "authenticationNo",  authenticationNo);
    setJsonParam(json, "cupTradeDiv",       cupTradeDiv);
    setJsonParam(json, "isRePrint",         false);
    setJsonParam(json, "firstGACCmdRes",    firstGACCmdRes);
    setJsonParam(json, "pinRetryFlg",       pinRetryFlg);
    
    setCookie("tradeState", "2");

    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    std::string url = "cj0852.do?cJ0852_1=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 承認後売上取消返品印字結果送信 */
auto JMupsAccessUnionPay::hpsUnionPayAfterApprovalCancel_Tagging
(
 const std::string&         slipNo,
 prmUnionPay::printResultDiv  printResultDiv,
 boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmUnionPay::printResultDiv::Success , "2"},
                     {prmUnionPay::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmUnionPay::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmUnionPay::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto cupTradeDiv = tradeResultData.get<std::string>("normalObject.tradeResult.TRADE");
    setJsonParam(json, "cupTradeDiv", cupTradeDiv);      //印字結果は　取消か返品か
    
    setJsonParam(json, "isRePrint", false);
    
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = "cj0852.do?cJ0852_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

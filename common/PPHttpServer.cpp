//
//  PPHttpServer.cpp
//  ppsdk
//
//  Created by tel on 2018/08/06.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "BaseSystem.h"
#include "PPHttpServer.h"
#include "LogManager.h"
#include "PaymentJobCreator.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "JMupsJobStorage.h"

PPWebSocketSession::PPWebSocketSession(boost::shared_ptr<PPHttpServer> http, int sessionIndex) :
    m_http(http),
    m_messageIndex(0),
    m_unsolicited_messageIndex(0),
    m_sessionIndex(sessionIndex),
    m_jobTimer(BaseSystem::instance().ioService()),
    m_dispath_bNoMore(false)
{
    LogManager_Function();
}

/* virtual */
PPWebSocketSession::~PPWebSocketSession()
{
    LogManager_Function();
}

void PPWebSocketSession::detach()
{
    LogManager_Function();
    boost::unique_lock<boost::recursive_mutex> lock(m_socket_mtx);
    LogManager_AddInformationF("[ws:%p] PPWebSocketSession::detach(%d)", reinterpret_cast<void*>(m_socket.get()) % m_sessionIndex);
    m_socket.reset();
}

void PPWebSocketSession::initialize()
{
    LogManager_Function();
    auto self = shared_from_this();

    m_wsRead.get_observable()
    .flat_map([self](json_t::readonly j){
        return self->process(j);
    }).as_dynamic()
    .subscribe
    (
     [self](auto){
         LogManager_AddInformation("wsRead subscribe next");
     },
     [self](auto err){
         /* m_wsRead はエラーを発行しないので、事実上ここには到達し得ない */
         LogManager_AddInformation("wsRead subscribe error");
     },
     [self](){
         LogManager_AddInformation("wsRead subscribe completed");
         self->detach();
         self->m_finish.get_subscriber().on_next(pprx::unit());
         if(self->m_job){
             LogManager_AddInformation("job reset");
             self->m_job.reset();
             self->m_http->addJobCount(-1);
         }
         else{
             LogManager_AddInformation("job not exists");
         }
         self->m_http->sessionDetachManageMap(self->m_sessionIndex);
     });
}


void PPWebSocketSession::attach(HttpServer::WebSocket::sp ws, boost::optional<json_t> json)
{
    LogManager_Function();
    auto self = shared_from_this();
    
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_socket_mtx);
        LogManager_AddInformationF("[ws:%p] PPWebSocketSession::attach(%d) (old)", reinterpret_cast<void*>(m_socket.get()) % m_sessionIndex);
        m_socket = ws;
        LogManager_AddInformationF("[ws:%p] PPWebSocketSession::attach(%d) (new)", reinterpret_cast<void*>(m_socket.get()) % m_sessionIndex);
    }
    
    pprx::subject<std::string> initial;
    
    /* wsの観測 */
    initial.get_observable()
    .merge(ws->getObservable())
    .take_until(self->m_finish.get_observable())
    .subscribe(
        [self](const std::string& data){
            LogManager_AddInformation("ws subscribe next");
            auto rmessage = message_t::fromString(data);
            if(rmessage->bUnsolicitedMessage){
                LogManager_AddInformationF("[ws:%p] *unsolicited* session(%d) receive websocket -> %s", reinterpret_cast<void*>(self->m_socket.get()) % self->m_sessionIndex % data);
                if(rmessage->bUnsolicitedResponse){
                    /* 送信した unsolicited message が到達した */
                    auto next = [self, rmessage](){
                        boost::unique_lock<boost::recursive_mutex> lock(self->m_sendMessages_mtx);
                        auto erase_it = std::find_if(self->m_sendMessages.begin(), self->m_sendMessages.end(), [rmessage](auto v){
                            return v->bUnsolicitedMessage && (rmessage->messageIndex == v->messageIndex);
                        });
                        if(erase_it != self->m_sendMessages.end()){
                            self->m_sendMessages.erase(erase_it);
                        }

                        auto next_it = std::find_if(self->m_sendMessages.begin(), self->m_sendMessages.end(), [rmessage](auto v){
                            return v->bUnsolicitedMessage;
                        });
                        if(next_it != self->m_sendMessages.end()){
                            return *next_it;
                        }
                        else{
                            return message_t::sp();
                        }
                    }();
                    /* 後続のunsolicited messageがあれば送信する */
                    if(next){
                        self->sendToSafeSocketAsync(next);
                    }
                }
                else{
                    /* 受信した unsolicited message が処理済みかを確認する */
                    auto proceed = [self, rmessage](){
                        boost::unique_lock<boost::recursive_mutex> lock(self->m_unsolicited_lastReceiveMessageIndex_mtx);
                        if(self->m_unsolicited_lastReceiveMessageIndex && (self->m_unsolicited_lastReceiveMessageIndex == rmessage->messageIndex)){
                            /* 既にemit済み */
                            return message_t::sp();
                        }
                        else{
                            self->m_unsolicited_lastReceiveMessageIndex = rmessage->messageIndex;
                            /* 受信した unsolicited message を処理 */
                            return rmessage;
                        }
                    }();
                    if(proceed){
                        self->m_wsRead.get_subscriber().on_next(proceed->messageJson);
                    }
                    /* 到達応答を返却する */
                    json_t jresp;
                    jresp.put("sessionIndex", self->m_sessionIndex);
                    jresp.put("messageIndex", rmessage->messageIndex);
                    jresp.put("messageJson", nullptr);
                    jresp.put("bUnsolicitedMessage", true);
                    jresp.put("bUnsolicitedResponse", true);
                    
                    self->sendToSafeSocketAsync(message_t::fromJson(jresp));
                }
            }
            else{
                /* 請求メッセージに対する応答処理 */
                
                LogManager_AddInformationF("[ws:%p] session(%d), message(%d), receive websocket -> %s",
                                           reinterpret_cast<void*>(self->m_socket.get()) %
                                           self->m_sessionIndex %
                                           rmessage->messageIndex %
                                           data);
                auto bFoundSolicitedRequest = [=](){
                    boost::unique_lock<boost::recursive_mutex> lock(self->m_sendMessages_mtx);
                    auto it = std::find_if(self->m_sendMessages.begin(), self->m_sendMessages.end(), [rmessage](auto v){
                        return (!v->bUnsolicitedMessage) && (rmessage->messageIndex == v->messageIndex);
                    });
                    
                    message_t::sp sr;
                    if(it == self->m_sendMessages.end()){
                        return false; /* 既に処理済み */
                    }
                    else{
                        self->m_sendMessages.erase(it);
                        return true;
                    }
                }();
                if(bFoundSolicitedRequest){
                    self->m_wsRead.get_subscriber().on_next(rmessage->messageJson);
                }

                /* 次に溜まっている請求メッセージがあれば送信する */
                auto nextSolicitedMessage = [=](){
                    boost::unique_lock<boost::recursive_mutex> lock(self->m_sendMessages_mtx);
                    auto it = std::find_if(self->m_sendMessages.begin(), self->m_sendMessages.end(), [rmessage](auto v){
                        return !v->bUnsolicitedMessage;
                    });
                    if(it == self->m_sendMessages.end()){
                        return message_t::sp();
                    }
                    else{
                        return *it;
                    }
                }();
                if(nextSolicitedMessage){
                    self->sendToSafeSocketAsync(nextSolicitedMessage);
                }
            }
        },
        [self](auto err){
            LogManager_AddError("ws subscribe error");
        },
        [self](){
            LogManager_AddInformation("ws subscribe completed");
        }
    );

    if(json){
        /* json は相手方からの接続時に投げられたメッセージ */
        initial.get_subscriber().on_next(json->str());
        initial.get_subscriber().on_completed();
        
        bool bSendMessage = false;
        bool bSendUnsolicited = false;

        boost::unique_lock<boost::recursive_mutex>  lock(m_sendMessages_mtx);
        for(auto it = m_sendMessages.begin(); it != m_sendMessages.end(); it++){
            if((*it)->bUnsolicitedMessage){
                if(bSendUnsolicited) continue;
                bSendUnsolicited = true;
                sendToSafeSocketAsync(*it);
            }
            else{
                if(bSendMessage) continue;
                bSendMessage = true;
                sendToSafeSocketAsync(*it);
            }
        }
    }
    else{
        LogManager_AddInformationF("新規接続(%d)", m_sessionIndex);
        send(boost::none);
    }
}

HttpServer::WebSocket::sp PPWebSocketSession::safeSocket()
{
    HttpServer::WebSocket::sp s;
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_socket_mtx);
        s = m_socket;
    }
    return s;
}

void PPWebSocketSession::send(boost::optional<json_t> json)
{
    LogManager_Function();
    const auto messageIndex = [=](){
        boost::unique_lock<boost::recursive_mutex>  lock(m_messageIndex_mtx);
        m_messageIndex++;
        return m_messageIndex;
    }();
    json_t j;
    j.put("sessionIndex", m_sessionIndex);
    j.put("messageIndex", messageIndex);
    if(json){
        j.put("messageJson", *json);
    }
    else{
        j.put("messageJson", nullptr);
    }
    j.put("bUnsolicitedMessage", false);
    j.put("bUnsolicitedResponse", false);

    auto message = message_t::fromJson(j);
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_sendMessages_mtx);
        m_sendMessages.push_back(message);
    }
    sendToSafeSocketAsync(message);
}

void PPWebSocketSession::sendUnsolicitedMessage(boost::optional<json_t> json)
{
    LogManager_Function();
    
    auto message = [=]{
        const auto messageIndex = [=](){
            boost::unique_lock<boost::recursive_mutex>  lock(m_unsolicited_messageIndex_mtx);
            m_unsolicited_messageIndex++;
            return m_unsolicited_messageIndex;
        }();

        json_t j;
        j.put("sessionIndex", m_sessionIndex);
        j.put("messageIndex", messageIndex);
        if(json){
            j.put("messageJson", *json);
        }
        else{
            j.put("messageJson", nullptr);
        }
        j.put("bUnsolicitedMessage", true);
        j.put("bUnsolicitedResponse", false);

        auto message = message_t::fromJson(j);
        auto countUnsolicitedMessage = [=](){
            boost::unique_lock<boost::recursive_mutex> lock(m_sendMessages_mtx);
            m_sendMessages.push_back(message);
            return std::count_if(m_sendMessages.begin(), m_sendMessages.end(), [](auto v){
                return v->bUnsolicitedMessage;
            });
        }();
        if(countUnsolicitedMessage == 1){
            return message;
        }
        else{
            LogManager_AddInformation("[ws:%p] *unsolicited* session(%d) push message buffer");
            return message_t::sp();
        }
    }();
    if(message){
        sendToSafeSocketAsync(message);
    }
}

void PPWebSocketSession::sendToSafeSocketAsync(message_t::sp message)
{
    LogManager_Function();

    auto socket = safeSocket();

    auto jmessage = message->toJson().str();
    if(message->bUnsolicitedMessage){
        LogManager_AddInformationF("[ws:%p] *unsolicited* session(%d), message(%d), send websocket -> %s", reinterpret_cast<void*>(socket.get()) % m_sessionIndex % message->messageIndex % jmessage);
    }
    else{
        LogManager_AddInformationF("[ws:%p] session(%d), message(%d), send websocket -> %s", reinterpret_cast<void*>(socket.get()) % m_sessionIndex % message->messageIndex % jmessage);
    }

    if(socket){
        /*
            socket->send() は非同期でかつ 内部で mutex を使ってブロッキングを行なっている。
            unsolicited message を導入したことで、socketへのsend要求が連続する場合、
            socket->send() は自分自身の罠にはまって動けなくなる。
            そこで、送信自体を別スレッドで実行することでこの問題を回避した。
            送信データの順序については、unsolicited messageは応答を待つので問題ない。
            また、JOBのdispatchは同期処理になるので、順序を追い越す事はない。
         */
        BaseSystem::instance().post([socket, jmessage](){
            socket->send(jmessage);
        });
    }
    else{
        LogManager_AddWarning("socket == null");
    }
}


auto PPWebSocketSession::process(json_t::readonly j)
    -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    LogManager_AddInformation(j.str());
    
    auto self = shared_from_this();
    auto const& type = j.get_optional<std::string>("type");
    if(!type){
        throw_error_internal("type not found.");
    }

    auto messageIndex = [=](){
        boost::unique_lock<boost::recursive_mutex>  lock(m_messageIndex_mtx);
        return m_messageIndex;
    }();
    
    LogManager_AddInformationF
    (
     "session(%d), message(%d), process(\"%s\")",
     m_sessionIndex % messageIndex % (*type)
     );
    if(*type == "executeJob"){
        auto const& data = j.get_optional("data");
        if(!data){
            throw_error_internal("data not found.");
        }
        
        auto const& jobName = data->get_optional<std::string>("jobName");
        if(!jobName){
            throw_error_internal("data.jobName not found.");
        }
        auto const& bTraining = data->get_optional<bool>("bTraining");
        auto const& bTrainingUseDigiSign = data->get_optional<bool>("bTrainingUseDigiSign");

        
        self->m_http->addJobCount(1);

        PaymentJobCreator jc;
        m_job = jc.create(*jobName);
        
        m_job->setStorage(m_http->storage());
        if(m_job->isCardReaderRequired()){
            LogManager_AddInformation("カードリーダが必要な業務");
            m_job->setCardReader(m_http->cardReader());
        }
        if(bTraining && (*bTraining)){
            m_job->setTrainingMode(true);
            if(bTrainingUseDigiSign && (*bTrainingUseDigiSign)){
                m_job->setUseDigiSignInTrainingMode(true);
            }
        }
        
        if(m_job->isJobTimeoutRequired()){
            startJobTimer(PPConfig::instance().get<int>("hps.jobTimeout"));
        }
        
        m_subscription = m_job->getObservable
        (
         [self](const std::string& inParam, std::string& outParam) -> bool{
             auto dispatch_bNoMore = [self](){
                 boost::unique_lock<boost::recursive_mutex> lock(self->m_dispath_bNoMore_mtx);
                 return self->m_dispath_bNoMore;
             }();
             if(dispatch_bNoMore){
                 LogManager_AddInformation("m_dispath_bNoMore == true");
                 outParam = "{}";
                 return false;
             }
             
             LogManager_Scoped("dispatch region");
             self->m_dispatch_sem.lock();
             json_t j;
             j.put("type", "onDispatch");
             j.put("data", json_t::from_str(inParam));
             if(j.get<std::string>("data.command") == "notice"){
                 if(j.get<std::string>("data.message") == "disableAborting"){
                     self->stopJobTimer();
                 }
             }
             self->send(j);
             self->m_dispatch_sem.lock();
             self->m_dispatch_sem.unlock();
             outParam = self->m_dispatch_result.str();
             return self->m_dispath_bContinue;
         },
         [self](){
             json_t j;
             j.put("type", "onDispatchCancel");
             self->sendUnsolicitedMessage(j);
         }
        )
        .subscribe(
           [self](const_json_t inParam){
               LogManager_AddInformationF("process.subscribe.success -> %s", inParam.str());
               json_t j;
               j.put("type", "onFinish");
               j.put("data", inParam.clone());
               self->send(j);
               self->stopJobTimer();
           },
           [self](std::exception_ptr err){
               /* ここにくるのは Force Abort Anyway のみ */
               LogManager_AddInformation("process.subscribe.error");
               self->stopJobTimer();
               self->m_wsRead.get_subscriber().on_completed();
               LogManager_AddInformation("unlock dispatch on PaymentJob::AbortReason::ForceAnyway");
               {
                   boost::unique_lock<boost::recursive_mutex> lock(self->m_dispath_bNoMore_mtx);
                   self->m_dispath_bNoMore = true;
               }
               self->m_dispatch_result = json_t();
               self->m_dispath_bContinue = false;
               self->m_dispatch_sem.unlock();
           },
           [self](){
               LogManager_AddInformation("process.subscribe.completed");
           }
        );
    }
    else if(*type == "goodbye"){
        json_t j;
        j.put("type", "onGoodbye");
        j.put("data", nullptr);
        self->send(j);
        m_wsRead.get_subscriber().on_completed();
    }
    else if(*type == "dispatchResult"){
        m_dispatch_result = j.get("data");
        m_dispath_bContinue = j.get<bool>("bContinue");
        m_dispatch_sem.unlock();
    }
    else if(*type == "cancelRequest"){
        self->abort(PaymentJob::AbortReason::UserRequest);
    }
    else if(*type == "finishResult"){
        /* 何もすることはない */
    }
    else if(*type == "canSendTextToAnotherApplication"){
        /*
         "canSendTextToAnotherApplication" と "canSendTextToAnotherApplicationResult" は同期処理ではあるが、
         * アプリケーション層からの要求であるのでmessageIndexを採番できない
         * 制御ソケットではなく業務ソケットに以降する場合がある
         という２点から、UnsolicitedMessageに仕様変更。
         */
        BaseSystem::instance().canSendTextToAnotherApplication(j.get<std::string>("data"))
        .subscribe([=](bool bSuccess){
            json_t resp;
            resp.put("type", "canSendTextToAnotherApplicationResult");
            resp.put("data.bSuccess", bSuccess);
            sendUnsolicitedMessage(resp);
        });
    }
    else if(*type == "sendTextToAnotherApplication"){
        /*
        "sendTextToAnotherApplication" と "sendTextToAnotherApplicationResult" は同期処理ではあるが、
            * アプリケーション層からの要求であるのでmessageIndexを採番できない
            * 制御ソケットではなく業務ソケットに以降する場合がある
        という２点から、UnsolicitedMessageに仕様変更。
         */
        BaseSystem::instance().applicationState()
        .flat_map([=](BaseSystem::ApplicationState state){
            if(state == BaseSystem::ApplicationState::Foreground){
                return pprx::just(pprx::unit()).as_dynamic();
            }
            return pprx::never<pprx::unit>().as_dynamic();
        }).as_dynamic()
        .take(1)
        .flat_map([=](pprx::unit){
            return BaseSystem::instance().sendTextToAnotherApplication(j.get<std::string>("data"));
        }).as_dynamic()
        .subscribe([=](bool bSuccess){
            json_t resp;
            resp.put("type", "sendTextToAnotherApplicationResult");
            resp.put("data.bSuccess", bSuccess);
            sendUnsolicitedMessage(resp);
        });
    }
    else{
        LogManager_AddErrorF("unknown type: %s", (*type));
    }
   
    return pprx::just(pprx::unit());
}


void PPWebSocketSession::abort(PaymentJob::AbortReason reason)
{
    LogManager_Function();
    
    auto job = m_job;
    
    if(job){
        job->abort(reason);
        if(reason == PaymentJob::AbortReason::ForceAnyway){
            /* 業務タイムアウトが先に発生すると、UI待ちが発生するため、ここでUI待ちを解除する */
            LogManager_AddInformation("unlock dispatch on PaymentJob::AbortReason::ForceAnyway");
            m_wsRead.get_subscriber().on_completed();
            {
                boost::unique_lock<boost::recursive_mutex> lock(m_dispath_bNoMore_mtx);
                m_dispath_bNoMore = true;
            }
            m_dispatch_result = json_t();
            m_dispath_bContinue = false;
            m_dispatch_sem.unlock();
        }
    }
    else{
        LogManager_AddInformation("no job available");
        m_http->sessionDetachManageMap(m_sessionIndex);
        return;
    }
}

void PPWebSocketSession::startJobTimer(int timeoutSecond)
{
    LogManager_Function();
    
    m_jobTimer.expires_from_now(boost::posix_time::seconds(timeoutSecond));
    m_jobTimer.async_wait([=](const boost::system::error_code& error){
        if (error){
            LogManager_AddWarningF("timer aborted with error. %s (%d)", error.message() % error.value());
        }
        else{
            LogManager_AddInformation("call abort() by job timer");
            abort(PaymentJob::AbortReason::JobTimer);
        }
    });
}

void PPWebSocketSession::stopJobTimer()
{
    LogManager_Function();
    m_jobTimer.cancel();
}

PPHttpServer::PPHttpServer() :
    m_sessionIndex(0),
    m_jobCountSubject(0)
{
}

/* virtual */
PPHttpServer::~PPHttpServer()
{
}

void PPHttpServer::initialize()
{
    PPConfig::instance().deserialize
    (
     BaseSystem::instance().getResourceDirectoryPath() / "ppconfig.default.json",
     false
     );
    LogManager::instance().updateOperationMode();
    m_cardReader = AbstructFactory::instance().createCardReader();
    m_storage    = boost::make_shared<JMupsJobStorage>();
    m_server     = HttpServer::create();
}

void PPHttpServer::begin(const std::string& documentRoot, int portNumber, bool bUseLocalhost)
{
    beginServerSbuscribe();
    auto ip = bUseLocalhost ? "127.0.0.1" : getSelfIpv4Address("en0");
    if(ip.empty()) ip = "127.0.0.1";
    m_server->begin(ip, portNumber, documentRoot);
}


void PPHttpServer::beginServerSbuscribe()
{
    LogManager_Function();
    if(m_serverSubscription.is_subscribed()){
        LogManager_AddInformation("server subscribe already started -> do nothing");
        return;
    }
    
    auto self = shared_from_this();
    m_serverSubscription = m_server->getObservable()
    .subscribe
    (
     [self](HttpServer::WebSocket::sp ws){
         LogManager_AddInformation("httpserver subscribe next");
         ws->getObservable()
         .take(1)    /* ここでの受信は１回のみで、以降はPPWebSocketSessionでlisteningする */
         .subscribe
         (
          [self, ws](const std::string& body){
              LogManager_AddInformation("websocket subscribe nest");
              if(body == "new"){
                  LogManager_AddInformation("新規接続要求");
                  const auto sessionIndex = [self](){
                      boost::unique_lock<boost::recursive_mutex>  lock(self->m_session_mtx);
                      self->m_sessionIndex++;
                      return self->m_sessionIndex;
                  }();
                  
                  auto sess = boost::make_shared<PPWebSocketSession>(self, sessionIndex);
                  sess->initialize();
                  sess->attach(ws, boost::none);
                  {
                      boost::unique_lock<boost::recursive_mutex>  lock(self->m_session_mtx);
                      self->m_sessionMap[sessionIndex] = sess;
                  }
              }
              else{
                  const auto& json = json_t::from_str(body);
                  const auto& sessionIndex = json.get_optional<int>("sessionIndex");
                  if(!sessionIndex){
                      LogManager_AddError("json内にsessionIndexプロパティが存在しない");
                      return;
                  }
                  auto session = [self, sessionIndex](){
                      boost::unique_lock<boost::recursive_mutex>  lock(self->m_session_mtx);
                      auto it = self->m_sessionMap.find(*sessionIndex);
                      if(it == self->m_sessionMap.end()){
                          LogManager_AddError("該当するsessionIndexが存在しない");
                          return PPWebSocketSession::sp();
                      }
                      return it->second;
                  }();
                  if(session){
                      LogManager_AddInformation("再接続開始");
                      session->attach(ws, json);
                  }
              }
          },
          [self](auto err){
              LogManager_AddError("websocket subscribe error");
          },
          [self](){
              LogManager_AddInformation("websocket subscribe complete");
          }
          );
     },
     [self](auto err){
         LogManager_AddError("httpserver subscribe error");
     },
     [self](){
         LogManager_AddInformation("httpserver subscribe complete");
     }
     );
}

void PPHttpServer::endServerSubscribe()
{
    LogManager_Function();
    m_serverSubscription.unsubscribe();
}


#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* static */
std::string PPHttpServer::getSelfIpv4Address(const std::string& ifName)
{
    auto ifdeleter = [](ifaddrs* p){
         ::freeifaddrs(p);
    };
    auto ifAddrStruct = std::unique_ptr<ifaddrs, decltype(ifdeleter)>(
        [](){
            ifaddrs* s = nullptr;
            ::getifaddrs(&s);
            return s;
        }(),
        ifdeleter
    );
    
    std::string result = "";
    
    for (ifaddrs* ifa = ifAddrStruct.get(); ifa != nullptr; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) { /* IPv4 */
            auto const& addrIn  = reinterpret_cast<sockaddr_in*>(ifa->ifa_addr);
            auto tmpAddrPtr     = &addrIn->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            ::inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            LogManager_AddInformationF("[IPv4] %s -> %s", ifa->ifa_name % addressBuffer);
            if(ifName == ifa->ifa_name){
                result = addressBuffer;
            }
        } else if (ifa->ifa_addr->sa_family == AF_INET6) { /* IPv6 */
            auto const& addrIn  = reinterpret_cast<sockaddr_in6*>(ifa->ifa_addr);
            auto tmpAddrPtr     = &addrIn->sin6_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            ::inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
            LogManager_AddInformationF("[IPv6] %s -> %s", ifa->ifa_name % addressBuffer);
        }
    }

    LogManager_AddInformationF("result(%s) ->%s", ifName % result);
    return result;
}

void PPHttpServer::sessionDetachManageMap(int sessionIndex)
{
    LogManager_Function();
    LogManager_AddInformationF("sessionIndex %d", sessionIndex);
    boost::unique_lock<boost::recursive_mutex>  lock(m_session_mtx);
    m_sessionMap.erase(sessionIndex);
    LogManager_AddInformationF("m_sessionMap.size() = %d", m_sessionMap.size());
}

void PPHttpServer::suspend()
{
    LogManager_Function();
    
    LogManager_AddInformation("suspend(): end web server subscribe");
    endServerSubscribe();
    
    LogManager_AddInformation("suspend(): detach websocket in session");
    auto smap2 = [=]{
        boost::unique_lock<boost::recursive_mutex>  lock(m_session_mtx);
        return m_sessionMap;
    }();
    for(auto s : smap2){
        s.second->detach();
    }
    
    LogManager_AddInformation("suspend(): server suspend");
    m_server->suspend();
}

void PPHttpServer::resume()
{
    LogManager_Function();

    LogManager_AddInformation("resume(): resume web server subscribe");
    beginServerSbuscribe();

    LogManager_AddInformation("resume(): server resume");
    m_server->resume();
}

void PPHttpServer::waitUntilServerRunning(boost::function<void(bool)> completionHandler)
{
    LogManager_Function();
    m_server->getState()
    .flat_map([=](HttpServer::State state){
        if(state == HttpServer::State::Running){
            LogManager_AddInformation("waitServerRunning(): state == HttpServer::State::Running");
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else{
            LogManager_AddInformation("waitServerRunning(): state != HttpServer::State::Running");
            return pprx::never<pprx::unit>().as_dynamic();
        }
    }).as_dynamic()
    .take(1)
    .subscribe
    (
     [=](pprx::unit){
         LogManager_AddInformation("waitServerRunning(): subscribe next");
         completionHandler(true);
     },
     [=](auto){
         LogManager_AddError("waitServerRunning(): subscribe error");
         completionHandler(false);
     },
     [=](){
         LogManager_AddInformation("waitServerRunning(): subscribe completed");
     });
}

void PPHttpServer::waitUntilNoJobs(boost::function<void(bool)> completionHandler)
{
    LogManager_Function();
    m_jobCountSubject.get_observable()
    .flat_map([=](int count){
        LogManager_AddInformationF("waitUntilNoJobs(): count = %d", count);
        if(count == 0){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else{
            return pprx::never<pprx::unit>().as_dynamic();
        }
    }).as_dynamic()
    .take(1)
    .subscribe
    (
     [=](pprx::unit){
         LogManager_AddInformation("waitUntilNoJobs(): subscribe next");
         completionHandler(true);
     },
     [=](auto){
         LogManager_AddError("waitUntilNoJobs(): subscribe error");
         completionHandler(false);
     },
     [=](){
         LogManager_AddInformation("waitUntilNoJobs(): subscribe completed");
     });
}

void PPHttpServer::waitUntilJobWakeup(boost::function<void(bool)> completionHandler)
{
    LogManager_Function();
    m_jobCountSubject.get_observable()
    .flat_map([=](int count){
        LogManager_AddInformationF("waitUntilJobWakeup(): count = %d", count);
        if(count != 0){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else{
            return pprx::never<pprx::unit>().as_dynamic();
        }
    }).as_dynamic()
    .take(1)
    .subscribe
    (
     [=](pprx::unit){
         LogManager_AddInformation("waitUntilJobWakeup(): subscribe next");
         completionHandler(true);
     },
     [=](auto){
         LogManager_AddError("waitUntilJobWakeup(): subscribe error");
         completionHandler(false);
     },
     [=](){
         LogManager_AddInformation("waitUntilJobWakeup(): subscribe completed");
     });
}

void PPHttpServer::forceAbortJobs()
{
    LogManager_Function();
    
    auto smap = [=]{
        boost::unique_lock<boost::recursive_mutex>  lock(m_session_mtx);
        return m_sessionMap;
    }();
    for(auto s : smap){
        LogManager_AddInformationF("abort job sessionid = %d", s.first);
        s.second->abort(PaymentJob::AbortReason::ForceAnyway);
    }
}

void PPHttpServer::addJobCount(int add)
{
    LogManager_Function();
    m_jobCountSubject.get_observable()
    .take(1)
    .subscribe([=](int count){
        m_jobCountSubject.get_subscriber().on_next(count + add);
    });
}


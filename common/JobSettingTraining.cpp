//
//  JobSettingTraining.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobSettingTraining.h"
#include "BaseSystem.h"

JobSettingTraining::JobSettingTraining(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobSettingTraining::~JobSettingTraining()
{
    
}

/* virtual */
auto JobSettingTraining::getObservableSelf() -> pprx::observable<const_json_t>
{
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        json_t j;
        j.put("status", "success");
        return pprx::just(j.as_readonly());
    }).as_dynamic();
}

/* virtual */
auto JobSettingTraining::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

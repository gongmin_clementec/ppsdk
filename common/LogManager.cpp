//
//  LogManager.cpp
//  ppsdk
//
//  Created by clementec on 2016/05/11.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#include "LogManager.h"
#include "BaseSystem.h"
#include "PPConfig.h"
#include <boost/regex.hpp>
#include <sstream>
#include "ZipFileCompressor.h"

static const int kLogMaxFiles = 7;

LogManager::LogManager(size_t maxLogItem) :
    m_items(maxLogItem),
    m_items_wp(0),
    m_items_wrapped(false),
    m_opmode_bOutputToConsole(false),
    m_opmode_bRecordLog(false),
    m_log_max_files(kLogMaxFiles)
{
    m_log_directory = BaseSystem::instance().getDocumentDirectoryPath() / "log";
    if(!boost::filesystem::exists(m_log_directory)){
        boost::filesystem::create_directories(m_log_directory);
    }
    BaseSystem::instance().post([=](){
        deleteOldFiles();
    });
}

LogManager::~LogManager()
{
}

void LogManager::updateOperationMode()
{
    boost::unique_lock<boost::recursive_mutex> lock(m_opmode_mtx);
    m_opmode_bOutputToConsole    = PPConfig::instance().get<bool>("debug.bOutputToConsole");
    m_opmode_bRecordLog          = PPConfig::instance().get<bool>("debug.bRecordLog");
}

void LogManager::clear()
{
    boost::unique_lock<boost::recursive_mutex> lock(m_items_mtx);
    auto count = m_items.size();
    m_items.clear();
    m_items.resize(count);
    m_items_wp = 0;
    m_items_wrapped = false;
}

void LogManager::add(EType type,const std::string& file, const std::string& func, int line, const std::string& what)
{
    bool bOutputToConsole;
    bool bRecordLog;
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_opmode_mtx);
        bOutputToConsole    = m_opmode_bOutputToConsole;
        bRecordLog          = m_opmode_bRecordLog;
    }
    
    item_sp item = boost::make_shared<item_t>();
    item->when = boost::posix_time::microsec_clock::local_time();
    item->type = type;
    item->file = file;
    item->function = func;
    item->line = line;
    item->what = what;
    item->threadid = ::pthread_mach_thread_np(::pthread_self());
    if(bRecordLog){
        if(type != EType::Debug){
            boost::unique_lock<boost::recursive_mutex> lock(m_items_mtx);
            m_items[m_items_wp] = item;
            m_items_wp++;
            if(m_items_wp >= m_items.size()){
                m_items_wp = 0;
                m_items_wrapped = true;
            }
        }
    }
    if(bOutputToConsole){
        {
            boost::unique_lock<boost::recursive_mutex> lock(m_display_queue_mtx);
            m_display_queue.push(item);
        }
#if !defined(DEBUG)
        BaseSystem::instance().post([=](){
#endif
            item_sp item;
            {
                boost::unique_lock<boost::recursive_mutex> lock(m_display_queue_mtx);
                item = m_display_queue.front();
                m_display_queue.pop();
            }
            {
                boost::unique_lock<boost::recursive_mutex> lock(m_display_mtx);
                std::fputs(item->toString(true).c_str(), stderr);
                std::fputc('\n', stderr);
            }
#if !defined(DEBUG)
        });
#endif
    }
    
    if(bRecordLog){
        if(type != EType::Debug){
            {
                boost::unique_lock<boost::recursive_mutex> lock(m_file_queue_mtx);
                m_file_queue.push(item);
            }
            BaseSystem::instance().post([=](){
                item_sp item;
                {
                    boost::unique_lock<boost::recursive_mutex> lock(m_file_queue_mtx);
                    if(m_file_queue.size() == 0) return;    /* ファイルは外部で一気にダンプする可能性あり */
                    item = m_file_queue.front();
                    m_file_queue.pop();
                }
                
                /* ここから m_file_mtx の保護区間 */
                boost::unique_lock<boost::recursive_mutex> lock(m_file_mtx);
                
                boost::filesystem::path cur_file_path;
                {
                    const auto now = std::chrono::system_clock::now();
                    const auto ctime = std::chrono::system_clock::to_time_t(now);
                    std::stringstream ss;
                    ss << std::put_time(std::localtime(&ctime), "%Y%m%d") << ".txt";
                    cur_file_path = m_log_directory / ss.str();
                }
                
                if(cur_file_path != m_file_path){
                    m_file_path = cur_file_path;
                }
                
                {
                    std::ofstream os(m_file_path.string(), std::ios::app);
                    os << item->toString(false) << "\n";
                }
            });
        }
    }
}

void LogManager::deleteOldFiles()
{
    LogManager_Function();
    
    /* ここから m_file_mtx の保護区間 */
    boost::unique_lock<boost::recursive_mutex> lock_file(m_file_mtx);

    auto files = collectLogFiles();
    LogManager_AddInformationF("total log file count = %d", files.size());

    std::vector<std::string> removeFiles;
    if(files.size() > m_log_max_files){
        std::sort(files.rbegin(), files.rend());
        while(files.size() > m_log_max_files){
            removeFiles.push_back(files.back());
            files.pop_back();
        }
    }

    LogManager_AddInformationF("total log file remove count = %d", removeFiles.size());

    if(removeFiles.size() > 0){
        for(auto file : removeFiles){
            bool bSuccess = boost::filesystem::remove(m_log_directory / file);
            if(bSuccess){
                LogManager_AddInformationF("delete %s successful.", file);
            }
            else{
                LogManager_AddErrorF("delete %s failue.", file);
            }
        }
    }
    
    BaseSystem::instance().postWithTimeout(60 * 60 * 24, [=](){
        deleteOldFiles();
    });
}

std::vector<std::string> LogManager::collectLogFiles()
{
    boost::regex expr("^[0-9]{8}\\.txt$" );
    std::vector<std::string> files;
    for(
        auto it = boost::filesystem::directory_iterator(m_log_directory);
        it != boost::filesystem::directory_iterator();
        it++
        ){
        auto&& fname = it->path().filename().string();
        if(boost::regex_match(fname, expr)){
            files.push_back(fname);
        }
    }
    return files;
}

void LogManager::deleteLogFileAll()
{
    auto files = collectLogFiles();
    for(auto it : files){
        boost::filesystem::remove(m_log_directory / files.back());
    }
}


void LogManager::flushLogFile()
{
    /* ここから m_file_mtx の保護区間 */
    boost::unique_lock<boost::recursive_mutex> lock_file(m_file_mtx);
    
    boost::filesystem::path cur_file_path;
    {
        const auto now = boost::chrono::system_clock::now();
        const auto ctime = boost::chrono::system_clock::to_time_t(now);
        std::stringstream ss;
        ss << std::put_time(std::localtime(&ctime), "%Y%m%d") << ".txt";
        cur_file_path = m_log_directory / ss.str();
    }
    
    std::ofstream os(cur_file_path.string(), std::ios::app);
    
    /* ここから m_file_queue の保護区間 */
    boost::unique_lock<boost::recursive_mutex> lock_file_queue(m_file_queue_mtx);
    while(m_file_queue.size() > 0){
        auto item = m_file_queue.front();
        os << item->toString(false) << "\n";
        m_file_queue.pop();
    }
}

void LogManager::archive(const boost::filesystem::path& dest)
{
    flushLogFile();
    ZipFileCompressor zip;
    zip.open(dest.string());
    auto files = collectLogFiles();
    for(auto file : files){
        zip.add((m_log_directory / file).string(), file);
    }
    zip.close();
}

std::string LogManager::item_t::toString(bool bOmitDate)
{
    std::string s;
    std::stringstream ss;
    
    std::auto_ptr<boost::posix_time::time_facet> facet
    (
     new boost::posix_time::time_facet
     (
      bOmitDate ? "%H:%M:%S.%f" : "%Y/%m/%d %H:%M:%S.%f"
      )
     );
    
    ss.imbue(std::locale(ss.getloc(), facet.release()));
    {
        
        boost::unique_lock<boost::recursive_mutex> lock(mtx);
        std::string t;
        switch(type){
            case EType::Error:       { t = "ERR"; break; }
            case EType::Warning:     { t = "WRN"; break; }
            case EType::Information: { t = "INF"; break; }
            case EType::Debug:       { t = "DBG"; break; }
            case EType::ScopeIn:     { t = "S->"; break; }
            case EType::ScopeOut:    { t = "<-S"; break; }
            default:
            case EType::Bad:         { t = "---"; break; }
        }
        boost::filesystem::path fp(file);
        std::string w;
        if(type == EType::ScopeOut){
            w = (boost::format("%s(-)/%s") % fp.filename().string() % function).str();
        }
        else{
            w = (boost::format("%s(%d)/%s") % fp.filename().string() % line % function).str();
        }
        
        ss << when;
        
        auto _trimWhat = [&](size_t n){
            if(type == EType::Error) return what;
            if(type == EType::Debug) return what;
            if(what.find("\t") == 0){
                return what.substr(1);    /* タブから始まるログは全て記録する */
            }
            if(what.size() < n) return what;
            return what.substr(0, (n - 10)) + " ... " + what.substr(what.size() - 4);
        };

        s = (
             boost::format("%s [%s] (%d) %s : %s")
             % ss.str()
             % t
             % threadid
             % w
             % _trimWhat(512)
             ).str();
    }
    
    return s;
}


void LogManager::dump(std::ostream& os)
{
    boost::unique_lock<boost::recursive_mutex> lock(m_items_mtx);
    if(m_items_wrapped){
        for(size_t i = m_items_wp; i < m_items.size(); i++){
            os << m_items[i]->toString(false) << '\n';
        }
    }
    for(size_t i = 0; i < m_items_wp; i++){
        os << m_items[i]->toString(false) << '\n';
    }
}


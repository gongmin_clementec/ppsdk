//
//  JobNFCIntermediateTotalJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobNFCIntermediateTotalJMups__)
#define __h_JobNFCIntermediateTotalJMups__

#include "PaymentJob.h"
#include "JMupsAccessJournal.h"

class JobNFCIntermediateTotalJMups :
    public PaymentJob,
    public JMupsAccessJournal
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobNFCIntermediateTotalJMups(const __secret& s);
    virtual ~JobNFCIntermediateTotalJMups();
    
    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCIntermediateTotalJMups__) */

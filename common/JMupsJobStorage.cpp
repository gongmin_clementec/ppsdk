//
//  JMupsResponces.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsJobStorage.h"

JMupsJobStorage::JMupsJobStorage()
    : m_bTrainingModePinInputted(false)
{
    LogManager_Function();
}

/* virtual */
JMupsJobStorage::~JMupsJobStorage()
{
    LogManager_Function();
}


/* virtual */
void JMupsJobStorage::serialize(std::ostream& os) const
{
}

/* virtual */
void JMupsJobStorage::deserialize(std::istream& is)
{
}

void JMupsJobStorage::saveCurrentHpsUrl(const std::string hpsUrl)
{
    std::lock_guard<std::mutex> guard(m_hpsUrl_mtx);
    m_vHpsUrl.push_back(hpsUrl);
}

void JMupsJobStorage::getCurrentHpsUrl(std::string& hpsUrl)
{
    std::lock_guard<std::mutex> guard(m_hpsUrl_mtx);
    if(!m_vHpsUrl.empty()){
        hpsUrl = m_vHpsUrl.back();
        m_vHpsUrl.clear();
    }
    else{
        hpsUrl = "";
    }
}

//
//  JMupsAccessJournal.h
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//



#if !defined(__h_JMupsAccessJournal__)
#define __h_JMupsAccessJournal__

#include "JMupsAccess.h"

class JMupsAccessJournal :
    virtual public JMupsAccess
{
private:
    
protected:
    struct prmJournalCommon
    {
        enum class detailOption
        {
            Bad,
            Summary,
            Detail
        };
        static detailOption stringToDetailOption(const std::string& str);
        
        enum class paymentType
        {
            Bad,
            Credit,
            NFC,
            UnionPay,
            All
        };
        static paymentType stringToPaymentType(const std::string& str);

        enum class reprint
        {
            Bad,
            No,
            Last,
            BeforeLast
        };
        static reprint stringToReprint(const std::string& str);
    };
    
    auto hpsDailyJournal
    (
     prmJournalCommon::paymentType  paymentType,
     prmJournalCommon::reprint      reprint,
     prmJournalCommon::detailOption detailOption
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsDailyJournalApply
    (
     prmJournalCommon::paymentType  paymentType,
     const std::string&             outputDate
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsImJournal
    (
     prmJournalCommon::paymentType  paymentType,
     prmJournalCommon::detailOption detailOption
    ) -> pprx::observable<JMupsAccess::response_t>;

    
    
public:
            JMupsAccessJournal() = default;
    virtual ~JMupsAccessJournal() = default;
    
};

#endif /* !defined(__h_JMupsAccessJournal__) */

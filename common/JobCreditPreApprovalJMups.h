//
//  JobCreditPreApprovalJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobCreditPreApprovalJMups__)
#define __h_JobCreditPreApprovalJMups__

#include "JobCreditSalesJMups.h"

class JobCreditPreApprovalJMups :
    public JobCreditSalesJMups
{
private:

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;

    virtual prmCommon::operationDiv getOperationDiv() const;

    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual auto processDcc() -> pprx::observable<pprx::unit>;
    virtual auto sendTradeConfirm(prmCredit::doubleTrade selector)  -> pprx::observable<JMupsAccess::response_t>;
    virtual auto waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData) -> pprx::observable<prmCredit::doubleTrade>;
    virtual auto sendTagging(prmCredit::doubleTrade selector) -> pprx::observable<JMupsAccess::response_t>;

    virtual void beginAsyncPreInputAmount();
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;

public:
            JobCreditPreApprovalJMups(const __secret& s);
    virtual ~JobCreditPreApprovalJMups();
};



#endif /* !defined(__h_JobCreditPreApprovalJMups__) */

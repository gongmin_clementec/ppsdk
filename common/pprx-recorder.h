//
//  pprx-recorder.h
//  ppsdk
//
//  Created by tel on 2017/11/15.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_pprx_recorder__)
#define __h_pprx_recorder__

#include "PPReactive.h"
#include <boost/any.hpp>

namespace pprx {
    template <typename T, typename C> class recorder
    {
    private:
        using value_t = T;
        using coord_t = C;
        enum class emethod {
            on_next,
            on_error,
            on_completed
        };
        struct method_value_t{
            emethod     method;
            boost::any  value;
        };
        using subject_t = pprx::subjects::replay<method_value_t, coord_t>;
        subject_t   m_replay;
        coord_t     m_coord;
        
    public:
        recorder(coord_t coord) : m_replay(coord), m_coord(coord) {}
        virtual ~recorder()
        {
            m_replay.get_subscriber().on_completed();
        }
        
        void reset()
        {
            m_replay.get_subscriber().on_completed();
            m_replay = subject_t(m_coord);
        }
        
        void on_next(value_t value)
        {
            m_replay.get_subscriber().on_next(method_value_t{emethod::on_next, std::move(value)});
        }
        void on_error(std::exception_ptr error)
        {
            m_replay.get_subscriber().on_next(method_value_t{emethod::on_error, error});
        }
        void on_completed()
        {
            m_replay.get_subscriber().on_next(method_value_t{emethod::on_completed, boost::none});
        }
        
        auto player() -> pprx::observable<value_t>
        {
            return pprx::observable<>::create<value_t>([=](pprx::subscriber<value_t> s){
                m_replay.get_observable()
                .subscribe([=](auto mv){
                    switch(mv.method){
                        case emethod::on_next:
                            s.on_next(boost::any_cast<value_t>(mv.value));
                            break;
                        case emethod::on_error:
                            s.on_error(boost::any_cast<std::exception_ptr>(mv.value));
                            break;
                        case emethod::on_completed:
                            s.on_completed();
                            break;
                    }
                });
            });
        }
    };
}

#endif /* !defined(__h_pprx_recorder__) */

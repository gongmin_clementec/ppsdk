//
//  JMupsAccessUnionPay.hpp
//  ppsdk
//
//  Created by tel on 2017/06/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JMupsAccessUnionPay__)
#define __h_JMupsAccessUnionPay__

#include "JMupsAccess.h"

class JMupsAccessUnionPay :
virtual public JMupsAccess
{
private:
    
protected:
    struct prmUnionPay
    {
        enum class doubleTrade
        {
            Bad,
            No,
            Yes
        };
        
        enum class payWayDiv {
            Bad,
            Lump
        };
        
        enum class printResultDiv {
            Bad,
            Success,
            Failure
        };
        
        enum class isSendingDesiSlip {
            Bad,
            ShoudBeSending, /* 送信対象伝票（カード会社控未印字） */
            Unnecessarily   /* 送信対象外伝票（カード会社控印字済） */
        };
    };
    
    /* 売上取引確認 */
    auto hpsUnionPaySales_Confirm
    (
     prmUnionPay::doubleTrade selector,
     int                    amount,
     boost::optional<std::string>   authenticationNo,
     bool                   isRePrint,
     boost::optional<int>   couponSelectListNum,
     boost::optional<int>   couponApplyPreviousAmount,
     bool                   pinRetryFlg,
     boost::optional<std::string> firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    /* 売上印字結果送信 */
    auto hpsUnionPaySales_Tagging
    (
     prmUnionPay::doubleTrade      selector,
     const std::string&            slipNo,
     prmUnionPay::printResultDiv   printResultDiv,
     boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip,
     bool                          isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
     /* オーソリ予約取引確認 */
    auto hpsUnionPayPreApproval_Confirm
    (
     prmUnionPay::doubleTrade       selector,
     int                            amount,
     boost::optional<std::string>   authenticationNo,
     bool                           pinRetryFlg,
     boost::optional<std::string>   firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* オーソリ予約印字結果送信 */
    auto hpsUnionPayPreApproval_Tagging
    (
     prmUnionPay::doubleTrade   selector,
     const std::string&         slipNo,
     prmUnionPay::printResultDiv  printResultDiv
     ) -> pprx::observable<JMupsAccess::response_t>;
    
     /* 承認後売上取引確認 */
    auto hpsUnionPayAfterApproval_Confirm
    (
     prmUnionPay::doubleTrade selector,
     const std::string&     approvalNo,
     const std::string&     cupNo,
     const std::string&     cupSendDate,
     int                    amount,
     bool                   isRePrint,
     boost::optional<std::string>   authenticationNo,
     bool                   pinRetryFlg,
     boost::optional<std::string> firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    /* 承認後売上印字結果送信 */
    auto hpsUnionPayAfterApproval_Tagging
    (
     prmUnionPay::doubleTrade      selector,
     const std::string&            slipNo,
     prmUnionPay::printResultDiv   printResultDiv,
     boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip,
     bool                          isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* 取消返品-取消対象取引検索 */
    auto hpsUnionPayRefund_SearchTrade
    (
     const std::string&     slipNo
    ) -> pprx::observable<JMupsAccess::response_t>;

    /* 取消返品取引確認 */
    auto hpsUnionPayRefund_Confirm
    (
     int                    amount,
     const std::string&     slipNo,
     const std::string&     approvalNo,
     const std::string&     cupNo,
     const std::string&     cupSendDate,
     const std::string&     cupTradeDiv,
     boost::optional<std::string>   authenticationNo,
     bool                           pinRetryFlg,
     boost::optional<std::string>   firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* 取消返品印字結果送信 */
    auto hpsUnionPayRefund_Tagging
    (
     const std::string&         slipNo,
     prmUnionPay::printResultDiv  printResultDiv,
     boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* オーソリ予約-取消対象取引検索 */
    auto hpsUnionPayPreApprovalCancel_SearchTrade
    (
     const std::string&     slipNo
    ) -> pprx::observable<JMupsAccess::response_t>;    

     /* オーソリ予約取消取引確認 */
    auto hpsUnionPayPreApprovalCancel_Confirm
    (
     int                    amount,
     const std::string&     slipNo,
     const std::string&     approvalNo,
     const std::string&     cupNo,
     const std::string&     cupSendDate,
     boost::optional<std::string>   authenticationNo,
     bool                           pinRetryFlg,
     boost::optional<std::string>   firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* オーソリ予約取消印字結果送信 */
    auto hpsUnionPayPreApprovalCancel_Tagging
    (
     const std::string&         slipNo,
     prmUnionPay::printResultDiv  printResultDiv,
     boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* 承認後売上取消返品-取消対象取引検索 */
    auto hpsUnionPayAfterApprovalCancel_SearchTrade
    (
     const std::string&     slipNo
    ) -> pprx::observable<JMupsAccess::response_t>;    

     /* 承認後売上取消返品取引確認 */
    auto hpsUnionPayAfterApprovalCancel_Confirm
    (
     int                    amount,
     const std::string&     slipNo,
     const std::string&     approvalNo,
     const std::string&     cupNo,
     const std::string&     cupSendDate,
     const std::string&     cupTradeDiv,
     boost::optional<std::string>   authenticationNo,
     bool                           pinRetryFlg,
     boost::optional<std::string>   firstGACCmdRes
     ) -> pprx::observable<JMupsAccess::response_t>;

    /* 承認後売上取消返品印字結果送信 */
    auto hpsUnionPayAfterApprovalCancel_Tagging
    (
     const std::string&         slipNo,
     prmUnionPay::printResultDiv  printResultDiv,
     boost::optional<prmUnionPay::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

public:
    JMupsAccessUnionPay();
    virtual ~JMupsAccessUnionPay();
    
};

#endif /* !defined(__h_JMupsAccessUnionPay__) */


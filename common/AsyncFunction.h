//
//  Header.h
//  ppsdk
//
//  Created by tel on 2017/06/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#ifndef Header_h
#define Header_h

#include <boost/any.hpp>
#include <algorithm>
#include "BaseSystem.h"
#include "AtomicObject.h"

class AsyncFunction
{
public:
    using onSuccess_t   = boost::function<void(const_json_sp)>;
    using onError_t     = boost::function<void(const_json_sp)>;
    using onProgress_t  = boost::function<bool(const_json_sp)>;
    
private:
    onSuccess_t         m_onSuccess;
    onError_t           m_onError;
    onProgress_t        m_onProgress;
    AtomicObject<bool>  m_bCalled;
    
    void clearCallbacks()
    {
        m_reactFunctions.clear();
        m_onSuccess     = onSuccess_t();
        m_onError       = onError_t();
        m_onProgress    = onProgress_t();
    }
    
    std::vector<std::string>    m_reactFunctions;
    
    bool inReactFunction(std::string functionName) const
    {
        return std::find(m_reactFunctions.begin(), m_reactFunctions.end(), functionName) != m_reactFunctions.end();
    }
    
    void asyncOnError(const std::string& functionName, const_json_sp result = const_json_sp())
    {
        if(!inReactFunction(functionName)) return;
        auto func = m_onError;
        if(func){
            clearCallbacks();
            func(result);
        }
    }

protected:
    void asyncPrepareCallbacks
    (
     const std::vector<std::string>&    reactFunctions,
     onSuccess_t    onSuccess   = onSuccess_t(),
     onError_t      onError     = onError_t(),
     onProgress_t   onProgress  = onProgress_t()
     )
    {
        m_bCalled           = false;
        m_reactFunctions    = reactFunctions;
        m_onSuccess         = onSuccess;
        m_onError           = onError;
        m_onProgress        = onProgress;
    }

    bool checkCalledAndReactAtmoically(const std::string& asyncOnXXX, const std::string& functionName)
    {
        bool bExecute = true;
        m_bCalled.modifyBlock([&](bool& bCalled){
            if(bCalled){
                LogManager_AddWarningF("already called %s('%s') -> cancel", asyncOnXXX % functionName);
                bExecute = false;
            }
            else{
                if(!inReactFunction(functionName)){
                    LogManager_AddWarningF("inReactFunction is false. %s('%s') -> cancel", asyncOnXXX % functionName);
                    bExecute = false;
                }
                else{
                    bExecute = true;
                    bCalled = true;
                }
            }
        });
        return bExecute;
    }
    
    void asyncOnSuccess(const std::string& functionName, const_json_sp result = const_json_sp())
    {
        if(!checkCalledAndReactAtmoically("asyncOnSuccess", functionName)) return;
        
        auto func = m_onSuccess;
        if(func){
            clearCallbacks();
            LogManager_AddErrorF("functionName : %s", functionName);
            if(result){
                LogManager_AddInformation(BaseSystem::jsonToText(*result));
            }
            else{
                LogManager_AddInformation("no result");
            }
            func(result);
        }
        else{
            LogManager_AddWarning("empty function.");
        }
    }
    
    void asyncOnError(const std::string& functionName, const std::string& where, const std::string& what, const boost::optional<int> code = boost::optional<int>())
    {
        if(!checkCalledAndReactAtmoically("asyncOnError", functionName)) return;
        
        auto func = m_onError;
        if(func){
            clearCallbacks();
            auto result = boost::make_shared<json_t>();
            result->put("where", where);
            result->put("detail.description", what);
            if(code){
                result->put("detail.code", *code);
            }
            LogManager_AddErrorF("functionName : %s", functionName);
            LogManager_AddError(BaseSystem::jsonToText(*result));
            func(result);
        }
        else{
            LogManager_AddWarning("empty function.");
        }
    }

    
    
    bool asyncOnProgress(const std::string& functionName, const_json_sp result = const_json_sp())
    {
        if(!inReactFunction(functionName)) return false;
        if(!m_onProgress) return false;
        return m_onProgress(result);
    }
    
public:
            AsyncFunction() = default;
    virtual ~AsyncFunction() = default;
};


#endif /* Header_h */

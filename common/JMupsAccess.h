//
//  JMupsAccess.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JMupsAccess__)
#define __h_JMupsAccess__

#include "PPReactive.h"

class JMupsJobStorage;
class WebClient;

class JMupsAccess
{
public:
    struct response_t
    {
        const_json_t header;
        const_json_t body;
    };
        
private:
    std::string m_basePath;
    std::mutex  m_hpsUrl_mtx;
    std::vector<std::string> m_vHpsUrl;
    
    boost::shared_ptr<WebClient> m_webClient;
    json_t     m_tradeStartInfo;

private:
    void setCurrentHpsUrl(const std::string hpsUrl);
    
protected:
    JMupsAccess();
    virtual ~JMupsAccess();
    
    void setUrlForActivation();
    
    auto callHps(const std::string& path, const_json_t json) -> pprx::observable<response_t>;

    
    auto downloadHps(const std::string& path, const_json_t json, const boost::filesystem::path& file)
    -> pprx::observable<pprx::unit>;
    
    auto callTraining(const std::string& path, const keyvalue_t& replaces) -> pprx::observable<response_t>;
    
    struct prmCommon {
        enum class mposDeviceType { Bad, iSMP, M010 };
        enum class digiReceiptDiv {
            Bad,
            DigiSlip,              /* 電子伝票保管対象（サイン要） */
            DigiSlipWithoutSign,   /* 電子伝票保管対象（サイン不要） */
            PaperSlip              /* 電子伝票保管中止（手書きサイン） */
        };
        
        enum class operationDiv {
            Bad,
            Sales,
            Refund,
            PreApproval,
            AfterApproval,
            CancelPreApproval,
            CancelAfterApproval,
            CardCheck
        };
    };
    

    struct prmOpenTerminal_Request {
        enum class nfcDeviceType { Bad, Panasonic, Others };
    };

    auto hpsOpenTerminal_Request
    (
     prmOpenTerminal_Request::nfcDeviceType nfcDeviceType
     ) -> pprx::observable<JMupsAccess::response_t>;

    struct prmTradeStart_Request {
        enum class serviceDiv {
            Bad,
            CreditCard,
            UnionPay,
            Suica,
            JDebit,
            SalesTagging,
            Point,
            Gift,
            iD,
            QuicPay,
            Edy,
            Waon,
            Nanaco,
            Nfc
        };
        enum class nfcDeviceType { Bad, Panasonic, Miura };
    };
        
    auto hpsTradeStart_Request
    (
     prmTradeStart_Request::serviceDiv       serviceDiv,
     boost::optional<std::string>            deal,
     boost::optional<prmTradeStart_Request::nfcDeviceType>    nfcDeviceType
    ) -> pprx::observable<JMupsAccess::response_t>;

    
    struct prmMSCardRead
    {
        enum class settlementMediaDiv {
            Bad,
            Credit,
            UnionPay,
            JDebit,
            Point,
            Gift,
            CheckCoupon
        };
    };

    auto hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv    settlementMediaDiv,
     prmCommon::operationDiv              operationDiv,
     bool                                 isMSFallback,
     bool                                 isFallback,
     const std::string&                   keySerialNo,
     boost::optional<std::string>         cryptData,
     boost::optional<std::string>         cryptDataJIS1Track1,
     boost::optional<std::string>         cryptDataJIS1Track2,
     boost::optional<std::string>         cryptDataJIS2,
     boost::optional<std::string>         cryptDataPattern,
     boost::optional<std::string>         mSFormatPattern
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsSendDigiSignForCredit
    (
     const std::string&             slipNo,
     const std::string&             signData
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsSendDigiSignForUnionPay
    (
     const std::string&             slipNo,
     const std::string&             signData
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsStoreDigiSignForCredit
    (
     const std::string&             slipNo,
     prmCommon::digiReceiptDiv      digiReceiptDiv,
     boost::optional<std::string>   signData
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsStoreDigiSignForUnionPay
    (
     const std::string&             slipNo,
     prmCommon::digiReceiptDiv      digiReceiptDiv,
     boost::optional<std::string>   signData
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsAccessFromEmvKernel
    (
     const std::string& url,
     const_json_t      postParam,
     const bool        bCredit=true
     ) -> pprx::observable<JMupsAccess::response_t>;
        
    template <class T> void setJsonParam(
        json_t&                         ioJson,
        const std::string&              key,
        const T&                        value,
        const std::map<T,std::string>&  mapping) const
    {
        auto it = mapping.find(value);
        if(it == mapping.end()) return;
        ioJson.put(key, it->second);
    }
    
    template <class T> void setJsonParam(
         json_t&                         ioJson,
         const std::string&              key,
         const boost::optional<T>&       value) const
    {
        if(!value) return;
        auto v = boost::lexical_cast<std::string>(value.get());
        
        if(typeid(T)==typeid(bool)){
            ioJson.put(key, v == "0" ? "false" : "true");
        }
        else{
            ioJson.put(key, v);
        }
    }
                                         
    template <class T> void setJsonParam(
         json_t&                         ioJson,
         const std::string&              key,
         const T&                        value) const
    {
        auto v = boost::lexical_cast<std::string>(value);
        
        if(typeid(T)==typeid(bool)){
            ioJson.put(key, v == "0" ? "false" : "true");
        }
        else{
            ioJson.put(key, v);
        }
    }
    
    enum hpsResultState
    {
        Bad,
        Success,
        Error,
        Fatal,
        Warning
    };
    
    hpsResultState  getHpsResultState(const_json_t json) const;
    std::string     getHpsResultCode(const_json_t json) const;
    bool isRetryCardReadWarning(const_json_t json, std::string& readerDisplayKey );

    bool    isTrainingMode() const;
    bool    isUseDigiSignInTrainingMode() const;
    
    boost::shared_ptr<JMupsJobStorage>         getJMupsStorage();
    const boost::shared_ptr<JMupsJobStorage>   getJMupsStorage() const;
    
    const_json_t getTerminalInfo() const;
    const_json_t getNfcMasterData() const;
    const_json_t getCookies() const;

    void setCookies(const_json_t cookies);
    void setCookie(const std::string& key, const std::string& value);
    
    std::string integerToYenStringInJson(int value) const;

public:
    const_json_t getTradeStartInfo() const { return m_tradeStartInfo; }
};


#endif /* !defined(__h_JMupsAccess__) */

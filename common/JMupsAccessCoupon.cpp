//
//  JMupsAccessCoupon.cpp
//  ppsdk
//
//  Created by tel on 2017/06/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessCoupon.h"

pprx::observable<JMupsAccess::response_t> JMupsAccessCoupon::hpsCouponSalesAutoDiscount_communication
(
 int                amount,
 prmCouponSalesAutoDiscount_communication::serviceDiv serviceDiv
 )
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount", amount);
    
    setJsonParam(json, "serviceDiv", serviceDiv,
                 {
                     {prmCouponSalesAutoDiscount_communication::serviceDiv::Credit      , "0"},
                     {prmCouponSalesAutoDiscount_communication::serviceDiv::UnionPay    , "1"}
                 });
    
    if(isTrainingMode()){
        return callTraining("cn0101.do?cN0101=aaa", keyvalue_t());
    }
    else{
        return callHps("cn0101.do?cN0101=aaa", json);
    }
}


const_json_t JMupsAccessCoupon::findCoupon(const_json_t json, const std::string& couponId) const
{
    do{
        auto printInfoList = json.get_optional<json_t::array_t>("normalObject.printInfoList");
        if(!printInfoList) break;
        
        for(auto printInfo : *printInfoList){
            if(couponId == printInfo.get<std::string>("printInfo.COUPON_ID")){
                return printInfo.get("printInfo");
            }
        }
    } while(false);
    
    return const_json_t();
}

boost::optional<int> JMupsAccessCoupon::getCouponSelectListNum(const_json_t json, const std::string& couponId) const
{
    do{
        auto printInfoList = json.get_optional<json_t::array_t>("normalObject.printInfoList");
        if(!printInfoList) break;
        
        int i = 0;
        for(auto printInfo : *printInfoList){
            if(couponId == printInfo.get<std::string>("printInfo.COUPON_ID")){
                return i;
            }
            i++;
        }
    } while(false);
    
    return boost::none;
}

//
//  JobCreditSalesJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobCreditSalesJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"
#include "JMupsAccessNFC.h"

/* static */
const std::vector<const std::string> JobCreditSalesJMups::s_paymentWayProperties =
{
    "partitionTimes","firsttimeClaimMonth","bonusTimes",
    "bonusMonth1","bonusAmount1",
    "bonusMonth2","bonusAmount2",
    "bonusMonth3","bonusAmount3",
    "bonusMonth4","bonusAmount4",
    "bonusMonth5","bonusAmount5",
    "bonusMonth6","bonusAmount6"
};

JobCreditSalesJMups::JobCreditSalesJMups(const __secret& s) :
    PaymentJob(s),
    m_cardType(CardType::Bad),
    m_bIsMSFallback(false),
    JMupsEmvKernel(JMupsEmvKernel::Type::Credit),
    m_emvAmountPreInputUI(UIBehaviorState::Bad)
{
    LogManager_Function();
}

/* virtual */
JobCreditSalesJMups::~JobCreditSalesJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobCreditSalesJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto) {
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForReaderConnect().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return createChildJob("JMups.NFCSales")
        .flat_map([=](auto) {
            return m_childJob->initializeChildJob();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto) {
        beginAsyncPreInputAmount();
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto){
        return syncEmvAmountPreInputUI();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.credit").clone();
        setValidChildJob(true);
        return processCardJob(cardReaderOption.as_readonly(), true );
    }).as_dynamic()
    
    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging(m_taggingSelector).as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

auto JobCreditSalesJMups::processCardJob(const_json_t cardReaderOption, bool bThreeSides)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    m_bIsMSFallback = false;
    auto strReaderDisplayTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_APPROVED");
    if(bThreeSides){
        strReaderDisplayTxt = "";
    }
    auto cardJobProcess = pprx::subjects::behavior<std::string>(strReaderDisplayTxt);

    return pprx::just(pprx::unit()).as_dynamic()
    .flat_map([=](auto){
        auto childParam = boost::make_shared<json_t>(createChildJobParam().clone());
        return cardJobProcess.get_observable()
        .flat_map([=](std::string strTxt) {
            /* カードリーダーの接続待ち */
            json_t param = cardReaderOption.clone();
            param.put("childParam", *childParam);
            return waitForReaderConnectAndReadMedia(param.as_readonly(), strTxt).as_dynamic();
        }).as_dynamic()
        .flat_map([=](const_json_t data){
            /* カード種ごとの読み込み（磁気・IC・NFC）と後続のJMups関連の処理 */
            json_t param = data.clone();
            param.put("childParam", *childParam);
            return processCardByCard(param).as_dynamic();
        }).as_dynamic()
        /*
            IC業務から磁気フォールバックする場合、カード抜去が発生するため、
            ファールバックエラー（警告）が発生したのちのカード抜去エラーを
            フィルタするため、take(1) する。
        */
        .flat_map([=](auto success){
            return pprx::maybe::success(success);
        }).as_dynamic()
        .on_error_resume_next([=](std::exception_ptr err){
            return pprx::maybe::error(err);
        }).as_dynamic()
        .take(1)
        .flat_map([=](pprx::maybe m){
            return m.observableForContinue<pprx::unit>();
        }).as_dynamic()
        .flat_map([=](auto success){
            return pprx::maybe::success(success);
        }).as_dynamic()
        .on_error_resume_next([=](std::exception_ptr err){
            /* フォールバックの場合にはリトライを行う */
            std::string strReaderKey("");
            try{std::rethrow_exception(err);}
            catch(pprx::ppex_paymentserver& perr){
                auto errjson = perr.json().get("result");
                if(isRetryCardReadWarning(errjson,strReaderKey)){
                    return getCardReader()->displayTextNoLf(getDisplayMsgForReader(strReaderKey)).as_dynamic()
                    .flat_map([=](auto){
                        return callOnDispatchWithCommandAndSubParameter("waitForRetryCardRead", "", perr.json())
                        .flat_map([=](auto){
                            std::string strReaderTxt("");
                            if(strReaderKey == "MIURA_CREDIT_UI_CARD_INSERT_ERROR"){
                                strReaderTxt = "";
                            }
                            else if(strReaderKey == "MIURA_CREDIT_UI_CARD_SWIPE_ERROR"){
                                m_bIsMSFallback = true;
                                strReaderTxt = "";
                            }
                            else{
                                strReaderTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_APPROVED");
                            }
                            cardJobProcess.get_subscriber().on_next(strReaderTxt);
                            return pprx::maybe::retry();
                        });
                    }).as_dynamic()
                    .on_error_resume_next([=](auto err2){
                        return getCardReader()->displayTextNoLf(getDisplayMsgForReader(strReaderKey)).as_dynamic()
                        .flat_map([=](auto) {
                            return pprx::maybe::error(err2);
                        }).as_dynamic();
                    }).as_dynamic();
                }
            }
            catch(pprx::ppex_cardrw& perrRw){
                auto desc = perrRw.json().get_optional<std::string>("result.detail.description");
                if(desc) {
                    LogManager_AddDebugF("processCardJob. error desc = %s", *desc);
                    // NFCCardErrorはCardTypeが決まる前に発生するがdescriptionの内容でNFCであることがわかるのでここで判断する
                    if(*desc == "NFCCardError"){
                        auto code = perrRw.json().get_optional<std::string>("result.detail.code");
                        if(!code || *code != "PP_E4001") {
                            BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_DECLINED");
                        }
                    } else if(*desc == "SeePhone"){
                        return callOnDispatchWithCommand("waitForCardActive")
                        .flat_map([=](auto) {
                            return pprx::maybe::retry();
                        }).as_dynamic();
                    } else if(m_cardType == CardType::NFC) {
                        if(*desc == "declined"){
                            BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_DECLINED");
                        }
                    }
                }
                if(m_bIsMSFallback){
                    auto cardStatus = perrRw.json().get<std::string>("result.detail.description");
                    if(cardStatus == "cardExist"){
                        json_t j = perrRw.json().clone();
                        j.remove_key_if_exist("result.detail.description");
                        j.put("result.detail.errorObject.errorCode", "PP_E2004");
                        j.put("result.detail.errorObject.errorLevel", "ERROR");
                        j.put("result.detail.errorObject.errorMessage", "cardExist");
                        j.put("result.detail.errorObject.errorType", "ERROR");
                        j.put("result.detail.resultCode", "1");
                        
                        return callOnDispatchWithCommandAndSubParameter("waitForMSFBCardExist", "", j)
                        .flat_map([=](const_json_t resp){
                            const bool bRetry = resp.get<bool>("bRetry");
                            return bRetry ? pprx::maybe::retry() : pprx::maybe::error(make_error_aborted("aborted")).as_dynamic();
                        }).as_dynamic()
                        .on_error_resume_next([=](auto err){
                            LogManager_AddDebug(pprx::exceptionToString(err));
                            return pprx::maybe::error(err);
                        }).as_dynamic();
                    }
                }
            }
            catch(...){
            }
            return pprx::maybe::error(err);
        }).as_dynamic()
        .retry()
        .flat_map([=](pprx::maybe m){
            return m.observableForContinue<pprx::unit>();
        }).as_dynamic()
        .take(1);
    }).as_dynamic();
}

auto JobCreditSalesJMups::treatSlipDocumentJson()
 -> pprx::observable<const_json_t>
{
    // NFCで決済していた場合は子ジョブの印字データを取得し返す。
    if(isUsedChildJob()) {
        return m_childJob->getChildSlipDocument();
    }

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto pinfo = tradeResultData.get("normalObject.printInfo.device.value").clone();
    auto values = pinfo.get<json_t::array_t>("values");
    auto types = pinfo.get<json_t::array_t>("type");

    /* 税・その他が不要な場合削除する　*/
    {
        auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
        if(!taxOtherAmount){
            for(auto v : values){
                v.remove_key_if_exist("TAX");
            }
        }
    }

    /* カード会社控えが不要な場合削除する。必要なら、サインする */
    {
        auto isNeedAquirerSlip = tradeResultData.get_optional<bool>("ppsdk.isNeedAquirerSlip");
        if(isNeedAquirerSlip){
            if(!(*isNeedAquirerSlip)){
                for(int n = 0; n < values.size(); n++){
                    if(values[n].get<std::string>("RECEIPT_DIV") == "4"){
                        values.erase(values.begin() + n);
                        types.erase(types.begin() + n);
                        break;
                    }
                }
            }
            else{ // 手書きサイン
                for(auto v : values){
                    auto sign = v.get_optional<std::string>("SIGN");
                    if(sign){
                        v.put("SIGN", "1");
                    }
                }
            }
        }
    }
    
    /* クーポン情報の埋め込み */
    {
        auto&& tradeResult = tradeResultData.get("normalObject.tradeResult");
        auto COUPON_ID = tradeResult.get_optional<std::string>("COUPON_ID");
        if(COUPON_ID && (!COUPON_ID->empty())){
            auto SETTLEDAMOUNT = ::atoi(tradeResult.get<std::string>("SETTLEDAMOUNT").c_str());
            auto DISCOUNT_PRICE = tradeResult.get<int>("DISCOUNT_PRICE");
            auto AMOUNT = std::string("\\") + BaseSystem::commaSeparatedString(SETTLEDAMOUNT + DISCOUNT_PRICE);
            auto strDISCOUNT_PRICE = std::string("-\\") + BaseSystem::commaSeparatedString(::abs(DISCOUNT_PRICE));
            
            auto COUPON_MEMBER_ID       = tradeResult.get_optional("COUPON_MEMBER_ID");
            auto COUPON_POSS_TIMES      = tradeResult.get_optional("MAXIMUM_USE_POSS_TIMES");
            auto COUPON_DONE_TIMES      = tradeResult.get_optional("USE_DONE_TIMES");
            auto COUPON_1               = tradeResult.get_optional<std::string>("COUPON_1");
            auto COUPON_2               = tradeResult.get_optional<std::string>("COUPON_2");

            auto COUPON_TEXT = [=](){
                std::stringstream ss;
                if(COUPON_1) ss << (*COUPON_1);
                if(COUPON_2){
                    if(COUPON_1) ss << "\n";
                    ss << (*COUPON_2);
                }
                return ss.str();
            }();
            
            for(auto v : values){
                v.put("_COUPON", true);
                v.put("_COUPON_ID"       , (*COUPON_ID));
                v.put("AMOUNT"           , AMOUNT);    /* 元のAMOUNTを上書き */
                v.put("_DISCOUNT_PRICE"  , strDISCOUNT_PRICE);
                if(COUPON_MEMBER_ID && (!COUPON_MEMBER_ID->is_null_or_empty())){
                    auto member_id = COUPON_MEMBER_ID->get<std::string>();
                    if(!member_id.empty()){
                        v.put("_COUPON_MEMBER_ID", member_id);
                    }
                }
                if(COUPON_POSS_TIMES && (!COUPON_POSS_TIMES->is_null_or_empty())){
                    auto poss_times = COUPON_POSS_TIMES->get<int>();
                    if(poss_times > 0){
                        v.put("_COUPON_POSS_TIMES", boost::lexical_cast<std::string>(poss_times));
                        if(COUPON_DONE_TIMES && (!COUPON_DONE_TIMES->is_null_or_empty())){
                            auto done_times = COUPON_DONE_TIMES->get<int>();
                            v.put("_COUPON_DONE_TIMES", boost::lexical_cast<std::string>(done_times));
                        }
                    }
                }
                if(!COUPON_TEXT.empty()){
                    v.put("_COUPON_TEXT", COUPON_TEXT);
                }
            }
        }
    }

    /* DCC決済を選択した場合、一括以外の支払い方法は選択できない。また印字結果のPAY_DIVISIONは定義されていない。 */
    {
        auto bUseDCC = m_tradeData.get_optional<bool>("bUseDCC");
        if(bUseDCC && (*bUseDCC)){
            for(auto v : values){
                v.put("PAY_DIVISION", "一括");
            }
        }
    }
    
    /* printInfoのvalueを再構築 */
    pinfo.put("values", values);
    pinfo.put("type", types);
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobCreditSalesJMups::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

/* virtual */
auto JobCreditSalesJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

auto JobCreditSalesJMups::tradeStart() -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    return hpsTradeStart_Request
    (
     JMupsAccess::prmTradeStart_Request::serviceDiv::CreditCard,
     boost::none,
     boost::none
     )
    .flat_map([=](auto resp){
        m_tradeStartResponseHeader = resp.header.clone();
        if(PaymentJob::isTrainingMode()){
            LogManager_AddDebug("skip adjusting time. (training)");
            time_t rawtime;
            time (&rawtime);
            m_tradeStartResponseHeader.put("Date", ctime (&rawtime));
        }
        m_tradeStartResponse = resp.body.clone();
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic();
}

auto JobCreditSalesJMups::processCardByCard(const_json_t data)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(data)
    .flat_map([=](const_json_t json){
        m_cardStartData = json.clone();
        if(json.empty()){
            return pprx::error<pprx::unit>(make_error_internal("json is null or empty."))
            .as_dynamic();
        }
        auto cardType  = json.get<std::string>("cardType");
        auto status    = json.get<std::string>("status");
        
        json_t dispatchParam;
        dispatchParam.put("cardType", cardType);
        if(cardType == "contact"){
            m_cardType = CardType::IC;
            return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
            .flat_map([=](auto){
                return getCardReader()->errorOnDeviceDisconnected(pprx::unit())
                .flat_map([=](pprx::unit){
                    return getCardReader()->errorOnICCardRemoved(pprx::unit());
                }).as_dynamic()
                .flat_map([=](auto){
                    return proceedContact(json).as_dynamic();
                }).as_dynamic();
            }).take(1).as_dynamic();
        }
        else if(cardType == "magneticStripe"){
            m_cardType = CardType::MS;
            return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
            .flat_map([=](auto){
                return processMagneticStripe(json).as_dynamic();
            }).as_dynamic();
        }
        else if(cardType == "nfc"){
            m_cardType = CardType::NFC;
            return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
            .flat_map([=](auto){
                return processNfc(json).as_dynamic();
            }).as_dynamic();
        }
        
        return pprx::error<pprx::unit>(make_error_internalf("no handler. cardType=%s, status=%s", cardType % status)).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobCreditSalesJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::Sales;
}

/* virtual */
auto JobCreditSalesJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    LogManager_AddDebug(data.str());
    LogManager_AddInformation("------ data.get --------");
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    LogManager_AddInformation("------ hpsMSCardRead --------");
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::Credit,
     getOperationDiv(),
     m_bIsMSFallback,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        LogManager_AddInformation("------ flat_map([=](JMupsAccess::response_t resp){ --------");
        m_cardInfoData = resp.body.clone();

        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY")).as_dynamic()
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
                .as_dynamic();
            }).as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            return waitForKidInput()
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        LogManager_AddInformation("------ waitForTradeInfo --------");
        return waitForTradeInfo("Input").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        LogManager_AddInformation("------ dccUserOnOffDiv --------");
        auto dccUserOnOffDiv = m_cardInfoData.get_optional<std::string>("normalObject.dccUseOnOffDiv");
        if(dccUserOnOffDiv && ((*dccUserOnOffDiv) == "1")){
            return processDcc().as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        LogManager_AddInformation("------ sendTradeConfirm --------");
        return sendTradeConfirm(prmCredit::doubleTrade::No).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        LogManager_AddInformation("------ waitForConfirmDoubleTrade --------");
        return waitForConfirmDoubleTrade(jmups).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto selector){
        m_taggingSelector = selector;
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::processNfc(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return m_childJob->mainProcessChildJob(data)
    .flat_map([=](auto){
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_internal& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_paymentserver& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_cardrw& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_network& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppex_aborted& ppex){
            return pprx::error<pprx::unit>(err).as_dynamic();
        }
        catch(pprx::ppexception& ppex){
            LogManager_AddInformationF("ppexception\t%s", ppex.json().str());
        }
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        // ここまできたらNFC決済成功とみなし成功音を鳴らす
        BaseSystem::instance().soundPlay("SOUND_TRADE_RESULT_NFC_APPROVED");
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

auto JobCreditSalesJMups::waitForKidInput()
 -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    return callOnDispatchWithCommand("waitForKidInput")
    
    .flat_map([=](const_json_t resp){
        auto kid = resp.get_optional<std::string>("kid");
        if(!kid){
            return pprx::error<JMupsAccess::response_t>(make_error_internal("kid not found"))
            .as_dynamic();
        }
        return hpsKIDInput
        (
         *kid,
         getOperationDiv(),
         prmKIDInput::manualDiv::NonManual
         ).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto resp){
        return callOnDispatchWithMessage("waitForKidInput-Success")
        .flat_map([=](auto){
            return pprx::maybe::success(resp);
        });
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        try{std::rethrow_exception(err);}
        catch(pprx::ppex_paymentserver& perr){
            auto errjson = perr.json().get("result");
            if(getHpsResultState(errjson) == hpsResultState::Warning){
                return callOnDispatchWithMessage("waitForKidInput-Retry")
                .flat_map([=](auto){
                    return pprx::maybe::retry();
                }).as_dynamic()
                .on_error_resume_next([=](auto err){
                    LogManager_AddError(pprx::exceptionToString(err));
                    return pprx::maybe::error(err);
                })
                .as_dynamic();
            }
        }
        catch(...){}
        return pprx::maybe::error(err)
        .as_dynamic();
    })
    .retry()
    .flat_map([=](pprx::maybe flow){
        return flow.observableForContinue<JMupsAccess::response_t>();
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        /* KID入力の場合、カード読込時のレスポンスに情報を追加する形式となる　*/
        auto ci = resp.body.clone();
        auto no = ci.get<json_t::map_t>("normalObject");
        for(auto a : no){
            auto key = std::string("normalObject.") + a.first;
            m_cardInfoData.put(key, a.second);
        }
        return resp;
    });
}

auto JobCreditSalesJMups::getCouponData() -> pprx::observable<const_json_t>
{
    return hpsCouponSalesAutoDiscount_communication
    (
     m_tradeData.get<int>("amount"),
     prmCouponSalesAutoDiscount_communication::serviceDiv::Credit
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_couponData = resp.body.clone();
        auto arr = m_couponData.get_optional<json_t::array_t>("normalObject.printInfoList");
        if(!arr || (arr->size() == 0)){
            LogManager_AddInformation("normalObject.printInfoList is null or empty -> skip coupon");
            return pprx::just(const_json_t()).as_dynamic();
        }
        else{
            return pprx::just(resp.body).as_dynamic();
        }
    }).as_dynamic();
}

auto JobCreditSalesJMups::processDcc() -> pprx::observable<pprx::unit>
{
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    return hpsDHSLookup
    (
     m_tradeData.get<int>("amount"),
     taxOtherAmount ? (*taxOtherAmount) : 0,
     getOperationDiv(),
     false).as_dynamic()
    .flat_map([=](JMupsAccess::response_t resp){
        auto dccTradeTargetJudgeResult = resp.body.get<std::string>("normalObject.dccTradeTargetJudgeResult");
        if(dccTradeTargetJudgeResult == "0"){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        return callOnDispatchWithMessage("beforeDccDescriptionPrint").as_dynamic()
        .flat_map([=](pprx::unit){
            return slipPrinterExecute(resp.body.get("normalObject.printInfo.device.value")).as_dynamic();
        }).as_dynamic()
        .flat_map([=](pprx::unit){
            return callOnDispatchWithCommandAndSubParameter("waitForDccSelect", "hps", resp.body).as_dynamic()
            .map([=](const_json_t json){
                const bool bDccTrade = json.get<bool>("bDccTrade");
                m_tradeData.put<bool>("bDccTrade", bDccTrade);
                if(bDccTrade){
                    std::string strDccAmount =
                    (
                     boost::format("%s %s") %
                     resp.body.get<std::string>("normalObject.currentlyCode") %
                     resp.body.get<std::string>("normalObject.fgnAmount")
                     ).str();
                    m_tradeData.put("strDccAmount", strDccAmount);
                }
                return pprx::unit();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

auto JobCreditSalesJMups::sendTradeConfirm(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    const std::map<std::string, prmCredit::payWayDiv> lutPayWayDiv =
    {
        { "lump"        , prmCredit::payWayDiv::Lump },
        { "installment" , prmCredit::payWayDiv::Installment },
        { "bonus"       , prmCredit::payWayDiv::Bonus },
        { "bonusCombine", prmCredit::payWayDiv::BonusCombine },
        { "revolving"   , prmCredit::payWayDiv::Revolving }
    };
    
    auto it = lutPayWayDiv.find(m_tradeData.get<std::string>("paymentWay"));
    auto payWayDiv = it->second;

    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    if(!taxOtherAmount) taxOtherAmount = 0;
    auto productCode = m_tradeData.get_optional<std::string>("productCode");
    if(!productCode) productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
    
    return hpsCreditSales_Confirm
    (
     selector,
     (bDccTrade && (*bDccTrade)) ?
        prmCredit::dcc::Yes :
        prmCredit::dcc::No,
     m_tradeData.get<int>("amount"),
     *taxOtherAmount,
     *productCode,
     payWayDiv,
     m_tradeData.get_optional<int>("partitionTimes"),
     m_tradeData.get_optional<int>("firsttimeClaimMonth"),
     false,         /* isRePrint */
     m_tradeData.get_optional<int>("couponSelectListNum"), /* couponSelectListNum */
     m_tradeData.get_optional<int>("couponApplyPreviousAmount"), /* couponApplyPreviousAmount */
     m_tradeData.get_optional<int>("couponApplyAfterAmount"), /* couponApplyAfterAmount */
     m_tradeData.get_optional<std::string>("firstGACCmdRes"),   /* firstGACCmdRes */
     m_tradeData.get_optional<int>("bonusTimes"),
     m_tradeData.get_optional<int>("bonusMonth1"),
     m_tradeData.get_optional<int>("bonusAmount1"),
     m_tradeData.get_optional<int>("bonusMonth2"),
     m_tradeData.get_optional<int>("bonusAmount2"),
     m_tradeData.get_optional<int>("bonusMonth3"),
     m_tradeData.get_optional<int>("bonusAmount3"),
     m_tradeData.get_optional<int>("bonusMonth4"),
     m_tradeData.get_optional<int>("bonusAmount4"),
     m_tradeData.get_optional<int>("bonusMonth5"),
     m_tradeData.get_optional<int>("bonusAmount5"),
     m_tradeData.get_optional<int>("bonusMonth6"),
     m_tradeData.get_optional<int>("bonusAmount6"),
     boost::none,   /* point */
     boost::none    /* exchangeablePoint */
    );
}

auto JobCreditSalesJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmCredit::doubleTrade>
{
    LogManager_Function();
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(prmCredit::doubleTrade::No).as_dynamic();
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmCredit::doubleTrade::Yes);
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return prmCredit::doubleTrade::Yes;
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<prmCredit::doubleTrade>(err).as_dynamic();
    }).as_dynamic();
}

auto JobCreditSalesJMups::sendTagging(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    // NFCで決済していた場合は子ジョブのタギングを実行して返す。
    if(isUsedChildJob()) {
        return m_childJob->execChildJobSendTagging();
    }

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsCreditSales_Tagging
        (
         selector,
         (bDccTrade && (*bDccTrade)) ?
             prmCredit::dcc::Yes :
             prmCredit::dcc::No,
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmCredit::printResultDiv::Success,
         boost::none,
         false
         );
    }).as_dynamic();
}

auto JobCreditSalesJMups::processSendDigiSign()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();

    // NFCで決済していた場合は子ジョブの印字データを取得し返す。
    if(isUsedChildJob()) {
        return m_childJob->execChildJobDigiSign();
    }
    if(PaymentJob::isTrainingMode()){
        if(getJMupsStorage()->getTrainingModePinInputted()){
            getJMupsStorage()->setTrainingModePinInputted(false);
            return pprx::observable<>::just(pprx::unit());
        }
    }

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    const auto digiReceiptDiv = tradeResultData.get_optional<std::string>("normalObject.digiReceiptDiv");
    if( digiReceiptDiv && (*digiReceiptDiv == "1")){
        LogManager_AddInformation("電子伝票保管対象（サイン要）");
        return waitForDigiSign()
        .flat_map([=](boost::optional<std::string> s){
            if(s){
                std::string  sign = *s;
                return sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip, sign);
            }
            else{
                return pprx::error<pprx::unit>(make_error_internal("sign data is empty")).as_dynamic();
            }
        }).as_dynamic()
        .flat_map([=](auto){
            return pprx::observable<>::just(pprx::unit());
        }).as_dynamic()
        .on_error_resume_next([=](auto err){
            LogManager_AddError(pprx::exceptionToString(err));
            return pprx::error<pprx::unit>(err).as_dynamic();
        }).as_dynamic();
    }
    else if(digiReceiptDiv && (*digiReceiptDiv == "2")){
        LogManager_AddInformation("電子伝票保管対象（サイン不要）");
        return pprx::observable<>::just(pprx::unit());
    }
    else{
        LogManager_AddInformation("電子伝票保管対象外");
        isNeedAquirerSlip(true);
        return pprx::observable<>::just(pprx::unit());
    }
}

auto JobCreditSalesJMups::isNeedAquirerSlip(bool bNeedAquirer /* =true */)
-> pprx::observable<pprx::unit>
{
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    tradeResultData.put("ppsdk.isNeedAquirerSlip", bNeedAquirer);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    return pprx::just(pprx::unit());
}

auto JobCreditSalesJMups::waitForDigiSign()
-> pprx::observable<boost::optional<std::string>>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    json_t param;
    auto modulus = tradeResultData.get<std::string>("normalObject.digiSignPubKeyModulus");
    auto exponent = tradeResultData.get<std::string>("normalObject.digiSignPubKeyExponent");
    param.put("modulus", modulus);
    param.put("exponent", exponent);
    const auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    const auto amount = m_tradeData.get<int>("amount") + (taxOtherAmount ? *taxOtherAmount : 0);
    param.put("amount", amount);
    
    std::string strAmount;
    const auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    if(bDccTrade && (*bDccTrade)){
        strAmount = m_tradeData.get<std::string>("strDccAmount");
    }
    else{
        strAmount = (boost::format("合計金額：%s 円") % BaseSystem::commaSeparatedString(amount)).str();
    }
    param.put("strAmount", strAmount );

    return callOnDispatchWithCommandAndSubParameter("waitForDigiSign", "hps", param)
    .map([=](const_json_t json){
        auto html5 = json.get_optional("html5RawCanvasDigiSignData");
        if(html5){
            auto s = encryptHtml5RawCanvasDigiSignData(*html5, modulus, exponent);
            return boost::optional<std::string>(s);
        }
        return json.get_optional<std::string>("encryptedDigiSignData");
     });
}

auto JobCreditSalesJMups::sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    
    return hpsSendDigiSignForCredit( slipNo, digiSign )
    .flat_map([=](auto resp){
        return pprx::maybe::success(resp);
    }).as_dynamic()
    .on_error_resume_next([=](std::exception_ptr err){
        LogManager_AddError(pprx::exceptionToString(err));
        return callOnDispatchWithCommand("waitForRetrySendingDigiSign")
        .flat_map([=](const_json_t json){
            const auto bRetry = json.get_optional<bool>("bRetry");
            if(bRetry){
                return *bRetry ? pprx::maybe::retry() : pprx::maybe::error(err);
            }
            else{
                return pprx::maybe::error(make_error_internal("bRetry is null")).as_dynamic();
            }
        }).as_dynamic();
    })
    .retry()
    .flat_map([=](pprx::maybe ctrl){
        return ctrl.observableForContinue<JMupsAccess::response_t>();
    }).as_dynamic()
    .map([=](auto resp){
        switch(digiReceiptDiv){
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip:
                isNeedAquirerSlip(false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlipWithoutSign:
                isNeedAquirerSlip(false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::PaperSlip:
                isNeedAquirerSlip(true);
                break;
            default:
                LogManager_AddError("invalid digiReceiptDiv");
                break;
        }
        
        return pprx::unit();
    })
    .on_error_resume_next([=](auto err){
        LogManager_AddWarning("sign failed.");
        
        json_t result;
        bool bOutjsonRetryNullFlg = false;
        try{std::rethrow_exception(err);}
        catch(pprx::ppexception& ex){
            result = json_t::from_str(ex.what());
            auto errorDesc = result.get_optional<std::string>("result.detail.description");
            if(errorDesc&& (*errorDesc == "bRetry is null")){
                bOutjsonRetryNullFlg = true;
            }
            else{
                result.put("resultCode", "1");
                result.put("errorObject.errorCode", "PP_W0002");
                result.put("errorObject.errorLevel", "ERROR");
                result.put("errorObject.errorType", "ERROR");
                result.put("errorObject.errorMessage", "電子サイン送信失敗");
            }
        }
        catch(...){
            LogManager_AddDebug("what??");
        }
        
        return bOutjsonRetryNullFlg ? pprx::error<pprx::unit>(make_error_internal("bRetry is nil")) : pprx::error<pprx::unit>(make_error_paymentserver(result));
    });
}

/* virtual */
auto JobCreditSalesJMups::proceedContact(const_json_t data)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return getCardReader()->beginICCardTransaction()
    .flat_map([=](auto){
        return emvRun(getJMupsStorage(), m_tradeStartResponse);
    }).as_dynamic()
    .map([=](auto tradeResult){
        getJMupsStorage()->setTradeResultData(tradeResult);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->endICCardTransaction();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardPinMessage(const_json_t param)  -> pprx::observable<pprx::unit>
{
    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING"))
    .as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithCommandAndSubParameter("showPinDialog", "", param).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardInputPlainOfflinePin(const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyPlainOfflinePin(param);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardInputEncryptedOfflinePin(const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyEncryptedOfflinePin(param);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardInputEncryptedOnlinePin(const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyEncryptedOnlinePin(param);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardTxPlainApdu(const std::string& apdu)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return getCardReader()->txPlainApdu(apdu);
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardTxEncryptedApdu(const std::string& apdu)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return getCardReader()->txEncryptedApdu(apdu);
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardGetIfdSerialNo()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return getCardReader()->getIfdSerialNo();
}

void JobCreditSalesJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    auto&& tradeInfo = getTradeStartInfo();
    const bool isNeedTaxOther = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
    
    TradeInfoBuilder builder;
    builder.addAmount(TradeInfoBuilder::attribute::required, boost::none);
    if(isNeedTaxOther) builder.addTaxOtherAmount(TradeInfoBuilder::attribute::required, boost::none);

    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::InProgress);

    callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "PreInput")
    .subscribe_on(pprx::observe_on_thread_pool())
    .map([=](const_json_t resp){
        m_tradeData.put("amount", resp.get<int>("results.amount"));
        if(isNeedTaxOther){
            auto taxOtherAmount = resp.get_optional<int>("results.taxOtherAmount");
            m_tradeData.put("taxOtherAmount", taxOtherAmount ? (*taxOtherAmount) : 0);
        }
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        auto strError = pprx::exceptionToString(err);
        LogManager_AddDebug(strError);
        if (strError.find("\"status\":\"aborted\"") == std::string::npos) {
            // not aborted;
            LogManager_AddDebug("UIBehaviorState::Bad");
            m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        } else {
            LogManager_AddDebug("UIBehaviorState::Cancelled");
            m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Cancelled);
        }
        return pprx::error<pprx::unit>(err).as_dynamic();
    })
    .publish()
    .connect();
}

/* virtual */
auto JobCreditSalesJMups::emvOnNetwork(const std::string& url, const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return hpsAccessFromEmvKernel(url, param)
    .map([=](auto resp){
        if(url == "ma0770.do?mA0770_1=aaa"){
            beginAsyncPreInputAmount();
        }
        else if(url == "ma0770.do?mA0770_3=aaa"){
            if(resp.body.get_optional("normalObject.dllPatternData")){
                /* normalObject.dllPatternData が存在しない場合には、kid入力処理でm_cardInfoDataが確定する　*/
                m_cardInfoData = resp.body.clone();
            }
        }
        else if(url == "sa1770.do?sA1770_3=aaa"){
            if(resp.body.get_optional("normalObject.dllPatternData")){
                /* normalObject.dllPatternData が存在しない場合には、kid入力処理でm_cardInfoDataが確定する　*/
                m_cardInfoData = resp.body.clone();
            }
        }
        return resp.body;
    });
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiSelectDeal()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "0");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiSelectApplication(const_json_t emvSession)
 -> pprx::observable<const_json_t>
{
    auto applicationList = emvSession.get("transaction.applicationList");
    return callOnDispatchWithCommandAndSubParameter("waitForApplicationSelect", "applicationList", applicationList);
}

auto JobCreditSalesJMups::buildDispatchRequestParamForTradeInfo()
 -> pprx::observable<TradeInfoBuilder>
{
    auto&& terminalInfo = getJMupsStorage()->getTerminalInfo();
    auto&& productCode = terminalInfo.get<std::string>("sessionObject.productCode");
    m_tradeData.put("productCode", productCode);
    
    return pprx::just(pprx::unit())
    .map([=](auto) -> TradeInfoBuilder{
        TradeInfoBuilderJMupsCredit builder;
        {
            auto amount = m_tradeData.get_optional<int>("amount");
            builder.addAmount(TradeInfoBuilder::attribute::required, amount);
        }
        
        auto&& tradeInfo = getTradeStartInfo();
        const bool isNeedTaxOther = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
        const bool isNeedProductCode = tradeInfo.get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
        
        if(isNeedTaxOther){
            auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
            builder.addTaxOtherAmount(TradeInfoBuilder::attribute::required, taxOtherAmount);
        }
        
        if(isNeedProductCode){
            auto productCode = m_tradeData.get_optional<std::string>("productCode");
            builder.addProductCode(TradeInfoBuilder::attribute::required, productCode);
        }
        
        {
            builder.addPaymentWay(m_tradeData, s_paymentWayProperties, m_cardInfoData);
        }
        
        return builder;
    });
}

auto JobCreditSalesJMups::applyDispatchResponseParamForTradeInfo(const_json_t resp)
 -> pprx::observable<bool>
{
    LogManager_AddDebugF("resp:\n%s", resp.str()); 
    auto results = resp.get("results");
    auto amount         = results.get_optional<int>("amount");
    auto taxOtherAmount = results.get_optional<int>("taxOtherAmount");
    auto productCode    = results.get_optional<std::string>("productCode");
    auto slipNo         = results.get_optional<std::string>("slipNo");
    auto approvalNo     = results.get_optional<std::string>("approvalNo");
    auto couponId       = results.get_optional<std::string>("coupon");
    auto paymentWay     = results.get_optional<std::string>("paymentWay");

    if(amount)          m_tradeData.put("amount"            , *amount);
    if(taxOtherAmount)  m_tradeData.put("taxOtherAmount"    , *taxOtherAmount);
    if(productCode)     m_tradeData.put("productCode"       , *productCode);
    if(slipNo)          m_tradeData.put("slipNo"            , *slipNo);
    if(approvalNo)      m_tradeData.put("approvalNo"        , *approvalNo);
    if(couponId)        m_tradeData.put("couponId"          , *couponId);
    if(paymentWay)      m_tradeData.put("paymentWay"        , *paymentWay);

    for(auto prop : s_paymentWayProperties){
        auto value = results.get_optional<int>(prop);
        if(value){
            m_tradeData.put(prop, *value);
        }
        else{
            m_tradeData.remove_key_if_exist(prop);
        }
    }
    
    auto operation = resp.get<std::string>("operation");
    const auto bApply = operation == "apply";
    if(bApply){
        if(couponId){
            auto couponInfo = findCoupon(m_couponData, *couponId);
            auto orgAmount = m_tradeData.get<int>("amount");
            auto discountValue = couponInfo.get<int>("DISCOUNT_PRICE");
            m_tradeData.put("couponApplyPreviousAmount", orgAmount);
            m_tradeData.put("amount", orgAmount - discountValue);
        }
    }
    if(PaymentJob::isTrainingMode()){
        getJMupsStorage()->setInputTradeData(m_tradeData.clone());
    }

    return pprx::just(bApply);
}

auto JobCreditSalesJMups::waitForTradeInfo(const std::string& strTradeOp /* = "" */)
 -> pprx::observable<pprx::unit>
{
    return buildDispatchRequestParamForTradeInfo()
    .flat_map([=](TradeInfoBuilder builder){
        return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::yes, strTradeOp).as_dynamic();
    }).as_dynamic()
    .flat_map([=](const_json_t resp){
        return applyDispatchResponseParamForTradeInfo(resp);
    }).as_dynamic()
    .flat_map([=](bool bApply){
        if(bApply) return pprx::maybe::success(pprx::unit());
        else return pprx::maybe::retry();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::maybe::error(err);
    })
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    });
}

auto JobCreditSalesJMups::syncEmvAmountPreInputUI()
-> pprx::observable<pprx::unit>
{
    return m_emvAmountPreInputUI.get_observable()
    .flat_map([=](UIBehaviorState state){
        if(state == UIBehaviorState::Cancelled){
            abort(AbortReason::UserRequest);
        }
        else if(state == UIBehaviorState::Finished){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else if(state == UIBehaviorState::Bad){
            return pprx::error<pprx::unit>(make_error_internal("amount/taxOtherAmount is empty")).as_dynamic();
        }
        return pprx::never<pprx::unit>().as_dynamic();
    })
    .take(1)
    .map([=](auto){
        return pprx::unit();
    });
}

/* virtual */
auto JobCreditSalesJMups::emvWaitInputPreAmount() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return syncEmvAmountPreInputUI()
    .map([=](auto){
        const auto amount = m_tradeData.get<int>("amount");
        const auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
        json_t j;
        j.put("amount", amount);
        if(taxOtherAmount){
            j.put("taxOtherAmount", *taxOtherAmount);
        }
        return j.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiAmount()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return syncEmvAmountPreInputUI()
    .flat_map([=](auto){
        return waitForTradeInfo("Input");
    })
    .map([=](auto){
        return m_tradeData.as_readonly();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiInputKid()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return syncEmvAmountPreInputUI()
    .flat_map([=](auto){
        return waitForKidInput();
    }).as_dynamic()
    .map([=](auto resp) -> const_json_t{
        return resp.body.get("normalObject");
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiInputPayWay(const_json_t emvSession)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto dcc = emvSession.get_optional<std::string>("transaction.dccUseOnOffDiv");
        if(dcc && ((*dcc) == "1")){
            return processDcc()
            .flat_map([=](auto){
                auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
                if(bDccTrade && (*bDccTrade)){
                    LogManager_AddInformation("use dcc");
                    m_tradeData.put("bUseDCC", true);
                    return hpsDccSelect()
                    .flat_map([=](auto){
                        return pprx::just(pprx::unit());
                    })
                    .as_dynamic();
                }
                else{
                    LogManager_AddInformation("not use dcc");
                    m_tradeData.put("bUseDCC", false);
                }
                return pprx::just(pprx::unit())
                .as_dynamic();
            })
            .as_dynamic();
        }
        else{
            return pprx::just(pprx::unit())
            .as_dynamic();
        }
    }).as_dynamic()
    .map([=](auto){
        json_t json;

        auto productCode = m_tradeData.get_optional<std::string>("productCode");
        if(!productCode){
            productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
        }
        json.put("productCode", *productCode);
        
        auto paymentWay = m_tradeData.get_optional<std::string>("paymentWay");
        if(paymentWay){
            const std::map<std::string, std::string> lutPayWayDiv =
            {
                { "lump"        , "0" },
                { "installment" , "1" },
                { "bonus"       , "2" },
                { "bonusCombine", "3" },
                { "revolving"   , "4" }
            };
            auto it = lutPayWayDiv.find(*paymentWay);
            if(it == lutPayWayDiv.end()){
                throw_error_internal("unknown paymentWay");
            }
            json.put("payWayDiv", it->second);
        }
        return json.as_readonly();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnUiAmountConfirm(const_json_t emvSession)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    m_tradeData.put("firstGACCmdRes", emvSession.get<std::string>("transaction.firstGACCmdRes"));
 
    return sendTradeConfirm(prmCredit::doubleTrade::No)
    .flat_map([=](auto resp)mutable{
        json_t jsonResponse = resp.body.clone();
        auto bIsFallback = jsonResponse.get_optional<bool>("normalObject.isFallback");
        if(bIsFallback&&(*bIsFallback)){
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_SKIP_FAILURE")).as_dynamic()
            .flat_map([=](auto) {
                json_t json;
                json.put("errorCode", "PP_E3001");
                json.put("description", "このカードはICでのお取り引きはできません。/n 磁気カードリーダを使用してください。");
                return pprx::error<prmCredit::doubleTrade>(make_error_pinreleted(json)).as_dynamic();
            }).as_dynamic();
        }
        else{
            return waitForConfirmDoubleTrade(resp);
        }
    }).as_dynamic()
    .map([=](prmCredit::doubleTrade taggingSelector) -> const_json_t{
        m_taggingSelector = taggingSelector;
        return getJMupsStorage()->getTradeResultData();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();
}

/* virtual */ // Todo ビルドのため。
auto JobCreditSalesJMups::emvOnTradeConfirmFinish(const_json_t emvTradeConfirm)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    json_t param;
    param.put("parameter.hps", tradeResultData.clone());
    param.put("parameter", emvTradeConfirm.clone());
        
    return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", emvTradeConfirm.clone()).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::processRemovingContact(bool bTradeIsDecline /*= false*/)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return getCardReader()->observableDeviceConnectState().take(1)
    .flat_map([=](CardReader::DeviceConnectState connState){
        if(connState != CardReader::DeviceConnectState::Connected){
            LogManager_AddDebug("デバイスが接続されていない");
            /* R/Wが切断状態の場合、カード抜き待ち処理は存在しない */
            return pprx::just(pprx::unit()).as_dynamic();
        }
        
        LogManager_AddDebug("デバイスが接続されている");
        if(m_cardType == CardType::MS){            
            return pprx::just(pprx::unit()).as_dynamic();
        }
        
        return getCardReader()->abortTransaction()
        .flat_map([=](auto) {
            return getCardReader()->observableICCardSlotState().take(1);
        }).as_dynamic()
        .flat_map([=](CardReader::ICCardSlotState slotState){
            if(slotState != CardReader::ICCardSlotState::Inserted){
                LogManager_AddDebug("カードが挿入されている状態ではなくなった");
                /* カードが挿入されている状態でない */
                return pprx::just(pprx::unit()).as_dynamic();
            }
            else{
                auto strLEDText = bTradeIsDecline ? getDisplayMsgForReader("MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD") : getDisplayMsgForReader("MIURA_CREDIT_UI_REMOVING_CARD");
                if(strLEDText.empty()){
                    return pprx::just(pprx::unit()).as_dynamic();
                }
                else{
                    return getCardReader()->displayTextNoLf(strLEDText)
                    .flat_map([=](auto){
                        return getCardReader()->observableICCardSlotState()
                        .distinct_until_changed()
                        .flat_map([=](CardReader::ICCardSlotState slotState){
                            if(slotState != CardReader::ICCardSlotState::Inserted){
                                LogManager_AddInformation("Card is removed");
                                return pprx::just(pprx::unit()).as_dynamic();
                            }
                            return pprx::never<pprx::unit>().as_dynamic();
                        }).as_dynamic()
                        .take(1)
                        .flat_map([=](auto){
                            LogManager_AddInformation("Card is removed manually.");
                            return pprx::just(pprx::unit()).as_dynamic();
                        }).as_dynamic();
                    }).as_dynamic()
                    .on_error_resume_next([=](auto err){
                        LogManager_AddInformationF("Card is not removed: \n%s", pprx::exceptionToString(err));
                        return pprx::just(pprx::unit()).as_dynamic();
                    }).take(1)
                    .map([=](auto){
                        LogManager_AddInformation("processRemovingContact finished");
                        return pprx::unit();
                    }).as_dynamic();
                }
            }
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::emvOnTrainingCardReaderDisplayData()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto&& inputTradeData = getJMupsStorage()->getInputTradeData();
    auto strTaxOtherAmount= inputTradeData.get_optional<int>("taxOtherAmount");
    auto nTaxOtherAmount  = strTaxOtherAmount ? (*strTaxOtherAmount) : 0;
    auto totalYen         = inputTradeData.get<int>("amount") + nTaxOtherAmount;
    
    auto strAmount = BaseSystem::commaSeparatedString(totalYen);
    auto msg1 = (boost::format("暗証番号を入力してください\n金額 ￥%s円") % strAmount).str();
    
    json_t  jsonDisplay;
    jsonDisplay.put("amount"            , totalYen);
    jsonDisplay.put("application.label" , "56495341435245444954");
    jsonDisplay.put("uiMessage.message1", msg1);
    jsonDisplay.put("uiMessage.message2", "");
    
    return pprx::just(jsonDisplay.as_readonly());
}

/* virtual */
auto JobCreditSalesJMups::emvOnCardPinResultConfirm(const_json_t param)  -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    if(PaymentJob::isTrainingMode()){ // TRAINING MODEで非表示
        return pprx::just(pprx::unit());
    }
    
    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING"))
    .as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", param).as_dynamic()
        .map([=](const_json_t resp){
            const bool bContinue = resp.get<bool>("bContinue");
            if(!bContinue){
                throw_error_aborted("aborted");
            }
            return pprx::unit();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobCreditSalesJMups::addMediaDivToCardNo(const_json_t tradeData) -> json_t
{
    LogManager_Function();

    if(PaymentJob::isTrainingMode()){
        json_t resp = tradeData.clone();
        auto pinfo = tradeData.get("normalObject.printInfo.device.value").clone();
        auto values = pinfo.get<json_t::array_t>("values");
        
        /* カードMEDIA_DIVを追加　ICかMSか */
        {
            for(auto v : values){
                std::string mediaDiv;
                std::string physicalMediaDiv;
                auto cardNo   = v.get_optional<std::string>("CARD_NO");
                if(m_cardType == CardType::IC){
                    mediaDiv = "IC ";
                    physicalMediaDiv = "1";
                }
                else if(m_cardType == CardType::MS){
                    mediaDiv = "MS ";
                    physicalMediaDiv = "0";
                }
                else{
                    mediaDiv = "XX ";
                }
                auto cardNoWithMediaDiv = mediaDiv + *cardNo;
                
                v.put("CARD_NO", cardNoWithMediaDiv);
                v.put("physicalMediaDiv", physicalMediaDiv);
            }
        }
        /* printInfoのvalueを再構築 */
        pinfo.put("values", values);
        resp.put("normalObject.printInfo.device.value", pinfo);
        
        getJMupsStorage()->setTradeResultData(resp.as_readonly());
        return resp;
    }
    
    getJMupsStorage()->setTradeResultData(tradeData);
    return tradeData.clone();
}

auto JobCreditSalesJMups::checkTradeFailureErrorCode(const_json_t tradeData, const std::string errorCode) -> bool
{
    LogManager_Function();
    
    auto bIsDecline = tradeData.get_optional<std::string>("normalObject.printInfo.isDecline");
    if(bIsDecline && (*bIsDecline == "true")){ /* 取引が不成立 */
        auto printInfo = tradeData.get("normalObject.printInfo.device.value").clone();
        auto values = printInfo.get<json_t::array_t>("values");
        /* FAILURE_INFOの情報を確認 */
        for(auto v : values){
            auto failureList = v.get_optional("FAILURE_INFO");
            if(failureList){
                auto&& list = failureList->get_array<std::string>();
                for(auto src: list){
                    auto found = src.find(errorCode.c_str());
                    if (found!=std::string::npos){
                        return true;
                    }
                }
            }
        }
    }
    
    return false;
}

auto JobCreditSalesJMups::createChildJob(const std::string JobName) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto) {
        PaymentJobCreator jc;
        m_childJob = jc.create(JobName);
        m_childJob->setCardReader(getCardReader());
        m_childJob->setStorage(getJMupsStorage());
        m_childJob->setTrainingMode(PaymentJob::isTrainingMode());
        m_childJob->setUseDigiSignInTrainingMode(PaymentJob::isUseDigiSignInTrainingMode());
        return m_childJob->registDispatches(retrieveJobDispatch(), retrieveJobDispatchCancel());
    }).as_dynamic();
}

void JobCreditSalesJMups::beginAsyncRemovingContact()
{
    LogManager_Function();
    // NFCで決済していた場合はカード抜去が不要です。
    if(isUsedChildJob()) {
        return;
    }
    
    bool bTradeIsDecline = false;
    auto bIsDecline = getJMupsStorage()->getTradeResultData().get_optional<std::string>("normalObject.printInfo.isDecline");
    if(bIsDecline && (*bIsDecline == "true")){ /* 取引が不成立 */
        bTradeIsDecline = true;
    }
    
    processRemovingContact(bTradeIsDecline)
    .map([](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([](auto err){
        LogManager_AddInformation(pprx::exceptionToString(err));
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .publish()
    .connect();
}

/* virtual */
auto JobCreditSalesJMups::isUsedChildJob() -> bool const
{
    bool ret = false;
    do{
        if(!m_childJob) {
            break;
        }
        auto cardType = m_cardStartData.get_optional<std::string>("cardType");
        if(!cardType) {
            break;
        }
        if(*cardType == "nfc") {
            ret = true;
        }
    } while(0);
    return ret;
}

/* virtual */
auto JobCreditSalesJMups::createChildJobParam() -> const_json_t
{
    LogManager_AddDebugF("m_tradeData = %s", m_tradeData.str());
    json_t childParam;
    auto&& amount = m_tradeData.get_optional<int>("amount");
    if(amount) {
        childParam.put("amount", *amount);
    }
    auto&& transactionCurrencyCode = getTerminalInfo().get_optional<std::string>("sessionObject.transactionCurrencyCode");
    if(transactionCurrencyCode) {
        childParam.put("currencyCode", boost::lexical_cast<int>(*transactionCurrencyCode));
    } else {
        // トレーニングモードなどで開局情報を取得できない場合は固定値で392を設定する。
        childParam.put("currencyCode", 392);
    }
    childParam.put("validChildJob", isValidChildJob());
    childParam.put("isTraining", (PaymentJob::isTrainingMode() ? true : false));
    auto dateOpt = m_tradeStartResponseHeader.get_optional<std::string>("Date");
    auto dateJson = PaymentJob::getTradeDateTime((dateOpt ? *dateOpt : ""));
    childParam.put("dateTime", dateJson.clone());
    childParam.put("tradeType", "sales");
    LogManager_AddDebugF("childParam = %s", childParam.str());
    return childParam.as_readonly();
}

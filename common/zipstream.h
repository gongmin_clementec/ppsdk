/* iostream クラスのテンプレート */

#if ! defined( __h_zipstream__ )
#define __h_zipstream__

#include <streambuf>
#include <iostream>
#include <vector>
#include <exception>
#include <string>
#include <zlib.h>

namespace ORANGEWERKS {
    class zipstream_exception :
        public std::exception
    {
    private:
        std::string m_strError;
        
    protected:
        
    public:
        zipstream_exception( const char* strError ) : m_strError( ( strError == NULL ) ? "unknown" : strError ) {}
        virtual const char* what() const noexcept { return( m_strError.c_str() ); }
    };
    
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
        class basic_zipstreambuf : public std::basic_streambuf<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_streambuf<_Elem,_Traits>;
        
        std::basic_ostream<_Elem,_Traits>*		m_out;
        std::basic_istream<_Elem,_Traits>*		m_in;
        
        std::vector<_Elem>		m_srcBuff;		/* バッファ実体 */
        std::vector<_Elem>		m_dstBuff;		/* バッファ実体 */
        
        _Elem*					m_pBuff;		/* バッファポインタ */
        std::streamsize			m_nBuff;		/* バッファサイズ */
        
        z_stream				m_zip;
        int						m_zipFlush;
        
    protected:
        /* 外部からの出力バッファ指定 */
        virtual superclass_t* setbuf( _Elem* b, std::streamsize s )
        {
            if( m_out ){
                m_srcBuff.clear();
                this->setp( b, b + s );
            }
            if( m_in ){
                m_dstBuff.clear();
                this->setg( b, b, b + s );
            }
            m_pBuff = b;
            m_nBuff = s;
            return( this );
        }
        
        /* 書き込みバッファが一杯になった時に呼び出される */
        virtual typename superclass_t::int_type overflow( typename superclass_t::int_type c = _Traits::eof() )
        {
            overflow_self();
            if( c == _Traits::eof() ){
                return( _Traits::eof() );
            }
            /* c はバッファから溢れたオブジェクトなので、ここで追加する */
            *superclass_t::pptr() = _Traits::to_char_type( c );
            superclass_t::pbump( 1 );
            return( _Traits::not_eof( c ) );
        }
        
        /* 読み込みバッファが空になった時に呼び出される */
        virtual typename superclass_t::int_type underflow()
        {
            if( superclass_t::egptr() <= superclass_t::gptr() ){
                if( m_zipFlush == Z_FINISH ){
                    return( _Traits::eof() );
                }
                underflow_self();
                if( superclass_t::egptr() <= superclass_t::gptr() ){
                    return( _Traits::eof() );
                }
            }
            return( _Traits::to_int_type( *superclass_t::gptr() ) );
        }
        
        /* flush() で呼び出される */
        virtual int sync()
        {
            if( m_out ){
                if( m_zipFlush == Z_FINISH ){
                    overflow_self();
                }
                else{
                    const int back_zipFlush = m_zipFlush;
                    m_zipFlush = Z_SYNC_FLUSH;
                    overflow_self();
                    m_zipFlush = back_zipFlush;
                }
                m_out->flush();
            }
            return( 0 );
        }
        
        
        void overflow_self()
        {
            m_zip.next_in	= reinterpret_cast<unsigned char*>( m_pBuff );
            m_zip.avail_in	= static_cast<uInt>( superclass_t::pptr() - m_pBuff );
            m_zip.next_out	= reinterpret_cast<unsigned char*>( &m_dstBuff.front() );
            m_zip.avail_out	= static_cast<uInt>( m_dstBuff.size() );
            
            if( m_zip.avail_in == 0 ){
                if( m_zipFlush != Z_FINISH ){
                    return;
                }
            }
            
            while( true ){
                const int status = ::deflate( &m_zip, m_zipFlush );
                if( status == Z_STREAM_END ){
                    /* 完了 */
                    m_out->write( &m_dstBuff.front(), m_dstBuff.size() - m_zip.avail_out );
                    return;
                }
                if( status != Z_OK ){
                    throw( zipstream_exception( m_zip.msg ) );
                }
                if( m_zip.avail_out == 0 ){
                    /* 出力バッファが尽きた */
                    m_out->write( &m_dstBuff.front(), m_dstBuff.size() );
                    m_zip.next_out	= reinterpret_cast<unsigned char*>( &m_dstBuff.front() );
                    m_zip.avail_out	= static_cast<uInt>( m_dstBuff.size() );
                    continue;
                }
                if( m_zip.avail_in == 0 ){
                    /* 入力バッファが尽きた */
                    m_out->write( &m_dstBuff.front(), m_dstBuff.size() - m_zip.avail_out );
                    this->setp( m_pBuff, m_pBuff + m_nBuff );
                    return;
                }
            }
        }
        
        void underflow_self()
        {
            m_zip.next_out	= reinterpret_cast<unsigned char*>( m_pBuff );
            m_zip.avail_out	= static_cast<uInt>( m_nBuff );
            
            while( true ){
                if( m_zip.avail_in == 0 ){
                    /* 入力バッファが尽きた */
                    m_in->read( &m_srcBuff.front(), m_srcBuff.size() );
                    m_zip.next_in	= reinterpret_cast<unsigned char*>( &m_srcBuff.front() );
                    m_zip.avail_in	= static_cast<uInt>( m_in->gcount() );
                    if( m_zip.avail_in == 0 ){
                        m_zipFlush = Z_FINISH;
                    }
                }
                
                if( m_zip.avail_out == 0 ){
                    /* 出力バッファが尽きた */
                    this->setg( m_pBuff, m_pBuff, m_pBuff + m_nBuff );
                    return;
                }
                
                const int status = inflate( &m_zip, m_zipFlush );
                if( status == Z_STREAM_END ){
                    /* 完了 */
                    this->setg( m_pBuff,m_pBuff,m_pBuff+m_nBuff - m_zip.avail_out);
                    return;
                }
                if( status != Z_OK ){
                    throw( zipstream_exception( m_zip.msg ) );
                }
            }
        }
        
        
    public:
        basic_zipstreambuf( std::basic_ostream<_Elem,_Traits>& os ) :
        m_out( &os ), m_in( NULL )
        {
            m_srcBuff.resize( 4096 );
            m_dstBuff.resize( 4096 );
            
            m_pBuff = &m_srcBuff.front();
            m_nBuff = m_srcBuff.size();
            
            this->setp( m_pBuff, m_pBuff + m_nBuff );
            
            ::memset( &m_zip, 0, sizeof( m_zip ) );
            if( ::deflateInit( &m_zip, Z_DEFAULT_COMPRESSION ) != Z_OK ){
                throw( zipstream_exception( m_zip.msg ) );
            }
            m_zipFlush = Z_NO_FLUSH;
        }
        
        basic_zipstreambuf( std::basic_istream<_Elem,_Traits>& is ) :
        m_out( NULL ), m_in( &is )
        {
            m_srcBuff.resize( 4096 );
            m_dstBuff.resize( 4096 );
            
            m_pBuff = &m_dstBuff.front();
            m_nBuff = m_dstBuff.size();
            
            this->setg( m_pBuff, m_pBuff + m_nBuff, m_pBuff + m_nBuff );
            
            ::memset( &m_zip, 0, sizeof( m_zip ) );
            if( ::inflateInit( &m_zip ) != Z_OK ){
                throw( zipstream_exception( m_zip.msg ) );
            }
            m_zipFlush = Z_NO_FLUSH;
        }
        
        virtual ~basic_zipstreambuf()
        {
            m_zipFlush = Z_FINISH;
            sync();
            
            if( m_out != NULL ){
                if( ::deflateEnd( &m_zip ) != Z_OK ){
                    //throw( zipstream_exception( "basic_zipstreambuf::~basic_zipstreambuf : deflateEnd" ) );
                }
            }
            if( m_in != NULL ){
                if( ::inflateEnd( &m_zip ) != Z_OK ){
                    //throw( zipstream_exception( "basic_zipstreambuf::~basic_zipstreambuf : inflateEnd" ) );
                }
            }
            
        }
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_ozipstream : public std::basic_ostream<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_ostream<_Elem,_Traits>;
    protected:
    public:
        basic_ozipstream( std::ostream& os ) :
        std::basic_ostream<_Elem,_Traits>( new basic_zipstreambuf<_Elem,_Traits>( os ) )
        {
        }
        
        virtual ~basic_ozipstream()
        {
            superclass_t::flush();
            delete superclass_t::rdbuf();
        }
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_izipstream : public std::basic_istream<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_istream<_Elem,_Traits>;
    protected:
    public:
        basic_izipstream( std::istream& is ) :
        std::basic_istream<_Elem,_Traits>( new basic_zipstreambuf<_Elem,_Traits>( is ) )
        {
        }
        
        virtual ~basic_izipstream()
        {
            delete superclass_t::rdbuf();
        }
    };
    
    using izipstream = basic_izipstream<char>;
    using ozipstream = basic_ozipstream<char>;
}	/* namespace ORANGEWERKS */

#endif /* ! defined( __h_zipstream__ ) */


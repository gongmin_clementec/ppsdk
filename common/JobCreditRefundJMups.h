//
//  JobCreditRefundJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobCreditRefundJMups__)
#define __h_JobCreditRefundJMups__

#include "JobCreditSalesJMups.h"

class JobCreditRefundJMups :
    public JobCreditSalesJMups
{
private:
    bool m_bDccUseOnOffDiv;   // true:DCC利用可;  false:DCC利用不可
    bool m_bDCCTradeDiv;      // true:DCC取引;  false:non-DCC
    bool m_bSearchResultDiv;  // true:取得;  false: 未取得
    json_t m_refundSearchTradeResp;

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual auto processDcc() -> pprx::observable<pprx::unit>;
    virtual auto sendTradeConfirm(prmCredit::doubleTrade selector)  -> pprx::observable<JMupsAccess::response_t>;
    virtual void beginAsyncPreInputAmount();

    auto sendTagging() -> pprx::observable<JMupsAccess::response_t>;
    auto waitForStoreTradeResultData(JMupsAccess::response_t jmupsData) -> pprx::observable<prmCredit::doubleTrade>;
    auto processCommon() -> pprx::observable<pprx::unit>;
   
protected:
    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto proceedContact(const_json_t data)  -> pprx::observable<pprx::unit>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    virtual auto emvOnNetwork(const std::string& url, const_json_t param) -> pprx::observable<const_json_t>;
     
public:
            JobCreditRefundJMups(const __secret& s);
    virtual ~JobCreditRefundJMups();
};



#endif /* !defined(__h_JobCreditRefundJMups__) */

//
//  JobIdle.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "JobIdle.h"

JobIdle::JobIdle(const __secret& s) :
    PaymentJob(s)
{
}

/* virtual */
JobIdle::~JobIdle()
{    
}

/* virtual */
auto JobIdle::getObservableSelf() -> pprx::observable<const_json_t>
{
    return pprx::never<const_json_t>();
}

/* virtual */
auto JobIdle::finalizeSelf() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

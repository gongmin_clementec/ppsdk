//
//  PPConfig.cpp
//  ppsdk
//
//  Created by clementec on 2016/05/12.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#include "PPConfig.h"
#include <boost/assign.hpp>
#include "zipstream.h"
#include "cipherstream.h"
#include "base64stream.h"
#include "LogManager.h"
#include <boost/foreach.hpp>

#define AES128_KEY uint8_t(0x9a), uint8_t(0x5c), uint8_t(0xe0), uint8_t(0x5d), uint8_t(0x28), uint8_t(0x80), uint8_t(0x3f), uint8_t(0xed), uint8_t(0x6a), uint8_t(0x8c), uint8_t(0xd4), uint8_t(0xdb), uint8_t(0xd9), uint8_t(0xda), uint8_t(0xb0), uint8_t(0xe3)


PPConfig::PPConfig()
{
}

void PPConfig::clear()
{
    boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
    m_json.clear();
}

void PPConfig::serialize(std::ostream& os, bool bEncryption) const
{
    if(bEncryption){
        using namespace boost::assign;
        std::vector<uint8_t> key;
        key += AES128_KEY;
        ORANGEWERKS::obase64stream  base64(os);
        ORANGEWERKS::ocipherstream  cipher(base64);
        cipher.initialize(ORANGEWERKS::ECipherMethod::AES128, key, key);
        {
            boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
            ORANGEWERKS::ozipstream     zip(cipher);
            m_json.serialize(zip, false);
        }
    }
    else{
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        m_json.serialize(os, true);
    }
}

void PPConfig::serialize(const boost::filesystem::path& path, bool bEncryption) const
{
    auto os = std::ofstream(path.string());
    serialize(os, bEncryption);
}

void PPConfig::deserialize(std::istream& is, bool bEncryption)
{
    if(bEncryption){
        using namespace boost::assign;
        std::vector<uint8_t> key;
        key += AES128_KEY;
        ORANGEWERKS::ibase64stream  base64(is);
        ORANGEWERKS::icipherstream  cipher(base64);
        cipher.initialize(ORANGEWERKS::ECipherMethod::AES128, key, key);
        ORANGEWERKS::izipstream     zip(cipher);
        {
            boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
            m_json.deserialize(zip);
        }
    }
    else{
        boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
        m_json.deserialize(is);
    }
    updateConnectedCardReaderInformation();
}

void PPConfig::updateConnectedCardReaderInformation()
{
    json_t j;
    auto path = BaseSystem::instance().getDocumentDirectoryPath() / "cardReader.json";
    if(boost::filesystem::exists(path)){
        std::ifstream is(path.string());
        j.deserialize(is);
    }
    boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);
    m_json.put("volatile.connectedCardReaderInformation", j);
}

void PPConfig::deserialize(const boost::filesystem::path& path, bool bEncryption)
{
    auto is = std::ifstream(path.string());
    deserialize(is, bEncryption);
}

void PPConfig::updateConfigure(const std::string& desc, const std::string& json)
{
    LogManager_Function();
    LogManager_AddInformationF("\tin updateConfigure. desc = %s", desc);
    
    json_t j;
    j.deserialize(json);
    LogManager_AddFullInformationF("updateConfigure  :\n%s",j.clone().str());
    
    enum class _type{
        string,
        integer,
        boolean
    };
    
    auto tbl = std::map<std::string, _type>{
        {"hps.urlForActivation"     , _type::string},
        {"hps.urlForTrading"        , _type::string},
        {"hps.keychainStoreLabel"   , _type::string},
        {"hps.jobTimeout"           , _type::integer},
        {"hps.bSupportCupIc"        , _type::boolean},

        {"tms.url"                  , _type::string},

        {"job.credit.media.ms"      , _type::boolean},
        {"job.credit.media.ic"      , _type::boolean},
        {"job.credit.media.nfc"     , _type::boolean},
        {"job.credit.media.qr"      , _type::boolean},

        {"job.unionpay.media.ms"    , _type::boolean},
        {"job.unionpay.media.ic"    , _type::boolean},
        {"job.unionpay.media.nfc"   , _type::boolean},
        {"job.unionpay.media.qr"    , _type::boolean},

        {"miura.ea.protocol"        , _type::string},
        {"miura.ea.name"            , _type::string},
        {"miura.ea.serial"          , _type::string},

        {"debug.bOutputToConsole"    , _type::boolean},
        {"debug.bRecordLog"          , _type::boolean},
        {"debug.bActivationSkipError", _type::boolean},
        {"debug.bResetTMSPasswords"  , _type::boolean},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_APPROVED",             _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_DECLINED",             _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_PLEASE_ENTER_PIN",     _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_ERROR_PROCESSING",     _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_REMOVE_CARD",          _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_PRESENT_CARD",         _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_PROCESSING",           _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_CARD_READ_OK_REMOVE",  _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_INSERT_OR_SWIPE_CARD", _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_CARD_COLLISION",       _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_PLEASE_SIGN",          _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_ONLINE_AUTHORISATION", _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_TRY_OTHER_CARD",       _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_INSERT_CARD",          _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_SEE_PHONE",            _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_PRESENT_CARD_AGAIN",   _type::string},
        {"miura.messages.contactless.prompts.MIURA_CONTACTLESS_UI_CALL_YOUR_BANK",       _type::string}
    };

    boost::unique_lock<boost::recursive_mutex>  lock(m_mtx);

    for(auto k : tbl){
        switch(k.second){
            case _type::string:
            {
                auto v = j.get_optional<std::string>(k.first);
                if(v){
                    set(k.first, *v);
                }
                break;
            }
            case _type::integer:
            {
                auto v = j.get_optional<int>(k.first);
                if(v){
                    set(k.first, *v);
                }
                break;
            }
            case _type::boolean:
            {
                auto v = j.get_optional<bool>(k.first);
                if(v){
                    set(k.first, *v);
                }
                break;
            }
        }
    }
}

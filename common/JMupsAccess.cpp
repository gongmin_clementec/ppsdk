//
//  JMupsAccess.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccess.h"
#include "WebClient.h"
#include "AbstructFactory.h"
#include "PaymentJob.h"
#include "PPConfig.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include "JMupsJobStorage.h"

JMupsAccess::JMupsAccess()
{
    LogManager_Function();
    
    m_basePath = PPConfig::instance().get<std::string>("hps.urlForTrading");
}

/* virtual */
JMupsAccess::~JMupsAccess()
{
    if(m_webClient){
        m_webClient->finalize();
    }
}

void JMupsAccess::setUrlForActivation()
{
    m_basePath = PPConfig::instance().get<std::string>("hps.urlForActivation");
}

JMupsJobStorage::sp JMupsAccess::getJMupsStorage()
{
    auto pj = dynamic_cast<PaymentJob*>(this);
    if(pj == nullptr) return JMupsJobStorage::sp();
    return boost::dynamic_pointer_cast<JMupsJobStorage>(pj->getStorage());
}

const JMupsJobStorage::sp JMupsAccess::getJMupsStorage() const
{
    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj == nullptr) return JMupsJobStorage::sp();
    return boost::dynamic_pointer_cast<JMupsJobStorage>(pj->getStorage());
}

bool JMupsAccess::isTrainingMode() const
{
    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj == nullptr) return false;
    return pj->isTrainingMode();
}

bool JMupsAccess::isUseDigiSignInTrainingMode() const
{
    auto pj = dynamic_cast<const PaymentJob*>(this);
    if(pj == nullptr) return false;
    return pj->isUseDigiSignInTrainingMode();
}

const_json_t JMupsAccess::getTerminalInfo() const
{
    return getJMupsStorage()->getTerminalInfo();
}

const_json_t JMupsAccess::getNfcMasterData() const
{
    return getJMupsStorage()->getNfcMasterData();
}

const_json_t JMupsAccess::getCookies() const
{
    return getJMupsStorage()->getCookies();
}

void JMupsAccess::setCookies(const_json_t cookies)
{
    getJMupsStorage()->setCookies(cookies);
}


void JMupsAccess::setCookie(const std::string& key, const std::string& value)
{
    LogManager_Function();
    
    auto cookies = getCookies().clone();;
    cookies.put((boost::format("%s.value") % key).str(), value);
    setCookies(cookies);
}


auto JMupsAccess::callTraining(const std::string& path, const keyvalue_t& replaces)
 -> pprx::observable<response_t>
{
    LogManager_Function();
    
    setCurrentHpsUrl(path);
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        boost::filesystem::path filepath;
        {
            /* url : "xxxx.do?yyyy=aaa"      -> file : "xxxx-yyyy.txt"  */
            /* url : "xxxx.do?yyyy=aaa&zzzz" -> file : "xxxx-yyyyzzzz.txt"  */
            boost::regex expr(R"->(([0-9A-Za-z_]+\.do)\?([0-9A-Za-z_]+)(=aaa)?(&([0-9A-Za-z_]+))?)->");
            const auto fname = boost::regex_replace(path, expr, "$1-$2$5.txt");
            filepath = BaseSystem::instance().getResourceDirectoryPath() / "training" / fname;
        }
        
        LogManager_AddInformationF("send hps : %s", path);
        LogManager_AddDebugF("path : %s", path);
        LogManager_AddDebugF("filepath : %s", filepath.string());

        std::string fdata;
        {
            std::ifstream ifs(filepath.string());
            if(!ifs){
                throw_error_internalf("file open error. %s", path);
            }
            fdata.assign(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
        }
        
        { /* ##DATE_TIME_NOW## */
            auto s = boost::posix_time::to_iso_extended_string(boost::posix_time::microsec_clock::local_time());
            s = s.substr(2, 17);
            s[2] = '/';
            s[5] = '/';
            s[8] = ' ';
            boost::algorithm::replace_all(fdata, "##DATE_TIME_NOW##", s);
        }
        
        { /* ##CUP_SEND_DATE_TIME## */
            auto sCupDate = boost::posix_time::to_iso_extended_string(boost::posix_time::microsec_clock::local_time());
            sCupDate = sCupDate.substr(5, 14);
            sCupDate[2] = '/';
            sCupDate[5] = ' ';
            boost::algorithm::replace_all(fdata, "##CUP_SEND_DATE_TIME##", sCupDate);
        }

        { /* ##digiReceiptDiv## */
            boost::algorithm::replace_all(fdata, "##digiReceiptDiv##", isUseDigiSignInTrainingMode() ? "1" : "0");
        }
        
        for(auto&& it : replaces){
            boost::algorithm::replace_all(fdata, it.first, it.second);
        }
        
        auto body = json_t::from_str(fdata);
        json_t header;
        return pprx::just(response_t{header, body});
    }).as_dynamic();
}

auto JMupsAccess::callHps(const std::string& path, const_json_t json)
 -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_AddInformationF("\tpath : %s", path);
    LogManager_AddInformationF("\tjson : %s", json.str());
    auto j2 = json.clone();
    if(PPConfig::instance().get<bool>("hps.bSupportCupIc")){
        j2.put("supportCupIc", "1");
    }
    
    const bool kJsonWithIndent = true;
    std::stringstream ss;
    j2.serialize(ss, kJsonWithIndent);
    auto&& s = ss.str();
    auto postData = boost::make_shared<bytes_t>(std::begin(s), std::end(s));
    
    const std::string url = m_basePath + path;
    setCurrentHpsUrl(path);
    
    if(!m_webClient){
        m_webClient = AbstructFactory::instance().createWebClientForJMups();
        m_webClient->initialize();
    }
    
    LogManager_AddInformationF("send hps : %s", path);
    LogManager_AddDebugF("send body : \n%s", s);
    LogManager_AddDebugF("send cookie : \n%s", getCookies().str());
    
    return m_webClient->postAsync(url, postData, getCookies())
    .flat_map([=](WebClient::response_t resp) mutable{
        std::stringstream ss;
        ss << std::string(resp.body->begin(), resp.body->end());
        LogManager_AddInformationF("receive hps : %s", path);
        LogManager_AddDebugF("receive body : \n%s", ss.str());
        LogManager_AddDebugF("receive cookie : \n%s", resp.cookies.str());
        json_t jsonBody;
        jsonBody.deserialize(ss);

        auto cookies = getCookies().clone();
        if(!resp.cookies.empty()){
            for(auto item: resp.cookies.get<json_t::map_t>()){
              cookies.put(item.first, item.second);
            }
        }
        else{
            LogManager_AddInformation("empty cookie");
            cookies.clear();
        }
        setCookies(cookies);

        auto resultCode = jsonBody.get_optional<std::string>("resultCode");
        if(resultCode && ((*resultCode) == "0")){
          return pprx::just(JMupsAccess::response_t{resp.headers, jsonBody})
          .as_dynamic();
        }
        else{
            if(jsonBody.empty()){
                return pprx::error<JMupsAccess::response_t>(make_error_internal("hps bad response"))
                .as_dynamic();
            }
            else{
              return pprx::error<JMupsAccess::response_t>(make_error_paymentserver(jsonBody))
              .as_dynamic();
            }
        }
    }).as_dynamic();
}

auto JMupsAccess::downloadHps(const std::string& path, const_json_t json, const boost::filesystem::path& file)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    const bool kJsonWithIndent = true;
    std::stringstream ss;
    json.serialize(ss, kJsonWithIndent);
    auto&& s = ss.str();
    auto postData = boost::make_shared<bytes_t>(std::begin(s), std::end(s));
    
    const std::string url = m_basePath + path;
    
    if(!m_webClient){
        m_webClient = AbstructFactory::instance().createWebClientForJMups();
        m_webClient->initialize();
    }
    
    LogManager_AddDebugF("url : %s", url);
    LogManager_AddInformationF("send hps : %s", path);
    LogManager_AddDebugF("send body : %s", s);
    LogManager_AddDebugF("send cookie : %s", getCookies().str());
    
    auto header = json_t();
    header.put("Content-type", "application/json; charset=utf-8");
    return m_webClient->downloadFileWithPost(url, postData, file, header);
}


JMupsAccess::hpsResultState JMupsAccess::getHpsResultState(const_json_t json) const
{
    LogManager_Function();
    
    auto resultCode = json.get_optional<std::string>("detail.resultCode");
    if(!resultCode){
        LogManager_AddError("resultCode not found.");
        return hpsResultState::Bad;
    }
    if(*resultCode == "0") return hpsResultState::Success;
    
    auto errorType = json.get_optional<std::string>("detail.errorObject.errorType");
    if(!resultCode){
        LogManager_AddError("errorObject.errorType not found.");
        return hpsResultState::Bad;
    }
    
    if(*errorType == "FATAL") return hpsResultState::Fatal;
    if(*errorType == "ERROR") return hpsResultState::Error;
    if(*errorType == "WARN") return hpsResultState::Warning;

    LogManager_AddErrorF("unknown result state %s", *errorType);
    return hpsResultState::Bad;
}

bool JMupsAccess::isRetryCardReadWarning(const_json_t json, std::string& readerDisplayKey)
{
    readerDisplayKey = "MIURA_CREDIT_UI_ERROR_PROCESSING";  // エラーが発生しました"
    
    if(getHpsResultState(json) != hpsResultState::Warning) return false;
    auto errorCode = json.get_optional<std::string>("detail.errorObject.errorCode");
    if(!errorCode) return false;
    /* クレジット
     T_W0201 カード読み取りエラーが発生しました。↑再度やり直してください。
     T_W2028 ICチップ付カードを磁気リーダで読み込みました。↑ICカードリーダーを使用してください。
     T_W2029 ICカードと磁気カードで会員番号が異なります。↑同一のカードを使用してください。
     PP_W_0000 このカードはICでのお取引はできません。↑カードを抜いて、磁気カードリーダーを使用してください。
     PP_W0001 このカードでのお取引はできません。↑カードを抜いて、別の取引方法に切替えてください。
     */
    if(*errorCode == "T_W0201"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY";  // 磁気カード
        return true;
    }
    if(*errorCode == "T_W2028"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_INSERT_ERROR";  // ICを使用してください
        return true;
    }
    if(*errorCode == "T_W2029"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_DIFF_ERROR";  // 同じカードでお試しください
        return true;
    }
    if(*errorCode == "PP_W0000"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_SWIPE_ERROR";  // 磁気を使用してください
        return true;
    }
    if(*errorCode == "PP_W0001"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_CHECK_ERROR";  // カードを確認してください
        return true;
    }
    
    /* 銀聯
     ・T_W3126 ICチップ付カードを磁気リーダで読み込みました。↑ICカードリーダーを使用してください。
     ・T_W3127 ICカードと磁気カードで会員番号が異なります。↑同一のカードを使用してください。
     */
    if(*errorCode == "T_W3126"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_INSERT_ERROR";  // ICを使用してください
        return true;
    }
    if(*errorCode == "T_W3127"){
        readerDisplayKey = "MIURA_CREDIT_UI_CARD_DIFF_ERROR";  // 同じカードでお試しください
        return true;
    }
    
    return false;
}

std::string JMupsAccess::getHpsResultCode(const_json_t json) const
{
    LogManager_Function();
    
    auto resultCode = json.get_optional<std::string>("resultCode");
    if(!resultCode){
        LogManager_AddError("resultCode not found.");
        return "unknown(resultCode not found.)";
    }
    if(*resultCode == "0") return "success";
    
    auto errorCode = json.get_optional<std::string>("errorObject.errorCode");
    if(!errorCode){
        LogManager_AddError("errorObject.errorCode not found.");
        return "unknown(errorObject.errorCode not found.)";
    }

    return *errorCode;
}

auto JMupsAccess::hpsOpenTerminal_Request(
 prmOpenTerminal_Request::nfcDeviceType nfcDeviceType)
 -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    setJsonParam(json, "nfcDeviceType", nfcDeviceType,
                 {
                     {prmOpenTerminal_Request::nfcDeviceType::Panasonic,   "1"},
                     {prmOpenTerminal_Request::nfcDeviceType::Others,      "2"}
                 });
    if(isTrainingMode()){
        return callTraining("opzz.do?oPZZ72_2=aaa", keyvalue_t());
    }
    else{
        return callHps("opzz.do?oPZZ72_2=aaa", json);
    }
}

auto JMupsAccess::hpsTradeStart_Request
(
 prmTradeStart_Request::serviceDiv    serviceDiv,
 boost::optional<std::string>         deal,
 boost::optional<prmTradeStart_Request::nfcDeviceType> nfcDeviceType)
 -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "serviceDiv", serviceDiv,
                 {
                     {prmTradeStart_Request::serviceDiv::CreditCard     , "0"},
                     {prmTradeStart_Request::serviceDiv::UnionPay       , "1"},
                     {prmTradeStart_Request::serviceDiv::Suica          , "2"},
                     {prmTradeStart_Request::serviceDiv::JDebit         , "3"},
                     {prmTradeStart_Request::serviceDiv::SalesTagging   , "4"},
                     {prmTradeStart_Request::serviceDiv::Point          , "7"},
                     {prmTradeStart_Request::serviceDiv::Gift           , "8"},
                     {prmTradeStart_Request::serviceDiv::iD             , "9"},
                     {prmTradeStart_Request::serviceDiv::QuicPay        , "A"},
                     {prmTradeStart_Request::serviceDiv::Edy            , "B"},
                     {prmTradeStart_Request::serviceDiv::Waon           , "C"},
                     {prmTradeStart_Request::serviceDiv::Nanaco         , "D"},
                     {prmTradeStart_Request::serviceDiv::Nfc            , "E"},
                 });
    
    setJsonParam(json, "deal", deal);
    
    setJsonParam(json, "nfcDeviceType", nfcDeviceType,
                 {
                     {prmTradeStart_Request::nfcDeviceType::Panasonic,   "1"},
                     {prmTradeStart_Request::nfcDeviceType::Miura,      "2"}
                 });
    
    if(serviceDiv == prmTradeStart_Request::serviceDiv::UnionPay){
        if(PPConfig::instance().get<bool>("hps.bSupportCupIc")){
            setJsonParam(json, "cupIcCardCopeDiv", "1");
        }
    }
    
    if(isTrainingMode()){
        return callTraining("mA0447.do?mA0447=aaa", keyvalue_t())
        .map([=](response_t resp){
            m_tradeStartInfo = resp.body.clone();
            // リクルートの要望で、「税・そのほか」と「商品コード」が不要
            m_tradeStartInfo.put("normalObject.dllPatternData.inputTaxOther", "0");
            m_tradeStartInfo.put("normalObject.dllPatternData.inputProductCode", "0");
            return resp;
        });
    }
    else{
        return callHps("mA0447.do?mA0447=aaa", json)
        .map([=](response_t resp){
            m_tradeStartInfo = resp.body.clone();
            return resp;
        });
    }
}

auto JMupsAccess::hpsMSCardRead
(
 prmMSCardRead::settlementMediaDiv    settlementMediaDiv,
 prmCommon::operationDiv              operationDiv,
 bool                                 isMSFallback,
 bool                                 isFallback,
 const std::string&                   keySerialNo,
 boost::optional<std::string>         cryptData,
 boost::optional<std::string>         cryptDataJIS1Track1,
 boost::optional<std::string>         cryptDataJIS1Track2,
 boost::optional<std::string>         cryptDataJIS2,
 boost::optional<std::string>         cryptDataPattern,
 boost::optional<std::string>         mSFormatPattern)
 -> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    setJsonParam(json, "settlementMediaDiv", settlementMediaDiv,
                 {
                     {prmMSCardRead::settlementMediaDiv::Credit         , "0"},
                     {prmMSCardRead::settlementMediaDiv::UnionPay       , "1"},
                     {prmMSCardRead::settlementMediaDiv::JDebit         , "3"},
                     {prmMSCardRead::settlementMediaDiv::Point          , "6"},
                     {prmMSCardRead::settlementMediaDiv::Gift           , "7"},
                     {prmMSCardRead::settlementMediaDiv::CheckCoupon    , "8"}
                 });
    
    
    switch(settlementMediaDiv)
    {
        case prmMSCardRead::settlementMediaDiv::Credit:
        {
            setJsonParam(json, "operationDiv", operationDiv,
             {
                 {prmCommon::operationDiv::Sales        , "0"},
                 {prmCommon::operationDiv::Refund       , "1"},
                 {prmCommon::operationDiv::PreApproval  , "3"},
                 {prmCommon::operationDiv::AfterApproval, "4"},
                 {prmCommon::operationDiv::CardCheck    , "5"}
             });
            break;
        }
        case prmMSCardRead::settlementMediaDiv::UnionPay:
        {
            setJsonParam(json, "operationDiv", operationDiv,
             {
                 {prmCommon::operationDiv::Sales                , "0"},
                 {prmCommon::operationDiv::Refund               , "1"},
                 {prmCommon::operationDiv::PreApproval          , "3"},
                 {prmCommon::operationDiv::AfterApproval        , "4"},
                 {prmCommon::operationDiv::CancelPreApproval    , "5"},
                 {prmCommon::operationDiv::CancelAfterApproval  , "6"}
             });
            break;
        }
        default:
        {
            break;
        }
    }
    
    setJsonParam(json, "isMSFallback"       , isMSFallback);
    setJsonParam(json, "isFallback"         , isFallback);
    setJsonParam(json, "keySerialNo"        , keySerialNo);
    setJsonParam(json, "cryptData"          , cryptData);
    setJsonParam(json, "cryptDataJIS1Track1", cryptDataJIS1Track1);
    setJsonParam(json, "cryptDataJIS1Track2", cryptDataJIS1Track2);
    setJsonParam(json, "cryptDataJIS2"      , cryptDataJIS2);
    setJsonParam(json, "cryptDataPattern"   , cryptDataPattern);
    setJsonParam(json, "mSFormatPattern"    , mSFormatPattern);
    
    
    if(isTrainingMode()){
        return callTraining("mA9271.do?mA9271=aaa", keyvalue_t());
    }
    else{
        return callHps("mA9271.do?mA9271=aaa", json);
    }
}

auto JMupsAccess::hpsSendDigiSignForCredit
(
 const std::string&             slipNo,
 const std::string&             signData)
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "signData", signData);
    
    if(isTrainingMode()){
        return callTraining("xAE01.do?xAE0101=aaa", keyvalue_t());
    }
    else{
        return callHps("xAE01.do?xAE0101=aaa", json);
    }
}

// 銀聯電子サイン情報送信
auto JMupsAccess::hpsSendDigiSignForUnionPay
(
 const std::string&             slipNo,
 const std::string&             signData)
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "signData", signData);
    
    
    if(isTrainingMode()){
        return callTraining("xAE01.do?xAE0102=aaa", keyvalue_t());
    }
    else{
        return callHps("xAE01.do?xAE0102=aaa", json);
    }
}

auto JMupsAccess::hpsStoreDigiSignForCredit
(
 const std::string&             slipNo,
 prmCommon::digiReceiptDiv      digiReceiptDiv,
 boost::optional<std::string>   signData)
 -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "digiReceiptDiv", digiReceiptDiv,
                 {
                     {prmCommon::digiReceiptDiv::DigiSlip               , "1"},
                     {prmCommon::digiReceiptDiv::DigiSlipWithoutSign    , "2"},
                     {prmCommon::digiReceiptDiv::PaperSlip              , "9"}
                 });
    setJsonParam(json, "signData", signData);
    
    
    if(isTrainingMode()){
        return callTraining("xAE03.do?xAE0301=aaa", keyvalue_t());
    }
    else{
        return callHps("xAE03.do?xAE0301=aaa", json);
    }
}

// 銀聯電子伝票保管(取引完了後)
auto JMupsAccess::hpsStoreDigiSignForUnionPay
(
 const std::string&             slipNo,
 prmCommon::digiReceiptDiv      digiReceiptDiv,
 boost::optional<std::string>   signData)
 -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "digiReceiptDiv", digiReceiptDiv,
                 {
                     {prmCommon::digiReceiptDiv::DigiSlip               , "1"},
                     {prmCommon::digiReceiptDiv::DigiSlipWithoutSign    , "2"},
                     {prmCommon::digiReceiptDiv::PaperSlip              , "9"}
                 });
    setJsonParam(json, "signData", signData);
    
    
    if(isTrainingMode()){
        return callTraining("xAE03.do?xAE0302=aaa", keyvalue_t());
    }
    else{
        return callHps("xAE03.do?xAE0302=aaa", json);
    }
}

auto JMupsAccess::hpsAccessFromEmvKernel
(
 const std::string& url,
 const_json_t      postParam,
 const bool        bCredit/*=true*/)
 -> pprx::observable<JMupsAccess::response_t>
{
    json_t json = postParam.clone();

    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();
        
        if(bCredit&&(0==url.compare("sa1027.do?sA1027_1=aaa"))){  //IC 売上 クレジット
            auto&& inputTradeData = getJMupsStorage()->getInputTradeData();
            
            auto paymentWay     = inputTradeData.get<std::string>("paymentWay");
            auto productCode    = inputTradeData.get<std::string>("productCode");
            auto AMOUNT         = inputTradeData.get<int>("amount");
            auto taxOtherAmount = inputTradeData.get_optional<int>("taxOtherAmount");
            auto TAX            = taxOtherAmount ? (*taxOtherAmount) : 0;
            auto TOTAL_YEN      = integerToYenStringInJson(AMOUNT+TAX);
            
            std::string PAY_WAY_DIV("");
            std::string PAY_DIVISION("");
            if(0==paymentWay.compare("lump")){
                PAY_WAY_DIV = "0";
                PAY_DIVISION = "一括";
                reps["##INSTALLMENT##"] = "";
                reps["##FIRSTTIME_CLAIM_MONTH##"] = "";
            }
            else if(0==paymentWay.compare("installment")){
                PAY_WAY_DIV ="1";
                PAY_DIVISION = "分割";
                reps["##INSTALLMENT##"] = "2回";
                reps["##FIRSTTIME_CLAIM_MONTH##"] = "3月";
            }
            else if(0==paymentWay.compare("bonus")){
                PAY_WAY_DIV ="2";
                PAY_DIVISION = "ボーナス";
                reps["##INSTALLMENT##"] = "";
                reps["##FIRSTTIME_CLAIM_MONTH##"] = "";
            }
            else if(0==paymentWay.compare("bonusCombine")){
                PAY_WAY_DIV ="3";
                PAY_DIVISION = "ボーナス併用";
                reps["##INSTALLMENT##"] = "2回";
                reps["##FIRSTTIME_CLAIM_MONTH##"] = "3月";
            }
            else if(0==paymentWay.compare("revolving")){
                PAY_WAY_DIV ="4";
                PAY_DIVISION = "リボ";
                reps["##INSTALLMENT##"] = "";
                reps["##FIRSTTIME_CLAIM_MONTH##"] = "";
            }
            else{
                LogManager_AddError("unknown paymentWay");
                throw_error_internal("unknown paymentWay");
            }

            reps["##PRODUCT_DIVISION##"] = productCode;
            reps["##AMOUNT##"]           = integerToYenStringInJson(AMOUNT);
            reps["##TAX##"]              = integerToYenStringInJson(TAX);
            reps["##SETTLEDAMOUNT##"]    = TOTAL_YEN;
            reps["##TOTAL_YEN##"]        = TOTAL_YEN;
            reps["##PAY_WAY_DIV##"]      = PAY_WAY_DIV;
            reps["##PAY_DIVISION##"]     = PAY_DIVISION;
        }
        
        if(!bCredit&&(0==url.compare("sa1027.do?sA1027_1=aaa"))){  //IC 売上 銀聯
            auto&& inputTradeData = getJMupsStorage()->getInputTradeData();
            auto AMOUNT         = inputTradeData.get<int>("amount");
            reps["##AMOUNT##"]       = integerToYenStringInJson(AMOUNT);
            reps["##SETTLEDAMOUNT##"] = integerToYenStringInJson(AMOUNT);
            reps["##digiReceiptDiv##"] = isUseDigiSignInTrainingMode() ? "1" : "0";
            
            return callTraining("sa1901.do?sA1901_2=aaa", reps);
        }
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

std::string JMupsAccess::integerToYenStringInJson(int value) const
{
    std::stringstream ss;
    if(0==value){
        ss << "\\\\0"; // ¥0
        return ss.str();
    }

    if(value < 0) ss << '-';
    ss << "\\\\";

    
    std::vector<int> _3digits;
    while (value !=0)
    {
        auto threeDigit = value % 1000;
        _3digits.push_back(threeDigit);
        value /= 1000 ;
    }
    
    std::reverse(_3digits.begin(), _3digits.end());
    
    for(auto i = 0; i < _3digits.size(); i++){
        if(i > 0){
            ss << ',' << std::setfill('0') << std::setw(3);
        }
        ss << _3digits[i];
    }

    return ss.str();
}

void JMupsAccess::setCurrentHpsUrl(const std::string hpsUrl)
{
    LogManager_AddDebugF("hpsUrl:\t%s", hpsUrl.c_str());
    
    getJMupsStorage()->saveCurrentHpsUrl(hpsUrl);
}

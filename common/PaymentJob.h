//
//  PaymentJob.h
//  ppsdk
//
//  Created by Clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_PaymentJob__)
#define __h_PaymentJob__

#include "LazyObject.h"
#include "AtomicObject.h"
#include "PPReactive.h"
#include "TradeInfoBuilder.h"
#include "JMupsAccess.h"

class CardReader;

class PaymentJob :
    public LazyObject
{
public:
    using sp = boost::shared_ptr<PaymentJob>;
    
    class Storage
    {
    public:
        using sp = boost::shared_ptr<Storage>;
        
    private:
    protected:
    public:
        Storage() = default;
        virtual ~Storage() = default;
        
        virtual void serialize(std::ostream& os) const {};
        virtual void deserialize(std::istream& is) {};
    };
    
    enum class AbortReason {
        Bad,
        UserRequest,
        JobTimer,
        Background,
        ForceAnyway   /* 強制中断 */
    };
    
public:
    using dispatch_t        = boost::function<bool(const std::string& inParam, std::string& outParam)>;
    using dispatchCancel_t  = boost::function<void()>;

private:
    AtomicObject<dispatch_t>                m_funcDispatch;
    dispatchCancel_t                        m_funcDispatchCancel;
    pprx::subjects::behavior<pprx::unit>    m_abortRequest;
    pprx::subjects::behavior<pprx::unit>    m_abortBySystemRequest;
    
    Storage::sp     m_storage;
    boost::shared_ptr<CardReader> m_cardReader;
    bool            m_bTrainingMode;
    bool            m_bUseDigiSignInTrainingMode;
    bool            m_bValidChildJob;
    bool            m_bUseChildJob;
    boost::recursive_mutex m_abortReason_mtx;
    AbortReason     m_abortReason;

    bool internalCallOnDispatch(const std::string& inParam, std::string& outParam);

    auto    finalize() -> pprx::observable<pprx::unit>;
    void    handleDispatchCancelResult(json_t& dispatchJson);

protected:
    
    virtual auto getObservableSelf() -> pprx::observable<const_json_t> = 0;
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit> = 0;

    bool internalCallOnDispatchWithCommand(const std::string& command, json_t& outParam);
    bool internalCallOnDispatchWithCommandAndSubParameter(const std::string& command, const std::string& sub, const_json_t subParam, json_t& outParam);
    bool internalCallOnDispatchWithMessage(const std::string& message);
    bool internalCallOnDispatchWithMessageAndSubParameter(const std::string& message, const std::string& sub, const_json_t subParam);

    
    auto callOnDispatchWithCommand(const std::string& command) -> pprx::observable<const_json_t>;
    auto callOnDispatchWithCommandAndSubParameter(const std::string& command, const std::string& sub, const_json_t subParam) -> pprx::observable<const_json_t>;
    auto callOnDispatchWithMessage(const std::string& message) -> pprx::observable<pprx::unit>;
    auto callOnDispatchWithMessageAndSubParameter(const std::string& message, const std::string& sub, const_json_t subParam) -> pprx::observable<pprx::unit>;
    
    auto callOnDispatchTradeInfo(const TradeInfoBuilder& builder, TradeInfoBuilder::updatable updatable, std::string strOp="") -> pprx::observable<const_json_t>;

    auto callOnDispatchCancel() -> pprx::observable<pprx::unit>;
    
    auto waitForReaderConnect() -> pprx::observable<pprx::unit>;
    auto waitForReaderConnectAndReadMedia(const_json_t readOption, const std::string& displayTxt) -> pprx::observable<const_json_t>;
    auto getDisplayMsgForReader(const std::string& msgId, const bool bContactless = false) -> std::string;
    auto waitForRemovingContact() -> pprx::observable<pprx::unit>;
    
    std::string encryptHtml5RawCanvasDigiSignData(const_json_t html5, const std::string& modulus, const std::string& exponent) const;
    
    template <class T> auto connectAbortBySystemError(T value) -> pprx::observable<T>
    {
        return m_abortBySystemRequest.get_observable()
        .map([=](pprx::unit){
            return value;
        }).as_dynamic();
    }
    
    auto retrieveJobDispatch() -> dispatch_t;
    auto retrieveJobDispatchCancel() -> dispatchCancel_t;
    void setUseChildJob(bool bUseChildJob) { m_bUseChildJob = bUseChildJob; }
    virtual auto isUsedChildJob() -> bool const;
    virtual auto createChildJobParam() -> const_json_t;
    void setValidChildJob(bool bValidChildJob) { m_bValidChildJob = bValidChildJob; }
    bool isValidChildJob() const { return m_bValidChildJob; }
    virtual auto isNeedWaitForShowingJobResult() -> bool;

public:
            PaymentJob(const __secret& s);
    virtual ~PaymentJob();
    
    virtual bool        isJobTimeoutRequired() const;
    virtual bool        isCardReaderRequired() const = 0;
    virtual auto initializeChildJob() -> pprx::observable<pprx::unit>;
    virtual auto mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto getChildSlipDocument() -> pprx::observable<const_json_t>;
    virtual auto execChildJobDigiSign() -> pprx::observable<pprx::unit>;
    virtual auto execChildJobSendTagging() -> pprx::observable<JMupsAccess::response_t>;

    void                abort(AbortReason reason);
    
    void                setStorage(Storage::sp storage) { m_storage = storage; }
    Storage::sp         getStorage()        { return m_storage; }
    const Storage::sp   getStorage() const  { return m_storage; }

    void                                  setCardReader(boost::shared_ptr<CardReader> cardReader) { m_cardReader = cardReader; }
    boost::shared_ptr<CardReader>         getCardReader()        { return m_cardReader; }
    const boost::shared_ptr<CardReader>   getCardReader() const  { return m_cardReader; }

    void setTrainingMode(bool bTrainingMode) { m_bTrainingMode = bTrainingMode; }
    bool isTrainingMode() const { return m_bTrainingMode; }
    
    void setUseDigiSignInTrainingMode(bool bUse) { m_bUseDigiSignInTrainingMode = bUse; }
    bool isUseDigiSignInTrainingMode() const { return m_bUseDigiSignInTrainingMode; }
    
    auto getObservable(dispatch_t onDispatch, dispatchCancel_t onDispatchCancel) -> pprx::observable<const_json_t>;
    auto registDispatches(dispatch_t dispatch, dispatchCancel_t dispatchCancel) -> pprx::observable<pprx::unit>;
    auto getTradeDateTime(std::string dateTimeStr) -> const_json_t;
};






#endif /* !defined(__h_PaymentJob__) */

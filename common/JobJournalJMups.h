//
//  JobJournalJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobJournalJMups__)
#define __h_JobJournalJMups__

#include "PaymentJob.h"
#include "JMupsAccessJournal.h"
#include "JMupsSlipPrinter.h"

class JobJournalJMups :
    public PaymentJob,
    public JMupsAccessJournal,
    public JMupsSlipPrinter
{
private:
    auto treatSlipDocumentJson(prmJournalCommon::paymentType paymentType, response_t resp, bool bIntermediate ) -> pprx::observable<const_json_t>;

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;

public:
            JobJournalJMups(const __secret& s);
    virtual ~JobJournalJMups();

    virtual bool isCardReaderRequired() const { return false; };
};



#endif /* !defined(__h_JobJournalJMups__) */

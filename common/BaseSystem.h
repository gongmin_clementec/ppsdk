//
//  BaseSystem.hpp
//  ppsdk
//
//  Created by clementec on 2016/04/14.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#ifndef BaseSystem_hpp
#define BaseSystem_hpp

#include <boost/filesystem.hpp>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/asio.hpp>
#pragma GCC diagnostic pop
#include <boost/date_time.hpp>
#include <boost/function.hpp>
#include <boost/optional.hpp>
#include "Singleton.h"
#include "Json.h"
#pragma GCC diagnostic ignored "-Wconversion"
#include <rxcpp/rx.hpp>
#pragma GCC diagnostic pop


using keyvalue_t        = std::map<std::string,std::string>;
using keyvalue_sp       = boost::shared_ptr<keyvalue_t>;
using const_keyvalue_sp = boost::shared_ptr<const keyvalue_t>;

using bytes_t           = std::vector<uint8_t>;
using bytes_sp          = boost::shared_ptr<bytes_t>;
using const_bytes_sp    = boost::shared_ptr<const bytes_t>;

using json_t            = basic_json_t<char>;
using const_json_t      = basic_json_t<char>::readonly;

using timer_sp          = boost::shared_ptr<boost::asio::deadline_timer>;

using timer_sp          = boost::shared_ptr<boost::asio::deadline_timer>;

class SerialDevice;

class BaseSystem : public Singleton<BaseSystem>
{
friend class Singleton<BaseSystem>;

public:
    enum class ApplicationState{
        Initializing,
        WillBackground,
        Background,
        Foreground
    };
    
    enum class OSType {
        Android,
        iOS,
        Windows,
        Linux
    };
    
    class imp
    {
    public:
        imp() = default;
        virtual ~imp() = default;
        virtual void callFunctionWithCatchException(boost::function<void()> func) = 0;
        virtual void invokeMainThread(boost::function<void()> func) = 0;
        virtual void invokeMainThreadAsync(boost::function<void()> func) = 0;
        virtual bool isMainThread() = 0;
        virtual boost::filesystem::path getResourceDirectoryPath() = 0;
        virtual boost::filesystem::path getDocumentDirectoryPath() = 0;
        virtual std::string sjisToUtf8(const std::string& src) = 0;
        virtual boost::shared_ptr<SerialDevice> createSerialDevice() = 0;
        virtual const_json_t getSystemInformation() = 0;
        virtual std::string getDeviceUuid() = 0;

        virtual boost::optional<std::string> getSecureElementText(const std::string& name) = 0;
        virtual bool setSecureElementText(const std::string& name, const std::string& value) = 0;

        virtual boost::optional<std::string> getSharedAreaText(const std::string& name) = 0;
        virtual bool setSharedAreaText(const std::string& name, const std::string& value) = 0;

        virtual bool certificateIsInstalled() = 0;
        virtual bool certificateInstall(const boost::filesystem::path& path, const std::string& password) = 0;
        virtual bool certificateUninstall() = 0;
        virtual auto canSendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool> = 0;
        virtual auto sendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool> = 0;
        virtual auto applicationState() -> rxcpp::observable<ApplicationState> = 0;
        virtual OSType getOSType() = 0;
        virtual auto soundPlay(const std::string& soundFile) -> void = 0;
    };
    
private:
    boost::shared_ptr<imp>                              m_imp;
    boost::shared_ptr<boost::asio::io_service>          m_ioService;
    boost::shared_ptr<boost::asio::io_service::work>    m_iosWork;
    boost::shared_ptr<boost::thread_group>              m_threads;
    
protected:
            BaseSystem();
    virtual ~BaseSystem();
    
public:
    void post(boost::function<void()> func);
    boost::asio::io_service&    ioService() { return *m_ioService; }
    timer_sp postWithTimeout(int second, boost::function<void()> func);
    void invokeMainThread(boost::function<void()> func);
    void invokeMainThreadAsync(boost::function<void()> func);
    bool isMainThread();
    
    boost::filesystem::path getResourceDirectoryPath();
    boost::filesystem::path getDocumentDirectoryPath();
    boost::shared_ptr<SerialDevice> createSerialDevice();
    
    const_json_t getSystemInformation();
    std::string getDeviceUuid();

    bool certificateIsInstalled();
    bool certificateInstall(const boost::filesystem::path& path, const std::string& password);
    bool certificateUninstall();
    auto canSendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool>;
    auto sendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool>;
    auto applicationState() -> rxcpp::observable<ApplicationState>;
    OSType getOSType();
    auto soundPlay(const std::string& soundFile) -> void;

    
    template <typename TYPE> TYPE invokeMainThreadWithResult(boost::function<TYPE()> func)
    {
        TYPE result = TYPE();
        invokeMainThread([&](){
            result = func();
        });
        return result;
    }

    static void sleep(uint32_t ms)
    {
        boost::this_thread::sleep(boost::posix_time::milliseconds(ms));
    }
    
    static std::string jis8ToUtf8(const std::string& src);
    std::string sjisToUtf8(const std::string& src);
        
    static std::string replaceTextAlignRight(const std::string& src, const std::string& append);
    
    static std::string urlEncode(const std::string& src);
    
    static std::string dumpHexText(const bytes_t& bytes);
    static boost::posix_time::ptime parseRFC2616DateTimeText(const std::string& src);
    static std::string commaSeparatedString(int value);
    static int commaSeparatedStringToInt(std::string value);
    static constexpr bool isBigEndian()
    {
#if 1   /* Xcode8で怒られる（armやx86はlittle endian） */
        return false;
#else
        const union {
            uint16_t abcd;
            uint8_t  bytes[2];
        } m = { 0xABCD };
        return m.bytes[0] == 0xAB;
#endif
        // reinterpret_cast<> は constexpr内では使用できない
        //const uint16_t test = 0x1234;
        //return (*reinterpret_cast<const uint8_t*>(&test)) == 0x12;
    }


    static bytes_sp hexTextToBytes(const std::string src)
    {
        auto result = boost::make_shared<bytes_t>();
        uint8_t ch = 0;
        for(auto i = 0; i < src.length(); i++){
            switch(src[i]){
                case '0': { ch |= 0; break; }
                case '1': { ch |= 1; break; }
                case '2': { ch |= 2; break; }
                case '3': { ch |= 3; break; }
                case '4': { ch |= 4; break; }
                case '5': { ch |= 5; break; }
                case '6': { ch |= 6; break; }
                case '7': { ch |= 7; break; }
                case '8': { ch |= 8; break; }
                case '9': { ch |= 9; break; }
                case 'A': case 'a': { ch |= 10; break; }
                case 'B': case 'b': { ch |= 11; break; }
                case 'C': case 'c': { ch |= 12; break; }
                case 'D': case 'd': { ch |= 13; break; }
                case 'E': case 'e': { ch |= 14; break; }
                case 'F': case 'f': { ch |= 15; break; }
            }
            if((i & 1) == 1){
                result->push_back(ch);
                ch = 0;
            }
            else{
                ch <<= 4;
            }
        }
        return result;
    }
    
    boost::optional<std::string> getSecureElementText(const std::string& name);
    bool setSecureElementText(const std::string& name, const std::string& value);

    boost::optional<std::string> getSharedAreaText(const std::string& name);
    bool setSharedAreaText(const std::string& name, const std::string& value);
};


/* enumをbitfieldとして扱う場合に下記のオペレータを定義して、型変換を避ける */
#define ENUM_AS_BITFIELD_OPERATORS(T) \
inline T  operator |  (T  a, T b) { return static_cast<T>(static_cast<unsigned>(a) | static_cast<unsigned>(b)); } \
inline T& operator |= (T& a, T b) { return a = a | b; } \
inline T  operator &  (T  a, T b) { return static_cast<T>(static_cast<unsigned>(a) & static_cast<unsigned>(b)); } \
inline T& operator &= (T& a, T b) { return a = a & b; } \
inline bool any(T a)              { return static_cast<unsigned>(a) != 0; }



#endif /* BaseSystem_hpp */

//
//  NFCTagConverterBluePad.h
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_NFCTagConverterBluePad__)
#define __h_NFCTagConverterBluePad__

#include "JMupsAccess.h"

class NFCTagConverterBluePad
{
private:
    json_t m_masterData;

    using pred_t = boost::function<std::string(const std::string&)>;
    
    class translator
    {
    private:
        const_json_t   m_src;
        json_t&        m_dst;
    protected:
    public:
        translator(json_t& dst, const_json_t src) : m_dst(dst), m_src(src) {}
        virtual ~translator() = default;
        
        void tr(const std::string& dstKey, const std::string& srcKey, pred_t pred = pred_t())
        {
            auto ovalue = m_src.get_optional<std::string>(srcKey);
            if(!ovalue) return;
            auto value = ovalue.get();
            if(pred){
                value = pred(value);
            }
            m_dst.put(dstKey, value);
        }
        
        static std::string asciiHexTextToAsciiText(const std::string& src);
        static std::string trimLeft00(const std::string& src);
        static std::string fetchTlv(const std::string& src, const std::string& tag);
        static std::string currencyCode(const std::string& src, const std::string& tag);
        static std::string fetchLv(const std::string& src, int lenchars);
        static std::string subString(const std::string& src, int start, int count);
        static std::string bcdTextToHexText(const std::string& src, int outputLength);
        
        void visa_checkFlag_to_DFC303_DFC308_DFC307(const std::string& tag);
    };
    
    json_t jmupsBuildCAPublicKey(const_json_t jmups, const std::string& hash) const;
    json_t jmupsBuildConfigure(const_json_t jmups, const std::string& hash) const;
    json_t  jmupsBuildMaster(const_json_t jmups) const;
    json_t  jmupsBuildVisa(const_json_t jmups) const;
    json_t  jmupsBuildAmex(const_json_t jmups) const;
    
    bytes_sp buildBytesAt(const std::string& key) const;
    bytes_sp buildBytes(const_json_t json) const;
    bytes_sp hexTextToBin(const std::string& text) const;
    bytes_sp lengthBytes(bytes_sp data) const;

    json_t saneiFixedData() const;

protected:
    
public:
            NFCTagConverterBluePad();
    virtual ~NFCTagConverterBluePad();
    
    void        loadJMupsData(const_json_t jmups);
    bytes_sp    getCAPublicKeyBytes() const;
    bytes_sp    getConfigureBytes() const;
    const_json_t  getMasterData() const { return m_masterData; }
};



#endif /* !defined(__h_NFCTagConverterBluePad__) */

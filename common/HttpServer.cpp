//
//  HttpServer.cpp
//  ppsdk
//
//  Created by tel on 2018/07/03.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "HttpServer.h"
#include "detect_ssl.hpp"
#include "server_certificate.hpp"
#include "ssl_stream.hpp"
#include "mime_type.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/beast/websocket.hpp>
#pragma GCC diagnostic pop

#include <boost/beast/version.hpp>
#include <boost/beast/core/ostream.hpp>
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/make_unique.hpp>
#include <boost/config.hpp>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <boost/regex.hpp>
#include <boost/bind.hpp>

using       tcp         = boost::asio::ip::tcp;     /* from <boost/asio/ip/tcp.hpp> */
namespace   ssl         = boost::asio::ssl;         /* from <boost/asio/ssl.hpp> */
namespace   http        = boost::beast::http;       /* from <boost/beast/http.hpp> */
namespace   websocket   = boost::beast::websocket;  /* from <boost/beast/websocket.hpp> */
using       strand_t    = boost::asio::strand<boost::asio::io_context::executor_type>;
using       path_sp_t   = boost::shared_ptr<boost::filesystem::path>;

/* ==========================================================================
 *  SSI処理
 *  サポート形式は下記のとおり
 *      <!--#include file="xxxx" -->
 *      <!--#include virtual="xxxx" -->
 * ========================================================================== */
std::string ssi(const boost::filesystem::path& path, path_sp_t doc_root)
{
    using attr_value_t = std::vector<std::pair<std::string, std::string>>;
    
    /* attributeとvalueを分解 */
    auto parse_attr_value = [](const std::string& s) -> attr_value_t {
        attr_value_t r;
        boost::regex expr1{R"***((\S+)\s*=\s*"(.*?)\")***"};    /* attribute="value" */
        boost::regex expr2{R"***((\S+)\s*=\s*'(.*?)\')***"};    /* attribute='value' */
        boost::regex expr3{R"***((\S+)\s*=\s*(\S.+))***"};      /* attribute=value */
        auto spos = 0;
        do{
            auto m = boost::make_shared<boost::smatch>();
            if(!boost::regex_search(s.cbegin() + spos, s.cend(), *m, expr1)){
                m = boost::make_shared<boost::smatch>();
                if(!boost::regex_search(s.cbegin() + spos, s.cend(), *m, expr2)){
                    m = boost::make_shared<boost::smatch>();
                    if(!boost::regex_search(s.cbegin() + spos, s.cend(), *m, expr3)){
                        break;
                    }
                }
            }
            r.push_back(attr_value_t::value_type{m->str(1), m->str(2)});
            spos += m->position() + m->length();
        } while(true);

        return r;
    };
 
    /* include 処理 */
    auto do_include = [&path, &doc_root](const attr_value_t& av) -> std::string{
        if(av.size() != 1){
            return "<!-- ssi parse error -->";
        }
        if(av[0].first == "file"){  /* 相対パス */
            return ssi(path.parent_path() / av[0].second, doc_root);
        }
        else if(av[0].first == "virtual"){  /* 絶対パス */
            return ssi(*doc_root / av[0].second, doc_root);
        }
        return "<!-- ssi parse error -->";
    };
    
    std::ifstream t(path.string());
    std::string org;
    
    t.seekg(0, std::ios::end);
    org.reserve(t.tellg());
    t.seekg(0, std::ios::beg);
    
    org.assign((std::istreambuf_iterator<char>(t)),
                std::istreambuf_iterator<char>());
    
    std::stringstream ss;
    auto spos = 0;
    
    /* <!--#element attribute=value attribute=value ... --> */
    boost::regex expr{"<!--#(\\S+)\\s+(.*?)-->"};
    do{
        boost::smatch m;
        if(!boost::regex_search(org.cbegin() + spos, org.cend(), m, expr)){
            ss << org.substr(spos);
            break;
        }
        ss << org.substr(spos, m.position());
        const auto& element = m.str(1);
        const auto& attr_value = parse_attr_value(m.str(2));
        if(element == "include"){
            ss << do_include(attr_value);
        }
        spos += m.position() + m.length();
    } while(true);
    return ss.str();
}


/* ==========================================================================
 *  GET / HEAD のリクエスト応答
 * ========================================================================== */
template<class Body, class Allocator, class Send>
    void handle_request(
       path_sp_t doc_root,
       http::request<Body, http::basic_fields<Allocator>>&& req,
       Send&& send
    )
{
    auto const server = "ppsdk httpd based on " BOOST_BEAST_VERSION_STRING;
    
    /* bodyをstringとする共通レスポンス処理 */
    auto const string_response = [&](http::status stat, std::string const& body){
        http::response<http::string_body> res{stat, req.version()};
        res.set(http::field::server, server);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = body;
        res.prepare_payload();
        return res;
    };
    
    /* bad request レスポンス */
    auto const bad_request = [&](boost::beast::string_view why){
        return string_response(http::status::bad_request, why.to_string());
    };
    
    /* not found レスポンス */
    auto const not_found = [&](boost::beast::string_view target){
        return string_response(http::status::not_found, "The resource '" + target.to_string() + "' was not found.");
    };
    
    /* server error レスポンス */
    auto const server_error = [&](boost::beast::string_view what){
        return string_response(http::status::internal_server_error, "An error occurred: '" + what.to_string() + "'");
    };
    
    if((req.method() != http::verb::get) && (req.method() != http::verb::head)){
        send(bad_request("Unknown HTTP-method"));
        return;
    }
    
    auto fname = [req](){
        auto p = req.target().to_string();
        auto ppos = p.find('?');
        if(ppos == std::string::npos){
            return p;
        }
        return p.substr(0, ppos);
    }();
    
    auto const& path = *doc_root / fname;
    
    if(!boost::filesystem::exists(path)){
        send(not_found(req.target()));
        return;
    }

    
    auto const&& mime_type = get_mime_type(path.extension().string());
    if(mime_type == "text/html"){
        send(string_response(http::status::ok, ssi(path, doc_root)));
    }
    else{
        boost::beast::error_code ec;
        http::file_body::value_type body;
        body.open(path.c_str(), boost::beast::file_mode::scan, ec);
        
        if(ec){
            send(server_error(ec.message()));
            return;
        }
        
        auto const size = body.size();
        
        if(req.method() == http::verb::head){
            http::response<http::empty_body> res{http::status::ok, req.version()};
            res.set(http::field::server, server);
            res.set(http::field::content_type, mime_type);
            res.content_length(size);
            res.keep_alive(req.keep_alive());
            send(std::move(res));
        }
        else if(req.method() == http::verb::get){
            http::response<http::file_body> res{
                std::piecewise_construct,
                std::make_tuple(std::move(body)),
                std::make_tuple(http::status::ok, req.version())
            };
            res.set(http::field::server, server);
            res.set(http::field::content_type, mime_type);
            res.content_length(size);
            res.keep_alive(req.keep_alive());
            send(std::move(res));
        }
    }
}


#include "AtomicObject.h"

/* ==========================================================================
 *  WebSocket 管理
 * ========================================================================== */
static AtomicObject<std::map<HttpServer::WebSocket*, HttpServer::WebSocket::sp>> g_webSockets;
static AtomicObject<bool> g_bEnableNewWebSocketConnection;


void webSocketManageAdd(HttpServer::WebSocket::sp sp)
{
    LogManager_Function();
    g_webSockets.modifyBlock([sp](auto& map){
        LogManager_AddInformationF("[ws:%p] webSocketManageAdd", reinterpret_cast<void*>(sp.get()));
        map[sp.get()] = sp;
    });
}

void webSocketManageDetach(HttpServer::WebSocket* s)
{
    LogManager_Function();
    g_webSockets.modifyBlock([s](auto& map){
        LogManager_AddInformationF("[ws:%p] webSocketManageDetach", reinterpret_cast<void*>(s));
        map.erase(s);
    });
}

void webSocketManageCloseAndDetachAll()
{
    LogManager_Function();
    g_webSockets.modifyBlock([](auto& map){
        for(auto ws : map){
            try{
                LogManager_AddInformationF("[ws:%p] webSocketManageCloseAndDetachAll", reinterpret_cast<void*>(ws.first));
                ws.first->close();
            }
            catch(...){
                
            }
        }
        map.clear();
    });
}



/* ==========================================================================
 *  WebSocket Session (base template)
 * ========================================================================== */
template<class Derived> class websocket_session : public HttpServer::WebSocket
{
private:
    auto derived() -> Derived&
    {
        return static_cast<Derived&>(*this);
    }
    
    boost::beast::multi_buffer  m_read_buffer;
    boost::beast::multi_buffer  m_write_buffer;
    char    m_ping_state = 0;
    
    boost::mutex      m_send_mtx;
    
protected:
    strand_t                    m_strand;
    boost::asio::steady_timer   m_timer;
    
public:
    explicit websocket_session(boost::asio::io_context& ioc) :
        m_strand(ioc.get_executor()),
        m_timer(ioc, (std::chrono::steady_clock::time_point::max)())
    {
        LogManager_AddInformationF("[ws:%p] create instance", reinterpret_cast<void*>(this));
    }
    
    virtual ~websocket_session()
    {
        LogManager_AddInformationF("[ws:%p] delete instance", reinterpret_cast<void*>(this));
    }
    
    virtual void close()
    {
        LogManager_AddInformationF("[ws:%p] close -> do_timeout", reinterpret_cast<void*>(this));
        /* 色々と試行を行なったが、非同期ソケト故、すぐに停止はできないため、timeoutを発生させて着地させることとした */
        derived().do_timeout();
    }
    
    template<class Body, class Allocator>
        void do_accept(http::request<Body, http::basic_fields<Allocator>> req)
    {
        LogManager_AddInformationF("[ws:%p] do_accept", reinterpret_cast<void*>(this));
        derived().ws().control_callback(boost::bind(
          &websocket_session::on_control_callback,
          this,
          boost::placeholders::_1,
          boost::placeholders::_2
        ));
        
        m_timer.expires_after(std::chrono::seconds(15));
        
        derived().ws().async_accept(
            req,
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &websocket_session::on_accept,
                    derived().shared_from_this(),
                    boost::placeholders::_1
                )
            )
         );
    }
    
    void on_accept(boost::system::error_code ec)
    {
        LogManager_AddInformationF("[ws:%p] on_accept", reinterpret_cast<void*>(this));
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        do_read();
    }
    
    void on_timer(boost::system::error_code ec)
    {
        //LogManager_Function();
        if(ec && ec != boost::asio::error::operation_aborted){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(m_timer.expiry() <= std::chrono::steady_clock::now()){
            if(derived().ws().is_open() && m_ping_state == 0){
                m_ping_state = 1;
                
                m_timer.expires_after(std::chrono::seconds(15));
                
                derived().ws().async_ping(
                    {},
                    boost::asio::bind_executor(
                        m_strand,
                        boost::bind(
                            &websocket_session::on_ping,
                            derived().shared_from_this(),
                            boost::placeholders::_1
                        )
                    )
                 );
            }
            else{
                derived().do_timeout();
                LogManager_AddInformationF("[ws:%p] detect timeout", reinterpret_cast<void*>(this));
                webSocketManageDetach(this);
                return;
            }
        }
        
        m_timer.async_wait
        (
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &websocket_session::on_timer,
                    derived().shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void activity()
    {
        //LogManager_Function();
        m_ping_state = 0;
        
        m_timer.expires_after(std::chrono::seconds(15));
    }
    
    void on_ping(boost::system::error_code ec)
    {
        //LogManager_Function();
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(m_ping_state == 1){
            m_ping_state = 2;
        }
        else{
            BOOST_ASSERT(m_ping_state == 0);
        }
    }
    
    void on_control_callback(
        websocket::frame_type kind,
        boost::beast::string_view payload)
    {
        //LogManager_Function();
        boost::ignore_unused(kind, payload);
        
        activity();
    }
    
    void do_read()
    {
        LogManager_AddInformationF("[ws:%p] do_read", reinterpret_cast<void*>(this));
        m_read_buffer.consume(m_read_buffer.size());
        derived().ws().async_read(
            m_read_buffer,
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &websocket_session::on_read,
                    derived().shared_from_this(),
                    boost::placeholders::_1,
                    boost::placeholders::_2
                )
            )
        );
    }
    
    void on_read(
        boost::system::error_code ec,
        std::size_t bytes_transferred)
    {
        LogManager_AddInformationF("[ws:%p] on_read", reinterpret_cast<void*>(this));
        boost::ignore_unused(bytes_transferred);
        
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec == websocket::error::closed){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
        }
        
        activity();

        if(derived().ws().got_text()){
            std::stringstream ss;
            ss << boost::beast::buffers(m_read_buffer.data());
            emitReceiveData(ss.str());
        }
        else{
            LogManager_AddError("binary data");
        }
        
        do_read();
    }
    
    virtual void send(const std::string& data)
    {
        LogManager_AddInformationF("[ws:%p] send", reinterpret_cast<void*>(this));
        if(!derived().ws().is_open()){
            LogManager_AddWarning("web socket stream is closed");
            return;
        }
        m_send_mtx.lock();

        m_write_buffer.consume(m_write_buffer.size());
        size_t n = buffer_copy(m_write_buffer.prepare(data.size()), boost::asio::buffer(data));
        m_write_buffer.commit(n);
        
        derived().ws().text(true);
        derived().ws().async_write(
            m_write_buffer.data(),
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &websocket_session::on_write,
                    derived().shared_from_this(),
                    boost::placeholders::_1,
                    boost::placeholders::_2
                )
            )
        );
    }
    
    void on_write(
         boost::system::error_code ec,
         std::size_t bytes_transferred)
    {
        LogManager_AddInformationF("[ws:%p] on_write", reinterpret_cast<void*>(this));
        m_send_mtx.unlock();

        boost::ignore_unused(bytes_transferred);
        
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
    }
};

/* ==========================================================================
 *  Plain WebSocket Session
 * ========================================================================== */
class plain_websocket_session :
    public websocket_session<plain_websocket_session>,
    public boost::enable_shared_from_this<plain_websocket_session>
{
private:
    websocket::stream<tcp::socket> m_ws;
    bool    m_close = false;
    
public:
    explicit plain_websocket_session(tcp::socket socket) :
        websocket_session<plain_websocket_session>(socket.get_executor().context()),
        m_ws(std::move(socket))
    {
        LogManager_Function();
    }
    
    virtual ~plain_websocket_session()
    {
        LogManager_Function();
    }
    
    auto ws() -> websocket::stream<tcp::socket>&
    {
        return m_ws;
    }
    
    template<class Body, class Allocator>
    void run(http::request<Body, http::basic_fields<Allocator>> req)
    {
        LogManager_AddInformationF("[ws:%p] run", reinterpret_cast<void*>(this));
        on_timer({});
        
        do_accept(std::move(req));
    }
    
    void do_timeout()
    {
        LogManager_AddInformationF("[ws:%p] do_timeout", reinterpret_cast<void*>(this));
        if(m_close){
            return;
        }
        m_close = true;
        
        m_timer.expires_after(std::chrono::seconds(15));
        
        m_ws.async_close(
            websocket::close_code::normal,
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &plain_websocket_session::on_close,
                    shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void on_close(boost::system::error_code ec)
    {
        LogManager_AddInformationF("[ws:%p] on_close", reinterpret_cast<void*>(this));
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
    }
};

/* ==========================================================================
 *  SSL WebSocket Session
 * ========================================================================== */
class ssl_websocket_session :
    public websocket_session<ssl_websocket_session>,
    public boost::enable_shared_from_this<ssl_websocket_session>
{
private:
    websocket::stream<ssl_stream<tcp::socket>> m_ws;
    strand_t    m_strand;
    bool        m_eof = false;
    
public:
    explicit ssl_websocket_session(ssl_stream<tcp::socket> stream) :
        websocket_session<ssl_websocket_session>(stream.get_executor().context()),
        m_ws(std::move(stream)),
        m_strand(m_ws.get_executor())
    {
        LogManager_Function();
    }
    
    virtual ~ssl_websocket_session()
    {
        LogManager_Function();
    }
    
    auto ws() -> websocket::stream<ssl_stream<tcp::socket>>&
    {
        return m_ws;
    }
    
    template<class Body, class Allocator>
        void run(http::request<Body, http::basic_fields<Allocator>> req)
    {
        LogManager_AddInformationF("[ws:%p] run", reinterpret_cast<void*>(this));
        on_timer({});
        
        do_accept(std::move(req));
    }
    
    void do_eof()
    {
        LogManager_AddInformationF("[ws:%p] do_eof", reinterpret_cast<void*>(this));
        m_eof = true;
        
        m_timer.expires_after(std::chrono::seconds(15));
        
        m_ws.next_layer().async_shutdown(
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &ssl_websocket_session::on_shutdown,
                    shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void on_shutdown(boost::system::error_code ec)
    {
        LogManager_AddInformationF("[ws:%p] on_shutdown", reinterpret_cast<void*>(this));
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
    }
    
    void do_timeout()
    {
        LogManager_AddInformationF("[ws:%p] do_timeout", reinterpret_cast<void*>(this));
        if(m_eof){
            return;
        }
        
        m_timer.expires_at((std::chrono::steady_clock::time_point::max)());
        on_timer({});
        do_eof();
    }
};

/* ==========================================================================
 *  Plain WebSocket 生成および実行
 * ========================================================================== */
template<class Body, class Allocator>
    void make_websocket_session(
        HttpServer::sp server,
        tcp::socket socket,
        http::request<Body, http::basic_fields<Allocator>> req)
{
    LogManager_Function();
    auto ws = boost::make_shared<plain_websocket_session>(std::move(socket));
    webSocketManageAdd(boost::static_pointer_cast<HttpServer::WebSocket>(ws));
    server->emitWebSocket(ws);
    ws->run(std::move(req));
}

/* ==========================================================================
 *  SSL WebSocket 生成および実行
 * ========================================================================== */
template<class Body, class Allocator>
    void make_websocket_session(
        HttpServer::sp server,
        ssl_stream<tcp::socket> stream,
        http::request<Body, http::basic_fields<Allocator>> req)
{
    LogManager_Function();
    auto ws = boost::make_shared<ssl_websocket_session>(std::move(stream));
    webSocketManageAdd(boost::static_pointer_cast<HttpServer::WebSocket>(ws));
    server->emitWebSocket(ws);
    ws->run(std::move(req));
}

/* ==========================================================================
 *  HTTP セッション（base template）
 * ========================================================================== */
template<class Derived> class http_session
{
private:
    auto derived() -> Derived&
    {
        return static_cast<Derived&>(*this);
    }
    
    class queue
    {
        enum
        {
            limit = 8
        };
        
        struct work
        {
            virtual ~work() = default;
            virtual void operator()() = 0;
        };
        
        http_session& m_self;
        std::vector<std::unique_ptr<work>> m_items;
        
    public:
        explicit queue(http_session& self) : m_self(self)
        {
            static_assert(limit > 0, "queue limit must be positive");
            m_items.reserve(limit);
        }
        
        bool is_full() const
        {
            return m_items.size() >= limit;
        }
        
        bool on_write()
        {
            BOOST_ASSERT(!m_items.empty());
            auto const was_full = is_full();
            m_items.erase(m_items.begin());
            if(!m_items.empty()){
                (*m_items.front())();
            }
            return was_full;
        }
        
        template<bool isRequest, class Body, class Fields>
            void operator()(http::message<isRequest, Body, Fields>&& msg)
        {
            struct work_impl : work
            {
                http_session& m_self;
                http::message<isRequest, Body, Fields> m_msg;
                
                work_impl(
                    http_session& self,
                    http::message<isRequest, Body, Fields>&& msg
                ) :
                    m_self(self),
                    m_msg(std::move(msg))
                {
                }
                
                void operator()()
                {
                    http::async_write(
                        m_self.derived().stream(),
                        m_msg,
                        boost::asio::bind_executor(
                            m_self.m_strand,
                            boost::bind(
                                &http_session::on_write,
                                m_self.derived().shared_from_this(),
                                boost::placeholders::_1,
                                m_msg.need_eof()
                            )
                        )
                    );
                }
            };
            
            m_items.push_back(
                boost::make_unique<work_impl>(m_self, std::move(msg))
            );
            
            if(m_items.size() == 1){
                (*m_items.front())();
            }
        }
    };
    
    http::request<http::string_body>        m_req;
    queue                                   m_queue;
    
protected:
    HttpServer::sp              m_server;
    boost::asio::steady_timer   m_timer;
    strand_t                    m_strand;
    boost::beast::flat_buffer   m_buffer;
    
public:
    http_session(
        HttpServer::sp              server,
        boost::beast::flat_buffer   buffer
    ) :
        m_server(server),
        m_queue(*this),
        m_timer(*server->ioc(), (std::chrono::steady_clock::time_point::max)()),
        m_strand(server->ioc()->get_executor()),
        m_buffer(std::move(buffer))
    {
    }
    
    virtual ~http_session()
    {
    }
    
    void do_read()
    {
        m_timer.expires_after(std::chrono::seconds(15));
        
        m_req = {};
        
        http::async_read(
            derived().stream(),
            m_buffer,
            m_req,
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &http_session::on_read,
                    derived().shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void on_timer(boost::system::error_code ec)
    {
        if(ec && ec != boost::asio::error::operation_aborted){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(m_timer.expiry() <= std::chrono::steady_clock::now()){
            derived().do_timeout();
            return;
        }
        
        m_timer.async_wait(
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &http_session::on_timer,
                    derived().shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void on_read(boost::system::error_code ec)
    {
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec == http::error::end_of_stream){
            derived().do_eof();
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(websocket::is_upgrade(m_req)){
            g_bEnableNewWebSocketConnection.referenceBlock([=](bool bEnableNewWebSocketConnection){
                if(bEnableNewWebSocketConnection){
                    make_websocket_session(
                                           m_server,
                                           derived().release_stream(),
                                           std::move(m_req)
                                           );
                }
                else{
                    LogManager_AddInformation("bEnableNewWebSocketConnection == false -> request cancel");
                    derived().do_eof();
                }
            });
            return;
        }
        
        handle_request(m_server->docroot(), std::move(m_req), m_queue);
        
        if(!m_queue.is_full()){
            do_read();
        }
    }
    
    void on_write(boost::system::error_code ec, bool close)
    {
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(close){
            derived().do_eof();
            return;
        }
        
        if(m_queue.on_write()){
            do_read();
        }
    }
};

/* ==========================================================================
 *  HTTP セッション
 * ========================================================================== */
class plain_http_session :
    public http_session<plain_http_session>,
    public boost::enable_shared_from_this<plain_http_session>
{
private:
    HttpServer::sp  m_server;
    tcp::socket     m_socket;
    strand_t        m_strand;
    
public:
    plain_http_session(
        HttpServer::sp              server,
        tcp::socket                 socket,
        boost::beast::flat_buffer   buffer
    ) :
        http_session<plain_http_session>(
            server,
            std::move(buffer)
        ),
        m_socket(std::move(socket)),
        m_strand(m_socket.get_executor()),
        m_server(server)
    {
    }
    
    virtual ~plain_http_session()
    {
    }
    
    auto stream() -> tcp::socket&
    {
        return m_socket;
    }
    
    auto release_stream() -> tcp::socket
    {
        return std::move(m_socket);
    }
    
    void run()
    {
        on_timer({});
        
        do_read();
    }
    
    void do_eof()
    {
        boost::system::error_code ec;
        m_socket.shutdown(tcp::socket::shutdown_send, ec);
    }
    
    void do_timeout()
    {
        boost::system::error_code ec;
        m_socket.shutdown(tcp::socket::shutdown_both, ec);
        m_socket.close(ec);
    }
};

/* ==========================================================================
 *  HTTPS セッション
 * ========================================================================== */
class ssl_http_session :
    public http_session<ssl_http_session>,
    public boost::enable_shared_from_this<ssl_http_session>
{
private:
    HttpServer::sp          m_server;
    ssl_stream<tcp::socket> m_stream;
    strand_t                m_strand;
    bool                    m_eof = false;
    
public:
    ssl_http_session(
        HttpServer::sp              server,
        tcp::socket                 socket,
        boost::beast::flat_buffer   buffer
    ) :
        http_session<ssl_http_session>(
            server,
            std::move(buffer)
        ),
        m_stream(std::move(socket), *server->sslctx()),
        m_strand(m_stream.get_executor()),
        m_server(server)
    {
    }
    
    virtual ~ssl_http_session()
    {
    }
    
    auto stream() -> ssl_stream<tcp::socket>&
    {
        return m_stream;
    }
    
    auto release_stream() -> ssl_stream<tcp::socket>
    {
        return std::move(m_stream);
    }
    
    void run()
    {
        on_timer({});
        
        m_timer.expires_after(std::chrono::seconds(15));
        
        m_stream.async_handshake(
            ssl::stream_base::server,
            m_buffer.data(),
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &ssl_http_session::on_handshake,
                    shared_from_this(),
                    boost::placeholders::_1,
                    boost::placeholders::_2
                )
            )
        );
    }
    
    void on_handshake(
         boost::system::error_code ec,
         std::size_t bytes_used)
    {
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        
        m_buffer.consume(bytes_used);
        
        do_read();
    }
    
    void do_eof()
    {
        m_eof = true;
        
        m_timer.expires_after(std::chrono::seconds(15));
        
        m_stream.async_shutdown(
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &ssl_http_session::on_shutdown,
                    shared_from_this(),
                    boost::placeholders::_1
                )
            )
        );
    }
    
    void on_shutdown(boost::system::error_code ec)
    {
        if(ec == boost::asio::error::operation_aborted){
            return;
        }
        
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
    }
    
    void do_timeout()
    {
        if(m_eof){
            return;
        }
        
        m_timer.expires_at((std::chrono::steady_clock::time_point::max)());
        on_timer({});
        do_eof();
    }
};

/* ==========================================================================
 *  acceptした後のセッション処理クラス
 * ========================================================================== */
class detect_session : public boost::enable_shared_from_this<detect_session>
{
private:
    HttpServer::sp  m_server;
    tcp::socket     m_socket;
    strand_t        m_strand;
    boost::beast::flat_buffer m_buffer;
    
public:
    explicit
    detect_session(
        HttpServer::sp  server,
        tcp::socket     socket
    ) :
        m_socket(std::move(socket)),
        m_server(server),
        m_strand(m_socket.get_executor())
    {
    }
    
    virtual ~detect_session()
    {
    }
    
    void run()
    {
        async_detect_ssl(
            m_socket,
            m_buffer,
            boost::asio::bind_executor(
                m_strand,
                boost::bind(
                    &detect_session::on_detect,
                    shared_from_this(),
                    boost::placeholders::_1,
                    boost::placeholders::_2
                )
            )
        );
        
    }
    
    void on_detect(boost::system::error_code ec, boost::tribool result)
    {
        if(ec){
            LogManager_AddError(ec.message());
            return;
        }
        
        if(result){
            boost::make_shared<ssl_http_session>(
                m_server,
                std::move(m_socket),
                std::move(m_buffer)
            )->run();
        }
        else{
            boost::make_shared<plain_http_session>(
                m_server,
                std::move(m_socket),
                std::move(m_buffer)
            )->run();
        }
    }
};

HttpServer::listener::listener(
    HttpServer::sp  server,
    tcp::endpoint   endpoint
) :
    m_server(server),
    m_endpoint(endpoint),
    m_acceptor(*server->ioc()),
    m_socket(*server->ioc())
{
    LogManager_Function();
}

/* virtual */
HttpServer::listener::~listener()
{
    LogManager_Function();
}

bool HttpServer::listener::prepare()
{
    LogManager_Function();
    
    boost::system::error_code ec;
    
    m_acceptor  = tcp::acceptor(*m_server->ioc());
    m_socket    = tcp::socket(*m_server->ioc());

    m_acceptor.open(m_endpoint.protocol(), ec);
    if(ec){
        LogManager_AddError(ec.message());
        return false;
    }
    
    m_acceptor.set_option(boost::asio::socket_base::reuse_address(true));
    if(ec){
        LogManager_AddError(ec.message());
        return false;
    }
    
    m_acceptor.bind(m_endpoint, ec);
    if(ec){
        LogManager_AddError(ec.message());
        return false;
    }
    
    m_acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
    if(ec){
        LogManager_AddError(ec.message());
        return false;
    }
    
    LogManager_AddInformation("listener prepare successful");
    return true;
}
    
void HttpServer::listener::run()
{
    LogManager_Function();

    if(!m_acceptor.is_open()){
        LogManager_AddError("listener closed");
        return;
    }
    do_accept();
}

void HttpServer::listener::stop()
{
    LogManager_Function();

    LogManager_AddInformation("socket close");
    m_socket.close();

    LogManager_AddInformation("acceptor close");
    m_acceptor.close();
}

void HttpServer::listener::do_accept()
{
    LogManager_Function();

    m_acceptor.async_accept(
        m_socket,
        boost::bind(
            &listener::on_accept,
            shared_from_this(),
            boost::placeholders::_1
        )
    );
}

void HttpServer::listener::on_accept(boost::system::error_code ec)
{
    LogManager_Function();

    if(ec){
        LogManager_AddError(ec.message());
        return;
    }
    else{
        boost::make_shared<detect_session>(m_server, std::move(m_socket))
            ->run();
    }
    
    do_accept();
}


static constexpr int kNumThreads = 4;

/* ==========================================================================
 *  HTTPサーバ
 * ========================================================================== */
HttpServer::HttpServer() :
    m_currentState(HttpServer::State::Stopped),
    m_stateSubject(HttpServer::State::Stopped),
    m_nextRequest(HttpServer::Request::Nothing)
{
    LogManager_Function();
}

/* virtual */
HttpServer::~HttpServer()
{
    LogManager_Function();
    
    if(m_ioc){
        m_ioc->stop();
    }
}

void HttpServer::begin(const std::string& address, uint16_t port, const boost::filesystem::path& doc_root)
{
    LogManager_Function();

    m_address   = address;
    m_port      = port;
    m_docroot   = boost::make_shared<boost::filesystem::path>(doc_root);
    m_sslctx    = boost::make_shared<ssl::context>(ssl::context::sslv23);
    
    trySetRequest(Request::Start);
}


auto HttpServer::startListener() -> pprx::observable<pprx::unit>
{
    auto self = shared_from_this();

    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([self](auto){
        return pprx::observable<>::create<pprx::unit>([self](pprx::subscriber<pprx::unit> s){
            try{
                LogManager_AddInformation("[httpd] create io_context");
                self->m_ioc  = boost::make_shared<boost::asio::io_context>(kNumThreads);

                LogManager_AddInformation("[httpd] create listener");
                self->m_listener = boost::make_shared<listener>
                (
                 self->shared_from_this(),
                 tcp::endpoint{
                     boost::asio::ip::make_address(self->m_address),
                     self->m_port
                 }
                 );

                if(!self->m_listener->prepare()){
                    s.on_error(make_error_internal("[httpd] prepare failure"));
                }
                LogManager_AddInformation("[httpd] m_listener->run()");
                self->m_listener->run();
                
                LogManager_AddInformation("[httpd] create threads");
                self->m_threads = boost::make_shared< boost::thread_group>();
                for(int i = 0; i < kNumThreads; i++){
                    self->m_threads->create_thread([self](){
                        self->m_ioc->run();
                    });
                }
                
                LogManager_AddInformation("[httpd] enable new web socket connection");
                g_bEnableNewWebSocketConnection.modifyBlock([=](bool& bEnableNewWebSocketConnection){
                    bEnableNewWebSocketConnection = true;
                });
                
                s.on_next(pprx::unit());
                s.on_completed();
            }
            catch(...){
                LogManager_AddError("[httpd] opening error : exception");
                s.on_error(make_error_internal("opening error"));
            }
        });
    }).as_dynamic();
}

auto HttpServer::stopListener() -> pprx::observable<pprx::unit>
{
    auto self = shared_from_this();
    
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([self](auto){
        LogManager_AddInformation("[httpd] disable new web socket connection");
        g_bEnableNewWebSocketConnection.modifyBlock([=](bool& bEnableNewWebSocketConnection){
            bEnableNewWebSocketConnection = false;
        });
        
        LogManager_AddInformation("[httpd] close all web socket");
        webSocketManageCloseAndDetachAll();
        
        /*
         まず最初にWebSocketを早めに停止して新規の接続を抑止する。
         次に、現在接続中のWebSocketを全て切断する。（前の手順で再接続は禁止されている）
         その後、数秒間はwebViewでhtmlコンテンツがローディングされている可能性があるので、
         HTTPサーバは機能を維持する。
         */
        LogManager_AddInformation("[httpd] final wait 2sec for loading html contents");
        BaseSystem::sleep(2000);
        
        LogManager_AddInformation("[httpd] stop listener");
        self->m_listener->stop();
        
        LogManager_AddInformation("[httpd] stop io_context");
        self->m_ioc->stop();
        
        LogManager_AddInformation("[httpd] threads.join_all()");
        self->m_threads->join_all();
        LogManager_AddInformation("[httpd] threads.join_all() done");
        
        LogManager_AddInformation("[httpd] delete threads");
        self->m_threads.reset();
        
        LogManager_AddInformation("[httpd] delete io_context");
        self->m_ioc.reset();
        
        LogManager_AddInformation("[httpd] delete listener");
        self->m_listener.reset();

        return pprx::just(pprx::unit());
    }).as_dynamic();
}

void HttpServer::trySetRequest(Request request)
{
    LogManager_AddInformationF("[httpd] trySetRequest(%s)", requestToString(request));

    auto self = shared_from_this();
    auto currentState = [self, request](){
        boost::unique_lock<boost::recursive_mutex> lock(self->m_stateChange_mtx);
        const auto oldState       = self->m_currentState;
        const auto oldNextRequest = self->m_nextRequest;
        switch(self->m_currentState){
            case State::Stopped:
            {
                if(request == Request::Start){
                    self->m_nextRequest  = Request::Nothing;
                    self->m_currentState = State::Prepare;
                    BaseSystem::instance().post([self](){
                        self->doPrepare();
                    });
                }
                else if(request == Request::Stop){
                    self->m_nextRequest = Request::Nothing;
                }
                break;
            }
                
            case State::Prepare:
            {
                if(request == Request::Start){
                    self->m_nextRequest = Request::Nothing;
                }
                else if(request == Request::Stop){
                    self->m_nextRequest = Request::Stop;
                }
                break;
            }

            case State::Running:
            {
                if(request == Request::Start){
                    self->m_nextRequest = Request::Nothing;
                }
                else if(request == Request::Stop){
                    self->m_nextRequest  = Request::Nothing;
                    self->m_currentState = State::Unprepare;
                    BaseSystem::instance().post([self](){
                        self->doUnprepare();
                    });
                }
                break;
            }

            case State::Unprepare:
            {
                if(request == Request::Start){
                    self->m_nextRequest = Request::Start;
                }
                else if(request == Request::Stop){
                    self->m_nextRequest = Request::Nothing;
                }
                break;
            }
        }
        LogManager_AddInformationF("[httpd] currentState(%s) nextRequest(%s) => request(%s) => modifiedRequest(%s) -> newState(%s)",
                                   self->stateToString(oldState) %
                                   self->requestToString(oldNextRequest) %
                                   self->requestToString(request) %
                                   self->requestToString(self->m_nextRequest) %
                                   self->stateToString(self->m_currentState) 
                                   );
        return self->m_currentState;
    }();
    m_stateSubject.get_subscriber().on_next(currentState);
}

void HttpServer::doPrepare()
{
    LogManager_Function();
    auto self = shared_from_this();
    startListener()
    .subscribe
    ([self](auto){
        LogManager_AddInformation("[httpd] doUnprepare() subscribe : next");
        auto currentState = [self](){
            boost::unique_lock<boost::recursive_mutex> lock(self->m_stateChange_mtx);
            if(self->m_nextRequest == Request::Stop){
                self->m_currentState = State::Unprepare;
                BaseSystem::instance().post([self](){
                    self->doUnprepare();
                });
            }
            else{
                self->m_currentState = State::Running;
            }
            return self->m_currentState;
        }();
        self->m_stateSubject.get_subscriber().on_next(currentState);
    }, [self](auto err){
        LogManager_AddError("[httpd] doUnprepare() subscribe : error");
    }, [self](){
        LogManager_AddInformation("[httpd] doUnprepare() subscribe : completed");
    });
}

void HttpServer::doUnprepare()
{
    LogManager_Function();
    auto self = shared_from_this();
    stopListener()
    .subscribe
    ([self](auto){
        LogManager_AddInformation("[httpd] doUnprepare() subscribe : next");
        auto currentState = [self](){
            boost::unique_lock<boost::recursive_mutex> lock(self->m_stateChange_mtx);
            if(self->m_nextRequest == Request::Start){
                self->m_currentState = State::Prepare;
                BaseSystem::instance().post([self](){
                    self->doPrepare();
                });
            }
            else{
                self->m_currentState = State::Stopped;
            }
            return self->m_currentState;
        }();
        self->m_stateSubject.get_subscriber().on_next(currentState);
    }, [self](auto err){
        LogManager_AddError("[httpd] doUnprepare() subscribe : error");
        
    }, [self](){
        LogManager_AddInformation("[httpd] doUnprepare() subscribe : completed");
    });
}

void HttpServer::suspend()
{
    LogManager_Function();
    BaseSystem::instance().post([=](){
        trySetRequest(Request::Stop);
    });
}

void HttpServer::resume()
{
    LogManager_Function();
    BaseSystem::instance().post([=](){
        trySetRequest(Request::Start);
    });
}

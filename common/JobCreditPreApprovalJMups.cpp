//
//  JobCreditPreApprovalJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobCreditPreApprovalJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "JMupsJobStorage.h"

JobCreditPreApprovalJMups::JobCreditPreApprovalJMups(const __secret& s) :
    JobCreditSalesJMups(s)
{
    LogManager_Function();
}

/* virtual */
JobCreditPreApprovalJMups::~JobCreditPreApprovalJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobCreditPreApprovalJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    m_bIsMSFallback = false;
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.credit").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return pprx::maybe::success(pprx::unit());
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging(m_taggingSelector).as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()      
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobCreditPreApprovalJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::PreApproval;
}

/* virtual */
auto JobCreditPreApprovalJMups::processDcc() -> pprx::observable<pprx::unit>
{
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    return hpsDHSLookup
    (
     m_tradeData.get<int>("amount"),
     taxOtherAmount ? (*taxOtherAmount) : 0,
     prmCommon::operationDiv::PreApproval,
     false).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto dccTradeTargetJudgeResult = resp.body.get<std::string>("normalObject.dccTradeTargetJudgeResult");
        if(dccTradeTargetJudgeResult == "1"){
            m_tradeData.put<bool>("bDccTrade", true);
        }
        return pprx::unit();
    }).as_dynamic();
}

/* virtual */
auto JobCreditPreApprovalJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    return pprx::start<TradeInfoBuilder>([=](){
        TradeInfoBuilderJMupsCredit builder;
        {
            auto amount = m_tradeData.get_optional<int>("amount");
            builder.addAmount(TradeInfoBuilder::attribute::required, amount);
        }
        
        auto&& tradeInfo = getTradeStartInfo();
        const bool isNeedTaxOther = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
        const bool isNeedProductCode = tradeInfo.get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
        
        if(isNeedTaxOther){
            auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
            builder.addTaxOtherAmount(TradeInfoBuilder::attribute::required, taxOtherAmount);
        }
        
        if(isNeedProductCode){
            auto productCode = m_tradeData.get_optional<std::string>("productCode");
            builder.addProductCode(TradeInfoBuilder::attribute::required, productCode);
        }
        
        return builder;
    });
}

/* virtual */
auto JobCreditPreApprovalJMups::sendTradeConfirm(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    if(!taxOtherAmount) taxOtherAmount = 0;

    auto productCode = m_tradeData.get_optional<std::string>("productCode");
    if(!productCode){
        productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
    }
            
    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");

    return hpsCreditPreApproval_Confirm
    (
     selector,
     (bDccTrade && (*bDccTrade)) ?
     prmCredit::dcc::Yes :
     prmCredit::dcc::No,
     m_tradeData.get<int>("amount"),
     *taxOtherAmount,
     *productCode
     );
}

/* virtual */
auto JobCreditPreApprovalJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmCredit::doubleTrade>
{
    LogManager_Function();
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(prmCredit::doubleTrade::No)
        .as_dynamic();
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmCredit::doubleTrade::Yes);
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        
        return prmCredit::doubleTrade::Yes;
    })
    .as_dynamic();
}

/* virtual */
auto JobCreditPreApprovalJMups::sendTagging(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    auto bDccTrade = m_tradeData.get_optional<bool>("bDccTrade");
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsCreditPreApproval_Tagging
        (
         selector,
         (bDccTrade && (*bDccTrade)) ?
         prmCredit::dcc::Yes :
         prmCredit::dcc::No,
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmCredit::printResultDiv::Success
         );
    }).as_dynamic();
}

/* virtual */
void JobCreditPreApprovalJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobCreditPreApprovalJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "3");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

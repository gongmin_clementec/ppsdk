//
//  JMupsAccessCredit.hpp
//  ppsdk
//
//  Created by tel on 2017/06/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JMupsAccessCredit__)
#define __h_JMupsAccessCredit__

#include "JMupsAccess.h"

class JMupsAccessCredit :
    virtual public JMupsAccess
{
private:
    
protected:
    struct prmCredit
    {
        enum class doubleTrade
        {
            Bad,
            No,
            Yes
        };
        
        enum class dcc {
            Bad,
            Yes,
            No
        };
        
        enum class payWayDiv {
            Bad,
            Lump,
            Installment,
            Bonus,
            BonusCombine,
            Revolving
        };

        enum class printResultDiv {
            Bad,
            Success,
            Failure
        };
        
        enum class isSendingDesiSlip {
            Bad,
            ShoudBeSending, /* 送信対象伝票（カード会社控未印字） */
            Unnecessarily   /* 送信対象外伝票（カード会社控印字済） */
        };
    };
    

    
    auto hpsCreditSales_Confirm
    (
     prmCredit::doubleTrade selector,
     prmCredit::dcc         dcc,
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode,
     prmCredit::payWayDiv   payWayDiv,
     boost::optional<int>   partitionTimes,
     boost::optional<int>   firsttimeClaimMonth,
     bool                   isRePrint,
     boost::optional<int>   couponSelectListNum,
     boost::optional<int>   couponApplyPreviousAmount,
     boost::optional<int>   couponApplyAfterAmount,
     boost::optional<std::string> firstGACCmdRes,
     boost::optional<int>   bonusTimes,
     boost::optional<int>   bonusMonth1,
     boost::optional<int>   bonusAmount1,
     boost::optional<int>   bonusMonth2,
     boost::optional<int>   bonusAmount2,
     boost::optional<int>   bonusMonth3,
     boost::optional<int>   bonusAmount3,
     boost::optional<int>   bonusMonth4,
     boost::optional<int>   bonusAmount4,
     boost::optional<int>   bonusMonth5,
     boost::optional<int>   bonusAmount5,
     boost::optional<int>   bonusMonth6,
     boost::optional<int>   bonusAmount6,
     boost::optional<int>   point,
     boost::optional<int>   exchangeablePoint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsCreditSales_Tagging
    (
     prmCredit::doubleTrade        selector,
     prmCredit::dcc                dcc,
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip,
     bool                                       isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmKIDInput
    {
        enum class manualDiv {
            Bad,
            NonManual,
            Manual
        };
    };
    
    auto hpsKIDInput
    (
     const std::string&         kid,
     prmCommon::operationDiv    operationDiv,
     prmKIDInput::manualDiv     manualDiv
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    
    auto hpsDHSLookup
    (
     int                        amount,
     int                        taxOtherAmount,
     prmCommon::operationDiv operationDiv,
     bool                       isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsDccAsCheck
    (
     const std::string&         approvalNo,
     int                        amount,
     int                        taxOtherAmount
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsDccSelect()
     -> pprx::observable<JMupsAccess::response_t>;
    
    
    auto hpsCreditPreApproval_Confirm
    (
     prmCredit::doubleTrade selector,
     prmCredit::dcc         dcc,
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode
     ) -> pprx::observable<JMupsAccess::response_t>;


    auto hpsCreditPreApproval_Tagging
    (
     prmCredit::doubleTrade     selector,
     prmCredit::dcc             dcc,
     const std::string&         slipNo,
     prmCredit::printResultDiv  printResultDiv
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsCreditAfterApproval_Confirm
    (
     prmCredit::doubleTrade selector,
     prmCredit::dcc         dcc,
     const std::string&     approvalNo,
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode,
     prmCredit::payWayDiv   payWayDiv,
     boost::optional<int>   partitionTimes,
     boost::optional<int>   firsttimeClaimMonth,
     bool                   isRePrint,
     boost::optional<int>   couponSelectListNum,
     boost::optional<int>   couponApplyPreviousAmount,
     boost::optional<int>   couponApplyAfterAmount,
     boost::optional<std::string> firstGACCmdRes,
     boost::optional<int>   bonusTimes,
     boost::optional<int>   bonusMonth1,
     boost::optional<int>   bonusAmount1,
     boost::optional<int>   bonusMonth2,
     boost::optional<int>   bonusAmount2,
     boost::optional<int>   bonusMonth3,
     boost::optional<int>   bonusAmount3,
     boost::optional<int>   bonusMonth4,
     boost::optional<int>   bonusAmount4,
     boost::optional<int>   bonusMonth5,
     boost::optional<int>   bonusAmount5,
     boost::optional<int>   bonusMonth6,
     boost::optional<int>   bonusAmount6,
     boost::optional<int>   point,
     boost::optional<int>   exchangeablePoint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsCreditAfterApproval_Tagging
    (
     prmCredit::doubleTrade        selector,
     prmCredit::dcc                dcc,
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip,
     bool                          isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsCreditAfterApproval_DCCSearchTrade
    (
     const std::string&     approvalNo,
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode,
     const std::string&     slipNo,
     boost::optional<std::string> otherTermJudgeNo
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditAfterApproval_DCCRejectConfirm
    (
     const std::string&     approvalNo,
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_SearchTrade
    (
     const std::string&     slipNo
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_CancelConfirm
    (
     const std::string&     slipNo
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_CancelTagging
    (
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_ReturnConfirm
    (
     int                    amount,
     int                    taxOtherAmount,
     const std::string&     productCode,
     const std::string&     slipNo,
     prmCredit::payWayDiv   payWayDiv,
     boost::optional<int>   partitionTimes,
     boost::optional<int>   firsttimeClaimMonth
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_ReturnTagging
    (
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_ReturnTagging
    (
     prmCredit::dcc                dcc,
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_DCCSearchTrade
    (
     const std::string& otherTermJudgeNo,
     const std::string&     slipNo
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    auto hpsCreditRefund_DCCCancelConfirm
    (
     int                amount,
     int                taxOtherAmount,
     const std::string& slipNo
     ) -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditRefund_DCCReturnConfirm
    (
     int                amount,
     int                taxOtherAmount,
     const std::string& slipNo
     ) -> pprx::observable<JMupsAccess::response_t>;

    struct prmCreditRefund_DCCTagging
    {
        enum class judgementTerm {
            Bad,
            Self,
            Other
        };
    };
    auto hpsCreditRefund_DCCTagging
    (
     prmCreditRefund_DCCTagging::judgementTerm    judgeTerm,
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv,
     boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    
    auto hpsCreditCardCheck_Confirm() -> pprx::observable<JMupsAccess::response_t>;

    auto hpsCreditCardCheck_Tagging
    (
     const std::string&            slipNo,
     prmCredit::printResultDiv     printResultDiv
    ) -> pprx::observable<JMupsAccess::response_t>;

private:
    void replaceTrainingData
    (
     const int amount,                      // [in]
     const int taxOtherAmount,              // [in]
     const std::string&     productCode,    // [in]
     prmCredit::payWayDiv   payWayDiv,      // [in]
     keyvalue_t& replaces                   // [out]
    );
    
public:
            JMupsAccessCredit();
    virtual ~JMupsAccessCredit();
    
};

#endif /* !defined(__h_JMupsAccessCredit__) */

//
//  JobNFCReprintSlipJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobNFCReprintSlipJMups__)
#define __h_JobNFCReprintSlipJMups__

#include "PaymentJob.h"
#include "JMupsAccessReprint.h"

class JobNFCReprintSlipJMups :
    public PaymentJob,
    public JMupsAccessReprint
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;
    
public:
            JobNFCReprintSlipJMups(const __secret& s);
    virtual ~JobNFCReprintSlipJMups();
    
    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobNFCReprintSlipJMups__) */

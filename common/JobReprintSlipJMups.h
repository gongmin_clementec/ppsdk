//
//  JobReprintSlipJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobReprintSlipJMups__)
#define __h_JobReprintSlipJMups__

#include "PaymentJob.h"
#include "JMupsAccessReprint.h"
#include "JMupsSlipPrinter.h"

class JobReprintSlipJMups :
    public PaymentJob,
    public JMupsAccessReprint,
    public JMupsSlipPrinter
{
private:
    prmReprintCommon::paymentType m_paymentType;
    
private:
    auto treatSlipDocumentJson(response_t resp) -> pprx::observable<const_json_t>;
    auto addMediaDivToCardNo(const_json_t tradeData) -> json_t;

protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;

public:
            JobReprintSlipJMups(const __secret& s);
    virtual ~JobReprintSlipJMups();
    
    virtual bool isCardReaderRequired() const { return false; };
};



#endif /* !defined(__h_JobReprintSlipJMups__) */

//
//  JobCreditRefundJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobCreditRefundJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"

JobCreditRefundJMups::JobCreditRefundJMups(const __secret& s) :
    JobCreditSalesJMups(s)
{
    LogManager_Function();
    m_bDccUseOnOffDiv  = false; // カードDCC利用不可
    m_bDCCTradeDiv     = false; // DCC引取じゃない
    m_bSearchResultDiv = false; // 未取得
}

/* virtual */
JobCreditRefundJMups::~JobCreditRefundJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobCreditRefundJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.credit").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()

    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging().as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()     
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobCreditRefundJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::Refund;
}

/* virtual */
auto JobCreditRefundJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    LogManager_Function();

    auto slipNo = boost::make_shared<std::string>();
    auto terminalId = boost::make_shared<std::string>(getTerminalInfo().get<std::string>("sessionObject.termJudgeNo"));
    auto dccUseOnOffDiv = m_cardInfoData.get_optional<std::string>("normalObject.dccUseOnOffDiv");
    if(dccUseOnOffDiv && (*dccUseOnOffDiv == "1")){
        m_bDccUseOnOffDiv = true;
    }
    else{
        m_bDccUseOnOffDiv = false;
    }
    auto&& tradeInfo = getTradeStartInfo();
    const bool isNeedTaxOther = tradeInfo.get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
    const bool isNeedProductCode = tradeInfo.get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
    
    return pprx::start_async<TradeInfoBuilder>([=](){
        TradeInfoBuilder builder;
        builder.addSlipNo(TradeInfoBuilder::attribute::required, *slipNo);
        return builder;
    }).as_dynamic()
    .flat_map([=](TradeInfoBuilder builder){
        return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "Search").as_dynamic();
    }).as_dynamic()
    .flat_map([=](const_json_t uiResp){
        *slipNo = uiResp.get<std::string>("results.slipNo");
        return hpsCreditRefund_SearchTrade(*slipNo).as_dynamic();
    })
    .flat_map([=](JMupsAccess::response_t searchResp){
        auto&& searchResultDiv = searchResp.body.get<std::string>("normalObject.searchResultDiv");
        m_bSearchResultDiv = searchResultDiv == "1";

        auto &&strSlipNo = *slipNo.get();
        m_tradeData.put("slipNo"        , strSlipNo);
        if(m_bSearchResultDiv){
            /* 検索成功 */
            m_refundSearchTradeResp     = searchResp.body.clone();
            auto&& pan                  = searchResp.body.get<std::string>("normalObject.pan");
            auto&& tradeDate            = searchResp.body.get<std::string>("normalObject.tradeDate");
            auto&& amountCancel         = searchResp.body.get<std::string>("normalObject.amountCancel");
            auto taxOtherAmountCancel   = searchResp.body.get<std::string>("normalObject.taxOtherAmountCancel");
            if(!isNeedTaxOther){
                taxOtherAmountCancel = "0";
            }
            auto&& productCodeCancel    = searchResp.body.get<std::string>("normalObject.productCodeCancel");

            m_tradeData.put("pan"                   , pan);
            m_tradeData.put("tradeDate"             , tradeDate);
            m_tradeData.put("amount"                ,::atoi(amountCancel.c_str()));
            m_tradeData.put("taxOtherAmount"        ,::atoi(taxOtherAmountCancel.c_str()));
            m_tradeData.put("productCode"           ,productCodeCancel);
            
            TradeInfoBuilderJMupsCredit builder;
            /* 通常取引 */
            builder.addPAN                (TradeInfoBuilder::attribute::readonly, pan);
            builder.addSlipNo             (TradeInfoBuilder::attribute::readonly, *slipNo);
            builder.addTradeDate          (TradeInfoBuilder::attribute::readonly, tradeDate);
            builder.addAmount             (TradeInfoBuilder::attribute::required, ::atoi(amountCancel.c_str()));
            if(isNeedTaxOther){
                builder.addTaxOtherAmount (TradeInfoBuilder::attribute::required, ::atoi(taxOtherAmountCancel.c_str()));
            }
            if(isNeedProductCode){
                builder.addProductCode    (TradeInfoBuilder::attribute::readonly, productCodeCancel);
            }
            builder.addPaymentWayForRefund(TradeInfoBuilder::attribute::readonly, searchResp.body, m_cardInfoData);
            builder.addSearchResultDiv    (TradeInfoBuilder::attribute::readonly, searchResultDiv);
            return pprx::maybe::success(builder);     
        }
        else{ // 検索失敗
            return pprx::maybe::error(make_error_internal("No Refund SLIPNO")).as_dynamic();
        }
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        try{ std::rethrow_exception(err); }
        catch(pprx::ppex_paymentserver& ex){
            return callOnDispatchWithMessageAndSubParameter("refundSearchError", "", ex.json())
            .on_error_resume_next([=](auto errorOnDispath){
                try{ std::rethrow_exception(err); }
                catch(pprx::ppex_aborted&){
                    return pprx::error<pprx::unit>(err);
                }
                return pprx::error<pprx::unit>(errorOnDispath);
            }).as_dynamic()
            .flat_map([=](auto){
                return pprx::maybe::retry().as_dynamic();
            }).as_dynamic()
            .on_error_resume_next([=](auto uiErr){
                return pprx::maybe::error(uiErr).as_dynamic();
            }).as_dynamic();
        }
        catch(pprx::ppexception& ppex){
            LogManager_AddInformationF("ppexception\t%s", ppex.json().str());
            return pprx::maybe::error(make_error_aborted("aborted")).as_dynamic();
        }
        catch(...){
            LogManager_AddError("rethrow_exception");
        }
        
        return pprx::maybe::error(err).as_dynamic();
    }).as_dynamic()
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<TradeInfoBuilderJMupsCredit>();
    }).as_dynamic()
    .map([=](TradeInfoBuilderJMupsCredit builder){
        return builder.asBaseClass();
    }).as_dynamic();
}


auto JobCreditRefundJMups::processDcc() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto JobCreditRefundJMups::sendTradeConfirm(prmCredit::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& slipNo         = m_tradeData.get<std::string>("slipNo");
    auto&& amount         = m_tradeData.get<int>("amount");
    auto&& taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
    auto taxOtherAmountUI = taxOtherAmount?boost::lexical_cast<int>(*taxOtherAmount):0; // ユーザー入力
    
    if(PaymentJob::isTrainingMode()){
        m_bSearchResultDiv = false;
    }
    
    if(m_bSearchResultDiv){ //取引が検索成功
        auto&& amountCancel     = m_refundSearchTradeResp.get<std::string>("normalObject.amountCancel");
        auto&& taxOtherAmountResp = m_refundSearchTradeResp.get_optional<std::string>("normalObject.taxOtherAmountCancel");
        auto taxOtherAmountCancel = taxOtherAmountResp?boost::lexical_cast<int>(*taxOtherAmountResp):0;  // JMupsセンターからデータ
        if( (amount==::atoi(amountCancel.c_str()))       // 金額と税、全て同じの場合、取消になる。
           && (taxOtherAmountUI== taxOtherAmountCancel)){
            if(m_bDCCTradeDiv){
                return hpsCreditRefund_DCCCancelConfirm(amount, taxOtherAmountUI, slipNo);
            }
            else{
                return hpsCreditRefund_CancelConfirm( slipNo );
            }
        }
    }
    
    /// 以下は返品
    if(m_bDCCTradeDiv){  // DCC取引
        return hpsCreditRefund_DCCReturnConfirm(amount, taxOtherAmountUI, slipNo);
    }
    else{
        auto productCode = m_tradeData.get_optional<std::string>("productCode");
        if(!productCode){
            productCode = getTerminalInfo().get<std::string>("sessionObject.productCode");
        }
        
        prmCredit::payWayDiv payWayDivRefund = prmCredit::payWayDiv::Bad;
        boost::optional<int>   partitionTimes;
        boost::optional<int>   firsttimeClaimMonth;
        auto payWayDivCancel = m_refundSearchTradeResp.get_optional<std::string>("normalObject.payWayDivCancel");
        if(payWayDivCancel&&(!PaymentJob::isTrainingMode())){
            auto payWayDiv = ::atoi((*payWayDivCancel).c_str());
            switch(payWayDiv){
                case 0:
                    payWayDivRefund = prmCredit::payWayDiv::Lump;
                    break;
                case 1:
                    payWayDivRefund = prmCredit::payWayDiv::Installment;
                    break;
                case 2:
                    payWayDivRefund = prmCredit::payWayDiv::Bonus;
                    break;
                case 3:
                    payWayDivRefund = prmCredit::payWayDiv::BonusCombine;
                    break;
                case 4:
                    payWayDivRefund = prmCredit::payWayDiv::Revolving;
                    break;
                default :
                    payWayDivRefund = prmCredit::payWayDiv::Bad;
            }
            
            auto partitionTimesCancel = m_refundSearchTradeResp.get_optional<std::string>("normalObject.partitionTimesCancel");
            if(partitionTimesCancel){
                partitionTimes = boost::lexical_cast<int>(*partitionTimesCancel);
            }
            auto firsttimeClaimMonthCancel = m_refundSearchTradeResp.get_optional<std::string>("normalObject.firsttimeClaimMonth");
            if(firsttimeClaimMonthCancel){
                firsttimeClaimMonth = boost::lexical_cast<int>(*firsttimeClaimMonthCancel);
            }
        }
        else{ // 手動で入力の場合
            payWayDivCancel     = m_tradeData.get_optional<std::string>("paymentWay");
            if(0==(*payWayDivCancel).compare("lump")){
                payWayDivRefund = prmCredit::payWayDiv::Lump;
            }
            else if(0==(*payWayDivCancel).compare("installment")){
                payWayDivRefund = prmCredit::payWayDiv::Installment;
            }
            else if(0==(*payWayDivCancel).compare("bonus")){
                payWayDivRefund = prmCredit::payWayDiv::Bonus;
            }
            else if(0==(*payWayDivCancel).compare("bonusCombine")){
                payWayDivRefund = prmCredit::payWayDiv::BonusCombine;
            }
            else if(0==(*payWayDivCancel).compare("revolving")){
                payWayDivRefund = prmCredit::payWayDiv::Revolving;
            }
            else{
                LogManager_AddError("unknown paymentWay");
                throw_error_internal("unknown paymentWay");
            }
            
            partitionTimes      = m_tradeData.get_optional<int>("partitionTimes");
            firsttimeClaimMonth = m_tradeData.get_optional<int>("firsttimeClaimMonth");
        }

        return hpsCreditRefund_ReturnConfirm
        (
         amount, taxOtherAmountUI, *productCode, slipNo,
         payWayDivRefund, partitionTimes, firsttimeClaimMonth
         );
    }
}

/* virtual */
auto JobCreditRefundJMups::sendTagging()
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();

    auto&& tradeResultData  = getJMupsStorage()->getTradeResultData().clone();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    prmCredit::isSendingDesiSlip  bSendingDiv = prmCredit::isSendingDesiSlip::Bad;
    auto&& digiReceiptDiv = tradeResultData.get<std::string>("normalObject.digiReceiptDiv");
    if(("1" == digiReceiptDiv)      // 電子伝票保管対象（サイン要）
       || "2" == digiReceiptDiv){  // 電子伝票保管対象（サイン不要）
        bSendingDiv = prmCredit::isSendingDesiSlip::Unnecessarily;
    }
    else{
        bSendingDiv = prmCredit::isSendingDesiSlip::ShoudBeSending;
    }
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        auto&& TRADEDIV = tradeResultData.get<std::string>("normalObject.tradeResult.TRADE");
        if(m_bDCCTradeDiv){ //DCC取引の場合
            if(0==TRADEDIV.compare("2")){ //返品
                return hpsCreditRefund_DCCTagging(prmCreditRefund_DCCTagging::judgementTerm::Other, slipNo, prmCredit::printResultDiv::Success, bSendingDiv);
            }
            else{ //取消
                return hpsCreditRefund_DCCTagging(prmCreditRefund_DCCTagging::judgementTerm::Self, slipNo, prmCredit::printResultDiv::Success, bSendingDiv);;
            }
        }
        else{
            if(0==TRADEDIV.compare("2")){ //返品
                return hpsCreditRefund_ReturnTagging(slipNo, prmCredit::printResultDiv::Success, bSendingDiv);
            }
            else{ //取消
                return hpsCreditRefund_CancelTagging(slipNo, prmCredit::printResultDiv::Success, bSendingDiv);
            }
        }
    }).as_dynamic();
}

/* virtual */
void JobCreditRefundJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
}

/* virtual */
auto JobCreditRefundJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    json_t json;
    json.put("operationDiv", "1");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

/* virtual */
auto JobCreditRefundJMups::emvOnNetwork(const std::string& url, const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    return hpsAccessFromEmvKernel(url, param)
    .map([=](auto resp){
        if(url == "ma0770.do?mA0770_1=aaa"){
            beginAsyncPreInputAmount();
        }
        else if(url == "ma0770.do?mA0770_3=aaa"){
            /*
             売上以外 EMVKernel は Level 1 相当で動作する。
             会社判定に失敗しても、磁気と同じ処理を走行させる。
             */
            m_cardInfoData = resp.body.clone();
        }
        else if(url == "sa1770.do?sA1770_3=aaa"){
            m_cardInfoData = resp.body.clone();
        }
        return resp.body;
    });
}

auto JobCreditRefundJMups::waitForStoreTradeResultData(JMupsAccess::response_t jmupsData)
-> pprx::observable<prmCredit::doubleTrade>
{
    LogManager_Function();
    
    auto tradeData = addMediaDivToCardNo(jmupsData.body);
    m_tradeConfirmFinished.put("hps", tradeData);
    tradeData.put("ppsdk.bTradeCompletionFlag", false);
    getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
    return pprx::just(prmCredit::doubleTrade::No).as_dynamic();
}

/* virtual */
auto JobCreditRefundJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    LogManager_AddDebug(data.str());
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::Credit,
     getOperationDiv(),
     m_bIsMSFallback,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_cardInfoData = resp.body.clone();
        return processCommon();
    }).as_dynamic();
}

/* virtual */
auto JobCreditRefundJMups::proceedContact(const_json_t data)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return getCardReader()->beginICCardTransaction()
    .flat_map([=](auto){
        return emvRunLevel1(getJMupsStorage());
    }).as_dynamic()
    .flat_map([=](auto){
        return processCommon();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->endICCardTransaction();
    });
}

auto JobCreditRefundJMups::processCommon() -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        LogManager_AddDebug(m_cardInfoData.str());
        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
            .as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            return waitForKidInput()
            .map([=](auto){
                return pprx::unit();
            }).as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTradeInfo("Confirm").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        if(m_bDccUseOnOffDiv){
            return processDcc().as_dynamic();
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTradeConfirm(prmCredit::doubleTrade::No).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForStoreTradeResultData(jmups).as_dynamic(); // 取消返品の時、二重確認不要
    }).as_dynamic()
    .flat_map([=](auto selector){
        m_taggingSelector = selector;
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::error<pprx::unit>(err).as_dynamic();
    }).as_dynamic();
}

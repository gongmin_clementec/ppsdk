//
//  JobActivationJMups.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobActivationJMups__)
#define __h_JobActivationJMups__

#include "PaymentJob.h"
#include "JMupsAccessSetting.h"

class JobActivationJMups :
    public PaymentJob,
    public JMupsAccessSetting
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobActivationJMups(const __secret& s);
    virtual ~JobActivationJMups();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobActivationJMups__) */

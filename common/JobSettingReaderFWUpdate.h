//
//  JobSettingReaderFWUpdate.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingReaderFWUpdate__)
#define __h_JobSettingReaderFWUpdate__

#include <stdio.h>
#include "PaymentJob.h"

class TMSAccess;

class JobSettingReaderFWUpdate :
    public PaymentJob
{
private:
    boost::shared_ptr<TMSAccess> m_tms;
    
    auto doUpdate(const std::string& url) -> pprx::observable<pprx::unit>;
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobSettingReaderFWUpdate(const __secret& s);
    virtual ~JobSettingReaderFWUpdate();

    virtual bool isJobTimeoutRequired() const;
    virtual bool isCardReaderRequired() const { return true; };
};

#endif /* !defined(__h_JobSettingReaderFWUpdate__) */

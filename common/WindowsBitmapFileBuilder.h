//
//  WindowsBitmapFileBuilder.h
//  ppsdk
//
//  Created by tel on 2017/07/27.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#ifndef __h_WindowsBitmapFileBuilder__
#define __h_WindowsBitmapFileBuilder__

#include <iostream>
#include <boost/function.hpp>

template <typename SRC_PIX_TYPE>
class WindowsBitmapFileBuilder
{
private:
    using src_pix_t = SRC_PIX_TYPE;
    const std::size_t src_pix_bytes = sizeof(SRC_PIX_TYPE);
    
    /* little endian */
    struct BITMAPFILEHEADER
    {
        uint16_t    bfType;
        uint32_t    bfSize;
        uint16_t    bfReserved1;
        uint16_t    bfReserved2;
        uint32_t    bfOffBits;
    } __attribute__((packed));
    
    struct BITMAPINFOHEADER
    {
        uint32_t    biSize;
        int32_t     biWidth;
        int32_t     biHeight;
        uint16_t    biPlanes;
        uint16_t    biBitCount;
        uint32_t    biCompression;
        uint32_t    biSizeImage;
        int32_t     biXPelsPerMeter;
        int32_t     biYPelsPerMeter;
        uint32_t    biClrUsed;
        uint32_t    biClrImportant;
    } __attribute__((packed));
    
    struct RGBQUAD
    {
        uint8_t     rgbBlue;
        uint8_t     rgbGreen;
        uint8_t     rgbRed;
        uint8_t     rgbReserved;
    } __attribute__((packed));
    
    
    struct BITMAPFILE
    {
        BITMAPFILEHEADER    bf;
        BITMAPINFOHEADER    bi;
    } __attribute__((packed));
    
protected:
    
    
public:
    WindowsBitmapFileBuilder() = default;
    
    void grayImageToBinBitmap(std::ostream& os, const void* pixData, uint32_t width, uint32_t height, uint32_t rowBytes, boost::function<bool(src_pix_t)> predIsBlack ) const
    {
        const auto dstRowBytes      = ((((width + 7) / 8) + 3) / 4) * 4;
        const auto paddingBytes     = dstRowBytes - ((width + 7) / 8);
        
        constexpr auto offsetToData     = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 2;
        const auto fileSize = offsetToData + height * dstRowBytes;
        
        {
            BITMAPFILEHEADER bfh;
            bfh.bfType          = 0x4D42;
            bfh.bfSize          = static_cast<uint32_t>(fileSize);
            bfh.bfReserved1     = 0;
            bfh.bfReserved2     = 0;
            bfh.bfOffBits       = offsetToData;
            os.write(reinterpret_cast<const char*>(&bfh), sizeof(bfh));
        }
        
        {
            BITMAPINFOHEADER bih;
            bih.biSize          = 40;
            bih.biWidth         = static_cast<int32_t>(width);
            bih.biHeight        = - static_cast<int32_t>(height);
            bih.biPlanes        = 1;
            bih.biBitCount      = 1;
            bih.biCompression   = 0;
            bih.biSizeImage     = static_cast<uint32_t>(dstRowBytes * height);
            bih.biXPelsPerMeter = 0;
            bih.biYPelsPerMeter = 0;
            bih.biClrUsed       = 0;
            bih.biClrImportant  = 0;
            os.write(reinterpret_cast<const char*>(&bih), sizeof(bih));
        }
        
        {
            RGBQUAD black, white;
            white.rgbBlue       = 0xFF;
            white.rgbGreen      = 0xFF;
            white.rgbRed        = 0xFF;
            white.rgbReserved   = 0;
            black.rgbBlue       = 0;
            black.rgbGreen      = 0;
            black.rgbRed        = 0;
            black.rgbReserved   = 0;
            os.write(reinterpret_cast<const char*>(&white), sizeof(white));
            os.write(reinterpret_cast<const char*>(&black), sizeof(black));
        }
        
        for(auto y = 0; y < height; y++){
            auto _s = reinterpret_cast<const src_pix_t*>(reinterpret_cast<const uint8_t*>(pixData) + y * rowBytes);
            auto x = 0;
            auto bit = 0;
            uint8_t bdata = 0;
            while(x < width){
                bdata |= predIsBlack(*_s) ? 1 : 0;
                bit++;
                if(bit == 8){
                    os << bdata;
                    bdata = 0;
                    bit = 0;
                }
                else{
                    bdata <<= 1;
                }
                x++;
                _s++;
            }
            if(bit != 0){
                os << bdata;
            }
            for(auto i = 0; i < paddingBytes; i++) os << uint8_t(0);
        }
    }
    
    
    
};

#endif /* __h_WindowsBitmapFileBuilder__ */

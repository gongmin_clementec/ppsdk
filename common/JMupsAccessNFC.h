//
//  JMupsAccessNFC.h
//  ppsdk
//
//  Created by tel on 2017/06/12.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JMupsAccessNFC__)
#define __h_JMupsAccessNFC__

#include "JMupsAccess.h"


class JMupsAccessNFC :
    virtual public JMupsAccess
{
private:
    
protected:
    
    struct prmNFCCommon
    {
        enum class nfcDeviceType { Bad, Panasonic, Miura };
        enum class cardBrandIdentifier
        {
            Bad,
            VISA,
            MASTER,
            AMEX,
            JCB
        };
        static cardBrandIdentifier stringToCardBrandIdentifier(const std::string& str);

        enum class resultCode{ Bad, Success };
        enum class payWayDiv { Bad, Lump };

        enum class printResultDiv { Bad, Success, Failure };
        enum class isSendingDegiSlip    /* JMupsが"Digi"を"Degi"とtypoしているのであって、間違いではない */
        {
            Bad,
            ShoudBeSending, /* 送信対象伝票（カード会社控未印字） */
            Unnecessarily   /* 送信対象外伝票（カード会社控印字済） */
        };
        
    };
    
    struct prmMasterDataDownload    /* xAAA0101 */
    {
        enum class downloadType {
            Bad,
            Full,
            Difference
        };
    };
    
    auto hpsMasterDataDownload
    (
     prmNFCCommon::nfcDeviceType            nfcDeviceType,
     prmMasterDataDownload::downloadType    downloadType
    ) -> pprx::observable<JMupsAccess::response_t>;
    

    struct hpsUpdateMasterDataDate
    {
        enum class nfcDeviceType { Bad, Panasonic, Miura };
    };
    
    auto hpsUpdateMasterDataDate
    (
     prmNFCCommon::nfcDeviceType            nfcDeviceType,
     boost::optional<std::string>           kernelCommonUpdateDate,
     boost::optional<std::string>           kernelC1UpdateDate,
     boost::optional<std::string>           kernelC2UpdateDate,
     boost::optional<std::string>           kernelC3UpdateDate,
     boost::optional<std::string>           kernelC4UpdateDate,
     boost::optional<std::string>           kernelC5UpdateDate,
     boost::optional<std::string>           icCardTableUpdateDate,
     boost::optional<std::string>           caKeyFile1UpdateDate,
     boost::optional<std::string>           merchantTableUpdateDate
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmNFCRead
    {
    };
    auto hpsNFCRead
    (
     prmNFCCommon::nfcDeviceType        nfcDeviceType,
     prmNFCCommon::resultCode           resultCode,
     const std::string&                 resultCodeExtended,
     const std::string&                 transactionResultBit,
     prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
     const std::string&                 nfcTradeResult,
     const std::string&                 outcomeParameter,
     boost::optional<std::string>       nfcTradeResultExtended,
     boost::optional<std::string>       keySerialNoForResult,
     boost::optional<std::string>       keySerialNoForExtended
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmNFCSales_Confirm
    {
        enum class selector {
            Bad,
            Normal,
            Double
        };
    };
    auto hpsNFCSales_Confirm
    (
     prmNFCSales_Confirm::selector      selector,
     prmNFCCommon::nfcDeviceType        nfcDeviceType,
     prmNFCCommon::resultCode           resultCode,
     const std::string&                 resultCodeExtended,
     int                                amount,
     int                                taxOtherAmount,
     const std::string&                 productCode,
     prmNFCCommon::payWayDiv            payWayDiv,
     bool                               isRePrint,
     const std::string&                 transactionResultBit,
     prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
     const std::string&                 nfcTradeResult,
     const std::string&                 outcomeParameter,
     boost::optional<std::string>       nfcTradeResultExtended,
     boost::optional<std::string>       keySerialNoForResult,
     boost::optional<std::string>       keySerialNoForExtended,
     boost::optional<int>               couponSelectListNum,
     boost::optional<int>               couponApplyPreviousAmount,
     boost::optional<int>               point,
     boost::optional<int>               exchangeablePoint
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmNFCSales_Tagging
    {
        enum class issureScriptResultDiv
        {
            Bad,
            Unexecuted,
            Success,
            Failure
        };
    };
    auto hpsNFCSales_Tagging
    (
     const std::string&                     slipNo,
     prmNFCCommon::printResultDiv           printResultDiv,
     bool                                   isRePrint,
     boost::optional<prmNFCCommon::isSendingDegiSlip>               isSendingDegiSlip,
     boost::optional<prmNFCSales_Tagging::issureScriptResultDiv>    issureScriptResultDiv,
     boost::optional<prmNFCCommon::nfcDeviceType>                   nfcDeviceType,
     boost::optional<std::string>       nfcTradeResult,
     boost::optional<std::string>       nfcTradeResultExtended,
     boost::optional<std::string>       keySerialNoForResult,
     boost::optional<std::string>       keySerialNoForExtended
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmNFCSearchTrade
    {
    };
    auto hpsNFCSearchTrade
    (
        const std::string&  slipNo,
        bool                isRePrint
     ) -> pprx::observable<JMupsAccess::response_t>;

    struct prmNFCSearchTradeWithCard
    {
    };
    auto hpsNFCSearchTradeWithCard
    (
     prmNFCCommon::nfcDeviceType        nfcDeviceType,
     prmNFCCommon::resultCode           resultCode,
     const std::string&                 resultCodeExtended,
     const std::string&                 slipNo,
     int                                amount,
     int                                taxOtherAmount,
     const std::string&                 productCode,
     prmNFCCommon::payWayDiv            payWayDiv,
     bool                               isRePrint,
     const std::string&                 transactionResultBit,
     prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
     const std::string&                 nfcTradeResult,
     const std::string&                 outcomeParameter,
     boost::optional<std::string>       nfcTradeResultExtended,
     boost::optional<std::string>       keySerialNoForResult,
     boost::optional<std::string>       keySerialNoForExtended
     ) -> pprx::observable<JMupsAccess::response_t>;
    
    struct prmNFCRefund
    {
        enum class undoDiv { Bad, Undo, ReturningGoods };
    };
    auto hpsNFCRefund
    (
     prmNFCCommon::nfcDeviceType        nfcDeviceType,
     prmNFCRefund::undoDiv              undoDiv,
     prmNFCCommon::resultCode           resultCode,
     const std::string&                 resultCodeExtended,
     const std::string&                 slipNo,
     int                                amount,
     int                                taxOtherAmount,
     const std::string&                 productCode,
     prmNFCCommon::payWayDiv            payWayDiv,
     bool                               isRePrint,
     const std::string&                 transactionResultBit,
     prmNFCCommon::cardBrandIdentifier  cardBrandIdentifier,
     const std::string&                 nfcTradeResult,
     const std::string&                 outcomeParameter,
     boost::optional<std::string>       nfcTradeResultExtended,
     boost::optional<std::string>       keySerialNoForResult,
     boost::optional<std::string>       keySerialNoForExtended
     ) -> pprx::observable<JMupsAccess::response_t>;

    struct prmNFCRefund_Tagging
    {
    };
    auto hpsNFCRefund_Tagging
    (
     const std::string&             slipNo,
     bool                           isRePrint,
     prmNFCCommon::printResultDiv   printResultDiv,
     boost::optional<prmNFCCommon::isSendingDegiSlip>   isSendingDegiSlip
     ) -> pprx::observable<JMupsAccess::response_t>;

    
    
public:
            JMupsAccessNFC();
    virtual ~JMupsAccessNFC();
    
};



#endif /* !defined(__h_JMupsAAccessNFC__) */

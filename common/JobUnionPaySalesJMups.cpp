//
//  JobUnionPaySalesJMups.cpp
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobUnionPaySalesJMups.h"
#include "AbstructFactory.h"
#include "PPConfig.h"
#include "pprx-observable-from.h"
#include "CardReader.h"
#include "JMupsJobStorage.h"

JobUnionPaySalesJMups::JobUnionPaySalesJMups(const __secret& s) :
    PaymentJob(s),
    m_bIsMSFallback(false),
	m_bICCardType(false),
    JMupsEmvKernel(JMupsEmvKernel::Type::UnionPay),
    m_emvAmountPreInputUI(UIBehaviorState::Bad)
{
    LogManager_Function();
    
    m_taggingSelector = prmUnionPay::doubleTrade::No;
}

/* virtual */
JobUnionPaySalesJMups::~JobUnionPaySalesJMups()
{
    LogManager_Function();
    
}

/* virtual */
auto JobUnionPaySalesJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::start_async<pprx::unit>([=](){
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return tradeStart().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithMessage("connectingToRW").as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto cardReaderOption = PPConfig::instance().getChild("job.unionpay").clone().as_readonly();
        return processCardJob(cardReaderOption);
    }).as_dynamic()
    
    /* 以降、上流で発生したエラーを無視する（take(1) だけではエラーが複数透過されるため値発行する） */
    .flat_map([=](auto success){
        return pprx::maybe::success(success);
    })
    .on_error_resume_next([=](auto err){
        return pprx::maybe::error(err);
    })
    .take(1)
    .flat_map([=](pprx::maybe m){
        return connectAbortBySystemError(m);
    }).as_dynamic()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    }).as_dynamic()
    .flat_map([=](auto){
        beginAsyncRemovingContact();
        return processSendDigiSign().as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return treatSlipDocumentJson()
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return sendTagging(m_taggingSelector).as_dynamic()
        .flat_map([=](auto){
            return pprx::just(pprx::unit()).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
        json_t json;
        json.put("status", "success");
        json.put("result.hps", tradeResultData.clone());
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

auto JobUnionPaySalesJMups::processCardJob(const_json_t cardReaderOption)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    m_bIsMSFallback = false;
    auto strReaderDisplayTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_APPROVED");
    auto cardJobProcess = pprx::subjects::behavior<std::string>(strReaderDisplayTxt);
    
    return pprx::just(pprx::unit()).as_dynamic()
    .flat_map([=](auto){
        return cardJobProcess.get_observable()
        .flat_map([=](std::string strTxt) {
            /* カードリーダーの接続待ち */
            return waitForReaderConnectAndReadMedia(cardReaderOption, strTxt).as_dynamic();
        }).as_dynamic()
        .flat_map([=](const_json_t data){
            /* カード読み込み（磁気・IC）と後続のJMups関連の処理 */
            return processMagneticStripeOrContact(data).as_dynamic();
        }).as_dynamic()
        /*
         IC業務から磁気フォールバックする場合、カード抜去が発生するため、
         ファールバックエラー（警告）が発生したのちのカード抜去エラーを
         フィルタするため、take(1) する。
         */
        .flat_map([=](auto success){
            return pprx::maybe::success(success);
        }).as_dynamic()
        .on_error_resume_next([=](std::exception_ptr err){
            return pprx::maybe::error(err);
        }).as_dynamic()
        .take(1)
        .flat_map([=](pprx::maybe m){
            return m.observableForContinue<pprx::unit>();
        }).as_dynamic()
        .flat_map([=](auto success){
            return pprx::maybe::success(success);
        }).as_dynamic()
        .on_error_resume_next([=](std::exception_ptr err){
            /* フォールバックの場合にはリトライを行う */
            std::string strReaderKey("");
            try{std::rethrow_exception(err);}
            catch(pprx::ppex_paymentserver& perr){
                auto errjson = perr.json().get("result");
                if(isRetryCardReadWarning(errjson,strReaderKey)){
                    return getCardReader()->displayTextNoLf(getDisplayMsgForReader(strReaderKey)).as_dynamic()
                    .flat_map([=](auto){
                        return callOnDispatchWithCommandAndSubParameter("waitForRetryCardRead", "", perr.json())
                        .flat_map([=](auto){
                            std::string strReaderTxt("");
                            if(strReaderKey == "MIURA_CREDIT_UI_CARD_INSERT_ERROR"){
                                strReaderTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_IC_CARD_INSERT");
                            }
                            else if(strReaderKey == "MIURA_CREDIT_UI_CARD_SWIPE_ERROR"){
                                m_bIsMSFallback = true;
                                strReaderTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_MS_CARD_SWIPE");
                            }
                            else{
                                strReaderTxt = getDisplayMsgForReader("MIURA_CREDIT_UI_APPROVED");
                            }
                            cardJobProcess.get_subscriber().on_next(strReaderTxt);
                            return pprx::maybe::retry();
                        });
                    }).as_dynamic()
                    .on_error_resume_next([=](auto err2){
                        return getCardReader()->displayTextNoLf(getDisplayMsgForReader(strReaderKey)).as_dynamic()
                        .flat_map([=](auto) {
                            return pprx::maybe::error(err2);
                        }).as_dynamic();
                    }).as_dynamic();
                }
            }
            catch(pprx::ppex_cardrw& perrRw){
                if(m_bIsMSFallback){
                    auto cardStatus = perrRw.json().get<std::string>("result.detail.description");
                    if(cardStatus == "cardExist"){
                        json_t j = perrRw.json().clone();
                        j.remove_key_if_exist("result.detail.description");
                        j.put("result.detail.errorObject.errorCode", "PP_E2004");
                        j.put("result.detail.errorObject.errorLevel", "ERROR");
                        j.put("result.detail.errorObject.errorMessage", "cardExist");
                        j.put("result.detail.errorObject.errorType", "ERROR");
                        j.put("result.detail.resultCode", "1");
                        
                        return callOnDispatchWithCommandAndSubParameter("waitForMSFBCardExist", "", j)
                        .flat_map([=](const_json_t resp){
                            const bool bRetry = resp.get<bool>("bRetry");
                            return bRetry ? pprx::maybe::retry() : pprx::maybe::error(make_error_aborted("aborted")).as_dynamic();
                        }).as_dynamic()
                        .on_error_resume_next([=](auto err){
                            LogManager_AddDebug(pprx::exceptionToString(err));
                            return pprx::maybe::error(err);
                        }).as_dynamic();
                    }
                }
            }
            catch(...){
            }
            return pprx::maybe::error(err);
        }).as_dynamic()
        .retry()
        .flat_map([=](pprx::maybe m){
            return m.observableForContinue<pprx::unit>();
        }).as_dynamic()
        .take(1);
    }).as_dynamic();
}


auto JobUnionPaySalesJMups::treatSlipDocumentJson()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    
    auto pinfo = tradeResultData.get("normalObject.printInfo.device.value").clone();
    auto values = pinfo.get<json_t::array_t>("values");
    auto types = pinfo.get<json_t::array_t>("type");
    
    /* カードMEDIA_DIVを追加　ICかMSか */
    {
        for(auto v : values){
            auto mediaDiv = v.get<std::string>("PHYSICAL_MEDIA_DIV");
            auto cardNo   = v.get<std::string>("CARD_NO");
            auto cardNoWithMediaDiv = mediaDiv + " " + cardNo;
            v.put("CARD_NO", cardNoWithMediaDiv);
        }
    }
    /* 税・その他が不要な場合削除する　*/
    {
        auto taxOtherAmount = m_tradeData.get_optional<int>("taxOtherAmount");
        if(!taxOtherAmount){
            for(auto v : values){
                v.remove_key_if_exist("TAX");
            }
        }
    }
    
    /* カード会社控えが不要な場合削除する。必要なら、サインする */
    {
        auto isNeedAquirerSlip = tradeResultData.get_optional<bool>("ppsdk.isNeedAquirerSlip");
        if(isNeedAquirerSlip){
            if(!(*isNeedAquirerSlip)){
                for(int n = 0; n < values.size(); n++){
                    if(values[n].get<std::string>("RECEIPT_DIV") == "4"){
                        values.erase(values.begin() + n);
                        types.erase(types.begin() + n);
                        break;
                    }
                }
            }
            else{ // 手書きサイン
                for(auto v : values){
                    auto sign = v.get_optional<std::string>("SIGN");
                    if(sign){
                        v.put("SIGN", "1");
                    }
                }
            }
        }
    }
    
    /* クーポン情報の埋め込み */
    {
        auto&& tradeResult = tradeResultData.get("normalObject.tradeResult");
        auto COUPON_ID = tradeResult.get_optional<std::string>("COUPON_ID");
        if(COUPON_ID && (!COUPON_ID->empty())){
            auto SETTLEDAMOUNT = ::atoi(tradeResult.get<std::string>("SETTLEDAMOUNT").c_str());
            auto DISCOUNT_PRICE = tradeResult.get<int>("DISCOUNT_PRICE");
            auto ORGAMOUNT = std::string("\\") + BaseSystem::commaSeparatedString(SETTLEDAMOUNT + DISCOUNT_PRICE);
            auto strDISCOUNT_PRICE = std::string("-\\") + BaseSystem::commaSeparatedString(::abs(DISCOUNT_PRICE));
            
            auto COUPON_MEMBER_ID       = tradeResult.get_optional("COUPON_MEMBER_ID");
            auto COUPON_POSS_TIMES      = tradeResult.get_optional("MAXIMUM_USE_POSS_TIMES");
            auto COUPON_DONE_TIMES      = tradeResult.get_optional("USE_DONE_TIMES");
            auto COUPON_1               = tradeResult.get_optional<std::string>("COUPON_1");
            auto COUPON_2               = tradeResult.get_optional<std::string>("COUPON_2");
            
            auto COUPON_TEXT = [=](){
                std::stringstream ss;
                if(COUPON_1) ss << (*COUPON_1);
                if(COUPON_2){
                    if(COUPON_1) ss << "\n";
                    ss << (*COUPON_2);
                }
                return ss.str();
            }();
            
            for(auto v : values){
                v.put("_COUPON", true);
                v.put("_COUPON_ID"       , (*COUPON_ID));
                v.put("ORGAMOUNT"        , ORGAMOUNT);         /* 元のAMOUNTを新しい項目に書く */
                v.put("AMOUNT"           , tradeResult.get<std::string>("SETTLEDAMOUNT"));     /* 元のAMOUNTを書き直す */
                v.put("_DISCOUNT_PRICE"  , strDISCOUNT_PRICE);
                if(COUPON_MEMBER_ID && (!COUPON_MEMBER_ID->is_null_or_empty())){
                    auto member_id = COUPON_MEMBER_ID->get<std::string>();
                    if(!member_id.empty()){
                        v.put("_COUPON_MEMBER_ID", member_id);
                    }
                }
                if(COUPON_POSS_TIMES && (!COUPON_POSS_TIMES->is_null_or_empty())){
                    auto poss_times = COUPON_POSS_TIMES->get<int>();
                    if(poss_times > 0){
                        v.put("_COUPON_POSS_TIMES", boost::lexical_cast<std::string>(poss_times));
                        if(COUPON_DONE_TIMES && (!COUPON_DONE_TIMES->is_null_or_empty())){
                            auto done_times = COUPON_DONE_TIMES->get<int>();
                            v.put("_COUPON_DONE_TIMES", boost::lexical_cast<std::string>(done_times));
                        }
                    }
                }
                if(!COUPON_TEXT.empty()){
                    v.put("_COUPON_TEXT", COUPON_TEXT);
                }
            }
        }
    }
    
    /* 取引不成立の時、取消返品の場合、元伝票番号もプリントする　*/
    {
        auto failureInfo = tradeResultData.get_optional<std::string>("normalObject.tradeResult.FAILURE_INFO");
        if(failureInfo){
            auto cupTradeDiv = tradeResultData.get<std::string>("normalObject.tradeResult.TRADE");
            if(0==cupTradeDiv.compare("1") ||  // 取消
               0==cupTradeDiv.compare("2")){   // 返品
                for(auto v : values){
                    v.put("_FAILURE_INFO", "1");
                }
            }
        }
    }
    
    /* printInfoのvalueを再構築 */
    pinfo.put("values", values);
    pinfo.put("type", types);
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobUnionPaySalesJMups::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    LogManager_Function();
    
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

/* virtual */
auto JobUnionPaySalesJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}

auto JobUnionPaySalesJMups::tradeStart() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return hpsTradeStart_Request
    (
     JMupsAccess::prmTradeStart_Request::serviceDiv::UnionPay,
     boost::none,
     boost::none
     )
    .map([=](auto resp){
        m_tradeStartResponse = resp.body.clone();
        return pprx::unit();
    });
}

auto JobUnionPaySalesJMups::processMagneticStripeOrContact(const_json_t data)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(data)
    .flat_map([=](const_json_t json){
        m_cardStartData = json.clone();
        if(json.empty()){
            return pprx::error<pprx::unit>(make_error_internal("json is null or empty."))
            .as_dynamic();
        }
        auto cardType  = json.get<std::string>("cardType");
        auto status    = json.get<std::string>("status");
        
        json_t dispatchParam;
        dispatchParam.put("cardType", cardType);
        if(cardType == "contact"){
            m_bICCardType = true;
            return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
            .flat_map([=](auto){
                return getCardReader()->errorOnDeviceDisconnected(pprx::unit())
                .flat_map([=](pprx::unit){
                    return getCardReader()->errorOnICCardRemoved(pprx::unit());
                }).as_dynamic()
                .flat_map([=](auto){
                    return proceedContact(json).as_dynamic();
                }).as_dynamic()
                .take(1);
            }).as_dynamic();
        }
        else if(cardType == "magneticStripe"){
            m_bICCardType = false;
            return callOnDispatchWithMessageAndSubParameter("cardDetected", "", dispatchParam)
            .flat_map([=](auto){
                return processMagneticStripe(json).as_dynamic();
            }).as_dynamic();
        }
        
        return pprx::error<pprx::unit>(make_error_internalf("no handler. cardType=%s, status=%s", cardType % status)).as_dynamic();
    }).as_dynamic();
}

/* virtual */
JMupsAccess::prmCommon::operationDiv JobUnionPaySalesJMups::getOperationDiv() const
{
    return prmCommon::operationDiv::Sales;
}

auto JobUnionPaySalesJMups::processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto loop = pprx::subjects::behavior<int>(0);
    m_tradeConfirmFinished.clear();
    
    LogManager_AddDebug(data.str());
    const auto keySerialNumber = data.get<std::string>("keySerialNumber");
    const auto encryptedTrackData = data.get_optional<std::string>("encryptedTrackData");
    boost::shared_ptr<boost::optional<std::string>> pin = boost::make_shared<boost::optional<std::string>>();
    return hpsMSCardRead
    (
     prmMSCardRead::settlementMediaDiv::UnionPay,
     getOperationDiv(),
     m_bIsMSFallback,
     false,
     keySerialNumber,
     boost::none,
     boost::none,
     (encryptedTrackData ? boost::optional<std::string>(encryptedTrackData.get()) : boost::none),
     boost::none,
     std::string("010"),
     std::string("02")
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_cardInfoData = resp.body.clone();
        auto judgeCardcoResult = m_cardInfoData.get_optional<std::string>("normalObject.judgeCardcoResult");
        if(!judgeCardcoResult){
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY")).as_dynamic()
            .flat_map([=](auto){
                return pprx::error<pprx::unit>(make_error_internal("normalObject.judgeCardcoResult not found"))
                .as_dynamic();
            }).as_dynamic();
        }
        else if(judgeCardcoResult.get() == "1"){
            throw_error_internal("UnionPay does not support KID.");
        }
        return pprx::just(pprx::unit()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return waitForTradeInfo("Confirm");
    }).as_dynamic()
    .flat_map([=](auto){
        json_t jparam;
        jparam.put("amount", m_tradeData.get<int>("amount"));
        jparam.put("application.label", "556E696F6E506179");
        jparam.put("uiMessage.message1", "暗証番号を入力してください。");
        jparam.put("uiMessage.message2", "");
        return loop.get_observable()
        .flat_map([=](auto nEnterPinCnt) {
            return emvOnCardInputEncryptedOnlinePin(jparam)
            .flat_map([=](const_json_t pinResult){
                auto result = pinResult.get<std::string>("result");
                if(result == "retry"){
                    json_t errorJson;
                    errorJson.put("description", "pinTimeOut");
                    return pprx::error<JMupsAccess::response_t>(make_error_cardrw(errorJson)).as_dynamic();
                }
                if(result == "success"){
                    *pin = pinResult.get<std::string>("data");
                }
                return sendTradeConfirm(prmUnionPay::doubleTrade::No, *pin)
                .flat_map([=](JMupsAccess::response_t confirmResult) mutable {
                    auto pinCheckResult = confirmResult.body.get_optional<std::string>("normalObject.pinCheckResult");
                    if(pinCheckResult && (*pinCheckResult == "1")){ /* PINチェック失敗 */
                        m_tradeConfirmFinished.put("hps", addMediaDivToCardNo(confirmResult.body));
                        m_pinParam.put("pinResult", "PinFail-deny");
                        m_pinParam.put("bOnetimeRemain", false);
                        if(result == "bypass"){ // PINバイパス失敗です。
                            m_pinParam.put("pinResult", "PinFail-bypass");
                        }
                        return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", m_pinParam).as_dynamic()
                        .flat_map([=](const_json_t resp){
                            const bool bContinue = resp.get<bool>("bContinue");
                            if(bContinue){
                                    loop.get_subscriber().on_next(nEnterPinCnt + 1);
                                    return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic()
                                    .flat_map([=](auto){
                                        return pprx::never<JMupsAccess::response_t>().as_dynamic();
                                    }).as_dynamic();
                            }
                            else{
                                return pprx::error<JMupsAccess::response_t>(make_error_aborted("aborted")).as_dynamic();
                            }
                        }).as_dynamic();
                    }
                    else{
                        auto bIsFallback = confirmResult.body.get<bool>("normalObject.isFallback");
                        if(bIsFallback){
                            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_SKIP_FAILURE")).as_dynamic()
                            .flat_map([=](auto) {
                                json_t json;
                                json.put("errorCode", "PP_E3001");
                                json.put("description", "このカードはICでのお取り引きはできません。/n 磁気カードリーダを使用してください。");
                                return pprx::error<JMupsAccess::response_t>(make_error_pinreleted(json)).as_dynamic();
                            }).as_dynamic();
                        }
                        else{
                            m_pinParam.put("pinResult", "success");
                            m_pinParam.put("bOnetimeRemain", false);
                            return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", m_pinParam).as_dynamic()
                            .flat_map([=](const_json_t resp){
                                const bool bContinue = resp.get<bool>("bContinue");
                                if(bContinue){
                                    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
                                    .map([=](auto) {
                                        return confirmResult;
                                    }).as_dynamic();
                                }
                                else{
                                    return pprx::error<JMupsAccess::response_t>(make_error_aborted("aborted")).as_dynamic();
                                }
                            }).as_dynamic();
                        }
                    }
                }).as_dynamic();
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto jmups){
        return waitForConfirmDoubleTrade(jmups, *pin).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto selector){
        m_taggingSelector = selector;
        return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", m_tradeConfirmFinished.as_readonly()).as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    });
}

auto JobUnionPaySalesJMups::getCouponData() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return hpsCouponSalesAutoDiscount_communication
    (
     m_tradeData.get<int>("amount"),
     prmCouponSalesAutoDiscount_communication::serviceDiv::UnionPay
     )
    .flat_map([=](JMupsAccess::response_t resp){
        m_couponData = resp.body.clone();  
        auto arr = m_couponData.get_optional<json_t::array_t>("normalObject.printInfoList");
        if(!arr || (arr->size() == 0)){
            LogManager_AddInformation("normalObject.printInfoList is null or empty -> skip coupon");
            return pprx::just(const_json_t()).as_dynamic();
        }
        else{
            return pprx::just(resp.body).as_dynamic();
        }
    }).as_dynamic();
    
}

auto JobUnionPaySalesJMups::sendTradeConfirm(prmUnionPay::doubleTrade selector, boost::optional<std::string> authenticationNo)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    return hpsUnionPaySales_Confirm
    (
     selector,
     m_tradeData.get<int>("amount"),
     authenticationNo,
     false,         /* isRePrint */
     m_tradeData.get_optional<int>("couponSelectListNum"),      /* couponSelectListNum */
     m_tradeData.get_optional<int>("couponApplyPreviousAmount"),/* couponApplyPreviousAmount */
     true,                                                      /* pinRetryFlg */
     m_tradeData.get_optional<std::string>("firstGACCmdRes")    /* firstGACCmdRes */
    );
}

auto JobUnionPaySalesJMups::waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData, boost::optional<std::string> authenticationNo)
-> pprx::observable<prmUnionPay::doubleTrade>
{
    LogManager_Function();
    
    const auto isDoubleTrade = jmupsData.body.get_optional<std::string>("normalObject.isDoubleTrade");
    if(isDoubleTrade && ((*isDoubleTrade) == "false")){
        auto tradeData = addMediaDivToCardNo(jmupsData.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return pprx::just(prmUnionPay::doubleTrade::No).as_dynamic();
    }
    
    return callOnDispatchWithCommand("waitForConfirmDoubleTrade")
    .flat_map([=](auto){
        return sendTradeConfirm(prmUnionPay::doubleTrade::Yes, authenticationNo);
    }).as_dynamic()
    .map([=](JMupsAccess::response_t resp){
        auto tradeData = addMediaDivToCardNo(resp.body);
        m_tradeConfirmFinished.put("hps", tradeData.clone());
        tradeData.put("ppsdk.bTradeCompletionFlag", false);
        getJMupsStorage()->setTradeResultData(tradeData.as_readonly());
        return prmUnionPay::doubleTrade::Yes;
    })
    .as_dynamic();
}

auto JobUnionPaySalesJMups::sendTagging(prmUnionPay::doubleTrade selector)
-> pprx::observable<JMupsAccess::response_t>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    LogManager_AddDebug(tradeResultData.str());
    tradeResultData.put("ppsdk.bTradeCompletionFlag", true);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    
    return callOnDispatchWithMessage("disableAborting")
    .flat_map([=](auto){
        return hpsUnionPaySales_Tagging
        (
         selector,
         tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO"),
         prmUnionPay::printResultDiv::Success,
         boost::none,
         false
         );
    }).as_dynamic();
}

auto JobUnionPaySalesJMups::processSendDigiSign()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    const auto digiReceiptDiv = tradeResultData.get_optional<std::string>("normalObject.digiReceiptDiv");
    if( digiReceiptDiv && (*digiReceiptDiv == "1")){
        LogManager_AddInformation("電子伝票保管対象（サイン要）");
        return waitForDigiSign()
        .flat_map([=](boost::optional<std::string> s){
            if(s&&!(s->empty())){
                std::string  sign = *s;
                return sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip, sign);
            }
            else{
                return pprx::error<pprx::unit>(make_error_internal("sign data is empty")).as_dynamic();
            }
        }).as_dynamic()
        .flat_map([=](auto){
            return pprx::observable<>::just(pprx::unit());
        });
    }
    else if(digiReceiptDiv && (*digiReceiptDiv == "2")){
        LogManager_AddInformation("電子伝票保管対象（サイン不要）");
        return pprx::observable<>::just(pprx::unit());
    }
    else{
        LogManager_AddInformation("電子伝票保管対象外");
        isNeedAquirerSlip(true);
        return pprx::observable<>::just(pprx::unit());
    }
}

auto JobUnionPaySalesJMups::waitForDigiSign()
-> pprx::observable<boost::optional<std::string>>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();

    json_t param;
    auto modulus = tradeResultData.get<std::string>("normalObject.digiSignPubKeyModulus");
    auto exponent = tradeResultData.get<std::string>("normalObject.digiSignPubKeyExponent");
    param.put("modulus", modulus);
    param.put("exponent", exponent);
    const auto amount = m_tradeData.get<int>("amount");
    param.put("amount", amount);
    auto strAmount = (boost::format("Total：%s YEN") % BaseSystem::commaSeparatedString(amount)).str();
    param.put("strAmount", strAmount);
    
    return callOnDispatchWithCommandAndSubParameter("waitForDigiSign", "hps", param)
    .map([=](const_json_t json){
        auto html5 = json.get_optional("html5RawCanvasDigiSignData");
        if(html5){
            auto s = encryptHtml5RawCanvasDigiSignData(*html5, modulus, exponent);
            return boost::optional<std::string>(s);
        }
        return json.get_optional<std::string>("encryptedDigiSignData");
    });
}

auto JobUnionPaySalesJMups::isNeedAquirerSlip(bool bNeedAquirer /* =true */)
-> pprx::observable<pprx::unit>
{
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData().clone();
    tradeResultData.put("ppsdk.isNeedAquirerSlip", bNeedAquirer);
    getJMupsStorage()->setTradeResultData(tradeResultData);
    return pprx::just(pprx::unit());
}

auto JobUnionPaySalesJMups::sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    auto&& tradeResultData = getJMupsStorage()->getTradeResultData();
    auto&& slipNo = tradeResultData.get<std::string>("normalObject.tradeResult.SLIP_NO");
    
    return hpsSendDigiSignForUnionPay( slipNo, digiSign )
    .flat_map([=](auto resp){
        return pprx::maybe::success(resp);
    }).as_dynamic()
    .on_error_resume_next([=](std::exception_ptr err){
        LogManager_AddError(pprx::exceptionToString(err));
        return callOnDispatchWithCommand("waitForRetrySendingDigiSign")
        .flat_map([=](const_json_t json){
            const auto bRetry = json.get_optional<bool>("bRetry");
            if(bRetry){
                return *bRetry ? pprx::maybe::retry() : pprx::maybe::error(err);
            }
            else{
                return pprx::maybe::error(make_error_internal("bRetry is null")).as_dynamic();
            }
        }).as_dynamic();
    })
    .retry()
    .flat_map([=](pprx::maybe ctrl){
        return ctrl.observableForContinue<JMupsAccess::response_t>();
    }).as_dynamic()
    .map([=](auto){
        switch(digiReceiptDiv){
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlip:
                isNeedAquirerSlip(false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::DigiSlipWithoutSign:
                isNeedAquirerSlip(false);
                break;
            case JMupsAccess::prmCommon::digiReceiptDiv::PaperSlip:
                 isNeedAquirerSlip(true);
                break;
            default:
                LogManager_AddError("invalid digiReceiptDiv");
                break;
        }

        return pprx::unit();
    })
    .on_error_resume_next([=](auto err){
        LogManager_AddWarning("sign failed.");
        
        json_t result;
        bool bOutjsonRetryNullFlg = false;
        try{std::rethrow_exception(err);}
        catch(pprx::ppexception& ex){
            result = json_t::from_str(ex.what());
            auto errorDesc = result.get_optional<std::string>("result.detail.description");
            if(errorDesc&& (*errorDesc == "bRetry is null")){
                bOutjsonRetryNullFlg = true;
            }
            else{
                result.put("resultCode", "1");
                result.put("errorObject.errorCode", "PP_W0002");
                result.put("errorObject.errorLevel", "ERROR");
                result.put("errorObject.errorType", "ERROR");
                result.put("errorObject.errorMessage", "電子サイン送信失敗");
            }
        }
        catch(...){
            LogManager_AddDebug("what??");
        }
        
        return bOutjsonRetryNullFlg ? pprx::error<pprx::unit>(make_error_internal("bRetry is nil")) : pprx::error<pprx::unit>(make_error_paymentserver(result));
    });
}

/* virtual */
auto JobUnionPaySalesJMups::proceedContact(const_json_t data)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return getCardReader()->beginICCardTransaction()
    .flat_map([=](auto){
        return emvRun(getJMupsStorage(), m_tradeStartResponse);
    }).as_dynamic()
    .map([=](auto tradeResult){
        getJMupsStorage()->setTradeResultData(tradeResult);
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto){
        return getCardReader()->endICCardTransaction();
    });
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardPinMessage(const_json_t param)  -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    if(PaymentJob::isTrainingMode()){ // TRAINING MODEで非表示
        return pprx::just(pprx::unit());
    }
    
    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING"))
    .as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithCommandAndSubParameter("showPinDialog", "", param)
        .as_dynamic();
    }).as_dynamic()
    .map([=](auto){
        return pprx::unit();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardInputPlainOfflinePin(const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyPlainOfflinePin(param);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardInputEncryptedOfflinePin(const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyEncryptedOfflinePin(json_t().as_readonly());
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardInputEncryptedOnlinePin(const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();

    return callOnDispatchWithMessageAndSubParameter("beginInputPIN", "", param)
    .flat_map([=](auto){
        return getCardReader()->verifyEncryptedOnlinePin(param);
    }).as_dynamic()
    .flat_map([=](const_json_t json){
        return callOnDispatchWithMessage("endInputPIN").as_dynamic()
        .flat_map([=](auto) {
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING")).as_dynamic()
            .map([=](auto){
                return json;
            }).as_dynamic();
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardTxPlainApdu(const std::string& apdu)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return getCardReader()->txPlainApdu(apdu);
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardTxEncryptedApdu(const std::string& apdu)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return getCardReader()->txEncryptedApdu(apdu);
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardGetIfdSerialNo()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return getCardReader()->getIfdSerialNo();
}

void JobUnionPaySalesJMups::beginAsyncPreInputAmount()
{
    LogManager_Function();

    TradeInfoBuilder builder;
    builder.addAmount(TradeInfoBuilder::attribute::required, boost::none);
    m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::InProgress);
    
    callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::no, "PreInput")
    .subscribe_on(pprx::observe_on_thread_pool())
    .map([=](const_json_t resp){
        m_tradeData.put("amount", resp.get<int>("results.amount"));
        m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Finished);
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        auto strError = pprx::exceptionToString(err);
        LogManager_AddDebug(strError);
        if (strError.find("\"status\":\"aborted\"") == std::string::npos) {
            // not aborted;
            LogManager_AddDebug("UIBehaviorState::Bad");
            m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Bad);
        } else {
            LogManager_AddDebug("UIBehaviorState::Cancelled");
            m_emvAmountPreInputUI.get_subscriber().on_next(UIBehaviorState::Cancelled);
        }
        return pprx::just(pprx::unit());
    })
    .publish()
    .connect();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnNetwork(const std::string& url, const_json_t param)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return hpsAccessFromEmvKernel(url, param, false)
    .map([=](auto resp){
        if(url == "ma0770.do?mA0770_1=aaa"){
            beginAsyncPreInputAmount();
        }
        else if(url == "ma0770.do?mA0770_3=aaa"){
            /* 銀聯の場合dllPatternDataは存在しないのでクレジットのような判定は不要 */
            m_cardInfoData = resp.body.clone();
        }
        else if(url == "sa1770.do?sA1770_3=aaa"){
            /* 銀聯の場合dllPatternDataは存在しないのでクレジットのような判定は不要 */
            m_cardInfoData = resp.body.clone();
        }
        return resp.body;
    });
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiSelectDeal()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    json_t json;
    json.put("operationDiv", "0");
    const_json_t cjson = json;
    return pprx::just(cjson, pprx::observe_on_thread_pool());
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiSelectApplication(const_json_t emvSession)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto applicationList = emvSession.get("transaction.applicationList");
    return callOnDispatchWithCommandAndSubParameter("waitForApplicationSelect", "applicationList", applicationList);
}

auto JobUnionPaySalesJMups::buildDispatchRequestParamForTradeInfo()
-> pprx::observable<TradeInfoBuilder>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .map([=](auto) -> TradeInfoBuilder{
        TradeInfoBuilderJMupsUnionPay builder;
        {
            auto amount = m_tradeData.get_optional<int>("amount");
            builder.addAmount(TradeInfoBuilder::attribute::required, amount);
        }

        return builder;
    });
}

auto JobUnionPaySalesJMups::applyDispatchResponseParamForTradeInfo(const_json_t resp)
-> pprx::observable<bool>
{
    LogManager_Function();

    auto results = resp.get("results");
    
    m_tradeData.put("amount", results.get<int>("amount"));
    auto approvalNo     = results.get_optional<std::string>("approvalNo");
    auto cupSendDate    = results.get_optional<std::string>("cupSendDate");
    auto cupNo          = results.get_optional<std::string>("cupNo");
    auto couponId       = results.get_optional<std::string>("coupon");

    if(approvalNo)      m_tradeData.put("approvalNo"        , *approvalNo);
    if(cupSendDate)     m_tradeData.put("cupSendDate"       , *cupSendDate);
    if(cupNo)           m_tradeData.put("cupNo"             , *cupNo);
    if(couponId)        m_tradeData.put("couponId"          , *couponId);
    
    auto operation = resp.get<std::string>("operation");
    const auto bApply = operation == "apply";
    if(bApply){
        if(couponId){
            auto couponInfo = findCoupon(m_couponData, *couponId);
            auto orgAmount = m_tradeData.get<int>("amount");
            auto discountValue = couponInfo.get<int>("DISCOUNT_PRICE");
            auto couponSelectListNum = getCouponSelectListNum(m_couponData, *couponId);
            m_tradeData.put("couponSelectListNum", *couponSelectListNum);
            m_tradeData.put("couponApplyPreviousAmount", orgAmount);
            m_tradeData.put("amount", orgAmount - discountValue);
        }
    }
    if(PaymentJob::isTrainingMode()){
        getJMupsStorage()->setInputTradeData(m_tradeData.clone());
    }
    
    return pprx::just(bApply);
}

auto JobUnionPaySalesJMups::waitForTradeInfo(const std::string& strTradeOp)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return buildDispatchRequestParamForTradeInfo()
    .flat_map([=](TradeInfoBuilder builder){
        return callOnDispatchTradeInfo(builder, TradeInfoBuilder::updatable::yes, strTradeOp)
        .as_dynamic();
    }).as_dynamic()
    .flat_map([=](const_json_t resp){     
        return applyDispatchResponseParamForTradeInfo(resp);
    }).as_dynamic()
    .flat_map([=](bool bApply){
        if(bApply)
            return pprx::maybe::success(pprx::unit());
        else
            return pprx::maybe::retry();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        return pprx::maybe::error(err);
    })
    .retry()
    .flat_map([=](pprx::maybe m){
        return m.observableForContinue<pprx::unit>();
    });
}

auto JobUnionPaySalesJMups::syncEmvAmountPreInputUI()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return m_emvAmountPreInputUI.get_observable()
    .flat_map([=](UIBehaviorState state){
        if(state == UIBehaviorState::Cancelled){
            abort(AbortReason::UserRequest);
        }
        else if(state == UIBehaviorState::Finished){
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else if(state == UIBehaviorState::Bad){
            return pprx::error<pprx::unit>(make_error_internal("amount is empty")).as_dynamic();
        }
        return pprx::never<pprx::unit>().as_dynamic();
    })
    .take(1)
    .map([=](auto){
        return pprx::unit();
    });
}

/* virtual */
auto JobUnionPaySalesJMups::emvWaitInputPreAmount() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return syncEmvAmountPreInputUI()
    .map([=](auto){
        const auto amount = m_tradeData.get<int>("amount");
        json_t j;
        j.put("amount", amount);
        return j.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiAmount()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return syncEmvAmountPreInputUI()
    .flat_map([=](auto){
        return waitForTradeInfo("Input");
    })     
    .map([=](auto){
        return m_tradeData.as_readonly();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiInputKid()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    throw_error_internal("UnionPay does not support KID.");
    return pprx::just(json_t().as_readonly());
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiInputPayWay(const_json_t emvSession)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .map([=](auto){
        json_t json;
        auto paymentWay = m_tradeData.get_optional<std::string>("paymentWay");
        if(paymentWay){
            json.put("payWayDiv", "lump");
        }
        return json.as_readonly();
    });
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnUiAmountConfirm(const_json_t emvSession)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    m_tradeData.put("firstGACCmdRes", emvSession.get<std::string>("transaction.firstGACCmdRes"));
    
    return sendTradeConfirm(prmUnionPay::doubleTrade::No, boost::none)
    .flat_map([=](auto resp)mutable{
        json_t jsonResponse = resp.body.clone();
        auto bIsFallback = jsonResponse.get<bool>("normalObject.isFallback");
        if(bIsFallback){
            return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_CARD_SKIP_FAILURE")).as_dynamic()
            .flat_map([=](auto) {
                json_t json;
                json.put("errorCode", "PP_E3001");
                json.put("description", "このカードはICでのお取り引きはできません。/n 磁気カードリーダを使用してください。");
                return pprx::error<prmUnionPay::doubleTrade>(make_error_pinreleted(json)).as_dynamic();
            }).as_dynamic();
        }
        else{
            return waitForConfirmDoubleTrade(resp, boost::none);
        }
    }).as_dynamic()
    .map([=](prmUnionPay::doubleTrade taggingSelector) -> const_json_t{
        m_taggingSelector = taggingSelector;
        return getJMupsStorage()->getTradeResultData();
    });
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnTradeConfirmFinish(const_json_t emvTradeConfirm)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return callOnDispatchWithMessageAndSubParameter("tradeConfirmFinished", "", emvTradeConfirm).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::processRemovingContact(bool bTradeIsDecline /*= false*/)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return getCardReader()->observableDeviceConnectState().take(1)
    .flat_map([=](CardReader::DeviceConnectState connState){
        if(connState != CardReader::DeviceConnectState::Connected){
            LogManager_AddDebug("デバイスが接続されていない");
            /* R/Wが切断状態の場合、カード抜き待ち処理は存在しない */
            return pprx::just(pprx::unit()).as_dynamic();
        }
        
        LogManager_AddDebug("デバイスが接続されている");
        return getCardReader()->abortTransaction()
        .flat_map([=](auto) {
            return getCardReader()->observableICCardSlotState().take(1);
        }).as_dynamic()
        .flat_map([=](CardReader::ICCardSlotState slotState){
            if(slotState != CardReader::ICCardSlotState::Inserted){
                LogManager_AddDebug("カードが挿入されている状態ではなくなった");
                /* カードが挿入されている状態でない */
                return pprx::just(pprx::unit()).as_dynamic();
            }
            else{
                auto strLEDText = bTradeIsDecline ? getDisplayMsgForReader("MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD") : getDisplayMsgForReader("MIURA_CREDIT_UI_REMOVING_CARD");
                if(strLEDText.empty()){
                    return pprx::just(pprx::unit()).as_dynamic();
                }
                else{
                    return getCardReader()->displayTextNoLf(strLEDText)
                    .flat_map([=](auto){
                        return getCardReader()->observableICCardSlotState()
                        .distinct_until_changed()
                        .flat_map([=](CardReader::ICCardSlotState slotState){
                            if(slotState != CardReader::ICCardSlotState::Inserted){
                                LogManager_AddInformation("Card is removed");
                                return pprx::just(pprx::unit()).as_dynamic();
                            }
                            return pprx::never<pprx::unit>().as_dynamic();
                        }).as_dynamic()
                        .take(1)
                        .flat_map([=](auto){
                            LogManager_AddInformation("Card is removed manually.");
                            return pprx::just(pprx::unit()).as_dynamic();
                        }).as_dynamic();
                    }).as_dynamic()
                    .on_error_resume_next([=](auto err){
                        LogManager_AddInformationF("Card is not removed: \n%s", pprx::exceptionToString(err));
                        return pprx::just(pprx::unit()).as_dynamic();
                    }).take(1)
                    .map([=](auto){
                        LogManager_AddInformation("processRemovingContact finished");
                        return pprx::unit();
                    }).as_dynamic();
                }
            }
        }).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnTrainingCardReaderDisplayData()
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto&& inputTradeData = getJMupsStorage()->getInputTradeData();
    auto AMOUNT           = inputTradeData.get<int>("amount");
    auto strAmount = BaseSystem::commaSeparatedString(AMOUNT);
    auto msg1 = (boost::format("金額 ￥%s円\n暗証番号を入力してください") % strAmount).str();
    
    json_t  jsonDisplay;
    jsonDisplay.put("amount"            , AMOUNT);
    jsonDisplay.put("application.label" , "556E696F6E50617920437265646974");
    jsonDisplay.put("uiMessage.message1", msg1);
    jsonDisplay.put("uiMessage.message2", "");
    
    return pprx::just(jsonDisplay.as_readonly());
}

auto JobUnionPaySalesJMups::addMediaDivToCardNo(const_json_t tradeData) -> json_t
{
    LogManager_Function();

    auto deviceName = tradeData.get_optional<std::string>("normalObject.printInfo.device.name");
    if(!deviceName){
        return tradeData.clone();
    }
    
    json_t resp = tradeData.clone();
    auto pinfo = tradeData.get("normalObject.printInfo.device.value").clone();
    auto values = pinfo.get<json_t::array_t>("values");
    
    /* カードMEDIA_DIVを追加　ICかMSか */
    {
        for(auto v : values){
            auto mediaDiv = v.get_optional<std::string>("PHYSICAL_MEDIA_DIV");
            auto cardNo   = v.get_optional<std::string>("CARD_NO");
            if(PaymentJob::isTrainingMode()){
                if(m_bICCardType){
                    *mediaDiv = "IC";
                }
                else{
                    *mediaDiv = "MS";
                }
                
                v.put("PHYSICAL_MEDIA_DIV", *mediaDiv);
            }

            if(mediaDiv){
                auto cardNoWithMediaDiv = *mediaDiv + " " + *cardNo;
                v.put("CARD_NO", cardNoWithMediaDiv);
            }
        }
    }
    /* printInfoのvalueを再構築 */
    pinfo.put("values", values);
    resp.put("normalObject.printInfo.device.value", pinfo);
    return resp;
}

auto JobUnionPaySalesJMups::checkTradeFailureErrorCode(const_json_t tradeData, const std::string errorCode) -> bool
{
    LogManager_Function();
    
    auto bIsDecline = tradeData.get_optional<std::string>("normalObject.printInfo.isDecline");
    if(bIsDecline && (*bIsDecline == "true")){ /* 取引が不成立 */
        auto printInfo = tradeData.get("normalObject.printInfo.device.value").clone();
        auto values = printInfo.get<json_t::array_t>("values");
        /* FAILURE_INFOの情報を確認 */
        for(auto v : values){
            auto failureList = v.get_optional("FAILURE_INFO");
            if(failureList){
                auto&& list = failureList->get_array<std::string>();
                for(auto src: list){
                    auto found = src.find(errorCode.c_str());
                    if (found!=std::string::npos){
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

/* virtual */
auto JobUnionPaySalesJMups::emvOnCardPinResultConfirm(const_json_t param)  -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return getCardReader()->displayTextNoLf(getDisplayMsgForReader("MIURA_CREDIT_UI_PROCESSING"))
    .as_dynamic()
    .flat_map([=](auto){
        return callOnDispatchWithCommandAndSubParameter("waitForPinResultConfirm", "", param).as_dynamic()
        .map([=](const_json_t resp){
            const bool bContinue = resp.get<bool>("bContinue");
            if(!bContinue){
                throw_error_aborted("aborted");
            }
            return pprx::unit();
        }).as_dynamic();
    }).as_dynamic();
}

void JobUnionPaySalesJMups::beginAsyncRemovingContact()
{
    LogManager_Function();
    
    bool bTradeIsDecline = false;
    auto bIsDecline = getJMupsStorage()->getTradeResultData().get_optional<std::string>("normalObject.printInfo.isDecline");
    if(bIsDecline && (*bIsDecline == "true")){ /* 取引が不成立 */
        bTradeIsDecline = true;
    }
    
    processRemovingContact(bTradeIsDecline)
    .map([](auto){
        return pprx::unit();
    }).as_dynamic()
    .on_error_resume_next([](auto err){
        LogManager_AddInformation(pprx::exceptionToString(err));
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .publish()
    .connect();
}

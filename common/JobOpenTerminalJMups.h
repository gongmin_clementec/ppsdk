//
//  JobOpenTerminalJMups.hpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobOpenTerminalJMups__)
#define __h_JobOpenTerminalJMups__

#include "PaymentJob.h"
#include "JMupsAccess.h"

class JobOpenTerminalJMups :
    public PaymentJob,
    public JMupsAccess
{
private:
    
protected:

    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;
    
public:
            JobOpenTerminalJMups(const __secret& s);
    virtual ~JobOpenTerminalJMups();
    
    virtual bool isCardReaderRequired() const { return false; };
};



#endif /* !defined(__h_JobOpenTerminalJMups__) */

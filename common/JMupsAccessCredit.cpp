//
//  JMupsAccessCredit.cpp
//  ppsdk
//
//  Created by tel on 2017/06/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JMupsAccessCredit.h"

JMupsAccessCredit::JMupsAccessCredit()
{
    
}

/* virtual */
JMupsAccessCredit::~JMupsAccessCredit()
{
    
}

/* 取引確認 */
auto JMupsAccessCredit::hpsCreditSales_Confirm
(
 prmCredit::doubleTrade selector,
 prmCredit::dcc         dcc,
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode,
 prmCredit::payWayDiv   payWayDiv,
 boost::optional<int>   partitionTimes,
 boost::optional<int>   firsttimeClaimMonth,
 bool                   isRePrint,
 boost::optional<int>   couponSelectListNum,
 boost::optional<int>   couponApplyPreviousAmount,
 boost::optional<int>   couponApplyAfterAmount,
 boost::optional<std::string> firstGACCmdRes,
 boost::optional<int>   bonusTimes,
 boost::optional<int>   bonusMonth1,
 boost::optional<int>   bonusAmount1,
 boost::optional<int>   bonusMonth2,
 boost::optional<int>   bonusAmount2,
 boost::optional<int>   bonusMonth3,
 boost::optional<int>   bonusAmount3,
 boost::optional<int>   bonusMonth4,
 boost::optional<int>   bonusAmount4,
 boost::optional<int>   bonusMonth5,
 boost::optional<int>   bonusAmount5,
 boost::optional<int>   bonusMonth6,
 boost::optional<int>   bonusAmount6,
 boost::optional<int>   point,
 boost::optional<int>   exchangeablePoint
 )
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "productCode"    , productCode);
    setJsonParam(json, "isRePrint"      , isRePrint);
    setJsonParam(json, "firstGACCmdRes" , firstGACCmdRes);
    setJsonParam(json, "couponSelectListNum"        , couponSelectListNum);
    setJsonParam(json, "couponApplyPreviousAmount"  , couponApplyPreviousAmount);

    if(dcc == prmCredit::dcc::No){
        setJsonParam
        (
         json, "payWayDiv", payWayDiv,
         {
             {prmCredit::payWayDiv::Lump           , "0"},
             {prmCredit::payWayDiv::Installment    , "1"},
             {prmCredit::payWayDiv::Bonus          , "2"},
             {prmCredit::payWayDiv::BonusCombine   , "3"},
             {prmCredit::payWayDiv::Revolving      , "4"}
         });
        
        setJsonParam(json, "partitionTimes"         , partitionTimes);
        setJsonParam(json, "firsttimeClaimMonth"    , firsttimeClaimMonth);
        setJsonParam(json, "couponApplyAfterAmount"     , couponApplyAfterAmount);
        
        setJsonParam(json, "bonusTimes"         , bonusTimes);
        setJsonParam(json, "bonusMonth1"        , bonusMonth1);
        setJsonParam(json, "bonusAmount1"       , bonusAmount1);
        setJsonParam(json, "bonusMonth2"        , bonusMonth2);
        setJsonParam(json, "bonusAmount2"       , bonusAmount2);
        setJsonParam(json, "bonusMonth3"        , bonusMonth3);
        setJsonParam(json, "bonusAmount3"       , bonusAmount3);
        setJsonParam(json, "bonusMonth4"        , bonusMonth4);
        setJsonParam(json, "bonusAmount4"       , bonusAmount4);
        setJsonParam(json, "bonusMonth5"        , bonusMonth5);
        setJsonParam(json, "bonusAmount5"       , bonusAmount5);
        setJsonParam(json, "bonusMonth6"        , bonusMonth6);
        setJsonParam(json, "bonusAmount6"       , bonusAmount6);
        setJsonParam(json, "point"              , point);
        setJsonParam(json, "exchangeablePoint"  , exchangeablePoint);
    }
    
    setCookie("tradeState", "2");

    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1027.do?sA1027_3=aaa" :
            "sa1027.do?sA1027_1=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1177.do?sA1477_3=aaa" :
            "sa1477.do?sA1477_1=aaa";
            break;
        default:
            break;
    }

    if(isTrainingMode()){
        auto reps = keyvalue_t();        
        replaceTrainingData(amount, taxOtherAmount, productCode, payWayDiv, reps);
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}


/* 印字結果送信 */
auto JMupsAccessCredit::hpsCreditSales_Tagging
(
 prmCredit::doubleTrade     selector,
 prmCredit::dcc             dcc,
 const std::string&         slipNo,
 prmCredit::printResultDiv  printResultDiv,
 boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip,
 bool                       isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmCredit::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmCredit::isSendingDesiSlip::Unnecessarily , "3"}
                 });

    setJsonParam(json, "isRePrint"              , isRePrint);

    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */

    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1027.do?sA1027_4=aaa" :
            "sa1027.do?sA1027_2=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1177.do?sA1477_4=aaa" :
            "sa1477.do?sA1477_2=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* KID入力 */
auto JMupsAccessCredit::hpsKIDInput
(
 const std::string&         kid,
 prmCommon::operationDiv    operationDiv,
 prmKIDInput::manualDiv     manualDiv
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());

    setJsonParam(json, "kid", kid);

    setJsonParam
    (
     json, "operationDiv", operationDiv,
     {
         {prmCommon::operationDiv::Sales        , "0"},
         {prmCommon::operationDiv::Refund       , "1"},
         {prmCommon::operationDiv::PreApproval  , "3"},
         {prmCommon::operationDiv::AfterApproval, "4"},
         {prmCommon::operationDiv::CardCheck    , "5"}
     });
    
    setJsonParam
    (
     json, "manualDiv", manualDiv,
     {
         {prmKIDInput::manualDiv::NonManual  , "0"},
         {prmKIDInput::manualDiv::Manual     , "1"}
     });
    
    return callHps("mA0919.do?mA0919=aaa", json);
}

/* DHS LookUp */
auto JMupsAccessCredit::hpsDHSLookup
(
 int                        amount,
 int                        taxOtherAmount,
 prmCommon::operationDiv    operationDiv,
 bool                       isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "isRePrint", isRePrint);
    setJsonParam(json, "amount", amount);
    setJsonParam(json, "taxOtherAmount", taxOtherAmount);

    setJsonParam
    (
     json, "operationDiv", operationDiv,
     {
         {prmCommon::operationDiv::Sales        , "0"},
         {prmCommon::operationDiv::PreApproval  , "3"},
         {prmCommon::operationDiv::AfterApproval, "4"}
     });
    
    return callHps("dc0101.do?dC0101=aaa", json);
}

/* DCCC承認後売上判定 */
auto JMupsAccessCredit::hpsDccAsCheck
(
 const std::string&         approvalNo,
 int                        amount,
 int                        taxOtherAmount
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "approvalNo", approvalNo);
    setJsonParam(json, "amount", amount);
    setJsonParam(json, "taxOtherAmount", taxOtherAmount);
    
    return callHps("dc0301.do?dC0301_1=aaa", json);
}

auto JMupsAccessCredit::hpsDccSelect()
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    return callHps("dc0301.do?dC0301_2=aaa", json);
}

/* 取引確認 */
auto JMupsAccessCredit::hpsCreditPreApproval_Confirm
(
 prmCredit::doubleTrade selector,
 prmCredit::dcc         dcc,
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode
 )
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "productCode"    , productCode);
    setJsonParam(json, "isRePrint"      , false);
    
    setCookie("tradeState", "2");
    
    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1527.do?pA0427_3=aaa" :
            "pa0427.do?pA0427_1=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1677.do?pA0877_3=aaa" :
            "pa0877.do?pA0877_1=aaa";
            break;
        default:
            break;
    }

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}


/* 印字結果送信 */
auto JMupsAccessCredit::hpsCreditPreApproval_Tagging
(
 prmCredit::doubleTrade     selector,
 prmCredit::dcc             dcc,
 const std::string&         slipNo,
 prmCredit::printResultDiv  printResultDiv
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isRePrint"              , false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1527.do?pA0427_4=aaa" :
            "pa0427.do?pA0427_2=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1677.do?pA0877_4=aaa" :
            "pa0877.do?pA0877_2=aaa";
            break;
        default:
            break;
    }

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 取引確認 */
auto JMupsAccessCredit::hpsCreditAfterApproval_Confirm
(
 prmCredit::doubleTrade selector,
 prmCredit::dcc         dcc,
 const std::string&     approvalNo,
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode,
 prmCredit::payWayDiv   payWayDiv,
 boost::optional<int>   partitionTimes,
 boost::optional<int>   firsttimeClaimMonth,
 bool                   isRePrint,
 boost::optional<int>   couponSelectListNum,
 boost::optional<int>   couponApplyPreviousAmount,
 boost::optional<int>   couponApplyAfterAmount,
 boost::optional<std::string> firstGACCmdRes,
 boost::optional<int>   bonusTimes,
 boost::optional<int>   bonusMonth1,
 boost::optional<int>   bonusAmount1,
 boost::optional<int>   bonusMonth2,
 boost::optional<int>   bonusAmount2,
 boost::optional<int>   bonusMonth3,
 boost::optional<int>   bonusAmount3,
 boost::optional<int>   bonusMonth4,
 boost::optional<int>   bonusAmount4,
 boost::optional<int>   bonusMonth5,
 boost::optional<int>   bonusAmount5,
 boost::optional<int>   bonusMonth6,
 boost::optional<int>   bonusAmount6,
 boost::optional<int>   point,
 boost::optional<int>   exchangeablePoint
 )
-> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "approvalNo"     , approvalNo);
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "productCode"    , productCode);
    setJsonParam(json, "isRePrint"      , isRePrint);
    setJsonParam(json, "firstGACCmdRes" , firstGACCmdRes);
    setJsonParam(json, "couponSelectListNum"        , couponSelectListNum);
    setJsonParam(json, "couponApplyPreviousAmount"  , couponApplyPreviousAmount);
    
    if(dcc == prmCredit::dcc::No){
        setJsonParam
        (
         json, "payWayDiv", payWayDiv,
         {
             {prmCredit::payWayDiv::Lump           , "0"},
             {prmCredit::payWayDiv::Installment    , "1"},
             {prmCredit::payWayDiv::Bonus          , "2"},
             {prmCredit::payWayDiv::BonusCombine   , "3"},
             {prmCredit::payWayDiv::Revolving      , "4"}
         });
        
        setJsonParam(json, "partitionTimes"         , partitionTimes);
        setJsonParam(json, "firsttimeClaimMonth"    , firsttimeClaimMonth);
        setJsonParam(json, "couponApplyAfterAmount"     , couponApplyAfterAmount);
        
        setJsonParam(json, "bonusTimes"         , bonusTimes);
        setJsonParam(json, "bonusMonth1"        , bonusMonth1);
        setJsonParam(json, "bonusAmount1"       , bonusAmount1);
        setJsonParam(json, "bonusMonth2"        , bonusMonth2);
        setJsonParam(json, "bonusAmount2"       , bonusAmount2);
        setJsonParam(json, "bonusMonth3"        , bonusMonth3);
        setJsonParam(json, "bonusAmount3"       , bonusAmount3);
        setJsonParam(json, "bonusMonth4"        , bonusMonth4);
        setJsonParam(json, "bonusAmount4"       , bonusAmount4);
        setJsonParam(json, "bonusMonth5"        , bonusMonth5);
        setJsonParam(json, "bonusAmount5"       , bonusAmount5);
        setJsonParam(json, "bonusMonth6"        , bonusMonth6);
        setJsonParam(json, "bonusAmount6"       , bonusAmount6);
        setJsonParam(json, "point"              , point);
        setJsonParam(json, "exchangeablePoint"  , exchangeablePoint);
    }
    
    setCookie("tradeState", "2");
    
    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1727.do?aS1127_3=aaa" :
            "as1127.do?aS1127_1=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1877.do?aS1577_3=aaa" :
            "as1577.do?aS1577_1=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();        
        replaceTrainingData(amount, taxOtherAmount, productCode, payWayDiv, reps);
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditAfterApproval_DCCSearchTrade
(
 const std::string&     approvalNo,
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode,
 const std::string&     slipNo,
 boost::optional<std::string> otherTermJudgeNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "otherTermJudgeNo"   , otherTermJudgeNo);
    setJsonParam(json, "approvalNo"         , approvalNo);
    setJsonParam(json, "amount"             , amount);
    setJsonParam(json, "taxOtherAmount"     , taxOtherAmount);
    setJsonParam(json, "slipNo"             , slipNo);
    setJsonParam(json, "productCode"        , productCode);
    setJsonParam(json, "isRePrint"          , false);

    setCookie("tradeState", "1");
    
    std::string url = otherTermJudgeNo ? "as1919.do?aS1919=aaa" : "as1819.do?aS1819=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 承認後売上・DCC不成立確認　*/
auto JMupsAccessCredit::hpsCreditAfterApproval_DCCRejectConfirm
(
 const std::string&     approvalNo,
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "approvalNo"     , approvalNo);
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "productCode"    , productCode);
    setJsonParam(json, "isRePrint"      , false);

    setCookie("tradeState", "2");
    
    std::string url = "as2131.do?aS2131_1=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

/* 印字結果送信 */
auto JMupsAccessCredit::hpsCreditAfterApproval_Tagging
(
 prmCredit::doubleTrade     selector,
 prmCredit::dcc             dcc,
 const std::string&         slipNo,
 prmCredit::printResultDiv  printResultDiv,
 boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip,
 bool                       isRePrint
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmCredit::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmCredit::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint"              , isRePrint);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    //## setCookie("printResultDiv", json->get<std::string>("printResultDiv")); /* 後で何とかすること */
    
    std::string url;
    switch(selector){
        case prmCredit::doubleTrade::No:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1727.do?aS1127_4=aaa" :
            "as1127.do?aS1127_2=aaa";
            break;
        case prmCredit::doubleTrade::Yes:
            url = (dcc == prmCredit::dcc::Yes) ?
            "dc1877.do?aS1577_4=aaa" :
            "as1577.do?aS1577_2=aaa";
            break;
        default:
            break;
    }
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_SearchTrade
(
 const std::string&     slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    
    std::string url = "ca0119.do?cA0119=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_CancelConfirm
(
 const std::string&     slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    setCookie("tradeState", "2");
    
    std::string url = "ca0327.do?cA0327_1=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_CancelTagging
(
 const std::string&            slipNo,
 prmCredit::printResultDiv     printResultDiv,
 boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmCredit::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmCredit::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint", false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = "ca0327.do?cA0327_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_ReturnConfirm
(
 int                    amount,
 int                    taxOtherAmount,
 const std::string&     productCode,
 const std::string&     slipNo,
 prmCredit::payWayDiv   payWayDiv,
 boost::optional<int>   partitionTimes,
 boost::optional<int>   firsttimeClaimMonth
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam
    (
     json, "payWayDiv", payWayDiv,
     {
         {prmCredit::payWayDiv::Lump           , "0"},
         {prmCredit::payWayDiv::Installment    , "1"},
         {prmCredit::payWayDiv::Bonus          , "2"},
         {prmCredit::payWayDiv::BonusCombine   , "3"},
         {prmCredit::payWayDiv::Revolving      , "4"}
     });
    setJsonParam(json, "partitionTimes"         , partitionTimes);
    setJsonParam(json, "firsttimeClaimMonth"    , firsttimeClaimMonth);
    setJsonParam(json, "amount"                 , amount);
    setJsonParam(json, "taxOtherAmount"         , taxOtherAmount);
    setJsonParam(json, "productCode"            , productCode);
    setJsonParam(json, "slipNo"                 , slipNo);
    setJsonParam(json, "isRePrint"              , false);

    setCookie("tradeState", "2");
    
    std::string url = "ca1327.do?cA1327_1=aaa";
    
    if(isTrainingMode()){
        auto reps = keyvalue_t();        
        replaceTrainingData(amount, taxOtherAmount, productCode, payWayDiv, reps);
        reps["##SLIP_NO##"]          = slipNo;
        
        return callTraining(url, reps);
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_ReturnTagging
(
 const std::string&            slipNo,
 prmCredit::printResultDiv     printResultDiv,
 boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmCredit::isSendingDesiSlip::ShoudBeSending , "2"},
                     {prmCredit::isSendingDesiSlip::Unnecessarily , "3"}
                 });
    
    setJsonParam(json, "isRePrint", false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = "ca1327.do?cA1327_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_DCCSearchTrade
(
 const std::string& otherTermJudgeNo,
 const std::string& slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "otherTermJudgeNo", otherTermJudgeNo);
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "isRePrint", false);
    
    std::string url = "ca1819.do?cA1819=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_DCCCancelConfirm
(
 int                amount,
 int                taxOtherAmount,
 const std::string& slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "slipNo"         , slipNo);
    setJsonParam(json, "isRePrint"      , false);
    setJsonParam(json, "forceRefundFlg" , "0"); //強制返品しない

    setCookie("tradeState", "2");
    
    std::string url = "dc1227.do?cA0327_3=aaa";

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_DCCReturnConfirm
(
 int                amount,
 int                taxOtherAmount,
 const std::string& slipNo
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "amount"         , amount);
    setJsonParam(json, "taxOtherAmount" , taxOtherAmount);
    setJsonParam(json, "slipNo"         , slipNo);
    setJsonParam(json, "isRePrint"      , false);
    setJsonParam(json, "forceRefundFlg" , "1"); //強制返品
    
    setCookie("tradeState", "2");
    
    std::string url = "dc1327.do?cA2027_1=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditRefund_DCCTagging
(
 prmCreditRefund_DCCTagging::judgementTerm    judgeTerm,
 const std::string&            slipNo,
 prmCredit::printResultDiv     printResultDiv,
 boost::optional<prmCredit::isSendingDesiSlip> isSendingDesiSlip
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    setJsonParam(json, "slipNo", slipNo);
    
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isSendingDesiSlip", isSendingDesiSlip,
                 {
                     {prmCredit::isSendingDesiSlip::ShoudBeSending , "1"},
                     {prmCredit::isSendingDesiSlip::Unnecessarily , "0"}
                 });
    
    setJsonParam(json, "isRePrint", false);
    
    setCookie("tradeState", "4");
    setCookie("printResult", "true");
    
    std::string url = (judgeTerm == prmCreditRefund_DCCTagging::judgementTerm::Self) ?
    "dc1227.do?cA0327_4=aaa" : "dc1327.do?cA2027_2=aaa";
    
    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}


auto JMupsAccessCredit::hpsCreditCardCheck_Confirm()
 -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    
    
    setJsonParam(json, "isRePrint", false);
    
    setCookie("tradeState", "2");
    
    std::string url = "cc0101.do?cC0101=aaa";

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

auto JMupsAccessCredit::hpsCreditCardCheck_Tagging
(
 const std::string&            slipNo,
 prmCredit::printResultDiv     printResultDiv
 ) -> pprx::observable<JMupsAccess::response_t>
{
    json_t json;
    
    setJsonParam(json, "mposDeviceType", prmCommon::mposDeviceType::M010,
                 {
                     {prmCommon::mposDeviceType::iSMP     , "0"},
                     {prmCommon::mposDeviceType::M010     , "1"}
                 });
    setJsonParam(json, "isTraining", isTrainingMode());
    setJsonParam(json, "slipNo", slipNo);
    setJsonParam(json, "printResultDiv", printResultDiv,
                 {
                     {prmCredit::printResultDiv::Success , "2"},
                     {prmCredit::printResultDiv::Failure , "3"}
                 });
    
    setJsonParam(json, "isRePrint", false);

    setCookie("tradeState", "4");
    setCookie("printResult", "true");

    std::string url = "cc0227.do?cC0227=aaa";

    if(isTrainingMode()){
        return callTraining(url, keyvalue_t());
    }
    else{
        return callHps(url, json);
    }
}

//
//
void JMupsAccessCredit::replaceTrainingData
(
 const int amount,
 const int taxOtherAmount,
 const std::string&     productCode,
 prmCredit::payWayDiv   payWayDiv,
 keyvalue_t& replaces
 )
{
    auto TOTAL_YEN = integerToYenStringInJson(amount+taxOtherAmount);
    std::string PAY_WAY_DIV("");
    std::string PAY_DIVISION("");
    switch(payWayDiv)
    {
        case prmCredit::payWayDiv::Lump:
            PAY_WAY_DIV = "0";
            PAY_DIVISION = "一括";
            replaces["##INSTALLMENT##"] = "";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "";
            break;
        case prmCredit::payWayDiv::Installment:
            PAY_WAY_DIV ="1";
            PAY_DIVISION = "分割";
            replaces["##INSTALLMENT##"] = "2回";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "3月";
            break;
        case prmCredit::payWayDiv::Bonus:
            PAY_WAY_DIV ="2";
            PAY_DIVISION = "ボーナス";
            replaces["##INSTALLMENT##"] = "";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "";
            break;
        case prmCredit::payWayDiv::BonusCombine:
            PAY_WAY_DIV ="3";
            PAY_DIVISION = "ボーナス併用";
            replaces["##INSTALLMENT##"] = "2回";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "3月";
            break;
        case prmCredit::payWayDiv::Revolving:
            PAY_WAY_DIV ="4";
            PAY_DIVISION = "リボ";
            replaces["##INSTALLMENT##"] = "";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "";
            break;
        default:
            PAY_WAY_DIV = "0";
            PAY_DIVISION = "一括";
            replaces["##INSTALLMENT##"] = "";
            replaces["##FIRSTTIME_CLAIM_MONTH##"] = "";
            break;
    }
    
    replaces["##PRODUCT_DIVISION##"] = productCode;
    replaces["##AMOUNT##"]           = integerToYenStringInJson(amount);
    replaces["##TAX##"]              = integerToYenStringInJson(taxOtherAmount);
    replaces["##SETTLEDAMOUNT##"]    = TOTAL_YEN;
    replaces["##TOTAL_YEN##"]        = TOTAL_YEN;
    replaces["##PAY_WAY_DIV##"]      = PAY_WAY_DIV;
    replaces["##PAY_DIVISION##"]     = PAY_DIVISION;
}

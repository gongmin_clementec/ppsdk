//
//  ZipFileDecompressor.cpp
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "ZipFileDecompressor.h"
#include <vector>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>

extern "C" {
#include "contrib/minizip/unzip.h"
}

ZipFileDecompressor::ZipFileDecompressor() : m_unzFile(nullptr)
{
}

/* virtual */
ZipFileDecompressor::~ZipFileDecompressor()
{
    close();
}

bool ZipFileDecompressor::open(const std::string& filepath)
{
    close();
    m_unzFile = ::unzOpen(filepath.c_str());
    return m_unzFile != nullptr;
}

void ZipFileDecompressor::close()
{
    if(m_unzFile == nullptr) return;
    ::unzClose(m_unzFile);
    m_unzFile = nullptr;
}

bool ZipFileDecompressor::enumerate(enumerate_callback_t callback)
{
    bool bSuccess = true;
    ::unzGoToFirstFile(m_unzFile);
    
    std::vector<char>   fname(1024);
    std::vector<char>   buff;
    
    do {
        unz_file_info    finfo;
        
        int err = ::unzGetCurrentFileInfo(m_unzFile,    /* unzFile */
                                          &finfo,       /* unz_file_info */
                                          fname.data(), /* szFileName */
                                          fname.size(), /* fileNameBufferSize */
                                          nullptr, 0,   /* extraField */
                                          nullptr, 0);  /* szComment */
        if(err != UNZ_OK){
            bSuccess = false;
            break;
        }
        
        boost::shared_ptr<std::ostream> os;
        if(!callback(fname.data(), os)) {
            bSuccess = false;
            break;
        }
        
        if(os.get() != nullptr){
            if(::unzOpenCurrentFile(m_unzFile) != UNZ_OK){
                bSuccess = false;
                break;
            }
            if(buff.size() == 0){
                buff.resize(4 * 1024);
            }
            while(true){
                const int readBytes = ::unzReadCurrentFile(m_unzFile, buff.data(), static_cast<uInt>(buff.size()));
                if(readBytes < 0){
                    bSuccess = false;
                    break;
                }
                if(readBytes <= 0) break;
                os->write(buff.data(), readBytes);
            }
            (void)::unzCloseCurrentFile(m_unzFile);
        }
        
        if(!bSuccess) break;
    } while(::unzGoToNextFile(m_unzFile) != UNZ_END_OF_LIST_OF_FILE);
    
    return bSuccess;
}

bool ZipFileDecompressor::decompress(const std::string& pathInZipFile, const std::string& pathInLocal)
{
    std::ofstream   os;
    os.open(pathInLocal.c_str());
    return decompress(pathInZipFile, os);
}

bool ZipFileDecompressor::decompress(const std::string& pathInZipFile, std::ostream& os)
{
    bool bSuccess = false;
    do{
        if(::unzLocateFile(m_unzFile, pathInZipFile.c_str(), 0) != UNZ_OK) break;
        
        std::vector<char>   buff(4 * 1024);
        while(true){
            const int readBytes = ::unzReadCurrentFile(m_unzFile, buff.data(), static_cast<uInt>(buff.size()));
            if(readBytes < 0) break;
            if(readBytes == 0){
                bSuccess = true;
                break;
            }
            os.write(buff.data(), readBytes);
        }
        (void)::unzCloseCurrentFile(m_unzFile);
    } while(false);
    return bSuccess;
}

bool ZipFileDecompressor::decompressAll(const std::string& basePathInLocal)
{
    auto base = boost::filesystem::path(basePathInLocal);
    boost::filesystem::remove_all(base);
    return enumerate([=](const std::string& path, boost::shared_ptr<std::ostream>& os){
        auto fpath = base / path;
        boost::filesystem::create_directories(fpath.parent_path());
        os = boost::make_shared<std::ofstream>(fpath.string());
        return true;
   });
}

//
//  JobNFCMasterDataCheckJMups.cpp
//  ppsdk
//
//  Created by codianz on 2020/02/26.
//  Copyright © 2020年 Clementec Co., Ltd. All rights reserved.
//

#include "JobNFCMasterDataCheckJMups.h"
#include "TMSAccess.h"

JobNFCMasterDataCheckJMups::JobNFCMasterDataCheckJMups(const __secret& s) :
    PaymentJob(s)
{
    LogManager_Function();
    m_bMasterDataChecked = false;
}

/* virtual */
JobNFCMasterDataCheckJMups::~JobNFCMasterDataCheckJMups()
{
    LogManager_Function();
    if(!m_bMasterDataChecked){
        json_t checkInfo;
        checkInfo.put("RWSerial"    , "");
        // bNeedUpdate項目 なし
        TMSAccess::setMasterDataCheckInfo(checkInfo);
    }
}

/* virtual */
auto JobNFCMasterDataCheckJMups::getObservableSelf()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return waitForReaderConnect().as_dynamic()
    .flat_map([=](auto){
        return masterDataDownload(true).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        m_bMasterDataChecked = true;
        return masterDataUpdateCheck(getCardReader()).as_dynamic();
    }).as_dynamic()
    .map([=](const_json_t resut){
        m_bMasterDataChecked = true;
        json_t j = resut.clone();
        j.put("status", "success");
        return j.as_readonly();
    }).as_dynamic()
    .on_error_resume_next([=](auto err){
        LogManager_AddError(pprx::exceptionToString(err));
        m_bMasterDataChecked = true;
        json_t checkInfo;
        checkInfo.put("RWSerial"    , "");
        // bNeedUpdate項目 なし
        TMSAccess::setMasterDataCheckInfo(checkInfo);
        return pprx::error<const_json_t>(err).as_dynamic();
    }).as_dynamic();;
}

/* virtual */
auto JobNFCMasterDataCheckJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto JobNFCMasterDataCheckJMups::isNeedWaitForShowingJobResult() -> bool
{
    return false;
}

auto JobNFCMasterDataCheckJMups::mainProcessChildJob(const_json_t data) -> pprx::observable<pprx::unit>
{
    return waitForReaderConnect().as_dynamic()
    .flat_map([=](auto){
        return masterDataDownload(true).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return masterDataUpdateCheck(getCardReader()).as_dynamic();
    }).as_dynamic()
    .flat_map([=](auto){
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

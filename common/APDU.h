//
//  APDU.h
//  ppsdk
//
//  Created by tel on 2017/09/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_APDU__)
#define __h_APDU__

#include "BaseSystem.h"
#include "LogManager.h"
#include <algorithm>

/* ISO 7816-4/8/9 */

class APDU
{
public:
    /* ISO7816 - Case 1 */
    /* CLA | INS | P1 | P2 */
    /* CLA ... クラス */
    /* INS ... 命令 */
    /* P1  ... パラメータ */
    /* P2  ... パラメータ */

    /* ISO7816 - Case 2 */
    /* CLA | INS | P1 | P2 | Le */
    /* Le ... 受信最大サイズ 0 - 0xFFFF （0-3バイト）*/
    
    
    /* ISO7816 - Case 3 */
    /* CLA | INS | P1 | P2 | Lc | data */
    /* Lc ... data バイト数（0, 1, 3 バイト） */
    /* data ... データ*/

    
    /* ISO7816 - Case 4 */
    /* CLA | INS | P1 | P2 | Lc | data | Le */
    
    
    /* status word */
    /* SW1 | SW2 */
    /* data | SW1 | SW2 */
    /* length を表すものは存在しない */
    
/*
    SW1     SW2
    0x90	0x00	正常終了。
    0x62	n       警告処理。不揮発性メモリの状態が変化していない。
            0x81	出力データに異常がある。
            0x83	DFが閉塞(へいそく)している。
    0x63	n       警告処理。不揮発性メモリの状態が変化している。
            0x00	照合不一致である。
            0x81	ファイルが今回の書き込みによっていっぱいになった。
            0xcn	照合不一致である。'n'によって、残りの再試行回数(1～15)を示す。
    0x64	n       実行誤り。不揮発性メモリの状態が変化していない。
            0x00	ファイル制御情報に異常がある。
    0x65	n       実行誤り。不揮発性メモリの状態が変化している。
            0x00	メモリへの書き込みが失敗した。
    0x67	00      Lc/Leフィールドが間違っている。
    0x68	n       検査誤り。CLAの機能が提供されない。
            0x81	指定された論理チャンネル番号によるアクセス機能を提供しない。
            0x82	CLAバイトで指定されたセキュアメッセージング機能を提供しない。
    0x69	n       検査誤り。コマンドは許されない。
            0x81	ファイル構造と矛盾したコマンドである。
            0x82	セキュリティステータスが満足されない。
            0x84	認証方法を受け付けない。
            0x84	参照されたIEFが閉塞している。
            0x85	コマンドの使用条件が満足されない。
            0x86	ファイルが存在しない。
            0x87	セキュアメッセージングに必要なデータオブジェクトが存在しない。
            0x88	セキュアメッセージング関連エラー。
    0x6a	n       検査誤り。間違ったパラメータP1,P2。
            0x80	データフィールドのタグが正しくない。
            0x81	機能が提供されていない。
            0x82	ファイルが存在しない。
            0x83	アクセス対象のレコードがない。
            0x84	ファイル内に十分なメモリ容量がない。
            0x85	Lcの値がTLV構造に矛盾している。
            0x86	P1-P2の値が正しくない。
            0x87	Lcの値がP1-P2に矛盾している。
            0x88	参照された鍵が正しく設定されていない。
    0x6b	0x00	EF範囲外にオフセット指定した(検査誤り)。
    0x6d	0x00	INSが提供されていない(検査誤り)。
    0x6e	0x00	CLAが提供されていない(検査誤り)。
    0x6f	0x00	自己診断異常(検査誤り)。
*/
    
    
    class Command
    {
    private:
        bytes_t m_raw;
        
    public:
        Command(uint8_t cla, uint8_t ins, uint8_t p1, uint8_t p2);
        Command(uint8_t cla, uint8_t ins, uint8_t p1, uint8_t p2, const bytes_t& data);
        Command(uint8_t cla, uint8_t ins, uint8_t p1, uint8_t p2, const bytes_t& data, uint32_t ne);
        Command(uint8_t cla, uint8_t ins, uint8_t p1, uint8_t p2, uint32_t ne);
        
        /*
         byte[]	getBytes()
         このAPDUのバイトのコピーを返します。
         int	getCLA()
         クラス・バイトCLAの値を返します。
         byte[]	getData()
         コマンド本体のデータ・バイトのコピーを返します。
         int	getINS()
         命令バイトINSの値を返します。
         int	getNc()
         コマンド本体(Nc)のデータ・バイト数を返します。このAPDUに本体がない場合は0を返します。
         int	getNe()
         レスポンスAPDU (Ne)内の予想される最大データ・バイト数を返します。
         int	getP1()
         パラメータ・バイトP1の値を返します。
         int	getP2()
         パラメータ・バイトP2の値を返します。
         int	hashCode()
         このコマンドAPDUのハッシュ・コード値を返します。
         String	toString()
         このコマンドAPDUの文字列表現を返します。
         */
        
        
    };
    
    class Response
    {
    private:
        
    public:
        /*
        byte[]	getBytes()
        このAPDUのバイトのコピーを返します。
        byte[]	getData()
        応答本体のデータ・バイトのコピーを返します。
        int	getNr()
        応答本体(Nr)のデータ・バイト数を返します。このAPDUに本体がない場合は0を返します。
        int	getSW()
        単一のステータス・ワードSWとして、ステータス・バイトSW1およびSW2の値を返します。
        int	getSW1()
        0から255の間の値としてステータス・バイトSW1の値を返します。
        int	getSW2()
        0から255の間の値としてステータス・バイトSW2の値を返します。
        int	hashCode()
        このレスポンスAPDUのハッシュ・コード値を返します。
        String	toString()
        このレスポンスAPDUの文字列表現を返します。
        */
        
    };
    
    
    class TLV
    {
    private:
        bytes_t m_tag;
        bytes_t m_value;
        
        bytes_t getLengthBytes() const
        {
            bytes_t result;
            if(m_value.size() <= 0x7F){
                result.push_back(m_value.size());
            }
            else if(m_value.size() <= 0xFF){
                result.push_back(0x81);
                result.push_back(m_value.size());
            }
            else if(m_value.size() <= 0xFFFF){
                result.push_back(0x82);
                result.push_back(m_value.size() >> 8);
                result.push_back(m_value.size());
            }
            else{
                LogManager_AddErrorF("bad length %d", m_value.size());
            }
            return result;
        }
        
    public:
        TLV(const bytes_t& tag, const bytes_t& value) :
            m_tag(tag), m_value(value)
        {
        }

        TLV(const bytes_t& tag) :
            m_tag(tag)
        {
        }

        bytes_t getBytes() const
        {
            bytes_t result;
            auto length = getLengthBytes();
            std::copy(std::begin(m_tag),    std::end(m_tag),    std::back_inserter(result));
            std::copy(std::begin(length),   std::end(length),   std::back_inserter(result));
            std::copy(std::begin(m_value),  std::end(m_value),  std::back_inserter(result));
            return result;
        }
        
        void innerAppend(const bytes_t& tag, const bytes_t& value)
        {
            innerAppend(TLV(tag, value));
        }
        
        void innerAppend(const TLV& src)
        {
            auto sbytes = src.getBytes();
            std::copy(std::begin(sbytes), std::end(sbytes), std::back_inserter(m_value));
        }
        
    };
};



#endif /* !defined(__h_APDU__) */

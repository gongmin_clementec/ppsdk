//
//  CardReaderUtil.cpp
//  ppsdk
//
//  Created by tel on 2018/11/14.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#include "CardReaderUtil.h"
#include "LogManager.h"
#include <boost/algorithm/hex.hpp>

/* virtual */
CardReaderUtil::CardReaderUtil()
{
    LogManager_Function();
}

CardReaderUtil::~CardReaderUtil()
{
    LogManager_Function();
}

// R/Wからの応答データのうち、SW1SW2を文字列に変換して返す
auto CardReaderUtil::retrieveSW1SW2(const bytes_t response) -> std::string
{
    bytes_t temp;
    temp.assign(response.end() - 2, response.end());
    return binaryToHextext(temp);
}

auto CardReaderUtil::binaryToHextext(const bytes_t response) -> std::string
{
    std::string text;
    boost::algorithm::hex(response.begin(), response.end(), std::back_inserter(text));
    return text;
}

// カードリーダからの応答APDUから任意のタグの先頭位置を返す
auto CardReaderUtil::getTLVDataFromRAPDU(const bytes_t rapdu, const bytes_t searchTag) -> bytes_t
{
    if(rapdu.size() <= 3) { return {}; }
    // スキップするテンプレートタグリスト
    std::vector<bytes_t> templateTags = {
        {0xA5}, 
        {0x61}, {0x6F}, 
        {0x70}, {0x71}, {0x72}, {0x73}, {0x77}, 
        {0xBF, 0x0C}, 
        {0xE0}, {0xE1}, {0xE2}, {0xE3}, {0xE4}, {0xE5}, {0xE6}, {0xE7}, {0xE8}, {0xEE}
    };
    bytes_t tag;
    bytes_t value = {};
    int32_t length_T;
    int32_t length_L;
    int32_t length_V;
    auto bodylen = (rapdu.size() - 2);
    // SWの前まで順にパース
    for(int32_t i = 0; i < bodylen;) {
        length_V = 0;
        // タグの取得(EMV Book3, B1 Coding of the Tag Field of BER-TLV Data Objects)
        if((rapdu[i] & 0x1F) != 0x1F) {
            // 1バイト目の下位5ビットがすべてONでなければ1バイトタグ
            length_T = 1;
        } else {
            // 1バイト目の下位5ビットがすべてONなら複数バイトタグ
            // 2バイト目は最上位ビットがONなら3バイトタグ、OFFなら2バイトタグと判断する。
            // 仕様上は2バイト目以降はMSBがONである限りタグは伸ばせるようだが
            // 4バイト以上のタグは使っていないようなので3バイトまでの判定とする。
            length_T = (rapdu[i + 1] & 0x80) ? 3 : 2;
        }
        tag.assign(rapdu.begin() + i, rapdu.begin() + i + length_T);
        i += length_T;
        // 長さの取得(EMV Book3, B2 Coding of the Length Field of BER-TLV Data Objects)
        if((rapdu[i] & 0x80) == 0) {
            length_L = 1;
            length_V = rapdu[i];
            i += 1;
        } else {
            // MSBがONの場合、下位7ビットが長さフィールド長を表す
            length_L = rapdu[i] & 0x7F;
            i += 1;
            for(int j = length_L; j > 0; j--) {
                length_V |= rapdu[i++] << 8 * (j - 1);
            }
        }
        // テンプレートは値を取得せずに次のタグの処理に飛ばす
        if([=]() -> bool {
            for(bytes_t templatetag : templateTags) {
                if(tag == templatetag) {
                    return true;
                }
            }
            return false;
        }()) { continue; }
        // 値の設定
        if(tag == searchTag) {
            value.assign(rapdu.begin() + i, rapdu.begin() + i + length_V);
            LogManager_AddDebug((boost::format("tag = %s, length = %d, value = %s") %
                binaryToHextext(tag) % length_V % binaryToHextext(value)).str());
            break;
        }
        i += length_V;
    }
    return value;
}


auto CardReaderUtil::get48Data(const bytes_t rapdu, const bytes_t searchTag) -> bytes_t
{
    if(rapdu.size() <= 3) {
        return {};
    } else if(rapdu[0] != 0xE1) {
        return {};
    }
    std::vector<bytes_t> templateTags = {
        {0xA5}, 
        {0x61}, {0x6F}, 
        {0x70}, {0x71}, {0x72}, {0x73}, {0x77}, 
        {0xBF, 0x0C}, 
        {0xE0}, {0xE1}, {0xE2}, {0xE3}, {0xE4}, {0xE5}, {0xE6}, {0xE7}, {0xE8}, {0xEE}
    };
    bytes_t tag;
    bytes_t value = {};
    int32_t length_T;
    int32_t length_L;
    int32_t length_V;
    auto bodylen = (rapdu.size() - 2);
    // SWの前まで順にパース
    for(int32_t i = 0; i < bodylen;) {
        length_V = 0;
        // タグの取得(EMV Book3, B1 Coding of the Tag Field of BER-TLV Data Objects)
        if((rapdu[i] & 0x1F) != 0x1F) {
            // 1バイト目の下位5ビットがすべてONでなければ1バイトタグ
            length_T = 1;
        } else {
            // 1バイト目の下位5ビットがすべてONなら複数バイトタグ
            // 2バイト目は最上位ビットがONなら3バイトタグ、OFFなら2バイトタグと判断する。
            // 仕様上は2バイト目以降はMSBがONである限りタグは伸ばせるようだが
            // 4バイト以上のタグは使っていないようなので3バイトまでの判定とする。
            length_T = (rapdu[i + 1] & 0x80) ? 3 : 2;
        }
        tag.assign(rapdu.begin() + i, rapdu.begin() + i + length_T);
        i += length_T;
        // 長さの取得(EMV Book3, B2 Coding of the Length Field of BER-TLV Data Objects)
        if((rapdu[i] & 0x80) == 0) {
            length_L = 1;
            length_V = rapdu[i];
            i += 1;
        } else {
            // MSBがONの場合、下位7ビットが長さフィールド長を表す
            length_L = rapdu[i] & 0x7F;
            i += 1;
            for(int j = length_L; j > 0; j--) {
                length_V |= rapdu[i++] << 8 * (j - 1);
            }
        }
        // テンプレートは値を取得せずに次のタグの処理に飛ばす
        if([=]() -> bool {
            for(bytes_t templatetag : templateTags) {
                if(tag == templatetag) {
                    LogManager_AddDebugF("tag [%s] template skip", binaryToHextext(tag));
                    return true;
                }
            }
            return false;
        }()) { continue; }
        // 値の設定
        if(tag == searchTag) {
            value.assign(rapdu.begin() + i, rapdu.begin() + i + length_V);
            LogManager_AddDebug((boost::format("tag = %s, length = %d, value = %s") %
                binaryToHextext(tag) % length_V % binaryToHextext(value)).str());
            break;
        }
        i += length_V;
    }
    return value;
}

auto CardReaderUtil::createTemplateData(const std::string tagName, const std::vector<std::string> data) -> bytes_t
{
    uint32_t datalen = 0;
    std::string datastr;
    for(const auto &str : data) {
        datastr.append(str);
        datalen += (str.size() / 2);
    }
    std::string bodystr = (boost::format("%02x%s%02x%s") %
        (1 + (tagName.size() / 2) + datalen) %  // データ長(1byte) + tag長(可変) + E0 template内のデータ長
        tagName %                               // 出力するテンプレートのタグ名
        datalen %                               // テンプレート内のデータ長
        datastr                                 // データ
    ).str();
    bytes_t ret;
    boost::algorithm::unhex(bodystr.begin(), bodystr.end(), std::back_inserter(ret));
    return ret;
}

auto CardReaderUtil::getTagLength(const bytes_t::const_iterator apdu) -> int32_t
{
    int32_t ret;
    // タグの取得(EMV Book3, B1 Coding of the Tag Field of BER-TLV Data Objects)
    if((*apdu & 0x1F) != 0x1F) {
        // 1バイト目の下位5ビットがすべてONでなければ1バイトタグ
        ret = 1;
    } else {
        // 1バイト目の下位5ビットがすべてONなら複数バイトタグ
        // 2バイト目は最上位ビットがONなら3バイトタグ、OFFなら2バイトタグと判断する。
        // 仕様上は2バイト目以降はMSBがONである限りタグは伸ばせるようだが
        // 4バイト以上のタグは使っていないようなので3バイトまでの判定とする。
        ret = (*(apdu + 1) & 0x80) ? 3 : 2;
    }
    return ret;
}

auto CardReaderUtil::parseTLVtoStr(const bytes_t tlv) -> std::vector<std::string>
{
    // スキップするテンプレートタグリスト
    std::vector<bytes_t> templateTags = {
        {0xA5}, 
        {0x61}, {0x6F}, 
        {0x70}, {0x71}, {0x72}, {0x73}, {0x77}, 
        {0xBF, 0x0C}, 
        {0xE0}, {0xE1}, {0xE2}, {0xE3}, {0xE4}, {0xE5}, {0xE6}, {0xE7}, {0xE8}, {0xED}, {0xEE}
    };
    std::vector<std::string> ret;
    bytes_t tag;
    bytes_t value = {};
    int32_t length_T = 0;
    int32_t length_L = 0;
    int32_t length_V = 0;
    auto bodylen = (tlv.size() - 2);
    for(int32_t i = 0; i < bodylen;) {
        length_V = 0;
        length_T = getTagLength(tlv.begin() + i);
        tag.assign(tlv.begin() + i, tlv.begin() + i + length_T);
        i += length_T;
        // 長さの取得(EMV Book3, B2 Coding of the Length Field of BER-TLV Data Objects)
        if((tlv[i] & 0x80) == 0) {
            length_L = 1;
            length_V = tlv[i];
            i += 1;
        } else {
            // MSBがONの場合、下位7ビットが長さフィールド長を表す
            length_L = tlv[i] & 0x7F;
            i += 1;
            for(int j = length_L; j > 0; j--) {
                length_V |= tlv[i++] << 8 * (j - 1);
            }
        }
        if([=]() -> bool {
            for(bytes_t templatetag : templateTags) {
                if(tag == templatetag) {
                    return true;
                }
            }
            return false;
        }()) {
            // テンプレートはタグとレングスのみ出力
            ret.push_back((boost::format("%-6s %04x") % binaryToHextext(tag) % length_V).str());
        } else {
            value.assign(tlv.begin() + i, tlv.begin() + i + length_V);
            ret.push_back((boost::format("%-6s %04x %s") % binaryToHextext(tag) % length_V % binaryToHextext(value)).str());
            i += length_V;
        }
    }
    return ret;
}

auto CardReaderUtil::debugTlvDump(const bytes_t tlv) -> void
{
    auto dumpDatas = parseTLVtoStr(tlv);
    std::string log = "\n";
    for(std::string str : dumpDatas) {
        log += str + "\n";
    }
    LogManager_AddDebugF("%s", log);
}


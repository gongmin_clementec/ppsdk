//
//  ZipFileCompressor.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_ZipFileCompressor__)
#define __h_ZipFileCompressor__

#include <string>
#include <iostream>

class ZipFileCompressor
{
private:
    typedef void*    zipfile_t;
    zipfile_t       m_zipfile;
    
    uint32_t getStreamCrc32(std::istream& is) const;
    
protected:
    
public:
            ZipFileCompressor();
    virtual ~ZipFileCompressor();
    bool    open(const std::string& pathname);
    void    close();
    bool    add(const std::string& pathInLocal, const std::string& pathInZipFile);
    bool    add(std::istream& is, const std::string& pathInZipFile);
};

#endif /* !defined(__h_ZipFileCompressor__) */

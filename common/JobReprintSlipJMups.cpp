//
//  JobReprintSlipJMups.cpp
//  ppsdk
//
//  Created by clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JobReprintSlipJMups.h"

JobReprintSlipJMups::JobReprintSlipJMups(const __secret& s) :
    PaymentJob(s)
    ,m_paymentType(prmReprintCommon::paymentType::Bad)
{
}

/* virtual */
JobReprintSlipJMups::~JobReprintSlipJMups()
{
    LogManager_Function();
}

/* virtual */
auto JobReprintSlipJMups::getObservableSelf()
-> pprx::observable<const_json_t>
{
    return callOnDispatchWithCommand("waitForReprintOptions")
    .flat_map([=](const_json_t json){
        m_paymentType = prmReprintCommon::stringToPaymentType(json.get<std::string>("paymentType"));
        auto slipNo = json.get_optional<std::string>("slipNo");
        if(slipNo){
            return hpsReprintWithSlipNo(m_paymentType, *slipNo);
        }
        else{
            return hpsReprintLastSlip(m_paymentType);
        }
    }).as_dynamic()
    .flat_map([=](response_t resp){
        return callOnDispatchWithCommandAndSubParameter("waitForReprintConfirm", "hps", resp.body).as_dynamic()
        .map([=](auto){
            return resp;
        }).as_dynamic();
    }).as_dynamic()
    .flat_map([=](response_t resp){
        return treatSlipDocumentJson(resp)
        .flat_map([=](auto json){
            return slipPrinterExecute(json);
        }).as_dynamic()
        .map([=](auto){
            return resp;
        }).as_dynamic();
    }).as_dynamic()
    .map([=](response_t resp){
        json_t result;
        result.put("status", "success");
        result.put("result.hps", addMediaDivToCardNo(resp.body.clone()));
        return result.as_readonly();
    });
}

/* virtual */
auto JobReprintSlipJMups::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}


auto JobReprintSlipJMups::treatSlipDocumentJson(JMupsAccess::response_t resp)
-> pprx::observable<const_json_t>
{
    auto pinfolist = resp.body.get_optional<json_t::array_t>("normalObject.printInfoList");
    if(!pinfolist){
        throw_error_internal("printInfoList not found");
    }
    if(pinfolist->size() == 0){
        throw_error_internal("printInfoList size 0");
    }
    auto pinfo = (*pinfolist)[0].get("device.value").clone();
    auto values = pinfo.get<json_t::array_t>("values");
    
    // TAX は開局情報によって判断する
    bool isNeedTaxOther = false;
    auto inputTaxOther = resp.body.get_optional<std::string>("normalObject.dllPatternData.inputTaxOther");
    if(inputTaxOther && ((*inputTaxOther) == "1")){
        isNeedTaxOther = true;
    }
    
    for(auto v : values){
        // TAX は開局情報によって削除しなければならない
        if(!isNeedTaxOther)/* 税・その他が不要な場合削除する　*/
        {
            v.remove_key_if_exist("TAX");
        }
        auto cupNo = v.get_optional<std::string>("CUP_NO");
        if(cupNo){ // 銀聯番号が存在なら、銀聯の業務か確認済み。
            auto mediaDiv = v.get<std::string>("PHYSICAL_MEDIA_DIV");
            auto cardNo   = v.get<std::string>("CARD_NO");
            auto cardNoWithMediaDiv = mediaDiv + " " + cardNo;
            v.put("CARD_NO", cardNoWithMediaDiv);
        }
        
        // 署名
        auto sign = v.get_optional<std::string>("SIGN");
        if(sign){
            v.put("SIGN", "1");
        }
    }
    pinfo.put("values", values);
    
    return pprx::just(pinfo.as_readonly());
}

/* virtual */
auto JobReprintSlipJMups::slipPrinterDispatch(const std::string& command, const_json_t subParam) -> pprx::observable<bool>
{
    return callOnDispatchWithCommandAndSubParameter(command, "", subParam)
    .map([=](const_json_t json){
        return json.get<bool>("bExecute");
    }).as_dynamic();
}

/* virtual */
auto JobReprintSlipJMups::slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>
{
    return callOnDispatchWithMessageAndSubParameter(message, "", subParam);
}

auto JobReprintSlipJMups::addMediaDivToCardNo(const_json_t tradeData) -> json_t
{
    LogManager_Function();
    
    if(m_paymentType != prmReprintCommon::paymentType::UnionPay){
        return tradeData.clone();
    }
    
    json_t resp = tradeData.clone();
    auto printInfoList = tradeData.get_optional<json_t::array_t>("normalObject.printInfoList");
    if(!printInfoList || (printInfoList->size()== 0)){
        return tradeData.clone();
    }
    
    for(auto list : *printInfoList){
        auto values = list.get<json_t::array_t>("device.value.values");
        /* カードMEDIA_DIVを追加　ICかMSか */
        for(auto v : values){
            auto mediaDiv = v.get_optional<std::string>("PHYSICAL_MEDIA_DIV");
            auto cardNo   = v.get_optional<std::string>("CARD_NO");
            
            if(mediaDiv){
                auto cardNoWithMediaDiv = *mediaDiv + " " + *cardNo;
                v.put("CARD_NO", cardNoWithMediaDiv);
            }
        }
        
        /* printInfoのvalueを再構築 */
        list.put("device.value.values", values);
    }
    /* printInfoListを再構築 */
    resp.put("normalObject.printInfoList", *printInfoList);

    return resp;
}

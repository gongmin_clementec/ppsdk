//
//  JMupsJobStorage.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JMupsStorage__)
#define __h_JMupsStorage__

#include "PaymentJob.h"

class JMupsJobStorage :
    public PaymentJob::Storage
{
public:
    using sp = boost::shared_ptr<JMupsJobStorage>;
    
private:
    json_t m_openTerminalResponce;
    json_t m_cookies;
    json_t m_nfcMasterData;
    json_t m_tradeResultData;
    json_t m_inputTradeData;
    bool   m_bTrainingModePinInputted;

    std::mutex  m_hpsUrl_mtx;
    std::vector<std::string> m_vHpsUrl;
    
protected:
public:
            JMupsJobStorage();
    virtual ~JMupsJobStorage();
    
    void            setTerminalInfo(const_json_t json)      { m_openTerminalResponce = json.clone(); }
    const_json_t    getTerminalInfo() const                 { return m_openTerminalResponce.as_readonly(); }

    void            setCookies(const_json_t json)           { m_cookies = json.clone(); }
    const_json_t    getCookies() const                      { return m_cookies.as_readonly(); }
    
    void            setNfcMasterData(const_json_t json)     { m_nfcMasterData = json.clone(); }
    const_json_t    getNfcMasterData() const                { return  m_nfcMasterData.as_readonly(); }

    void            setTradeResultData(const_json_t json)   { m_tradeResultData = json.clone(); }
    const_json_t    getTradeResultData() const              { return  m_tradeResultData.as_readonly(); }

    void            setInputTradeData(const_json_t json)   { m_inputTradeData = json.clone(); }
    const_json_t    getInputTradeData() const              { return  m_inputTradeData.as_readonly(); }
    
    void            saveCurrentHpsUrl(const std::string hpsUrl);
    void            getCurrentHpsUrl(std::string& hpsUrl);
    
    void            setTrainingModePinInputted(bool bInputted)   { m_bTrainingModePinInputted = bInputted; }
    bool            getTrainingModePinInputted() const           { return m_bTrainingModePinInputted; }
    
    virtual void serialize(std::ostream& os) const;
    virtual void deserialize(std::istream& is);
};



#endif /* !defined(__h_JMupsStorage__) */

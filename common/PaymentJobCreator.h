//
//  PaymentJobCreator.h
//  ppsdk
//
//  Created by Clementec on 2017/05/31.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_PaymentJobCreator__)
#define __h_PaymentJobCreator__

#include <unordered_map>
#include "LazyObject.h"

class PaymentJob;

class PaymentJobCreator
{
private:
    using creator_t = boost::function<boost::shared_ptr<PaymentJob>()>;
    using map_t     = std::unordered_map<std::string,creator_t>;
    template<class T> static boost::shared_ptr<PaymentJob> creator()
    {
        return LazyObject::create<T>();
    }
    map_t  m_map;
    
protected:
    
public:
    PaymentJobCreator();
    
    boost::shared_ptr<PaymentJob>  create(const std::string& jobName) const
    {
        boost::shared_ptr<PaymentJob> job;
        const auto it = m_map.find(jobName);
        if(it != m_map.end()){
            job = it->second();
        }
        return job;
    }
};






#endif /* !defined(__h_PaymentJobCreator__) */

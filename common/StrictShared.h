//
//  StrictShared.h
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_StrictShared__)
#define __h_StrictShared__

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

/*
 必ずshared_ptrを使用しなければならないクラスを提供する。
 
 boost::noncopyableでコピーコンストラクタおよびoperator =を隠蔽し、
 コンストラクタ引数を非public領域のempty structを使用し、
 クラス外部からインスタンス生成はできず、create()の使用を強要させる。
 
 派生クラスは下記のようにコンストラクタを定義すること。
 
         class DerivedClass : public StrictShared
         {
         public:
            DerivedClass(const __secret& s) : StrictShared(s) {}
         };
 
 クラス生成は下記のとおり行う。
 
        auto instance = StrictShared::create<DerivedClass>();
 
 このStrictShared派生クラスでは下記のコードはコンパイルエラーとなる。
 （このクラスの効果）
 
         DerivedClass a();              // エラー（デフォルトコンストラクタ不在）
         auto b = new DerivedClass();   // エラー（デフォルトコンストラクタ不在）
         
         auto c = new DerivedClass(StrictShared::__secret); // エラー（__secretはprotected）
         auto d = StrictShared::create<DerivedClass>();     // OK
         DerivedClass e(*d);            // エラー（コピーコンストラクタは隠蔽されている）
*/

class StrictShared :
    public boost::noncopyable
{
private:
    
protected:
    struct __secret{
        explicit __secret() = default;
    };
    
public:
    explicit StrictShared(const __secret&) {}
    virtual ~StrictShared() = default;
    
    template<class T> static boost::shared_ptr<T> create()
    {
        return boost::make_shared<T>(__secret());
    }
};

#endif /* !defined(__h_StrictShared__) */

//
//  CardReaderUtil.h
//  ppsdk
//
//  Created by tel on 2018/11/14.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_CardReaderUtil__)
#define __h_CardReaderUtil__

#include "BaseSystem.h"

class CardReaderUtil
{
public:
    CardReaderUtil();
    virtual ~CardReaderUtil();
    static auto retrieveSW1SW2(const bytes_t response) -> std::string;
    static auto binaryToHextext(const bytes_t response) -> std::string;
    static auto getTLVDataFromRAPDU(const bytes_t rapdu, const bytes_t searchTag) -> bytes_t;
    static auto get48Data(const bytes_t rapdu, const bytes_t searchTag) -> bytes_t;
    static auto createTemplateData(const std::string tagName, const std::vector<std::string> data) -> bytes_t;
    static auto getTagLength(const bytes_t::const_iterator apdu) -> int32_t;
    static auto parseTLVtoStr(const bytes_t apdu) -> std::vector<std::string>;
    static auto debugTlvDump(const bytes_t tlv) -> void;

protected:
};


#endif /* !defined(__h_CardReaderUtil__) */

#if ! defined( __h_base64stream__ )
#define __h_base64stream__

#include <streambuf>
#include <iostream>
#include <vector>


namespace ORANGEWERKS {
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_base64streambuf : public std::basic_streambuf<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_streambuf<_Elem,_Traits>;
        
        std::basic_ostream<_Elem,_Traits>*		m_out;
        std::basic_istream<_Elem,_Traits>*		m_in;
        
        std::vector<_Elem>		m_buff;		/* バッファ実体 */
        _Elem*					m_pBuff;	/* バッファポインタ */
        std::streamsize			m_nBuff;	/* バッファサイズ */
        
        
    protected:
        /* 外部からの出力バッファ指定 */
        virtual superclass_t* setbuf( _Elem* b, std::streamsize s )
        {
            m_buff.clear();
            if( m_out ){
                this->setp( b, b + s );
            }
            if( m_in ){
                this->setg( b, b, b + s );
            }
            m_pBuff = b;
            m_nBuff = s;
            return( this );
        }
        
        /* 書き込みバッファが一杯になった時に呼び出される */
        virtual typename superclass_t::int_type overflow( typename superclass_t::int_type c = _Traits::eof() )
        {
            overflow_self();
            if( c == _Traits::eof() ){
                return( _Traits::eof() );
            }
            /* c はバッファから溢れたオブジェクトなので、ここで追加する */
            *superclass_t::pptr() = _Traits::to_char_type( c );
            superclass_t::pbump( 1 );
            return( _Traits::not_eof( c ) );
        }
        
        /* 読み込みバッファが空になった時に呼び出される */
        virtual typename superclass_t::int_type underflow()
        {
            if( superclass_t::egptr() <= superclass_t::gptr() ){
                underflow_self();
                if( superclass_t::egptr() <= superclass_t::gptr() ){
                    return( _Traits::eof() );
                }
            }
            return( _Traits::to_int_type( *superclass_t::gptr() ) );
        }
        
        /* flush() で呼び出される */
        virtual int sync()
        {
            if( m_out ){
                encode();
                m_out->flush();
            }
            return( 0 );
        }
        
        
        void overflow_self()
        {
            encode();
        }
        
        void encode( bool bFinish = false )
        {
            constexpr char base64_table[ 65 ] =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            
            const ptrdiff_t length = superclass_t::pptr() - m_pBuff;		/* 使用バッファサイズ */
            if( length == 0 ) return;						/* もうデータが存在していない */
            
            if( bFinish ){
            }
            else{
                if( length < 3 ){
                    return;
                }
            }
            
            char out[ 4 ];
            if( length == 3 ){
                out[0] = base64_table[ ((m_pBuff[0]>>2)&0x3F)								& 0x3F];
                out[1] = base64_table[(((m_pBuff[0]<<4)&0xF0) | ((m_pBuff[1]>>4)&0x0F))		& 0x3F];
                out[2] = base64_table[(((m_pBuff[1]<<2)&0xFC) | ((m_pBuff[2]>>6)&0x03))		& 0x3F];
                out[3] = base64_table[   m_pBuff[2]											& 0x3F];
            }
            else if( length == 2 ){
                out[0] = base64_table[ ((m_pBuff[0]>>2)&0x3F)								& 0x3F];
                out[1] = base64_table[(((m_pBuff[0]<<4)&0xF0) | ((m_pBuff[1]>>4)&0x0F))		& 0x3F];
                out[2] = base64_table[ ((m_pBuff[1]<<2)&0xFC)								& 0x3F];
                out[3] = '=';
            }
            else if( length == 1 ){
                out[0] = base64_table[ ((m_pBuff[0]>>2)&0x3F)								& 0x3F];
                out[1] = base64_table[ ((m_pBuff[0]<<4)&0xF0)								& 0x3F];
                out[2] = '=';
                out[3] = '=';
            }
            
            m_out->write( out, 4 );	/* stream に出力 */
            this->setp( m_pBuff, m_pBuff + m_nBuff );	/* buffer ポインタリセット */
        }
        
        void underflow_self()
        {
            char in[ 4 ] = { 0, 0, 0, 0 };
            m_in->read( in, 4 );						/* stream から入力してバッファに展開 */
            
            size_t bytes = 0;
            for( int i = 0; i < 4; ++i ){
                char n = 0;
                
                if ('A' <= in[i] && in[i] <= 'Z') n = (in[i]-'A') ;
                else if ('a' <= in[i] && in[i] <= 'z') n = (in[i]-'a'+26);
                else if ('0' <= in[i] && in[i] <= '9') n = (in[i]-'0'+52);
                else if ('+' == in[i]) n = 62;
                else if ('/' == in[i]) n = 63;
                else if ('=' == in[i]) break;
                else break;
                
                switch (i)
                {
                    case 0:
                        m_pBuff[0]  = n << 2;
                        bytes = 1;
                        break;
                    case 1:
                        m_pBuff[0] |= (n >> 4);
                        m_pBuff[1]  = (n << 4);
                        if (m_pBuff[1]) bytes = 2;
                        break;
                    case 2:
                        m_pBuff[1] |= (n >> 2);
                        m_pBuff[2]  = (n << 6);
                        if (m_pBuff[2]) bytes = 3;
                        break;
                    case 3:
                        m_pBuff[2] |= n;
                        bytes = 3;
                        break;
                }
            }
            
            this->setg( m_pBuff, m_pBuff, m_pBuff + bytes );			/* buffer ポインタリセット */
        }
        
        
    public:
        basic_base64streambuf( std::basic_ostream<_Elem,_Traits>& os ) :
        m_out( &os ), m_in( NULL )
        {
            m_buff.resize( 3 );
            
            m_pBuff = &m_buff.front();
            m_nBuff = m_buff.size();
            
            this->setp( m_pBuff, m_pBuff + m_nBuff );
        }
        
        basic_base64streambuf( std::basic_istream<_Elem,_Traits>& is ) :
        m_out( NULL ), m_in( &is )
        {
            m_buff.resize( 3 );
            
            m_pBuff = &m_buff.front();
            m_nBuff = m_buff.size();
            
            this->setg( m_pBuff, m_pBuff + m_nBuff, m_pBuff + m_nBuff );
        }
        
        virtual ~basic_base64streambuf()
        {
            if( m_out ){
                encode( true );
            }
            sync();
        }
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_obase64stream : public std::basic_ostream<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_ostream<_Elem,_Traits>;
    protected:
    public:
        basic_obase64stream( std::ostream& os ) :
        std::basic_ostream<_Elem,_Traits>( new basic_base64streambuf<_Elem,_Traits>( os ) )
        {
        }
        
        virtual ~basic_obase64stream()
        {
            superclass_t::flush();
            delete superclass_t::rdbuf();
        }
    };
    
    template <class _Elem, class _Traits = std::char_traits<_Elem>>
    class basic_ibase64stream : public std::basic_istream<_Elem,_Traits>
    {
    private:
        using superclass_t = std::basic_istream<_Elem,_Traits>;
    protected:
    public:
        basic_ibase64stream( std::istream& is ) :
        std::basic_istream<_Elem,_Traits>( new basic_base64streambuf<_Elem,_Traits>( is ) )
        {
        }
        
        virtual ~basic_ibase64stream()
        {
            delete superclass_t::rdbuf();
        }
    };
    
    using ibase64stream = basic_ibase64stream<char>;
    using obase64stream = basic_obase64stream<char>;
}	/* namespace ORANGEWERKS */

#endif /* ! defined( __h_base64stream__ ) */
//
//  JMupsAccessNFCWithTool.h
//  ppsdk
//
//  Created by tel on 2017/06/19.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JMupsAccessNFCWithTool__)
#define __h_JMupsAccessNFCWithTool__

#include "BaseSystem.h"
#include "JMupsAccessNFC.h"
#include <unordered_map>

class CardReader;

class JMupsAccessNFCWithMasterDataTool :
    virtual public JMupsAccessNFC
{
private:
    struct mdUpateFlag_t
    {
        enum class State {
            Bad,
            None,
            Update,
            Delete
        };
        
        enum class Where {
            Bad,
            Cakey,
            KernelCommon,
            Kernel1,
            Kernel2,
            Kernel3,
            Kernel4,
            Kernel5
        };
        
        std::unordered_map<Where,State> Flags;
        std::string                     UpdateDate;
        
        mdUpateFlag_t()
        {
            Flags[Where::Cakey]          = State::Bad;
            Flags[Where::KernelCommon]   = State::Bad;
            Flags[Where::Kernel1]        = State::Bad;
            Flags[Where::Kernel2]        = State::Bad;
            Flags[Where::Kernel3]        = State::Bad;
            Flags[Where::Kernel4]        = State::Bad;
            Flags[Where::Kernel5]        = State::Bad;
        }
        
        void setAll(State state)
        {
            std::for_each(Flags.begin(), Flags.end(), [=](auto& kv){
                kv.second = state;
            });
        }
        
        std::string whereToString(Where w) const
        {
            switch(w){
                case Where::Cakey:          return "cakeyFile";
                case Where::KernelCommon:   return "kernelCommonFile";
                case Where::Kernel1:        return "kernel1File";
                case Where::Kernel2:        return "kernel2File";
                case Where::Kernel3:        return "kernel3File";
                case Where::Kernel4:        return "kernel4File";
                case Where::Kernel5:        return "kernel5File";
                default: break;
            }
            LogManager_AddError("unknonw enum where");
            return "";
        }
        
        void loadFromTerminalInfo(const_json_t ti)
        {
            const std::string json_path_base = "sessionObject.nfcMasterDataUpdateInfo.";
            
            for(auto& kv : Flags){
                const auto update_path = json_path_base + whereToString(kv.first) + "UpdateFlg";
                const auto delete_path = json_path_base + whereToString(kv.first) + "DeleteFlg";
                const auto date_path   = json_path_base + whereToString(kv.first) + "UpdateDate";
                const auto update_flag = ti.get_optional<std::string>(update_path);
                const auto delete_flag = ti.get_optional<std::string>(delete_path);
                const auto date_text   = ti.get_optional<std::string>(date_path);
                if(update_flag){
                    if(*update_flag == "1"){
                        if(delete_flag && (*delete_flag == "1")){
                            kv.second = State::Delete;
                        }
                        else{
                            kv.second = State::Update;
                        }
                        /* 更新フラグが立っているものは、すべて開局時の日時が入ってくる */
                        if(UpdateDate.empty()){
                            if(date_text){
                                UpdateDate = *date_text;
                            }
                        }
                    }
                    else{
                        kv.second = State::None;
                    }
                }
                else{
                    kv.second = State::None;
                }
            }
        }
        
        bool isNeedUpdate() const
        {
            for(auto kv : Flags){
                if(kv.second != State::None) return true;
            }
            return false;
        }
        
        bool isUpdateAll() const
        {
            size_t count = 0;
            for(auto kv : Flags){
                if(kv.second != State::None) count++;
            }
            return count == Flags.size();
        }
    };
    
    mdUpateFlag_t   m_mdUpdateFlag;
    
protected:
    auto masterDataDownload(bool bForceDownload) -> pprx::observable<pprx::unit>;
    auto masterDataUpdateCheck(boost::shared_ptr<CardReader> cardReader) -> pprx::observable<const_json_t>;
    auto masterDataUpdate(boost::shared_ptr<CardReader> cardReader) -> pprx::observable<pprx::unit>;
    auto masterDataSendUpdateDate(const_json_t tradeStartInfo) -> pprx::observable<pprx::unit>;
    
public:
            JMupsAccessNFCWithMasterDataTool() = default;
    virtual ~JMupsAccessNFCWithMasterDataTool() = default;
};

#endif /* !defined(__h_JMupsNFCMasterDataTools__) */

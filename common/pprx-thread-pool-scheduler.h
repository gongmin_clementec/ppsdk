//
//  pprx-thread-pool-scheduler.h
//  ppsdk
//
//  Created by tel on 2017/11/15.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_pprx_thread_pool_scheduler__)
#define __h_pprx_thread_pool_scheduler__

#include <rxcpp/rx-scheduler.hpp>
#include <boost/asio.hpp>
#include "BaseSystem.h"


namespace pprx {
    using namespace rxcpp;
    using namespace rxcpp::schedulers;

    class thread_pool : public scheduler_interface
    {
    private:
        thread_pool(const thread_pool&) = delete;
        
        struct thread_pool_worker : public worker_interface
        {
        private:
            composite_subscription m_lifetime;

        public:
            explicit thread_pool_worker(composite_subscription cs)
            : m_lifetime(cs)
            {
            }
            
            virtual ~thread_pool_worker()
            {
                m_lifetime.unsubscribe();
            }
            
            virtual clock_type::time_point now() const override { return clock_type::now(); }
            
            virtual void schedule(const schedulable& scbl) const override
            {
                if (scbl.is_subscribed()) {
                    auto keep_alive = shared_from_this();
                    BaseSystem::instance().post([=](){
                        auto _ = keep_alive;
                        (void)_;
                        scbl(recursion(true).get_recurse());
                    });
                }
            }
            
            virtual void schedule(clock_type::time_point when, const schedulable& scbl) const override
            {
                if (scbl.is_subscribed()) {
                    printf("scheduled on %p with timeout\n", this);
                    auto keep_alive = shared_from_this();
                    auto timer = std::make_shared<boost::asio::basic_waitable_timer<clock_type>>
                    (BaseSystem::instance().ioService(), when);
                    timer->async_wait([=](const boost::system::error_code&) {
                        auto _ = keep_alive;
                        auto __ = timer;
                        (void)_;
                        (void)__;
                        scbl(recursion(true).get_recurse());
                    });
                }
            }
            
        };
        
    public:
        thread_pool(){}
        
        virtual ~thread_pool() { }
        
        virtual clock_type::time_point now() const { return clock_type::now(); }
        
        virtual worker create_worker(composite_subscription cs) const
        {
            return worker(cs, std::make_shared<thread_pool_worker>(cs));
        }
    };
    
    inline scheduler make_thread_pool()
    {
        return make_scheduler<thread_pool>();
    }

    inline observe_on_one_worker observe_on_thread_pool()
    {
        return observe_on_one_worker(make_thread_pool());
    }
    
    inline synchronize_in_one_worker synchronize_in_thread_pool()
    {
        return synchronize_in_one_worker(make_thread_pool());
    }
    
    inline identity_one_worker identitiy_thread_pool()
    {
        return identity_one_worker(make_thread_pool());
    }
    
    inline serialize_one_worker serialize_thread_pool()
    {
        return serialize_one_worker(make_thread_pool());
    }
}

#endif /* !defined(__h_pprx_thread_pool_scheduler__) */

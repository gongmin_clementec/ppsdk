//
//  SlipPrinterESCP.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_SlipPrinterESCP__)
#define __h_SlipPrinterESCP__

#include "SlipPrinter.h"

class SlipPrinterESCP :
    public SlipPrinter
{
private:
    struct zhchar_t
    {
        char32_t    c32;    /* 文字コード */
        int         length; /* キャラクタデバイス上の文字数 */
        int         nth;    /* この要素がlength内の何番目の要素か */
        zhchar_t() : c32(0), length(1), nth(0) {}
        zhchar_t(char32_t c, int l, int n) : c32(c), length(l), nth(n) {}
        void clear() { c32 = 0; length = 1, nth = 0; }
        
        /* 半角文字だとしても、倍角指定された場合、複数領域を使用することになるので */
        /* 半角か全角かで判定するのではなく、lengthで判定すること。 */
        bool isSingle() const { return length == 1; }   /* 半角１文字か */
        bool isMultiple() const { return length > 1; }  /* 倍角文字か */
        bool isFirst() const { return nth == 0; }
        bool isHalf() const { return nth > 0; }
        bool isLast() const { return nth == (length - 1); }
    };
    using   zhstr_t = std::vector<zhchar_t>;
    SlipDocument::Font  m_currentFont;
    
    zhstr_t utf8ToZhstr(const std::string& src) const;
    std::string zhstrToUtf8(const zhstr_t& src) const;

    void zhPutLeft(zhstr_t& ioBuffer, const SlipDocument::Td& src) const;
    void zhPutRight(zhstr_t& ioBuffer, const SlipDocument::Td& src) const;
    void zhPutCenter(zhstr_t& ioBuffer, const SlipDocument::Td& src) const;

    void zhPut(zhstr_t& ioBuffer, const zhstr_t& src, int bufferStartOffset) const;

    zhstr_t tdToZhstr(const SlipDocument::Td& src) const;
    
    void doTableWithBorder(const SlipDocument::Table& table);
    
    void setFont(SlipDocument::Font font);
    int bytesPerChar() const;   /* 倍角の指定の場合、半角1文字が、出力の何文字使用するか */

    zhstr_t doWrappedText(const SlipDocument::Td& src) const;
    bytes_t doLogoImage();
    
    
protected:
    virtual auto printPageSelf(SlipDocument::sp slip)  -> pprx::observable<pprx::unit>;

    virtual void writeString(const std::string& str) = 0;
    virtual void writeBytes(const bytes_t& bytes) = 0;
    
    virtual int  getRowChars() const = 0;
    virtual int  getRowPixels() const = 0;
    
public:
            SlipPrinterESCP(const __secret& s);
    virtual ~SlipPrinterESCP();
    
};


#endif /* !defined(__h_SlipPrinterESCP__) */

//
//  JobIdle.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobIdle__)
#define __h_JobIdle__

#include "PaymentJob.h"

class JobIdle :
    public PaymentJob
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobIdle(const __secret& s);
    virtual ~JobIdle();

    virtual bool isJobTimeoutRequired() const { return false; }
    virtual bool isCardReaderRequired() const { return false; }
};

#endif /* !defined(__h_JobIdle__) */

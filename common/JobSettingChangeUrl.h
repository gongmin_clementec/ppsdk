//
//  JobSettingChangeUrl.h
//  ppsdk
//
//  Created by tel on 2019/01/09.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__h_JobSettingChangeUrl__)
#define __h_JobSettingChangeUrl__

#include "PaymentJob.h"

class JobSettingChangeUrl :
    public PaymentJob
{
private:
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

public:
            JobSettingChangeUrl(const __secret& s);
    virtual ~JobSettingChangeUrl();

    virtual bool isCardReaderRequired() const { return false; };
};

#endif /* !defined(__h_JobSettingChangeUrl__) */

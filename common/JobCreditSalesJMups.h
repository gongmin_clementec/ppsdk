//
//  JobCreditSalesJMups.h
//  ppsdk
//
//  Created by tel on 2017/06/02.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_JobCreditSalesJMups__)
#define __h_JobCreditSalesJMups__

#include "PaymentJob.h"
#include "JMupsAccessCredit.h"
#include "JMupsAccessCoupon.h"
#include "JMupsEmvKernel.h"
#include "JMupsSlipPrinter.h"
#include "JobNFCSalesJMups.h"
#include "PaymentJobCreator.h"

class JobCreditSalesJMups :
    public PaymentJob,
    public JMupsAccessCredit,
    public JMupsAccessCoupon,
    public JMupsEmvKernel,
    public JMupsSlipPrinter
{
protected:
    class TradeInfoBuilderJMupsCredit : public TradeInfoBuilder
    {
    public:
        virtual ~TradeInfoBuilderJMupsCredit() = default;
        void addPaymentWay(const_json_t tradeData, const std::vector<const std::string> properties, const_json_t hps)
        {
            json_t current;
            auto paymentWay = tradeData.get_optional<std::string>("paymentWay");
            if(paymentWay) current.put("paymentWay", *paymentWay);
            else current.put("paymentWay", nullptr);
            
            for(auto prop : properties){
                auto value = tradeData.get_optional<int>(prop);
                if(value){
                    current.put(prop, *value);
                }
            }
            
            addInternal<json_t>("paymentWay", attribute::required, current, hps);
        }

        void addSearchResultDiv(attribute attr, boost::optional<std::string> value)
        {
            addInternal<std::string>("searchResultDiv", attr, value);
        }
        
        void addPaymentWayForRefund(attribute attr, const_json_t refundSearchResult, const_json_t hps)
        {
            json_t current;
            if(!refundSearchResult.is_null_or_empty()){
                auto&& payWayDivCancel = refundSearchResult.get_optional<std::string>("normalObject.payWayDivCancel");
                if(payWayDivCancel){
                    auto paymentWay = boost::make_shared<std::string>();
                    auto payWayDiv = ::atoi((*payWayDivCancel).c_str());
                    switch(payWayDiv){
                        case 0:
                            *paymentWay = "lump";
                            break;
                        case 1:
                            *paymentWay = "installment";
                            break;
                        case 2:
                            *paymentWay = "bonus";
                            break;
                        case 3:
                            *paymentWay = "bonusCombine";
                            break;
                        case 4:
                            *paymentWay = "revolving";
                            break;
                        default :
                            *paymentWay = nullptr;
                    }
                    
                    if(paymentWay)
                        current.put("paymentWay", *paymentWay);
                    else
                        current.put("paymentWay", nullptr);
                }
                
                auto partitionTimesCancel   = refundSearchResult.get_optional<std::string>("normalObject.partitionTimesCancel");
                auto firsttimeClaimMonth    = refundSearchResult.get_optional<std::string>("normalObject.firsttimeClaimMonth");
                auto bonusTimes             = refundSearchResult.get_optional<std::string>("normalObject.bonusTimes"  );
                auto bonusMonth1            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth1" );
                auto bonusAmount1           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount1");
                auto bonusMonth2            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth2" );
                auto bonusAmount2           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount2");                
                auto bonusMonth3            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth3" );
                auto bonusAmount3           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount3");
                auto bonusMonth4            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth4" );
                auto bonusAmount4           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount4");
                auto bonusMonth5            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth5" );
                auto bonusAmount5           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount5");
                auto bonusMonth6            = refundSearchResult.get_optional<std::string>("normalObject.bonusMonth6" );
                auto bonusAmount6           = refundSearchResult.get_optional<std::string>("normalObject.bonusAmount6");
                
                if(partitionTimesCancel)   current.put("partitionTimes"     , *partitionTimesCancel );
                if(firsttimeClaimMonth)    current.put("firsttimeClaimMonth", *firsttimeClaimMonth );
                if(bonusTimes)             current.put("bonusTimes"  , *bonusTimes   );
                if(bonusMonth1)            current.put("bonusMonth1" , *bonusMonth1  );
                if(bonusAmount1)           current.put("bonusAmount1", *bonusAmount1 );
                if(bonusMonth2)            current.put("bonusMonth2" , *bonusMonth2  );
                if(bonusAmount2)           current.put("bonusAmount2", *bonusAmount2 );
                if(bonusMonth3)            current.put("bonusMonth3" , *bonusMonth3  );
                if(bonusAmount3)           current.put("bonusAmount3", *bonusAmount3 );
                if(bonusMonth4)            current.put("bonusMonth4" , *bonusMonth4  );
                if(bonusAmount4)           current.put("bonusAmount4", *bonusAmount4 );
                if(bonusMonth5)            current.put("bonusMonth5" , *bonusMonth5  );
                if(bonusAmount5)           current.put("bonusAmount5", *bonusAmount5 );
                if(bonusMonth6)            current.put("bonusMonth6" , *bonusMonth6  );
                if(bonusAmount6)           current.put("bonusAmount6", *bonusAmount6 );
                
                addInternal<json_t>("paymentWay", attr, current, hps);
            }
            else{
                addInternal<json_t>("paymentWay", attr, boost::none, hps);
            }
        }
        
        void addCardInfo(const_json_t hps)
        {
            addInternal<json_t>("CardInfo", attribute::optional, boost::none, hps);
        }
    };
    
    json_t m_tradeStartResponse;
    json_t m_tradeStartResponseHeader;
    json_t m_cardStartData;
    json_t m_cardInfoData;
    json_t m_tradeData;
    json_t m_couponData;
    json_t m_tradeConfirmFinished; // 取引確認処理終了《通知》json
    prmCredit::doubleTrade    m_taggingSelector;
    boost::shared_ptr<PaymentJob>   m_childJob;
    
    bool   m_bIsMSFallback;
    
    enum class CardType
    {
        Bad,
        IC,
        MS,
        NFC
    };
    
    CardType m_cardType;
    
    enum class UIBehaviorState
    {
        Bad,
        InProgress,
        Finished,
        Cancelled
    };
    static const std::vector<const std::string> s_paymentWayProperties;
    pprx::subjects::behavior<UIBehaviorState> m_emvAmountPreInputUI;

protected:
    auto syncEmvAmountPreInputUI() -> pprx::observable<pprx::unit>;
    auto tradeStart() -> pprx::observable<pprx::unit>;
    auto processCardJob(const_json_t cardReaderOption, bool bThreeSides = false ) -> pprx::observable<pprx::unit>;
    auto processCardByCard(const_json_t data) -> pprx::observable<pprx::unit>;
    auto waitForKidInput() -> pprx::observable<JMupsAccess::response_t>;
    auto getCouponData() -> pprx::observable<const_json_t>;
    auto processSendDigiSign() -> pprx::observable<pprx::unit>;
    auto waitForDigiSign() -> pprx::observable<boost::optional<std::string>>;
    auto sendDigiSign(JMupsAccess::prmCommon::digiReceiptDiv digiReceiptDiv, const std::string& digiSign)  -> pprx::observable<pprx::unit>;
    auto isNeedAquirerSlip(bool bNeedAquirer = true) -> pprx::observable<pprx::unit>;
    virtual auto isUsedChildJob() -> bool const;
    virtual auto createChildJobParam() -> const_json_t;

    virtual auto processMagneticStripe(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto proceedContact(const_json_t data)  -> pprx::observable<pprx::unit>;
    virtual auto processNfc(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual void beginAsyncPreInputAmount();
    virtual auto buildDispatchRequestParamForTradeInfo() -> pprx::observable<TradeInfoBuilder>;
    virtual auto applyDispatchResponseParamForTradeInfo(const_json_t resp) -> pprx::observable<bool>;
    virtual auto waitForTradeInfo(const std::string& tradeParam = "") -> pprx::observable<pprx::unit>;
    virtual auto processDcc() -> pprx::observable<pprx::unit>;
    virtual auto sendTradeConfirm(prmCredit::doubleTrade selector)  -> pprx::observable<JMupsAccess::response_t>;
    virtual auto waitForConfirmDoubleTrade(JMupsAccess::response_t jmupsData) -> pprx::observable<prmCredit::doubleTrade>;
    virtual auto sendTagging(prmCredit::doubleTrade selector) -> pprx::observable<JMupsAccess::response_t>;
    virtual auto treatSlipDocumentJson() -> pprx::observable<const_json_t>;
    virtual prmCommon::operationDiv getOperationDiv() const;
    virtual auto beginAsyncRemovingContact() -> void;
    virtual auto processRemovingContact(bool bTradeIsDecline = false) -> pprx::observable<pprx::unit>;
    virtual auto slipPrinterDispatch(const std::string& command, const_json_t subParam)  -> pprx::observable<bool>;
    virtual auto slipPrinterMessage(const std::string& message, const_json_t subParam) -> pprx::observable<pprx::unit>;
    virtual auto addMediaDivToCardNo(const_json_t tradeData) -> json_t;
    virtual auto checkTradeFailureErrorCode(const_json_t tradeData, const std::string errorCode) -> bool;
    virtual auto createChildJob(const std::string JobName) -> pprx::observable<pprx::unit>;
    
protected:
    virtual auto getObservableSelf() -> pprx::observable<const_json_t>;
    virtual auto finalizeSelf()  -> pprx::observable<pprx::unit>;

    virtual auto emvOnCardPinMessage(const_json_t param)  -> pprx::observable<pprx::unit>;
    virtual auto emvOnCardInputPlainOfflinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardInputEncryptedOfflinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardInputEncryptedOnlinePin(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardTxPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardTxEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto emvOnCardGetIfdSerialNo() -> pprx::observable<const_json_t>;
    virtual auto emvOnNetwork(const std::string& url, const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectApplication(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiSelectDeal() -> pprx::observable<const_json_t>;
    virtual auto emvWaitInputPreAmount() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiAmount() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiInputKid() -> pprx::observable<const_json_t>;
    virtual auto emvOnUiInputPayWay(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnUiAmountConfirm(const_json_t emvSession) -> pprx::observable<const_json_t>;
    virtual auto emvOnTradeConfirmFinish(const_json_t emvTradeConfirm)  -> pprx::observable<pprx::unit>;
    virtual auto emvOnTrainingCardReaderDisplayData() -> pprx::observable<const_json_t>;
    virtual auto emvOnCardPinResultConfirm(const_json_t param)  -> pprx::observable<pprx::unit>;
    
public:
            JobCreditSalesJMups(const __secret& s);
    virtual ~JobCreditSalesJMups();

    virtual bool isCardReaderRequired() const { return true; };
};



#endif /* !defined(__h_JobCreditSalesJMups__) */

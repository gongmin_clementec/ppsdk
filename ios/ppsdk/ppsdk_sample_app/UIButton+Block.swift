//
//  UIButton+Block.swift
//  ppsdk
//
//  Created by tel on 2017/06/19.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

import Foundation
import ObjectiveC

private var ButtonActionBlockKey: UInt8 = 0

public typealias ButtonActionBlock_t = (_ sender: UIButton) -> Void

private class ButtonActionBlockWrapper : NSObject {
    var block : ButtonActionBlock_t
    init(block: @escaping ButtonActionBlock_t) {
        self.block = block
    }
}

extension UIButton {
    public func onTouchUpInside(_ block: @escaping ButtonActionBlock_t) {
        objc_setAssociatedObject(
            self,
            &ButtonActionBlockKey,
            ButtonActionBlockWrapper(block: block),
            objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        addTarget(
            self,
            action: #selector(UIButton.blockHandleAction(_:)),
            for: .touchUpInside)
    }
    
    @objc fileprivate func blockHandleAction(_ sender: UIButton) {
        let wrapper = objc_getAssociatedObject(self, &ButtonActionBlockKey) as! ButtonActionBlockWrapper
        wrapper.block(sender)
    }
}

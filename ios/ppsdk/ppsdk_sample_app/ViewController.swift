//
//  ViewController.swift
//  ppsdk_sample_app
//
//  Created by clementec on 2017/05/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var ppsdk: PPBridge? = nil
    var pphttpd: PPHttpServerBridge? = nil
    
    @IBOutlet weak var textLog: UITextView!
    @IBOutlet weak var btJobOpenTerminal: UIButton!

    @IBOutlet weak var btJobCreditSales: UIButton!
    @IBOutlet weak var btJobCreditRefund: UIButton!
    @IBOutlet weak var btJobCreditPreApproval: UIButton!
    @IBOutlet weak var btJobCreditAfterApproval: UIButton!
    @IBOutlet weak var btJobCreditCardCheck: UIButton!

    @IBOutlet weak var btJobUnionPaySales: UIButton!
    @IBOutlet weak var btJobUnionPayRefund: UIButton!
    @IBOutlet weak var btJobUnionPayPreApproval: UIButton!
    @IBOutlet weak var btJobUnionPayAfterApproval: UIButton!
    @IBOutlet weak var btJobUnionPayPreApprovalCancel: UIButton!
    @IBOutlet weak var btJobUnionPayAfterApprovalRefund: UIButton!
    
    @IBOutlet weak var btJobNfcSales: UIButton!
    @IBOutlet weak var btJobNfcRefund: UIButton!
    @IBOutlet weak var btJobNfcUpdateMasterData: UIButton!
    @IBOutlet weak var btJobNfcCheckMasterData: UIButton!
    
    @IBOutlet weak var btJobJournal: UIButton!
    @IBOutlet weak var btJobReprintSlip: UIButton!

    @IBOutlet weak var btCancel: UIButton!
    
    @IBOutlet weak var btFWUpdate: UIButton!
    @IBOutlet weak var btActivation: UIButton!
    @IBOutlet weak var btIssuers: UIButton!
    @IBOutlet weak var btService: UIButton!
    @IBOutlet weak var btTraining: UIButton!
    @IBOutlet weak var btTerminalInfo: UIButton!
    @IBOutlet weak var btChangeUrl: UIButton!
    @IBOutlet weak var btSendLog: UIButton!
    @IBOutlet weak var bCheckTrainingMode: UIButton!
    
    @IBOutlet weak var textAmount: UITextField!
    @IBOutlet weak var textTaxOtherAmount: UITextField!
    @IBOutlet weak var textKid: UITextField!
    @IBOutlet weak var textSlipNo: UITextField!
    @IBOutlet weak var textApprovalNo: UITextField!
    @IBOutlet weak var btInputDone: UIButton!
    @IBOutlet weak var btInputRetry: UIButton!
    @IBOutlet weak var pickCoupon: UIPickerView!
    @IBOutlet weak var textOtherTermJudgeNo: UITextField!
    @IBOutlet weak var textCupSendDate: UITextField!
    @IBOutlet weak var textCupNo: UITextField!
    @IBOutlet weak var textCupTradeDiv: UITextField!
    
    @IBOutlet weak var textProductCode: UITextField!
    @IBOutlet weak var textPartitionTimes: UITextField!
    @IBOutlet weak var textFirsttimeClaimMonth: UITextField!
    @IBOutlet weak var textBonusTimes: UITextField!
    @IBOutlet weak var textBonusMonth1: UITextField!
    @IBOutlet weak var textBonusMonth2: UITextField!
    @IBOutlet weak var textBonusMonth3: UITextField!
    @IBOutlet weak var textBonusMonth4: UITextField!
    @IBOutlet weak var textBonusMonth5: UITextField!
    @IBOutlet weak var textBonusMonth6: UITextField!
    @IBOutlet weak var textBonusAmount1: UITextField!
    @IBOutlet weak var textBonusAmount2: UITextField!
    @IBOutlet weak var textBonusAmount3: UITextField!
    @IBOutlet weak var textBonusAmount4: UITextField!
    @IBOutlet weak var textBonusAmount5: UITextField!
    @IBOutlet weak var textBonusAmount6: UITextField!
    
    @IBOutlet weak var scPaymentWay: UISegmentedControl!
    
    @IBOutlet weak var scJournalDetail: UISegmentedControl!
    @IBOutlet weak var scJournalIntermediate: UISegmentedControl!
    @IBOutlet weak var scJournalReprint: UISegmentedControl!
    @IBOutlet weak var scJournalPaymentType: UISegmentedControl!
    @IBOutlet weak var scReprintPaymentType: UISegmentedControl!
    
    @IBOutlet weak var swTrainingMode: UISwitch!
    @IBOutlet weak var swTrainingDigiSign: UISwitch!
    
    var pickListTitles: Array<String> = []
    var pickListValues: Array<String?> = []
    var pickCurrentSelected: String? = nil
    var paymentWayCtrls: [String:UITextField] = [:]
    var transactionLog: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appCameToForeground), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)

        // HTTPサーバ起動
        //pphttpd = PPHttpServerBridge()
        //pphttpd?.begin(withDocumentRoot: Bundle.main.bundlePath + "/web/html", portNumber: 50555, isUseLocalhost: false)
        // Do any additional setup after loading the view, typically from a nib.
        ppsdk = PPBridge.sharedInstance()
        
        #if HONBAN
        var desc        = "本番"
        let jmupsUrl_1  = "https://hps01.j-mups.jp:10442/hps_trm/"
        let jmupsUrl_2  = "https://hps01.j-mups.jp:10443/hps_trm/"
        let tmsUrl      = "https://tms.clementec.jp/"
        #elseif JUNHON
        var desc        = "準本番"
        let jmupsUrl_1  = "https://hps01.j-mups.jp.net:10442/hps_trm/"
        let jmupsUrl_2  = "https://hps01.j-mups.jp.net:10443/hps_trm/"
        let tmsUrl      = "https://tmsweb-development2.azurewebsites.net"
        #elseif TEST1
        var desc        = "テスト１"
        let jmupsUrl_1  = "https://hps30.j-mups.jp.net:10442/hps_trm/"
        let jmupsUrl_2  = "https://hps30.j-mups.jp.net:10443/hps_trm/"
        let tmsUrl      = "https://tmsweb-development2.azurewebsites.net"
        #elseif TEST2
        var desc        = "テスト２"
        let jmupsUrl_1  = "https://hps40.j-mups.jp.net:10442/hps_trm/"
        let jmupsUrl_2  = "https://hps40.j-mups.jp.net:10443/hps_trm/"
        let tmsUrl      = "https://tmsweb-development2.azurewebsites.net"
        #else
        var desc        = "テスト２（未設定による）"
        let jmupsUrl_1  = "https://hps40.j-mups.jp.net:10442/hps_trm/"
        let jmupsUrl_2  = "https://hps40.j-mups.jp.net:10443/hps_trm/"
        let tmsUrl      = "https://tmsweb-development2.azurewebsites.net"
        #endif
        
        #if RECRUIT
        desc = desc + " for Recruit"
        let miura_ea_protocol   = "((com\\.miura\\.shuttle)|(jp\\.co\\.recruit\\.Air\\.Payment\\.Universal\\.1))"
        let miura_ea_name       = "((Recruit Card Reader.*)|(Miura.*)|(Air PAYMENT.*))"
        let miura_ea_serial     = ".*711$"
        #else
        desc = desc + " for Clementec"
        let miura_ea_protocol   = "((com\\.miura\\.shuttle)|(jp\\.co\\.recruit\\.Air\\.Payment\\.Universal\\.1))|(com\\.clementec\\.payment\\.JETSmart)"
        //let miura_ea_protocol   = "com\\.clementec\\.payment\\.JETSmart"
        let miura_ea_name       = "((Recruit Card Reader.*)|(Miura.*)|(Air PAYMENT.*)|(JET-Smart.*))"
        let miura_ea_serial     = ".*"
        #endif
        
        let config = [
            "debug":[
                 "bActivationSkipError": true
                //,"bResetTMSPasswords" : true
            ],
            "miura":[
                "ea":[
                    "protocol": miura_ea_protocol,
                    "name": miura_ea_name,
                    "serial": miura_ea_serial
                ]
            ],
            "hps": [
                "keychainStoreLabel": "JMupsIdentity",
                "urlForActivation": jmupsUrl_1,
                "urlForTrading": jmupsUrl_2,
                "bSupportCupIc": true
            ],
            "tms": [
                "url": tmsUrl
            ]
        ]
        
        do{
            let jdata = try JSONSerialization.data(withJSONObject: config, options: [])
            let jstr = String(data:jdata, encoding:String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            ppsdk?.updateConfigure(desc, json: jstr)
        }
        catch{
        
        }
        
        btInputDone.isEnabled = false
        textAmount.isEnabled = false
        textAmount.keyboardType = .numberPad
        textTaxOtherAmount.isEnabled = false
        textTaxOtherAmount.keyboardType = .numberPad
        textProductCode.isEnabled = false
        textKid.isEnabled = false
        textKid.keyboardType = .numberPad
        textSlipNo.isEnabled = false
        textSlipNo.keyboardType = .numberPad
        textApprovalNo.isEnabled = false
        textApprovalNo.keyboardType = .numberPad
        textOtherTermJudgeNo.isEnabled = false
        textCupSendDate.isEnabled = false
        textCupSendDate.keyboardType = .numberPad
        textCupNo.isEnabled = false
        textCupNo.keyboardType = .numberPad
        textCupTradeDiv.isEnabled = false
        textCupTradeDiv.keyboardType = .numberPad
        pickCoupon.isUserInteractionEnabled = false
        pickCoupon.alpha = 0.5
        btInputRetry.isEnabled = false
        btCancel.isEnabled = false
        swTrainingMode.isOn = false
        swTrainingDigiSign.isOn = false
        
        scPaymentWay.isEnabled = false
        
        scJournalIntermediate.isEnabled = false
        scJournalReprint.isEnabled = false
        scJournalPaymentType.isEnabled = false
        scJournalDetail.isEnabled = false

        scReprintPaymentType.isEnabled = false

        textPartitionTimes.isEnabled = false
        textFirsttimeClaimMonth.isEnabled = false
        
        paymentWayCtrls = [
            "partitionTimes" : textPartitionTimes,
            "firsttimeClaimMonth": textFirsttimeClaimMonth,
            "bonusTimes" : textBonusTimes,
            "bonusMonth1": textBonusMonth1,
            "bonusMonth2": textBonusMonth2,
            "bonusMonth3": textBonusMonth3,
            "bonusMonth4": textBonusMonth4,
            "bonusMonth5": textBonusMonth5,
            "bonusMonth6": textBonusMonth6,
            "bonusAmount1" : textBonusAmount1,
            "bonusAmount2" : textBonusAmount2,
            "bonusAmount3" : textBonusAmount3,
            "bonusAmount4" : textBonusAmount4,
            "bonusAmount5" : textBonusAmount5,
            "bonusAmount6" : textBonusAmount6
        ]
        paymentWayCtrls.forEach { (_, ctrl) in
            ctrl.isEnabled = false
            ctrl.keyboardType = .numberPad
        }
        
        loadMasterData()
    }
    
    func loadMasterData()
    {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask ).first {
            let path_file_name = dir.appendingPathComponent("MasterData.json")
            do {
                let text = try String(contentsOf: path_file_name, encoding: .utf8)
                ppsdk?.setJMupsNfcMasterData(text)
            }
            catch {
            }
        }
    }

    func saveMasterData()
    {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask ).first {
            let path_file_name = dir.appendingPathComponent("MasterData.json")
            do {
                let text = ppsdk?.getJMupsNfcMasterData()
                try text?.write(to: path_file_name, atomically: false, encoding: .utf8)
            }
            catch {
            }
        }
    }

    func setEnableJobButtons(_ bEnable:Bool){
        btJobOpenTerminal.isEnabled = bEnable
        btJobCreditSales.isEnabled = bEnable
        btJobCreditRefund.isEnabled = bEnable
        btJobCreditPreApproval.isEnabled = bEnable
        btJobCreditAfterApproval.isEnabled = bEnable
        btJobCreditCardCheck.isEnabled = bEnable
        btJobUnionPaySales.isEnabled = bEnable
        btJobUnionPayRefund.isEnabled = bEnable
        btJobUnionPayPreApproval.isEnabled = bEnable
        btJobUnionPayAfterApproval.isEnabled = bEnable
        btJobUnionPayPreApprovalCancel.isEnabled = bEnable
        btJobUnionPayAfterApprovalRefund.isEnabled = bEnable
        btJobNfcSales.isEnabled = bEnable
        btJobNfcRefund.isEnabled = bEnable
        btJobJournal.isEnabled = bEnable
        btFWUpdate.isEnabled = bEnable
        btActivation.isEnabled = bEnable
        btIssuers.isEnabled = bEnable
        btService.isEnabled = bEnable
        btTraining.isEnabled = bEnable
        btTerminalInfo.isEnabled = bEnable
        btChangeUrl.isEnabled = bEnable
        btSendLog.isEnabled = bEnable
        btJobReprintSlip.isEnabled = bEnable
        btJobNfcUpdateMasterData.isEnabled = bEnable
        btJobNfcCheckMasterData.isEnabled = bEnable
        swTrainingMode.isEnabled = bEnable
        swTrainingDigiSign.isEnabled = bEnable
        btCancel.isEnabled = !bEnable
        bCheckTrainingMode.isEnabled = bEnable
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func appMovedToBackground() {
        addTextLog("app enters background")
    }

    @objc func appCameToForeground() {
        addTextLog("app enters foreground")
    }
    
    @IBAction func onTrainingMode(_ sender: Any) {
        ppsdk?.setTrainingMode(swTrainingMode.isOn)
    }

    @IBAction func onTrainingDigiSign(_ sender: Any) {
        ppsdk?.setUseDigiSignInTrainingMode(swTrainingDigiSign.isOn)
    }
    
    @IBAction func onOpenTerminal(_ sender: AnyObject) {
        #if false
            /* 画像から電子サインの情報生成のデバッグコード */
            let image = UIImage(named: "Drawing.png")
            let sign = self.ppsdk?.hexString(fromDigiSign: image?.cgImage, modulus: "00be363ecea583cabf71a669c06ca56fa3ecea40464f40292df0ca231bb4cf5797de0078c8d47c0ae4469836cd3ec9800b908327d1087df8368304e84258d7ba5c010a2319c7b5fe42cdf1e25cc81792441bd0d7f9395294fa6c72874486110e2bccf729ac89e12c32b77a3e2ff6ccb5b951af2fe55b23b7913908fb3a86ade0ff3ccd821c1516048f6a7e9ae8898441f522c279ed6fb229c0986c194017e9479fedc20ad09a42804bd50675e616c2337bdf1552458f4d6ec5537da32600559e497711de1fc6e01fc466588a004f9ef9871a95549215a1dd92d029143ce0b9548dd060a24b713e6651217104c5d3baa35fed770b081a5b890d28098a1156f224e1", exponent: "010001")
            NSLog(sign!);
        #endif
        
        onJobCommon("open terminal", jobName:"JMups.OpenTerminal", optText:"onJournal")
    }
    
    @IBAction func onJournal(_ sender: Any) {
        onJobCommon("journal", jobName:"JMups.Journal", optText:"onJournal")
    }
    
    func onJobCommon(_ logtext: String, jobName: String, optText: String?){
        resetDispatchSyncObjects()
        setEnableJobButtons(false)
        textLog.text = logtext
        self.transactionLog = "[\n"
        ppsdk?.executeJob(
            jobName,
            onDispatch: { (inParam, outParam) in
                self.transactionLog.append("{\"type\":\"onDispatch\",\"data\":" + inParam! + "},\n")
                self.transactionLog.append("{\"type\":\"-wait\",\"data\":500},\n")
                return self.ppsdk_onDispatch(inParam!, outParam:outParam!, opt:optText)
            },
            onDispatchCancel: { () in
                self.transactionLog.append("{\"type\":\"onDispatchCancel\"}")
                self.addTextLog("\n<< onDispatchCancel >>")
                DispatchQueue.main.sync(execute: {
                    if(self.bInDispatch){
                        self.ppsdk_dispatch_bContinue = false
                        self.ppsdk_dispatch_result = NSMutableDictionary()
                        self.ppsdk_dispatch_sem.signal()
                        self.btInputDone.sendActions(for: UIControlEvents.touchUpInside)
                    }
                })
            },
            onFinish:
            { (result) in
                self.transactionLog.append("{\"type\":\"onFinish\",\"data\":" + result! + "}\n")
                self.transactionLog.append("]")
                let dfmt = DateFormatter()
                dfmt.locale = NSLocale(localeIdentifier: "en_US") as Locale
                dfmt.dateFormat = "yyyyMMddHHmmss"
                dfmt.string(from: Date())
                if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask ).first {
                    do {
                        let basedir = dir.appendingPathComponent("responses")
                        try FileManager.default.createDirectory(at: basedir, withIntermediateDirectories: true)
                        let fpath = basedir.appendingPathComponent(dfmt.string(from: Date()) + "." + jobName + ".json")
                        try self.transactionLog.write(to: fpath, atomically: false, encoding: .utf8)
                    }
                    catch {
                    }
                }
                self.ppsdk_onFinish(result!)
                if(jobName == "JMups.OpenTerminal"){
                    do{
                        guard let data = result!.data(using: String.Encoding.utf8) else{
                            return
                        }
                        guard let json = try JSONSerialization.jsonObject(with: data) as? NSDictionary else{
                            return
                        }
                        guard let result = json["result"] as? NSDictionary else{
                            return
                        }
                        guard let hps = result["hps"] as? NSDictionary else{
                            return
                        }
                        let rdata = try JSONSerialization.data(withJSONObject: hps, options: [])
                        let sdata = NSString(data:rdata, encoding:String.Encoding.utf8.rawValue)
                        let dic: Dictionary = ["response": sdata]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ppNotification:updateJMupsOpenTerminalResponse"), object: self, userInfo: dic as [AnyHashable : Any])
                    }
                    catch{
                        
                    }

                }
            }
        )
    }
    
    @IBAction func onReprint(_ sender: Any) {
        onJobCommon("reprint", jobName:"JMups.ReprintSlip", optText:"onReprint")
    }
    
    @IBAction func onCreditSales(_ sender: AnyObject) {
        onJobCommon("credit sales", jobName:"JMups.CreditSales", optText: nil)
    }
    
    @IBAction func onCreditRefund(_ sender: Any) {
        onJobCommon("credit refund", jobName:"JMups.CreditRefund", optText: nil)
    }

    @IBAction func onCreditPreApproval(_ sender: Any) {
        onJobCommon("credit pre-approval", jobName:"JMups.CreditPreApproval", optText: nil)
    }

    @IBAction func onCreditAfterApproval(_ sender: Any) {
        onJobCommon("credit after approval", jobName:"JMups.CreditAfterApproval", optText: nil)
    }

    @IBAction func onCreditCardCheck(_ sender: Any) {
        onJobCommon("credit card check", jobName:"JMups.CreditCardCheck", optText: nil)
    }

    @IBAction func onUnionPaySales(_ sender: Any) {
        onJobCommon("union pay sales", jobName:"JMups.UnionPaySales", optText: nil)
    }

    @IBAction func onUnionPayRefund(_ sender: Any) {
        onJobCommon("Union pay refund", jobName:"JMups.UnionPayRefund", optText: nil)
    }
    
    @IBAction func onUnionPayPreApproval(_ sender: Any) {
        onJobCommon("Union pay preApproval", jobName:"JMups.UnionPayPreApproval", optText: nil)
    }
   
    @IBAction func onUnionPayAfterApproval(_ sender: Any) {
        onJobCommon("Union pay after approval", jobName:"JMups.UnionPayAfterApproval", optText: nil)
    }
    
    @IBAction func onUnionPayPreApprovalCancel(_ sender: Any) {
        onJobCommon("Union pay preApproval Cancel", jobName:"JMups.UnionPayPreApprovalCancel", optText: nil)
    }
    
    @IBAction func onUnionPayAfterApprovalRefund(_ sender: Any) {
        onJobCommon("Union pay after approval Refund", jobName:"JMups.UnionPayAfterApprovalRefund", optText: nil)
    }
    
    @IBAction func onNFCSales(_ sender: AnyObject) {
//        onJobCommon("nfc sales", jobName:"JMups.NFCSales", optText: nil)
        // 三面待ちなので、NFCの売上もクレジットの売上クラスを使用
        onJobCommon("credit sales", jobName:"JMups.CreditSales", optText: nil)
    }
    
    @IBAction func onNFCRefund(_ sender: AnyObject) {
        onJobCommon("nfc refund", jobName:"JMups.NFCRefund", optText: nil)
    }

    @IBAction func onNFCUpdateMasterData(_ sender: Any) {
        onJobCommon("nfc update master data", jobName:"JMups.NFCUpdateMasterData", optText: nil)
    }

    @IBAction func onNFCCheckMasterData(_ sender: Any) {
        onJobCommon("nfc check master data", jobName:"JMups.NFCCheckMasterData", optText: nil)
//        ppsdk?.checkMasterData()
    }

    @IBAction func onFWUpdate(_ sender: Any) {
        onJobCommon("Miura firmware update", jobName:"Setting.ReaderFWUpdate", optText: nil)
    }
        
    @IBAction func onActivation(_ sender: Any) {
        onJobCommon("Activation", jobName:"JMups.Activation", optText: nil)
    }
    
    @IBAction func onIssuers(_ sender: Any) {
        onJobCommon("Issuers", jobName:"Setting.Issuers", optText: nil)
    }
    
    @IBAction func onService(_ sender: Any) {
        onJobCommon("Service", jobName:"Setting.Service", optText: nil)
    }
    
    @IBAction func onTraining(_ sender: Any) {
        onJobCommon("Training", jobName:"Setting.Training", optText: nil)
    }
    
    @IBAction func checkIsTrainingMode(_ sender: Any) {
        let bTrainingMode = ppsdk?.isTrainingMode() as! Bool
        var msg = "";

        if bTrainingMode {
            msg = "トレーニングモードです";
        }
        else{
            msg = "通常モードです";
        }
        
        showAlert(msg, completion: {
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
        });
    }
    
    @IBAction func clearCardReaderDisplay(_ sender: Any) {
        ppsdk?.clearCardReaderDisplay()
    }
    
    @IBAction func onDisconnectCardReader(_ sender: Any) {
        ppsdk?.disconnectCardReader()
    }
    
    @IBAction func onTerminalInfo(_ sender: Any) {
        onJobCommon("Terminal information", jobName:"Setting.TerminalInfo", optText: nil)
    }

    @IBAction func onChangeUrl(_ sender: Any) {
        onJobCommon("Change url", jobName:"Setting.ChangeUrl", optText: nil)
    }

    @IBAction func onSendLog(_ sender: Any) {
        onJobCommon("Send Log", jobName:"Setting.SendLog", optText: nil)
    }

    @IBAction func onCancel(_ sender: AnyObject) {
        if(bInDispatch){
            ppsdk_dispatch_bContinue = false
            btInputDone.sendActions(for: UIControlEvents.touchUpInside)
        }
        else{
            ppsdk?.abort()
        }
    }

    func addTextLog(_ text:String)
    {
        DispatchQueue.main.async(execute: {
            self.textLog.text = self.textLog.text + "\n" + text
        })
    }
    
    var bInDispatch: Bool = false
    var ppsdk_dispatch_sem : DispatchSemaphore = DispatchSemaphore(value: 1)
    var ppsdk_dispatch_result : NSMutableDictionary = NSMutableDictionary()
    var ppsdk_dispatch_bContinue : Bool = false
    
    func resetDispatchSyncObjects()
    {
        bInDispatch = false
        ppsdk_dispatch_sem = DispatchSemaphore(value: 1)
        ppsdk_dispatch_result = NSMutableDictionary()
        ppsdk_dispatch_bContinue = false
    }
    
    func ppsdk_onPost(_ inParam:String)
    {
        addTextLog("\n<<onPost >>")
        addTextLog("inParam : " + formatJson(inParam))
    }
    
    func ppsdk_onDispatch(_ inParam:String, outParam:AutoreleasingUnsafeMutablePointer<NSString?>, opt:String? = nil) -> Bool
    {
        bInDispatch = true
        defer { /* return statement直前で呼び出される（厳密なスコープアウト時ではないので注意） */
            bInDispatch = false
        }
        addTextLog("\n<< onDispatch >>")
        addTextLog("inParam : " + formatJson(inParam))
        ppsdk_dispatch_result.removeAllObjects()
        do{
            guard let data = inParam.data(using: String.Encoding.utf8) else{
                return false
            }
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary else{
                return false
            }
            
            /* <dispatchSucceeded> は直近のdispatchがppsdkに戻り、dispatch処理が正常に終わった場合に呼び出される特殊command */
            let ppsdk_command = json["command"] as! String
            if (ppsdk_command == "<DispatchSucceeded>"){
                return true
            }
            if (ppsdk_command == "<DispatchRetry>"){
                return true
            }
            
            _ = self.ppsdk_dispatch_sem.wait(timeout: DispatchTime.distantFuture)
            self.ppsdk_dispatch_bContinue = true
            
            DispatchQueue.main.async(execute: {
                self.onDispatchInMainThread(json, opt:opt)
            })
            
            _ = self.ppsdk_dispatch_sem.wait(timeout: DispatchTime.distantFuture)
            self.ppsdk_dispatch_sem.signal()
            
            let rdata = try JSONSerialization.data(withJSONObject: self.ppsdk_dispatch_result, options: [])
            outParam.pointee = NSString(data:rdata, encoding:String.Encoding.utf8.rawValue)
            
            addTextLog("isContinue : " + (self.ppsdk_dispatch_bContinue ? "true" : "false"))
            addTextLog(" outParams : " + formatJson(String(outParam.pointee!)))
            
            return self.ppsdk_dispatch_bContinue
        }
        catch{
        }
        return false
    }
    
    func ppsdk_onFinish(_ result:String)
    {
        DispatchQueue.main.sync(execute: {
            if(self.bInDispatch){
                self.ppsdk_dispatch_bContinue = false
                self.btInputDone.sendActions(for: UIControlEvents.touchUpInside)
            }
        })

        addTextLog("\n<< onFinish >>")
        addTextLog("result : " + formatJson(result))

        self.showAlert("終わった", completion: {})
        DispatchQueue.main.async(execute: {
            self.setEnableJobButtons(true)
        })
        
        saveMasterData()
    }
    
    func onDispatchInMainThread(_ json: NSDictionary, opt: String?){
        let ppsdk_command = json["command"] as! String
        switch ppsdk_command {
        case "notice":
            onNotice(json)
            break
        case "waitForStartingSlipPrint":
            onDispatchStartingSlipPrint(json)
            break
        case "waitForPrintingSlipPage":
            onDispatchPrintingSlipPage(json)
            break
        case "waitForRecoverablePrinterError":
            onDispatchRecoverablePrinterError(json)
            break
        case "waitForRetryConnectingReader":
            onDispatchRetryConnectingReader(json)
            break
        case "waitForApplicationSelect":
            onDispatchApplicationSelect(json)
            break
        case "waitForKidInput":
            onDispatchKidInput(json)
            break
        case "waitForTradeInfo":
            onDispatchTradeInfo(json)
            break
        case "waitForRefundInput":  /* NFCのみ */
            onDispatchRefundInput(json)
            break
        case "detectDcc":
            onDispatchDccSelect(json)
            break
        case "waitForDccSelect":
            onDispatchDccSelect(json)
            break
        case "waitForConfirmDoubleTrade":
            onDispatchConfirmDoubleTrade(json)
            break
        case "waitForDigiSign":
            onDispatchDigiSign(json)
            break
        case "waitForRetrySendingDigiSign":
            onWaitForRetrySendingDigiSign(json)
            break
        case "waitForCardActive":
            onDispatchCardActiveDialog(json)
            break
        case "waitForConfirmUpdateMasterData":
            onDispatchConfirmUpdateMasterData(json)
            break
        case "waitForOptions":
            onWaitForOptions(json, opt:opt!)
            break
        case "waitForJournalOptions":
            onWaitForOptions(json, opt:opt!)
            break
        case "waitForJournalReprintConfirm":
            onDispatchReprintConfirm(json)
            break
        case "waitForReprintOptions":
            onWaitForOptions(json, opt:opt!)
            break
        case "waitForReprintConfirm":
            onDispatchReprintConfirm(json)
            break
        case "waitForStart":
            onWaitForStart(json)
            break
        case "waitForActivationOptions":
            onWaitForActivationOptions(json)
            break
        case "waitForCardCheckResultConfirm":
            onWaitForCardCheckResultConfirm(json)
            break
        case "waitForServicePrintConfirm":
            onDispatchServiceConfirm(json)
            break
        case "waitForSettingIssuersConfirm":
            onDispatchSettingIssuersConfirm(json)
            break
        case "waitForShowingJobResult":
            onWaitForShowingJobResult(json)
            break
        case "waitForRetryCardRead":
            onDispatchWaitForRetryCardRead(json)
            break
        case "waitForPinResultConfirm":
            onDispatchPinResultConfirm(json)
            break
        case "waitForMSFBCardExist":
            onDispatchWaitForMSFBCardExist(json)
            break
        default:
            debugPrint("onDispatchInMainThread command: " + ppsdk_command)
            break
        }
    }
    
    func onWaitForActivationOptions(_ json:NSDictionary){
        self.ppsdk_dispatch_result.setValue("0000900123", forKey: "subscribeSequence")
        self.ppsdk_dispatch_result.setValue("01849130", forKey: "activateID")
        self.ppsdk_dispatch_result.setValue("92634868", forKey: "oneTimePassword")
        self.ppsdk_dispatch_sem.signal()
    }
    
    func onWaitForOptions(_ json: NSDictionary, opt: String){
        var ctrls = Array<UIControl>()
        switch(opt){
        case "onReprint":
            ctrls.append(scReprintPaymentType)
            ctrls.append(textSlipNo)
            break

        case "onJournal":
            ctrls.append(scJournalIntermediate)
            ctrls.append(scJournalReprint)
            ctrls.append(scJournalPaymentType)
            ctrls.append(scJournalDetail)
            break

        default:
            break
        }
        
        for ctrl in ctrls {
            ctrl.isEnabled = true
            if ctrl is UITextField {
                ctrl.becomeFirstResponder()
                ctrl.selectAll(nil)
            }
        }
        btInputDone.isEnabled = true
        
        btInputDone.onTouchUpInside({sender in

            for ctrl in ctrls {
                switch(ctrl){
                case self.textSlipNo:
                    if let slipNo = self.textSlipNo.text, !slipNo.isEmpty {
                        self.ppsdk_dispatch_result.setValue(slipNo, forKey:"slipNo")
                    }
                    break
                case self.scJournalIntermediate:
                    self.ppsdk_dispatch_result.setValue(
                        (self.scJournalIntermediate.selectedSegmentIndex == 1),
                        forKey:"bIntermediate")
                    break
                case self.scJournalReprint:
                    switch(self.scJournalReprint.selectedSegmentIndex){
                    case 0: self.ppsdk_dispatch_result.setValue("No", forKey:"reprint"); break
                    case 1: self.ppsdk_dispatch_result.setValue("Last", forKey:"reprint"); break
                    case 2: self.ppsdk_dispatch_result.setValue("BeforeLast", forKey:"reprint"); break
                    default: break
                    }
                    break
                case self.scReprintPaymentType:
                    switch(self.scReprintPaymentType.selectedSegmentIndex){
                    case 0: self.ppsdk_dispatch_result.setValue("Credit", forKey:"paymentType"); break
                    case 1: self.ppsdk_dispatch_result.setValue("NFC", forKey:"paymentType"); break
                    case 2: self.ppsdk_dispatch_result.setValue("UnionPay", forKey:"paymentType"); break
                    default: break
                    }
                    break
                case self.scJournalPaymentType:
                    switch(self.scJournalPaymentType.selectedSegmentIndex){
                    case 0: self.ppsdk_dispatch_result.setValue("Credit", forKey:"paymentType"); break
                    case 1: self.ppsdk_dispatch_result.setValue("NFC", forKey:"paymentType"); break
                    case 2: self.ppsdk_dispatch_result.setValue("UnionPay", forKey:"paymentType"); break
                    case 3: self.ppsdk_dispatch_result.setValue("All", forKey:"paymentType"); break
                    default: break
                    }
                    break
                case self.scJournalDetail:
                    self.ppsdk_dispatch_result.setValue(
                        (self.scJournalDetail.selectedSegmentIndex == 0) ?
                            "Summary" : "Detail",
                        forKey:"detailOption")
                    break
                default:
                    break
                }
                ctrl.isEnabled = false
            }
            self.btInputDone.isEnabled = false
            self.ppsdk_dispatch_sem.signal()
        })
    }
    
    func onDispatchKidInput(_ json: NSDictionary){
        btInputDone.isEnabled = true
        textKid.isEnabled = true
        textKid.becomeFirstResponder()
        textKid.selectAll(nil)
        btInputDone.onTouchUpInside({sender in
            self.ppsdk_dispatch_result.setValue(self.textKid.text!, forKey: "kid")
            
            self.btInputDone.isEnabled = false
            self.textKid.isEnabled = false
            self.ppsdk_dispatch_sem.signal()
        })
    }

    func onDispatchAmountInput(_ json: NSDictionary){
        btInputDone.isEnabled = true
        textAmount.isEnabled = true
        textAmount.becomeFirstResponder()
        textAmount.selectAll(nil)
        btInputDone.onTouchUpInside({sender in
            self.ppsdk_dispatch_result.setValue(Int(self.textAmount.text!), forKey: "amount")
            self.ppsdk_dispatch_result.setValue(0, forKey: "taxOtherAmount")
            self.ppsdk_dispatch_result.setValue("0000", forKey: "productCode")
            self.ppsdk_dispatch_result.setValue("OneTime", forKey: "payWayDiv")
            self.btInputDone.isEnabled = false
            self.textAmount.isEnabled = false
            self.ppsdk_dispatch_sem.signal()
        })
    }
    
    func onDispatchTradeInfo(_ json: NSDictionary){
        let parameter = json["parameter"] as! NSDictionary
        let values = parameter["values"] as! NSArray
        
        var bHasPaymentWay = false
        
        for value in values {
            let param = value as! NSDictionary
            switch (param["type"] as! String){
            case "amount":
                if let amount = param["current"] as? Int  {
                    textAmount.text = String(amount)
                }
                textAmount.isEnabled = ((param["attribute"] as! String) != "readonly")
                textAmount.becomeFirstResponder()
                textAmount.selectAll(nil)
                break
                
            case "taxOtherAmount":
                if let taxOtehrAmount = param["current"] as? Int {
                    textTaxOtherAmount.text = String(taxOtehrAmount)
                }
                textTaxOtherAmount.isEnabled = ((param["attribute"] as! String) != "readonly")
                break

            case "slipNo":
                textSlipNo.isEnabled = ((param["attribute"] as! String) != "readonly")
                textSlipNo.text = param["current"] as? String
                break
                
            case "approvalNo":
                textApprovalNo.isEnabled = ((param["attribute"] as! String) != "readonly")
                textApprovalNo.text = param["current"] as? String
                break

            case "otherTermJudgeNo":
                textOtherTermJudgeNo.isEnabled = ((param["attribute"] as! String) != "readonly")
                textOtherTermJudgeNo.text = param["current"] as? String
                break

            case "productCode":
                textProductCode.isEnabled = ((param["attribute"] as! String) != "readonly")
                textProductCode.text = param["current"] as? String;
                break
                
            case "paymentWayForRefund", "paymentWay":
                bHasPaymentWay = true
                preparePaymentWay(param)
                if let current = param["current"] as? NSDictionary {
                    if let paymentWay = current["paymentWay"] as? String {
                        switch paymentWay {
                        case "lump":
                            scPaymentWay.selectedSegmentIndex = 0
                            break
                        case "installment":
                            scPaymentWay.selectedSegmentIndex = 1
                            break
                        case "bonus":
                            scPaymentWay.selectedSegmentIndex = 2
                            break
                        case "bonusCombine":
                            scPaymentWay.selectedSegmentIndex = 3
                            break
                        case "revolving":
                            scPaymentWay.selectedSegmentIndex = 4
                            break
                        default:
                            break
                        }
                    }
                    else{
                        scPaymentWay.selectedSegmentIndex = -1
                    }
                    
                    paymentWayCtrls.forEach { (key, ctrl) in
                        if let value = current[key] as? Int {
                            ctrl.text = String(value)
                        }
                        else{
                            ctrl.text = nil
                        }
                    }
                }
                break

            case "coupon":
                prepareCouponList(param)
                break;           
            case "pan":
                break;

            case "cupSendDate":
                textCupSendDate.isEnabled = ((param["attribute"] as! String) != "readonly")
                textCupSendDate.text = param["current"] as? String
                break

            case "cupNo":
                textCupNo.isEnabled = ((param["attribute"] as! String) != "readonly")
                textCupNo.text = param["current"] as? String
                break
                
            case "cupTradeDiv":
                textCupTradeDiv.isEnabled = ((param["attribute"] as! String) != "readonly")
                textCupTradeDiv.text = param["current"] as? String
                break
                
            default:
                let output = param["type"] as! String
                debugPrint("onDispatchTradeInfo param[type]=" + output )
                break
            }
        }
        
        btInputRetry.isEnabled = parameter["updatable"] as! Bool
        btInputDone.isEnabled = true

        btInputDone.onTouchUpInside({sender in
            if bHasPaymentWay {
                guard self.scPaymentWay.selectedSegmentIndex >= 0 else {
                    self.showAlert("支払い方法を選択してください。", completion: {})
                    return
                }
            }
            self.finishDispatchTradeInfoInput(json, operation:"apply")
        })
        btInputRetry.onTouchUpInside({sender in
            self.finishDispatchTradeInfoInput(json, operation:"update")
        })
    }
    
    func finishDispatchTradeInfoInput(_ json: NSDictionary, operation:String)
    {
        let parameter = json["parameter"] as! NSDictionary
        let values = parameter["values"] as! NSArray
        let results = NSMutableDictionary();
        for value in values {
            let param = value as! NSDictionary
            switch (param["type"] as! String){
            case "amount":
                results.setValue(Int(self.textAmount.text!), forKey: "amount")
                break
                
            case "taxOtherAmount":
                results.setValue(Int(self.textTaxOtherAmount.text!), forKey: "taxOtherAmount")
                break

            case "slipNo":
                results.setValue(self.textSlipNo.text!, forKey: "slipNo")
                break

            case "approvalNo":
                results.setValue(self.textApprovalNo.text!, forKey: "approvalNo")
                break
                
            case "otherTermJudgeNo":
                results.setValue(self.textOtherTermJudgeNo.text!, forKey: "otherTermJudgeNo")
                break

            case "productCode":
                results.setValue(self.textProductCode.text!, forKey: "productCode")
                break
                
            case "paymentWay", "paymentWayForRefund":
                switch self.scPaymentWay.selectedSegmentIndex {
                case 0:
                    results.setValue("lump", forKey: "paymentWay")
                    break
                case 1:
                    results.setValue("installment", forKey: "paymentWay")
                    break
                case 2:
                    results.setValue("bonus", forKey: "paymentWay")
                    break
                case 3:
                    results.setValue("bonusCombine", forKey: "paymentWay")
                    break
                case 4:
                    results.setValue("revolving", forKey: "paymentWay")
                    break
                default:
                    results.setValue(NSNull(), forKey: "paymentWay")
                    break
                }
                paymentWayCtrls.forEach { (key, ctrl) in
                    if let value = Int(ctrl.text!) {
                        results.setValue(value, forKey: key)
                    }
                }
                break
                
            case "coupon":
                results.setValue(self.pickCurrentSelected == nil ? NSNull() : self.pickCurrentSelected, forKey: "coupon")
                break

            case "cupSendDate":
                results.setValue(self.textCupSendDate.text!, forKey: "cupSendDate")
                break
                
            case "cupNo":
                results.setValue(self.textCupNo.text!, forKey: "cupNo")
                break

            case "cupTradeDiv":
                results.setValue(self.textCupTradeDiv.text!, forKey: "cupTradeDiv")
                break
                
            default:
                let output = param["type"] as! String
                debugPrint("finishDispatchTradeInfoInput param[type]=" + output )
                break
            }
        }
        ppsdk_dispatch_result.setValue(operation, forKey: "operation")
        ppsdk_dispatch_result.setValue(results, forKey: "results")

        unprepareCouponList()
        unpreparePaymentWay()
        btInputRetry.isEnabled = false
        btInputDone.isEnabled = false
        textAmount.isEnabled = false
        textTaxOtherAmount.isEnabled = false
        textSlipNo.isEnabled = false
        textApprovalNo.isEnabled = false
        textOtherTermJudgeNo.isEnabled = false
        textProductCode.isEnabled = false
        textCupSendDate.isEnabled = false
        textCupNo.isEnabled = false
        textCupTradeDiv.isEnabled = false
        ppsdk_dispatch_sem.signal()
    }
    
    func prepareCouponList(_ param: NSDictionary)
    {
        guard let hps = param["config"] as? NSDictionary else {
            return
        }
        let normalObject = hps["normalObject"] as! NSDictionary
        let printInfoList = normalObject["printInfoList"] as! NSArray
        
        pickListTitles.append("（なし）")
        pickListValues.append(nil)
        
        for printInfo in printInfoList {
            let items = (printInfo as! NSDictionary)["printInfo"] as! NSDictionary
            pickListTitles.append(items["COUPON1"] as! String)
            pickListValues.append(items["COUPON_ID"] as? String)
        }
        pickCurrentSelected = nil
        
        pickCoupon.isUserInteractionEnabled = true
        pickCoupon.alpha = 1.0
        pickCoupon.delegate = self
        pickCoupon.dataSource = self
        pickCoupon.selectRow(0, inComponent: 0, animated: false)
    }
    
    func unprepareCouponList()
    {
        pickCoupon.isUserInteractionEnabled = false
        pickCoupon.alpha = 0.5
        pickCoupon.delegate = nil
        pickCoupon.dataSource = nil
        pickListTitles = []
        pickListValues = []
        pickCurrentSelected = nil
    }
    
    func preparePaymentWay(_ param: NSDictionary)
    {
        if let hps = param["config"] as? NSDictionary {
            let normalObject = hps["normalObject"] as! NSDictionary
            let dllPatternData = normalObject["dllPatternData"] as! NSDictionary
            
            let payBonus = dllPatternData["payBonus"] as! String
            let payBonusCombine = dllPatternData["payBonusCombine"] as! String
            let payInstallment = dllPatternData["payInstallment"] as! String
            let payLumpPay = dllPatternData["payLumpPay"] as! String
            let payRevolvingPay = dllPatternData["payRevolvingPay"] as! String
            
            scPaymentWay.isEnabled = true
            scPaymentWay.selectedSegmentIndex = -1
            scPaymentWay.setEnabled(payLumpPay != "0", forSegmentAt: 0)
            scPaymentWay.setEnabled(payInstallment != "0", forSegmentAt: 1)
            scPaymentWay.setEnabled(payBonus != "0", forSegmentAt: 2)
            scPaymentWay.setEnabled(payBonusCombine != "0", forSegmentAt: 3)
            scPaymentWay.setEnabled(payRevolvingPay != "0", forSegmentAt: 4)
        }
        else{
            scPaymentWay.isEnabled = false
            scPaymentWay.selectedSegmentIndex = -1
            if let paymentWay = param["paymentWay"] as? String {
                scPaymentWay.setEnabled(paymentWay == "lump", forSegmentAt: 0)
                scPaymentWay.setEnabled(paymentWay == "installment", forSegmentAt: 1)
                scPaymentWay.setEnabled(paymentWay == "bonus", forSegmentAt: 2)
                scPaymentWay.setEnabled(paymentWay == "bonusCombine", forSegmentAt: 3)
                scPaymentWay.setEnabled(paymentWay == "revolving", forSegmentAt: 4)
            }
        }
        paymentWayCtrls.forEach { (_, ctrl) in
            ctrl.isEnabled = true
        }
    }

    func unpreparePaymentWay()
    {
        scPaymentWay.isEnabled = false
        paymentWayCtrls.forEach { (_, ctrl) in
            ctrl.isEnabled = false
        }
    }
    
    /* NFCのみ */
    func onDispatchRefundInput(_ json: NSDictionary){
        btInputDone.isEnabled = true
        textAmount.isEnabled = true
        textSlipNo.isEnabled = true
        textSlipNo.becomeFirstResponder()
        textSlipNo.selectAll(nil)
        btInputDone.onTouchUpInside({sender in
            self.ppsdk_dispatch_result.setValue(Int(self.textAmount.text!), forKey: "amount")
            self.ppsdk_dispatch_result.setValue(self.textSlipNo.text!, forKey: "slipNo")
            self.ppsdk_dispatch_result.setValue(0, forKey: "taxOtherAmount")
            self.ppsdk_dispatch_result.setValue("0000", forKey: "productCode")
            self.ppsdk_dispatch_result.setValue("OneTime", forKey: "payWayDiv")
            
            self.btInputDone.isEnabled = false
            self.textAmount.isEnabled = false
            self.textSlipNo.isEnabled = false
            
            self.ppsdk_dispatch_sem.signal()
        })
    }
    
    func onDispatchRetryConnectingReader(_ json: NSDictionary){
         showAlert("R/W接続できない。再試行する", button1: "再試行", button2: "諦める") { (button) in
            if button == "再試行" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "諦める" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = true
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }

    func onDispatchRecoverablePrinterError(_ json: NSDictionary){
        showAlert("プリンタエラーが発生", button1: "再試行", button2: "諦める") { (button) in
            if button == "再試行" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bExecute")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "諦める" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bExecute")
                self.ppsdk_dispatch_bContinue = true
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }

    func onDispatchStartingSlipPrint(_ json: NSDictionary){
        self.ppsdk_dispatch_result.setValue(false, forKey: "bExecute")
        self.ppsdk_dispatch_bContinue = true
        self.ppsdk_dispatch_sem.signal()
    }
    
    func onDispatchPrintingSlipPage(_ json: NSDictionary){
        showAlert("次のページを印字する？", button1: "する", button2: "しない") { (button) in
            if button == "する" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bExecute")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "しない" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bExecute")
                self.ppsdk_dispatch_bContinue = true
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onWaitForRetrySendingDigiSign(_ json: NSDictionary){
        showAlert("電子サイン情報送信失敗", button1: "もう１回", button2: "おわり") { (button) in
            if button == "もう１回" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "おわり" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = true
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onDispatchConfirmDoubleTrade(_ json: NSDictionary){
        showAlert("同一取引だよ", button1: "やれ", button2: "やめとく") { (button) in
            if button == "やれ" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "やめとく" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onWaitForStart(_ json: NSDictionary){
        showAlert("開始待ちです", button1: "開始", button2: "中断") { (button) in
            if button == "開始" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "中断" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onWaitForShowingJobResult(_ json: NSDictionary){
        showAlert("業務結果表示イベント", completion: {
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
        });
    }
    
    func onDispatchPinResultConfirm(_ json: NSDictionary){
        let param        = json["parameter"] as! NSDictionary
        let pinResult    = param["pinResult"] as! String

        var msg = pinResult
        var bPinSuccess = false
        var bPinOver = false
        switch msg {
        case "success":
            msg = "PIN成功"
            bPinSuccess = true
            break

        case "PinFail-deny":
            msg = "PIN誤り"
            bPinSuccess = false
            break

        case "PinFail-bypass":
            msg = "PINバイパス失敗"
            bPinSuccess = false
            break

        case "PinFail-over":
            msg = "PIN超過"
            bPinSuccess = false
            bPinOver = true
            break

        default:
            break;
        }

        if(!bPinSuccess && !bPinOver){
            let bOnetimeRemain = param["bOnetimeRemain"] as! Bool
            if(bOnetimeRemain){
                msg = msg + "、残り一回"
            }
            msg = msg + "\n続きますか"
            showAlert(msg, button1: "続きます", button2: "おわり") { (button) in
                if button == "続きます" {
                    self.ppsdk_dispatch_result.setValue(true, forKey: "bContinue")
                    self.ppsdk_dispatch_bContinue = true
                }
                else if button == "おわり" {
                    self.ppsdk_dispatch_result.setValue(false, forKey: "bContinue")
                    self.ppsdk_dispatch_bContinue = true
                }
                self.ppsdk_dispatch_sem.signal()
            }
        }
        else{ // 成功またPIN超過なら、選択が不要になる
            showAlert(msg, completion: {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bContinue")
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
        }
    }
    
    func onDispatchWaitForRetryCardRead(_ json: NSDictionary){
        let param        = json["parameter"] as! NSDictionary
        let result       = param["result"] as! NSDictionary
        let detail       = result["detail"] as! NSDictionary
        let errorObject  = detail["errorObject"] as! NSDictionary
        let errorCode    = errorObject["errorCode"] as! String
        let errorMessage = errorObject["errorMessage"] as! String
        
        let message = errorCode + "\n" + errorMessage
        
        showAlert(message, button1: "もう１回", button2: "おわり") { (button) in
            if button == "もう１回" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "おわり" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onDispatchWaitForMSFBCardExist(_ json: NSDictionary){
        let param        = json["parameter"] as! NSDictionary
        let result       = param["result"] as! NSDictionary
        let detail       = result["detail"] as! NSDictionary
        let errorObject  = detail["errorObject"] as! NSDictionary
        let errorCode    = errorObject["errorCode"] as! String
        let errorMessage = errorObject["errorMessage"] as! String
    
        let message = errorCode + " : " + errorMessage + "\n" + "カードが抜いたことを確認しました"
        showAlert(message, button1: "確認", button2: "キャンセル") { (button) in
            if button == "確認" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "キャンセル" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bRetry")
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onDispatchCardActiveDialog(_ json: NSDictionary){
        showAlert("携帯みてカードを何とかしろ", button1: "やった", button2: "やめた") { (button) in
            if button == "やった" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "やめた" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }
    
    func onDispatchConfirmUpdateMasterData(_ json: NSDictionary){
        showAlert("マスターデータ更新する？", button1: "する", button2: "しない") { (button) in
            if button == "する" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "しない" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }

    func onDispatchApplicationSelect(_ json: NSDictionary){
        let parameter = json["parameter"] as! NSDictionary
        let applicationList = parameter["applicationList"] as! NSArray
        
        pickCurrentSelected = nil
        for item in applicationList {
            let aitem = item as! NSDictionary
            pickListTitles.append(aitem["displayLabel"] as! String)
            pickListValues.append(aitem["aid"] as? String)
            if pickCurrentSelected == nil {
                pickCurrentSelected = aitem["aid"] as? String
            }
        }
                
        pickCoupon.isUserInteractionEnabled = true
        pickCoupon.alpha = 1.0
        pickCoupon.delegate = self
        pickCoupon.dataSource = self
        pickCoupon.selectRow(0, inComponent: 0, animated: false)
        btInputDone.isEnabled = true
        btInputDone.onTouchUpInside({sender in
            self.ppsdk_dispatch_result.setValue(self.pickCurrentSelected == nil ? NSNull() : self.pickCurrentSelected, forKey: "aid")
            //self.ppsdk_dispatch_result.setValue(self.pickCurrentSelected, forKey: "couponIndex")
            self.btInputDone.isEnabled = false
            self.pickCoupon.isUserInteractionEnabled = false
            self.pickCoupon.alpha = 0.5
            self.pickCoupon.delegate = nil
            self.pickCoupon.dataSource = nil
            self.pickListTitles = []
            self.pickListValues = []
            self.pickCurrentSelected = nil
            self.ppsdk_dispatch_sem.signal()
        })
        
    }
    
    func onDispatchDccSelect(_ json: NSDictionary){
        showAlert("DCCで決済するする？", button1: "する", button2: "しない") { (button) in
            if button == "する" {
                self.ppsdk_dispatch_result.setValue(true, forKey: "bDccTrade")
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "しない" {
                self.ppsdk_dispatch_result.setValue(false, forKey: "bDccTrade")
                self.ppsdk_dispatch_bContinue = true
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }

    func onDispatchDigiSign(_ json: NSDictionary){
        let parameter = json["parameter"] as! NSDictionary
        let hps = parameter["hps"] as! NSDictionary
        let modulus = hps["modulus"] as! String
        let exponent = hps["exponent"] as! String
        
        showAlert("電子サインを送る", completion: {
            let image = UIImage(named: "Drawing.png")
            let sign = self.ppsdk?.hexString(fromDigiSign: image?.cgImage, modulus: modulus, exponent: exponent)
            self.ppsdk_dispatch_result.setValue(sign!, forKey: "encryptedDigiSignData")
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
        })
    }
    
    func onDispatchReprintConfirm(_ json: NSDictionary){
        showAlert("再印字表示確認", button1: "続行", button2: "中断") { (button) in
            if button == "続行" {
                self.ppsdk_dispatch_bContinue = true
            }
            else if button == "中断" {
                self.ppsdk_dispatch_bContinue = false
            }
            self.ppsdk_dispatch_sem.signal()
        }
    }

    func onWaitForCardCheckResultConfirm(_ json: NSDictionary){
        let parameter   = json["parameter"] as! NSDictionary
        let cardNo      = parameter["CARD_NO"] as! String
        let slipNo      = parameter["SLIP_NO"] as! String
        let approvalNo  = parameter["A_NO"] as! String
        let checkResult = parameter["checkResult"] as! String
        
        textOtherTermJudgeNo.text   = cardNo  // カード番号
        textSlipNo.text             = slipNo
        textApprovalNo.text         = approvalNo
        
        showAlert(checkResult, completion: {
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
        });
    }

    func onNotice(_ json: NSDictionary){
        let message = json["message"] as! String
        switch (message){
        case "tradeConfirmFinished":
            let parameter     = json["parameter"] as! NSDictionary
            let hps           = parameter["hps"] as! NSDictionary
            let normalObject  = hps["normalObject"] as! NSDictionary
            let tradeResult   = normalObject["tradeResult"] as! NSDictionary
            
            let CARD_NO       = tradeResult["CARD_NO"] as! String
            let SLIP_NO       = tradeResult["SLIP_NO"] as! String
            let TRADE_DATE    = tradeResult["TRADE_DATE"] as! String
            let displayMsg    = "伝票番号：" + SLIP_NO + "\nカード番号：" + CARD_NO + "\n送信日時：" + TRADE_DATE;
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            break;
            
        case "refundSearchError":
            let parameter    = json["parameter"] as! NSDictionary
            let result       = parameter["result"] as! NSDictionary
            let detail       = result["detail"] as! NSDictionary
            let errorObject  = detail["errorObject"] as! NSDictionary
            let errorMessage = errorObject["errorMessage"] as! String
            let errorCode    = errorObject["errorCode"] as! String
            let errorType    = errorObject["errorType"] as! String
            let displayMsg   = errorCode + "\n" + errorType + "\n" + errorMessage;
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            break;
            
        case "printFinished":
            addTextLog("印字終了。" + message )
            showAlert("印字終了", completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            break;
            
        case "waitForCardRead":
            addTextLog("waitForCardRead" )
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
            break
            
        case "onCardReadFinished":
            let parameter     = json["parameter"] as! NSDictionary
            let type          = parameter["type"] as! String
            let displayMsg    = "NFC：" + type + "\nカードリードが終わったらしい";
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            
            break
            
        case "cardDetected":
            let parameter     = json["parameter"] as! NSDictionary
            let type          = parameter["cardType"] as! String
            addTextLog("cardDetected : " + type )
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
            break;
            
        case "waitForCardActive":
            let displayMsg    = "NFC：SEE PHONE";
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            
            break
            
        case "checkingMasterData":
            let displayMsg    = "マスターデータチェック";
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            
            break
        case "masterDataUpdating":
            let displayMsg    = "マスターデータ更新開始";
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            
            break
          
        case "centerTransaction":
            let displayMsg    = "NFCオンライン決済";
            
            showAlert(displayMsg, completion: {
                self.ppsdk_dispatch_bContinue = true
                self.ppsdk_dispatch_sem.signal()
            });
            
            break
            
        default:
            debugPrint("json[message]:" + message )
            addTextLog("onNotice : " + message )
            self.ppsdk_dispatch_bContinue = true
            self.ppsdk_dispatch_sem.signal()
            break
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickListTitles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickListTitles[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickCurrentSelected = pickListValues[row]
    }
    
    func onDispatchServiceConfirm(_ json: NSDictionary){
        let parameter    = json["parameter"] as! NSDictionary
        let data : NSData! = try? JSONSerialization.data(withJSONObject: parameter, options: []) as NSData
        let jsonTradeHps = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)! as String
        addTextLog("\n<< ServiceConfirm >> ")
        addTextLog("\nServic List : " + formatJson(jsonTradeHps))

        self.ppsdk_dispatch_bContinue = true
        self.ppsdk_dispatch_sem.signal()
    }
    
    func onDispatchSettingIssuersConfirm(_ json: NSDictionary){
        let parameter    = json["parameter"] as! NSDictionary
        let data : NSData! = try? JSONSerialization.data(withJSONObject: parameter, options: []) as NSData
        let jsonTradeHps = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)! as String
        addTextLog("\n<< SettingIssuersConfirm >> ")
        addTextLog("\nSettingIssuers : " + formatJson(jsonTradeHps))
        
        self.ppsdk_dispatch_bContinue = true
        self.ppsdk_dispatch_sem.signal()
    }    
    
    func showAlert(_ message: String, completion: @escaping ()->Void) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(
                title: "ppsdk Sample",
                message: message,
                preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(
                title: "OK",
                style: .default,
                handler: {(action:UIAlertAction!) -> Void in
                    completion()
                })
            alertController.addAction(defaultAction)
            
            self.present(
                alertController,
                animated: false,
                completion: nil)
        })
    }

    func showAlert(_ message: String, button1: String, button2: String, completion: @escaping (String)->Void) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(
                title: "ppsdk Sample",
                message: message,
                preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(
                title: button1,
                style: .default,
                handler: {(action:UIAlertAction!) -> Void in
                    completion(button1)
            })
            alertController.addAction(defaultAction)
            
            let cancelAction = UIAlertAction(
                title: button2,
                style: .cancel,
                handler: {(action:UIAlertAction!) -> Void in
                    completion(button2)
            })
            alertController.addAction(cancelAction)

            self.present(
                alertController,
                animated: false,
                completion: nil)
        })
    }
    
    func formatJson(_ src: String) -> String {
        do {
            let jsonData = src.data(using: String.Encoding.utf8)!
            
            let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
            
            let prettyJsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            
            return "json -> \n" + String(data: prettyJsonData, encoding: String.Encoding.utf8)!
        } catch {
        }
        return "plain text -> \"\(src)\""
    }    
}


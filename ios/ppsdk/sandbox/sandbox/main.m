//
//  main.m
//  sandbox
//
//  Created by tel on 2017/12/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

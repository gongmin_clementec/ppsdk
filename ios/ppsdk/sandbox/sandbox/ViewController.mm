//
//  ViewController.m
//  sandbox
//
//  Created by tel on 2017/12/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import "ViewController.h"
#include <Json.h>
#include <rxcpp/rx.hpp>
#include <execinfo.h>

using json_t = basic_json_t<char>;

namespace pprx {
    using namespace rxcpp;
    using namespace rxcpp::subjects;
    using namespace rxcpp::sources;
    using namespace rxcpp::operators;
    using namespace rxcpp::util;
}

#include <memory>
#include <sstream>

static std::string getBacktraceSymbols()
{
    std::array<void*, 100> buff;
    
    auto nptrs = ::backtrace(buff.data(), buff.size());
    char** s = backtrace_symbols(buff.data(), nptrs);
    
    std::stringstream ss;
    for(auto i = 0; i < nptrs; i++){
        ss << s[i] << std::endl;
    }
    
    ::free(s);
    
    return ss.str();
}


int getThreadId()
{
    static std::recursive_mutex mtx;
    static std::map<int,int> tmap;
    
    const auto tid = static_cast<int>(::pthread_mach_thread_np(::pthread_self()));
    
    std::unique_lock<std::recursive_mutex> lock(mtx);
    auto&& it = tmap.find(tid);
    if(it == tmap.end()){
        const int altid = tmap.size();
        tmap.insert(std::map<int,int>::value_type(tid, altid));
        return altid;
    }
    else{
        return it->second;
    }
}

static void log(const char* f, ...)
{
    va_list args;
    
    char buff[10240];
    va_start(args, f);
    ::vsprintf(buff, f, args);
    va_end(args);

    char buff2[1024];
    ::sprintf(buff2, "[%02d] %s\n", getThreadId(), buff);

    ::fputs(buff2, stdout);
}




static void testRxThreads()
{
    log("-- begin testRxThreads() -- ");
    
    /* 下記のクラスをshared_ptrで生成し、ラムダ式でキャプチャすれば、解放タミングが分かる筈 */
    struct CheckMemoryLeak
    {
        CheckMemoryLeak() { log("create CheckMemoryLeak"); }
        ~CheckMemoryLeak(){ log("free CheckMemoryLeak"); }
    };
    
    auto memcheck = std::make_shared<CheckMemoryLeak>();
    
    auto obs = pprx::observable<>::create<int>([memcheck](pprx::subscriber<int> s){
        /* ここは subscribe_on で指定されたスレッドで走行する */
        for(int i = 0; ; i++){
            usleep(1 * 1000 * 1000);
            log("on_next(%d)", i);
            s.on_next(i);
            /* 連続した値を投げ続けるobservableはunsubscribeしても動作し続ける。 */
            /* したがって、unsubscribeしたかどうかを判定しon_completed()することで、全てのリソースが解放される。 */
            if(!s.is_subscribed()){
                log("on_complete()");
                s.on_completed();
                break;
            }
            /* ちなみに、全てリソースの解放とは、関数オブジェクトの解放であり、関数オブジェクトの解放により、キャプチャされているオブジェクトが解放される。 */
            /* このケースは連続した値を投げ続けるケースであり、単発のobservableはon_next(),on_complete()を順に呼べば問題はない。　*/
        }
    });
    
    auto subs = obs
    /* subscribe_on()でobservableのon_next()を呼ぶ処理のスレッドを指定する。 */
    /* もう少し正確に言うと、observable::create()の関数オブジェクトが呼び出されるスレッドである。 */
    /* なので、on_next() を呼び出した後、スレッドが切り替わるのではなく、指定スレッドに切り替わった後、createの関数オブジェクトが呼び出される。 */
    /* 当たり前だが、subjectのsubscriber::on_next()をやってもスレッドが切り替わる訳じゃない。 */
    .subscribe_on(pprx::observe_on_new_thread())
    .map([memcheck](auto data){
        /* 特に指定がない場合は、on_nextが実行されたスレッド（つまり、subscribe_onで指定したスレッド）で走行する */
        log("before observable_on");
        return data * 10;
    })
    .observe_on(pprx::observe_on_new_thread()) /* 以降のスレッドを指定する */
    .map([memcheck](auto data){
        /* ここは observe_on で指定されたスレッドで実行される */
        log("after observable_on");
        return data +1;
    })
    .subscribe([memcheck](auto data){
        /* ここは observe_on で指定されたスレッドで実行される（subscribe_onで指定したスレッドではない） */
        log("subscribe success (%d)", data);
    }, [memcheck](auto error){
        log("subscribe error");
    }, [memcheck](){
        log("subscribe completed");
    });
    
    std::async(std::launch::async, [subs,memcheck](){
        usleep(5 * 1000 * 1000);
        log("**unsubscribe**");
        subs.unsubscribe();
    });

    /* subscribe_on によりobservableの源流が非同期実行されるため、*/
    /* 関数連鎖の後段も非同期実行される。 */
    /* なので、subscribeが完了しなくても、ココに到達しtestRxThreads()関数は終了する。 */
    log("-- end testRxThreads() -- ");
}


void testJson()
{
    auto add = [](json_t j)
    {
        /* json_t はsmart_ptrの挙動をするので、呼び出し側のインスタンスにも反映される。*/
        j.put("add()", "called");
    };
    
    auto repl = [](json_t j)
    {
        /* しかし、この場合は、json_tが内包するsmart_ptrが別のインスタンスに管理されるため、呼び出し側のインスタンスに影響はない。 */
        j = json_t();
        j.put("repl()", "called");
        
        /* また、ルートが配列／連想配列が切り替わると、ルートのjson_tが抱えるsmart_ptrのインスタンスが変わるため、同様の結果となる点に注意。 */
    };

    auto repl2 = [](json_t& j)
    {
        /* あまりオススメしないが、下記のようにすれば呼び出し側のインスタンスにも反映される（参照なので当たり前） */
        /* だが、これだとsmart_ptr管理が行われないため、非同期処理ではクラッシュするかも知れない */
        j = json_t();
        j.put("repl2()", "called");
    };
    
    json_t j;
    j.put("json", "initialize");
    log(j.str().c_str());

    /* json_t はsmart_ptrの挙動をする */
    add(j);
    log(j.str().c_str());

    repl(j);
    log(j.str().c_str());

    repl2(j);
    log(j.str().c_str());

    /* smart_ptr的な動作は、get()で取得されるjson_tでも同様 */
    {
        j.put("sub.test", "1");
        log(j.str().c_str());

        auto sub = j.get("sub");
        sub.put("add", "add");
        log(j.str().c_str());
    }

    /* get()で取得したjson_tを切り離したい場合、clone()で複製を作成する */
    {
        j.put("sub.test", "1");
        log(j.str().c_str());
        
        auto sub = j.get("sub").clone();
        sub.put("add2", "add2");
        log(j.str().c_str());
        log("sub --> %s", sub.str().c_str());
    }

    /* json_t は内部的にshare_ptrで管理しているため、const で属性管理を行うことが必要。 */
    /* const json_t という定義はできるのは当然であるが、reactive xにて下記のコードはコンパイルエラーとなる。 */

#if 0
    pprx::just(1)
    .flat_map([](auto n) -> pprx::observable<const json_t>{ /* json_tはsmart_ptr的な動作をするので、後段で編集不能にするためconstにしたい */
        json_t j;
        return pprx::just(j); /* でもjustで生成する時点でコンパイルエラーとなる。 */
    })
    .subscribe([](auto j){
        j.put("a", "b");    /* こういうことを防ぎたい（ここでコンパイルエラーしたい） */
    });
#endif
    
    /* 仮に const json_t が出来たとしても、json_tへコピーした場合、依然としてshared_ptr的な動作で不測の事態が発生する可能性がる。　*/
    /* そこで、json_t を完全に const にしたい場合には、const json_tではなく、json_t::readonlyを使用しなければならない。　*/
    /* json_t::readonlyはjson_tとの派生関係はなく、const json_tを内包しており、json_tのconstメンバ関数のみ透過する。　*/
    /* 使用感はjson_tと全く同じであるが、json_tへのコピーができないのが大きな特徴である。　*/
    /* もし、json_tへのコピーを行いたい場合には、clone()を使用して、明示的に元のjson_tから切り離すことが必要になる。（これが目的） */
    /* ただし、clone() は serialize -> deserialize という実装なので、処理速度は速くない点に注意すること。 */
    pprx::just(1)
    .flat_map([](auto n){ /* json_tはsmart_ptr的な動作をするので、後段で編集不能にするためconstにしたい */
        json_t org;
        org.put("org", "initialize");
        return pprx::just(org.as_readonly());
    })
    .subscribe([](json_t::readonly j){
        ::fputs(getBacktraceSymbols().c_str(), stdout);
        //j.put("a", "b");   /* 非constなメンバ関数は定義されていない（コンパイルエラー） */
        auto a = j.get<std::string>("org");
        //json_t jj = j;  /* 親子関係にないので許されない（コンパイルエラー）　*/
        json_t jj = j.clone(); /* json_tのshared_ptrを切り離す */
    });
}

static void testAnonymousStructure()
{
    auto f = []()
    {
        struct {int a; int b; int c;} nums = {0, 1, 2};
        return nums;
    };
    {
        auto a = f();
        log("a=%d, b=%d, c=%d", a.a, a.b, a.c);
    }
    
    { /* c++17 */
        auto [a, b, c] = f();
        log("a=%d, b=%d, c=%d", a, b, c);
    }

    pprx::just(1)
    .map([](auto n){
        struct {int a; int b; int c;} nums = {0, 1, 2};
        return nums;
    })
    .subscribe([](auto a){
        log("a=%d, b=%d, c=%d", a.a, a.b, a.c);
    });
    
    
}

void testRxTimeout()
{
    struct unit{};
    auto timeout = pprx::just(unit())
    .delay(std::chrono::seconds(5))
    .flat_map([=](auto){
        return pprx::error<long>(std::make_exception_ptr(std::exception()));
    });
    
    timeout
    .subscribe([=](auto){
        log("onnext");
    }, [=](auto){
        log("onerror");
    }, [=](){
        log("completed");
    });
    
    
    
//    pprx::interval(std::chrono::milliseconds(1000))
//    .amb(pprx::observe_on_new_thread(), timeout)
//    .subscribe([=](auto n){
//        log("%d", n);
//    });
}


#include <boost/any.hpp>

template <typename T, typename C> class recorder
{
private:
    using value_t = T;
    using coord_t = C;
    enum class emethod {
        on_next,
        on_error,
        on_completed
    };
    struct method_value_t{
        emethod     method;
        boost::any  value;
    };
    pprx::subjects::replay<method_value_t, coord_t> m_replay;

public:
    recorder(coord_t coord) : m_replay(coord) {}
    ~recorder(){
        m_replay.get_subscriber().on_completed();
    }
    
    void on_next(value_t&& value)
    {
        m_replay.get_subscriber().on_next(method_value_t{emethod::on_next, std::move(value)});
    }
    void on_error(std::exception_ptr error)
    {
        m_replay.get_subscriber().on_next(method_value_t{emethod::on_error, error});
    }
    void on_completed()
    {
        m_replay.get_subscriber().on_next(method_value_t{emethod::on_completed, boost::none});
    }

    auto player() -> pprx::observable<value_t>
    {
        return pprx::observable<>::create<value_t>([=](pprx::subscriber<value_t> s){
            m_replay.get_observable()
            .subscribe([=](auto mv){
                switch(mv.method){
                    case emethod::on_next:
                        s.on_next(boost::any_cast<value_t>(mv.value));
                        break;
                    case emethod::on_error:
                        s.on_error(boost::any_cast<std::exception_ptr>(mv.value));
                        break;
                    case emethod::on_completed:
                        s.on_completed();
                        break;
                }
            });
        });
    }
};

void testRxReplaySubject()
{
    recorder<int, pprx::observe_on_one_worker> r(pprx::observe_on_new_thread());
    r.on_next(1);
    r.on_next(2);
    r.on_next(3);
    r.on_completed();

    r.player().subscribe([=](auto a){
        log("%d", a);
    },[=](auto err){
        log("error");
    },[=](){
        log("completed");
    });

    r.on_next(4);
    r.on_next(5);
    r.on_next(6);

#if 0
    {
        auto published = pprx::range<int>(0, 10).publish();
        std::cout << "subscribe to published" << std::endl;
        published.subscribe(
                            // on_next
                            [](int v){std::cout << v << ", ";},
                            // on_completed
                            [](){std::cout << " done." << std::endl;});
        std::cout << "connect to published" << std::endl;
        published.connect();
    }
    {
        auto published = pprx::range<int>(0, 10).publish().ref_count();
        std::cout << "subscribe to ref_count" << std::endl;
        published.subscribe(
                            // on_next
                            [](int v){std::cout << v << ", ";},
                            // on_completed
                            [](){std::cout << " done." << std::endl;});
    }
#endif


#if 0
    pprx::subjects::replay<int,pprx::observe_on_one_worker> sub(pprx::observe_on_new_thread());
    auto obs = sub.get_observable().publish();
    //pprx::subject<int> sub;
//    sub.get_subscriber().on_error(std::make_exception_ptr(1));

    log("start subscribe");

    sub.get_subscriber().on_next(1);
    sub.get_subscriber().on_next(2);
    sub.get_subscriber().on_next(3);
    sub.get_subscriber().on_completed();
    

    obs
    .subscribe([=](auto n){
        log("onnext %d", n);

    }, [=](auto){
        log("onerror");
    }, [=](){
        log("completed");
    });
    
    
    obs
    .map([=](auto n){
        //        std::async(std::launch::async, [=](){
        //            sub.get_subscriber().on_completed();
        //        });
        return n * 2;
    })
    .subscribe([=](auto n){
        log("onnext %d", n);
        
    }, [=](auto){
        log("onerror");
    }, [=](){
        log("completed");
    });
    
    //obs.connect();
    
#endif
    
    
    
}

template <typename T> struct wrap
{
    const T& m_vec;
    wrap(const T& vec) : m_vec(vec) {}
    auto observable() -> pprx::observable<typename T::value_type>
    {
        pprx::observable<>::create<T::value_type>([=](pprx::subscriber<typename T::value_type> s){
            for(auto&& c : m_vec){
                s.on_next(c);
            }
            s.on_completed();
        });
    }
    
};

template <typename T> auto observable_from(const T& list) -> pprx::observable<typename T::value_type>
{
    return pprx::observable<>::create<typename T::value_type>([=](pprx::subscriber<typename T::value_type> s){
        for(auto&& item : list){
            s.on_next(item);
        }
        s.on_completed();
    });
}

void testRxListConvert()
{
    std::vector<int> a{1, 2, 3};
    observable_from(a)
    .subscribe([=](auto n){
        log("%d", n);
    });
    
    std::map<std::string,int> b{
        {"abc", 1},
        {"def", 2},
    };
    observable_from(b)
    .subscribe([=](auto v){
        log("%s, %d", v.first.c_str(), v.second);
    });

    auto c = {1, 2, 3};
    observable_from(c)
    .subscribe([=](auto n){
        log("%d", n);
    });


}

void testBehaviorSubject()
{
    log("testBehaviorSubject");
#if 0   // 基本形
    {
        auto s = rxcpp::subjects::behavior<int>(0);
        s.get_observable()
        .observe_on(rxcpp::observe_on_new_thread()) // これをやらないとsubscribe に再入する（behaviorに限らない）
        .subscribe([=](auto n){
            log("%d", n);
            n++;
            if(n == 10) s.get_subscriber().on_completed();
            else s.get_subscriber().on_next(n);
        }, [=](auto){
            log("error");
        }, [=](){
            log("completed");
        });
    }
#endif
#if 1   // flat_mapでの謎
    {
        auto s = rxcpp::subjects::behavior<int>(0); // ここなら動く

        rxcpp::sources::just(1)
        .flat_map([=](auto){
#if 1
            // auto s = rxcpp::subjects::behavior<int>(0); // ここだと get_observable() で落ちる
            // rxcpp::subjects::behavior<int> s(0);         // 宣言方法を変えたがダメ
            return s.get_observable()
            .observe_on(rxcpp::observe_on_new_thread())
            .flat_map([=](auto n){
                log("flatmap %d", n);
                n++;
                if(n == 10) s.get_subscriber().on_completed();
                else s.get_subscriber().on_next(n);
                return rxcpp::sources::just(n);
            });
#endif
#if 0
            auto s = rxcpp::subjects::behavior<int>(0);
            auto o = s.get_observable();
            return o
            .observe_on(rxcpp::observe_on_new_thread()) // これをやらないとsubscribe に再入する（behaviorに限らない）
            .flat_map([o, s](auto n){
                log("flatmap %d", n);
                n++;
                if(n == 10) s.get_subscriber().on_completed();
                else s.get_subscriber().on_next(n);
                return rxcpp::sources::just(n);
            });
#endif
        })
        .subscribe([=](auto n){
            log("next %d", n);
        }, [=](auto){
            log("error");
        }, [=](){
            log("completed");
        });
    }
#endif
}

void testMergeMap()
{
    pprx::subject<std::string> s1;
    pprx::subject<std::string> s2;
    
    s1.get_observable()
    .merge(rxcpp::observe_on_new_thread(), s2.get_observable())
    .subscribe([=](std::string str){
        log("next %s", str.c_str());
    }, [=](auto){
        log("error");
    }, [=](){
        log("completed");
    });

    s2.get_subscriber().on_next("s2 #1");
//    s2.get_subscriber().on_next("s2 #2");
//    s2.get_subscriber().on_next("s2 #3");
//
//    s1.get_subscriber().on_next("s1 #1");
//
//    s1.get_subscriber().on_completed();
//    s2.get_subscriber().on_error(std::make_exception_ptr(std::bad_exception()));

    
    
    
    
}


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //testRxThreads();
    //testRxTimeout();
    //testRxReplaySubject();

    //testRxListConvert();

    //testJson();
    
    //testAnonymousStructure();
    
    //testBehaviorSubject();
    
    testMergeMap();
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

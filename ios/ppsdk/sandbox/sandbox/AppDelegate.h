//
//  AppDelegate.h
//  sandbox
//
//  Created by tel on 2017/12/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


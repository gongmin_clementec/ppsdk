#!/bin/bash


## DEVELOPER_DIR
### ビルドで使用するXcode.appへのパス
### user_enviromnent.sh があれば、その設定を優先する
### user_enviromnent.sh はgitに登録されていないので
### 自身で作成してください。
### sample:
###   export DEVELOPER_DIR="/Applications/Xcode731.app"
if [ -f "user_enviromnent.sh" ]
then
  . ./user_enviromnent.sh
fi

# 最終成果物の削除を前段に移動した
if [ -f "ppsdk.tar.gz" ]
then
    rm ppsdk.tar.gz
fi

# CONF=Debug
CONF=Release

# ------------------------------------------------------------------
#   SDKのビルド
#   シミュレータ版（i386, x86_64）と実機版（arm64, arm7）を合成する
# ------------------------------------------------------------------

if [ -d "./build/${CONF}-universal" ]
then
    rm -R "./build/${CONF}-universal"
fi

mkdir "./build/${CONF}-universal"

# シミュレータのclear
xcodebuild \
    -project ppsdk.xcodeproj \
    -target ppsdk \
    -configuration ${CONF} \
    -sdk iphonesimulator \
    ONLY_ACTIVE_ARCH=NO \
    clean

# 実機のclear
xcodebuild \
    -project ppsdk.xcodeproj \
    -target ppsdk \
    -configuration ${CONF} \
    -sdk iphoneos \
    ONLY_ACTIVE_ARCH=NO \
    clean

# シミュレータのbuild
xcodebuild \
    -project ppsdk.xcodeproj \
    -target ppsdk \
    -configuration ${CONF} \
    -sdk iphonesimulator \
    ONLY_ACTIVE_ARCH=NO \
    build

# 実機のbuild
xcodebuild \
    -project ppsdk.xcodeproj \
    -target ppsdk \
    -configuration ${CONF} \
    -sdk iphoneos \
    ONLY_ACTIVE_ARCH=NO \
    build

# シミュレータ用のフォルダにframeworkをコピー
if [ -d "./build/${CONF}-universal" ]
then
    rm -R "./build/${CONF}-universal"
fi
mkdir -p "./build/${CONF}-universal"
cp -R "./build/${CONF}-iphoneos/ppsdk.framework" "./build/${CONF}-universal/ppsdk.framework/"

# シミュレータと実機のライブラリを合成する
lipo -create \
    -output \
        "./build/${CONF}-universal/ppsdk.framework/ppsdk" \
    "./build/${CONF}-iphoneos/ppsdk.framework/ppsdk" \
    "./build/${CONF}-iphonesimulator/ppsdk.framework/ppsdk"

# ------------------------------------------------------------------
#   成果物をsdk.libにコピーする
#   sample.xcodeproj が sdk.lib を参照するため
# ------------------------------------------------------------------
if [ -d "sdk.lib" ]
then
    rm -R sdk.lib
fi

mkdir sdk.lib
cp -R "./build/${CONF}-universal/ppsdk.framework"  "sdk.lib/"


# ------------------------------------------------------------------
#   Sampleを含めてアーカイブする
# ------------------------------------------------------------------
if [ -d sdk ]
then
    rm -R sdk
fi

mkdir sdk
cp -R ppsdk_sample_app.xcodeproj    sdk
cp -R ppsdk_sample_app              sdk
cp -R sdk.lib                       sdk

rm -R sdk/ppsdk_sample_app.xcodeproj/xcuserdata
rm -R sdk/ppsdk_sample_app.xcodeproj/project.xcworkspace/xcuserdata

sed -i -e "s/: Kazuo Ide ............//g" sdk/ppsdk_sample_app.xcodeproj/project.pbxproj
sed -i -e "s/35fc9579-aa52-4cd4-b467-d403aafd41da//g" sdk/ppsdk_sample_app.xcodeproj/project.pbxproj


tar cvfz sdk.tar.gz sdk

rm -R sdk



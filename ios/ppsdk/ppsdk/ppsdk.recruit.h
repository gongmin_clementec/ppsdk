//
//  ppsdk.h
//  ppsdk
//
//  Created by clementec on 2017/05/30.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ppsdk.
FOUNDATION_EXPORT double ppsdkVersionNumber;

//! Project version string for ppsdk.
FOUNDATION_EXPORT const unsigned char ppsdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ppsdk/PublicHeader.h>


#import <ppsdk.recruit/PPBridge.h>
#import <ppsdk.recruit/PPTextUtils.h>
#import <ppsdk.recruit/PPHttpServerBridge.h>

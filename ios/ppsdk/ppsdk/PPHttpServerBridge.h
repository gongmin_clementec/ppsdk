//
//  PPHttpServerBridge.h
//  ppsdk
//
//  Created by tel on 2018/10/24.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, LogLevel)
{
    LogLevelError,
    LogLevelWarning,
    LogLevelInformation,
    LogLevelDebug
};

@interface PPHttpServerBridge : NSObject

+ (PPHttpServerBridge*)sharedInstance;

- (void)beginWithDocumentRoot:(NSString*)documentRoot
                   portNumber:(int)portNumber
               isUseLocalhost:(BOOL)isUseLocalhost;

- (void)updateConfigure:(NSString*)desc
                   json:(NSString*)json;

- (void)suspend;
- (void)resume;
- (void)forceAbortJobs;

- (void)waitUntilServerRunning:(void (^)(BOOL))completionHandler;
- (void)waitUntilNoJobs:(void (^)(BOOL))completionHandler;
- (void)waitUntilJobWakeup:(void (^)(BOOL))completionHandler;


- (void)log:(LogLevel)logLevel message:(NSString*)message;

@end

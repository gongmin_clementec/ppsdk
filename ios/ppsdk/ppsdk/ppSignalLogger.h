//
//  SignalLogger.h
//  ppsdk
//
//  Created by tel on 2019/02/16.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#ifndef SignalLogger_h
#define SignalLogger_h

@interface ppSignalLogger : NSObject
+ (ppSignalLogger*)sharedInstance;
@end

#endif /* SignalLogger_h */

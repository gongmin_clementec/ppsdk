//
//  SDEAAccessory.hpp
//  ppsdk
//
//  Created by tel on 2019/02/18.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__hpp_SDEAAccessory__)
#define __hpp_SDEAAccessory__

#include "SerialDevice.h"

class SDEAAccessory : public SerialDevice
{
private:
    boost::recursive_mutex  m_easession_close_mtx;
    id                      m_easession;  /* EASessionInternal */
    pprx::subject<bytes_sp> m_subjectRead;

    struct cache_t
    {
        bytes_t buffer;
        size_t  ptr;
        cache_t() : ptr(0) {}
    };
    using cache_sp = boost::shared_ptr<cache_t>;
    
    std::deque<cache_sp>    m_writeCache;
    boost::recursive_mutex  m_writeCache_mtx;


    void setErrorAndCloseSerialDevice(std::exception_ptr err);


protected:
    void readCacheData();
    void writeCacheData();

    
public:
            SDEAAccessory();
    virtual ~SDEAAccessory();
    
    virtual auto open(const_json_t param) -> pprx::observable<pprx::unit>;
    virtual auto close() -> pprx::observable<pprx::unit>;
    
    virtual auto getReadObservable() -> pprx::observable<bytes_sp>;
    virtual void writeAsync(bytes_sp data);
    virtual const_json_t getDeviceInfo();

    
    void    handleEAEvent(const void* nsstream, unsigned long eventCode);
    void    handleEADidDisconnect();
};

#endif /* !defiend(__hpp_SDEAAccessory__) */

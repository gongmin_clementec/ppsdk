//
//  PPTextUtils.mm
//  ppsdk
//
//  Created by tel on 2017/08/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import "PPTextUtils.h"
#include "TextUtils.h"

@implementation PPTextUtils

+ (NSArray<NSString*>*)split:(NSString*)src
       rowCountWithHalfwidth:(int)rowCountWithHalfwidth
               textAlignment:(PPTextUtilsTextAlignment)textAlignment
                     maxLine:(int)maxLine
{
    TextUtils::TextAlignment ta;
    switch(textAlignment){
        default:
        case PPTextUtilsTextAlignmentBad:    ta = TextUtils::TextAlignment::Bad;    break;
        case PPTextUtilsTextAlignmentNone:   ta = TextUtils::TextAlignment::None;   break;
        case PPTextUtilsTextAlignmentLeft:   ta = TextUtils::TextAlignment::Left;   break;
        case PPTextUtilsTextAlignmentRight:  ta = TextUtils::TextAlignment::Right;  break;
        case PPTextUtilsTextAlignmentCenter: ta = TextUtils::TextAlignment::Center; break;
    }
    
    auto&& st = TextUtils::split([src UTF8String], rowCountWithHalfwidth, ta, maxLine);
    
    auto result = [[NSMutableArray<NSString*> alloc] init];
    
    for(auto t : st){
        [result addObject:[NSString stringWithUTF8String:t.c_str()]];
    }
    return result;
}



+ (int)countWithHalfWidth:(NSString *)src {
    
    const char *c = [src UTF8String];
    auto&& u32src  = TextUtils::utf8To32(c);
    int     count = 0;
    for(auto ch : u32src){
        count += TextUtils::isHalfwidthChar(ch) ? 1 : 2;
    }
    
    return count;
    
}


@end

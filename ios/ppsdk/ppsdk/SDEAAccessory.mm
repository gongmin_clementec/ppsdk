//
//  SDEAAccessory.mm
//  ppsdk
//
//  Created by tel on 2019/02/18.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>

#include "SDEAAccessory.hpp"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/asio.hpp>
#pragma GCC diagnostic pop
#include <boost/algorithm/hex.hpp>
#include <boost/regex.hpp>


@interface ppEASessionInternal : NSObject <EAAccessoryDelegate, NSStreamDelegate>
{
    SDEAAccessory::wp                           sdeaa_;
    boost::shared_ptr<boost::asio::io_service>  ioservice_;
}
@property (nonatomic,   strong) EASession*      session_;
@property (atomic,      weak)   NSRunLoop*      runloop_;
@property (nonatomic,   strong) NSThread*       thread_;
@property (nonatomic,   strong) EAAccessory*    accessory_;
@end


@implementation ppEASessionInternal

- (id)init:(SDEAAccessory::wp)sdeaa
{
    self = [super init];
    if(self != nil){
        sdeaa_ = sdeaa;
        ioservice_ = nullptr;
        self.runloop_ = nil;
        self.thread_ = nil;
        self.accessory_ = nil;
    }
    return self;
}

- (void)threadMain:(id)userInfo
{
    LogManager_Function();
    /*
     * inputStream / outputStream はworker threadでdelegateされる。
     * しかし、EASession の disconnect は main threadでdelegateされる。
     * すると、outputStreamの書き込み可能通知（NSStreamEventHasSpaceAvailable）受け取り
     * writeが完了するまでの間でdisconnectが発生するとSIGPIPEが発信され
     * アプリケーションが終了するため、SIGPIPEをマスクする。
     * これにより、writeは-1で失敗するようになる。
     * signalマスクはスレッド単位になるため、ここでマスクを設定する。
     *
     */
    ::signal(SIGPIPE, SIG_IGN);
    self.runloop_ = [NSRunLoop currentRunLoop];
    
    [self doRunLoop];
    ioservice_->run();
    ioservice_.reset();
    self.runloop_ = nil;
}

- (boost::shared_ptr<boost::asio::io_service>) ioService
{
    return ioservice_;
}

- (void) doRunLoop
{
    @autoreleasepool{
        [self.runloop_ runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    auto ios = ioservice_;
    auto _SELF = self;
    ios->post([ios, _SELF](){
        [_SELF doRunLoop];
    });
}

- (bool)open:(const char*)name protocol:(const char*)protocol serial:(const char*)serial
{
    LogManager_Function();
    LogManager_AddInformationF("name : \"%s\", protocol \"%s\", serial \"%s\"", name % protocol % serial);
    
    boost::regex exprName       = boost::regex(name);
    boost::regex exprProtocol   = boost::regex(protocol);
    boost::regex exprSerial     = boost::regex(serial);
    
    NSArray* accessoryList = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
    for(EAAccessory* accessory in accessoryList){
        LogManager_AddInformationF("name:             %s", [accessory.name UTF8String]);
        LogManager_AddInformationF("serialNumber:     %s", [accessory.serialNumber UTF8String]);
        LogManager_AddInformationF("firmwareRevision: %s", [accessory.firmwareRevision UTF8String]);
        LogManager_AddInformationF("hardwareRevision: %s", [accessory.hardwareRevision UTF8String]);
        LogManager_AddInformationF("modelNumber:      %s", [accessory.modelNumber UTF8String]);
        LogManager_AddInformationF("manufacturer:     %s", [accessory.manufacturer UTF8String]);
        
        bool bFound = true;
        
        {
            std::string aName = [accessory.name UTF8String];
            if(!boost::regex_match(aName, exprName)){
                bFound = false;
                LogManager_AddInformation("name -> NG");
            }
            else{
                LogManager_AddInformation("name -> OK");
            }
        }
        {
            std::string aSerial = [accessory.serialNumber UTF8String];
            if(!boost::regex_match(aSerial, exprSerial)){
                bFound = false;
                LogManager_AddInformation("serial -> NG");
            }
            else{
                LogManager_AddInformation("serial -> OK");
            }
        }
        NSString* ACCESSORY_PROTOCOL = nil;
        {
            for(int i = 0; i < [accessory.protocolStrings count]; i++){
                std::string aProtocol = [[accessory.protocolStrings objectAtIndex:i] UTF8String];
                if(boost::regex_match(aProtocol, exprProtocol)){
                    ACCESSORY_PROTOCOL = [accessory.protocolStrings objectAtIndex:i];
                }
            }
            if(ACCESSORY_PROTOCOL == nil){
                bFound = false;
                LogManager_AddInformation("protocol -> NG");
            }
            else{
                LogManager_AddInformation("protocol -> OK");
            }
        }
        
        
        if (bFound) {
            LogManager_AddInformation("found");
            
            ioservice_.reset(new boost::asio::io_service());
            self.runloop_ = nil;
            self.thread_ = [[NSThread alloc] initWithTarget:self selector:@selector(threadMain:) object:nil];
            [self.thread_ start];
            while(self.runloop_ == nil) {
                [NSThread sleepForTimeInterval:0.00001];
            }
            
            boost::mutex mtx;
            mtx.lock();
            ioservice_->dispatch([&](){
                auto session = [[EASession alloc] initWithAccessory:accessory
                                                        forProtocol:ACCESSORY_PROTOCOL];
                
                if (session != nil)
                {
                    self.session_ = session;
                    
                    [accessory setDelegate:self];
                    self.accessory_ = accessory;
                    
                    NSRunLoop* runloop = self.runloop_;
                    
                    [[self.session_ inputStream] setDelegate:self];
                    [[self.session_ inputStream] scheduleInRunLoop:runloop
                                                           forMode:NSDefaultRunLoopMode];
                    [[self.session_ inputStream] open];
                    
                    [[self.session_ outputStream] setDelegate:self];
                    [[self.session_ outputStream] scheduleInRunLoop:runloop
                                                            forMode:NSDefaultRunLoopMode];
                    [[self.session_ outputStream] open];
                }
                else
                {
                }
                mtx.unlock();
            });
            mtx.lock();
            mtx.unlock();
            if(self.session_ != nil) break;
        }
    }
    
    if(self.session_ == nil){
        LogManager_AddError("device not found.");
        /* ioservice_ と self.runloop_ は終了待機が必要なので、thread側で削除しなければならない点に注意すること */
        if(ioservice_){
            ioservice_->stop();
        }
        self.thread_ = nil;
        self.accessory_ = nil;
    }
    
    return self.session_ != nil;
}

-(void) close
{
    LogManager_Function();
    if (self.session_)
    {
        ioservice_->dispatch([=](){
            NSRunLoop* runloop = self.runloop_;
            
            [[self.session_ inputStream] setDelegate:nil];
            [[self.session_ inputStream] removeFromRunLoop:runloop
                                                   forMode:NSDefaultRunLoopMode];
            [[self.session_ inputStream] close];
            
            [[self.session_ outputStream] setDelegate:nil];
            [[self.session_ outputStream] removeFromRunLoop:runloop
                                                    forMode:NSDefaultRunLoopMode];
            [[self.session_ outputStream] close];
            
            [[self.session_ accessory] setDelegate:nil];
            
            self.session_ = nil;
            self.accessory_ = nil;

            ioservice_->stop(); /* これにより run() が終了して、threadMainが終わる */

            sdeaa_.reset();
            self.runloop_ = nil;
            self.thread_ = nil;
        });
    }
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
//    LogManager_Function();
    auto locked_sdeaa = sdeaa_.lock();
    if(locked_sdeaa){
        LogManager_AddInformation("Disconnected by handleEADidDisconnect");
        auto DERR_SDEAA = boost::dynamic_pointer_cast<SDEAAccessory>(locked_sdeaa);
        DERR_SDEAA->handleEAEvent((__bridge_retained void*)aStream, (NSStreamEvent)eventCode);
    }
}

- (void)accessoryDidDisconnect:(EAAccessory *)accessory
{
    LogManager_Function();
    if(self.accessory_ == accessory){
        auto locked_sdeaa = sdeaa_.lock();
        if(locked_sdeaa){
            LogManager_AddInformation("Disconnected by handleEADidDisconnect");
            auto DERR_SDEAA = boost::dynamic_pointer_cast<SDEAAccessory>(locked_sdeaa);
            DERR_SDEAA->handleEADidDisconnect();
        }
    }
}

@end

SDEAAccessory::SDEAAccessory() :
    m_easession(nil)
{
    LogManager_Function();
}

/* virtual */
SDEAAccessory::~SDEAAccessory()
{
    LogManager_Function();
    close().subscribe([](auto){}, [](auto){}, [](){});

}

/* virtual */
auto SDEAAccessory::open(const_json_t param)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        LogManager_Scoped("SDEAAccessory::open() -> create observable");
        @try{
            auto eas = [[ppEASessionInternal alloc] init:shared_from_this()];
            
            if(![eas open:param.get<std::string>("name").c_str()
                 protocol:param.get<std::string>("protocol").c_str()
                   serial:param.get<std::string>("serial").c_str()]){
                LogManager_AddError("m_subjectRead.get_subscriber().on_error(makeOpenError())");
                m_subjectRead.get_subscriber().on_error(makeOpenError());
                LogManager_AddError("s.on_error(makeOpenError())");
                s.on_error(makeOpenError());
            }
            else{
                LogManager_AddInformation("SDEAAccessory::open() success");
                m_easession = eas;
                s.on_next(pprx::unit());
                s.on_completed();
            }
        }
        @catch(...)
        {
            LogManager_AddError("catch exception");
            LogManager_AddError("s.on_error(makeUnknownError())");
            s.on_error(makeUnknownError());
        }
    }).as_dynamic()
    .zip(
        // pprx::observable<>::createでopenの完了を待ち、
        // この節で最初の電文受信が完了を待ち、
        // 両方の節の処理の完了を待ってから次の節に進む。
        m_subjectRead.get_observable()
        .flat_map([=](auto){
            LogManager_AddInformation("m_subjectRead received.");
            return pprx::just(pprx::unit()).as_dynamic();
        })
    )
    .take(1)
    .flat_map([=](auto) {
        LogManager_AddInformation("open completed.");
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto SDEAAccessory::close()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto rsubject = m_subjectRead;
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        LogManager_Scoped("SDEAAccessory::close() -> create observable");
        boost::unique_lock<boost::recursive_mutex> lock(m_easession_close_mtx);

        /* 値の発行はブロッキング期間中で、かつ、その後、デストラクタに突入する可能性があるため、遅延させる。
         本来的にはSDEAAccessoryをenable_shared_from_thisにするべきだと思われる。
         */
        
        if(m_easession == nil){
            LogManager_AddInformation("m_easession == nil");
            wp THIS = shared_from_this();            
            BaseSystem::instance().post([THIS, s](){
                auto locked_this = THIS.lock();
                if(locked_this){
                    LogManager_Scoped("defered emit data");
                    s.on_next(pprx::unit());
                    s.on_completed();
                }
            });       
            return;
        }
        
        @try{
            [m_easession close];
            m_easession = nil;
            LogManager_Scoped("m_subjectRead.get_subscriber().on_completed()");
            wp THIS = shared_from_this();
            BaseSystem::instance().post([THIS, s, rsubject](){
                auto locked_this = THIS.lock();
                if(locked_this){
                    rsubject.get_subscriber().on_completed();
                    s.on_next(pprx::unit());
                    s.on_completed();
                }
            });
        }
        @catch(...)
        {
            LogManager_AddError("catch exception");
            LogManager_AddError("s.on_error(makeUnknownError())");
            wp THIS = shared_from_this();
            BaseSystem::instance().post([THIS, s](){
                auto locked_this = THIS.lock();
                if(locked_this){
                    auto DERR_THIS = boost::dynamic_pointer_cast<SDEAAccessory>(locked_this);
                    s.on_error(DERR_THIS->makeUnknownError());
                }
            });
        }
    });
}

/* virtual */
auto SDEAAccessory::getReadObservable()
 -> pprx::observable<bytes_sp>
{
    return m_subjectRead.get_observable();
}

/* virtual */
void SDEAAccessory::writeAsync(bytes_sp data)
{
    LogManager_Function();
    if(m_easession == nullptr){
        LogManager_AddWarning("m_easession == nullptr");
        return;
    }

    auto cache = boost::make_shared<cache_t>();
    cache->buffer = *data;
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_writeCache_mtx);
        m_writeCache.push_back(cache);
    }

    ppEASessionInternal* eas = m_easession;
    auto ios = [eas ioService];
    if(ios){
        wp THIS = shared_from_this();
        ios->post([THIS](){
            auto locked_this = THIS.lock();
            if(locked_this){
                auto DERR_THIS = boost::dynamic_pointer_cast<SDEAAccessory>(locked_this);
                DERR_THIS->writeCacheData();
            }
        });
    }
    else{
        LogManager_AddWarning("ios not exists.");
    }
}

const_json_t SDEAAccessory::getDeviceInfo()
{
    LogManager_Function();
    ppEASessionInternal* eas = m_easession;
    auto accessory = eas.accessory_;
    
    json_t j;
    std::string model = (boost::format("%s/%s")
                % [accessory.name UTF8String]
                % [accessory.modelNumber UTF8String]).str();
    std::string version = (boost::format("%s/%s")
                  % [accessory.hardwareRevision UTF8String]
                  % [accessory.firmwareRevision UTF8String]).str();
    std::string serial = [accessory.serialNumber UTF8String];
    
    j.put("model", model);
    j.put("version", version);
    j.put("serial", serial);

    return j.as_readonly();
}

void SDEAAccessory::readCacheData()
{
    LogManager_Function();
    if(m_easession == nil){
        LogManager_AddWarning("m_easession == nullptr");
        return;
    }
    
    ppEASessionInternal* eas = m_easession;
    if(eas == nil){
        LogManager_AddWarning("eas == nil");
        return;
    }

    auto is = [[eas session_] inputStream];
    if(is == nil){
        LogManager_AddWarning("is == nil");
        return;
    }

    while([is hasBytesAvailable]){
        auto receive = boost::make_shared<bytes_t>(4096);
        auto length = [is read:reinterpret_cast<uint8_t*>(receive->data())
                     maxLength:receive->size()];
            if(length > 0){
            receive->resize(length);

            std::string sresult;
            boost::algorithm::hex(receive->begin(), receive->begin() + length, std::back_inserter(sresult));
            LogManager_AddDebugF("READ EAA : %ld - %s", length % sresult);
        
            wp THIS = shared_from_this();
            BaseSystem::instance().post([receive, THIS](){
                auto locked_this = THIS.lock();
                if(locked_this){
                    LogManager_Scoped("defered emit data");
                    auto DERR_THIS = boost::dynamic_pointer_cast<SDEAAccessory>(locked_this);
                    DERR_THIS->m_subjectRead.get_subscriber().on_next(receive);
                }
            });
        }
    }
}

void SDEAAccessory::writeCacheData()
{
//    LogManager_Function();
    if(m_easession == nil){
        LogManager_AddWarning("m_easession == nullptr");
        return;
    }

    ppEASessionInternal* eas = m_easession;
    if(eas == nil){
        LogManager_AddWarning("eas == nil");
        return;
    }
    
    auto os = [[eas session_] outputStream];
    if(os == nil){
        LogManager_AddWarning("os == nil");
        return;
    }
    
    if([os hasSpaceAvailable]){
        boost::unique_lock<boost::recursive_mutex> lock(m_writeCache_mtx);
        
        while((m_writeCache.size() > 0)){
            
            cache_sp cache = m_writeCache.front();
            
            const uint8_t* wp = reinterpret_cast<const uint8_t*>(cache->buffer.data() + cache->ptr);
            const size_t   len = cache->buffer.size() - cache->ptr;
            
            auto written = [os write:wp maxLength:len];
            if(written == 0){
                LogManager_AddWarning("written == 0 -> retry");
                break;
            } else if(written < 0){
                setErrorAndCloseSerialDevice(makeDataError());
                LogManager_AddError("written < 0");
                break;
            }
            
            if(written > 1024){
                LogManager_AddDebugF("WRITE EAA : %ld - over 1024 probably f/w update", written);
            }
            else{
                std::string sresult;
                boost::algorithm::hex(cache->buffer.begin() + cache->ptr, cache->buffer.begin() + len, std::back_inserter(sresult));
                LogManager_AddInformationF("WRITE EAA : %ld - %s", written % sresult);
            }
            
            if(len == written){
                m_writeCache.pop_front();
            }
            else{
                cache->ptr += written;
                break;
            }
        }
    }
    else{
        bool bRetry = false;
        {
            boost::unique_lock<boost::recursive_mutex> lock(m_writeCache_mtx);
            bRetry = m_writeCache.size() > 0;
        }
        if(bRetry){
            ppEASessionInternal* eas = m_easession;
            auto ios = [eas ioService];
            if(ios){
                wp THIS = shared_from_this();
                ios->post([THIS](){
                    auto locked_this = THIS.lock();
                    if(locked_this){
                        auto DERR_THIS = boost::dynamic_pointer_cast<SDEAAccessory>(locked_this);
                        DERR_THIS->writeCacheData();
                    }
                });
            }
            else{
                LogManager_AddWarning("ios not exists.");
            }
        }
    }
}

void SDEAAccessory::setErrorAndCloseSerialDevice(std::exception_ptr err)
{
    LogManager_Function();
    m_subjectRead.get_subscriber().on_error(err);
    close().subscribe([](auto){}, [](auto){}, [](){});
}

void SDEAAccessory::handleEAEvent(const void* nsstream, unsigned long eventCode)
{
//    LogManager_Function();
    const NSStreamEvent errorMask = NSStreamEventErrorOccurred |  NSStreamEventEndEncountered;
    
    if((eventCode & errorMask) != 0){
        setErrorAndCloseSerialDevice(makeUnknownError());
    }
    
    if((eventCode & NSStreamEventHasBytesAvailable) != 0){
        readCacheData();
    }
    
    if((eventCode & NSStreamEventHasSpaceAvailable) != 0){
        writeCacheData();
    }
}

void SDEAAccessory::handleEADidDisconnect()
{
    LogManager_Function();
    wp THIS = shared_from_this();

    BaseSystem::instance().post([THIS](){
        auto locked_this = THIS.lock();
        if(locked_this){
            LogManager_AddInformation("Disconnected by handleEADidDisconnect");
            auto DERR_THIS = boost::dynamic_pointer_cast<SDEAAccessory>(locked_this);
            DERR_THIS->setErrorAndCloseSerialDevice(DERR_THIS->makeDisconnectedError());
            
            DERR_THIS->close().subscribe([](auto){}, [](auto){}, [](){});
            {
                LogManager_Scoped("m_subjectRead.get_subscriber().on_completed()");
                DERR_THIS->m_subjectRead.get_subscriber().on_completed();
            }
        }
    });
}

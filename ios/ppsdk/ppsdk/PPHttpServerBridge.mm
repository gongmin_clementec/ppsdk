//
//  PPHttpServerBridge.m
//  ppsdk
//
//  Created by tel on 2018/10/24.
//  Copyright © 2018年 Clementec Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPHttpServerBridge.h"
#import "ppSignalLogger.h"
#include "PPHttpServer.h"
#include "PPConfig.h"
#include "KeyChainAccess.hpp"


@interface PPHttpServerBridge()

@property() boost::shared_ptr<PPHttpServer> http_;

@end


@implementation PPHttpServerBridge

+ (PPHttpServerBridge*)sharedInstance
{
    static dispatch_once_t      onceToken;
    static PPHttpServerBridge*  staticInstance = nil;
    
    dispatch_once(&onceToken, ^{
        staticInstance = [[PPHttpServerBridge alloc] init];
    });
    
    return staticInstance;
}

- (id)init
{
    self = [super init];
    if(self == nil) return nil;

    [ppSignalLogger sharedInstance];
    self.http_ = boost::make_shared<PPHttpServer>();
    self.http_->initialize();
    
    return self;
}

- (void)beginWithDocumentRoot:(NSString*)documentRoot
                   portNumber:(int)portNumber
               isUseLocalhost:(BOOL)isUseLocalhost
{
    self.http_->begin([documentRoot UTF8String], portNumber, isUseLocalhost);

}

- (void)updateConfigure:(NSString*)desc
                   json:(NSString*)json
{
    PPConfig::instance().updateConfigure([desc UTF8String], [json UTF8String]);
    if(PPConfig::instance().get<bool>("debug.bResetTMSPasswords")){
        KeyChainAccess::deleteAllTmsPassword();
    }
}

- (void)suspend
{
    LogManager_Function();
    BaseSystem::instance().post([=](){
        self.http_->suspend();
    });
}

- (void)resume
{
    LogManager_Function();
    BaseSystem::instance().post([=](){
        self.http_->resume();
    });
}

- (void)waitUntilServerRunning:(void (^)(BOOL))completionHandler
{
    LogManager_Function();
    self.http_->waitUntilServerRunning([=](bool bSuccess){
        completionHandler(bSuccess ? YES : NO);
    });
}

- (void)forceAbortJobs
{
    LogManager_Function();
    BaseSystem::instance().post([=](){
        self.http_->forceAbortJobs();
    });
}

- (void)waitUntilNoJobs:(void (^)(BOOL))completionHandler
{
    LogManager_Function();
    self.http_->waitUntilNoJobs([=](bool bSuccess){
        completionHandler(bSuccess ? YES : NO);
    });
}

- (void)waitUntilJobWakeup:(void (^)(BOOL))completionHandler
{
    LogManager_Function();
    self.http_->waitUntilJobWakeup([=](bool bSuccess){
        completionHandler(bSuccess ? YES : NO);
    });
}

- (void)log:(LogLevel)logLevel message:(NSString*)message
{
    switch(logLevel){
        case LogLevelError:         LogManager::instance().add(LogManager::EType::Error      , "(EXTERNAL)", "-", 0, [message UTF8String]); break;
        case LogLevelWarning:       LogManager::instance().add(LogManager::EType::Warning    , "(EXTERNAL)", "-", 0, [message UTF8String]); break;
        case LogLevelInformation:   LogManager::instance().add(LogManager::EType::Information, "(EXTERNAL)", "-", 0, [message UTF8String]); break;
        case LogLevelDebug:         LogManager::instance().add(LogManager::EType::Debug      , "(EXTERNAL)", "-", 0, [message UTF8String]); break;
        default: break;
    }
}

@end


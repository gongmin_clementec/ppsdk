//
//  PPBridge.mm
//  ppsdk
//
//  Created by Clementec on 2019/06/15.
//
//

#if defined(RECRUIT)

#import "PPBridge.h"
#include "Json.h"
#include "IOSUtilities.hpp"

@interface ppRecruit : NSObject
@end

@implementation ppRecruit

- (id) init {
    self = [super init];
    if(self == nil) return nil;
    
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];

    [nc addObserver: self
           selector: @selector(updateJMupsOpenTerminalResponse:)
               name: @"ppNotification:updateJMupsOpenTerminalResponse"
             object: nil];

    [nc addObserver: self
           selector: @selector(updateMiuraConnectSerialId:)
               name: @"ppNotification:updateMiuraConnectSerialId"
             object: nil];

    return self;
}

- (void) updateJMupsOpenTerminalResponse: (NSNotification*)notification {
    id response = [notification.userInfo objectForKey:@"response"];
    [PPBridge.sharedInstance applyJMupsOpenTerminalResponse: response];
}

- (void) updateMiuraConnectSerialId: (NSNotification*)notification {
    NSString* serial = [notification.userInfo objectForKey:@"serial"];
    if(serial.length  == 0)
        serial = @".*";
    basic_json_t<char> j;
    j.put("miura.ea.serial", IOSUtilities::convert(serial));
    [PPBridge.sharedInstance updateConfigure: @"updateMiuraConnectSerialId"
                                        json: IOSUtilities::convert(j.str())];
}



@end

static ppRecruit* recruit = [[ppRecruit alloc] init];

#endif /* defined(RECRUIT) */

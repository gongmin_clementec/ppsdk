
# iPhoneモバイル決済端末⽤ PPSDK IF仕様書

> ##### 第1.2.1.0501版
> ##### 株式会社クレメンテック
> ##### 2020年05⽉01⽇

- [1. PPSDK IF仕様](#1-ppsdk-if仕様)
  - [1.1. 概要](#11-概要)  
  - [1.2. PPBridgeインスタンス](#12-ppbridgeインスタンス)
  - [1.3. 業務処理](#13-業務処理)
  - [1.4. PPBridge.executeJob](#14-ppbridgeexecutejob)
  - [1.5. トレーニングモードの切替](#15-トレーニングモードの切替)   
    - [1.5.1. トレーニングモードフラグの設定](#151-トレーニングモードフラグの設定)
    - [1.5.2. トレーニングモード電子サイン使用](#152-トレーニングモード電子サイン使用)
    - [1.5.3. クレジットSDKとの共通部分について](#153-クレジットsdkとの共通部分について取引未了リカバリ機能と開局データとm010接続シリアルについて)
- [2. onDispatch(PPDispatchCallback_t)](#2-ondispatchppdispatchcallback_t)
  - [2.1. R/W接続開始《通知》](#21-rw接続開始通知)
  - [2.2. R/W接続失敗によるリトライ実行確認](#22-rw接続失敗によるリトライ実行確認)
  - [2.3. カード読み込み待ち《通知》](#23-カード読み込み待ち通知)
  - [2.4. カード検出《通知》](#24-カード検出通知)
  - [2.5. カード再読み開始待ち](#25-カード再読み開始待ち)
  - [2.6. アプリケーション（AID）選択待ち](#26-アプリケーションaid選択待ち)
  - [2.7. 取引情報入力待ち](#27-取引情報入力待ち)
    - [2.7.1. operation内容](#271-operation内容)
    - [2.7.2. values内容](#272-values内容)
      - [2.7.2.1. values.typeの設定](#2721-valuestypeの設定)
      - [2.7.2.1.1. 金額](#27211-金額)
      - [2.7.2.1.2. 税その他](#27212-税その他)
      - [2.7.2.1.3. 銀聯送信日程](#27213-銀聯送信日程)
      - [2.7.2.1.4. 銀聯番号](#27214-銀聯番号)
      - [2.7.2.1.5. 銀聯業務区分](#27215-銀聯業務区分)
      - [2.7.2.1.6. 伝票番号](#27216-伝票番号)
      - [2.7.2.1.7. 承認番号](#27217-承認番号)
      - [2.7.2.1.8. クーポン](#27218-クーポン)
      - [2.7.2.1.9. 支払方法(クレジット専用)](#27219-支払方法クレジット専用)
      - [2.7.2.1.10. 支払方法設定値](#272110-支払方法設定値)
  - [2.8. 取消・返品業務について](#28-取消・返品業務について)
  - [2.9. 二重取引確認](#29-二重取引確認)
  - [2.10. PIN結果確認](#210-PIN結果確認)
  - [2.11. 業務中断不可《通知》](#211-業務中断不可通知)
  - [2.12. 取引確認処理終了《通知》](#212-取引確認処理終了通知)
  - [2.13. 電子サイン要求](#213-電子サイン要求)
    - [2.13.1. 暗号化処理を施す場合](#2131-暗号化処理を施す場合)
      - [2.13.1.1. 画像の変換について](#21311-画像の変換について)
    - [2.13.2. HTML5のCanvasを無変換で返却する場合](#2132-html5のcanvasを無変換で返却する場合)
  - [2.14. 電子サイン送信に失敗した場合のリトライ](#214-電子サイン送信に失敗した場合のリトライ)
  - [2.15. 印字開始前確認](#215-印字開始前確認)
  - [2.16. 伝票出力前確認](#216-伝票出力前確認)
  - [2.17. 印字時復帰可能エラー応答](#217-印字時復帰可能エラー応答)
  - [2.18. 印字終了《通知》](#218-印字終了通知)
  - [2.19. 業務結果表示](#219-業務結果表示)
  - [2.20. PIN入力開始《通知》](#220-pin入力開始通知)
  - [2.21. PIN入力終了《通知》](#221-pin入力終了通知)
  - [2.22. センター問い合わせ中《通知》](#222-センター問い合わせ中通知)
  - [2.23. マスターデータ更新確認中《通知》](#223-マスターデータ更新確認中通知)
  - [2.24. マスターデータ更新実行《通知》](#224-マスターデータ更新実行通知)
  - [2.25. マスターデータチェック実行](#225-マスターデータチェック実行)
  - [2.26. "SEE PHONE"に基づくカード活性化待ち](#226-see-phoneに基づくカード活性化待ち)
  - [2.27. Journal業務のパラメータ入力待ち](#227-journal業務のパラメータ入力待ち)
  - [2.28. 再印字業務のパラメータ入力待ち](#228-再印字業務のパラメータ入力待ち)
  - [2.29. 再印字業務の確認表示終了待ち](#229-再印字業務の確認表示終了待ち)
  - [2.30. ジャーナル再印字業務の確認表示終了待ち](#230-ジャーナル再印字業務の確認表示終了待ち)
  - [2.31. 処理開始待ち](#231-処理開始待ち)
  - [2.32. アクティベーション情報入力待ち](#232-アクティベーション情報入力待ち)
  - [2.33. サーバーで検索失敗《通知》](#233-サーバーで検索失敗通知)
    - [2.33.1. 注意点](#2331-注意点)
  - [2.34. TMSからの情報《通知》](#234-tmsからの情報通知)
  - [2.35. 端末サービス設定確認](#235-端末サービス設定確認)
  - [2.36. カード会社一覧](#236-カード会社一覧)
  - [2.37. カード先挿入検知](#237-カード先挿入検知)
  - [2.38. KID入力待ち](#238-kid入力待ち)
  - [2.39. KID入力成功《通知》](#239-kid入力成功通知)
  - [2.40. KID入力失敗《通知》](#240-kid入力失敗通知)
  - [2.41. カードリードが終了《通知》](#241-カードリードが終了通知)
  - [2.42. NFC取消情報入力待ち](#242-nfc取消情報入力待ち)
- [3. onDispatchCancel(PPDispatchCancelCallback_t)](#3-ondispatchcancelppdispatchcancelcallback_t)
- [4. onFinish(PPFinishCallback_t)](#4-onfinishppfinishcallback_t)
  - [4.1. 戻り値](#41-戻り値)
  - [4.2. エラーについて](#42-エラーについて)
- [5. コンフィグファイル](#5-コンフィグファイル)
  - [5.1. PPBridge.updateConfigure](#51-ppbridgeupdateconfigure)
    - [5.1.1. 入力パラメータ(desc)](#511-入力パラメータdesc)
    - [5.1.2. 入力パラメータ(json)](#512-入力パラメータjson)
  - [5.2. コンフィグファイル内容](#52-コンフィグファイル内容)
    - [5.2.1. hps項目](#521-hps項目)
      - [5.2.1.1. pinMessagesについて](#5211-pinmessagesについて)
    - [5.2.2. ios項目](#522-ios項目)
    - [5.2.3. bRecruit項目](#523-bRecruit項目)
    - [5.2.4. job項目](#524-job項目)
    - [5.2.5. debug項目](#525-debug項目)
    - [5.2.6. カードリーダーについて](#526-カードリーダーについて)
      - [5.2.6.1. miuraについて](#5261-miuraについて)
        - [5.2.6.1.1. contactlessのdefaultsとtable内容](#52611-contactlessのdefaultsとtable内容)
        - [5.2.6.1.2. creditのdefaultsとtable内容](#52612-creditのdefaultsとtable内容)
      - [5.2.6.2. bluepadについて](#5262-bluepadについて)
- [6. その他のAPI](#6-その他のAPI)
  - [6.1. PPBridge.abort](#61-ppbridgeabort)
  - [6.2. PPBridge.disconnectCardReader](#62-ppbridgedisconnectcardreader)
  - [6.3. PPBridge.isTrainingMode](#63-ppbridgeistrainingmode)
  - [6.4. PPBridge.checkMasterData](#64-ppbridgecheckmasterdata)
  - [6.5. PPBridge.clearCardReaderDisplay](#65-ppbridgeclearcardreaderdisplay)

# 1. PPSDK IF仕様

## 1.1. 概要

PPSDKは業務実行関数と２つのコールバックで構成されています。
業務実行関数は、業務を識別する文字列と、コールバック関数へのポインタをパラメータとして渡します。
業務実行関数は非同期で実行され、業務処理中に適宜コールバック関数が呼ばれます。

２つのコールバックのうち、ひとつめのコールバックはアプリケーション側への処理依頼（金額入力など）や通知（IC処理中・PIN入力中など）を行うため呼び出されます。

ふたつめのコールバックは業務終了時に呼び出されます。業務処理の結果はこのコールバックで得られます。また、業務処理中にエラーが発生した場合でも、このコールバックが呼び出されます。

なお、PPSDKはC++（一部Objective-C++）をベースに実装されております。したがって、Objective-CおよびSwiftからPPSDKを利用するためにはPPBridgeクラスを仲介します。

## 1.2. PPBridgeインスタンス

PPSDKはアプリケーションとのI/FクラスPPBridgeを仲介して使用します。  
PPBridgeはシングルトンでアプリケーション実行中に削除することはできません。  

## 1.3. 業務処理

業務はPPBridgeのexecuteJobを呼び出すことで開始します。業務呼び出し以降、アプリケーションは executeJob からのコールバックの指示に従うことで継続処理されます。

コールバックは onDispatch、onDispatchCancelとonFinishの三種類です。

1.  onDispatchは、アプリケーションの入力要求、およびアプリケーションへの通知になります。

2. onDispatchCancelは、何らかのエラーによりPPSDK側からアプリケーションに対して処理の中断を要求するもので、このコールバックを受けたら、速やかにonDispatchを終了してください。

3. onFinishは、業務終了で呼び出され、業務処理が成功した時のみならず、何らかのエラーにより失敗しても呼び出されます。アプリケーションはonFinishで業務終了を検出するまで、他の業務を開始しないでください。



## 1.4. PPBridge.executeJob

executeJobの呼び出し型は下記のとおりです。

```
- (void)executeJob:(NSString*)jobName
        onDispatch:(PPDispatchCallback_t)onDispatch
  onDispatchCancel:(PPDispatchCancelCallback_t)onDispatchCancel
          onFinish:(PPFinishCallback_t)onFinish;
```

コールバックの関数型は下記のとおりです。

```
typedef BOOL (^PPDispatchCallback_t)(NSString*, NSString**);
typedef void (^PPDispatchCancelCallback_t)();
typedef void (^PPFinishCallback_t)(NSString*);
```

jobNameで与えることができる文字列は下記のとおりです。
| 業務 | jobName文字列 |
|:-|:-|
| クレジット売上　　　　　　| "JMups.CreditSales" |
| クレジット取消・返品　　　| "JMups.CreditRefund" |
| クレジットオーソリ予約　　| "JMups.CreditPreApproval" |
| クレジット承認後売上　　　| "JMups.CreditAfterApproval" |
| クレジットカードチェック　| "JMups.CreditCardCheck" |
| 銀聯売上　　　　　　　　　| "JMups.UnionPaySales" |
| 銀聯取消・返品　　　　　　| "JMups.UnionPayRefund" |
| 銀聯オーソリ予約　　　　　| "JMups.UnionPayPreApproval" |
| 銀聯承認後売上　　　　　　| "JMups.UnionPayAfterApproval" |
| 銀聯オーソリ予約取消　　　| "JMups.UnionPayPreApprovalCancel" |
| 銀聯承認後売上取消　　　　| "JMups.UnionPayAfterApprovalRefund" |
| NFC売上　　　　　　　　　| "JMups.NFCSales" |
| NFC取消・返品　　　　　　| "JMups.NFCRefund" |
| マスターデータ更新　　　　| "JMups.NFCUpdateMasterData" |
| マスターデータチェック　　| "JMups.NFCCheckMasterData" |
| 集計　　　　　　　　　　　| "JMups.Journal" |
| 再印字　　　　　　　　　　| "JMups.ReprintSlip" |
| 開局　　　　　　　　　　　| "JMups.OpenTerminal" |
| クアクティベーション　　　| "JJMups.Activation" |
| ファームウェア更新　　　　| "Setting.ReaderFWUpdate" |
| ログ送信　　　　　　　　　| "Setting.SendLog" |
| トレーニングモード　　　　| "Setting.Training" |
| 端末情報　　　　　　　　　| "Setting.TerminalInfo" |
| カード会社一覧　　　　　　| "Setting.Issuers" |
| 端末サービス　　　　　　　| "Setting.Service" |
|

## 1.5. トレーニングモードの切替

PPSDKにはトレーニングモード機能があります。
トレーニングモードをONに設定すると、以降、すべての業務はトレーニングモードで動作します。
トレーニングモードのON/OFFの状態は、PPSDKのインスタンスが削除されるまで保持しています。


### 1.5.1. トレーニングモードフラグの設定

トレーニングモードのON/OFFを設定します。

```
- (void)setTrainingMode:(BOOL)bTrainigMode;
```

bTrainigModeにtrueを設定するとトレーニングモードON、falseを設定するとトレーニングモードOFFになります。
PPSDKのデフォルト値はfalseです。

### 1.5.2. トレーニングモード電子サイン使用

トレーニングモード中のサインを電子サインで行うか否かを設定します。

```
- (void)setUseDigiSignInTrainingMode:(BOOL)bUse;
```

bUseにtrueを設定すると電子サインを行い、falseを設定すると紙サインになります。（印字実行時にサイン枠が印字されます）
PPSDKのデフォルト値はfalseです。
NFCを利用したトレーニングモードの場合1万円以下の取引であれば、固定で電子サイン不要となります。


### 1.5.3. クレジットSDKとの共通部分について（取引未了リカバリ機能と開局データとM010接続シリアルについて）

J-Mups取引では、取引未了リカバリを実施するために取引状態をCookieにて保存する必要があります。
PPSDK単独利用の場合については、SDKでの内部処理で当該機能を実施しますが、
別提供のcoreFrameWork(クレジットSDK)を利用している場合は、SDK間で取引状態を共有するためにPPSDKでの業務実行時から開局終了までcoreFrameWork(クレジットSDK)のインスタンス(PokeposDocumentクラス)が生成されている必要があります。
また、coreFrameWork(クレジットSDK)を利用している場合は、coreFrameWork(クレジットSDK)の開局情報をPPSDKに自動で連携されます。
coreFrameWork(クレジットSDK)を利用している場合は、PPSDKの開局は不要です。
また、coreFrameWork(クレジットSDK)の共通部分として接続するM010端末シリアル指定をcoreFrameWorkの値を引き継ぐことができます。


# 2. onDispatch(PPDispatchCallback_t)

「onDispatch」の呼び出し型は下記のとおりです。
```
BOOL onDispatch(NSString* inJson, NSString** outJson);
```

1. 入力パラメータ(inJson)
 
    PPSDKからJSON文字列で指定されます。
    詳細については、各commandを参照してください。

2. 出力パラメータ(outJson)

    inJsonで受けた各commandの内容に沿って結果をJSON文字列にしてPPSDKに返却してください。
    出力パラメータ(outJson)で必須になってる項目がコールバックのレスポンスで設定されていなかった場合、OnFinishでInternalErrorが返却されます。
    ただし、必須になっていた場合でも返却値にfalseを指定した場合には、onDispatchのoutJsonを無視し、onFinish({status:"aborted"})で終了します。

3. 返却値(BOOL)

    inJsonで受けた各commandの処理を継続するか否かを指定します。
    trueを指定した場合には、処理を継続します。
    falseを指定した場合には、onDispatchのoutJsonを無視し、onFinish({status:"aborted"})で終了します。

ただし、「電子伝票保管（送信）に失敗した場合のリトライ」などfalseに指定しても"aborted"にならない場合があります。また、タイミングによっては他の原因でonFinishが呼び出される可能性あります。


下記よりPPSDKから返却される「onDispatch」のcommandの説明を記載します。


## 2.1. R/W接続開始《通知》

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "connectingToRW"
> }
> ```
> 出力パラメータ(outJson)<br/>
（なし）



## 2.2. R/W接続失敗によるリトライ実行確認

R/W接続失敗に対して、Trueを返せば自動で再度リトライが実行されます。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForRetryConnectingReader"
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bRetry" : true
> }
> ```
再試行する場合trueを設定します。
それ以外の場合、R/Wと接続のリトライが失敗エラー(PP_W0003)となります。



## 2.3. カード読み込み待ち《通知》

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "waitForCardRead"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.4. カード検出《通知》
IC、磁気、NFCストライプを検出した際に通知されます。
ICの場合には、UIにてプログレス表示を行って、次のonDispatchまで維持してください。
磁気ストライプ、NFCの場合は、すぐに次のonDispatchが入るので、UIにて特に何もする必要はありません。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "cardDetected"
>     "parameter" : {
>         "cardType" : "contact"
>     }
> }
> ```
|parameter.cardType|内容|
|:-|:-|
| contact　　　　　　　| ICカード　     |
| magneticStripe　　　| 磁気ストライプ |
|

> 出力パラメータ(outJson)<br/>
（なし）



## 2.5. カード再読み開始待ち
ICカードから磁気フォールバック、磁気カードからICフォールバック時に発生します。
アラート表示を行い、表示が終了したPPSDKに制御を戻してください。
その後、再度カード接続確認から処理が行われます。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForRetryCardRead",
>     "parameter" : {
>         "result": {
>             "detail": {
>                 "errorObject": {
>                      "errorCode": "T_W3126",
>                      "errorLevel": "WARN",
>                      "errorMessage": "ICチップ付カードを磁気リーダで読み込みました。↑ICカードリーダーを使用してください。",
>                      "errorType": "WARN",
>                      "printBugReport": "false"
>                 },
>                 "resultCode": "1"
>             },
>             "where": "hps"
>         },
>         "status": "error"
>     }
> }
> ```

> |parameter|内容|
> |:-|:-|
> | result     |    「外部向けＩ／Ｆ仕様書_10（共通編）」の「1.2 異常時のレスポンスパラメータ」を参照 ※Cookieで返却される部分については返却していません |
> |

<b>カード読み取りエラー（errorCode）一覧<b/>

> | errorCode | 内容 |
> |:-|:-|
> | T_W3126  | ICチップ付カードを磁気リーダで読み込みました。↑ICカードリーダーを使用してください。     | 
> | T_W3127  | ICカードと磁気カードで会員番号が異なります。↑同一のカードを使用してください。           | 
> | PP_W0000 | このカードはICでのお取引はできません。↑カードを抜いて、磁気カードリーダーを使用してください。|
> | PP_W0001 | このカードでのお取引はできません。↑カードを抜いて、別の取引方法に切替えてください。       | 
> | PP_W0002 | 電子サイン送信失敗。  | 
> | PP_W0003 | R/Wと接続のリトライが失敗しました。  | 
> | PP_W0004 | 取消対象取引検索で対象が無い状態です。  | 
> |

> 出力パラメータ(outJson) <br/>
（なし）



## 2.6. アプリケーション（AID）選択待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForApplicationSelect",
>     "parameter" : {
>         "applicationList" : [
>         {
>             "displayLabel" : "VISA CREDIT",
>             "aid" : "A0000000000"
>         },
>         {
>             "displayLabel" : "VISA DEBIT",
>             "aid" : "A0000000001"
>         }
>         ]
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "aid" : "A0000000000"
> }
> ```

| プロパティ名 | 変数型 | 必須 | 制限 | 機能 |
|:-|:-|:-:|:-|:-|
| aid | string   | ● | null | Application ID |
|



## 2.7. 取引情報入力待ち

> 入力パラメータ(inJson)
> ```js
> {
> 	"command": "waitForTradeInfo",
> 	"parameter": {
> 		"operation": "Input",
> 		"updatable": false,
> 		"values": 
>                 [{
>                     "type": "amount",
>                     "attribute": "required",
>                     "current": 0
>                  },
>                  {
>                     "type": "taxOtherAmount",
>                     "attribute": "required",
>                     "current": 0
>                  },
>                  {
>                     "type": "cupNo",
>                     "attribute": "required",
>                     "current": "000001"
>                  },
>                  {
>                     "type": "cupSendDate",
>                     "attribute": "required",
>                     "current": "1212010203"
>                  },
>                  {
>                     "type": "cupTradeDiv",
>                     "attribute": "readonly",
>                     "current": "1"
>                  },
>                  {
>                     "type": "coupon",
>                     "attribute": "optional",
>                     "current": "00000000000",
>                     "config": {
>                   「外部向けI／F仕様書_14（クーポン編））」の「2.1.3 I／F項目」の「（３）レスポンス」を参照　※Cookieで返却される部分については返却していません
>                      }
>                  }
>                  {
>                     "type" : "paymentWay",
>                     "attribute" : false,
>                     "current" : {
>                        "paymentWay" : "lump",
>                        "partitionTimes" : null,
>                        "firsttimeClaimMonth" : null,
>                        "bonusTimes" : null,
>                        "bonusMonth1" : null,
>                        "bonusAmount1" : null,
>                        "bonusMonth2" : null,
>                        "bonusAmount2" : null,
>                        "bonusMonth3" : null,
>                        "bonusAmount3" : null,
>                        "bonusMonth4" : null,
>                        "bonusAmount4" : null,
>                        "bonusMonth5" : null,
>                        "bonusAmount5" : null,
>                        "bonusMonth6" : null,
>                        "bonusAmount6" : null
>                      },
>                   "config" : {
>                   }
>               } ]
> 	 }
> }
> ```

| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| updatable | boolean    | ● | 更新可能か？ 出力パラメータ "operation" で "update" を指定できるかどうか |
| operation | string     | ● | 操作の種類、詳細は"operation" で説明 |
| values    |array< map> | ● | 設定項目 |
|

### 2.7.1. operation内容 
| プロパティ名 | 変数型  | 内容 |
|:-|:-|:-|:-|
| operation | string   | 操作名　"PreInput" ... 事前金額入力、"Input" ... トレード情報入力、"Search" ... トレード取引検索、"Confirm" ... トレード確認　 |
|

### 2.7.2. values内容
● ... 必須で返却される
▲ ... 返却されない場合が存在する

| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | 入力項目タイプ |
| attribute | string   | ● | 項目の属性 ”required", "optional", "readonly" |
| current   | -        | ▲ | 項目の現設定値（integer, string, boolean, map, null） |
| config    | map      | - | 項目設定のための情報 |
|


#### 2.7.2.1. values.typeの設定
● ... 必須で返却される
▲ ... 返却されない場合が存在する

##### 2.7.2.1.1. 金額　
values type = "amount" 共通

| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "amount" |
| attribute | string   | ● | 項目の属性 ”required", "readonly" |
| current   | integer  | ▲ | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| XX  | 1000   | 範囲：1 ~ 99999999 |
|

##### 2.7.2.1.2. 税その他
values type = "taxOtherAmount"　売上
この項目を無視してください。
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "taxOtherAmount" |
| attribute | string   | ● | 項目の属性 ”optional", "readonly" |
| current   | integer  | ▲ | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| XX  | 1000   | 範囲：1 ~ 99999999 |
|

##### 2.7.2.1.3. 銀聯送信日時
values type = "cupSendDate"　取消
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "cupSendDate" |
| attribute | string   | ● | 項目の属性 ”required", "readonly" |
| current   | string   | ▲ | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| "MMDDhhmmss"  | "0820143045"   | 8月20日14：30：45　→　"0820143045"|
|

##### 2.7.2.1.4. 銀聯番号
values type = "cupNo"　取消
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "cupNo" |
| attribute | string   | ● | 項目の属性 ”required", "readonly" |
| current   | string   | ▲ | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| "XXXXXX"  | "000001"   | 6桁の数字　|
|

##### 2.7.2.1.5. 銀聯業務区分
values type = "cupTradeDiv"　共通
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "cupTradeDiv" |
| attribute | string   | ● | 項目の属性, "readonly" |
| current   | string   | ● | 項目の現設定値 |
|

<b>"cupTradeDiv" に入る文字列<b/>
* "1" ... 銀聯取消
* "2" ... 銀聯返品
* "5" ... 銀聯オーソリ予約取消
* "6" ... 銀聯承認後売上取消
* "7" ... 銀聯承認後売上返品

##### 2.7.2.1.6. 伝票番号
values type = "slipNo"　取消
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "slipNo" |
| attribute | string   | ● | 項目の属性 ”required", "readonly" |
| current   | string   | ● | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| "XXXXX"  | "12345"   | 5桁の数字　|
|

##### 2.7.2.1.7. 承認番号
values type = "approvalNo"　承認後売上・取消
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "approvalNo" |
| attribute | string   | ● | 項目の属性 ”required", "readonly" |
| current   | string   | ▲ | 項目の現設定値 |
|

出力の形式について
| format | 例 | 備考 |
|:-|:-|:-|
| "XXXXXX"  | "000001"   | 6桁の数字　|
|

##### 2.7.2.1.8. クーポン
values type = "coupon"　売上
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "coupon" |
| attribute | string   | ● | 項目の属性 ”required",  "optional", "readonly" |
| current   | string   | ● | 項目の現設定値（couponId） |
| center    | map      | ● | 「外部向けI／F仕様書_14（クーポン編））」の「2.1.3 I／F項目」の「（３）レスポンス」を参照 　※Cookieで返却される部分については返却していません|
|

##### 2.7.2.1.9. 支払方法(クレジット専用)
values type = "paymentWay" クレジットの支払方法
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| type      | string   | ● | "paymentWay" |
| attribute | string   | ● | 項目の属性 ”required",  "optional", "readonly" |
| current   | string   | ● | 項目の現設定値（「支払方法設定値」参照） |
|

##### 2.7.2.1.10. 支払方法設定値
values.current支払方法設定値について
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
| paymentWay             | string    |  ●  | 支払方法定義文字列 |
| partitionTimes         | integer   |  -  | 1〜99      | 分割回数 |
| firsttimeClaimMonth    | integer   |  -  | 1〜12      | 初回請求月 |
| bonusTimes             | integer   |  -  | 1〜6       | ボーナス回数 |
| bonusMonth1            | integer   |  -  | 1〜12      | ボーナス月１ |
| bonusAmount1           | integer   |  -  | 最大7桁     | ボーナス金額１ |
| bonusMonth2            | integer   |  -  | 1〜12      | ボーナス月２ |
| bonusAmount2           | integer   |  -  | 最大7桁     | ボーナス金額２ |
| bonusMonth3            | integer   |  -  | 1〜12      | ボーナス月３ |
| bonusAmount3           | integer   |  -  | 最大7桁     | ボーナス金額３ |
| bonusMonth4            | integer   |  -  | 1〜12      | ボーナス月４ |
| bonusAmount4           | integer   |  -  | 最大7桁     | ボーナス金額４ |
| bonusMonth5            | integer   |  -  | 1〜12      | ボーナス月５ |
| bonusAmount5           | integer   |  -  | 最大7桁     | ボーナス金額５ |
| bonusMonth6            | integer   |  -  | 1〜12      | ボーナス月６ |
| bonusAmount6           | integer   |  -  | 最大7桁     | ボーナス金額６ |
|

|支払方法|定義文字列|
|:-|:-|
| 一括         | lump |
| 分割         | installment |
| ボーナス      | bonus |
| ボーナス併用   | bonusCombine |
| リボ         | revolving |
|

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "operation": "apply",
>     "results":{
>         "amount" : 1234,
>         "couponIndex" : "000000000"
>         "paymentWay"  : "installment"
>         "partitionTimes" : 3
>     }
> }
> ```

* "operation"について
入力内容を確定し、次の処理に遷移させる場合には"apply"、現状の入力内容で他の項目（クーポンなど）を更新させる場合には"update"。

* "results"について
以下の表を参照してください。

| "results"内プロパティ名 | 変数型 | 必須 | 制限 | 機能 |
|:-|:-|:-:|:-|:-|
| amount              | integer   | ▲ | -           | 金額 |
| taxOtherAmount      | integer   | ▲ | -           | 税・その他 |
| paymentWay          | string    | ▲ | -           | 支払方法、クレジット用。　支払方法定義文字列を参照してください。 |
| couponIndex         | string    | ▲ | [1-9][0-9]* | 選択したクーポンのインデックス（0〜）またはnull |
|

▲ ... ディスパッチのパラメータで"required"の場合には必須


## 2.8. 取消・返品業務について
取消・返品業務はのexecuteJobを呼び出すことで開始します。
業務呼び出し以降、onDispatchコールバックの指示に従うことで、伝票番号(slipNo)入力要求を入力する。
伝票番号を検索して引当が存在した場合は、金額を入力して、業務が処理されて、業務終了でonFinishを呼び出されます。
伝票番号を検索して、存在しない場合は、onFinishを呼び出されて、エラーで業務終了となります。

伝票番号検索時（operation"Search"の時、出力パラメータ(outJson)は以下のようになります。
> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "operation": "apply",
>     "results":{
>         "slipNo": "01234"
>     }
> }
> ```

+ 伝票番号が引き当てない場合、onFinishで返却値は以下のようになります。
> 入力パラメータ(inJson)
> ```js
> {
>     "status" : "error",
>     "result" : {
>         "whrere" : "internal",
>         "detail" : {
>            "errorObject": {
>                "errorCode": "PP_W0004",
>                "errorLevel": "WARN",
>                "errorMessage": "No Refund SLIPNO",
>                "errorType": "WARN"
>             },
>            "resultCode": "1"
>         },
>         "recruit":{
>            "errorCode": "PP_W0004",
>            "detail": "No Refund SLIPNO",
>            "hpsUrl": "cd0119.do?cD0119=aaa",
>            "bTradeCompletionFlag" : false
>        }
>     }
> }
> ```


## 2.9. 二重取引確認

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForConfirmDoubleTrade"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.10. PIN結果確認

PIN入力後発生します。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForPinResultConfirm",
>     "parameter": {
>             "pinResult" : "success"
>             "bOnetimeRemain": false
>         }
> }
> ```

| statusに入る文字列 | 内容 |
|:-|:-|
| success          | PIN成功   |
| PinFail-deny     | PIN誤り |
| PinFail-bypass   | PINバイパス失敗 |
| PinFail-over     | PIN入力回数が超過 |
|

* "bOnetimeRemain"について
trueの時、PIN入力回数残り一回となります。 

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bContinue" : true
> }
> ```
続きを行う場合にはtrue、終了する場合にはfalseを設定することができます。
falseを返却すると、業務は中断されます。（処理未了になる可能性があります。）



## 2.11. 業務中断不可《通知》
この通知以降はキャンセルができません。
また、この通知以降は業務タイムアウトは発生しません。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "disableAborting"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）

## 2.12. 取引確認処理終了《通知》 

JMupsへの取引確認が正常に終了した際に通知されます。
“parameter” の要素は “onFinish” 時の “result” と同じものです。

> 入力パラメータ(inJson)
> ```js
>{
>     "command" : "notice",
>     "message" : "tradeConfirmFinished",
>     "parameter": {
>         "hps" : {
>           「外部向けＩ／Ｆ仕様書_」の「Ｉ／Ｆ項目」の「正常時レスポンスパラメータ」を参照
>             （クレジットの場合 外部向けＩ／Ｆ仕様書_11（クレジット編）　3.1.3.1.	取引確認　/　銀聯の場合　外部向けＩ／Ｆ仕様書_12（銀聯編）：3.5.1.3.1. 取引確認、※ICの場合は、3.1.2.3.8. 2nd GENERATE ACコマンド、イシュアスクリプトコマンド(GAC後)実行結果検証）NFCの場合　外部向けＩ／Ｆ仕様書_22（NFCペイメント編）6.1.3.1.	NFC売上要求 ※Cookieで返却される部分については返却していません
>         }
>     }
>}
>```

> 出力パラメータ(outJson)<br/>
（なし）


> 入力（異常時・ネットワーク要因）
> ```js {
>      "status" : "error",
>      "result" : {
>          "whrere" : "network",
>          "bTradeIncompleted": flase,
>          "detail" : {
>              "code" : nnnn, ... integer (OSStatus)
>              "description" : "xxxx" ... string
>          }
>      }
> }
> ```

## 2.13. 電子サイン要求

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForDigiSign",
>     "parameter" : {
>         "hps": {
>             "modulus" : "NNNNNNNN ...",
>             "exponent" : "NNNNNNNN ....",
>             "amount" : 1234,
>             "strAmount" : "Total： USD 11.02"
>         }
>     }
> }
> ```
* "modulus", "exponent" は [PPBridge hexStringFromDigiSignImage:modulus:exponent] で使用する。
* "amount" は決済金額（クーポン適用時は適用後金額）。
* "strAmount" が表示する文字列は以下です。
* + クレジットの場合、"合計金額： X,XXX 円"
* + 銀聯の場合、"Total： X,XXX YEN"
* + DCC使用の場合、"Total：USD X,XXX.XX"  



### 2.13.1. 暗号化処理を施す場合

#### 2.13.1.1. 画像の変換について
出力（JSON）で返却すべき画像データの文字列は下記の関数で生成する。
```
- (NSString*)hexStringFromDigiSignImage:(CGImageRef)image
modulus:(NSString*)modulus
exponent:(NSString*)exponent;
```
modulusとexponentは入力（JSON）の値をそのまま渡す。

CFImageRefはバックグラウンドが白、サインは黒とし、は32BPPカラーでも、16bitグレーでも良い。
画像は 178 x 44 に縮小される。（元画像もこのアスペクト比が望ましい）

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "encryptedDigiSignData" : "NNNNNNN ..."
> }
> ```
"encryptedDigiSignData" は [PPBridge hexStringFromDigiSignImage:modulus:exponent] で生成した文字列を設定します。

"encryptedDigiSignData" 省略することができ、この場合、電子サインを行わず、伝票にサインする方式とする。（PPSDK側では電子サインを送付せず、取引完了へと流れる）

なお、電子サインをしたかどうかはアプリ側で状態を保持し、印字時にカード会社控えを出力するかどうかを判定する必要がある。
（電子サインを行ってもprintInfoにはカード会社控えが存在するため）

### 2.13.2. HTML5のCanvasを無変換で返却する場合
> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "html5RawCanvasDigiSignData" : {
>         "pixelType": "RGBA",
>         "depth": 8,
>         "width": WIDTH,
>         "height": HEIGHT,
>         "pixelData": "NNNNN...."
>     }
> }
> ```
"pixelType" は "RGBA" 固定です。
"depth" は 8 固定です。
"width" は横のピクセル数、"height" は縦のピクセル数です。
"pixelData" は Canvas から得られる byte[] の toString() で生成します。


## 2.14. 電子サイン送信に失敗した場合のリトライ
この dispatch は、電子サイン送信を失敗すると発生します。（電子サイン変換に失敗した場合空文字が返却されます。）
アプリ側は画面に応じ、リトライを行わない場合（単にエラーが発生した旨を表示するだけ）には false に設定してください。
なお、本 dispatch は、リトライを行わない（継続しない）場合でも、abortによる終了ではなく、onFinishのsuccessが返却される点に注意してください。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForRetrySendingDigiSign"
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bRetry" : true
> }
> ```
再試行する場合trueを設定します。
falseを返却した場合、電子サイン送信失敗エラー(PP_W0002)となります。




## 2.15. 印字開始前確認

印字処理の開始前に発生します。
なお、返却値でfalseに指定してもabortedにはなりません。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForStartingSlipPrint"
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bExecute" : true
> }
> ```
印字を行う場合 true、印字を行わない場合 falseを設定します。
POS連携で印字不要の場合には、bExecuteをfalseに設定します。
> 返却値(BOOL)<br/>
常にtrueを返却してください。
falseを返却しても業務は中断されません。



## 2.16. 伝票出力前確認

伝票を出力する直前に発生します。
> 入力パラメータ(inJson)
> ```js
> {
>     "command":"waitForPrintingSlipPage",
>     "parameter":{
>         "pageCurrent":1,
>         "pageTotal":3
>     }
> }
> ```

| プロパティ名 | 変数型 |  機能 |
|:-|:-|:-|
| pageCurrent    | integer   | これから印字する伝票のページ番号（１〜） |
| pageTotal      | integer   | 印字する伝票の総ページ数 |
| 

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bExecute" : true
> }
> ```
印字を行う場合 true、印字を行わない場合 falseを指定します。
もし、印字を行わない指定をしても、業務処理は継続されます。
> 返却値(BOOL)<br/>
>>常にtrueを返却してください。falseを返却しても業務は中断されません。



## 2.17. 印字時復帰可能エラー応答

印字処理中にエラーが発生し、そのエラーにつき復帰するか否かを返却します。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForRecoverablePrinterError",
>     "parameter": {
>         "status" : "success"
>     }
> }
> ```

<b> エラー発生理由（parameter.status）一覧<b/>
| statusに入る文字列 | 内容 |
|:-|:-|
| success             | 成功 |
| coverOpen           | カバーオープン |
| presenterPaperJamError | 紙詰まり|
| receiptPaperEmpty   | 用紙切れ |
| openError           | ライブラリエラー |
| compulsionSwitch    | ドロワのコンパルジョン SW が押されている |
| overTemp            | ヘッドが高温になり印刷停止している状態 |
| unrecoverableError  | 復帰不可能エラー(ヘッドサーミスタエラー、オートカッターエラー、 電源電圧エラー等) |
| cutterError         | カッターエラー |
| mechError           | （不明なエラー） |
| headThermistorError | ヘッドサーミスタ異常値検出 |
| receiveBufferOverflow | 受信バッファエラー |
| pageModeCmdError    | ページモードエラー |
| blackMarkError      | 用紙裏面のアイマークエラー |
| headUpError         | 印字ヘッドエラー |
| voltageError        | 電圧異常エラー |
| offline             | プリンタオフライン |
| -portExcption       | StarSDKで発生した例外 *PPSDK独自エラー |
| -portOpenError      | プリンタ接続エラー  *PPSDK独自エラー |
| -pageClose          | ページ印字終了時のシステムエラー *PPSDK独自エラー |
| -writeError         | 通信エラー *PPSDK独自エラー |
|

※ その他、定義されていないエラーが発生する可能性があり、その場合は「不明なプリンタエラー」として取り扱ってください。  

※ また、エラーが同時に発生している場合がありますが、この場合 PPSDK側でエラーを１つ選択して通知します。優先順位は上記一覧の順で発生したエラーを検出します。

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bExecute" : true
> }
> ```
再試行する場合はbExecuteにtrue、印字を終了する場合はfalseを設定します。
なお、bExecuteにfalseを指定し印字を終了しても、業務処理は継続されます。
> 返却値(BOOL)<br/>
常にtrueを返却してください。
falseを返却しても業務は中断されません。



## 2.18. 印字終了《通知》
全ての印字処理が終了（成功・失敗問わず）した際に発生します。
ただし、「印字開始前確認」でfalseを返却した場合には、当該通知は発生しません。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "printFinished",
>     "parameter": {
>         "status" : "success"
>     }
> }
> ```
> 出力パラメータ(outJson)<br/>
（なし）



## 2.19. 業務結果表示

ICカード抜去待ちを判定する直前にdispatchされます。
成功時も失敗時もdispatchされます。
このdispatchを受けてメッセージを表示するか否かは任意です。
"parameter" の要素は "onFinish" 時の "result" と同じものです。
> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForShowingJobResult",
>     "parameter": {
>         "status" : "success"
>     }
> }
> ```
> 出力パラメータ(outJson)<br/>
（なし）

> 返却値(BOOL)<br/>
常にtrueを返却してください。
（falseを返却しないでください）



## 2.20. PIN入力開始《通知》

PIN入力開始時に通知されます。
> 入力パラメータ(inJson)
> ```js
> {
>     "command":"notice",
>     "message":"beginInputPIN",
>     "parameter": {
>         "amount": 2333,
>         "application": {
>             "aid": "a000000333010102",
>             "api": "01",
>             "brandApJudgeNo": "05",
>             "brandApVersion": "0030",
>             "brandName": "UnionPay Debit",
>             "defaultAcquirerKid": "105",
>             "displayLabel": "UICC DEBIT",
>             "label": "5549434320435245444954",
>             "selectIndexDiv": "0",
>             "settlementMediaDiv": "1",
>             "value": "6f318407a0000000031010a526500b56495341204352454449549f120f4352454449544f20444520564953419f110101870101"
>         },
>         "uiMessage": {
>             "message1": "AMT ￥2,333\nPlease enter PIN",
>             "message2": ""
>         }
>     }
> }
> ```
なお、"parameter.amount"と"parameter.application"はUIでは使用せず、業務によってはnullになる可能性がありますので無視してください。（カードリーダーが必要とする情報になります）

> 出力パラメータ(outJson)<br/>
（なし）



## 2.21. PIN入力終了《通知》

PIN入力終了時に通知されます。
なお、PIN入力のリトライが発生した場合には、続けて "beginInputPIN"通知が発生する可能性があります。
> 入力パラメータ(inJson)
> ```js
> {
>     "command":"notice",
>     "message":"endInputPIN"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.22. センター問い合わせ中《通知》

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "centerTransaction"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.23. マスターデータ更新確認中《通知》

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "checkingMasterData"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.24. マスターデータ更新実行《通知》

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "masterDataUpdating"
> }
> ```
> 出力パラメータ(outJson)<br/>
（なし）

## 2.25. マスターデータチェック実行

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "checkingMasterData",
>     "parameter":{
>         "bNeedUpdate":false,
>     }
> }
> ```

| プロパティ名 | 変数型 |  機能 |
|:-|:-|:-|
| isNeedUpdateMasterData    | boolean   | マスタ更新が必要かどうか。trueが必要　|


> 出力パラメータ(outJson)<br/>
（なし）



## 2.26. "SEE PHONE"に基づくカード活性化待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForCardActive"
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.27. Journal業務のパラメータ入力待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForJournalOptions"
> }
> ```

> 出力パラメータ(outJson)
> ```js
> {
>     "bIntermediate" : false,
>     "paymentType" : "Credit",
>     "reprint" : "No",
>     "detailOption" : "Deatil"
> }
> ```
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
|bIntermediate  | boolean | ● | 中間計の場合trueを指定します。<br>falseを指定した場合はクローズ処理を行います。|
|paymentType    | string  | ● | ジャーナル種別を指定します。<br>Credit / NFC / UnionPay / All |
|reprint        | string  | ● | 再印字かどうかを指定します。<br>No ... 再印字なし（通常）<br>Last ... 前回再印字<br>BeforeLast ... 前々回再印字 |
|detailOption   | string  | ● | 詳細印字か簡易印字かを指定します。<br>Detail ... 詳細<br>Summary ... 簡易 |
|

中間計の再印字（前回・前々回）を指定した場合にはパラメータエラーで終了します。



## 2.28. 再印字業務のパラメータ入力待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForReprintOptions"
> }
> ```

> 出力パラメータ(outJson)
> ```js
> {
>     "paymentType" : "UnionPay",
>     "slipNo" : "99999"
> }
> ```
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
|paymentType    | string  | ● | 再印字を行う種別を指定します。<br>Credit / NFC / UnionPay |
|slipNo         | string  | - | 伝票番号指定の場合に設定します。<br>slipNoが存在しない場合には前回再印字となります。 |
|



## 2.29. 再印字業務の確認表示終了待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForReprintConfirm"
>     "parameter" : {
>         "hps" :{
>            「外部向けＩ／Ｆ仕様書_30（再印字編）」の「3 伝票再印字」の各種伝票情報取得の「（３）正常時レスポンスパラメータ」を参照　※Cookieで返却される部分については返却していません
>         }
>     }
> }
> ```

> 出力パラメータ(outJson)
（なし）



## 2.30. ジャーナル再印字業務の確認表示終了待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForJournalReprintConfirm"
>     "parameter" : {
>         "hps" :{
>            「外部向けＩ／Ｆ仕様書_30（再印字編）」の「3 伝票再印字」の各種伝票情報取得の「（３）正常時レスポンスパラメータ」を参照　※Cookieで返却される部分については返却していません
>         }
>     }
> }
> ```

> 出力パラメータ(outJson)
（なし）



## 2.31. 処理開始待ち

（F/W更新、ログ送信画面の時のみ）ユーザの開始を待ちます。
> 入力パラメータ(inJson)
> ```js
> {
>     "command":"waitForStart",
>     "parameter":{
>         "bAvailable": true,
>     }
> }
> ```

| プロパティ名 | 変数型 |  機能 |
|:-|:-|:-|
| bAvailable    | boolean   | 実行可能かどうか |
|

> 出力パラメータ(outJson)<br/>
（なし）
<br/>
> 返却値(BOOL)
>>処理を開始する場合にはtrueを返却してくだい。
処理を行わない場合にはfalseを返却してください。
bAvailableがfalseの場合には、必ずfalseを返却してください。（trueを返却した場合internalエラーが発生します）


## 2.32. アクティベーション情報入力待ち

アクティベート業務にて、必要情報の入力を行います。
なお、既にアクティベートが完了している場合には、このdispatchは呼び出されず、onFinish（status=success）となります。

"debug.bActivationSkipError" を設定すると、「op0619. do?oP0619_1」のエラーを無視するので、ダウンロード可能な状態の証明書についてはアクティベート完了通知をチェックすることができます。

> 入力パラメータ(inJson)
> ```js
> {
>     "command":"waitForActivationOptions"
> }
> ```

> 出力パラメータ(outJson)
> ```js
> {
>     "subscribeSequence" : "0000001057",
>     "activateID" : "77669162",
>     "oneTimePassword" : "77669162"
> }
> ```
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
|subscribeSequence  | string  | ● | 申込通番 |
|activateID         | string  | ● | アクティベートID |
|oneTimePassword    | string  | ● | パスワード |
|



## 2.33. サーバーで検索失敗《通知》

主に指定された伝票番号の取引が既に取り消されている際に通知されますが、
取引検索においてHPSでエラーが発生した際にも当該通知が行われます。
HPSの詳細エラーについては、「外部向けＩ／Ｆ仕様書_補足(メッセージ一覧).xls」をご確認ください。

> 入力パラメータ(inJson)
> ```js
> {
>     "command": "notice",
>     "message": "refundSearchError",
>         "result": {
>             "detail": {
>                 "errorObject": {
>                         "errorCode": "T_W2016",
>                         "errorLevel": "WARN",
>                         "errorMessage": "指定された取引は既に取り消されています。↑再度入力してください。",
>                         "errorType": "WARN",
>                         "printBugReport": "false"
>                 },
>                 "resultCode": "1"
>             },
>             "where": "hps"
>         },
>         "status": "error"
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）

### 2.33.1. 注意点
この refundSearchError は応答で false を返却すると、abortedではなくerrorでonFinishが呼び出されます。
これは、POS連携で伝票番号の引き当てがない時に、その旨POSに返却することを考慮しての仕様です。
非POS連携では、trueを返却して、ユーザに再入力を促してください。



## 2.34. TMSからの情報《通知》

開局業務でのみdhispatchされる通知です。
なお、TMSでエラーが発生、または、タイムアウトした場合には通知されません。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "notice",
>     "message" : "tmsUpdates",
>     "parameter":{
>         "bAvailableNewApp":false,
>         "bFirmwareUpdate":true
>     }
> }
> ```
| プロパティ名 | 変数型 | 必須 | 内容 |
|:-|:-|:-:|:-|
|bAvailableNewApp   | boolean  | ● | アプリケーション更新が存在する場合true |
|bFirmwareUpdate    | boolean  | ● | ファームウェア更新が存在する場合true |
|

> 出力パラメータ(outJson)<br/>
（なし）



## 2.35. 端末サービス設定確認
端末サービス設定が表示する直前に呼び出されます。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForServicePrintConfirm"
>     "parameter" : {
>         "COMPANY_LIST" :[
>             {
>                 "SERVICE_CONTRACTOR_ID": "0000078",
>                 "SERVICE_CONTRACTOR_NAME": "スマホテスト用",
>                 "SERVICE_DIV_NAME": "売上集計"
>             },
>             ......,
>             {
>                 "SERVICE_CONTRACTOR_ID": "0000078",
>                 "SERVICE_CONTRACTOR_NAME": "スマホテスト用",
>                 "SERVICE_DIV_NAME": "電子サイン"
>             }
>         ]
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.36. カード会社一覧
カード会社一覧が表示する直前に呼び出されます。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForSettingIssuersConfirm"
>     "parameter" : {
>         "COMPANY_LIST" :[
>         {
>             "CO_NAME": "株式会社ジェーシービー",
>             "KID": "102"
>         },
>         ......,
>         {
>             "CO_NAME": "三菱ＵＦＪニコス株式会社",
>             "KID": "209"
>         }
>         ]
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.37. カード先挿入検知
磁気フォールバックが発生した場合、カード読み取り前にカード検知を受信した場合のみ、onFinishにならないコマンドを発行します。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForMSFBCardExist",
>     "parameter" : {
>         "result": {
>             "detail": {
>                 "errorObject": {
>                      "errorCode": "PP_E2004",
>                      "errorLevel": "ERROR",
>                      "errorMessage": "cardExist",
>                      "errorType": "ERROR",
>                 },
>                 "resultCode": "1"
>             },
>             "where": "r/w"
>         },
>         "status": "error"
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "bRetry" : true
> }
> ```
bRetryにtrue、falseにした場合はabortedで返却されます。

## 2.38. KID入力待ち

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForKidInput",
>     "parameter" : {
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
> ```js
> {
>     "kid" : "105"
> }
> ```

| プロパティ名 | 変数型 | 必須 | 制限 | 機能 |
|:-|:-|:-:|:-|:-|
| kid | string   | ● | [0-9]{3} | KID |
|



## 2.39. KID入力成功《通知》

> 入力パラメータ(inJson)
> ```js
> {
>   "command" : "notice",
>   "notice" : "waitForKidInput-Success"
> }
> ``

> 出力パラメータ(outJson)<br/>
（なし）



## 2.40. KID入力失敗《通知》
この通知の後、「KID入力待ち」が再度呼び出されます。

> 入力パラメータ(inJson)
> ```js
> {
>   "command" : "notice",
>   "notice" : "waitForKidInput-Retry"
> }
> ``

> 出力パラメータ(outJson)<br/>
（なし）



## 2.41. カードリードが終了《通知》 
NFCを使用の場合、R/Wへカードリードが終了した際に通知されます。
noticeには、オンライン認証であった場合には、Typeにonlineが返却され、オフライン承認の場合は、offlineが設定されます。

> 入力パラメータ(inJson)
> ```js

> {
>     "command" : "notice",
>     "message" : "onCardReadFinished"
>     "parameter" : {
>         "Type" : "online"
>     }
> }
> ```

> 出力パラメータ(outJson)<br/>
（なし）



## 2.42. NFC取消情報入力待ち
NFCで取消の場合、取引の情報を入力待ち。

> 入力パラメータ(inJson)
> ```js
> {
>     "command" : "waitForRefundInput",
>     "parameter" : {
>         "hps": {
>             "isNeedProductCode": true,
>             "isNeedTaxOther": true
>         }
>     }
> }
> ```

| プロパティ名 | 変数型 |  機能 |
|:-|:-|:-|
| isNeedProductCode    | boolean   | 商品コードが必要かどうか。trueが必要　※ |
| isNeedTaxOther　　    | boolean   | 税・その他が必要かどうか。trueが必要　※ |
|

※　以上2つ項目について、開局情報により、入力不要となります。


> 出力パラメータ(outJson)<br/>
> ```js
> {
>         "amount" : 1234,
>         "slipNo" : "12345",
>         "taxOtherAmount": 800,
>         "productCode": "0990"
> }
> ```

| プロパティ名 | 変数型 | 必須 | 制限 | 機能 |
|:-|:-|:-:|:-|:-|
| amount              | integer   | ● | -           | 金額 |
| slipNo              | string    | ● | [5][0-9]*  |伝票番号 |
| taxOtherAmount      | integer   | ▲ | -           | 税・その他, inJson中isNeedTaxOtherがtrueの場合が必須　 |
| productCode         | string    | ▲ | [４][0-9]*  | 商品コード, inJson中isNeedProductCodeがtrueの場合が必須 |
|



# 3. onDispatchCancel(PPDispatchCancelCallback_t)

エラーが発生した場合にはonDispatch中でもonDispatchCancelが呼び出される可能性があります。
この場合、onDispatchの処理を速やかに終わらせ、PPSDK側に制御を戻してください。

>入力パラメータ(inJson)<br/>
（なし）
<br/>
> 出力パラメータ(outJson)<br/>
（なし）

なお、onDispatchの返却値は json は {} （空要素）、関数返却値は false（中断要求）を設定してください。



# 4. onFinish(PPFinishCallback_t)

業務終了時に呼び出されます。
エラー時でもonFinishが呼び出されます。
業務は、このonFinishを受けて終了となります。
なお、このonFinishは、PPSDK内部でエラーが発生した場合、onDispatch中でもonFinishが呼び出される可能性があります。
この場合、事前にonDispatchCancelが呼び出されるので、onDispatchの処理を速やかに終わらせ、PPSDK側に制御を戻してください。

## 4.1. 戻り値

 正常的に終了させる場合、入力パラメータは下記のとおりです。
 "result”は業務によって返却されない場合があります。
> 入力パラメータ(inJson)　売上/取消
> ```js
> {
>     "status" : "success",
>     "result" : {
>         "hps" : {
>     「外部向けＩ／Ｆ仕様書_」の「Ｉ／Ｆ項目」の「正常時レスポンスパラメータ」を参照
>     （クレジットの場合 外部向けＩ／Ｆ仕様書_11（クレジット編）　3.1.3.1.	取引確認　/　銀聯の場合　外部向けＩ／Ｆ仕様書_12（銀聯編）：3.5.1.3.1. 取引確認、※ICの場合は、3.1.2.3.8. 2nd GENERATE ACコマンド、イシュアスクリプトコマンド(GAC後)実行結果検証）Cookieで返却される部分については返却していません
>            "PPSDK" : {
>                 "bTradeCompletionFlag": true,
>                 "isNeedAquirerSlip" : true | false | empty ... 「カード会社控」を印字するか否か
>            }
>         }
>     }
> }
> ```
> "bTradeCompletionFlag" は印字結果送信を行ったかを判定するフラグです。デフォルト値はfalseとする。PPSDKが印字結果送信をリクエストした場合は、値をtrueで設定されます。
> "isNeedAquirerSlip" は電子伝票保管対象時に true か false で設定されます。
電子伝票保管対象外では "isNeedAquirerSlip" は設定されません。

> 入力パラメータ(inJson)　再印字
> ```js
> {
>     "status" : "success",
>     "result" : {
>         "hps" : {
>     「外部向けＩ／Ｆ仕様書_」の「Ｉ／Ｆ項目」の「正常時レスポンスパラメータ」を参照
>     （外部向けＩ／Ｆ仕様書_30（再印字編）：3.	伝票再印字）Cookieで返却される部分については返却していません
>         }
>     }
> }
> ```


+ ユーザーによる中断の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>    "status" : "aborted"
> }
> ```

+ 業務タイマによる中断の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>    "status" : "abortedByJobTimer"
> }
> ```
>業務タイマの値はコンフィグファイル（ppconfig.default.json）で項目(hps.jobTimeout)を設定する。



## 4.2. エラーについて

異常的に終了させる場合、入力パラメータは下記のとおりです。

+ 異常時・JMups要因の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>     "status" : "error",
>     "result" : {
>         "whrere" : "hps",
>         "detail" : {
>            「外部向けＩ／Ｆ仕様書_10（共通編）」の「1.2 異常時のレスポンスパラメータ」を参照　Cookieで返却される部分については返却していません
>         },
>         "recruit":{
>            "errorCode": "T_W3126",
>            "detail": "j-mupsエラーメッセージ",
>            "hpsUrl": "opzz.do?oPZZ72_2=aaa",
>            "bTradeCompletionFlag" : false
>        }
>     }
> }
> ```

"errorcode"について、HPSエラーコードが適応されます。
bTradeCompletionFlagについては、 [4.onFinish(PPFinishCallback_t)](#4-onfinishppfinishcallback_t)をご確認ください。


+ 異常時・ネットワーク要因の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>     "status" : "error",
>     "result" : {
>         "whrere" : "network",
>         "detail" : {
>             "code" : nnnn,  ... integer (OSStatus)
>             "description" : "xxxx" ... string
>         },
>         "recruit":{
>            "errorCode": "N_E-1200",
>            "detail": "OSエラーメッセージ",
>            "hpsUrl": "opzz.do?oPZZ72_2=aaa",
>            "bTradeCompletionFlag" : false
>        }
>     }
> }
> ```

+ 異常時・r/w要因の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>     "status" : "error",
>     "result" : {
>         "whrere" : "r/w",
>         "detail" : {
>             "code" : nnnn, ... optional integer
>             "description" : "xxxx" ... string
>         },
>         "recruit":{
>            "errorCode": "PP_E2001",
>            "detail": "SDKエラーメッセージ",
>            "hpsUrl": "opzz.do?oPZZ72_2=aaa",
>            "bTradeCompletionFlag" : false
>        }
>     }
> }
> ```

"recruit.errorCode"について
> | errorCode | 内容 |
> |:-|:-|
> | "PP_E2001"       | カードリーダ接続失敗かカードリーダ接続切れか |
> | "PP_E2002"       | ICカード業務最中にICカードが抜かれた |
> | "PP_E2003"       | ICカードは利用不可設定になるか、 MiuraのファームウェアやMPIが古い場合エラーか、その他エラー |
> | "PP_E2004"       | カード読み取り前にカード検知 |
> | "PP_E2005"       | カードを正常に読めなかったかICチップが読み取れなかったか |
> | "PP_E4001"       | RWの待ち受けタイムアウト |
> | "PP_E4002"       | 指定業務を開始できなかった場合のIC誘導 |
> | "PP_E4003"       | 指定業務を開始できなかった場合の磁気誘導  |
> | "PP_E4004"       | NFC取引：RW内configファイルに指定ブランド情報が無かった場合 |
> | "PP_E4005"       | RW故障 |
> | "PP_E4006"       | NFC取引：NFC取引ができないカードであった場合 |
> | "PP_E4007"       | NFC取引：RWに送信するパラメータが不正で有った場合 |
> | "PP_E4008"       | NFC取引：Miurat指定タグ（80）が異常値の場合 |
> | "PP_E4009"       | NFC取引：RWで受付できないコマンドが発行された |
> | "PP_E4010"       | NFC取引：かざしたカードにR/Wが対応しているアプリケーションがない |
> | "PP_E4011"       | NFC取引不可（Declined） |
> | "PP_E3001"       | PIN不知フォールバック |
> | "PP_E3002"       | PIN入力超過エラー |
> | "PP_E9999"       | 異常時・内部要因のエラー |
> |

"recruit.detail" に入る文字列
> | description | 内容 |
> |:-|:-|
> | "disconnected"        | 切断 | 
> | "icCardRemoved"       |  ICカード業務最中にICカードが抜かれた | 
> | "general"             |  異常時 | 
> | "cardExist"           |  取引開始時点で既にICカードが挿入されている |
> | "disableCardInserted" |  カードを正常に読めなかったかICチップが読み取れなかったかー | 
> |


+ 異常時・内部要因の場合、入力パラメータは下記のとおりです。
> 入力パラメータ(inJson)
> ```js
> {
>     "status" : "error",
>     "result" : {
>         "whrere" : "internal",
>         "detail" : {
>         "code" : nnnn, ... optional integer
>         "description" : "xxxx" ... string
>         },
>         "recruit":{
>            "errorCode": "PP_E9999",
>            "detail": "SDKエラーメッセージ",
>            "hpsUrl": "opzz.do?oPZZ72_2=aaa",
>            "bTradeCompletionFlag" : false
>        }
>     }
> }
> ```

"recruit.errorCode"の値は"PP_E9999"とする。　


# 5. コンフィグファイル

コンフィグファイルppconfig.default.jsonの更新関数を記載しております。

## 5.1. PPBridge.updateConfigure

呼び出し型は下記のとおりです。

```
- (void)updateConfigure:(NSString*)desc
                   json:(NSString*)json;
```

### 5.1.1. 入力パラメータ(desc)
debug用のパラメータです。
指定すると、PPSDKのdebugログファイルを指定した文字列が出力されます。
特に指定が無い場合はデフォルト値をそのまま設定してください。

### 5.1.2. 入力パラメータ(json)

PPSDKからの指示がJSON文字列で指定されます。差分でコンフィグファイルの内容を更新します。
コンフィグファイル内容の一部が変更可能です。変更可能の項目は後述します。


## 5.2. コンフィグファイル内容

デフォルトでコンフィグファイルの項目は以下のように記載しております。

> 入力パラメータ(json)
> ```js
> {
>     "hps":{
>         "urlForActivation": "https://xxxx/hps_trm/",
>         "urlForTrading": "https://xxxx/hps_trm/",
>         "terminalInfo": null,
>         "masterData": null,
>         "cookies": null,
>         "bSupportCupIc": false,
>         "keychainStoreLabel": "JMupsIdentity",
>         "jobTimeout": 300,
>         "bSignBeforeSendTagging" : true,
>         "pinMessages":{
>             "11": "暗証番号を入力してください\n金額　##AMOUNT##円",
>             "12": "暗証番号を入力してください\n（残り1回）\n金額　##AMOUNT##円",
>             "13": "暗証番号が間違っています",
>             "14": "暗証番号を確認しました",
>             "15": "暗証番号が間違っています\nカード会社に連絡してください",
>             "1": "AMT ￥##AMOUNT##\nPlease enter PIN",
>             "2": "AMT ￥##AMOUNT## (Last try)\nPlease enter PIN",
>             "3": "Incorrect PIN",
>             "4": "Correct PIN",
>             "5": "Incorrect PIN\nPlease call Issuer",
>             "6": "AMT ￥##AMOUNT##\nPlease enter PIN\n",
>             "7": "Accept?\n%s\nYes>Press Green\nNo >Press Red",
>             "8": "Amount\n%s\nPlease enter PIN",
>             "9": "Amount  (Last try)\n%s\nPlease enter PIN",
>             "10": "Please remove\nthe card",
>             "unionpay": "暗証番号を入力してください。",
>             "retry": "再度、"
>         }
>     },
>     "ios":{
>         "appGroupId": "group.com.clementec.pokepos"
>     },
>     "job":{
>         "credit":{
>             "media": {
>                 "ic": true,
>                 "ms": true,
>                 "nfc": true,
>                 "qr": false
>             }
>         },
>         "unionpay":{
>             "media": {
>                 "ic": true,
>                 "ms": true,
>                 "nfc": false,
>                 "qr": false
>             }
>         }
>     },
>     "debug":{
>         "bOutputToConsole": true,
>         "bRecordLog": false,
>         "bActivationSkipError": false,
>         "bResetTMSPasswords": false
>     },
>     "miura":{
>         "connectTimeoutSecond": 5,
>         "ea":{
>             "protocol": "(com\\.clementec\\.payment\\.JETSmart)",
>             "name": "Miura.*",
>             "serial": null
>         },
>         "messages":{
>             "contactless":{
>                 "defaults":{
>                 },
>                 "table":[
>                 ],
>                 "prompts":{
>                     "MIURA_CONTACTLESS_UI_APPROVED"                       : "承認されました\\nカードを離して下さい", 
>                     "MIURA_CONTACTLESS_UI_DECLINED"                       : "お取扱いできません\\nカードを離して下さい",
>                     "MIURA_CONTACTLESS_UI_PLEASE_ENTER_PIN"               : "PINを入力して下さい",
>                     "MIURA_CONTACTLESS_UI_ERROR_PROCESSING"               : "エラーが発生しました",
>                     "MIURA_CONTACTLESS_UI_REMOVE_CARD"                    : "カードを離して下さい",
>                     "MIURA_CONTACTLESS_UI_PRESENT_CARD"                   : "かざして下さい",
>                     "MIURA_CONTACTLESS_UI_PROCESSING"                     : "処理中です",
>                     "MIURA_CONTACTLESS_UI_CARD_READ_OK_REMOVE"            : "受け付けました\\nカードを離して下さい",
>                     "MIURA_CONTACTLESS_UI_INSERT_OR_SWIPE_CARD"           : "no_prompt",
>                     "MIURA_CONTACTLESS_UI_CARD_COLLISION"                 : "１枚だけ\\nかざして下さい",
>                     "MIURA_CONTACTLESS_UI_PLEASE_SIGN"                    : "サイン確認しました",
>                     "MIURA_CONTACTLESS_UI_ONLINE_AUTHORISATION"           : "センタ通信中です\\nお待ち下さい",
>                     "MIURA_CONTACTLESS_UI_TRY_OTHER_CARD"                 : "お取り扱いできません\\n他のカードをどうぞ",
>                     "MIURA_CONTACTLESS_UI_INSERT_CARD"                    : "カードをどうぞ",
>                     "MIURA_CONTACTLESS_UI_SEE_PHONE"                      : "携帯の画面を\\n確認して下さい",
>                     "MIURA_CONTACTLESS_UI_PRESENT_CARD_AGAIN"             : "再度かざして下さい",
>                     "MIURA_CONTACTLESS_UI_CALL_YOUR_BANK"                 : "no_prompt"
>                 }
>             },
>             "credit":{
>                 "defaults":{
>                 },
>                 "table":[
>                     { "MIURA_CREDIT_UI_APPROVED"                    : "カードを挿入してください\nInsert card"},
>                     { "MIURA_CREDIT_UI_ERROR_PROCESSING"            : ""},
>                     { "MIURA_CREDIT_UI_PROCESSING"                  : "処理中...\nProcessing..."},
>                     { "MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD"      : ""},
>                     { "MIURA_CREDIT_UI_IC_CARD_INSERT"              : "カードを挿入してください\nInsert card"},
>                     { "MIURA_CREDIT_UI_MS_CARD_SWIPE"               : "カードをスライドしてください\nSwipe card"},
>                     { "MIURA_CREDIT_UI_REMOVING_CARD"               : "処理完了\nカードを取り出してください\nApproved. Remove card"},
>                     { "MIURA_CREDIT_UI_PIN_ERROR"                   : "暗証番号エラー カード会社にお問い合わせください\nIncorrect PIN"},
>                     { "MIURA_CREDIT_UI_CARD_SWIPE_ERROR"            : "ICチップ読み取り失敗 カードをスライドしてください\nERROR Swipe card"},
>                     { "MIURA_CREDIT_UI_CARD_INSERT_ERROR"           : "ICカード読み取り失敗\nカードを挿入してください\nERROR Insert card"},
>                     { "MIURA_CREDIT_UI_CARD_DIFF_ERROR"             : "カードが違います 同じカードでお試しください\nERROR Different card"},
>                     { "MIURA_CREDIT_UI_CARD_CHECK_ERROR"            : "読み取り失敗\nカードを確認してください\nERROR Check card"},
>                     { "MIURA_CREDIT_UI_CARD_REMOVE_TRY"             : "カードを取り出して\nもう一度お試しください\nERROR Try again"},
>                     { "MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY"       : "読み取り失敗\nもう一度お試しください\nERROR Try again"},
>                     { "MIURA_CREDIT_UI_CARD_SKIP_FAILURE"           : "スキップ失敗 カードをスライドしてください\nERROR Swipe card"}
>                 ]
>             }
>         }
>     },
>     "bluepad":{
>         "connectTimeoutSecond": 5,
>         "minAppVersion": "2.4.39.63",
>         "messages":{
>             "contactless":{
>                 "defaults":{
>                     "font": "FONT_10X10_JPN",
>                     "text": ""
>                 },
>                 "table":[
>                     { "id":"EMV_UI_APPROVED"            , "text":"承認されました\nカードを離して下さい"},
>                     { "id":"EMV_UI_DECLINED"            , "text":"お取扱いできません\nカードを離して下さい"},
>                     { "id":"EMV_UI_PLEASE_ENTER_PIN"    , "text":"PINを入力して下さい"},
>                     { "id":"EMV_UI_ERROR_PROCESSING"    , "text":"お取り扱いできません\n他のカードをどうぞ"},
>                     { "id":"EMV_UI_REMOVE_CARD"         , "text":"カードを離して下さい"},
>                     { "id":"EMV_UI_IDLE"                , "text":"いらっしゃいませ"},
>                     { "id":"EMV_UI_PRESENT_CARD"        , "text":"かざして下さい"},
>                     { "id":"EMV_UI_PROCESSING"          , "text":"処理中です"},
>                     { "id":"EMV_UI_CARD_READ_OK_REMOVE" , "text":"受け付けました\nカードを離して下さい"},
>                     { "id":"EMV_UI_TRY_OTHER_INTERFACE" , "text":"カードを挿入するか\nスワイプして下さい"},
>                     { "id":"EMV_UI_CARD_COLLISION"      , "text":"１枚だけ\nかざして下さい"},
>                     { "id":"EMV_UI_SIGN_APPROVED"       , "text":""},
>                     { "id":"EMV_UI_ONLINE_AUTHORISATION", "text":"センタ通信中です\nお待ち下さい"},
>                     { "id":"EMV_UI_TRY_OTHER_CARD"      , "text":"お取り扱いできません\n他のカードをどうぞ"},
>                     { "id":"EMV_UI_INSERT_CARD"         , "text":"カードを挿入して下さい"},
>                     { "id":"EMV_UI_SEE_PHONE"           , "text":"携帯の画面を\n確認して下さい"},
>                     { "id":"EMV_UI_PRESENT_CARD_AGAIN"  , "text":"再度かざして下さい"}
>                 ]
>             },
>             "credit":{
>                 "defaults":{
>                     "font": "FONT_10X10_JPN",
>                     "text": ""
>                 },
>                 "table":[
>                     { "id":"EMV_UI_PRESENT_CARD"            , "text":"カードをどうぞ"},
>                     { "id":"EMV_UI_INSERT_CARD"             , "text":"カードをどうぞ"},
>                     { "id":"EMV_UI_PROMPT_ENTER_PIN"        , "text":"暗証番号を入力してください"},
>                     { "id":"EMV_UI_ERROR_PROCESSING"        , "text":"エラーが発生しました"},
>                     { "id":"EMV_UI_STATUS_ERROR_PROCESSING" , "text":"エラーが発生しました"},
>                     { "id":"EMV_UI_STATUS_NOT_READY"        , "text":"準備中"},
>                     { "id":"EMV_UI_STATUS_IDLE"             , "text":"待機中"},
>                     { "id":"EMV_UI_STATUS_READY_TO_READ"    },
>                     { "id":"EMV_UI_STATUS_PROCESSING"       },
>                     { "id":"EMV_UI_PROCESSING"              },
>                     { "id":"EMV_UI_STATUS_CARD_READ_SUCCESS"}
>                 ]
>             }
>         }
>     }
> }
> ```

### 5.2.1. hps項目 

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| urlForActivation | string   | ● | ● | アクティベーション用URL |
| urlForTrading    | string   | ● | ● | JMUPSの取引用URL |
| terminalInfo     | string   | ▲ | ▲ | 端末情報（UID、端末番号、OSバージョン、APPバージョン）|
| masterData       | string   | ▲ | ▲ | masterData ※NFCのため、保留|
| cookies          | string   | ▲ | ▲ | 使用のcookies　※NFCのため、保留 |
| bSupportCupIc    | boolean  | ● | ● | SDKレベルでの銀聯IC使用可否 trueの場合、利用 |
| multipleInterfaces   | boolean  | ● | ● | NFC売上業務を3面待ちにするかのフラグ trueの場合、利用 |
| bSignBeforeSendTagging| boolean  | ● | ● | 印字結果送信前サインするか trueの場合、利用　APYの場合はtrueで設定してください。　※2020年3月10日以降SDKでは、本項目は利用しません。 |
| keychainStoreLabel| string  | ● | ● | 証明書用のラベル　※当該値を変更することで証明書の切り替えが可能です。 |
| rwTimeout        | integer | ● | ● | rwタイムアウト時間（単位:s）当該値は、M010ConfigのDFFF01 に設定|
| jobTimeout        | integer | ● | ● | 業務タイムアウト時間（単位:s）|
| pinMessages 　     |array< map>| ● | ▲ | カードリーダーで表示のPINと関連なメッセージ |
|


「使用」列と「変更可」について
> ▲: 未使用/変更不可 <br/>
> ●: 使用中/変更可

「jobTimeout」とは、業務タイマーによるタイムアウトの値です。デフォルト値は300sです。
業務タイマーの開始タイミングはPPBridgeのexecuteJobを呼び出すタイミングです。
時間になったら、タイムアウトが発生し、業務強制的に中断します。
但し、印字結果送信直前のタイミングでは業務中断は行いません。


#### 5.2.1.1. pinMessagesについて 

PIN入力時のUI表示で利用するメッセージです。
※R/Wに表示するメッセージではありません。

| キー | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| "11"       | string  | ● | ▲  | "暗証番号を入力してください\n金額　##AMOUNT##円" |
| "12"       | string  | ● | ▲  | "暗証番号を入力してください\n（残り1回）\n金額　##AMOUNT##円" |
| "13"       | string  | ● | ▲  | "暗証番号が間違っています" |
| "14"       | string  | ● | ▲  | "暗証番号を確認しました" |
| "15"       | string  | ● | ▲  | "暗証番号が間違っています\nカード会社に連絡してください" |
| "1"        | string  | ● | ▲  | "AMT ￥##AMOUNT##\nPlease enter PIN" |
| "2"        | string  | ● | ▲  | "AMT ￥##AMOUNT## (Last try)\nPlease enter PIN" |
| "3"        | string  | ● | ▲  | "Incorrect PIN" |
| "4"        | string  | ● | ▲  | "Correct PIN" |
| "5"        | string  | ● | ▲  | "Incorrect PIN\nPlease call\nIssuer" |
| "6"        | string  | ● | ▲  | "AMT ￥##AMOUNT##\nPlease enter PIN\n" |
| "7"        | string  | ● | ▲  | "Accept?\n%s\nYes>Press Green\nNo >Press Red" |
| "8"        | string  | ● | ▲  | "Amount\n%s\nPlease enter PIN" |
| "9"        | string  | ● | ▲  | "Amount  (Last try)\n%s\nPlease enter PIN" |
| "10"       | string  | ● | ▲  | "Please remove\nthe card" |
| "unionpay" | string  | ● | ▲  | "暗証番号を入力してください。" |
| "retry"    | string  | ● | ▲  | "再度、" |
|


### 5.2.2. ios項目 

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| appGroupId       | string   | ● | ▲ | App Group ID |

### 5.2.3. bRecruit項目

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| bRecruit       | boolean   | ● | ▲ | RTC社専用エラーコードを表示するための値, デフォルトでtrue |
|
※2020年3月10日以降SDKでは、本項目は利用しません。

### 5.2.4. job項目 
取引開始時にR/Wの決済に対する待ち受けを設定することが可能です。

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| credit.media.ic     | boolean   | ● | ● | クレジットICを待ちうけるか否か  trueの場合、利用 |
| credit.media. ms    | boolean   | ● | ● | クレジット磁気を待ちうけるか否か  trueの場合、利用 |
| credit.media.nfc    | boolean   | ● | ● | クレジットNFCを待ちうけるか否か  trueの場合、利用 |
| credit.media.qr     | boolean   | ● | ● | クレジットqrを待ちうけるか否か  trueの場合、利用 |
| unionpay.media.ic   | boolean   | ● | ● | 銀聯ICを待ちうけるか否か  trueの場合、利用 |
| unionpay.media. ms  | boolean   | ● | ● | 銀聯磁気を待ちうけるか否か  trueの場合、利用 |
| unionpay.media.nfc  | boolean   | ● | ● | 銀聯NFCを待ちうけるか否か  trueの場合、利用 |
| unionpay.media.qr   | boolean   | ● | ● | 銀聯qrを待ちうけるか否か  trueの場合、利用 |
|

### 5.2.5. debug項目 

| プロパティ名 | 変数型 |  使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| bOutputToConsole     | boolean   | ● | ● | Consoleへ出力するかどうか   trueの場合、利用 |
| bRecordLog           | boolean   | ● | ● | ログファイルに記入するかどうか  trueの場合、利用します。ログファイルの書き込みは共有フォルダに書き込まれる仕様としております。plistの設定を「Key:Application supports iTunes file sharing　Value: YESにしてください。|
| bActivationSkipError | boolean   | ● | ● | アクティベーションのエラーを無視するかどうか   trueの場合、無視|
| bResetTMSPasswords   | boolean   | ● | ● | TMSの暗証番号をリセットするかどうか ※SDK利用会社によって提供していない場合がございます。 |
|

### 5.2.6. カードリーダーについて

#### 5.2.6.1. miuraについて

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| connectTimeoutSecond          | integer   | ● | ▲ | 接続タイムアウト時間（単位:s）、時間内接続していない場合、リトライする|
| ea.protocol                   | string    | ● | ● | カードリーダー接続使用するプロトコル |
| ea. name                      | string    | ● | ● | カードリーダー接続使用名前 |
| ea.serial                     | string    | ● | ● | カードリーダーシリアル（10桁の下３桁）※CoreFrameWork同時利用の場合は当該値は空にしてください。空でない場合、当該値が優先的に設定されます。|
| messages.contactless.defaults |array< map>| ● | ▲ | 非接触カード表示用のフォントとサイズなど |
| messages.contactless.table    |array< map>| ● | ▲ | 非接触カード表示用メッセージ |
| messages.credit.defaults      |array< map>| ● | ▲ | クレジット表示用のフォントとサイズなど |
| messages.credit.table         |array< map>| ● | ▲ | クレジット表示用メッセージ、キーの説明は下記です。 |
|

##### 5.2.6.1.1. contactlessのdefaultsとtable内容

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| "font"    | string   | ▲ | ▲ | カードリーダーがディスプレイするフォント |
| "text"    | string   | ▲ | ▲ | カードリーダーがディスプレイするフォントのサイズ |
|

messages.contactless.tableのキーについて
| messages.contactless.tableのキー | メッセージ変数型　| デフォルトメッセージ内容 |表示タイミング|
|:-|:-|:-|:-|
| "MIURA_CREDIT_UI_APPROVED"               | string | \nカードをどうぞ\nInsert/Swipe Card  | カード読み込み待機中               | 
| "MIURA_CREDIT_UI_ERROR_PROCESSING"       | string | nエラーが発生しました\nError.　       | R/Wエラー発生時 ※当該値をNULLもしくは空に設定することで直前に表示していた内容を引き継ぎます。| 
| "MIURA_CREDIT_UI_PROCESSING"             | string | \n   処理中...\n   Processing...    | IC処理中                         | 
| "MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD" | string | \nカードを抜いてください\nRemove Card | IC取引が中断される時または異常終了時 ※ MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARDの内容は空（null/""）の時、R/Wの表示を画像にします。| 
|



下記については、デフォルトでは用意しておりませんが、R/Wへのメッセージをイベントとして定義しております。
アプリケーションの用途によりメッセージの変更を行ってください。

| messages.contactless.tableのキー | メッセージ変数型　| 想定メッセージ内容 |表示タイミング|
|:-|:-|:-|:-|
| "MIURA_CONTACTLESS_UI_APPROVED"             | string | 承認されました\\nカードを離して下さい     | 取引が成功で完了 |
| "MIURA_CONTACTLESS_UI_DECLINED"             | string | お取扱いできません\\nカードを離して下さい  | 取引が失敗で完了 |
| "MIURA_CONTACTLESS_UI_PLEASE_ENTER_PIN"     | string | PINを入力して下さい                    | PINが必要の場合 |
| "MIURA_CONTACTLESS_UI_ERROR_PROCESSING"     | string | エラーが発生しました                    | エラーが発生の場合 |
| "MIURA_CONTACTLESS_UI_REMOVE_CARD"          | string | カードを離して下さい                    |               |
| "MIURA_CONTACTLESS_UI_PRESENT_CARD_SALES"   | string | カードをどうぞ                         | 三面待ち売上時のカード読み取り待ち表示 |
| "MIURA_CONTACTLESS_UI_PRESENT_CARD_UNDO"    | string | かざして下さい                         | NFC取り消し時表示 |
| "MIURA_CONTACTLESS_UI_PROCESSING"           | string | 処理中です                            | 処理中状態      |
| "MIURA_CONTACTLESS_UI_CARD_READ_OK_REMOVE"  | string | 受け付けました\\nカードを離して下さい     | 　             |
| "MIURA_CONTACTLESS_UI_CARD_COLLISION"       | string | １枚だけ\\nかざして下さい               | 数枚カードの場合 |
| "MIURA_CONTACTLESS_UI_PLEASE_SIGN"          | string | サイン確認しました                     |                |
| "MIURA_CONTACTLESS_UI_ONLINE_AUTHORISATION" | string | センタ通信中です\\nお待ち下さい          | センタ通信中です  |
| "MIURA_CONTACTLESS_UI_TRY_OTHER_CARD"       | string | お取り扱いできません\\n他のカードをどうぞ  | カードが使えない場合 |
| "MIURA_CONTACTLESS_UI_INSERT_CARD"          | string | カードをどうぞ                         |                 |
| "MIURA_CONTACTLESS_UI_SEE_PHONE"            | string | 携帯の画面を\\n確認して下さい            | SeePhoneイベント発生時    |
| "MIURA_CONTACTLESS_UI_PRESENT_CARD_AGAIN"   | string | 再度かざして下さい                      | 半かざしをした場合          |
| "MIURA_CONTACTLESS_UI_INSERT_OR_SWIPE_CARD" | string | 磁気カードまたはICカードでお試し下さい     | NFC取引が出来なかった場合     |
| "MIURA_CONTACTLESS_UI_CALL YOUR BANK"       | string | カード発行会社に確認して下さい            |  NFC取引が出来なかった場合    |
|


##### 5.2.6.1.2. creditのdefaultsとtable内容

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| "font"    | string   | ▲ | ▲ | カードリーダーがディスプレイするフォント |
| "text"    | string   | ▲ | ▲ | カードリーダーがディスプレイするフォントのサイズ |
|

messages.credit.tableのキーについて
| messages.credit.tableのキー | メッセージ変数型　| デフォルトメッセージ内容 |表示タイミング|
|:-|:-|:-|:-|
| "MIURA_CREDIT_UI_APPROVED"               | string | \nカードをどうぞ\nInsert/Swipe Card  | カード読み込み待機中               | 
| "MIURA_CREDIT_UI_ERROR_PROCESSING"       | string | nエラーが発生しました\nError.　       | R/Wエラー発生時 ※当該値をNULLもしくは空に設定することで直前に表示していた内容を引き継ぎます。| 
| "MIURA_CREDIT_UI_PROCESSING"             | string | \n   処理中...\n   Processing...    | IC処理中                         | 
| "MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARD" | string | \nカードを抜いてください\nRemove Card | IC取引が中断される時または異常終了時 ※ MIURA_CREDIT_UI_WAIT_FOR_REMOVING_CARDの内容は空（null/""）の時、R/Wの表示を画像にします。| 
|



下記については、デフォルトでは用意しておりませんが、R/Wへのメッセージをイベントとして定義しております。
アプリケーションの用途によりメッセージの変更を行ってください。

| messages.credit.tableのキー | メッセージ変数型　| 想定メッセージ内容 |表示タイミング|
|:-|:-|:-|:-|
| "MIURA_CREDIT_UI_IC_CARD_INSERT"        | string | \nカードを挿入してください\nInsert Card                           |ICフォールバック後のカード読み取り状態          |
| "MIURA_CREDIT_UI_MS_CARD_SWIPE"         | string | \nカードをスワイプしてください\nSwipe Card                    　   |磁気フォールバック後のカード読み取り状態         |
| "MIURA_CREDIT_UI_REMOVING_CARD"         | string | 処理完了\nカードを抜いてください\nApproved. Remove Card            |IC取引が正常に完了時                          |
| "MIURA_CREDIT_UI_PIN_ERROR"             | string | 暗証番号エラー\nカード会社にお問い合わせしてください\nIncorrect PIN    |暗証番号入力エラー超過時                      |
| "MIURA_CREDIT_UI_CARD_SWIPE_ERROR"      | string | ICチップ読み取り失敗\nカードをスワイプしてください\nERROR. Swipe Card |磁気フォールバック時                          |
| "MIURA_CREDIT_UI_CARD_INSERT_ERROR"   　| string | ICカード読み取り失敗\nカードを挿入してください\nERROR. Insert Card"   |ICフォールバック時                           |
| "MIURA_CREDIT_UI_CARD_DIFF_ERROR"       | string | カードが違います\n同じカードでお試しください\nERROR. Different Card   |磁気フォールバック後に違うカードが挿入された時    |
| "MIURA_CREDIT_UI_CARD_CHECK_ERROR"      | string | 読み取り失敗\nカードを確認してください\nERROR. Check Card            |カード挿入は検知したが、ICカードが見つからない場合|
| "MIURA_CREDIT_UI_CARD_REMOVE_TRY"       | string | カードを取り出して\nもう一度お試しください\nERROR. Try Again          |カード読取前にカードを検知した場合             |
| "MIURA_CREDIT_UI_CARD_READ_FAILURE_TRY" | string | 読み取り失敗\nもう一度お試しください\nERROR. Try Again               |カード情報の読取が出来なかった場合             |
| "MIURA_CREDIT_UI_CARD_SKIP_FAILURE"     | string | スキップ失敗\nカードをスワイプしてください\nERROR. Swipe Card         |PIN不知フォールバック                       |
|



#### 5.2.6.2. bluepadについて

| プロパティ名 | 変数型 | 使用　| 変更可 | 内容 |
|:-|:-|:-:|:-:|:-|
| connectTimeoutSecond          | integer   | ● | ▲ | 接続タイムアウト時間（単位:s）、時間内接続していない場合、リトライする |
| minAppVersion                 | string    | ● | ▲ | OSバージョン |
| messages.contactless.defaults |array< map>| ● | ▲ | 非接触カード表示用のフォントとサイズなど |
| messages.contactless.table    |array< map>| ● | ▲ | 非接触カード表示用メッセージ |
| messages.credit.defaults      |array< map>| ● | ▲ | クレジット表示用のフォントとサイズなど |
| messages.credit.table         |array< map>| ● | ▲ | クレジット表示用メッセージ |
|

# 6. その他のAPI


## 6.1. PPBridge.abort

業務実行中に当該業務を中断したい場合には、下記のインスタンスメソッドを呼び出します。

```
- (void)abort;
```

通常はexecuteJobのdispatchコールバックの返却値により、業務の続行・継続を制御しますが、カード待機中、PIN入力中など、「dispatchコールバック処理の中でない場合」に業務を中断させたい場合に使用します。

このabortの呼び出しは、業務実行中であればいつでも呼び出すことができます。


abortを呼び出した場合でも、PPSDKの処理状態によっては中断ができない場合がありますので、onFinishにてabortedかどうかを確認してください。なお、中断できないケースは下記のとおりです。


* 取引確認が存在する業務で、その取引確認を呼び出した後。
* 既に何らかのエラーが発生している場合。（この場合はエラーにより業務終了となります）

## 6.2. PPBridge.disconnectCardReader

カードリーダを強制的に切断したい場合に、下記のインスタンスメソッドを呼び出します。

```
- (void)disconnectCardReader;
```

カードリーダが既に切断されている場合には、何も行いません。
また、カードリーダの切断時のエラーは返却しません。

この関数は同期関数です。メインスレッドを止めたくない場合には、ワーカスレッドから呼び出す必要があります。

## 6.3. PPBridge.isTrainingMode

SDKがトレーニングモードか通常モードかを確認するための関数です。

```
- (BOOL)isTrainingMode;
```

* TRUEが返却された場合には、トレーニングモードです。
* FALSEが返却された場合には、通常モードです。

## 6.4. PPBridge.checkMasterData

PPSDKが内部でマスターデータの更新の必要有無をチェックするための関数です。

```
- (void)checkMasterData;
```

この関数は同期関数です。
void型なので呼び出し元には値を返しません。

## 6.5. PPBridge.clearCardReaderDisplay

カードリーダの文字を強制的にクリアする場合、下記のインスタンスメソッドを呼び出します。

```
- (void)clearCardReaderDisplay;
```

カードリーダが既に画像が表示されている場合には、何も行いません。

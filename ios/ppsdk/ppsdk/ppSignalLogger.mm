//
//  SignalLogger.mm
//  ppsdk
//
//  Created by tel on 2019/02/16.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <map>
#include "LogManager.h"

typedef std::map<int,struct sigaction> sigmap_t;

static const int kSingnals[] =
{
    SIGABRT,
    SIGILL,
    SIGSEGV,
    SIGFPE,
    SIGBUS,
    SIGTRAP
};

static const int kNumSignals = sizeof(kSingnals) / sizeof(kSingnals[0]);

@interface ppSignalLogger : NSObject
{
    sigmap_t*           sigactions_;
}
+ (ppSignalLogger*)sharedInstance;

-(void)uncaughtExceptionHandler:(NSException*)exp;
-(void)signalHandler:(int)signum;

@end

static void uncaughtExceptionHandlerStatic(NSException *exception)
{
    [[ppSignalLogger sharedInstance] uncaughtExceptionHandler:exception];
}

static void signalHandlerStatic(int signum)
{
    [[ppSignalLogger sharedInstance] signalHandler:signum];
}

@implementation ppSignalLogger

+ (ppSignalLogger*)sharedInstance
{
    static ppSignalLogger*     sharedInstance;
    static dispatch_once_t  once;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self = [super init];
    if(self != nil)
    {
        NSSetUncaughtExceptionHandler(&uncaughtExceptionHandlerStatic);
        
        [self setSigactions];
    }
    return self;
}

- (void)dealloc{
    [self restoreSigactions];
}

- (void)setSigactions
{
    [self restoreSigactions];
    
    sigactions_ = new sigmap_t();
    
    struct sigaction newsa;
    ::memset(&newsa, 0, sizeof(newsa));
    newsa.sa_handler = &signalHandlerStatic;
    
    for(int i = 0; i < kNumSignals; i++){
        struct sigaction oldsa;
        ::sigaction(kSingnals[i] , &newsa, &oldsa);
        sigactions_->insert(sigmap_t::value_type(kSingnals[i], oldsa));
    }
}

- (void)restoreSigactions
{
    if(sigactions_ != nullptr){
        for(int i = 0; i < kNumSignals; i++){
            struct sigaction sa = (*sigactions_)[kSingnals[i]];
            ::sigaction(kSingnals[i] , &sa, nullptr);
        }
        delete sigactions_;
        sigactions_ = nullptr;
    }
}


-(void)uncaughtExceptionHandler:(NSException*)exp
{
    LogManager_AddError("!!!! UNCAUGHT EXCEPTION !!!!");
    
    std::string stack = [[NSString stringWithFormat:@"%@", exp.callStackSymbols] UTF8String];
    LogManager_AddErrorF("Name: %s"     , ([exp.name UTF8String]));
    LogManager_AddErrorF("Reason: %s"   , ([exp.reason UTF8String]));
    LogManager_AddErrorF("CallStack: %s", stack);
}

-(void)signalHandler:(int)signum
{
    LogManager_AddError("!!!! SIGNAL RISED !!!!");
    [self restoreSigactions];
    
    std::string signame = "unknown signal";
    
    auto _sn = sys_signame[signum];
    if(_sn != nullptr){
        signame = _sn;
    }
    
    std::string stack = [[NSString stringWithFormat:@"%@", [NSThread callStackSymbols]] UTF8String];
    
    LogManager_AddErrorF("Description: %s (%d)", signame % signum);
    LogManager_AddErrorF("CallStack: %s", stack);
}

@end

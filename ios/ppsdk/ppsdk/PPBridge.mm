//
//  PPBridge.mm
//  ppsdk
//
//  Created by Clementec on 2017/05/29.
//
//

#import "PPBridge.h"
#include "WebClient.h"
#include <future>
#include <thread>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic pop
#include "AbstructFactory.h"
#include "CardReader.h"
#include "IOSUtilities.hpp"
#include "WindowsBitmapFileBuilder.h"
#include "PPConfig.h"
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <boost/algorithm/hex.hpp>
#include <boost/iostreams/copy.hpp>
#include "PaymentJobCreator.h"
#include "PaymentJob.h"
#include "JMupsJobStorage.h"
#include "JMupsAccess.h"
#include "KeyChainAccess.hpp"

class bridge
{
private:
    PaymentJobCreator               m_jobCreator;
    PaymentJob::Storage::sp         m_storage;
    CardReader::sp                  m_cardReader;
    boost::shared_ptr<PaymentJob>   m_job;
    bool                            m_bTrainingMode;
    BOOL                            m_bUseDigiSignInTrainingMode;

    pprx::subscription                  m_subscription;
    AtomicObject<PPFinishCallback_t>    m_onFinish;
    boost::asio::deadline_timer         m_jobTimer;

    CGImageRef resizeImageAndDecolorToGrayScale(CGImageRef src, int width, int height) const
    {
        auto bounds = CGRectMake(0, 0, width, height);
        CGImageRef gray;
        {
            auto colorspace = ::CGColorSpaceCreateDeviceGray();
            auto context = ::CGBitmapContextCreate(
                                NULL,
                                width,
                                height,
                                8,
                                0,
                                colorspace,
                                kCGImageAlphaNone);
            ::CGColorSpaceRelease(colorspace);
            
            ::CGContextSetRGBFillColor(context, 1, 1, 1, 1);
            ::CGContextAddRect(context, bounds);
            ::CGContextFillPath(context);

            ::CGContextDrawImage(context, bounds, src);
            gray = CGBitmapContextCreateImage(context);
            ::CGContextRelease(context);
        }
        return gray;
    }

    void startJobTimer(int timeoutSecond)
    {
        LogManager_Function();
        
        m_jobTimer.expires_from_now(boost::posix_time::seconds(timeoutSecond));
        m_jobTimer.async_wait([=](const boost::system::error_code& error){
            if (error){
                LogManager_AddWarningF("timer aborted with error. %s (%d)", error.message() % error.value());
            }
            else{
                LogManager_AddInformation("call abort() by job timer");
                abort(PaymentJob::AbortReason::JobTimer);
            }
        });
    }
    
    void stopJobTimer()
    {
        LogManager_Function();
        m_jobTimer.cancel();
    }
    
protected:
    
public:
    bridge() : m_jobTimer(BaseSystem::instance().ioService()) {}
    virtual ~bridge() = default;
    
    void initialize()
    {
        LogManager_Function();
        PPConfig::instance().deserialize
        (
         BaseSystem::instance().getResourceDirectoryPath() / "ppconfig.default.json",
         false
        );
        LogManager::instance().updateOperationMode();
        m_storage       = boost::make_shared<JMupsJobStorage>();
        m_cardReader    = AbstructFactory::instance().createCardReader();
        m_bTrainingMode = false;
        m_bUseDigiSignInTrainingMode = false;
    }
    
    void applyJMupsOpenTerminalResponse(const std::string& json)
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        js->setTerminalInfo(json_t::from_str(json));
    }

    std::string getJMupsOpenTerminalInfo() const
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        return js->getTerminalInfo().str();
    }
    
    void setJMupsNfcMasterData(const std::string& json)
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        js->setNfcMasterData(json_t::from_str(json));
    }

    std::string getJMupsNfcMasterData() const
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        return js->getNfcMasterData().str();
    }
    
    std::string getJMupsTradeResultDataIfExists() const
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        auto&& tr = js->getTradeResultData();
        if(tr.is_null_or_empty()){
            return std::string();
        }
        return tr.str();
    }

    void execute
    (
     const std::string&         jobName,
     PPDispatchCallback_t       onDispatch,
     PPDispatchCancelCallback_t onDispatchCancel,
     PPFinishCallback_t         onFinish
     )
    {
        LogManager_Function();
        
        m_onFinish = onFinish;
        m_job = m_jobCreator.create(jobName);
        m_job->setStorage(m_storage);
        if(m_job->isCardReaderRequired()){
            LogManager_AddInformation("カードリーダが必要な業務");
            m_job->setCardReader(m_cardReader);
        }
        m_job->setTrainingMode(m_bTrainingMode);
        if(m_bTrainingMode){
            m_job->setUseDigiSignInTrainingMode(m_bUseDigiSignInTrainingMode);
        }
        
        if(m_job->isJobTimeoutRequired()){
            startJobTimer(PPConfig::instance().get<int>("hps.jobTimeout"));
        }
        
        m_subscription = m_job->getObservable
        (
         /* onDispatch */
         [=](const std::string& inParam, std::string& outParam){
             LogManager_AddInformation("onDispatch entry");
             auto json = json_t::from_str(inParam);
             if(json.get<std::string>("command") == "notice"){
                 if(json.get<std::string>("message") == "disableAborting"){
                     stopJobTimer();
                 }
             }
             auto _ioParam = IOSUtilities::convert(outParam);
             const bool bContinue = onDispatch(IOSUtilities::convert(inParam),&_ioParam);
             outParam = IOSUtilities::convert(_ioParam);
             return bContinue;
         },
         /* onDispatchCancel */
         [=](){
             LogManager_AddInformation("onDispatchCancel");
             onDispatchCancel();
         })
        .subscribe([=](const_json_t resp){
            LogManager_AddInformation("observable -> success");
            auto func = m_onFinish.modifyBlockWithResult<PPFinishCallback_t>([](auto& f){
                auto _func = f;
                f = nil;
                return _func;
            });
            if(func){
                m_storage = m_job->getStorage();
                auto rtxt = IOSUtilities::convert(resp.clone().str());
                callFinished(func, rtxt);
            }
         }, [=](std::exception_ptr ep){
             LogManager_AddInformation("observable -> error");
             auto func = m_onFinish.modifyBlockWithResult<PPFinishCallback_t>([](auto& f){
                 auto _func = f;
                 f = nil;
                 return _func;
             });
             if(func){
                 try{std::rethrow_exception(ep);}
                 catch(pprx::ppexception& ex){
                     auto rtxt = IOSUtilities::convert(ex.what());
                     callFinished(func, rtxt);
                 }
                 catch(std::exception& ex){
                     json_t j;
                     j.put("status", "error");
                     j.put("result.where", "internal");
                     j.put("result.detail.description", ex.what());
                     auto rtxt = IOSUtilities::convert(j.str());
                     callFinished(func, rtxt);
                 }
             }
         },
         [=](){
             // 業務完了後、取引情報をクリアする
             boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage)->setTradeResultData(const_json_t());
             // リクルートの要望で、disconnectしない処理
             //disconnectCardReader();
             LogManager_AddInformation("observable -> completed");
         });
    }
    
    void abort(PaymentJob::AbortReason reason)
    {
        LogManager_Function();
        m_job->abort(reason);
    }
    
    void callFinished(PPFinishCallback_t callback, NSString* result)
    {
        LogManager_Function();
        LogManager_AddFullInformationF("<<OnFinish>> :\n%s", std::string([result UTF8String]).c_str());
        stopJobTimer();
        BaseSystem::instance().post([=](){
            m_job.reset();
            callback(result);
            m_subscription.unsubscribe();
        });
    }
    
    std::string getTerminalID() const
    {
        LogManager_Function();
        auto js = boost::dynamic_pointer_cast<JMupsJobStorage>(m_storage);
        auto terminalId = js->getTerminalInfo().get_optional<std::string>("sessionObject.termJudgeNo");
        return terminalId ? *terminalId : "";
    }
    
    bool isNeedTaxOtherAmount() const
    {
        LogManager_Function();
        
        auto ja = dynamic_cast<const JMupsAccess*>(m_job.get());
        return ja->getTradeStartInfo().get<std::string>("normalObject.dllPatternData.inputTaxOther") == "1";
    }

    bool isNeedProductCode() const
    {
        LogManager_Function();
        
        auto ja = dynamic_cast<const JMupsAccess*>(m_job.get());
        return ja->getTradeStartInfo().get<std::string>("normalObject.dllPatternData.inputProductCode") == "1";
    }

    void checkMasterData()
    {
        LogManager_Function();
        
        m_job = m_jobCreator.create("JMups.NFCCheckMasterData");
        m_job->setStorage(m_storage);
        m_job->setCardReader(m_cardReader);
        m_job->setTrainingMode(m_bTrainingMode);
        if(m_bTrainingMode){
            m_job->setUseDigiSignInTrainingMode(m_bUseDigiSignInTrainingMode);
        }
        m_job->mainProcessChildJob(json_t())
        .subscribe(
            [=](auto) {
                LogManager_AddInformation("on next case in.");
            },
            [=](auto err) {
                LogManager_AddInformation("on error case in.");
            },
            [=]() {
                LogManager_AddInformation("on complete case in.");
            }
        );
    }

    void setTrainingMode(bool bTrainingMode)
    {
        LogManager_Function();
        
        m_bTrainingMode = bTrainingMode;
    }
    
    void setUseDigiSignInTrainingMode(bool bUse)
    {
        LogManager_Function();
        
        m_bUseDigiSignInTrainingMode = bUse;
    }
    
    bool isTrainingMode() const
    {
        LogManager_Function();
        
        return m_bTrainingMode;
    }
    
    void clearCardReaderDisplay()
    {
        LogManager_Function();
        m_cardReader->clearCardreaderDisplay()
        .subscribe([](auto){}, [](auto){}, [](){});
    }
    
    void disconnectCardReader()
    {
        LogManager_Function();
        m_cardReader->disconnect()
        .subscribe([=](auto){
            LogManager_AddInformation("disconnect -> success");
        }, [=](auto){
            LogManager_AddError("disconnect -> error");
        }, [=](){
        });
    }
    
    NSString* digiSignImageToHexString(CGImageRef image, NSString* modulus, NSString* exponent)
    {
        std::stringstream dsts;
        std::stringstream srcs;
        {
            auto resized = resizeImageAndDecolorToGrayScale(image, 178, 44);
            const auto width            = ::CGImageGetWidth(resized);
            const auto height           = ::CGImageGetHeight(resized);
            const auto rowBytes         = ::CGImageGetBytesPerRow(resized);
            const auto bitsPerPixel     = ::CGImageGetBitsPerPixel(resized);
            const auto bitsPerComponent = ::CGImageGetBitsPerComponent(resized);
            const auto bytesPerPixel    = bitsPerPixel / bitsPerComponent;

            CGDataProviderRef provider = ::CGImageGetDataProvider(resized);
            NSData* data = (id)::CFBridgingRelease(::CGDataProviderCopyData(provider));
            
            if(bytesPerPixel == 1){
                WindowsBitmapFileBuilder<uint8_t>().grayImageToBinBitmap
                (
                 srcs,
                 reinterpret_cast<const uint8_t*>([data bytes]),
                 static_cast<int>(width), static_cast<int>(height), static_cast<int>(rowBytes),
                 [](auto pix){
                     return pix != 0xFF;
                 });
            }
            else if(bytesPerPixel == 2){
                WindowsBitmapFileBuilder<uint16_t>().grayImageToBinBitmap
                (
                 srcs,
                 reinterpret_cast<const uint16_t*>([data bytes]),
                 static_cast<int>(width), static_cast<int>(height), static_cast<int>(rowBytes),
                 [](auto pix){
                     return pix != 0xFFFF;
                 });
            }
            else{
                LogManager_AddErrorF("unsupported bytes per pixel %d", bytesPerPixel);
            }
            ::CFRelease(resized);
        }
        
#if defined(DEBUG)
        {
            auto paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            auto documentsDirectory = [paths objectAtIndex:0];
            auto bmpfile = [documentsDirectory stringByAppendingPathComponent:@"tset.bmp"];
            
            std::ofstream obmp([bmpfile UTF8String]);
            boost::iostreams::copy(srcs, obmp);
            srcs.seekg(0);
        }
#endif
        
        {
            RSA* rsa = ::RSA_new();
            ::BN_hex2bn(&rsa->n, [modulus UTF8String]);
            ::BN_hex2bn(&rsa->e, [exponent UTF8String]);
            const auto blockSize = ::RSA_size(rsa);
            /*
             RSA_PKCS1_PADDINGの場合、ソースの最大バイト数は RSA_size() -11 にしなければならない。
             https://www.openssl.org/docs/man1.0.2/crypto/RSA_public_encrypt.html
             */
            std::vector<uint8_t> raw(blockSize - 11);
            std::vector<uint8_t> enc(blockSize);
            
            while(!srcs.eof()){
                srcs.read(reinterpret_cast<char*>(raw.data()), raw.size());
                const auto readBytes = srcs.gcount();
                
                const auto writtenBytes = ::RSA_public_encrypt(
                                 static_cast<int>(readBytes),
                                 raw.data(),
                                 enc.data(),
                                 rsa,
                                 RSA_PKCS1_PADDING);
                if(writtenBytes == -1){
                    LogManager_AddErrorF("%s", ::ERR_error_string(::ERR_get_error(), nullptr));
                    break;
                }
                boost::algorithm::hex
                (
                 enc.begin(),
                 enc.begin() + writtenBytes,
                 std::ostream_iterator<char>(dsts));
            }
            ::RSA_free(rsa);
        }
        
        return [NSString stringWithUTF8String:dsts.str().c_str()];
    }
};


@interface PPBridge()
@property() boost::shared_ptr<bridge> bridge_;
@property() BOOL bTrainingMode_;
@end

@implementation PPBridge


+ (PPBridge*)sharedInstance
{
    static dispatch_once_t  onceToken;
    static PPBridge*        staticInstance = nil;
    
    dispatch_once(&onceToken, ^{
        staticInstance = [[PPBridge alloc] init];
    });
    
    return staticInstance;
}

-(id)init
{
    self = [super init];
    if(self == nil) return nil;

    self.bridge_ = boost::make_shared<bridge>();
    self.bridge_->initialize();
    self.bTrainingMode_ = NO;

    return self;
}

- (void)dealloc
{
}

- (void)updateConfigure:(NSString*)desc
                   json:(NSString*)json
{
    PPConfig::instance().updateConfigure([desc UTF8String], [json UTF8String]);
    LogManager::instance().updateOperationMode();
    if(PPConfig::instance().get<bool>("debug.bResetTMSPasswords")){
        KeyChainAccess::deleteAllTmsPassword();
    }
}

- (void)clearCardReaderDisplay
{
    self.bridge_->clearCardReaderDisplay();
}

- (void)disconnectCardReader
{
    self.bridge_->disconnectCardReader();
}

- (void)setTrainingMode:(BOOL)bTrainigMode
{
    self.bridge_->setTrainingMode(bTrainigMode ? true : false);
}

- (void)setUseDigiSignInTrainingMode:(BOOL)bUse
{
    self.bridge_->setUseDigiSignInTrainingMode(bUse ? true : false);
}

- (BOOL)isTrainingMode
{
    return self.bridge_->isTrainingMode() ? YES : NO;
}

- (void)applyJMupsOpenTerminalResponse:(NSString*)json
{
    self.bridge_->applyJMupsOpenTerminalResponse([json UTF8String]);
}

- (NSString*)getJMupsOpenTerminalInfo
{
    return [NSString stringWithUTF8String:self.bridge_->getJMupsOpenTerminalInfo().c_str()];
}

- (void)setJMupsNfcMasterData:(NSString*)json
{
    self.bridge_->setJMupsNfcMasterData([json UTF8String]);
}

- (NSString*)getJMupsNfcMasterData
{
    return [NSString stringWithUTF8String:self.bridge_->getJMupsNfcMasterData().c_str()];
}

-(NSString*)getJMupsTradeResultDataIfExists
{
    auto data = self.bridge_->getJMupsTradeResultDataIfExists();
    if(data.empty()) return nil;
    return [NSString stringWithUTF8String:data.c_str()];
}

- (void)executeJob:(NSString*)jobName
        onDispatch:(PPDispatchCallback_t)onDispatch
  onDispatchCancel:(PPDispatchCancelCallback_t)onDispatchCancel
          onFinish:(PPFinishCallback_t)onFinish
{
    self.bridge_->execute([jobName UTF8String], onDispatch, onDispatchCancel, onFinish);
}

- (NSString*)hexStringFromDigiSignImage:(CGImageRef)image
                                modulus:(NSString*)modulus
                               exponent:(NSString*)exponent
{
    return self.bridge_->digiSignImageToHexString(image, modulus, exponent);
}

- (void)abort
{
    self.bridge_->abort(PaymentJob::AbortReason::UserRequest);
}

- (NSString*)getTerminalID
{
    return [NSString stringWithUTF8String:self.bridge_->getTerminalID().c_str()];
}

- (BOOL)isNeedTaxOtherAmount
{
    return self.bridge_->isNeedTaxOtherAmount() ? YES : NO;
}

- (BOOL)isNeedProductCode
{
    return self.bridge_->isNeedProductCode() ? YES : NO;
}

- (void)checkMasterData
{
    self.bridge_->checkMasterData();
}

@end




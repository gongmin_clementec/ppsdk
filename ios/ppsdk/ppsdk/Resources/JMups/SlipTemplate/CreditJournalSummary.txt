#
# クレジット売上集計印字（簡略）
# R227, R235
#

$FONT(Normal)

$IF_EXIST(is_TRAINING){
_  "**********トレーニング**********"  _;
}

$LOGO_IMAGE()

_              $(TITLE)              _;

$IF_EXIST(is_REPRINT){
_           "****再印字****"          _;
}

______________________________________;
"加盟店名"              $(MERCHANT_NAME);
_                    $(MERCHANT_TELNO);

$TABLE(2:1,1:L,L){
"売場"           "係員";
_                 _;
}
#______________________________________;
"出力日時"                       $(DATE);
______________________________________;
"端末番号"                    $(TERM_NO);
______________________________________;
$IF_EXIST(_INTERMEDIATE){
    "中間計期間FROM"     $(DR_DATE_FROM);
    "          TO"       $(DR_DATE_TO);
}
$IF_EXIST(_NOT_INTERMEDIATE){
    "日計期間FROM"       $(DR_DATE_FROM);
    "        TO"         $(DR_DATE_TO);
}
______________________________________;
$IF_EXIST(_INTERMEDIATE){
    "［中間計総合計］";
}
$IF_EXIST(_NOT_INTERMEDIATE){
    "［日計総合計］";
}
______________________________________;
"端末合計";
$TABLE(3:2,3,5:L,R,R){
    "一括"    "$(T_1X_NUMBER)件"      $(T_1X_AMOUNT);
    "ボー"    "$(T_2X_NUMBER)件"      $(T_2X_AMOUNT);
    "ボ併"    "$(T_3X_NUMBER)件"      $(T_3X_AMOUNT);
    "分割"    "$(T_6X_NUMBER)件"      $(T_6X_AMOUNT);
    "リボ"    "$(T_8X_NUMBER)件"      $(T_8X_AMOUNT);
    _____    _____________________   ______________;
    "売上"    "$(T_SA_NUMBER)件"      $(T_SA_AMOUNT);
    "返品"    "$(T_RE_NUMBER)件"      $(T_RE_AMOUNT);
    "取消"    "$(T_CA_NUMBER)件"      $(T_CA_AMOUNT);
    _____    _____________________   ______________;
    "合計"    "$(T_TOTAL_NUMBER)件"   $(T_TOTAL_AMOUNT);
}
;
"［カード会社別合計］";
______________________________________;
$FOREACH(list){
    $(CO_NAME);
    $TABLE(3:2,3,5:L,R,R){
        "一括"    "$(C_1X_NUMBER)件"      $(C_1X_AMOUNT);
        "ボー"    "$(C_2X_NUMBER)件"      $(C_2X_AMOUNT);
        "ボ併"    "$(C_3X_NUMBER)件"      $(C_3X_AMOUNT);
        "分割"    "$(C_6X_NUMBER)件"      $(C_6X_AMOUNT);
        "リボ"    "$(C_8X_NUMBER)件"      $(C_8X_AMOUNT);
        _____    _____________________   ______________;
        "売上"    "$(C_SA_NUMBER)件"      $(C_SA_AMOUNT);
        "返品"    "$(C_RE_NUMBER)件"      $(C_RE_AMOUNT);
        "取消"    "$(C_CA_NUMBER)件"      $(C_CA_AMOUNT);
        _____    _____________________   ______________;
        "合計"    "$(C_TOTAL_NUMBER)件"   $(C_TOTAL_AMOUNT);
    }
}
#______________________________________;
_ "";

$IF_EXIST(is_TRAINING){
_  "**********トレーニング**********"  _;
}

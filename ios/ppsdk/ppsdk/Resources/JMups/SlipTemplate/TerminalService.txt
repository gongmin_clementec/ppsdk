#
# 端末サービス設定
# R283
#

$FONT(Normal)

$LOGO_IMAGE()

_              $(TITLE)              _;
______________________________________;
"出力日時"                       $(DATE);
"端末番号"                    $(TERM_NO);
______________________________________;
$FOREACH(COMPANY_LIST){
        $(SERVICE_DIV_NAME)                             _;
        "サービス契約者ID"         $(SERVICE_CONTRACTOR_ID);
        "サービス契約者名"                                _;
         _                     $(SERVICE_CONTRACTOR_NAME);
        _________________________________________________;
}
_ "";

{
	"resultCode": "0",
	"normalObject": {
		"printInfo": {
			"device": {
				"name": "prt",
				"value": {
					"values": [{
						"JOURNAL_DATE": "XXXX/XX/XX",
						"T_8X_AMOUNT": "\\9,999,999",
                        "TERM_NO": "XXXXX-XXX-XXXXX",
						"T_TOTAL_AMOUNT": "-\\9,999,999",
						"MERCHANT_TELNO": "XX-XXXX-XXXX",
						"T_8X_NUMBER": "0",
						"DR_DATE_FROM": "XX/XX/XX XX:XX:XX",
						"T_SA_AMOUNT": "\\9,999,999",
						"T_RE_AMOUNT": "-\\9,999,999",
                        "is_REPRINT": "false",
						"T_CA_NUMBER": "9999",
						"T_6X_NUMBER": "9999",
						"T_3X_NUMBER": "9999",
						"is_TRAINING": "true",
						"DR_DATE_TO": "XX/XX/XX XX:XX:XX",
						"T_2X_AMOUNT": "\\9,999,999",
						"T_1X_AMOUNT": "\\9,999,999",
						"T_SA_NUMBER": "9999",
                        "MERCHANT_NAME": "XXXXXXXXXXXXXXXXXXXXXX",
                        "T_RE_NUMBER": "9999",
                        "T_CA_AMOUNT": "-\\9,999,999",
                        "DATE": "##DATE_TIME_NOW##",
                        "T_3X_AMOUNT": "\\9,999,999",
                        "T_6X_AMOUNT": "\\9,999,999",
                        "TITLE": "NFCペイメント日計票",
                        "T_2X_NUMBER": "9999",
                        "T_1X_NUMBER": "9999",
                        "T_TOTAL_NUMBER": "9999",
                        "logoServiceInfo": {
                            "logoSwitchDate": "2017/07/25",
                            "logoImageServiceOnoffDiv": "1"
                        },
						"list": [{
							"C_8X_AMOUNT": "\\9,999,999",
							"C_CA_NUMBER": "9999",
							"C_6X_NUMBER": "9999",
							"C_3X_NUMBER": "9999",
							"list_partners": [],
							"C_2X_AMOUNT": "\\9,999,999",
							"C_1X_AMOUNT": "\\9,999,999",
							"C_SA_NUMBER": "9999",
							"C_RE_NUMBER": "9999",
							"CO_NAME": "KID:カード会社1",
							"C_CA_AMOUNT": "-\\9,999,999",
							"C_3X_AMOUNT": "\\9,999,999",
							"C_6X_AMOUNT": "\\9,999,999",
							"C_TOTAL_NUMBER": "9999",
							"C_TOTAL_AMOUNT": "\\9,999,999",
							"C_SA_AMOUNT": "\\9,999,999",
							"C_2X_NUMBER": "9999",
							"C_8X_NUMBER": "9999",
							"C_1X_NUMBER": "9999",
							"C_RE_AMOUNT": "-\\9,999,999",
							"value": [{
								"SLIP_NO": "99999",
								"CARD_NO": "999999XXXXXX9999",
								"TRADE": "売上",
								"A_NO": "999999",
								"PAY_DIVISION": "一括",
								"EXP_DATE": "XX/XX",
								"AMOUNT": "\\9,999,999",
								"TRADE_DATE": "XX/XX/XX XX:XX:XX",
								"CARD_CO_CODE": null
							}]
						}]
					}],
					"type": ["RI12"]
				}
			}
		},
		"outputDate": "2017-07-25 13:30:11.298"
	}
}

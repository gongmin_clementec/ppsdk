{
	"resultCode": "0",
	"normalObject": {
		"printInfo": {
			"device": {
				"name": "prt",
				"value": {
					"values": [{
						"JOURNAL_DATE": "XXXX/XX/XX",
						"T_CA_NUMBER": "9999",
						"TERM_NO": "XXXXX-XXX-XXXXX",
						"is_TRAINING": "true",
						"DR_DATE_TO": "XX/XX/XX XX:XX:XX",
						"T_SA_NUMBER": "9999",
						"T_TOTAL_AMOUNT": "\\9,999,999",
						"list": [{
							"CO_NAME": "金融機関名称１",
							"C_1X_NUMBER": "9999",
							"C_1X_AMOUNT": "\\9,999,999"
						}],
						"MERCHANT_NAME": "XXXXXXXXXXXXXXXXXXXXXX",
						"T_RE_NUMBER": "9999",
						"T_CA_AMOUNT": "-\\9,999,999",
						"DATE": "##DATE_TIME_NOW##",
						"logoServiceInfo": {
							"logoSwitchDate": "2019/02/05",
							"logoImageServiceOnoffDiv": "1"
						},
						"MERCHANT_TELNO": "XX-XXX-XXXXX",
						"TITLE": "中国銀聯カード日計票",
						"DR_DATE_FROM": "XX/XX/XX XX:XX:XX",
						"T_SA_AMOUNT": "\\9,999,999",
						"T_RE_AMOUNT": "-\\9,999,999",
						"is_REPRINT": "true",
						"T_TOTAL_NUMBER": "9999"
					}],
					"type": ["R241"]
				}
			}
		},
		"outputDate": "2019-02-05 21:12:45.707"
	}
}
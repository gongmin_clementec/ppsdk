{
	"resultCode": "0",
	"normalObject": {
		"tradeResult": {
			"CARD_NO": "999999XXXXXX9999",
			"SLIP_NO": "99999",
			"A_NO": "XXXXXX",
			"TRADE": "3",
			"TERM_NO": "XXXXXXXXXXXXX",
			"DISCOUNT_PRICE": 0,
			"USE_DONE_TIMES": 0,
			"SETTLEDAMOUNT": "9999999",
			"KID": "105",
			"COUPON_2": "",
			"COUPON_1": "",
			"COUPON_ID": "",
			"MAXIMUM_USE_POSS_TIMES": 0,
			"DOMESTIC_RESPONSE_CODE": "   ",
			"COUPON_MEMBER_ID": "",
			"PROCESS_SEQ": "999999",
			"TRADE_DATE": "##DATE_TIME_NOW##"
		},
		"slipNo": "99999",
		"isDoubleTrade": "false",
		"digiReceiptDiv": "##digiReceiptDiv##",
		"digiSignPubKeyModulus": "00be363ecea583cabf71a669c06ca56fa3ecea40464f40292df0ca231bb4cf5797de0078c8d47c0ae4469836cd3ec9800b908327d1087df8368304e84258d7ba5c010a2319c7b5fe42cdf1e25cc81792441bd0d7f9395294fa6c72874486110e2bccf729ac89e12c32b77a3e2ff6ccb5b951af2fe55b23b7913908fb3a86ade0ff3ccd821c1516048f6a7e9ae8898441f522c279ed6fb229c0986c194017e9479fedc20ad09a42804bd50675e616c2337bdf1552458f4d6ec5537da32600559e497711de1fc6e01fc466588a004f9ef9871a95549215a1dd92d029143ce0b9548dd060a24b713e6651217104c5d3baa35fed770b081a5b890d28098a1156f224e1",
		"printInfo": {
			"device": {
				"name": "prt",
				"value": {
					"values": [{
						"CARD_NO": "999999XXXXXX9999",
						"TRADE": "ｵｰｿﾘ予約",
						"EXP_DATE": "XX/XX",
						"TERM_NO": "XXXXX-XXX-XXXXX",
						"CUP_SEND_DATE": "##CUP_SEND_DATE_TIME##",
						"ORIGINAL_SLIP_NO": null,
						"AMOUNT": "\\9,999,999",
						"RECEIPT": "金融機関控",
						"MERCHANT_TELNO": "XX-XXXX-XXXX",
						"RECEIPT_DIV": "4",
						"SIGN": "",
						"is_REPRINT": "false",
						"SLIP_NO": "99999",
						"A_NO": "XXXXXX",
						"INFORM1": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
						"is_TRAINING": "true",
						"INFORM2": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
						"CUP_NO": "000001",
						"INFORM3": null,
						"CARD_CO": "XXXXXXXXXXXXXXXXXXX",
						"MERCHANT_NAME": "XXXXXXXXXXXXXXXXXXXXXX",
						"DATE": "##DATE_TIME_NOW##",
						"logoServiceInfo": {
							"logoSwitchDate": "2019/01/25",
							"logoImageServiceOnoffDiv": "1"
						},
						"PROCESS_SEQ": "999999",
						"TITLE": "中国銀聯カード売上票",
						"TRADE_DIV": "3"
					}, {
						"CARD_NO": "999999XXXXXX9999",
						"TRADE": "ｵｰｿﾘ予約",
						"EXP_DATE": "XX/XX",
						"TERM_NO": "XXXXX-XXX-XXXXX",
						"CUP_SEND_DATE": "##CUP_SEND_DATE_TIME##",
						"ORIGINAL_SLIP_NO": null,
						"AMOUNT": "\\9,999,999",
						"RECEIPT": "お客様控",
						"MERCHANT_TELNO": "XX-XXXX-XXXX",
						"RECEIPT_DIV": "1",
						"is_REPRINT": "false",
						"SLIP_NO": "99999",
						"A_NO": "XXXXXX",
						"INFORM1": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
						"is_TRAINING": "true",
						"INFORM2": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
						"CUP_NO": "000001",
						"INFORM3": null,
						"CARD_CO": "XXXXXXXXXXXXXXXXXXX",
						"MERCHANT_NAME": "XXXXXXXXXXXXXXXXXXXXXX",
						"DATE": "##DATE_TIME_NOW##",
						"logoServiceInfo": {
							"logoSwitchDate": "2019/01/25",
							"logoImageServiceOnoffDiv": "1"
						},
						"PROCESS_SEQ": "999999",
						"TITLE": "中国銀聯カード売上票",
						"TRADE_DIV": "3"
					}, {
						"CARD_NO": "999999XXXXXX9999",
						"SLIP_NO": "99999",
						"A_NO": "XXXXXX",
						"TRADE": "ｵｰｿﾘ予約",
						"EXP_DATE": "XX/XX",
						"TERM_NO": "XXXXX-XXX-XXXXX",
						"is_TRAINING": "true",
						"CUP_NO": "000001",
						"CARD_CO": "XXXXXXXXXXXXXXXXXXX",
						"MERCHANT_NAME": "XXXXXXXXXXXXXXXXXXXXXX",
						"CUP_SEND_DATE": "##CUP_SEND_DATE_TIME##",
						"ORIGINAL_SLIP_NO": null,
						"DATE": "##DATE_TIME_NOW##",
						"AMOUNT": "\\9,999,999",
						"logoServiceInfo": {
							"logoSwitchDate": "2019/01/25",
							"logoImageServiceOnoffDiv": "1"
						},
						"RECEIPT": "加盟店控",
						"MERCHANT_TELNO": "XX-XXXX-XXXX",
						"PROCESS_SEQ": "999999",
						"RECEIPT_DIV": "2",
						"TITLE": "中国銀聯カード売上票",
						"is_REPRINT": "false",
						"TRADE_DIV": "3"
					}],
					"type": ["R094", "R091", "R092"]
				}
			}
		},
		"digiSignPubKeyExponent": "010001"
	}
}

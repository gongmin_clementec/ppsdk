/**
 * 「ICカード」「EMV仕様」の仕様・振る舞いを表すクラスです。
 * @class 「ICカード」「EMV仕様」の仕様・振る舞いを表すクラス
 * @extends RuleBaseModel
 */
var IcCardMediaModel = function() {
	this.modelRuleName = "IcCardMediaModel";
	this.flowType = "iccard";

	this.procedureSet = {};
	this.procedureSet.Instance = this;

	//フロー定義
	/**
	 * @property EMVの処理フローです。
	 */
	this.flow = {};
	this.firstFlow = "EMV01";
	this.flow["__RESET__"] = [
		{"type":"dialogconfirm","obj":"reset"}
		,{"type":"end","obj":"-"}
	];
	this.flow["__RETRY__"] = [
		 {"type":"dialogconfirm","obj":"retry"}
		 ,{"type":"move","obj":"__RESET__"}
	];
	this.flow["__ERROR__"] = [
		{"type":"release", "obj":"icCardError"}
	 ];


	/**
	 * ブランチメソッドです。業務モデルからも参照されます。
	 */
	this.branch = {};
	this.branch.Instance = this;
	this.branch.Instance.branchGenerate = {};
	this.branch.Instance.branchGenerate.Instance = this;

	///////////////////////////////////////////////////////////////////////////////////

	/*
	 * EMVルールフロー
	 */
	/**
	 *  ICカード挿入
	 */
	this.procedureSet["emv-init_RECIEVES_DNX-iccard_input-cardinsert"] = function(session, m ,v) {
		return {
			"flowControl":{
				"name":"start"
			}
		};
	};

	/**
	 * アプリケーション候補リスト作成
	 */
	this.flow.EMV01 = [
		{"type":"emv","obj":"init"}
		,{"type":"emv","obj":"EMV01_selectCommandCreate"}
		,{"type":"branch","obj":"EMV01_switchSelect"}
	];

	/**
	 * NODEIN 事前処理
	 */
	this.procedureSet["NODEIN-emv-init"] = function(session,m,v){

		session.transaction.selectAidList = new Array();
		session.transaction.applicationList = new Array();
		session.transaction.applicationCount = 0;
		session.transaction.selectAidListIndex = -1;
		session.transaction.rdrcdCmdIndex = -1;
		session.transaction.gdCmdIndex = -1;

		// プロパティからトランザクションへアプリケーション情報をコピー
		for (var i = 0; i < session.property.aidList.length; i++) {
			session.transaction.selectAidList.push(Utility.copy(session.property.aidList[i]));
		}

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * NODEIN PSEメソッドファイル読み込み
	 */
	this.procedureSet["NODEIN-emv-EMV01_selectCommandCreate"] = function(session,m,v){
		// プロパティからファイル名取得
		var filename = EMV_SELECT_FILENAME;
		var asciiFilename = function(filename) {
			var s = "";
			var charCode;
			for (var i = 0; i < filename.length; i++) {
				charCode = filename.charCodeAt(i);
				if (charCode < 16) {
					s += "0";
				}
				s += charCode.toString(16);
			}
			return s;
		}(filename);

		var length = function (length) {
			if (length < 16) {
				return "0" + length.toString(16);
			} else {
				return length.toString(16);
			}
		}(filename.length);

		var selectCmd = "00a40400" + length + asciiFilename + "00";

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":selectCmd
					}
				}
			}
		};
	};

	/**
	 * PSE ファイル読み込み結果取得
	 */
	this.procedureSet["emv-EMV01_selectCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.selectCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * PSEファイル選択結果検証
	 */
	this.branch["EMV01_switchSelect"] = function(session) {

		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);

		switch(sw) {
			case "9000":	// 正常

				// 必須項目のチェック
				var tvListMap = this.Instance.analyzeTlv2(response, true);

				if (this.Instance.isMandatoryError(tvListMap.list, "6f")
						|| this.Instance.isMandatoryError(tvListMap.list, "84")
						|| this.Instance.isMandatoryError(tvListMap.list, "a5")
						|| this.Instance.isMandatoryError(tvListMap.list, "88")) {
					//  List of AID
					return "EMV12";
				}

				var sfi = parseInt(tvListMap.map["88"],16);

				if (sfi < 1 || 10 < sfi) {
					// SFI("88")が1～10以外
					// List of AID
					return "EMV12";
				}

				// PSEメソッド
				return "EMV02";

			case "6a81":	// カードブロックまたはSELECTコマンドをサポートしていない
				// エラー (msFallback判定)
				return "EMV46";

			default:
				// その他は List of AID
				return "EMV12";
		};
	};

	////////// PSE method //////////
	// PSE ReadRecord
	this.flow.EMV02 = [
		{"type":"emv","obj":"readRecordCommandCreate"}
		,{"type":"move","obj":"EMV03"}
	];

	/**
	 * NODEIN PSEメソッドでのREAD RECORDコマンド生成
	 */
	this.procedureSet["NODEIN-emv-readRecordCommandCreate"] = function(session,m,v){
		//var tvMap = {};

		// sfiを取得 (タグ"88"の下位5ビット + "100")
		var tvMap = this.Instance.analyzeTlv2(session.transaction.selectCmdRes, true).map;
		var sfi = parseInt(tvMap["88"],16).toString(2);

		for (var i = sfi.length; i < 8; i++) {
			sfi = "0" + sfi;
		}
		sfi = parseInt(sfi.substring(3,8)+"100",2).toString(16);
		if (sfi.length < 2) {
			sfi = "0" + sfi;
		}
		session.transaction.sfi = sfi;

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	this.flow.EMV03 = [
			{"type":"emv","obj":"readRecordCommandExcecute"}
			,{"type":"branch","obj":"EMV03_switchSelect"}
	];

	/**
	 * NODEIN PSEメソッドでのREAD RECORDコマンド実行
	 */
	this.procedureSet["NODEIN-emv-readRecordCommandExcecute"] = function(session,m,v){
		var sfi = session.transaction.sfi;

		// recordNoを取得
		if (Utility.isEmpty(session.transaction.recordNo)) {
			session.transaction.recordNo = 1;
		} else {
			session.transaction.recordNo += 1;
		}

		// recordNoを16進に変換
		var length = function (length) {
			if (length < 16) {
				return "0" + length.toString(16);
			} else {
				return length.toString(16);
			}
		}(session.transaction.recordNo);

		var rrCmd = "00b2" + length + sfi + "00";

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":rrCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN PSEメソッドでのREAD RECORDコマンド結果取得
	 */
	this.procedureSet["emv-readRecordCommandExcecute_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.rdrcdCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};


	/**
	 * PSEメソッドのReadRecoradの実行結果検証
	 */
	this.branch["EMV03_switchSelect"] = function(session) {

		var response = session.transaction.rdrcdCmdRes;

		var sw = response.substring(response.length - 4);

		switch(sw) {
			case "9000":	// 正常
				// 必須項目のチェック
				var tvListMap = this.Instance.analyzeTlv2(response, true);

				if (this.Instance.isMandatoryError(tvListMap.list, "70")
						|| this.Instance.isMandatoryError(tvListMap.list, "61")) {
					// アプリケーション候補リスト削除
					return "EMV11";
				}
				// 結果検証
				return "EMV05";
			case "6a83":	// レコードが見つからない
				// PSEメソッド結果検証
				return "EMV10";
			default:		// 上記以外 (想定外SW)
				// アプリケーション候補リスト削除
				return "EMV11";
		};
	};


	// アプリケーションリスト候補リスト追加
	/**
	 * アプリケーションリスト候補リスト追加
	 */
	this.flow.EMV05 = [
		{"type":"emv","obj":"EMV05_addApplicationList"}
		,{"type":"branch","obj":"EMV05_switchSelect"}
	];

	/**
	 * NODEIN アプリケーション候補リスト追加
	 */
	this.procedureSet["NODEIN-emv-EMV05_addApplicationList"] = function(session,m,v){

		var readRecoadResponse = session.transaction.rdrcdCmdRes;

		var tvListMap = this.Instance.analyzeTlv2(readRecoadResponse, true);
		var entryList = new Array();

		this.Instance.getTagList(tvListMap.list, entryList, "61");
		var childList;
		var child;
		var i;
		var j;
		var aidList = new Array();
		var edfEntry;
		var edfProperty;

		for (i = 0; i < entryList.length; i++) {
			childList = entryList[i].child;
			edfEntry = {};
			for (j = 0; j < childList.length; j++) {
				child = childList[j];
				switch (child.tag) {
					case "4f":
						edfProperty = "aid";	// ADF Name
						break;
					case "50":
						edfProperty = "label";	// Application Label
						break;
					case "87":
						edfProperty = "api";
						break;
					default:
						edfProperty = null;
						break;
				}
				if (edfProperty != null) {
					edfEntry[edfProperty] = child.value;
				}
			}

			if (edfEntry.hasOwnProperty("aid") && edfEntry.hasOwnProperty("label")) {
				// ADF Name、Application Labelが設定してある場合のみ追加
				edfEntry.value =  entryList[i].value;
				edfEntry.displayLabel = this.Instance.decode(edfEntry.label);
				aidList.push(edfEntry);
			} else {
				// PSEはエラー、追加せずに終了
				session.transaction.pseError = true;
				return {
						"flowControl":{
							"name":"flownext"
						}
				};
			}
		}
		var terminalAidList = session.transaction.selectAidList;
		for (i = 0; i < aidList.length; i++) {
			for (j = 0; j < terminalAidList.length; j++) {
				if (aidList[i].aid == terminalAidList[j].aid) {
					session.transaction.applicationList.push(
							$.extend(true,{}, terminalAidList[j], aidList[i]));// コピーしたものを追加
				}
			}
		}
		session.transaction.applicationCount = terminalAidList.length;

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * PSEメソッドのReadRecoradの実行結果検証
	 */
	this.branch["EMV05_switchSelect"] = function(session) {

		if (session.transaction.pseError) {
			// アプリケーション候補リスト削除
			return "EMV11";
		} else {

			return "EMV03";
		}
	};

	this.flow.EMV10 = [
		{"type":"branch","obj":"EMV10_switchSelect"}
	];

	/**
	 * PSEメソッド結果検証
	 * 候補リストに存在すれば最終選択、なければList of AID
	 */
	this.branch["EMV10_switchSelect"] = function(session) {

		if (session.transaction.applicationList.length == 0) {
			// List of AID
			return "EMV12";
		} else {
			// Final Selection
			return "EMV19";
		}
	};

	// アプリケーション候補リスト削除
	this.flow.EMV11 = [
		{"type":"emv","obj":"deletePSEMethodApplication"}
		,{"type":"move","obj":"EMV12"}
	];

	/**
	 * PSEメソッドで追加したアプリケーション候補リストを削除
	 */
	this.procedureSet["NODEIN-emv-deletePSEMethodApplication"] = function(session) {

		session.transaction.applicationList = new Array();
		session.transaction.applicationCount = 0;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	////////// List of AID //////////
	this.flow.EMV12 = [
		{"type":"emv","obj":"nextFlgFalse"}
		,{"type":"branch","obj":"EMV12_switchSelect"}
	];

	/**
	 * List of AID NextFlgにfalseを設定
	 */
	this.procedureSet["NODEIN-emv-nextFlgFalse"] = function(session,m,v){

		session.transaction.nextFlg = false;
		session.transaction.selectAidListIndex += 1;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * List of AID 実行判定
	 */
	this.branch["EMV12_switchSelect"] = function(session) {

		// 未判定のAIDリスト有無判定
		if (session.transaction.selectAidList.length > session.transaction.selectAidListIndex) {
			// List of AID コマンド実行
			return "EMV13";
		} else {
			// Final Selection
			return "EMV19";
		}
	};

	/**
	 * List of AID アプリケーション選択
	 */
	this.flow.EMV13 = [
		{"type":"emv","obj":"EMV13_selectCommandCreate"}
		,{"type":"branch","obj":"EMV13_switchSelect"}
	];

	/**
	 * List of AID AIDのSELECTコマンド
	 */
	this.procedureSet["NODEIN-emv-EMV13_selectCommandCreate"] = function(session,m,v){

		var filename = session.transaction.selectAidList[session.transaction.selectAidListIndex].aid;

		var length = function (length) {
			if (length < 16) {
				return "0" + length.toString(16);
			} else {
				return length.toString(16);
			}
		}(filename.length / 2);

		var selectCmd = null;
		if (session.transaction.nextFlg) {
			selectCmd = "00a40402" + length + filename + "00";
		} else {
			selectCmd = "00a40400" + length + filename + "00";
		}

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":selectCmd
				   }
				}
			}
		};
	};

	/**
	 * List of AID AIDのSELECTコマンド結果取得
	 */
	this.procedureSet["emv-EMV13_selectCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.selectCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * List of AID SELECTコマンド実行結果検証
	 */
	this.branch["EMV13_switchSelect"] = function(session) {
		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);

		//  List of AID SELECTコマンド実行結果検証
		switch(sw) {
			case "9000":	// 正常
			case "6283":	// アプリケーションブロック
				// ファイルは見つかった
				return "EMV14";

			case "6a81":	// カードブロックまたはSELECTコマンドをサポートしていない
				// エラー (msFallback判定)
				return "EMV46";

			case "6a82":	// ファイルが見つからない
				// List of AID
				return "EMV12";
			default:
				// 想定外のSW
				return "EMV12";
		};
	};

	/**
	 * List of AID SELECTコマンド実行結果
	 */
	this.flow.EMV14 = [
		{"type":"branch","obj":"EMV14_switchSelect"}
	];

	/**
	 *  List of AID SELECTコマンド実行結果処理
	 */
	this.branch["EMV14_switchSelect"] = function(session) {

		var response = session.transaction.selectCmdRes;
		var sw = response.substring(response.length - 4);

		// 必須項目のチェック
		var tvListMap = this.Instance.analyzeTlv2(response, true);

		var isMandatoryError = (this.Instance.isMandatoryError(tvListMap.list, "6f")
				|| this.Instance.isMandatoryError(tvListMap.list, "84")
				|| this.Instance.isMandatoryError(tvListMap.list, "a5"));

		if (isMandatoryError) {
			return "EMV12";
		}

		var dfname = tvListMap.map["84"];
		var aid = session.transaction.selectAidList[session.transaction.selectAidListIndex].aid;

		if (aid == dfname) {
			// 完全一致
			if (sw == "9000") {
				// 正常終了
				return "EMV16";// 候補追加
			} else {
				// アプリケーションブロック
				// List of AID
				return "EMV12";
			}
		} else if ((aid.length < dfname.length) && (aid == dfname.substring(0, aid.length))) {
			// 前方一致
			return "EMV17";
		} else {
			// 一致しない
			return "EMV12";
		}
	};

	/**
	 * List of AID アプリケーション候補リスト追加
	 */
	this.flow.EMV16 = [
		{"type":"emv","obj":"addApplicationList"}
		,{"type":"move","obj":"EMV12"}
	];

	/**
	 * NODEIN List of AID アプリケーション候補リスト追加
	 */
	this.procedureSet["NODEIN-emv-addApplicationList"] = function(session,m,v){

		var response = session.transaction.selectCmdRes;

		var tvMap = this.Instance.analyzeTlv2(response, true);

		var application = {};
		application.aid = tvMap.map["84"];
		application.label = tvMap.map["50"];
		application.displayLabel = this.Instance.decode(tvMap.map["50"]);
		application.api = tvMap.map["87"];
		application.value = response.substring(0, response.length - 4);

		// エラー無しの場合のみ追加
		if (!tvMap.duplicate
				&& (application.aid.length >= 10 && application.aid.length <= 32)
				&& ((Utility.isEmpty(application.api)) || application.api.length == 2)) {
			var terminalAidList = session.transaction.selectAidList;
			session.transaction.applicationList.push(
					$.extend(true, {}, terminalAidList[session.transaction.selectAidListIndex], application));// コピーしたものを追加

			session.transaction.applicationCount = session.transaction.applicationList.length;
		}

		return {
			"flowControl":{
				"name":"flownext"
			}
		};
	};

	this.flow.EMV17 = [
		{"type":"emv","obj":"nextFlgTrue"}
		,{"type":"branch","obj":"EMV17_switchSelect"}
	];

	/**
	 * List of AID NextFlgにtrueを設定
	 */
	this.procedureSet["NODEIN-emv-nextFlgTrue"] = function(session,m,v){

		session.transaction.nextFlg = true;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * Multiple Occurrence判定
	 */
	this.branch["EMV17_switchSelect"] = function(session) {

		var selectionIndicator = session.transaction.selectAidList[session.transaction.selectAidListIndex].selectIndexDiv;
		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);

		if (sw == "9000" && selectionIndicator == "0") {
			// SW 9000で選択指標区分0（許可）のみ追加
			return "EMV18";		// 候補追加
		} else {
			return "EMV13";		// SELECTコマンド実行
		}
	};

	this.flow.EMV18 = [
		{"type":"emv","obj":"addApplicationList"}
		,{"type":"move","obj":"EMV18_2"}
	];

	/**
	 * List of AID アプリケーション選択(NEXT)
	 */
	this.flow.EMV18_2 = [
		{"type":"emv","obj":"EMV13_selectCommandCreate"}
		,{"type":"branch","obj":"EMV18_2_switchSelect"}
	];

	/**
	 * List of AID SELECTコマンド実行結果検証
	 */
	this.branch["EMV18_2_switchSelect"] = function(session) {
		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);
		var sw1 = sw.substring(0, 2);

		//  List of AID SELECTコマンド実行結果検証
		if (sw == "9000" || sw1 == "62" || sw1 == "63") {
			return "EMV14";
		} else {
			return "EMV12";
		}
	};


	////////// アプリケーション最終選択 //////////
	/**
	 * アプリケーション最終選択
	 */
	this.flow.EMV19 = [
		{"type":"branch","obj":"EMV19_switchSelect"}
	];

	/**
	 * アプリケーション選択・アプリケーション最終選択
	 */
	this.branch["EMV19_switchSelect"] = function(session) {
		if (Utility.isEmpty(session.transaction.application)) {
			// 選択アプリケーションなし

			if(session.transaction.applicationList.length == 0) {
				// 候補無し (msFallback判定)
				return "EMV46";

			} else if (session.transaction.applicationList.length == 1) {
				// 候補1件
				var application = session.transaction.applicationList[0];

				var appPriorityIndicator = application.api;
				if (Utility.isEmpty(appPriorityIndicator)) {
					return "EMV23";		// アプリケーション最終選択コマンド発行
				}

				// 優先度指標子のb8を取得
				appPriorityIndicator = parseInt(appPriorityIndicator,16).toString(2);

				if (appPriorityIndicator.length < 8) {
					appPriorityIndicator = "0";
				} else {
					appPriorityIndicator = appPriorityIndicator.substring(0,1);
				}

				if (appPriorityIndicator == "1") {
					return "EMV26";		// アプリケーション選択
				} else {
					return "EMV23";		// アプリケーション最終選択コマンド発行
				}

			} else {
				// 候補複数あり
				return "EMV21";		// アプリケーションソート
			}
		} else {
			// // 選択アプリケーションあり
			return "EMV23";			// アプリケーション最終選択コマンド発行
		}
	};

	this.flow.EMV21 = [
		{"type":"emv","obj":"applicationListSort"}
		,{"type":"move","obj":"EMV26"}
	];

	/**
	 * NODEIN アプリケーションリストソート
	 */
	this.procedureSet["NODEIN-emv-applicationListSort"] = function(session,m,v){

		session.transaction.applicationList.sort(function(a,b) {
			// 下位4ビットを優先度指標子としてソート
			var sortkey1 = Utility.isEmpty(a.api) ? 0 : parseInt(a.api.substring(1,2),16);
			var sortkey2 = Utility.isEmpty(b.api) ? 0 : parseInt(b.api.substring(1,2),16);
			if (sortkey2 == 0) {
				return -1;
			} else if (sortkey1 == 0) {
				return 1;
			} else if (sortkey1 > sortkey2) {
				return 1;
			} else if (sortkey1 < sortkey2) {
				return -1;
			} else {
				return 0;
			}
		});
		return {
			"flowControl":{
				"name":"flownext"
			}
		};
	};

	this.flow.EMV23 = [
		{"type":"emv","obj":"EMV23_selectCommandCreate"}
		,{"type":"branch","obj":"EMV23_switchSelect"}
	];

	/**
	 * アプリケーション最終選択コマンド発行
	 */
	this.procedureSet["NODEIN-emv-EMV23_selectCommandCreate"] = function(session,m,v){

		if (!Utility.isEmpty(session.transaction.application)) {
			delete session.transaction.applicationListIndex;
			// 選択済みの場合、選択アプリケーションのindexを抽出
			for (var i = 0; i < session.transaction.applicationList.length; i++) {
				if (session.transaction.applicationList[i].aid == session.transaction.application) {
					session.transaction.applicationListIndex = i;
					break;
				}
			}
		} else {
			session.transaction.applicationListIndex = 0;
		}

		var apInfo = session.transaction.applicationList[session.transaction.applicationListIndex].value;
		var tvMap = this.Instance.analyzeTlv2(apInfo, false).map;

		var filename = null;

		if (!Utility.isEmpty(tvMap["4f"])) {
			filename = tvMap["4f"];
		} else {
			filename = tvMap["84"];
		}

		var length = function (length) {
			if (length < 16) {
				return "0" + length.toString(16);
			} else {
				return length.toString(16);
			}
		}(filename.length / 2);

		var selectCmd = "00a40400" + length + filename + "00";

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":selectCmd
					}
				}
			}
		};
	};

	/**
	 * アプリケーション最終選択コマンド（SELECTコマンド）結果取得
	 */
	this.procedureSet["emv-EMV23_selectCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.fci = Utility.toLowerCase(v.result);
			session.transaction.selectCmdRes = session.transaction.fci;
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	this.branch["EMV23_switchSelect"] = function(session) {

		var response = session.transaction.fci;
		var sw = response.substring(response.length - 4);

		switch(sw) {
			case "9000":	// 正常終了

				var tvListMap = this.Instance.analyzeTlv2(response, true);

				// 必須項目のチェック
				var isMandatoryError = (this.Instance.isMandatoryError(tvListMap.list, "6f")
						|| this.Instance.isMandatoryError(tvListMap.list, "84")
						|| this.Instance.isMandatoryError(tvListMap.list, "a5"));

				if (isMandatoryError) {
					// 候補アプリケーション削除
					return "EMV24";
				} else {
					// 2CL.032.00 フォーマットチェック
					var ficTemplate = null;
					for (var i = 0; i < tvListMap.list.length; i++) {
						if (tvListMap.list[i].tag == "6f") {
							ficTemplate = tvListMap.list[i];
							break;
						}
					}

					if (Utility.isNull(ficTemplate.child)) {
						// 候補アプリケーション削除
						return "EMV24";
					} else {
						// 2CA.099.00の絡みもあるので、PDOLが6fの子の場合のみエラー
						for (var i = 0; i < ficTemplate.child.length; i++) {
							if (ficTemplate.child[i].tag == "9f38") {
								// 候補アプリケーション削除
								return "EMV24";
							}
						}
					}

					// アプリケーション選択
					return "EMV27";
				}
			case "6a81":	// カードブロックまたはSELECTコマンドをサポートしていない
			case "6a82":	// ファイルが見つからない
				// 候補アプリケーション削除
				return "EMV24";

			default:	// 想定外のSW
				return "EMV24";
		};
	};

	this.flow.EMV24 = [
		{"type":"emv","obj":"deleteApplication"}
		,{"type":"branch","obj":"EMV24_switchSelect"}
	];

	/**
	 * NODEIN アプリケーション候補リストから削除
	 */
	this.procedureSet["NODEIN-emv-deleteApplication"] = function(session, m ,v) {

		// 選択アプリケーション情報を削除
		session.transaction.applicationList.splice(session.transaction.applicationListIndex,1);
		delete session.transaction.fci;
		session.transaction.retrySelectApplication = true;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * 選択AIDの有無判定
	 */
	this.branch["EMV24_switchSelect"] = function(session) {

		if(!Utility.isEmpty(session.transaction.application)) {
			// アプリケーション候補リスト有無判定
			if(session.transaction.applicationList.length == 0) {
				// 候補無し
				return "EMV46"; // msFallback判定

			} else {
				// 候補あり
				return "EMV26"; // アプリケーション選択
			}
		} else {
			return "EMV46";	// msFallback判定
		}
	};

	/**
	 * アプリケーション選択待ち
	 */
	this.flow.EMV26 = [
		{"type":"wait","obj":"applicationSelect"}
		,{"type":"move","obj":"EMV19"}
	];

	/**
	 * アプリケーション選択
	 */
	this.procedureSet["wait-applicationSelect_RECIEVES_BVR-select-application"] = function(session,m,v){
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
			};
		return rtn;
	};

	/**
	 * アプリケーション選択(wait-applicationSelect_RECIEVES_BVR-select-applicationが動かない場合用）
	 */
	this.procedureSet["wait-applicationSelect_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
			};
		return rtn;
	};

	////////// 業務選択待ち //////////
	this.flow.EMV27 = [
		{"type":"branch","obj":"EMV27_switchSelectDeal"}
	];

	/**
	 * EMV処理判定
	 */
	this.branch["EMV27_switchSelectDeal"] = function(session) {

		if (session.transaction.operationDiv == null) {
			return "EMV27_waitSelectDeal";
		} else if (session.transaction.isEmvProcess == "true") {
			return "EMV27_0";
		} else {
			return "endOfEmv";
		}
	};

	this.flow.EMV27_waitSelectDeal = [
		{"type":"wait","obj":"selectDeal"}
		,{"type":"branch","obj":"EMV27_switchSelect"}
	];

	/**
	 * NODEIN 業務選択
	 */
	this.procedureSet["wait-selectDeal_RECIEVES_PDL-select"] = function(session,m,v){
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
			};
		return rtn;
	};

	/**
	 * 業務選択選択(wait-selectDeal_RECIEVES_PDL-selectが動かない場合用）
	 */
	this.procedureSet["wait-selectDeal_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
			};
		return rtn;
	};

	/**
	 * EMV処理判定
	 */
	this.branch["EMV27_switchSelect"] = function(session) {
		if (session.transaction.isEmvProcess == "true") {
			return "EMV27_0";
		} else {
			return "endOfEmv";
		}
	};

	////////// アプリケーション初期化 //////////
	/**
	 * アプリケーション初期化・コマンド実行
	 */
	this.flow.EMV27_0 = [
		{"type":"emv","obj":"getIFDSerialNo"}
		,{"type":"emv","obj":"getProcessingOptionCreate"}
		,{"type":"branch","obj":"EMV27_0_switchSelect"}
	];

	/**
	 * NODEIN ICカードR/WのIFDシリアル番号の取得
	 */
	this.procedureSet["NODEIN-emv-getIFDSerialNo"] = function(session,m,v){

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"ifdSerialNo"
				}
			}
		};
	};

	/**
	 * NODEIN ICカードR/WのIFDシリアル番号の取得結果
	 */
	this.procedureSet["emv-getIFDSerialNo_RECIEVES_DNX-iccard_output-ifdSerialNo"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.ifdSerialNumber = v.result;
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * NODEIN GET PROCESSING OPTIONコマンド生成(サーバリクエスト）
	 */
	this.procedureSet["NODEIN-emv-getProcessingOptionCreate"] = function(session,m,v){
		var tyep;
		if (session.transaction.amountFlg == "true") {
			tyep = "CreditSales_SA1770_1";
		} else if (session.transaction.isInsertedFirst == "true") {
			tyep = "serviceMenu_ma0370_1";
		} else {
			tyep = "serviceMenu_ma0770_1";
		}

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":tyep
				}
			}
		};
		// アプリケーション情報を設定
		rtn.device.value.session = Utility.copy(session);
		rtn.device.value.session.transaction.fci = session.transaction.fci.substring(0, session.transaction.fci.length - 4);
		rtn.device.value.session.transaction.aid = session.transaction.applicationList[session.transaction.applicationListIndex].aid;
		rtn.device.value.session.transaction.brandApVersion = session.transaction.applicationList[session.transaction.applicationListIndex].brandApVersion;
		rtn.device.value.session.transaction.brandApJudgeNo = session.transaction.applicationList[session.transaction.applicationListIndex].brandApJudgeNo;

		return rtn;
	};

	/**
	 * アプリケーション初期化・コマンド実行
	 */
	this.branch["EMV27_0_switchSelect"] = function(session) {
		if (session.transaction.amountFlg == "true") {
			// 金額指定あり
			if (!Utility.isEmpty(session.transaction.amount)) {
				// 金額入力済み
				return "EMV27_2";
			} else {
				// 金額未入力
				return "EMV27_1";
			}
		} else {
			// 金額指定無し
			return "EMV27_2";
		}
	};

	/**
	 * 金額入力待ち
	 */
	this.flow.EMV27_1 = [
		{"type":"wait","obj":"inputAmount"}
		,{"type":"move","obj":"EMV27"}
	];

	this.procedureSet["wait-inputAmount_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){
		if (session.transaction.amountFlg == "true"
				&& (session.transaction.amount != null) && (session.transaction.taxOtherAmount != null)) {
			// 金額フラグがtrueで金額が入力された場合、フローを進める
			var rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
			return rtn;
		}
	};


	this.flow.EMV27_2 = [
		{"type":"emv","obj":"getProcessingOptionExecute"}
		,{"type":"branch","obj":"EMV27_2_switchSelect"}
	];

	/**
	 * NODEIN GPO(GET PROCESSING OPTION)コマンド実行
	 */
	this.procedureSet["NODEIN-emv-getProcessingOptionExecute"] = function(session,m,v){
		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.gpoCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN GPO(GET PROCESSING OPTION)コマンド結果取得
	 */
	this.procedureSet["emv-getProcessingOptionExecute_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.gpoCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * GPOコマンド実行結果判定
	 */
	this.branch["EMV27_2_switchSelect"] = function(session) {
		var response = session.transaction.gpoCmdRes;

		var sw = response.substring(response.length - 4);

		switch(sw) {
			case "9000":	// 正常
				// GPOコマンド実行結果検証
				return "EMV28";

			default:
				// エラー (候補リスト削除)
				return "EMV29";
		};
	};

	/**
	 * NODEIN GET PROCESSING OPTIONコマンド実行結果検証
	 */
	this.flow.EMV28 = [
		{"type":"emv","obj":"getProcessingOptionResult"}
		,{"type":"branch","obj":"EMV28_switchSelect"}
	];

	/**
	 * NODEIN GET PROCESSING OPTIONコマンド実行結果検証(サーバリクエスト）
	 */
	this.procedureSet["NODEIN-emv-getProcessingOptionResult"] = function(session,m,v){
		var tyep;
		if (session.transaction.amountFlg == "true") {
			tyep = "CreditSales_SA1770_2";
		} else if (session.transaction.isInsertedFirst == "true") {
			tyep = "serviceMenu_ma0370_2";
		} else {
			tyep = "serviceMenu_ma0770_2";
		}
		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":tyep
					,"session": session
				}
			}
		};
		return rtn;
	};

	/**
	 * GPOコマンド実行結果検証結果判定
	 */
	this.branch["EMV28_switchSelect"] = function(session) {
		switch(session.transaction.result) {
			case "0":
				// GPOコマンド成功
				return "EMV30"; // readRecordCmd

			default:
				return "EMV29"; // 検証結果エラー (候補リスト削除)
		};
	};

	/**
	 * GPO結果検証エラー
	 */
	this.flow.EMV29 = [
		{"type":"emv","obj":"deleteApplicationGpoFail"}	// アプリケーション候補リストからの削除
		,{"type":"move","obj":"EMV19"}				// アプリケーション最終選択
	];

	/**
	 * NODEIN アプリケーション候補リストから削除
	 */
	this.procedureSet["NODEIN-emv-deleteApplicationGpoFail"] = function(session, m ,v) {

		// 選択アプリケーション情報を削除
		session.transaction.applicationList.splice(session.transaction.applicationListIndex,1);
		delete session.transaction.fci;
		delete session.transaction.application;
		session.transaction.retrySelectApplication = true;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * アプリケーション読出・コマンド実行
	 */
	this.flow.EMV30 = [
		{"type":"emv","obj":"rdrcdCmdListIndexCountup"}
		,{"type":"branch","obj":"EMV30_switchSelect"}
	];

	/**
	 * アプリケーション初期化 readRecordコマンドリストの配列番号を１加算
	 */
	this.procedureSet["NODEIN-emv-rdrcdCmdListIndexCountup"] = function(session,m,v){

		session.transaction.rdrcdCmdIndex += 1;

		var rtn = {
			"flowControl":{
				"name":"flownext"
			}
		};

		return rtn;
	};

	/**
	 * アプリケーション読出し・コマンド実行
	 */
	this.branch["EMV30_switchSelect"] = function(session) {
		if (session.transaction.rdrcdCmdList.length > session.transaction.rdrcdCmdIndex) {
			return "EMV31";	// readRecordCmd実行
		} else {
			return "EMV34";	// カード会社判定
		}
	};

	/**
	 * NODEIN READ RECORDコマンド実行
	 */
	this.flow.EMV31 = [
		{"type":"emv","obj":"EMV31_readRecordCommandCreate"}
		,{"type":"branch","obj":"EMV31_switchSelect"}
	];

	/**
	 * NODEIN READ RECORDコマンド実行
	 */
	this.procedureSet["NODEIN-emv-EMV31_readRecordCommandCreate"] = function(session,m,v){

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.rdrcdCmdList[session.transaction.rdrcdCmdIndex]
					}
				}
			}
		};
	};

	/**
	 * NODEIN READ RECORDコマンド結果取得
	 */
	this.procedureSet["emv-EMV31_readRecordCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			if (Utility.isEmpty(session.transaction.rdrcdCmdResList)) {
				session.transaction.rdrcdCmdResList = new Array();
			}
			session.transaction.rdrcdCmdResList.push(Utility.toLowerCase(v.result));

			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * READ RECORDコマンド結果SW判定
	 * 2CL.010.00 Case 01によりJavaScriptでSW検証
	 */
	this.branch["EMV31_switchSelect"] = function(session) {

		var response = session.transaction.rdrcdCmdResList[session.transaction.rdrcdCmdIndex];

		var sw = response.substring(response.length - 4);

		if (sw == "9000") {
			// READ RECOEDコマンド
			return "EMV30";
		} else {
			// 想定外のSW
			session.transaction.eventcode = "T_E2031";
			session.transaction.errorSW = sw;
			return "endOfIcCardError";
		}

	};

	/**
	 * Get Data Command
	 */
	this.flow.EMV32 = [
		{"type":"emv","obj":"getDataCmdListIndexCountup"}
		,{"type":"branch","obj":"EMV32_switchSelect"}
	];

	/**
	 * アプリケーション初期化 getDataCmdの配列番号を１加算
	 */
	this.procedureSet["NODEIN-emv-getDataCmdListIndexCountup"] = function(session,m,v){

		session.transaction.gdCmdIndex += 1;

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * 未実行GetDataコマンド判定
	 */
	this.branch["EMV32_switchSelect"] = function(session) {

		if (session.transaction.gdCmdList.length > session.transaction.gdCmdIndex) {
			return "EMV33";	// getDataCmd実行
		} else {
			return "EMV34_1";	// データ認証
		}
	};

	/**
	 * GetDataコマンド実行
	 */
	this.flow.EMV33 = [
		{"type":"emv","obj":"getDataCommandCreate"}
		,{"type":"move","obj":"EMV32"}
	];

	/**
	 * NODEIN GET DATAコマンド実行
	 */
	this.procedureSet["NODEIN-emv-getDataCommandCreate"] = function(session,m,v){

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.gdCmdList[session.transaction.gdCmdIndex]
					}
				}
			}
		};
	};

	/**
	 * NODEIN GET DATAコマンド結果取得
	 */
	this.procedureSet["emv-getDataCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			if (Utility.isEmpty(session.transaction.gdCmdResList)) {
				session.transaction.gdCmdResList = new Array();
			}
			session.transaction.gdCmdResList.push(Utility.toLowerCase(v.result));

			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	////////// 動的データ認証・コマンド実行 //////////
	/**
	 * カード会社判定
	 */
	this.flow.EMV34 = [
		{"type":"emv","obj":"cardcoJudge"}
		,{"type":"move","obj":"EMV32"}
	];

	/**
	 * NODEIN カード会社判定
	 */
	this.procedureSet["NODEIN-emv-cardcoJudge"] = function(session, m ,v) {

		var tyep;
		if (session.transaction.amountFlg == "true") {
			tyep = "CreditSales_SA1770_3";
		} else if (session.transaction.isInsertedFirst == "true") {
			tyep = "serviceMenu_ma0370_3";
		} else {
			tyep = "serviceMenu_ma0770_3";
		}

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":tyep
					,"session":session
				}
			}
		};

		return rtn;
	};

	/**
	 * カード会社判定フロー
	 */
	this.flow.EMV34_1 = [
		{"type":"branch","obj":"EMV34_1_switchSelect"}
	];

	/**
	 * カード会社判定
	 */
	this.branch["EMV34_1_switchSelect"] = function(session) {
		switch(session.transaction.judgeCardcoResult) {
			case "0":
				return "EMV34_3"; // カード会社判定成功 (データ認証)
			default:
				return "EMV34_2"; // カード会社判定失敗 (KID入力)
		}
	};

	/**
	 * カード会社入力待ちフロー
	 */
	this.flow.EMV34_2 = [
		{"type":"wait","obj":"inputKid"}
		,{"type":"move","obj":"EMV34_3"}
	];

	/**
	 * Kid入力からEMVのフローを先に進めるための処理です。
	 */
	this.procedureSet["wait-inputKid_RECIEVES_NODEIN-proceed-inputKidComplete"] = function(session, m, v) {
		var rtn = {};
		rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * カード読取終了
	 */
	this.flow.EMV34_3 = [
		{"type":"move","obj":"endOfEmv"}
	];

	/**
	 * 磁気フォールバックルール
	 */
	this.flow.EMV46 = [
		{"type":"branch","obj":"EMV46_switchSelect"}
	];

	/**
	 * 磁気フォールバックルール
	 */
	this.branch["EMV46_switchSelect"] = function(session) {

		if (session.property.pinUnknownFbOnOffDiv == "0") {
			// PIN不知FB有無区分:"0"(PIN不知FB無)
			return "EMV47";	// エラー (フォールバック不可)
		}
		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);

		if(sw == "6a81") {
			// カードブロック
			return "EMV47";	// エラー (フォールバック不可)

		} else if (sw == "6283" && session.transaction.applicationCount == 1){
			// アプリケーションブロック、候補リスト１件
			return "EMV47";	// エラー (フォールバック不可)
		} else {
			return "EMV48";	// msFallback処理
		}
	};

	/**
	 * フォールバック不可
	 */
	this.flow.EMV47 = [
		{"type":"emv","obj":"EMV47_errorReport"}
		,{"type":"move","obj":"endOfIcCardError"}
	];

	/**
	 * メッセージIDとSWを設定
	 */
	this.procedureSet["NODEIN-emv-EMV47_errorReport"] = function(session, m ,v) {
		var response = session.transaction.selectCmdRes;
		var sw = response.substring(response.length - 4);
		session.transaction.eventcode = "T_E2031";
		session.transaction.errorSW = sw;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	this.flow.EMV48 = [
		{"type":"emv","obj":"msFallback"}
		,{"type":"move", "obj":"endOfEmv"}
	];

	/**
	 * 磁気フォールバック
	 */
	this.procedureSet["NODEIN-emv-msFallback"] = function(session, m ,v) {
		session.transaction.isMSFallback = "true";

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * 終了処理
	 */
	this.flow.endOfEmv = [
		{"type":"emv","obj":"readEnd"}
		,{"type":"release","obj":"iccard"}
	];

	this.procedureSet["NODEIN-emv-readEnd"] = function(session, m ,v) {
		session.transaction.readCardInfo = "true";

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	this.flow.endOfIcCardError = [
		{"type":"release","obj":"icCardError"}
	];


	/**
	 * NODEOUT 自身のフローの場合、処理を行います
	 */
	this.procedureSet["NODEOUT"] = function(session,m,v) {
		if (v.flowType != this.Instance.flowType ) {
			return {};
		}
		var rtn = {};
		if (v && v.direction) {
			rtn = {
				"flowControl":{
					"name":v.direction
				}
			};
		}

		return rtn;
	};

	/**
	 * EMVイベント
	 */
	this.procedureSet["ICC"] = function(session,m,v){

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		for(var key in v) {
			session.transaction[key] = v[key];
		}

		return rtn;
	};

	/**
	 *
	 */
	this.procedureSet["FRT-wait-cardReadWait"] = function(session,m,v) {
		var rtn = {
			"state":{
				"name" : "release"
			}
		};

		return rtn;
	};

	this.procedureSet["iccard_open-cardData_RECIEVES_NODEIN-wait-cardReadWait"] = function(session, m, v) {
		var rtn = {
				"state":{
					"name" : "release"
				}
		};

		return rtn;
	};

	/**
	 * TLVデータオブジェクト解析処理.<br/>
	 * <p>
	 * APDUコマンドの応答電文を解析し、マップに格納して返却する.<br/>
	 * </p>
	 * @param responseMessage APDUコマンド応答電文（16進文字列）
	 * @param tvArray TVマップ（タグと値のマップ）
	 * @param isIncludeSw SWを含む場合true
	 */
	this.analyzeTlv2 = function(responseMessage, isIncludeSw) {
		// 長さチェック
		var result = {"list":new Array(), "map":{}, "duplicate":false};

		var tvArray = result.list;
		var response = responseMessage;
		if (isIncludeSw) {
			response = response.substring(0, response.length - 4);
		}

		var parentTvArray = new Array();
		var parentValue = new Array();
		while (true) {
			var tag = "";
			while (response.length >= 2 && (Utility.isEmpty(tag) || tag === "00" || tag === "ff")) {
				tag = response.substring(0, 2);
				response = response.substring(2);
			}

			if (Utility.isEmpty(tag) || (tag === "00") || (tag === "ff")) {
				// 親の処理に戻る
				tvArray = parentTvArray.pop();
				response = parentValue.pop();

				if (Utility.isEmpty(response)) {
					break;
				} else {
					continue;
				}
			} else if (Utility.isEmpty(response)) {
				// パースエラー
				return {"list":new Array(), "map":{}, "duplicate":false};
			}

			var tagType;
			if ((parseInt(tag, 16) & 32) == 32) {
				tagType = "OBJECT_STRUCT";
			} else {
				tagType = "OBJECT_BASE";
			}

			if ((parseInt(tag, 16) & 31) == 31) {
				tag +=  response.substring(0, 2);
				response = response.substring(2);
			}

			var len = parseInt(response.substring(0, 2), 16);
			response = response.substring(2);

			if (len > 127) {
				len = len - 128;

				if ((len != 0) && (response.length > (len * 2))) {
					var lengthValue = parseInt(response.substring(0, len * 2), 16);
					response = response.substring(len * 2);
					len = lengthValue;
				} else {
					len = 0;
				}
			}

			if (response.length >= len * 2) {
				var data = response.substring(0, len * 2);
				response = response.substring(len * 2);
					if ((tagType == "OBJECT_BASE")
					|| (tag == "71")
					|| (tag == "72")) {
					// 基本オブジェクトまたは発行者スクリプトテンプレートの場合
					tvArray.push({"tag":tag,"value":data});
					if (!Utility.isNull(result.map[tag])) {
						result.duplicate = true;
					}
					result.map[tag] = data;

				} else {
					// 現在のものを保持
					var parentArray = tvArray;
					parentTvArray.push(tvArray);
					parentValue.push(response);
					// 子の解析
					tvArray = new Array();
					response = data;
					parentArray.push({"tag":tag,"value":data,"child":tvArray});
				}
			} else {
				// パースエラー
				return {"list":new Array(), "map":{}, "duplicate":false};
			}
		}
		return result;
	};

	/**
	 * タグ配列取得
	 *
	 * 指定のタグの値をresultListに詰めます。
	 * @param tagListMap タグと値のList
	 * @param resultList 結果を詰める配列
	 * @param tag 抽出するタグ
	 */
	this.getTagList = function(tagListMap, resultList, tag) {

		for (var i = 0; i < tagListMap.length; i++) {
			var tagMap = tagListMap[i];
			if (tagMap.tag == tag) {
				resultList.push(Utility.copy(tagMap));
			} else {
				if (!Utility.isEmpty(tagMap.child)) {
					this.getTagList(tagMap.child, resultList, tag);
				}
			}
		}
	};

	/**
	 * 必須項目チェック
	 *
	 * 指定タグが含まれているかチェックします
	 * @param tagListMap タグと値のList
	 * @param tag チェック対象のタグ
	 * @return タグが含まれない場合true
	 */
	this.isMandatoryError = function(tvListMap , tag) {
		var entryList = new Array();
		this.getTagList(tvListMap, entryList, tag);

		return entryList.length == 0;
	};

	/**
	 * デコード
	 *
	 * 16進数文字列を元に文字列に変換する
	 * @param str
	 * @return 生成した文字列
	 */
	this.decode = function(str) {
		var decodeString = "";
		if (!Utility.isEmpty(str)) {
			for (var j = 0, len = str.length; j * 2 < len; j++) {
				decodeString += String.fromCharCode("0x" + str.substring(2 * j, 2 * j + 2));
			}
		}
		return decodeString;
	};

};

/**
 * 「ICカード」-「EMV仕様」クラスの基底クラスはメディアルール基底クラスです。
 */
IcCardMediaModel.prototype = new MediaRuleBaseModel();

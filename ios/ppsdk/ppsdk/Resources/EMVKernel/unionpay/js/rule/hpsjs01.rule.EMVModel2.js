/**
 * 「ICカード」「EMV仕様」の仕様・振る舞いを表すクラスです。
 * @class 「ICカード」「EMV仕様」の仕様・振る舞いを表すクラス
 * @extends RuleBaseModel
 */
var EMVRuleModel = function() {
	this.modelRuleName = "EMVRuleModel";
	this.flowType = "emvRule";

	this.procedureSet = {};
	this.procedureSet.Instance = this;

	//フロー定義
	/**
	 * @property EMVの処理フローです。
	 */
	this.flow = {};
	this.firstFlow = "EMV34_3";
	this.flow["__RESET__"] = [
		{"type":"dialogconfirm","obj":"reset"}
		,{"type":"end","obj":"-"}
	];
	this.flow["__RETRY__"] = [
		 {"type":"dialogconfirm","obj":"retry"}
		 ,{"type":"move","obj":"__RESET__"}
	];
	this.flow["__ERROR__"] = [
		{"type":"release", "obj":"icCardError"}
	 ];


	/**
	 * ブランチメソッドです。業務モデルからも参照されます。
	 */
	this.branch = {};
	this.branch.Instance = this;
	this.branch.Instance.branchGenerate = {};
	this.branch.Instance.branchGenerate.Instance = this;

	///////////////////////////////////////////////////////////////////////////////////

	this.flow.EMV34_3 = [
		{"type":"emv", "obj":"initEmv"}
		,{"type":"branch","obj":"EMV34_3_switchSelect"}
	];

	/**
	 * NODEIN 動作開始
	 */
	this.procedureSet["emv-initEmv_RECIEVES_NODEIN-input-amount"] = function(session, m ,v) {
		session.transaction.issuerScriptCmdListBeforeIndex1 = -1;
		session.transaction.issuerScriptCmdListAfterIndex1 = -1;

		return {
			"flowControl":{
				"name":"start"
			}
		};
	};

	/**
	 * NODEIN 動作開始
	 */
	this.procedureSet["NODEIN-emv-initEmv"] = function(session, m ,v) {
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	this.branch["EMV34_3_switchSelect"] = function(session) {

		if (session.transaction.amountFlg == "true") {
			return "EMV34_4";
		} else {
			return "EMV34_5";
		}
	};

	this.flow.EMV34_4 = [
		{"type":"emv","obj":"initAmountEmv"}
		,{"type":"move","obj":"EMV34_5"}
	];

	/**
	 * NODEIN 動作開始
	 */
	this.procedureSet["emv-initAmountEmv_RECIEVES_RELEASE-release-iccard"] = function(session, m ,v) {

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * データ認証要求 フロー
	 */
	this.flow.EMV34_5 = [
		{"type":"emv","obj":"dataAuthentication"}
		,{"type":"branch","obj":"EMV34_5_switchSelect"}
	];

	/**
	 * NODEIN データ認証要求
	 */
	this.procedureSet["NODEIN-emv-dataAuthentication"] = function(session, m ,v) {
		var tyep;
		if (session.transaction.amountFlg == "true") {
			tyep = "CreditSales_SA1770_4";
		} else if (session.transaction.isInsertedFirst == "true") {
			tyep = "serviceMenu_ma0370_4";
		} else {
			tyep = "serviceMenu_ma0770_4";
		}

		var rtn = {
				"device":{
					"name":"network"
						,"value":{
							"type":tyep
							,"session":session
						}
				}
		};

		return rtn;
	};

	/**
	 * データ認証要求結果判定
	 */
	this.branch["EMV34_5_switchSelect"] = function(session) {
		if (session.transaction.isRiskFallback == "true") {
			return "EMV48";	// フォールバック
		} else if (Utility.isEmpty(session.transaction.intAuthCmd)) {
			return "EMV40";	// カードアクション分析
		} else {
			return "EMV35";	// INTERNAL AUTHENTICATEコマンド
		}
	};

	/**
	 * INTERNAL AUTHENTICATEコマンド実行
	 */
	this.flow.EMV35 = [
		{"type":"emv","obj":"internalAuthenticateCommandCreate"}
		,{"type":"emv","obj":"internalAuthenticateCommandResult"}
		,{"type":"move","obj":"EMV40"}	// カードアクション分析
	];

	/**
	 * NODEIN INTERNAL AUTHENTICATEコマンド生成
	 */
	this.procedureSet["NODEIN-emv-internalAuthenticateCommandCreate"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.intAuthCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN INTERNAL AUTHENTICATEコマンド結果取得
	 */
	this.procedureSet["emv-internalAuthenticateCommandCreate_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りがStringの場合、処理を行う
			session.transaction.intAuthCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};


	/**
	 * NODEIN INTERNAL AUTHENTICATEコマンド結果送信
	 */
	this.procedureSet["NODEIN-emv-internalAuthenticateCommandResult"] = function(session, m ,v) {

		var tyep;
		if (session.transaction.amountFlg == "true") {
			tyep = "CreditSales_SA1770_5";
		} else if (session.transaction.isInsertedFirst == "true") {
			tyep = "serviceMenu_ma0370_5";
		} else {
			tyep = "serviceMenu_ma0770_5";
		}

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":tyep
					,"session": session
				}
			}
		};

		return rtn;
	};

	this.flow.EMV40 = [
		{"type":"wait","obj":"inputPayWay"}
		,{"type":"move","obj":"EMV42"}
	];

	this.procedureSet["NODEIN-wait-inputPayWay"] = function(session, m ,v) {
		session.transaction.isDataAuthEnd = "true";
	};

	this.procedureSet["wait-inputPayWay_RECIEVES_NODEIN-wait-icProcess"] = function(session, m ,v) {
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};


	/**
	 * カード保有者検証
	 */
	this.flow.EMV36 = [
		{"type":"branch","obj":"EMV36_switchSelect"}
	];

	/**
	 * カード保有者検証
	 */
	this.branch["EMV36_switchSelect"] = function(session) {

		if (session.transaction.msgFlg == "true") {
			// メッセージ表示のみ
			return "EMV37";
		}

		switch(session.transaction.inspectionMethod) {
			case "5":
			case "6":
				// オフライン暗号PIN
				if (session.transaction.verifyCmd) {
					return "EMV38_2";	// Verifyコマンド
				} else {
					return "EMV38";		// GET CHALLENGE
				}
			default:
				// オフライン平文PIN (オンライン暗号PIN)
				return "EMV39";
		};
	};

	/**
	 * メッセージ表示
	 */
	this.flow.EMV37 = [
		{"type":"wait","obj":"inputPin"}	// メッセージ出力
		,{"type":"move","obj":"EMV42"}		// カードアクション分析
	];


	/**
	 * PIN入力
	 */
	this.procedureSet["wait-inputPin_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * オフライン暗号PIN
	 */
	this.flow.EMV38 = [
		{"type":"emv","obj":"getChallengeCommand"}			// GET CHALLENGE
		,{"type":"branch","obj":"EMV38_switchSelect"}
	];

	/**
	 * NODEIN GET CHALLENGEコマンド発行
	 */
	this.procedureSet["NODEIN-emv-getChallengeCommand"] = function(session, m ,v) {
		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.getChallengeCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN GET CHALLENGEコマンド結果取得
	 */
	this.procedureSet["emv-getChallengeCommand_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.resultMsg = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * GET CHALLENGEコマンド結果検証
	 */
	this.branch["EMV38_switchSelect"] = function(session) {

		var response = session.transaction.resultMsg;

		var sw = response.substring(response.length - 4);

		if (sw == "9000") {
			// PIN入力
			return "EMV39";
		} else {
			// カード保有者検証
			return "EMV42";
		}
	};

	/**
	 * Verifyコマンド実行
	 */
	this.flow.EMV38_2 = [
		{"type":"emv","obj":"verifyCommand"}				// Verifyコマンド発行
		,{"type":"move","obj":"EMV42"}						// カードアクション分析
	];

	/**
	 * NODEIN Verifyコマンド
	 */
	this.procedureSet["NODEIN-emv-verifyCommand"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
					,"value":{
						"type":"iccard_output"
							,"obj":"apduresponse"
								,"param":{
									"in_apdu_data":session.transaction.verifyCmd
								}
					}
			}
		};
	};

	/**
	 * NODEIN Verifyコマンド結果取得
	 */
	this.procedureSet["emv-verifyCommand_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.resultMsg = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};


	/**
	 * PIN入力
	 */
	this.flow.EMV39 = [
		{"type":"wait","obj":"inputPin"}		// PIN入力
		,{"type":"move","obj":"EMV42"}			// カードアクション分析
	];

	/**
	 * IC処理中
	 */
	this.procedureSet["wait-inputPayWay_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};


	this.flow.EMV42 = [
		{"type":"emv","obj":"generateAcCommandCreate"}
		,{"type":"branch","obj":"EMV42_switchSelect"}
	];

	/**
	 * NODEIN GENERATE ACコマンド生成
	 */
	this.procedureSet["NODEIN-emv-generateAcCommandCreate"] = function(session, m ,v) {

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":"CreditSales_SA1719"
					,"session":session
				}
			}
		};

		return rtn;
	};

	this.branch["EMV42_switchSelect"] = function(session) {
		if (Utility.isEmpty(session.transaction.gacCommand)) {
			return "EMV36";	// カード保有者検証
		} else {
			return "EMV43";	// firstGACCmd実行
		}
	};

	/**
	 * GACコマンド実行
	 */
	this.flow.EMV43 = [
		{"type":"emv","obj":"generateAcCommandExecute"}
		,{"type":"move","obj":"EMV50"}
	];

	/**
	 * NODEIN GENERATE ACコマンド実行
	 */
	this.procedureSet["NODEIN-emv-generateAcCommandExecute"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.gacCommand
					}
				}
			}
		};
	};

	/**
	 * NODEIN GENERATE ACコマンド結果取得
	 */
	this.procedureSet["emv-generateAcCommandExecute_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.firstGACCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * 磁気フォールバックルール
	 */
	this.flow.EMV46 = [
		{"type":"branch","obj":"EMV46_switchSelect"}
	];

	/**
	 * 磁気フォールバックルール
	 */
	this.branch["EMV46_switchSelect"] = function(session) {

		if (session.property.pinUnknownFbOnOffDiv == "0") {
			// PIN不知FB有無区分:"0"(PIN不知FB無)
			return "EMV47";	// エラー (フォールバック不可)
		}
		var response = session.transaction.selectCmdRes;

		var sw = response.substring(response.length - 4);

		if(sw == "6a81") {
			// カードブロック
			return "EMV47";	// エラー (フォールバック不可)

		} else if (sw == "6283" && session.transaction.applicationCount == 1){
			// アプリケーションブロック、候補リスト１件
			return "EMV47";	// エラー (フォールバック不可)
		} else {
			return "EMV48";	// msFallback処理
		}
	};

	/**
	 * フォールバック不可
	 */
	this.flow.EMV47 = [
		{"type":"emv","obj":"EMV47_errorReport"}
		,{"type":"move","obj":"endOfIcCardError"}
	];

	/**
	 * メッセージIDとSWを設定
	 */
	this.procedureSet["NODEIN-emv-EMV47_errorReport"] = function(session, m ,v) {
		var response = session.transaction.selectCmdRes;
		var sw = response.substring(response.length - 4);
		session.transaction.eventcode = "T_E2031";
		session.transaction.errorSW = sw;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	this.flow.EMV48 = [
		{"type":"emv","obj":"msFallback"}
		,{"type":"release", "obj":"emv"}
	];

	this.procedureSet["NODEIN-emv-msFallback"] = function(session, m ,v) {
		if (v.flowType != this.Instance.flowType ) {
			var rtn = {
					"state":{
						"name" : "release"
					}
				};

		} else {
			session.transaction.isMSFallback = "true";
			var rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}

		return rtn;
	};

	////////// 完了処理・コマンド実行・コマンド実行結果検証 //////////
	/**
	 * 完了処理・コマンド実行・コマンド実行結果検証
	 */
	this.flow.EMV50 = [
		{"type":"wait","obj":"amountConfirm"}
		,{"type":"branch","obj":"EMV50_switchSelect"}
	];

	/**
	 * 完了処理 secondGAC判定
	 */
	this.branch["EMV50_switchSelect"] = function(session) {

		if(Utility.isEmpty(session.transaction.extAuthCmd)) {
			// EXTERNAL AUTHENTICATEコマンド無し
			return "EMV52"; // issuerScript(GAC前)判定

		} else {
			return "EMV51"; // EXTERNAL AUTHENTICATEコマンド

		}
	};

	/**
	 * EXTERNAL AUTHENTICATEコマンド実行
	 */
	this.flow.EMV51 = [
		{"type":"emv","obj":"externalAuthenticateCommand"}
		,{"type":"move","obj":"EMV52"}
	];

	/**
	 * NODEIN EXTERNAL AUTHENTICATEコマンド実行
	 */
	this.procedureSet["NODEIN-emv-externalAuthenticateCommand"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.extAuthCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN EXTERNAL AUTHENTICATEコマンド結果取得
	 */
	this.procedureSet["emv-externalAuthenticateCommand_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.extAuthCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * 完了処理 発行者スクリプト(GAC前)ループフロー１
	 */
	this.flow.EMV52 = [
		{"type":"emv","obj":"issuerScriptCmdListBeforeIndex1Countup"}
		,{"type":"branch","obj":"EMV52_switchSelect"}
	];

	/**
	 * 完了処理 発行者スクリプト(GAC前)の配列番号１を１加算、配列番号２とエラーフラグを初期化
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListBeforeIndex1Countup"] = function(session,m,v){

		session.transaction.issuerScriptCmdListBeforeIndex1 += 1;
		session.transaction.issuerScriptCmdListBeforeIndex2 = -1;
		session.transaction.issuerScriptCmdErrorFlg = false;

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * 完了処理 issuerScript(GAC前)判定
	 */
	this.branch["EMV52_switchSelect"] = function(session) {
		if (Utility.isEmpty(session.transaction.issuerScriptCmdListBefore)
				|| (session.transaction.issuerScriptCmdListBefore.length == 0)) {
			// issuerScript(GAC前)無し
			return "EMV54"; // secondGAC

		} else if (session.transaction.issuerScriptCmdListBefore.length > session.transaction.issuerScriptCmdListBeforeIndex1) {
			return "EMV52_1"; // 次の発行者スクリプト(GAC前)

		} else {
			// すべて実行
			return "EMV54"; // 発行者スクリプト(GAC前)検証
		}
	};

	/**
	 * 完了処理 発行者スクリプト(GAC前)ループフロー２
	 */
	this.flow.EMV52_1 = [
		{"type":"emv","obj":"issuerScriptCmdListBeforeIndex2Countup"}
		,{"type":"branch","obj":"EMV52_1_switchSelect"}
	];

	/**
	 * 完了処理 発行者スクリプト(GAC前)の配列番号２を１加算
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListBeforeIndex2Countup"] = function(session,m,v){

		session.transaction.issuerScriptCmdListBeforeIndex2 += 1;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * 完了処理 issuerScript(GAC前)判定
	 */
	this.branch["EMV52_1_switchSelect"] = function(session) {

		var i = session.transaction.issuerScriptCmdListBeforeIndex1;
		var j = session.transaction.issuerScriptCmdListBeforeIndex2;

		if (session.transaction.issuerScriptCmdListBefore[i].length <= session.transaction.issuerScriptCmdListBeforeIndex2) {
			return "EMV52"; // 次のタグの発行者スクリプト(GAC前)
		}
		if (session.transaction.issuerScriptCmdErrorFlg) {
			// 同タグ内でSWエラー
			return "EMV52_2"; // 空の結果を設定し次の発行者スクリプト(GAC前)

		} else if (Utility.isEmpty(session.transaction.issuerScriptCmdListBefore[i][j])) {
			// 空のコマンド
			return "EMV52_2"; // 空の結果を設定し次の発行者スクリプト(GAC前)

		} else {
			return "EMV53"; // 発行者スクリプト(GAC前)
		}
	};

	/**
	 * 発行者スクリプト(GAC前)ブランク結果取得
	 */
	this.flow.EMV52_2 = [
		{"type":"emv","obj":"issuerScriptCmdListBeforeBlankResultSet"}
		,{"type":"move","obj":"EMV52_1"}
	];

	/**
	 * NODEIN 発行者スクリプト(GAC前)ブランク結果取得
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListBeforeBlankResultSet"] = function(session,m,v){

		if (Utility.isEmpty(session.transaction.issuerScriptCmdListResBefore)) {
			session.transaction.issuerScriptCmdListResBefore = new Array();
		}
		session.transaction.issuerScriptCmdListResBefore.push("");
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * 発行者スクリプト（GAC前）実行フロー
	 */
	this.flow.EMV53 = [
		{"type":"emv","obj":"issuerScriptBefore"}
		,{"type":"branch","obj":"EMV53_switchSelect"}
	];

	/**
	 * NODEIN 発行者スクリプト(GAC前)実行
	 */
	this.procedureSet["NODEIN-emv-issuerScriptBefore"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.issuerScriptCmdListBefore[session.transaction.issuerScriptCmdListBeforeIndex1][session.transaction.issuerScriptCmdListBeforeIndex2]
					}
				}
			}
		};
	};

	/**
	 * NODEIN 発行者スクリプト(GAC前)結果取得
	 */
	this.procedureSet["emv-issuerScriptBefore_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			if (Utility.isEmpty(session.transaction.issuerScriptCmdListResBefore)) {
				session.transaction.issuerScriptCmdListResBefore = new Array();
			}
			session.transaction.issuerScriptCmdListResBefore.push(Utility.toLowerCase(v.result));
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * 完了処理 発行者スクリプト(GAC前)の結果判定
	 */
	this.branch["EMV53_switchSelect"] = function(session) {
		var issuerScriptCmdResBefore = session.transaction.issuerScriptCmdListResBefore[session.transaction.issuerScriptCmdListResBefore.length - 1];
		var sw1 = issuerScriptCmdResBefore.substring(issuerScriptCmdResBefore.length - 4, issuerScriptCmdResBefore.length - 2);
		if(sw1 == "90" || sw1 == "62" || sw1 == "63") {
			// sw1 = 90 or 62 or 63
			return "EMV52_1"; // 正常 (issuerScriptCmdBefore)

		} else {
			return "EMV53_1"; // エラー (次のコマンド)
		}
	};

	/**
	 * 発行者スクリプトのタグ別コマンド結果フラグにTrue(エラー)を設定する。
	 */
	this.flow.EMV53_1 = [
		{"type":"emv","obj":"setIssuerScriptCmdErrorFlgTrue"}
		,{"type":"move","obj":"EMV52_1"}
	];

	/**
	 * NODEIN 発行者スクリプト(GAC前)のタグ別コマンド結果をTrue(エラー)に設定する
	 */
	this.procedureSet["NODEIN-emv-setIssuerScriptCmdErrorFlgTrue"] = function(session, m ,v) {

		session.transaction.issuerScriptCmdErrorFlg = true;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * 完了処理・コマンド実行・コマンド実行結果検証(発行者認証コマンド検証、発行者スクリプト(GAC前)検証、2ndGAC生成)フロー
	 */
	this.flow.EMV54 = [
		{"type":"emv","obj":"completionResultInspect2ndGACCreate"}
		,{"type":"branch","obj":"EMV54_switchSelect"}
	];

	/**
	 * NODEIN 完了処理・コマンド実行・コマンド実行結果検証(発行者認証コマンド検証、発行者スクリプト(GAC前)検証、2ndGAC生成)
	 */
	this.procedureSet["NODEIN-emv-completionResultInspect2ndGACCreate"] = function(session, m ,v) {

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":"CreditSales_SA1901_1"
					,"session":session
				}
			}
		};

		return rtn;
	};

	/**
	 * 2ndGACコマンド有無判定
	 */
	this.branch["EMV54_switchSelect"] = function(session) {
		if (Utility.isEmpty(session.transaction.secondGACCmd)) {
		// 2ndGACコマンド無し
			return "endOfEmv"; // EMV処理終了

		} else {
			return "EMV54_1"; // 2ndGACコマンド実行
		}
	};

	/**
	 * 2ndGACコマンド実行フロー
	 */
	this.flow.EMV54_1 = [
		{"type":"emv","obj":"secondGACCommand"}
		,{"type":"move","obj":"EMV56"}
	];

	/**
	 * NODEIN 2ndGACコマンド実行
	 */
	this.procedureSet["NODEIN-emv-secondGACCommand"] = function(session, m ,v) {

		return {
			"device":{
				"name":"iccard"
				,"value":{
					"type":"iccard_output"
					,"obj":"apduresponse"
					,"param":{
						"in_apdu_data":session.transaction.secondGACCmd
					}
				}
			}
		};
	};

	/**
	 * NODEIN 2ndGACコマンド実行結果取得
	 */
	this.procedureSet["emv-secondGACCommand_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (v.result.length >= 4) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			session.transaction.secondGACCmdRes = Utility.toLowerCase(v.result);
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * 発行者スクリプト(GAC後)実行ループフロー１
	 */
	this.flow.EMV56 = [
		{"type":"emv","obj":"issuerScriptCmdListAfterIndex1Countup"}
		,{"type":"branch","obj":"EMV56_switchSelect"}
	];

	/**
	 * 完了処理 発行者スクリプト(GAC後)の配列番号１を１加算、配列番号２とエラーフラグを初期化
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListAfterIndex1Countup"] = function(session,m,v){

		session.transaction.issuerScriptCmdListAfterIndex1 += 1;
		session.transaction.issuerScriptCmdListAfterIndex2 = -1;
		session.transaction.issuerScriptCmdErrorFlg = false;

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * 完了処理 issuerScript(GAC後)判定
	 */
	this.branch["EMV56_switchSelect"] = function(session) {
		if (Utility.isEmpty(session.transaction.issuerScriptCmdListAfter)
				|| session.transaction.issuerScriptCmdListAfter.length == 0) {
			// issuerScript(GAC後)無し
			return "EMV58"; // コマンド実行結果検証

		} else if (session.transaction.issuerScriptCmdListAfter.length > session.transaction.issuerScriptCmdListAfterIndex1) {
			return "EMV56_1"; // 次の発行者スクリプト(GAC後)

		} else {
			return "EMV58"; // コマンド実行結果検証
		}
	};

	/**
	 * 発行者スクリプト(GAC後)実行ループフロー２
	 */
	this.flow.EMV56_1 = [
		{"type":"emv","obj":"issuerScriptCmdListAfterIndex2Countup"}
		,{"type":"branch","obj":"EMV56_1_switchSelect"}
	];

	/**
	 * 完了処理 発行者スクリプト(GAC後)の配列番号２を１加算
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListAfterIndex2Countup"] = function(session,m,v){

		session.transaction.issuerScriptCmdListAfterIndex2 += 1;
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};
		return rtn;
	};

	/**
	 * 完了処理 issuerScript(GAC後)判定
	 */
	this.branch["EMV56_1_switchSelect"] = function(session) {

		var i = session.transaction.issuerScriptCmdListAfterIndex1;
		var j = session.transaction.issuerScriptCmdListAfterIndex2;

		if (session.transaction.issuerScriptCmdListAfter[i].length <= session.transaction.issuerScriptCmdListAfterIndex2) {
			return "EMV56"; // 次のタグの発行者スクリプト(GAC後)
		}
		if (session.transaction.issuerScriptCmdErrorFlg) {
			// 同タグ内でSWエラー
			return "EMV56_2"; // 空の結果を設定し次の発行者スクリプト(GAC後)

		} else if (Utility.isEmpty(session.transaction.issuerScriptCmdListAfter[i][j])) {
			// 空のコマンド
			return "EMV56_2"; // 空の結果を設定し次の発行者スクリプト(GAC後)

		} else {
			return "EMV57"; // 発行者スクリプト(GAC後)
		}
	};

	/**
	 * 発行者スクリプト(GAC後)ブランク結果取得
	 */
	this.flow.EMV56_2 = [
		{"type":"emv","obj":"issuerScriptCmdListAfterBlankResultSet"}
		,{"type":"move","obj":"EMV56_1"}
	];

	/**
	 * NODEIN 発行者スクリプト(GAC後)ブランク結果取得
	 */
	this.procedureSet["NODEIN-emv-issuerScriptCmdListAfterBlankResultSet"] = function(session,m,v){

		if (Utility.isEmpty(session.transaction.issuerScriptCmdListResAfter)) {
			session.transaction.issuerScriptCmdListResAfter = new Array();
		}
		session.transaction.issuerScriptCmdListResAfter.push("");
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		return rtn;
	};

	/**
	 * 発行者スクリプト(GAC後)実行フロー
	 */
	this.flow.EMV57 = [
		{"type":"emv","obj":"issuerScriptAfter"}
		,{"type":"branch","obj":"EMV57_switchSelect"}
	];

	/**
	 * NODEIN 発行者スクリプト(GAC後)実行
	 */
	this.procedureSet["NODEIN-emv-issuerScriptAfter"] = function(session, m ,v) {
		return {
			"device":{
				"name":"iccard"
					,"value":{
						"type":"iccard_output"
							,"obj":"apduresponse"
								,"param":{
									"in_apdu_data":session.transaction.issuerScriptCmdListAfter[session.transaction.issuerScriptCmdListAfterIndex1][session.transaction.issuerScriptCmdListAfterIndex2]
								}
					}
			}
		};
	};

	/**
	 * NODEIN 発行者スクリプト(GAC後)結果取得
	 */
	this.procedureSet["emv-issuerScriptAfter_RECIEVES_DNX-iccard_output-apduresponse"] = function(session, m ,v) {

		var rtn = {};
		if (Utility.isString(v.result)) {

			// デバイスからの戻りが4文字以上（エラーコード以外）の場合、処理を行う
			if (Utility.isEmpty(session.transaction.issuerScriptCmdListResAfter)) {
				session.transaction.issuerScriptCmdListResAfter = new Array();
			}
			session.transaction.issuerScriptCmdListResAfter.push(Utility.toLowerCase(v.result));
			rtn = {
					"flowControl":{
						"name":"flownext"
					}
			};
		}
		return rtn;
	};

	/**
	 * 完了処理 発行者スクリプト(GAC後)の結果判定
	 */
	this.branch["EMV57_switchSelect"] = function(session) {
		var issuerScriptCmdResAfter = session.transaction.issuerScriptCmdListResAfter[session.transaction.issuerScriptCmdListResAfter.length - 1];
		var sw1 = issuerScriptCmdResAfter.substring(issuerScriptCmdResAfter.length - 4, issuerScriptCmdResAfter.length - 2);
		if(sw1 == "90" || sw1 == "62" || sw1 == "63") {
			// sw1 = 90 or 62 or 63
			return "EMV56_1"; // 正常 (issuerScriptCmdAfter)
		} else {
			return "EMV57_1"; // エラー (コマンド実行結果検証)
		};
	};

	/**
	 * 発行者スクリプト(GAC後)のタグ別コマンド結果フラグにTrue(エラー)を設定する。
	 */
	this.flow.EMV57_1 = [
		{"type":"emv","obj":"setIssuerScriptCmdErrorFlgTrue"}
		,{"type":"move","obj":"EMV56_1"}
	];


	/**
	 * 業務ルール:印字情報生成
	 */
	this.flow.EMV58 = [
		{"type":"emv","obj":"completionResultCreate"}
		,{"type":"move","obj":"endOfEmv"}
	];

	/**
	 * NODEIN 完了処理・コマンド実行・コマンド実行結果検証(業務ルール:印字情報生成)
	 */
	this.procedureSet["NODEIN-emv-completionResultCreate"] = function(session, m ,v) {

		var rtn = {
			"device":{
				"name":"network"
				,"value":{
					"type":"CreditSales_SA1901_2"
					,"session":session
				}
			}
		};

		return rtn;
	};

	/**
	 * 終了処理
	 */
	this.flow.endOfEmv = [
		{"type":"release","obj":"iccard"}
	];

	this.flow.endOfIcCardError = [
		{"type":"release","obj":"icCardError"}
	];


	/**
	 * NODEOUT 自身のフローの場合、処理を行います
	 */
	this.procedureSet["NODEOUT"] = function(session,m,v) {
		if (v.flowType != this.Instance.flowType ) {
			return {};
		}

		var rtn = {};
		if (v && v.direction) {
			rtn = {
				"flowControl":{
					"name":v.direction
				}
			};
		}

		return rtn;
	};


	/**
	 * IC処理中
	 */
	this.procedureSet["wait-amountConfirm_RECIEVES_NODEIN-wait-icProcess"] = function(session,m,v){

		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
			};
		return rtn;
	};

	/**
	 * EMVイベント
	 */
	this.procedureSet["EMV"] = function(session,m,v){
		var rtn = {
				"flowControl":{
					"name":"flownext"
				}
		};

		for(var key in v) {
			session.transaction[key] = v[key];
		}

		return rtn;
	};

	/**
	 *
	 */
	this.procedureSet["FRT-wait-cardReadWait"] = function(session,m,v) {
		var rtn = {
			"state":{
				"name" : "release"
			}
		};

		return rtn;
	};

};

/**
 * 「ICカード」-「EMV仕様」クラスの基底クラスはメディアルール基底クラスです。
 */
EMVRuleModel.prototype = new MediaRuleBaseModel();

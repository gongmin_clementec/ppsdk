        /**
         * EMVカーネルコンテキスト
         */
        var EMVKernel = {
           	"kernel":{
            		"pre":new IcCardMediaModel()
        			,"main":new EMVRuleModel()
            	}
           	,"contextKey":null
            ,"property":{
                "EMV_SELECT_FILENAME" : "1PAY.SYS.DDF01"
                ,"EMV_SELECT_FILENAME2" : "2PAY.SYS.DDF01"
            }
           	,"init":function(session){
    			this.contextKey = "pre";
    			EMV_SELECT_FILENAME = this.property["EMV_SELECT_FILENAME"];
    			delete session.state;
    		}
           	,"changeContext":function(mode){
           		if (mode == "main") this.contextKey = "main";
           		if (mode == "pre") this.contextKey = "pre";
           	}
            ,"getCurrentNode" : function(session){
            	var node = {};
            	try {
            	   	node = {
                		"type":this.kernel[this.contextKey].flow[session.state.flow][session.state.nodeNumber].type
                		,"obj":this.kernel[this.contextKey].flow[session.state.flow][session.state.nodeNumber].obj
                		,"flow":session.state.flow
                		,"context":this.contextKey
                	};
            	} catch(ex) {
                	return undefined;
            	}
            	return node;
            }
            ,"execute":function(modelControl,session){
            	var rtn = this.procedure(modelControl,session);
            	if (this.contextKey == "pre" && (session.transaction.judgeCardcoResult != "undefined" || session.transaction.kid != "undefined") && session.state.flow == "endOfEmv" && session.state.nodeNumber == 1){
            		this.contextKey = "main";
            		session.state = {
            			"flow":this.kernel[this.contextKey].firstFlow
            			,"nodeNumber":0
            			,"activeChildren":new Array()
            			,"childrenState":{}
         				,"history":new Array()
            		};
            		//カーネルコンテキストの境界にきたら金額入力開始待ちをwaitingコントロールで通知する
            		rtn = {};
            		rtn.waiting = {"name":"wait-NODEIN","value":{"type":"input", "obj":"amount"}};
            	}
            	//wait系nodeに入ったらwaitingコントロールで通知する
            	try {
                	if (this.kernel[this.contextKey].flow[session.state.flow][session.state.nodeNumber].type == "wait"){
                		rtn.waiting = {"name":"wait","value":{"type":"wait", "obj":this.kernel[this.contextKey].flow[session.state.flow][session.state.nodeNumber].obj}};
                	}
            	} catch(ex){
            		//例外時は何もしない
            	}
                console.log(rtn);
            	return rtn;
            }
        };
        /**
         * EMVカーネル実行メソッド
         * @param modelControl
         * @param session
         * @returns
         */
        EMVKernel.procedure = function(modelControl,session){
            var rtnvc = {};
//            var requestChannel = null;
//            var requestFlowType = null;
            var actionIsContinue = true;
            var actionChain = new Array();
            var rtnmr = null;
            var rtnmc = null;
            var mergedViewControl = null;
            try { //■暫定：デバッグ用に例外キャッチを一時中断

                //アクションチェーン用whileループ。再帰呼出は性能がかなり低下するため使用していない。
                while(actionIsContinue){
                    actionIsContinue = false;

                    //chainが存在していれば、actionChainからshiftして実行
                    if (actionChain.length > 0) {
                        rtnmc = actionChain.shift();
                    } else {
                        rtnmc = modelControl;//□□

                        // 優先制御の実施
                        //requestChannel = rtnmc.channel;
                        //requestFlowType = rtnmc.flowType;
                        /*
                        // 優先制御が取得できない場合はModelの呼び出し処理を行わずに終了させる//×××
                        if (!this.setUpPriorityControl(action.viewObjectId, requestChannel, requestFlowType)) {//×××
                            //無効//×××
                            rtnmc = {//×××
                                "method":["_none_","_none_","_none_"]//×××
                                ,"arg":null//×××
                            };//×××
                        }//×××*/
                    }

                    /*
                    if (rtnmc) {//nullでなければプロトコルバージョン番号を付与//×××
                        rtnmc.protocolVersion = this.viewControlVersion;//×××
                    }//×××
                    */

                    if (rtnmc.method[0] != "_none_") {
                        rtnmr = this.kernel[this.contextKey].procedure(rtnmc, session);//□□

                        //"modelResponse"に"chain"配列が含まれていれば、isContinueをtrueにして,アクション配列を更新
                        if (rtnmr.chain && rtnmr.chain.length > 0) {
                            for (var i = 0; i < rtnmr.chain.length; i++) {
                                actionChain.push({
                                    "method":[rtnmr.chain[i].method[0],rtnmr.chain[i].method[1],rtnmr.chain[i].method[2]]
                                });

                                if (rtnmr.chain[i].arg) {
                                    actionChain[actionChain.length -1]["arg"] = rtnmr.chain[i].arg;
                                } else {
                                    actionChain[actionChain.length -1]["arg"] = [{"value":""}];
                                }
                            }
                        }

                        if (actionChain.length > 0) {
                            actionIsContinue = true;
                        }

                        if (actionIsContinue) {
                            rtnvc = rtnmr;
                        } else {
                            rtnvc = rtnmr;//□□
                        }

                        // ViewCOntrolがnullでなければプロトコルバージョン番号を付与/×××
/*                        if (rtnvc) {/×××
                            rtnvc.protocolVersion = this.viewControlVersion;/×××
                        }/×××
*/

                        if (rtnvc && !mergedViewControl) {
                            mergedViewControl = {};
                        }

                        mergedViewControl = mergeControl(mergedViewControl, rtnvc);

                    } else {
                        //アクションは発生したが、何もしない処理
                    }
                }//アクションチェーン用whileループ

                // 優先制御を解除を行う
//                this.releasePriorityControl(action.viewObjectId, requestChannel, requestFlowType);/×××

                if (mergedViewControl) {
                    //actionのViewオブジェクトIDを引き継ぐ
//                    mergedViewControl["viewObjectId"] = action.viewObjectId;

                    delete mergedViewControl.chain;
                    delete mergedViewControl.direction;
                }

        //  } catch (ex) {//例外発生処理
//              //hpsjsExceptionクラスを生成
//              alert(ex.toString());
//              rtnvc = {"screen":{
//                          "name":"displaySystemError","value":{"id":"MAX1","exception":""}
//                      }
//              };
//              rtnvc["screen"]["value"]["exception"] = ex.toString();

//              throw ex;

            } finally {

            }
            return mergedViewControl;
        };


        //初期化の実行
        EMVKernel.init(session);

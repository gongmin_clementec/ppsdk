//
//  PPBridge.h
//  ppsdk
//
//  Created by Clementec on 2017/05/29.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef BOOL (^PPDispatchCallback_t)(NSString*, NSString**);
typedef void (^PPDispatchCancelCallback_t)();
typedef void (^PPFinishCallback_t)(NSString*);
typedef void (*ppsdkLogFunction_t)(const char* file, const char* func, int line, const char* what);

@interface PPBridge : NSObject

+ (PPBridge*)sharedInstance;

- (void)updateConfigure:(NSString*)desc
                   json:(NSString*)json;

- (void)setTrainingMode:(BOOL)bTrainigMode;
- (void)setUseDigiSignInTrainingMode:(BOOL)bUse;


- (void)applyJMupsOpenTerminalResponse:(NSString*)json;
- (NSString*)getJMupsOpenTerminalInfo;

- (void)setJMupsNfcMasterData:(NSString*)json;
- (NSString*)getJMupsNfcMasterData;

- (NSString*)getJMupsTradeResultDataIfExists;    /* onFinish以降で利用可能（エラー終了時POS連返却で使用） */

- (void)executeJob:(NSString*)jobName
        onDispatch:(PPDispatchCallback_t)onDispatch
  onDispatchCancel:(PPDispatchCancelCallback_t)onDispatchCancel
          onFinish:(PPFinishCallback_t)onFinish;


- (NSString*)hexStringFromDigiSignImage:(CGImageRef)image
                                modulus:(NSString*)modulus
                               exponent:(NSString*)exponent;


- (void)abort;
- (void)clearCardReaderDisplay;
- (void)disconnectCardReader;
- (NSString*)getTerminalID;
- (BOOL)isNeedTaxOtherAmount;
- (BOOL)isNeedProductCode;
- (void)checkMasterData;
- (BOOL)isTrainingMode;

@end


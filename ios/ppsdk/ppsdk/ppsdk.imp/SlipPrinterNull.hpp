//
//  SlipPrinterNull.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_SlipPrinterNull__)
#define __h_SlipPrinterNull__

#import <Foundation/Foundation.h>
#include "SlipPrinter.h"

class SlipPrinterNull :
    public SlipPrinter
{
private:
protected:
    virtual auto printPageSelf(SlipDocument::sp slip)  -> pprx::observable<pprx::unit>;

    virtual auto openDocumentSelf() -> pprx::observable<pprx::unit>;
    virtual auto openPageSelf() -> pprx::observable<pprx::unit>;
    virtual auto closePageSelf() -> pprx::observable<pprx::unit>;
    virtual auto closeDocumentSelf() -> pprx::observable<pprx::unit>;

    virtual void writeString(const std::string& str);
    virtual void writeBytes(const bytes_t& bytes);

    virtual bool hasAutoCutter() const;
    virtual int  getRowChars() const;
    virtual int  getRowPixels() const;
    
public:
            SlipPrinterNull(const __secret& s);
    virtual ~SlipPrinterNull();
};


#endif /* !defined(__h_SlipPrinterNull__) */

//
//  BaseSystemImpIOS.hpp
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#ifndef BaseSystemIOS_hpp
#define BaseSystemIOS_hpp

#include "PPReactive.h"

class BaseSystemImpIOS :
    public BaseSystem::imp
{
private:
    pprx::subjects::behavior<BaseSystem::ApplicationState>  m_applicationState;
    
protected:
    
public:
    BaseSystemImpIOS();
    virtual ~BaseSystemImpIOS() = default;
    
    virtual void callFunctionWithCatchException(boost::function<void()> func);
    virtual void invokeMainThread(boost::function<void()> func);
    virtual void invokeMainThreadAsync(boost::function<void()> func);
    virtual bool isMainThread();
    virtual std::string sjisToUtf8(const std::string& src);
    virtual boost::filesystem::path getResourceDirectoryPath();
    virtual boost::filesystem::path getDocumentDirectoryPath();
    virtual boost::shared_ptr<SerialDevice> createSerialDevice();
    virtual const_json_t getSystemInformation();
    virtual std::string getDeviceUuid();

    virtual boost::optional<std::string> getSecureElementText(const std::string& name);
    virtual bool        setSecureElementText(const std::string& name, const std::string& value);
    
    virtual boost::optional<std::string> getSharedAreaText(const std::string& name);
    virtual bool setSharedAreaText(const std::string& name, const std::string& value);

    virtual bool certificateIsInstalled();
    virtual bool certificateInstall(const boost::filesystem::path& path, const std::string& password);
    virtual bool certificateUninstall();
    
    virtual auto canSendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool>;
    virtual auto sendTextToAnotherApplication(const std::string& text) -> rxcpp::observable<bool>;
    virtual auto applicationState() -> rxcpp::observable<BaseSystem::ApplicationState>;
    virtual BaseSystem::OSType getOSType();
    virtual auto soundPlay(const std::string& soundFile) -> void;
    
    void setApplicationState(BaseSystem::ApplicationState);
};

#endif /* BaseSystemIOS_hpp */

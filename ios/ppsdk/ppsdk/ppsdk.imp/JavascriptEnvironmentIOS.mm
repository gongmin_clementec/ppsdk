//
//  JavascriptEnvironmentIOS.mm
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "JavascriptEnvironmentIOS.hpp"
#include "BaseSystem.h"

@interface ppWebViewDelegator()
{
    JavascriptEnvironmentIOS* m_parent;
}
- (id)initWithParent:(JavascriptEnvironmentIOS*)parent;
@end


@implementation ppWebViewDelegator

- (id)initWithParent:(JavascriptEnvironmentIOS*)parent
{
    self = [super init];
    if(self != nil){
        m_parent = parent;
    }
    return self;
}

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    return m_parent->shouldStartLoadWithRequest(request, navigationType);
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    m_parent->webViewDidStartLoad();
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    m_parent->webViewDidFinishLoad();
}

- (void)webView:(UIWebView *)webView
didFailLoadWithError:(NSError *)error
{
    m_parent->didFailLoadWithError(error);
}

@end

JavascriptEnvironmentIOS::JavascriptEnvironmentIOS(const __secret& s) :
    JavascriptEnvironment(s)
{
    m_delegator = [[ppWebViewDelegator alloc] initWithParent:this];

    BaseSystem::instance().invokeMainThread([=]{
        m_webView = [[UIWebView alloc] init];
        m_webView.delegate = m_delegator;
    });
}

/* virtual */
JavascriptEnvironmentIOS::~JavascriptEnvironmentIOS()
{
}

/* virtual */
auto JavascriptEnvironmentIOS::load
(
 const std::string& htmlText,
 const std::string& documentBasePath
 ) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    auto hold_this = shared_from_this();
    return pprx::just(pprx::unit(), pprx::observe_on_thread_pool())
    .observe_on(pprx::observe_on_main_thread())
    .flat_map([=](auto){
        hold_this->explicitCapture();
        auto html = [NSString stringWithUTF8String:htmlText.c_str()];
        auto path = [NSURL fileURLWithPath:[NSString stringWithUTF8String:documentBasePath.c_str()]];
        [m_webView loadHTMLString:html  baseURL:path];
        return m_subjectWebViewDidFinishLoad.get_observable();
    }).as_dynamic()
    .amb(pprx::observe_on_thread_pool(), m_subjectDidFailLoadWithError.get_observable());
}

/* virtual */
auto JavascriptEnvironmentIOS::call(const std::string& jsText)
 -> pprx::observable<std::string>
{
    LogManager_Function();
    auto hold_this = shared_from_this();
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_main_thread())
    .flat_map([=](auto){
        hold_this->explicitCapture();
        auto text = [NSString stringWithUTF8String:jsText.c_str()];
        auto resp = [m_webView stringByEvaluatingJavaScriptFromString:text];
        return pprx::just(std::string([resp UTF8String]));
    }).as_dynamic()
    .observe_on(pprx::observe_on_thread_pool());
}

BOOL JavascriptEnvironmentIOS::shouldStartLoadWithRequest(NSURLRequest* request, UIWebViewNavigationType type)
{
    return YES;
}

void JavascriptEnvironmentIOS::webViewDidStartLoad()
{
    
}

void JavascriptEnvironmentIOS::webViewDidFinishLoad()
{
    LogManager_Function();
    //NSLog(@"%@", [m_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].outerHTML"]);
    m_subjectWebViewDidFinishLoad.get_subscriber().on_next(pprx::unit());
    m_subjectWebViewDidFinishLoad.get_subscriber().on_completed();
}

void JavascriptEnvironmentIOS::didFailLoadWithError(NSError* error)
{
    LogManager_Function();
    m_subjectDidFailLoadWithError.get_subscriber()
    .on_error(make_error_internal("internal"));
    m_subjectDidFailLoadWithError.get_subscriber().on_completed();
}




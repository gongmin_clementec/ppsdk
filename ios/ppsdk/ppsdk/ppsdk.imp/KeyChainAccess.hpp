//
//  KeyChainAccess.hpp
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__hpp_KeyChainAccess__)
#define __hpp_KeyChainAccess__

#include <Foundation/Foundation.h>
#include <string>
#include <boost/optional.hpp>

class KeyChainAccess
{
private:
    
protected:
    
public:
    static NSArray* getIdentities(const std::string& label);
    static NSURLCredential* getCredential(const std::string& label);
    static bool isCredentialInstalled(const std::string& label);
    static bool installCredential(const std::string& label, const std::string& file, const std::string& password);
    static bool uninstallCredential(const std::string& label);

    static boost::optional<std::string> getString(const std::string& label, const std::string& generic, const std::string& account);
    static bool deleteString(const std::string& generic, const std::string& account);
    static bool createString(const std::string& value, const std::string& label, const std::string& generic, const std::string& account);
    static bool setString(const std::string& value, const std::string& label, const std::string& generic, const std::string& account);


    static boost::optional<std::string> getTmsPassword(const std::string& server);
    static bool setTmsPassword(const std::string& server, const std::string& newValue);
    static void deleteAllTmsPassword();
};


#endif /* !defined(__hpp_KeyChainAccess__) */

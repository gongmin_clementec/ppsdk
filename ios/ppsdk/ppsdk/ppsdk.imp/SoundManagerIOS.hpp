//
//  SoundManager.hpp
//  pokepos.felica
//
//  Created by clementec on 2016/06/06.
//  Copyright © 2016年 Clementec Co., Ltd. All rights reserved.
//

#ifndef SoundManager_hpp
#define SoundManager_hpp

#include "Singleton.h"
#include <map>
#import <AVFoundation/AVFoundation.h>

class SoundManager : public Singleton<SoundManager>
{
friend class Singleton<SoundManager>;
    
private:
    class WrapAVAudioPlayer
    {
    private:
        AVAudioPlayer* m_player;
    protected:
    public:
        WrapAVAudioPlayer() : m_player(nil) {}
        WrapAVAudioPlayer(AVAudioPlayer* player) : m_player(player) {}
        virtual ~WrapAVAudioPlayer()
        {
            m_player = nil;
        }
        
        AVAudioPlayer* get() { return m_player; }
    };
    
    std::map<std::string,WrapAVAudioPlayer>  m_players;
    
    SoundManager();
    virtual ~SoundManager();
    
    void    addSound(const std::string& name, const std::string& filename, bool bLoop);
    void    stop();
    std::string getResourceFilePath(const std::string& filename);

protected:
    
public:
    void play(int type, int code);
    void play(const std::string& name);
};

#endif /* SoundManager_hpp */

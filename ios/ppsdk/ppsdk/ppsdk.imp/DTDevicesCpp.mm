//
//  DTDevicesCpp.mm
//  ppsdk
//
//  Created by tel on 2017/06/20.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "DTDevicesCpp.hpp"
#include "LogManager.h"
#include "IOSUtilities.hpp"

@interface ppDTDevicesCpp_Delegator()
{
    DTDevicesCpp::EventListener* m_listener;
}
- (id)initWithListener:(DTDevicesCpp::EventListener*)listener;
@end


@implementation ppDTDevicesCpp_Delegator

- (id)initWithListener:(DTDevicesCpp::EventListener*)listener
{
    self = [super init];
    if(self != nil){
        m_listener = listener;
    }
    return self;
}

- (void)connectionState:(int)state
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->connectionState((CONN_STATES)state);
        });
    }
    else{
        m_listener->connectionState((CONN_STATES)state);
    }
}

-(void)deviceFeatureSupported:(int)feature value:(int)value
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->deviceFeatureSupported((FEATURES)feature, value);
        });
    }
    else{
        m_listener->deviceFeatureSupported((FEATURES)feature, value);
    }
}

-(void)rfCardDetected:(int)cardIndex info:(DTRFCardInfo *)info
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->rfCardDetected(cardIndex, info);
        });
    }
    else{
        m_listener->rfCardDetected(cardIndex, info);
    }
}

-(void)smartCardInserted:(SC_SLOTS)slot
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->smartCardInserted(slot);
        });
    }
    else{
        m_listener->smartCardInserted(slot);
    }
}

-(void)smartCardRemoved:(SC_SLOTS)slot
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->smartCardRemoved(slot);
        });
    }
    else{
        m_listener->smartCardRemoved(slot);
    }
}

-(BOOL)pinEntryWaitRunLoop:(NSError **)error
{
    return m_listener->pinEntryWaitRunLoop(error);
}

-(void)PINEntryCompleteWithError:(NSError *)error
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->PINEntryCompleteWithError(error);
        });
    }
    else{
        m_listener->PINEntryCompleteWithError(error);
    }
}

-(void)emv2OnOnlineProcessing:(NSData *)data;
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->emv2OnOnlineProcessing(data);
        });
    }
    else{
        m_listener->emv2OnOnlineProcessing(data);
    }
}

-(void)emv2OnUserInterfaceCode:(int)code status:(int)status holdTime:(NSTimeInterval)holdTime
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->emv2OnUserInterfaceCode(code, status, holdTime);
        });
    }
    else{
        m_listener->emv2OnUserInterfaceCode(code, status, holdTime);
    }
}

-(void)emv2OnTransactionFinished:(NSData *)data
{
    if(BaseSystem::instance().isMainThread()){
        BaseSystem::instance().post([=](){
            m_listener->emv2OnTransactionFinished(data);
        });
    }
    else{
        m_listener->emv2OnTransactionFinished(data);
    }
}

@end

DTDevicesCpp::DTDevicesCpp(EventListener* listner) :
    m_listner(listner),
    m_delegator(nil),
    m_dtd(nil)
{
    m_delegator = [[ppDTDevicesCpp_Delegator alloc] initWithListener:m_listner];
    m_dtd = [DTDevices sharedDevice];
    [m_dtd addDelegate:m_delegator];
}

/* virtual */
DTDevicesCpp::~DTDevicesCpp()
{
    
}

void DTDevicesCpp::connect()
{
    LogManager_Function();
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        [m_dtd connect];
    }
    LogManager_AddDebug("success");
}

void DTDevicesCpp::disconnect()
{
    LogManager_Function();
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        [m_dtd disconnect];
    }
    LogManager_AddDebug("success");
}

DTDeviceInfo* DTDevicesCpp::getConnectedDeviceInfo(SUPPORTED_DEVICE_TYPES deviceType /* = DEVICE_TYPE_PINPAD */, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd getConnectedDeviceInfo:deviceType error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::emv2Initialize(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2Initialise:&err]; /* DTLIBがtypo（sとzを間違えている）　*/
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2Deinitialize(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2Deinitialise:&err];   /* DTLIBがtypo（sとzを間違えている）　*/
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::uiShowInitScreen(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd uiShowInitScreen:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2SetPINOptions(EMV_PIN_ENTRY pinEntry, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2SetPINOptions:pinEntry
                                  error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2SetTransactionType(int type, int amount, int currencyCode, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2SetTransactionType:type
                                      amount:amount
                                currencyCode:currencyCode
                                       error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2StartTransactionOnInterface(int interfaces, int flags, NSData* initData, NSTimeInterval timeout, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2StartTransactionOnInterface:interfaces
                                                flags:flags
                                             initData:initData
                                              timeout:timeout
                                                error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2SetMessageForID(int messageID, FONTS font, NSString* message, NSError** error /* = nil */)
{
    //LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2SetMessageForID:messageID
                                     font:font
                                  message:message
                                    error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        //LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

DTEMV2Info* DTDevicesCpp::emv2GetInfo(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2GetInfo:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}



bool DTDevicesCpp::emv2LoadContactlessCAPK(NSData* capk, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2LoadContactlessCAPK:capk
                                        error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2LoadContactCAPK(NSData* capk, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2LoadContactCAPK:capk
                                    error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2LoadContactlessConfiguration(NSData* configuration, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2LoadContactlessConfiguration:configuration
                                                        error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::rtcSetDeviceDate(NSDate* date, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd rtcSetDeviceDate:date
                                 error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

bool DTDevicesCpp::emv2CancelTransaction(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2CancelTransaction:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

NSData* DTDevicesCpp::scCardPowerOn(SC_SLOTS slot, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd scCardPowerOn:slot
                              error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::emv2SetOnlineResult(NSData* result, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    const auto bSuccess = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2SetOnlineResult:result
                                    error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return bSuccess == YES;
}

NSData* DTDevicesCpp::emv2GetCardTracksEncryptedWithFormat(int format, int keyID, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2GetCardTracksEncryptedWithFormat:format
                                                     keyID:keyID
                                                     error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::emv2GetTagsEncrypted(NSData* tagList, int format, int keyType, int keyIndex, uint32_t packetID, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd emv2GetTagsEncrypted:tagList
                                    format:format
                                   keyType:keyType
                                  keyIndex:keyIndex
                                  packetID:packetID
                                     error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::ppadGetDUKPTKeyKSN(int keyID, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadGetDUKPTKeyKSN:keyID
                                   error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::ppadEnableStatusLine(bool enabled, NSError** error /*= nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadEnableStatusLine:enabled ? YES : NO
                                     error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::emv2SetMessage(int messageID, FONTS font, const std::string& message, NSError** error /* = nil */)
{
    return emv2SetMessageForID(messageID, font, IOSUtilities::convert(message), error);
}

void DTDevicesCpp::emv2SetMessages(const std::vector<emv2SetMessage_t>& msgs)
{
    for(auto msg : msgs){
        emv2SetMessage(msg.messageID, msg.font, msg.message);
    }
}

bool DTDevicesCpp::rfInit(int supportedCards, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd rfInit:supportedCards
                       error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::rfClose(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd rfClose:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

DTRFCardInfo* DTDevicesCpp::rfDetectCardOnChannel(int channel, NSData* additionalData, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd rfDetectCardOnChannel:channel
                            additionalData:additionalData
                                      error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
    
}

DTRFCardInfo* DTDevicesCpp::rfDetectCardOnChannel(int channel, NSData* additionalData, NSTimeInterval timeout, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd rfDetectCardOnChannel:channel
                             additionalData:additionalData
                                    timeout:timeout
                                      error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::felicaTransieve(int cardIndex, NSData* data, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd felicaTransieve:cardIndex
                             data:data
                            error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::playSound(int volume, const int* beepData, int length, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd playSound:volume
                       beepData:beepData
                         length:length
                          error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::uiDrawText(NSString* text, int topLeftX, int topLeftY, FONTS font, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd uiDrawText:text
                        topLeftX:topLeftX
                        topLeftY:topLeftY
                            font:font
                           error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
            LogManager_AddErrorF("error : %d", err.code);
            }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::uiControlLEDsWithBitMask(uint32_t mask, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd uiControlLEDsWithBitMask:mask
                                         error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::uiControlLEDsEx(const LEDControl* leds, int numLeds, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd uiControlLEDsEx:const_cast<LEDControl*>(leds)
                              numLeds:numLeds
                                error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::uiFillRectangle(int topLeftX /* = 0 */, int topLeftY /* = 0 */, int width /* = 0 */, int height /* = 0 */, UIColor* color /* = nil */, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        if(color == nil){
            color = [UIColor whiteColor];
        }
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd uiFillRectangle:topLeftX
                             topLeftY:topLeftY
                                width:width
                               height:height
                                color:color
                                error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
            LogManager_AddErrorF("error : %d", err.code);
            }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}


NSData* DTDevicesCpp::scCAPDU(SC_SLOTS slot, NSData* apdu, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd scCAPDU:slot
                         apdu:apdu
                        error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::scEncryptedCAPDU(SC_SLOTS slot, int encryption, int keyID, NSData* apdu, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd scEncryptedCAPDU:slot
                            encryption:encryption
                                 keyID:keyID
                                  apdu:apdu
                                 error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::ppadStartPINEntry(int startX, int startY, int timeout, char echoChar, NSString* message /* = nil */, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadStartPINEntry:startX
                                 startY:startY
                                timeout:timeout
                               echoChar:echoChar
                                message:message
                                  error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::ppadGetPINBlockUsingDUKPT(int dukptKeyID, NSData* keyVariant, int pinFormat, NSError** error /* = nil */)
{
    LogManager_Function();

    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadGetPINBlockUsingDUKPT:dukptKeyID
                                     keyVariant:keyVariant
                                      pinFormat:pinFormat
                                          error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::ppadVerifyPINOfflineEncryptedAndEncryptResponse(int encryption, int keyID, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadVerifyPINOfflineEncryptedAndEncryptResponse:encryption
                                                                keyID:keyID
                                                                error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

NSData* DTDevicesCpp::ppadVerifyPINOfflinePlainAndEncryptResponse(int encryption, int keyID, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadVerifyPINOfflinePlainAndEncryptResponse:encryption
                                                            keyID:keyID
                                                            error:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

DTPinpadInfo* DTDevicesCpp::ppadGetSystemInfo(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadGetSystemInfo:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}

bool DTDevicesCpp::ppadCancelPINEntry(NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto result = [&](){
        boost::unique_lock<boost::recursive_mutex> lock(m_mtx);
        return [m_dtd ppadCancelPINEntry:&err];
    }();
    if(err){
        if(error != nil) *error = err;
        LogManager_AddErrorF("error : %d", err.code);
    }
    else{
        LogManager_AddDebug("success");
    }
    return result;
}


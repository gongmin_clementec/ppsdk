//
//  IOSUtilities.cpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "IOSUtilities.hpp"
#include <boost/make_shared.hpp>


/* static */
NSData* IOSUtilities::convert(const bytes_t& src)
{
    return [NSData dataWithBytes:src.data() length:src.size()];
}

/* static */
bytes_sp IOSUtilities::convert(NSData* src)
{
    auto bytes = boost::make_shared<bytes_t>([src length]);
    std::memcpy(bytes->data(), [src bytes], [src length]);
    return bytes;
}

/* static */
NSString* IOSUtilities::convert(const std::string& src)
{
    return [NSString stringWithUTF8String:src.c_str()];
}

/* static */
std::string IOSUtilities::convert(NSString* src)
{
    return std::string([src UTF8String]);
}

/* static */
NSDate* IOSUtilities::convert(const boost::posix_time::ptime& src)
{
    auto s = boost::posix_time::to_iso_extended_string(src);
    s = s.substr(0, 19);
    auto formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    return [formatter dateFromString:[NSString stringWithUTF8String:s.c_str()]];
}


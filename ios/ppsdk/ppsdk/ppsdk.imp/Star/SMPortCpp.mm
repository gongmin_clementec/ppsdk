//
//  SMPortCpp.cpp
//  ppsdk
//
//  Created by tel on 2019/01/19.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//

#include "SMPortCpp.hpp"
#include "BaseSystem.h"
#include "LogManager.h"
#include "IOSUtilities.hpp"

SMPortCpp::SMPortCpp() : m_smport(nil)
{
    LogManager_Function();
}

/* virtual */
SMPortCpp::~SMPortCpp()
{
    LogManager_Function();
    close();
}

bool SMPortCpp::open(NSString* portName, NSString* portSettings, uint32_t ioTimeoutMillis)
{
    LogManager_Function();
    if(isOpened()){
        close();
    }
    const int retryMax = 10;
    int retry = 0;
    while(true){
        @try{
            m_smport = [SMPort getPort:portName :portSettings :ioTimeoutMillis];
            if(m_smport != nil){
                LogManager_AddInformation("port open success. -> wait 500ms");
                BaseSystem::sleep(500);
                break;
            }
            else{
                retry++;
                if(retry >= retryMax){
                    LogManager_AddErrorF("m_smport == nil -> retry %d -> error", retryMax);
                    break;
                }
                LogManager_AddWarningF("m_smport == nil -> wait and retry %d / %d", retry % retryMax);
                BaseSystem::sleep(1000);
            }
        }
        @catch(PortException* ex) {
            retry++;
            if(retry >= retryMax){
                LogManager_AddErrorF("port exception -> retry %d -> error", retryMax);
                break;
            }
            LogManager_AddWarningF("port exception -> wait and retry %d / %d", retry % retryMax);
            BaseSystem::sleep(1000);
        }
    }
    return isOpened();
}

void SMPortCpp::close()
{
    LogManager_Function();
    if(m_smport != nil){
        [SMPort releasePort:m_smport];
        m_smport = nil;
        BaseSystem::sleep(500);
    }
}

void SMPortCpp::getParsedStatus(void* starPrinterStatus, uint32_t level)
{
    LogManager_Function();
    [m_smport getParsedStatus:starPrinterStatus :level];
    BaseSystem::sleep(500);
}

void SMPortCpp::beginCheckedBlock(void* starPrinterStatus, uint32_t level)
{
    LogManager_Function();
    [m_smport beginCheckedBlock:starPrinterStatus :level];
    BaseSystem::sleep(500);
}

uint32_t SMPortCpp::writePort(u_int8_t const* writeBuffer, uint32_t offSet, uint32_t size)
{
    LogManager_Function();
    auto r = [m_smport writePort:writeBuffer :offSet :size];
    BaseSystem::sleep(500);
    return r;
}

bool SMPortCpp::endCheckedBlock(void* starPrinterStatus, uint32_t level, NSError** error /* = nil */)
{
    LogManager_Function();
    NSError* err;
    auto r = [m_smport endCheckedBlock:starPrinterStatus :level: &err];
    if(err){
        LogManager_AddError(IOSUtilities::convert([err description]));
    }
    if(error){
        *error = err;
    }
    BaseSystem::sleep(500);
    return r;
}

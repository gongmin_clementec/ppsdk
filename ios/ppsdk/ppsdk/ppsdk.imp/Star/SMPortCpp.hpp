//
//  SMPortCpp.hpp
//  ppsdk
//
//  Created by tel on 2019/01/19.
//  Copyright © 2019年 Clementec Co., Ltd. All rights reserved.
//
#import <StarIO/SMPort.h>

#if !defined(__hpp_SMPortCpp__)
#define __hpp_SMPortCpp__

class SMPortCpp
{
private:
    SMPort* m_smport;
    
protected:

public:
            SMPortCpp();
    virtual ~SMPortCpp();
    
    void        releasePort();
    
    bool        open(NSString* portName, NSString* portSettings, uint32_t ioTimeoutMillis);
    void        close();
    bool        isOpened() const { return m_smport != nil; }
    
    void        getParsedStatus(void* starPrinterStatus, uint32_t level);
    void        beginCheckedBlock(void* starPrinterStatus, uint32_t level);
    uint32_t    writePort(uint8_t const* writeBuffer, uint32_t offSet, uint32_t size);
    bool        endCheckedBlock(void* starPrinterStatus, uint32_t level, NSError** error = nil);
    
    SMPort*     raw() { return m_smport; }
    
};

#endif /* !defined(__h_SMPortCpp__) */

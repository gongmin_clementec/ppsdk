//
//  SlipPrinterNull.cpp
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipPrinterNull.hpp"
#include "LogManager.h"
#import "IOSUtilities.hpp"


SlipPrinterNull::SlipPrinterNull(const __secret& s) :
    SlipPrinter(s)
{
    LogManager_Function();
}

/* virtual */
SlipPrinterNull::~SlipPrinterNull()
{
    LogManager_Function();
}

/* virtual */
auto SlipPrinterNull::printPageSelf(SlipDocument::sp slip)
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto SlipPrinterNull::openDocumentSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto SlipPrinterNull::openPageSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}

/* virtual */
auto SlipPrinterNull::closePageSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}


/* virtual */
auto SlipPrinterNull::closeDocumentSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit());
}


/* virtual */
void SlipPrinterNull::writeString(const std::string& /* str */)
{
//    LogManager_Function();
}

/* virtual */
void SlipPrinterNull::writeBytes(const bytes_t& /* bytes */)
{
//    LogManager_Function();
}

/* virtual */
bool SlipPrinterNull::hasAutoCutter() const
{
    return false;
}

/* virtual */
int SlipPrinterNull::getRowChars() const
{
    return 32;
}

/* virtual */
int SlipPrinterNull::getRowPixels() const
{
    return 32 * 12;
}

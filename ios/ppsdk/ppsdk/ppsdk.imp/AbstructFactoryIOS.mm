//
//  AbstructFactoryIOS.mm
//  ppsdk
//
//  Created by Clementec on 2017/05/26.
//
//

#import <Foundation/Foundation.h>
#include "AbstructFactory.h"
#include "WebClientIOS.hpp"
#include "WebClientIOSJMups.hpp"
#include "BaseSystemImpIOS.hpp"
#include "CardReaderMiura.h"
#include "JavascriptEnvironmentIOS.hpp"
#include "IOSCGImage.hpp"

#if defined(RECRUIT)
    #include "SlipPrinterNull.hpp"
#else
    #include "CardReaderBluePad.hpp"
    #include "SlipPrinterStar.hpp"
#endif

class setup
{
private:
protected:
public:
    setup()
    {
        AbstructFactory::instance().createWebClient = []{
            return LazyObject::create<WebClientIOS>();
        };

        AbstructFactory::instance().createWebClientForJMups = []{
            return LazyObject::create<WebClientIOSJMups>();
        };

        AbstructFactory::instance().createBaseSystemImp = []{
            return new BaseSystemImpIOS();
        };
        
        AbstructFactory::instance().createCardReader = []{
            //return LazyObject::create<CardReaderBluePad>();
            return LazyObject::create<CardReaderMiura>();
        };
        
        AbstructFactory::instance().createSlipPrinter = []{
            #if defined(RECRUIT)
                return LazyObject::create<SlipPrinterNull>();
            #else
                auto printer = LazyObject::create<SlipPrinterStar>();
                auto fpath = [NSString stringWithUTF8String:(BaseSystem::instance().getResourceDirectoryPath() / "jmupslogo.bmp").string().c_str()];
                auto image = [[UIImage imageWithContentsOfFile: fpath] CGImage];
            
                IOSCGImage cgi(image);
                auto cgi2 = cgi.resizeAndDecolorToGrayScale();
                printer->setLogoImage(cgi2.toImage());
                return printer;
            #endif
        };

        AbstructFactory::instance().createJavascriptEnvironment = []{
            return LazyObject::create<JavascriptEnvironmentIOS>();
        };

    }
};

static setup setup;

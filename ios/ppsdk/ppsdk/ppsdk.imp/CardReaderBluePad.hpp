//
//  CardReaderBluePad.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__hpp_CardReaderBluePad__)
#define __hpp_CardReaderBluePad__

#include <Foundation/Foundation.h>
#include "CardReader.h"
#include "DTDevicesCpp.hpp"

class CardReaderBluePad :
    public CardReader,
    public DTDevicesCpp::EventListener
{
private:
    DTDevicesCpp        m_dtd;
    SC_SLOTS            m_currentSlot;
    timer_sp            m_connectTimer;
    AtomicObject<bool>  m_abortedForTrainingMode;
    bool                m_bEnableErrorOnSmatCardRemoved;
    bool                m_bInSeePhone;
    
    pprx::subject<const_json_t> m_subjectContactlessTransactionFinished;
    pprx::subject<const_json_t> m_subjectPINEntryCompleteWithError;
    
    std::vector<std::string>   m_callbackHandleFunctions;
    
    NSData*     createNfcTagListForTradeResult() const;
    uint64_t    bytesToUint64(NSData* data) const;
    std::string convertResultCodeForJMups(uint64_t resultCode, uint64_t kernel) const;
    std::string cardBrandFrom0xDF810C(NSData* data) const;
    std::string generateTransactionResultBit(NSArray* tags) const;
    
    void setMessages(const_json_t messages);
    
    
    auto readContactlessForTraining(const_json_t param) -> pprx::observable<const_json_t>;
    
    bool isValidAppVersion();
    
    bytes_t convertEncryptedApduResponseForJMups(NSData* emvData) const;

    auto waitPinInput() -> pprx::observable<const_json_t>;
    
protected:
    std::map<std::string,int>       tableStringToEMV_UIXXXX() const;
    std::map<std::string,FONTS>     tableStringToFONT_XXXX() const;
    
    virtual auto finalizeSelf() -> pprx::observable<pprx::unit>;
    virtual void prepareObservablesSelf();

public:
            CardReaderBluePad(const __secret& s);
    virtual ~CardReaderBluePad();


    virtual auto displayText(const std::string& str) -> pprx::observable<pprx::unit>;
    virtual auto displayTextNoLf(const std::string& str) -> pprx::observable<pprx::unit>;
    virtual auto displayIdleImage() -> pprx::observable<pprx::unit>;
    virtual auto connect() -> pprx::observable<pprx::unit>;
    virtual auto disconnect() -> pprx::observable<pprx::unit>;
    
    virtual auto beginCardRead(const_json_t param) -> pprx::observable<const_json_t>;
    virtual auto beginICCardTransaction() -> pprx::observable<pprx::unit>;
    virtual auto endICCardTransaction() -> pprx::observable<pprx::unit>;
    virtual auto setICCardRemoved() -> pprx::observable<pprx::unit>;

    virtual auto checkMasterData(const_json_t data) -> pprx::observable<const_json_t>;
    virtual auto updateMasterData(const_json_t data) -> pprx::observable<pprx::unit>;
    virtual auto readContactless(const_json_t param) -> pprx::observable<const_json_t>;
    
    virtual auto adjustDateTime(const boost::posix_time::ptime& dtime)-> pprx::observable<pprx::unit>;
    
    virtual auto sendApdu(const_json_t data) -> pprx::observable<const_json_t>;
    
    virtual auto verifyPlainOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto verifyEncryptedOfflinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto verifyEncryptedOnlinePin(const_json_t paramjson) -> pprx::observable<const_json_t>;
    virtual auto abortTransaction() -> pprx::observable<pprx::unit>;
    virtual auto txPlainApdu(const std::string& apdu) -> pprx::observable<const_json_t>;
    virtual auto txEncryptedApdu(const std::string& apdu) -> pprx::observable<const_json_t>;

    virtual auto getIfdSerialNo() -> pprx::observable<const_json_t>;

    virtual auto onlineProccessingResult(bool bApproved) -> pprx::observable<pprx::unit>;
    
    virtual auto getReaderInfo() -> pprx::observable<const_json_t>;
    virtual auto isCardInserted() -> pprx::observable<bool>;
    virtual auto updateFirmware(const boost::filesystem::path& basedir) -> pprx::observable<pprx::unit>;
    virtual auto getDisplayMsgForReader(const std::string& msgId, const bool bContactless = false) -> std::string;
    
    virtual void setLockTransactionState(bool blockd = false);
    virtual void connectionState(CONN_STATES state);
    virtual void deviceFeatureSupported(FEATURES feature, int value);
    virtual void rfCardDetected(int cardIndex, DTRFCardInfo* info);
    virtual void smartCardInserted(SC_SLOTS slot);
    virtual void smartCardRemoved(SC_SLOTS slot);
    virtual BOOL pinEntryWaitRunLoop(NSError** error);
    virtual void PINEntryCompleteWithError(NSError* error);
    virtual void emv2OnOnlineProcessing(NSData* data);
    virtual void emv2OnUserInterfaceCode(int code, int status, NSTimeInterval holdTime);
    virtual void emv2OnTransactionFinished(NSData* data);
    virtual auto getCurrentReaderInfo()  -> const_json_t;
    void    broadcastError(std::exception_ptr err);
    virtual auto clearCardreaderDisplay() -> pprx::observable<pprx::unit>;
};


#endif /* !defined(__hpp_CardReaderBluePad__) */

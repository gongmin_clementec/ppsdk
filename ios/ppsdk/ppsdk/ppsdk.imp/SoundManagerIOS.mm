//
//  SoundManager.cpp
//  ppsdk
//
//  Created by tel on 2020/03/11.
//  Copyright © 2020年 Clementec Co., Ltd. All rights reserved.
//

#include "SoundManagerIOS.hpp"
#include <boost/format.hpp>
#import <CoreFoundation/CoreFoundation.h>
#include "LogManager.h"
#include <boost/filesystem.hpp>
#include "PPConfig.h"

SoundManager::SoundManager()
{
    addSound("SOUND_TRADE_RESULT_NFC_APPROVED", "nfc.wav", false);
    addSound("SOUND_TRADE_RESULT_NFC_DECLINED", "nfc_error.wav", false);
}

/* virtual */
SoundManager::~SoundManager()
{
    stop();
}

void SoundManager::play(int type, int code)
{
    if(code == 99){
        stop();
        return;
    }
    play((boost::format("%d,%d") % type % code).str());
}

void SoundManager::play(const std::string& name)
{
    stop();
    auto it = m_players.find(name);
    if(it != m_players.end()){
        it->second.get().currentTime = 0;
        [it->second.get() play];
    }
}

void SoundManager::stop()
{
    for(auto it : m_players){
        [it.second.get() stop];
    }
}

void SoundManager::addSound(const std::string& name, const std::string& filename, bool bLoop)
{
    NSError *error = nil;
    auto path = getResourceFilePath(filename);
    NSURL *url = [[NSURL alloc] initFileURLWithPath:
                  [NSString stringWithUTF8String:
                                  getResourceFilePath(filename).c_str()]];
    auto player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if(bLoop){
        player.numberOfLoops = -1;
    }
    if ( error != nil )
    {
        NSLog(@"Error %@", [error localizedDescription]);
    }
    [player prepareToPlay];
    m_players[name] = WrapAVAudioPlayer(player);
}

/* Framework内のBundleにアクセスするために[NSBundle bundleForClass:]を使用する方法を採用する。 */
/* そのため、ダミーのクラスをここで定義しておく。 */
@interface DummyClassForSearchingBundle : NSObject
@end

@implementation DummyClassForSearchingBundle
@end

#include <boost/foreach.hpp>

namespace fs = boost::filesystem;
/* static */
std::string SoundManager::getResourceFilePath(const std::string& filename)
{
    NSBundle *bundle = [NSBundle bundleForClass:[DummyClassForSearchingBundle class]];
    boost::filesystem::path path(filename);
    std::string ext8 = path.extension().c_str();
    if(!ext8.empty()){
        if(ext8.at(0) == '.'){
            ext8 = std::string(ext8.begin() + 1, ext8.end());
        }
    }
    auto name   = [NSString stringWithUTF8String:path.stem().c_str()];
    NSString *dir = @"SoundResources";
    auto ext    = [NSString stringWithUTF8String:ext8.c_str()];
    auto nspath = [bundle pathForResource:name
                                   ofType:ext
                              inDirectory:dir];
    if(nspath != nil){
        return [nspath UTF8String];
    }
    return "";
}

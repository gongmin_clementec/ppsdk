//
//  IOSCGImage.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__hpp_IOSCGImage__)
#define __hpp_IOSCGImage__

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#include "BaseSystem.h"

class Image;

class IOSCGImage
{
private:
    CGImageRef  m_imageRef;
    NSData*     m_bits;
    
    void assign(CGImageRef src)
    {
        reset();
        m_imageRef = src;
    }
    
protected:
    
public:
    IOSCGImage() : m_imageRef(nil), m_bits(nil) {}

    IOSCGImage(CGImageRef src) : m_imageRef(nil), m_bits(nil)
    {
        reset(src);
    }
    
    IOSCGImage(const IOSCGImage& src) : m_imageRef(nil), m_bits(nil)
    {
        reset(src.m_imageRef);
    }
    
    void reset(CGImageRef ref = nil)
    {
        if(m_imageRef){
            ::CFRelease(m_imageRef);
            m_imageRef = nil;
        }
        if(ref != nil){
            m_imageRef = ::CGImageRetain(ref);
        }
        if(m_bits != nil){
            m_bits = nil;
        }
    }
    
    virtual ~IOSCGImage()
    {
        reset();
    }
    
    bool isEmpty() const { return m_imageRef == nil; }
    
    size_t width() const            { return ::CGImageGetWidth(m_imageRef); }
    size_t height() const           { return ::CGImageGetHeight(m_imageRef); }
    size_t rowBytes() const         { return ::CGImageGetBytesPerRow(m_imageRef); }

    size_t bitsPerPixel() const     { return ::CGImageGetBitsPerPixel(m_imageRef); }
    size_t bitsPerComponent() const { return ::CGImageGetBitsPerComponent(m_imageRef); }
    size_t bytesPerPixel() const    { return bitsPerPixel() / bitsPerComponent(); }

    template<class T> T bits()
    {
        if(m_bits == nil){
            CGDataProviderRef provider = ::CGImageGetDataProvider(m_imageRef);
            m_bits = (id)::CFBridgingRelease(::CGDataProviderCopyData(provider));
        }
        return reinterpret_cast<T>([m_bits bytes]);
    }
    
    IOSCGImage resizeAndDecolorToGrayScale(size_t width = 0, size_t height = 0);
    
    boost::shared_ptr<Image> toImage()
    {
        return boost::make_shared<Image>(bits<const void*>(), width(), height(), rowBytes(), bytesPerPixel());
    }
};




#endif /* !defined(__hpp_IOSCGImage__) */

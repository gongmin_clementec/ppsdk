//
//  DTDevicesCpp.hpp
//  ppsdk
//
//  Created by tel on 2017/06/20.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#if !defined(__hpp_DTDevicesCpp__)
#define __hpp_CardReaderBluePad__

#import "DTDevices.h"
#include "BaseSystem.h"
#include <string>

@interface ppDTDevicesCpp_Delegator: NSObject<DTDeviceDelegate>
@end

class DTDevicesCpp
{
public:
    class EventListener
    {
    public:
        virtual void connectionState(CONN_STATES state) = 0;
        virtual void deviceFeatureSupported(FEATURES feature, int value) = 0;
        virtual void rfCardDetected(int cardIndex, DTRFCardInfo* info) = 0;
        virtual void smartCardInserted(SC_SLOTS slot) = 0;
        virtual void smartCardRemoved(SC_SLOTS slot) = 0;
        virtual BOOL pinEntryWaitRunLoop(NSError** error) = 0;
        virtual void PINEntryCompleteWithError(NSError* error) = 0;
        virtual void emv2OnOnlineProcessing(NSData* data) = 0;
        virtual void emv2OnUserInterfaceCode(int code, int status, NSTimeInterval holdTime) = 0;
        virtual void emv2OnTransactionFinished(NSData* data) = 0;
    };
    
private:
    EventListener*          m_listner;
    ppDTDevicesCpp_Delegator* m_delegator;
    DTDevices*              m_dtd;
    boost::recursive_mutex  m_mtx;

protected:
    
public:
            DTDevicesCpp(EventListener* listner);
    virtual ~DTDevicesCpp();
    
    void    connect();
    void    disconnect();

    DTDeviceInfo* getConnectedDeviceInfo(SUPPORTED_DEVICE_TYPES deviceType = DEVICE_TYPE_PINPAD, NSError** error = nil);

    
    bool    emv2Initialize(NSError** error = nil);
    bool    emv2Deinitialize(NSError** error = nil);
    
    bool    uiShowInitScreen(NSError** error = nil);
    bool    emv2SetPINOptions(EMV_PIN_ENTRY pinEntry, NSError** error = nil);

    bool    emv2SetTransactionType(int type, int amount, int currencyCode, NSError** error = nil);
    bool    emv2StartTransactionOnInterface(int interfaces, int flags, NSData* initData, NSTimeInterval timeout, NSError** error = nil);
    
    bool    emv2SetMessageForID(int messageID, FONTS font, NSString* message, NSError** error = nil);
    DTEMV2Info* emv2GetInfo(NSError** error = nil);
    
    
    bool emv2LoadContactlessCAPK(NSData* capk, NSError** error = nil);
    bool emv2LoadContactCAPK(NSData* capk, NSError** error = nil);
    bool emv2LoadContactlessConfiguration(NSData* configuration, NSError** error = nil);

    bool rtcSetDeviceDate(NSDate* date, NSError** error = nil);
    bool emv2CancelTransaction(NSError** error = nil);

    NSData* scCardPowerOn(SC_SLOTS slot, NSError** error = nil);

    bool emv2SetOnlineResult(NSData* result, NSError** error = nil);
    NSData* emv2GetCardTracksEncryptedWithFormat(int format, int keyID, NSError** error = nil);
    NSData* emv2GetTagsEncrypted(NSData* tagList, int format, int keyType, int keyIndex, uint32_t packetID, NSError** error = nil);
    NSData* ppadGetDUKPTKeyKSN(int keyID, NSError** error = nil);
    
    bool ppadEnableStatusLine(bool enabled, NSError** error = nil);

    struct emv2SetMessage_t
    {
        int         messageID;
        FONTS       font;
        std::string message;
    };
    bool emv2SetMessage(int messageID, FONTS font, const std::string& message, NSError** error = nil);
    void emv2SetMessages(const std::vector<emv2SetMessage_t>& msgs);
    
    bool rfInit(int supportedCards, NSError** error = nil);
    bool rfClose(NSError** error = nil);
    
    DTRFCardInfo* rfDetectCardOnChannel(int channel, NSData* additionalData, NSTimeInterval timeout, NSError** error = nil);
    DTRFCardInfo* rfDetectCardOnChannel(int channel, NSData* additionalData, NSError** error = nil);
    
    NSData* felicaTransieve(int cardIndex, NSData* data, NSError** error = nil);
    
    bool playSound(int volume, const int* beepData, int length, NSError** error = nil);
    bool uiDrawText(NSString* text, int topLeftX, int topLeftY, FONTS font, NSError** error = nil);
    bool uiControlLEDsWithBitMask(uint32_t mask, NSError** error = nil);
    bool uiControlLEDsEx(const LEDControl* leds, int numLeds, NSError** error = nil);
    bool uiFillRectangle(int topLeftX = 0, int topLeftY = 0, int width = 0, int height = 0, UIColor* color = nil, NSError** error = nil);
    
    NSData* scCAPDU(SC_SLOTS slot, NSData* apdu, NSError** error = nil);
    NSData* scEncryptedCAPDU(SC_SLOTS slot, int encryption, int keyID, NSData* apdu, NSError** error = nil);
    
    bool ppadStartPINEntry(int startX, int startY, int timeout, char echoChar, NSString* message = nil, NSError** error = nil);
    NSData* ppadGetPINBlockUsingDUKPT(int dukptKeyID, NSData* keyVariant, int pinFormat, NSError** error = nil);
    NSData* ppadVerifyPINOfflineEncryptedAndEncryptResponse(int encryption, int keyID, NSError** error = nil);
    NSData* ppadVerifyPINOfflinePlainAndEncryptResponse(int encryption, int keyID, NSError** error = nil);
    DTPinpadInfo* ppadGetSystemInfo(NSError** error = nil);
    bool ppadCancelPINEntry(NSError** error = nil);
};





#endif /* !defined(__hpp_DTDevicesCpp__) */

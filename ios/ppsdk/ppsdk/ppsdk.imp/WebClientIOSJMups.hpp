//
//  WebClientIOSJMups.hpp
//  ppsdk
//
//  Created by Clementec on 2017/05/26.
//
//

#if !defined(__hpp_WebClientIOSJMups__)
#define __hpp_WebClientIOSJMups__

#include "WebClientIOS.hpp"

class WebClientIOSJMups :
    public WebClientIOS
{
private:
    NSURLCredential* m_credential;
    
protected:
    
public:
    WebClientIOSJMups(const __secret& s);
    virtual ~WebClientIOSJMups();
    
    virtual void initialize();
    
    virtual auto postAsync
    (
     const std::string& url,
     const_bytes_sp     postData,
     const_json_t       cookieData,
     const_json_t       extraHeaderData
     ) -> pprx::observable<response_t>;

    /* NSURLSessionDelegate */
    virtual void didReceiveChallenge(
             NSURLSession*                  session,
             NSURLAuthenticationChallenge*  challenge,
             comp::didReceiveChallenge_t    completionHandler);
};

#endif /* !defined(__hpp_WebClientIOSJMups__) */

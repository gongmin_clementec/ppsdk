//
//  WebClientIOS.hpp
//  ppsdk
//
//  Created by Clementec on 2017/05/26.
//
//

#if !defined(__hpp_WebClientIOS__)
#define __hpp_WebClientIOS__

#include <Foundation/Foundation.h>
#include "BaseSystem.h"
#include "WebClient.h"
#include "AtomicObject.h"
#include <mutex>

class WebClientIOS;

@interface ppHttpConnectionDelegator: NSObject<
        NSURLSessionDelegate,
        NSURLSessionTaskDelegate,
        NSURLSessionDataDelegate>

- (id)initWithParent:(WebClientIOS*)parent;
- (BOOL)isInvalidated;
@end



class WebClientIOS :
    public WebClient
{
private:

protected:
    ppHttpConnectionDelegator*    m_delegator;
    NSURLSession*               m_nsUrlSession;
    NSURLSessionConfiguration*  m_config;
    LazyObject::holder_t        m_thisHolder;

    struct taskDatas_t
    {
        pprx::subject<response_t>   subject;
        uint16_t    statusCode;
        bytes_sp    receiveData;
        json_t      receiveHeaders;
        json_t      receiveCookies;
        
        taskDatas_t() :
            statusCode(0),
            receiveData(boost::make_shared<bytes_t>())
        {
        }
    };
    using taskDatas_sp = boost::shared_ptr<taskDatas_t>;
    
    AtomicObject<std::map<NSURLSessionTask*,taskDatas_sp>> m_taskDatas;
    
    auto createTaskData(NSMutableURLRequest* request) -> pprx::observable<response_t>;

    void            discardTaskDataCallbacks(NSURLSessionTask* task);
    taskDatas_sp    fetchTaskData(NSURLSessionTask* task);
    void            deleteTaskData(NSURLSessionTask* task);
    
    void            setCookies(NSMutableURLRequest* request, const_json_t cookies);
    
    NSDateFormatter*    createCookieDateFormatter() const;

    void    downloadCommon
    (
     NSURLRequest* request,
     const boost::filesystem::path file,
     pprx::subscriber<pprx::unit> s);
    
public:
            WebClientIOS(const __secret& s);
    virtual ~WebClientIOS();

    virtual void initialize();
    virtual void finalize();

    virtual auto postAsync
    (
     const std::string& url,
     const_bytes_sp     postData,
     const_json_t       cookieData,
     const_json_t       extraHeaderData
     ) -> pprx::observable<response_t>;
    
    virtual auto getAsync
    (
     const std::string& url,
     const_json_t       cookieData
     ) -> pprx::observable<response_t>;

    virtual auto downloadFileWithGet
    (
     const std::string&             url,
     const boost::filesystem::path  file
     ) -> pprx::observable<pprx::unit>;

    virtual auto downloadFileWithPost
    (
     const std::string&             url,
     const_bytes_sp                 postData,
     const boost::filesystem::path  file,
     const_json_t                   extraHeaderData
     ) -> pprx::observable<pprx::unit>;

    virtual auto putFile
    (
     const std::string&             url,
     const boost::filesystem::path  file
     ) -> pprx::observable<pprx::unit>;
    
    struct comp
    {
        using didReceiveChallenge_t = void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential*);
        using didReceiveResponse_t  = void (^)(NSURLSessionResponseDisposition);
        using willCacheResponse_t   = void (^)(NSCachedURLResponse*);
    };
    
    using didReceiveChallenge_comp_t = void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential*);
   
    /* NSURLSessionDelegate */
    virtual void didReceiveChallenge(
                    NSURLSession*                  session,
                    NSURLAuthenticationChallenge*  challenge,
                    comp::didReceiveChallenge_t    completionHandler);

    virtual void didBecomeInvalidWithError(
                    NSURLSession*     session,
                    NSError*          error);

    /* NSURLSessionTaskDelegate */
    virtual void didCompleteWithError(
                    NSURLSession*     session,
                    NSURLSessionTask* task,
                    NSError*          error);

     /* NSURLSessionDataTaskDelegate */
    virtual void didReceiveResponse(
                    NSURLSession*           session,
                    NSURLSessionDataTask*   dataTask,
                    NSURLResponse*          response,
                    comp::didReceiveResponse_t completionHandler);
    
    virtual void willCacheResponse(
                    NSURLSession*                session,
                    NSURLSessionDataTask*        dataTask,
                    NSCachedURLResponse*         proposedResponse,
                    comp::willCacheResponse_t    completionHandler);
    
    virtual void didReceiveData(
                    NSURLSession*           session,
                    NSURLSessionDataTask*   dataTask,
                    NSData*                 data);
};

#endif /* !defined(__hpp_WebClientIOS__) */

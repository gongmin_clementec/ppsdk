//
//  SlipPrinterStar.h
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__h_SlipPrinterStar__)
#define __h_SlipPrinterStar__

#import <Foundation/Foundation.h>
#include "SlipPrinterESCP.h"
#include "SMPortCpp.hpp"

class SlipPrinterStar :
    public SlipPrinterESCP
{
private:
    SMPortCpp               m_printerPort;
    std::vector<uint8_t>    m_buffer;

protected:
    virtual auto openDocumentSelf() -> pprx::observable<pprx::unit>;
    virtual auto openPageSelf() -> pprx::observable<pprx::unit>;
    virtual auto closePageSelf() -> pprx::observable<pprx::unit>;
    virtual auto closeDocumentSelf() -> pprx::observable<pprx::unit>;

    virtual void writeString(const std::string& str);
    virtual void writeBytes(const bytes_t& bytes);

    virtual bool hasAutoCutter() const;
    virtual int  getRowChars() const;
    virtual int  getRowPixels() const;

    bool    isError(const StarPrinterStatus_2& stat) const;
    json_t  statusToErrorJson(const StarPrinterStatus_2& stat) const;
    json_t  portExceptionToErrorJson(PortException* exception) const;
    
public:
            SlipPrinterStar(const __secret& s);
    virtual ~SlipPrinterStar();
};


#endif /* !defined(__h_SlipPrinterStar__) */

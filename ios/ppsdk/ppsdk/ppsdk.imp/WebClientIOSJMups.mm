//
//  WebClientIOSJMups.mm
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "WebClientIOSJMups.hpp"
#include "KeyChainAccess.hpp"
#include "LogManager.h"
#include "PPConfig.h"

WebClientIOSJMups::WebClientIOSJMups(const __secret& s) :
    WebClientIOS(s),
    m_credential(nil)
{
    LogManager_Function();
}

/* virtual */
WebClientIOSJMups::~WebClientIOSJMups()
{
    LogManager_Function();
}


/* virtual */
void WebClientIOSJMups::initialize()
{
    LogManager_Function();
    WebClientIOS::initialize();
    auto label = PPConfig::instance().get<std::string>("hps.keychainStoreLabel");
    m_credential = KeyChainAccess::getCredential(label);
}


/* virtual */
auto WebClientIOSJMups::postAsync
(
    const std::string& url,
    const_bytes_sp     postData,
    const_json_t       cookieData,
    const_json_t       extraHeaderData
 ) -> pprx::observable<response_t>
{
    LogManager_Function();
    auto request    = [NSMutableURLRequest requestWithURL:
                       [NSURL URLWithString:
                        [NSString stringWithUTF8String:url.c_str()]]];
    
    setCookies(request, cookieData);
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[NSData dataWithBytes:postData->data() length:postData->size()]];

    return createTaskData(request);
}


void WebClientIOSJMups::didReceiveChallenge(
                   NSURLSession*                   session,
                   NSURLAuthenticationChallenge*   challenge,
                   didReceiveChallenge_comp_t      completionHandler)
{
    LogManager_Function();
    auto    disposition     = NSURLSessionAuthChallengePerformDefaultHandling;
    NSURLCredential* credential = nil;
    auto    protectionSpace = [challenge protectionSpace];
    auto    method          = [protectionSpace authenticationMethod];
    
    
    if([method isEqualToString:NSURLAuthenticationMethodClientCertificate]){
        if([challenge previousFailureCount] == 0){
            credential = m_credential;
            disposition = NSURLSessionAuthChallengeUseCredential;
        }
    }
    else if([method isEqualToString:NSURLAuthenticationMethodServerTrust]){
        auto trust = [protectionSpace serverTrust];
        auto testCredential = [NSURLCredential credentialForTrust:trust];
        
        credential = testCredential;
        disposition = NSURLSessionAuthChallengeUseCredential;
    }

    completionHandler(disposition, credential);
}

//
//  WebClientImpIOS.mm
//  ppsdk
//
//  Created by Clementec on 2017/05/26.
//
//

#import <Foundation/Foundation.h>
#include "WebClientIOS.hpp"
#include "LogManager.h"
#include "IOSUtilities.hpp"

WebClientIOS::WebClientIOS(const __secret& s) :
    WebClient(s),
    m_delegator(nil),
    m_nsUrlSession(nil)
{
    LogManager_Function();
    m_config = [NSURLSessionConfiguration defaultSessionConfiguration];
}

/* virtual */
WebClientIOS::~WebClientIOS()
{
    LogManager_Function();
}


/* virtual */
void WebClientIOS::initialize()
{
    LogManager_Function();
    m_delegator = [[ppHttpConnectionDelegator alloc]initWithParent: this];
        
    m_nsUrlSession = [NSURLSession sessionWithConfiguration:m_config
                                                   delegate:m_delegator
                                              delegateQueue:nil];
    m_thisHolder = holdThis();
}

/* virtual */
void WebClientIOS::finalize()
{
    LogManager_Function();
    if(m_nsUrlSession != nil){
        [m_nsUrlSession invalidateAndCancel];
    }
}

/* virtual */
auto WebClientIOS::postAsync
(
 const std::string& url,
 const_bytes_sp     postData,
 const_json_t       cookieData,
 const_json_t       extraHeaderData
 ) -> pprx::observable<response_t>
{
    LogManager_Function();
    auto request    = [NSMutableURLRequest requestWithURL:
                       [NSURL URLWithString:
                        [NSString stringWithUTF8String:url.c_str()]]];
    
    [request setHTTPMethod:@"POST"];
    
    if(!extraHeaderData.empty()){
        for (auto item : extraHeaderData.get<json_t::map_t>()){
            [request addValue:IOSUtilities::convert(item.second.get<std::string>())
           forHTTPHeaderField:IOSUtilities::convert(item.first.c_str())];
        }
    }
    
    [request setHTTPBody:[NSData dataWithBytes:postData->data() length:postData->size()]];
    setCookies(request, cookieData);

    return createTaskData(request);
}

/* virtual */
auto WebClientIOS::getAsync
(
 const std::string& url,
 const_json_t          cookieData
 ) -> pprx::observable<response_t>
{
    LogManager_Function();
    auto request    = [NSMutableURLRequest requestWithURL:
                       [NSURL URLWithString:
                        [NSString stringWithUTF8String:url.c_str()]]];
    setCookies(request, cookieData);
    
    return createTaskData(request);
}


/* virtual */
auto WebClientIOS::downloadFileWithGet
(
 const std::string&             url,
 const boost::filesystem::path  file
 ) -> pprx::observable<pprx::unit>
{
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        auto nsurl = [NSURL URLWithString: IOSUtilities::convert(url)];
        auto request = [NSMutableURLRequest requestWithURL:nsurl];
        [request setHTTPMethod:@"GET"];
        
        downloadCommon(request, file, s);
    });
}

/* virtual */
auto WebClientIOS::downloadFileWithPost
(
 const std::string&             url,
 const_bytes_sp                 postData,
 const boost::filesystem::path  file,
 const_json_t                   extraHeaderData
 ) -> pprx::observable<pprx::unit>
{
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        auto nsurl = [NSURL URLWithString: IOSUtilities::convert(url)];
        auto request = [NSMutableURLRequest requestWithURL:nsurl];
        [request setHTTPMethod:@"POST"];
        if(!extraHeaderData.empty()){
            for (auto item : extraHeaderData.get<json_t::map_t>()){
                [request addValue:IOSUtilities::convert(item.second.get<std::string>())
               forHTTPHeaderField:IOSUtilities::convert(item.first.c_str())];
            }
        }
        [request setHTTPBody:[NSData dataWithBytes:postData->data() length:postData->size()]];

        downloadCommon(request, file, s);
    });
}

void WebClientIOS::downloadCommon
(
 NSURLRequest* request,
 const boost::filesystem::path  file,
 pprx::subscriber<pprx::unit> s)
{
    auto session = m_nsUrlSession;
    
    auto task = [session
     downloadTaskWithRequest:request
     completionHandler:^(NSURL* location, NSURLResponse*  response, NSError* error) {
         if(error){
             json_t json;
             json.put("code", static_cast<int>(error.code));
             json.put("description", [[error description] UTF8String]);
             s.on_error(make_error_network(json));
             return;
         }
         
         if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
             NSInteger statusCode = [(NSHTTPURLResponse*)response statusCode];
             if (statusCode != 200) {
                 json_t json;
                 json.put("httpStatus", (boost::format("%d") % statusCode).str());
                 s.on_error(make_error_network(json));
                 return;
             }
         }
         auto fm = [NSFileManager defaultManager];
         /* copyItemAtPath の非同期関数ないの？ */
         [fm copyItemAtPath:[location path]
                     toPath:IOSUtilities::convert(file.string())
                      error:&error];
         s.on_next(pprx::unit());
         s.on_completed();
     }];
    [task resume];
}


/* virtual */
auto WebClientIOS::putFile
(
 const std::string&             url,
 const boost::filesystem::path  file
 ) -> pprx::observable<pprx::unit>
{
    return pprx::observable<>::create<pprx::unit>([=](pprx::subscriber<pprx::unit> s){
        auto nsurl = [NSURL URLWithString: IOSUtilities::convert(url)];
        auto fileurl = [NSURL URLWithString: IOSUtilities::convert(file.string())];
        
        auto request = [NSMutableURLRequest requestWithURL:nsurl];
        [request addValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"BlockBlob" forHTTPHeaderField:@"x-ms-blob-type"];
        [request setHTTPMethod:@"PUT"];

        auto session = [NSURLSession sessionWithConfiguration:m_config
                                                     delegate:nil
                                                delegateQueue:nil];
        
        auto task = [session
                     uploadTaskWithRequest:request
                     fromFile:fileurl
                     completionHandler:^(NSData* data, NSURLResponse*  response, NSError* error) {
                         if(error){
                             json_t json;
                             json.put("code", static_cast<int>(error.code));
                             json.put("description", [[error description] UTF8String]);
                             s.on_error(make_error_network(json));
                             return;
                         }
                         
                         if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                             NSInteger statusCode = [(NSHTTPURLResponse*)response statusCode];
                             if ((statusCode != 201) && (statusCode != 200)) {  /* （azure storage の場合）"200 OK" ではなく、PUTの場合 "201 Created" が正常終了*/
                                 json_t json;
                                 json.put("httpStatus", (boost::format("%d") % statusCode).str());
                                 s.on_error(make_error_network(json));
                                 return;
                             }
                         }
                         
                         s.on_next(pprx::unit());
                         s.on_completed();
                     }];
        [task resume];
    });
}

void WebClientIOS::setCookies(NSMutableURLRequest* request, const_json_t cookies)
{
    if(cookies.empty()) return;
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    for (auto item : cookies.get<json_t::map_t>()){
        auto prop = [[NSMutableDictionary alloc] init];
        [prop setValue:[NSString stringWithUTF8String:item.first.c_str()]
                forKey:NSHTTPCookieName];
        
        auto value = item.second.get_optional<std::string>("value");
        if(value){
            [prop setValue:[NSString stringWithUTF8String:value.get().c_str()]
                    forKey:NSHTTPCookieValue];
        }
        
        auto domain = item.second.get_optional<std::string>("domain");
        if(domain){
            [prop setValue:[NSString stringWithUTF8String:domain.get().c_str()]
                    forKey:NSHTTPCookieDomain];
        }

        auto path = item.second.get_optional<std::string>("path");
        if(path){
            [prop setValue:[NSString stringWithUTF8String:path.get().c_str()]
                    forKey:NSHTTPCookiePath];
        }

        auto secure = item.second.get_optional<std::string>("secure");
        if(secure){
            const BOOL b = (secure.get() == "true");
            if(b){
                [prop setObject:[NSNumber numberWithBool:b]
                        forKey:NSHTTPCookieSecure];
            }
        }
        
        auto formatter = createCookieDateFormatter();

        auto expires = item.second.get_optional<std::string>("expires");
        if(expires){
            [prop setValue:[formatter dateFromString:[NSString stringWithUTF8String:expires.get().c_str()]]
                    forKey:NSHTTPCookieExpires];
        }
        auto nsCookie = [NSHTTPCookie cookieWithProperties:prop];
        if(nsCookie != nil){
            [array addObject:nsCookie];
        }
    }

    auto headers = [NSHTTPCookie requestHeaderFieldsWithCookies:array];
    [request setAllHTTPHeaderFields:headers];

    //NSLog(@"NSHTTPCookie %@", array);
}

NSDateFormatter* WebClientIOS::createCookieDateFormatter() const
{
    auto formatter = [[NSDateFormatter alloc] init];
    auto enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:enUS];
    [formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss z"];
    
    return formatter;
}


auto WebClientIOS::createTaskData(NSMutableURLRequest* request)
 -> pprx::observable<response_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto taskData = boost::make_shared<taskDatas_t>();
        auto task = [m_nsUrlSession dataTaskWithRequest:request];
        m_taskDatas.modifyBlock([=](auto&& taskDatas){
            taskDatas[task] = taskData;
        });
        [task resume];
        return taskData->subject.get_observable();
    }).as_dynamic();
}

WebClientIOS::taskDatas_sp WebClientIOS::fetchTaskData(NSURLSessionTask* task)
{
    LogManager_Function();

    return m_taskDatas.referenceBlockWithResult<taskDatas_sp>([=](auto&& taskDatas){
        taskDatas_sp sd;
        auto it = taskDatas.find(task);
        if(it != taskDatas.end()){
            sd = it->second;
        }
        return sd;
    });
}



void WebClientIOS::deleteTaskData(NSURLSessionTask* task)
{
    LogManager_Function();
    m_taskDatas.modifyBlock([=](auto&& taskDatas){
        taskDatas.erase(task);
    });
}



#pragma mark - from HttpConnectionDelegator

void WebClientIOS::didReceiveChallenge(
                    NSURLSession*                   session,
                    NSURLAuthenticationChallenge*   challenge,
                    didReceiveChallenge_comp_t      completionHandler)
{
    LogManager_Function();
    auto    disposition     = NSURLSessionAuthChallengePerformDefaultHandling;
    NSURLCredential* credential = nil;
    completionHandler(disposition, credential);
}

void WebClientIOS::didBecomeInvalidWithError(
                NSURLSession*     session,
                NSError*          error)
{
    LogManager_Function();
    m_thisHolder.reset();
}


void WebClientIOS::didCompleteWithError(
                 NSURLSession*       session,
                 NSURLSessionTask*   task,
                 NSError*            error)
{
    LogManager_Function();
    {
        auto sd = fetchTaskData(task);
        
        if(sd){
            
            if(error){
                json_t json;
                json.put("code", static_cast<int>(error.code));
                json.put("description", [[error description] UTF8String]);
                sd->subject.get_subscriber().on_error(make_error_network(json));
            }
            else{
                sd->subject.get_subscriber().on_next(response_t{
                    sd->receiveHeaders,
                    sd->receiveCookies,
                    sd->receiveData
                });
                sd->subject.get_subscriber().on_completed();
            }
        }
    }
    deleteTaskData(task);
}

void WebClientIOS::willCacheResponse(
                  NSURLSession*                session,
                  NSURLSessionDataTask*        dataTask,
                  NSCachedURLResponse*         proposedResponse,
                  comp::willCacheResponse_t    completionHandler)
{
    LogManager_Function();
    completionHandler(proposedResponse);
}


void WebClientIOS::didReceiveResponse(
                   NSURLSession*           session,
                   NSURLSessionDataTask*   dataTask,
                   NSURLResponse*          response,
                   comp::didReceiveResponse_t completionHandler)
{
    LogManager_Function();
   auto sd = fetchTaskData(dataTask);
    
    if(sd){
        auto httpr = (NSHTTPURLResponse*)response;
        
        sd->statusCode = static_cast<uint16_t>(httpr.statusCode);
        for(id key in httpr.allHeaderFields){
            const std::string _key = [key UTF8String];
            const std::string _val = [[httpr.allHeaderFields objectForKey:key] UTF8String];
            if(boost::iequals(_key,"Set-Cookie")) continue;
            sd->receiveHeaders.put(_key, _val);
        }
        
        auto formatter = createCookieDateFormatter();
        
        auto cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:httpr.allHeaderFields
                                                              forURL:response.URL];
        for(NSHTTPCookie* cookie in cookies){
            if([cookie name] == nil) continue;
            
            json_t sub;
            const std::string name = [[cookie name] UTF8String];
            
            if([cookie value] != nil){
                sub.put("value", [[cookie value] UTF8String]);
            }
            
            if([cookie expiresDate] != nil){
                sub.put("expires", [[formatter stringFromDate:[cookie expiresDate]] UTF8String]);
            }
            
            if([cookie domain] != nil){
                sub.put("domain", [[cookie domain] UTF8String]);
            }
            
            if([cookie path] != nil){
                sub.put("path", [[cookie path] UTF8String]);
            }

            sub.put("secure", [cookie isSecure] ? "true" : "false");

            sd->receiveCookies.put(name, sub);
        }
    }
    
    completionHandler(NSURLSessionResponseAllow);
}

void WebClientIOS::didReceiveData(
                   NSURLSession*           session,
                   NSURLSessionDataTask*   dataTask,
                   NSData*                 data)
{
    LogManager_Function();
    auto sd = fetchTaskData(dataTask);
    
    if(sd){
        const auto oldsize = sd->receiveData->size();
        sd->receiveData->resize(oldsize + [data length]);
        ::memcpy(sd->receiveData->data() + oldsize, [data bytes], [data length]);
    }
}


#pragma mark - HttpConnectionDelegator

@interface ppHttpConnectionDelegator ()
{
    WebClientIOS* m_parent;
}
@property (assign, atomic) BOOL invalidated_;
@end

@implementation ppHttpConnectionDelegator

- (id)initWithParent:(WebClientIOS*)parent
{
    self = [super init];
    if(self != nil){
        m_parent = parent;
        self.invalidated_  = NO;
    }
    return self;
}

- (BOOL)isInvalidated
{
    return self.invalidated_;
}

#pragma mark - NSURLSessionDelegate

- (void) URLSession:(NSURLSession *)session
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
  completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    m_parent->didReceiveChallenge(session, challenge, completionHandler);
}

- (void) URLSession:(NSURLSession *)session
didBecomeInvalidWithError:(NSError *)error
{
    if(error != nil){
        LogManager_AddErrorF("error 0x%X", [error code]);
    }
    
    self.invalidated_ = YES;
    m_parent->didBecomeInvalidWithError(session, error);
}

#pragma mark - NSURLSessionTaskDelegate


- (void) URLSession:(NSURLSession *)session
               task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error != nil){
        LogManager_AddErrorF("error 0x%X", [error code]);
    }
    
    m_parent->didCompleteWithError(session, task, error);
}


#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    m_parent->didReceiveResponse(session, dataTask, response, completionHandler);
}


- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    m_parent->willCacheResponse(session, dataTask, proposedResponse, completionHandler);
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    m_parent->didReceiveData(session, dataTask, data);
}

@end


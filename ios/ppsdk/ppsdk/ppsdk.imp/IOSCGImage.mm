//
//  IOSCGImage.cpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "IOSCGImage.hpp"
#include "Image.h"

IOSCGImage IOSCGImage::resizeAndDecolorToGrayScale(size_t width /* = 0 */, size_t height /* = 0 */)
{
    if(width == 0){
        width = this->width();
    }
    if(height == 0){
        height = this->height();
    }
    auto bounds = CGRectMake(0, 0, width, height);
    CGImageRef gray;
    {
        auto colorspace = ::CGColorSpaceCreateDeviceGray();
        auto context = ::CGBitmapContextCreate(
                                               NULL,
                                               width,
                                               height,
                                               8,
                                               0,
                                               colorspace,
                                               kCGImageAlphaNone);
        ::CGColorSpaceRelease(colorspace);
        
        ::CGContextSetRGBFillColor(context, 1, 1, 1, 1);
        ::CGContextAddRect(context, bounds);
        ::CGContextFillPath(context);
        
        ::CGContextDrawImage(context, bounds, m_imageRef);
        gray = CGBitmapContextCreateImage(context);
        ::CGContextRelease(context);
    }
    IOSCGImage cgi;
    cgi.assign(gray);
    return cgi;
}



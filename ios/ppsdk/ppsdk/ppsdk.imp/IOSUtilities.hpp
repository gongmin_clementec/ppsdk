//
//  IOSUtilities.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__hpp_IOSUtilities__)
#define __hpp_IOSUtilities__

#import <Foundation/Foundation.h>
#include "BaseSystem.h"
#include <string>

class IOSUtilities
{
private:
    
protected:
    
    
public:
    
    static NSData*      convert(const bytes_t& src);
    static bytes_sp     convert(NSData* src);
    
    static NSString*    convert(const std::string& src);
    static std::string  convert(NSString* src);
    
    static NSDate*      convert(const boost::posix_time::ptime& src);
};




#endif /* !defined(__hpp_IOSUtilities__) */

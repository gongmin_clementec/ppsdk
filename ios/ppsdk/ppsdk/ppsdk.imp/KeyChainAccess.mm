//
//  KeyChainAccess.cpp
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "KeyChainAccess.hpp"
#include "IOSUtilities.hpp"
#include "LogManager.h"

static NSString* KEYCHAIN_SERVICE = @"com.clementec.pokepos";


/* static */
bool KeyChainAccess::installCredential(const std::string& label, const std::string& file, const std::string& password)
{
    LogManager_Function();

    auto data = [NSData dataWithContentsOfFile:IOSUtilities::convert(file)];
    if(data == nil){
        LogManager_AddError("file read error");
        return false;
    }
    
    auto certOptions =
    @{
      (__bridge id)kSecImportExportPassphrase: IOSUtilities::convert(password)
    };
    
    CFArrayRef importedItems = NULL;
    OSStatus err;
    
    err = SecPKCS12Import((__bridge CFDataRef)data, (__bridge CFDictionaryRef)certOptions, &importedItems);
    if(err != noErr){
        LogManager_AddErrorF("SecPKCS12Import: %d", err);
        return false;
    }

    for (NSDictionary* itemDict in (__bridge id)importedItems) {
        SecIdentityRef identity =  (__bridge SecIdentityRef)[itemDict objectForKey:(__bridge NSString *)kSecImportItemIdentity];
        auto keychainItem =
        @{
          (__bridge id)kSecAttrLabel:       IOSUtilities::convert(label),
          (__bridge id)kSecAttrAccessible:  (__bridge id)kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
          (__bridge id)kSecValueRef:        (__bridge id)identity
        };
        
        err = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
        if(err != noErr){
            LogManager_AddErrorF("SecItemAdd: %d", err);
            break;
        }
    }
    if (importedItems != NULL) {
        CFRelease(importedItems);
    }
    
    return err == noErr;
}

/* static */
bool KeyChainAccess::uninstallCredential(const std::string& label)
{
    LogManager_Function();
    auto query = @{
                   (__bridge id)kSecAttrLabel: IOSUtilities::convert(label),
                   (__bridge id)kSecClass: (__bridge id)kSecClassIdentity
                   };

    
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)query);
    if (status != noErr) {
        LogManager_AddErrorF("Remove Keychain Error Query: %d", status);
    }
    return status == noErr;
}

/* static */
NSArray* KeyChainAccess::getIdentities(const std::string& label)
{
    LogManager_Function();
    auto query = @{
                   (__bridge id)kSecAttrLabel: IOSUtilities::convert(label),
                   (__bridge id)kSecClass: (__bridge id)kSecClassIdentity,
                   (__bridge id)kSecMatchLimit: (__bridge id)kSecMatchLimitAll,
                   (__bridge id)kSecReturnRef: (__bridge id)kCFBooleanTrue
                   };
    
    CFArrayRef _identities = NULL;
    auto err = SecItemCopyMatching(
                                   (__bridge CFDictionaryRef)query,
                                   (CFTypeRef*)&_identities);
    
    if(err != noErr) return nil;
    return (__bridge_transfer NSArray *)_identities;
}

/* static */
NSURLCredential* KeyChainAccess::getCredential(const std::string& label)
{
    LogManager_Function();
    auto identities = getIdentities(label);
    if((identities == nil) || ([identities count] == 0)) return nil;
    
    auto identity = (__bridge SecIdentityRef)[identities objectAtIndex:0];
    if(identity == NULL) return nil;
    
    return [NSURLCredential credentialWithIdentity:identity
                                      certificates:nil
                                       persistence:NSURLCredentialPersistenceNone];
}

/* static */
bool KeyChainAccess::isCredentialInstalled(const std::string& label)
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrLabel:IOSUtilities::convert(label),
                                    (__bridge id)kSecClass:(__bridge id)kSecClassIdentity,
                                    (__bridge id)kSecMatchLimit:(__bridge id)kSecMatchLimitAll,
                                    (__bridge id)kSecReturnRef:(__bridge id)kCFBooleanFalse
                                    } mutableCopy];
    CFDictionaryRef cfQuery = (__bridge CFDictionaryRef)query;
    CFTypeRef *ref = nil;
    
    return SecItemCopyMatching(cfQuery, ref) == noErr;
}



/* static */
boost::optional<std::string> KeyChainAccess::getString(const std::string& label, const std::string& generic, const std::string& account)
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrGeneric:IOSUtilities::convert(generic),
                                    (__bridge id)kSecAttrAccount:IOSUtilities::convert(account),
                                    (__bridge id)kSecAttrService:KEYCHAIN_SERVICE,
                                    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecMatchLimit:(__bridge id)kSecMatchLimitOne,
                                    (__bridge id)kSecReturnData:(__bridge id)kCFBooleanTrue
                                    } mutableCopy];
    
    boost::optional<std::string> rs;
    CFDataRef   dataRef;
    OSStatus    result = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&dataRef);
    if (result != noErr) {
        return rs;
    }
    NSData  *data = (__bridge_transfer NSData *)dataRef;
    NSString *str = [[NSString alloc] initWithBytes:[data bytes]
                                             length:[data length]
                                           encoding:NSUTF8StringEncoding];
    
    rs = IOSUtilities::convert(str);
    return rs;
}

/* static */
bool KeyChainAccess::deleteString(const std::string& generic, const std::string& account)
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrGeneric:IOSUtilities::convert(generic),
                                    (__bridge id)kSecAttrAccount:IOSUtilities::convert(account),
                                    (__bridge id)kSecAttrService:KEYCHAIN_SERVICE,
                                    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
                                    } mutableCopy];
    
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)query);
    if (status != noErr) {
        LogManager_AddErrorF("Remove Keychain Error Query: %d", status);
    }
    return status == noErr;
}

/* static */
bool KeyChainAccess::createString(const std::string& value, const std::string& label, const std::string& generic, const std::string& account)
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrGeneric:IOSUtilities::convert(generic),
                                    (__bridge id)kSecAttrAccount:IOSUtilities::convert(account),
                                    (__bridge id)kSecAttrService:KEYCHAIN_SERVICE,
                                    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecAttrLabel:IOSUtilities::convert(label),
                                    (__bridge id)kSecAttrDescription:IOSUtilities::convert(label),
                                    (__bridge id)kSecValueData:[IOSUtilities::convert(value) dataUsingEncoding:NSUTF8StringEncoding]
                                    } mutableCopy];
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)query, NULL);
    if(status != noErr) {
        LogManager_AddErrorF("error (OSStatus = %d) -> label: \"%s\", generic: \"%s\", account: \"%s\"", status % label % generic % account);
    }
    else{
        LogManager_AddInformationF("success -> label: \"%s\", generic: \"%s\", account: \"%s\"", label % generic % account);
    }
    return status == noErr;
}

/* static */
bool KeyChainAccess::setString(const std::string& value, const std::string& label, const std::string& generic, const std::string& account)
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrGeneric:IOSUtilities::convert(generic),
                                    (__bridge id)kSecAttrAccount:IOSUtilities::convert(account),
                                    (__bridge id)kSecAttrService:KEYCHAIN_SERVICE,
                                    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecMatchLimit:(__bridge id)kSecMatchLimitOne,
                                    (__bridge id)kSecReturnData:(__bridge id)kCFBooleanTrue
                                    } mutableCopy];
    
    CFDictionaryRef attributesDictRef = nil;
    OSStatus status = SecItemCopyMatching ((__bridge CFDictionaryRef)query, (CFTypeRef *)&attributesDictRef);
    if(status == noErr) {
        deleteString(generic, account);
    }
    else if ( status != errSecItemNotFound) {
        LogManager_AddErrorF("error (OSStatus = %d) -> label: \"%s\" str: \"%s\"", status % label % value);
        return false;
    }
    
    return createString(value, label, generic, account);
}

/* static */
boost::optional<std::string> KeyChainAccess::getTmsPassword(const std::string& server)
{
    LogManager_Function();
    return getString("TMS", "TMS", server);
}

/* static */
bool KeyChainAccess::setTmsPassword(const std::string& server, const std::string& newValue)
{
    LogManager_Function();
    return setString(newValue, "TMS", "TMS", server);
}


/* static */
void KeyChainAccess::deleteAllTmsPassword()
{
    LogManager_Function();
    NSMutableDictionary *query = [@{
                                    (__bridge id)kSecAttrGeneric:@"TMS",
                                    (__bridge id)kSecAttrService:KEYCHAIN_SERVICE,
                                    (__bridge id)kSecClass:(__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecMatchLimit:(__bridge id)kSecMatchLimitAll,
                                    (__bridge id)kSecReturnAttributes:(__bridge id)kCFBooleanTrue
                                    } mutableCopy];
    
    CFArrayRef  cfitems = nil;
    OSStatus    result = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&cfitems);
    if (result == noErr) {
        NSArray* items = (__bridge NSArray*)cfitems;
        for(NSDictionary* dic in items){
            deleteString("TMS", IOSUtilities::convert((NSString*)[dic objectForKey:(__bridge id)kSecAttrAccount]));
        }
    }
}

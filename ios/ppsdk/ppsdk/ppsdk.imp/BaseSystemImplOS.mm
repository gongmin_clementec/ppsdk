//
//  BaseSystemIOS.cpp
//  ppsdk
//
//  Created by clementec on 2017/06/01.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "BaseSystemImpIOS.hpp"
#include "IOSUtilities.hpp"
#include "KeyChainAccess.hpp"
#include "PPConfig.h"
#import <sys/utsname.h>
#import <UIKit/UIKit.h>
#include "PPReactive.h"
#include "SDEAAccessory.hpp"
#include "SoundManagerIOS.hpp"

BaseSystemImpIOS::BaseSystemImpIOS()
    : m_applicationState(BaseSystem::ApplicationState::Initializing)
{
    [[NSNotificationCenter defaultCenter] addObserverForName:@"applicationWillResignActive"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification*) {
                                                      LogManager_AddInformation("applicationWillResignActive");
                                                      setApplicationState(BaseSystem::ApplicationState::WillBackground);
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"applicationDidEnterBackground"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification*) {
                                                      LogManager_AddInformation("applicationDidEnterBackground");
                                                      setApplicationState(BaseSystem::ApplicationState::Background);
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"applicationDidBecomeActive"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification*) {
                                                      LogManager_AddInformation("applicationDidBecomeActive");
                                                      setApplicationState(BaseSystem::ApplicationState::Foreground);
                                                  }];
}

/* virtual */
void BaseSystemImpIOS::callFunctionWithCatchException(boost::function<void()> func)
{
    @autoreleasepool {
        @try{
            try{
                func();
            }
            catch(std::exception& ex){
                LogManager_AddErrorF("catch an exception : at BaseSystem::post() -> %s", ex.what());
            }
            catch(...){
                LogManager_AddError("catch an exception : at BaseSystem::post() -> (unknown)");
            }
        }
        @catch(id exception){
            LogManager_AddErrorF("catch a NSException : at BaseSystem::post() -> %s", [[exception string] UTF8String]);
        }
    }
}

/* virtual */
bool BaseSystemImpIOS::isMainThread()
{
    return [NSThread isMainThread];
}

/* virtual */
void BaseSystemImpIOS::invokeMainThread(boost::function<void()> func)
{
    if(isMainThread()){
        callFunctionWithCatchException(func);
    }
    else{
        dispatch_sync(dispatch_get_main_queue(), ^{
            callFunctionWithCatchException(func);
        });
    }
}

/* virtual */
void BaseSystemImpIOS::invokeMainThreadAsync(boost::function<void()> func)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        callFunctionWithCatchException(func);
    });
}

/* virtual */
std::string BaseSystemImpIOS::sjisToUtf8(const std::string& src)
{
    auto u16 = [NSString stringWithCString:src.c_str() encoding:NSShiftJISStringEncoding];
    return [u16 UTF8String];
}


/* Framework内のBundleにアクセスするために[NSBundle bundleForClass:]を使用する方法を採用する。 */
/* そのため、ダミーのクラスをここで定義しておく。 */
@interface ppDummyClassForSearchingBundle : NSObject
@end

@implementation ppDummyClassForSearchingBundle
@end

/* virtual */
boost::filesystem::path BaseSystemImpIOS::getResourceDirectoryPath()
{
    NSBundle *bundle = [NSBundle bundleForClass:[ppDummyClassForSearchingBundle class]];
    return boost::filesystem::path([bundle.resourcePath UTF8String]);
}

/* virtual */
boost::filesystem::path BaseSystemImpIOS::getDocumentDirectoryPath()
{
    NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*   docDir = [paths objectAtIndex:0];
    return boost::filesystem::path([docDir UTF8String]);
}

/* virtual */
boost::shared_ptr<SerialDevice> BaseSystemImpIOS::createSerialDevice()
{
    return boost::shared_ptr<SerialDevice>(new SDEAAccessory());
}

/* virtual */
const_json_t BaseSystemImpIOS::getSystemInformation()
{
    json_t j;
    {
        auto label = PPConfig::instance().get<std::string>("hps.keychainStoreLabel");
        auto identities = KeyChainAccess::getIdentities(label);
        if((identities != nil) && ([identities count] > 0)){
            auto identity = (__bridge SecIdentityRef)[identities objectAtIndex:0];
            SecCertificateRef cert;
            SecIdentityCopyCertificate(identity, &cert);
            auto certSummary = (__bridge_transfer NSString*)SecCertificateCopySubjectSummary(cert);
            j.put("UserName", IOSUtilities::convert(certSummary));
        }
        else{
            j.put("UserName", "");
        }
    }
    {
        struct utsname si;
        uname(&si);
        j.put("PlatformDeviceName", si.machine);
    }
    
    j.put("PlatformOSName", IOSUtilities::convert([UIDevice currentDevice].systemName));
    j.put("PlatformOSVersion", IOSUtilities::convert([UIDevice currentDevice].systemVersion));
    j.put("AppName", IOSUtilities::convert((NSString*)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]));
    j.put("AppVersion", IOSUtilities::convert((NSString*)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]));
    return j;
}

/* virtual */
std::string BaseSystemImpIOS::getDeviceUuid()
{
    auto s = IOSUtilities::convert([[UIDevice currentDevice].identifierForVendor UUIDString]);
    boost::algorithm::replace_all(s, "-", "");
    return s;
}

/* virtual */
boost::optional<std::string> BaseSystemImpIOS::getSecureElementText(const std::string& name)
{
    boost::optional<std::string> result;
    if(name == "TmsPassword"){
        auto&& server = PPConfig::instance().get<std::string>("tms.url");
        result = KeyChainAccess::getTmsPassword(server);
    }
    return result;
}

/* virtual */
bool BaseSystemImpIOS::setSecureElementText(const std::string& name, const std::string& value)
{
    bool bSuccess = false;
    if(name == "TmsPassword"){
        auto&& server = PPConfig::instance().get<std::string>("tms.url");
        bSuccess = KeyChainAccess::setTmsPassword(server, value);
    }
    return bSuccess;
}


/* virtual */
boost::optional<std::string> BaseSystemImpIOS::getSharedAreaText(const std::string& name)
{
    auto appGroupId = IOSUtilities::convert(PPConfig::instance().get<std::string>("ios.appGroupId"));
    
    auto fm = [NSFileManager defaultManager];
    auto baseUrl = [fm containerURLForSecurityApplicationGroupIdentifier: appGroupId];
    
    auto url = [baseUrl URLByAppendingPathComponent: IOSUtilities::convert(name)];
    
    NSError* err;
    auto nsstr = [NSString stringWithContentsOfURL:url
                                           encoding:NSUTF8StringEncoding
                                              error:&err];
    
    boost::optional<std::string> s;
    if(err){
        LogManager_AddError([[err description] UTF8String]);
    }
    else{
        s = IOSUtilities::convert(nsstr);
    }
    
    return s;
}

/* virtual */
bool BaseSystemImpIOS::setSharedAreaText(const std::string& name, const std::string& value)
{
    auto appGroupId = IOSUtilities::convert(PPConfig::instance().get<std::string>("ios.appGroupId"));
    
    auto fm = [NSFileManager defaultManager];
    auto baseUrl = [fm containerURLForSecurityApplicationGroupIdentifier: appGroupId];
    
    auto url = [baseUrl URLByAppendingPathComponent: IOSUtilities::convert(name)];
    
    NSError* err;
    auto nsstr = IOSUtilities::convert(value);
    [nsstr writeToURL:url
           atomically:YES
             encoding:NSUTF8StringEncoding
                error:&err];
    
    
    if(err){
        LogManager_AddError([[err description] UTF8String]);
    }
    
    return err == nil;
}


/* virtual */
bool BaseSystemImpIOS::certificateIsInstalled()
{
    auto&& label = PPConfig::instance().get<std::string>("hps.keychainStoreLabel");
    return KeyChainAccess::isCredentialInstalled(label);
}

/* virtual */
bool BaseSystemImpIOS::certificateInstall(const boost::filesystem::path& path, const std::string& password)
{
    auto&& label = PPConfig::instance().get<std::string>("hps.keychainStoreLabel");
    return KeyChainAccess::installCredential(label, path.string(), password);
}

/* virtual */
bool BaseSystemImpIOS::certificateUninstall()
{
    auto&& label = PPConfig::instance().get<std::string>("hps.keychainStoreLabel");
    return KeyChainAccess::uninstallCredential(label);
}

/* virtual */
auto BaseSystemImpIOS::canSendTextToAnotherApplication(const std::string& text)
-> rxcpp::observable<bool>
{
    LogManager_Function();
    
    return pprx::just(text)
    .observe_on(pprx::observe_on_main_thread())
    .flat_map([=](std::string urltext){
        return pprx::observable<>::create<bool>([=](pprx::subscriber<bool> s){
            auto url = [NSURL URLWithString:IOSUtilities::convert(urltext)];
            bool canOpenURL = ([[UIApplication sharedApplication] canOpenURL:url] == YES);
            LogManager_AddInformationF("canOpenURL = %s", (canOpenURL ? "true" : "false"));
            s.on_next(canOpenURL);
             s.on_completed();
        });
    }).as_dynamic();
}

/* virtual */
auto BaseSystemImpIOS::sendTextToAnotherApplication(const std::string& text)
 -> rxcpp::observable<bool>
{
    LogManager_Function();

    return pprx::just(text)
    .observe_on(pprx::observe_on_main_thread())
    .flat_map([=](std::string urltext){
        return pprx::observable<>::create<bool>([=](pprx::subscriber<bool> s){
            auto url = [NSURL URLWithString:IOSUtilities::convert(urltext)];
            [[UIApplication sharedApplication]
             openURL: url
             options: @{}
             completionHandler: ^(BOOL success) {
                 if(success){
                     LogManager_AddInformationF("execute openURL: success -> %s", urltext);
                     s.on_next(true);
                 }
                 else{
                     LogManager_AddErrorF("execute openURL: failure -> %s", urltext);
                     s.on_next(false);
                 }
                 s.on_completed();
             }];
        });
    }).as_dynamic();
}


/* virtual */
auto BaseSystemImpIOS::applicationState()
 -> rxcpp::observable<BaseSystem::ApplicationState>
{
    return m_applicationState.get_observable();
}

void BaseSystemImpIOS::setApplicationState(BaseSystem::ApplicationState state)
{
    BaseSystem::instance().post([=](){
        auto stateStr = [state](){
            switch(state){
                case BaseSystem::ApplicationState::WillBackground: return "ApplicationState::WillBackground";
                case BaseSystem::ApplicationState::Background: return "ApplicationState::Background";
                case BaseSystem::ApplicationState::Foreground: return "ApplicationState::Foreground";
                default: break;
            }
            return "(unknown)";
        }();
        LogManager_AddInformationF("setApplicationState(%s) -> emit", stateStr);
        m_applicationState.get_subscriber().on_next(state);
    });
}

/* virtual */
BaseSystem::OSType BaseSystemImpIOS::getOSType()
{
    return BaseSystem::OSType::iOS;
}

/* virtual*/
auto BaseSystemImpIOS::soundPlay(const std::string& soundFile) -> void
{
    SoundManager::instance().play(soundFile);
}

#if defined(__x86_64__) || defined(__i386__)

#include <dirent.h>

/*
 シミュレータビルドで boost で下記のリンクエラーを回避するための関数定義
 Undefined symbols for architecture i386:
 "_nanosleep$UNIX2003", referenced from:
 boost::this_thread::no_interruption_point::hiden::sleep_for(timespec const&) in libboost_thread.a(thread.o)
 boost::this_thread::no_interruption_point::hiden::sleep_until(timespec const&) in libboost_thread.a(thread.o)
 boost::this_thread::hiden::sleep_for(timespec const&) in libboost_thread.a(thread.o)
 */

extern "C" {
    int nanosleep$UNIX2003(useconds_t secs)
    {
        return usleep(secs);
    }
    
    DIR* opendir$INODE64(char * dirName)
    {
        return opendir(dirName);
    }
    
    DIR* opendir$INODE64$UNIX2003(char * dirName)
    {
        return opendir(dirName);
    }
    
    int closedir$INODE64(DIR *dirp)
    {
        return closedir(dirp);
    }
    
    int closedir$UNIX2003(DIR *dirp)
    {
        return closedir(dirp);
    }
    
    struct dirent* readdir$INODE64(DIR * dir)
    {
        return readdir(dir);
    }
    
    int readdir_r$INODE64(DIR *dirp, struct dirent *entry, struct dirent **result)
    {
        return readdir_r(dirp, entry, result);
    }
}

#endif


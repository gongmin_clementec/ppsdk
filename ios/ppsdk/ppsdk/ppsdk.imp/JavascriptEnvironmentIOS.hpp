//
//  JavascriptEnvironmentIOS.hpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//


#if !defined(__hpp_JavascriptEnvironmentIOS__)
#define __hpp_JavascriptEnvironmentIOS__

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include "JavascriptEnvironment.h"


@interface ppWebViewDelegator: NSObject<UIWebViewDelegate>
@end


class JavascriptEnvironmentIOS :
    public JavascriptEnvironment
{
public:
    
    
private:
    UIWebView*          m_webView;
    ppWebViewDelegator*   m_delegator;
    
    pprx::subject<pprx::unit> m_subjectWebViewDidFinishLoad;
    pprx::subject<pprx::unit> m_subjectDidFailLoadWithError;
    
    
protected:
    
public:
    JavascriptEnvironmentIOS(const __secret& s);
    virtual ~JavascriptEnvironmentIOS();
    
    
    BOOL shouldStartLoadWithRequest(NSURLRequest* request, UIWebViewNavigationType type);
    void webViewDidStartLoad();
    void webViewDidFinishLoad();
    void didFailLoadWithError(NSError* error);

    virtual auto load
    (
     const std::string& htmlText,
     const std::string& documentBasePath
     ) -> pprx::observable<pprx::unit>;
    
    virtual auto call(const std::string& jsText) -> pprx::observable<std::string>;
};


#endif /* !defined(__hpp_JavascriptEnvironmentIOS__) */

//
//  SlipPrinterStar.cpp
//  ppsdk
//
//  Created by tel on 2017/06/05.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "SlipPrinterStar.hpp"
#include "LogManager.h"
#import "IOSUtilities.hpp"


SlipPrinterStar::SlipPrinterStar(const __secret& s) :
    SlipPrinterESCP(s),
    m_buffer()
{
    LogManager_Function();
}

/* virtual */
SlipPrinterStar::~SlipPrinterStar()
{
    LogManager_Function();
}

/* virtual */
auto SlipPrinterStar::openDocumentSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        @try{
            if(!m_printerPort.open(@"BT:", @"Portable;escpos", 10000)){
                json_t j;
                j.put("reason", "-portOpenError");
                throw_error_printer(j);
            }
            StarPrinterStatus_2 stat;
            m_printerPort.getParsedStatus(&stat, 2);
        }
        @catch(PortException* ex) {
            throw_error_printer(portExceptionToErrorJson(ex));
        }
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto SlipPrinterStar::openPageSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        @try{
            m_buffer.clear();
        }
        @catch(PortException* ex) {
            throw_error_printer(portExceptionToErrorJson(ex));
        }
        return pprx::just(pprx::unit());
    });
}

/* virtual */
auto SlipPrinterStar::closePageSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        @try{
            m_buffer.push_back('\n');
            m_buffer.push_back('\n');
            m_buffer.push_back('\n');

            StarPrinterStatus_2 stat;
            /* 印字終了の監視開始 */
            m_printerPort.raw().endCheckedBlockTimeoutMillis = 30 * 1000; /* 印字完了までの待ち時間（30秒） */

            m_printerPort.beginCheckedBlock(&stat, 2);
            if(isError(stat)){
                throw_error_printer(statusToErrorJson(stat));
            }

            u_int32_t offset = 0;
            const auto buflen = static_cast<u_int32_t>(m_buffer.size());
            while(offset < buflen){
                LogManager_AddDebugF("write %d / %d", offset % m_buffer.size());
                const auto written = m_printerPort.writePort(
                                                             m_buffer.data(),
                                                             offset,
                                                             buflen - offset);
                LogManager_AddDebugF("wittern = %d", written);
                if(written == 0){
                    LogManager_AddError("writePort -> written == 0");
                    json_t j;
                    j.put("reason", "-writeError");
                    throw_error_printer(j);
                }
                offset += written;
            }
            m_buffer.clear();
            
            /* 印字終了の監視 */
            NSError* err;
            m_printerPort.endCheckedBlock(&stat, 2, &err);
            if(isError(stat)){
                throw_error_printer(statusToErrorJson(stat));
            }
            if(err){
                json_t j;
                j.put("reason", "-pageClose");
                throw_error_printer(j);
            }
        }
        @catch(PortException* ex) {
            throw_error_printer(portExceptionToErrorJson(ex));
        }

        return pprx::just(pprx::unit());
    }).as_dynamic();
}

bool SlipPrinterStar::isError(const StarPrinterStatus_2& stat) const
{
    bool bError = false;

    /* ユーザーサポートを考慮して、複合エラーをログに記録 */
    
    std::stringstream ss;
    if(stat.coverOpen               == SM_TRUE){ ss << " coverOpen"; bError = true; }
    if(stat.offline                 == SM_TRUE){ ss << " offline"; bError = true; }
    if(stat.compulsionSwitch        == SM_TRUE){ ss << " compulsionSwitch"; bError = true; }
    
    /* LEVEL2 */
    if(stat.overTemp                == SM_TRUE){ ss << " overTemp"; bError = true; }
    if(stat.unrecoverableError      == SM_TRUE){ ss << " unrecoverableError"; bError = true; }
    if(stat.cutterError             == SM_TRUE){ ss << " cutterError"; bError = true; }
    if(stat.mechError               == SM_TRUE){ ss << " mechError"; bError = true; }
    if(stat.headThermistorError     == SM_TRUE){ ss << " headThermistorError"; bError = true; }
    
    /* LEVEL3 */
    if(stat.receiveBufferOverflow   == SM_TRUE){ ss << " receiveBufferOverflow"; bError = true; }
    if(stat.pageModeCmdError        == SM_TRUE){ ss << " pageModeCmdError"; bError = true; }
    if(stat.blackMarkError          == SM_TRUE){ ss << " blackMarkError"; bError = true; }
    if(stat.presenterPaperJamError  == SM_TRUE){ ss << " presenterPaperJamError"; bError = true; }
    if(stat.headUpError             == SM_TRUE){ ss << " headUpError"; bError = true; }
    if(stat.voltageError            == SM_TRUE){ ss << " voltageError"; bError = true; }

    /* LEVEL4 */
    if(stat.receiptBlackMarkDetection == SM_TRUE){ ss << " receiptBlackMarkDetection"; /* bError = true; */ }
    if(stat.receiptPaperEmpty         == SM_TRUE){ ss << " receiptPaperEmpty"; bError = true; }
    if(stat.receiptPaperNearEmptyInner== SM_TRUE){ ss << " receiptPaperNearEmptyInner"; /* bError = true; */ }
    if(stat.receiptPaperNearEmptyOuter== SM_TRUE){ ss << " receiptPaperNearEmptyOuter"; /* bError = true; */ }

    auto s = ss.str();
    if(!s.empty()){
        if(bError){
            LogManager_AddErrorF("printer error : %s", s);
        }
        else{
            LogManager_AddErrorF("printer warning : %s", s);
        }
    }
    return bError;
}

json_t SlipPrinterStar::statusToErrorJson(const StarPrinterStatus_2& stat) const
{
    std::string msg;
    
    /* 複合的に発生したエラーについては、どれを採用するかは下記比較順で決まる */
    
    /* LEVEL1 */
         if(stat.coverOpen              == SM_TRUE) msg = "coverOpen";
    else if(stat.presenterPaperJamError == SM_TRUE) msg = "presenterPaperJamError";     /* LEVEL3 */
    else if(stat.receiptPaperEmpty      == SM_TRUE) msg = "receiptPaperEmpty";          /* LEVEL4 */
    //else if(stat.offline              == SM_TRUE) msg = "offline";  /* 複合エラーで発生することが多いので一番最後に移動 */
    else if(stat.compulsionSwitch       == SM_TRUE) msg = "compulsionSwitch";
    
    /* LEVEL2 */
    else if(stat.overTemp           == SM_TRUE) msg = "overTemp";
    else if(stat.unrecoverableError == SM_TRUE) msg = "unrecoverableError";
    else if(stat.cutterError        == SM_TRUE) msg = "cutterError";
    else if(stat.mechError          == SM_TRUE) msg = "mechError";
    else if(stat.headThermistorError== SM_TRUE) msg = "headThermistorError";

    /* LEVEL3 */
    else if(stat.receiveBufferOverflow   == SM_TRUE) msg = "receiveBufferOverflow";
    else if(stat.pageModeCmdError        == SM_TRUE) msg = "pageModeCmdError";
    else if(stat.blackMarkError          == SM_TRUE) msg = "blackMarkError";
    //else if(stat.presenterPaperJamError  == SM_TRUE) msg = "presenterPaperJamError";  /* 上に移動 */
    else if(stat.headUpError             == SM_TRUE) msg = "headUpError";
    else if(stat.voltageError            == SM_TRUE) msg = "voltageError";

    /* LEVEL4 */
    //else if(stat.receiptBlackMarkDetection  == SM_TRUE) msg = "receiptBlackMarkDetection";    /* 警告なので無視 */
    //else if(stat.receiptPaperEmpty          == SM_TRUE) msg = "receiptPaperEmpty";            /* 上に移動 */
    //else if(stat.receiptPaperNearEmptyInner == SM_TRUE) msg = "receiptPaperNearEmptyInner";   /* 警告なので無視 */
    //else if(stat.receiptPaperNearEmptyOuter == SM_TRUE) msg = "receiptPaperNearEmptyOuter";   /* 警告なので無視 */
    
    else if(stat.offline            == SM_TRUE) msg = "offline";        /* LEVEL1 から最後に移動 */
    
    json_t j;
    j.put("reason", msg);
    return j;
}

json_t SlipPrinterStar::portExceptionToErrorJson(PortException* exception) const
{
    std::string msg = IOSUtilities::convert([exception reason]);
    json_t j;
    j.put("reason", "-portExcption");
    j.put("detail", msg);
    return j;
}

/* virtual */
auto SlipPrinterStar::closeDocumentSelf()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        @try{
            m_printerPort.close();
        }
        @catch(PortException* ex) {
            throw_error_printer(portExceptionToErrorJson(ex));
        }
        return pprx::just(pprx::unit());
    }).as_dynamic();
}


/* virtual */
void SlipPrinterStar::writeString(const std::string& str)
{
//    LogManager_Function();
    auto ustr = [NSString stringWithUTF8String:str.c_str()];
    auto sjdata = [ustr dataUsingEncoding:NSShiftJISStringEncoding
                     allowLossyConversion:YES];
    auto sjbytes = IOSUtilities::convert(sjdata);
    m_buffer.insert(m_buffer.end(), sjbytes->cbegin(), sjbytes->cend());
}

/* virtual */
void SlipPrinterStar::writeBytes(const bytes_t& bytes)
{
//    LogManager_Function();
    m_buffer.insert(m_buffer.end(), bytes.cbegin(), bytes.cend());
}

/* virtual */
bool SlipPrinterStar::hasAutoCutter() const
{
    return false;
}

/* virtual */
int SlipPrinterStar::getRowChars() const
{
    return 32;
}

/* virtual */
int SlipPrinterStar::getRowPixels() const
{
    return 32 * 12;
}

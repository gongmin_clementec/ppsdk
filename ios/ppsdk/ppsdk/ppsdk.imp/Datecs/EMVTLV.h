#import <Foundation/Foundation.h>

@interface ppTLV : NSObject

@property (assign) int tag;
@property (copy) NSData *data;
@property (readonly) const unsigned char *bytes;
@property (readonly) NSData *encodedData;

+(NSArray *)decodeTagList:(NSData *)data;
+(NSData *)encodeTagList:(NSArray *)data;


+(uint)tagFromHexString:(NSString *)string;

+(ppTLV *)tlvWithString:(NSString *)data tag:(uint)tag;
+(ppTLV *)tlvWithHexString:(NSString *)data tag:(uint)tag;
+(ppTLV *)tlvWithData:(NSData *)data tag:(uint)tag;
+(ppTLV *)tlvWithInt:(UInt64)data nBytes:(int)nBytes tag:(uint)tag;
+(ppTLV *)tlvWithBCD:(UInt64)data nBytes:(int)nBytes tag:(uint)tag;

+(ppTLV *)findLastTag:(int)tag tags:(NSArray *)tags;
+(NSArray *)findTag:(int)tag tags:(NSArray *)tags;
+(NSArray *)decodeTags:(NSData *)data;
+(NSData *)encodeTags:(NSArray *)tags;

@end

//---------------------------------------------------------------------------
// 日本語のNSStringを10x10フォントを使用したUIImageに変換する
// Release date: 2016/01/22
// Copyright:    Sanei Electric Inc. All right reserved.
//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "libJPFont.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#ifndef _LIBJPFONT_H_
#define _LIBJPFONT_H_


// LCDサイズ
#define BP50LCD_SIZE_W			128
#define BP50LCD_SIZE_H			32

// フォントサイズ
#define FONT_SIZE_W5			5
#define FONT_SIZE_W10			10
#define FONT_SIZE_H10			10
#define FONT_SIZE_W12			12
#define FONT_SIZE_H12			12
#define FONT_SIZE_W16			16
#define FONT_SIZE_H16			16
#define FONT_SIZE_W32			32
#define FONT_SIZE_H32			32

// 文字コードのエンコード方式の選択
#define ENCFLAG_JIS				1	// JISにエンコード
#define ENCFLAG_SJIS			0	// ShiftJISにエンコード

// マスク
#define MASK_ZENKAKU16			0x8000
#define MASK_ZENKAKU8			0x80
#define IS_ZENKAKU16			0x8000
#define IS_ZENKAKU8				0x80
#define IS_HANKAKU16			0x0000
#define IS_HANKAKU8				0x00

// 制御コードの値
#define CONTROL_CODE_MAX		0x20	// 半角スペースの位置
#define CONTROL_CODE_CR			0x0A	// 改行コード


@interface libJPFont : NSObject

@property (nonatomic, assign) NSInteger number_line;
@property (nonatomic, strong) NSString *print_content;


+(UIImage *)NSStringToUIImage:(NSString *)string imageSize:(CGSize)imageSize startPoint:(CGPoint)startPoint;

+(uint8_t *)NSstringTo2byteCode:(NSString *)strText JISorSJIS:(BOOL)flag;
+(unsigned int)length2byteCode:(NSString *)strText JISorSJIS:(BOOL)flag;

@end

#endif

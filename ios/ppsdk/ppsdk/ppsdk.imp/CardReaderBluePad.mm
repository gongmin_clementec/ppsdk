//
//  CardReaderBluePad.cpp
//  ppsdk
//
//  Created by tel on 2017/06/06.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#include "CardReaderBluePad.hpp"
#import "DTDevices.h"
#include "IOSUtilities.hpp"
#import "EMVTLV.h"
#import "EMVTags.h"
#import "EMVPrivateTags.h"
#include "NFCTagConverterBluePad.h"
#include "PPConfig.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include "APDU.h"

template <typename T> class version
{
private:
    std::vector<T> m_vers;
protected:
public:
    version(const std::string& s)
    {
        std::vector<std::string> sarr;
        boost::split(sarr, s, boost::is_any_of("."));
        for(auto n : sarr){
            m_vers.push_back(boost::lexical_cast<T>(n));
        }
    }
    
    bool operator < (const version<T>& aver)
    {
        const auto length = std::min(m_vers.size(), aver.m_vers.size());
        for(auto i = 0; i < length; i++){
            if(m_vers[i] == aver.m_vers[i]) continue;
            else if(m_vers[i] < aver.m_vers[i]) return true;
            else return false;
        }
        return false;
    }
    
    std::string toString() const
    {
        std::stringstream ss;
        for(auto i = 0; i < m_vers.size(); i++){
            if(i != 0) ss << ".";
            ss << m_vers[i];
        }
        return ss.str();
    }
};


CardReaderBluePad::CardReaderBluePad(const __secret& s) :
    CardReader(s),
    m_dtd(this),
    m_currentSlot(),
    m_abortedForTrainingMode(false),
    m_bEnableErrorOnSmatCardRemoved(false),
    m_bInSeePhone(false)
{
    LogManager_Function();
}

/* virtual */
CardReaderBluePad::~CardReaderBluePad()
{
    LogManager_Function();
}

void CardReaderBluePad::broadcastError(std::exception_ptr err)
{
    LogManager_Function();
    m_subjectContactlessTransactionFinished.get_subscriber().on_error(err);
    m_subjectPINEntryCompleteWithError.get_subscriber().on_error(err);
    subscriberICCardInserted().on_error(err);
}

bool CardReaderBluePad::isValidAppVersion()
{
    LogManager_Function();
    auto dinfo = m_dtd.getConnectedDeviceInfo();
    if(dinfo == nil){
        LogManager_AddError("dinfo == nil");
        return false;
    }
    LogManager_AddInformationF("app version: %s", [dinfo.firmwareRevision UTF8String]);
    LogManager_AddInformationF("f/w version: %s", [dinfo.hardwareRevision UTF8String]);
    
    version<int> minVer(PPConfig::instance().get<std::string>("bluepad.minAppVersion"));
    version<int> curVer([dinfo.firmwareRevision UTF8String]);
    
    if(curVer < minVer){
        LogManager_AddWarningF("app ver too old : cur %s, min %s", curVer.toString() % minVer.toString());
        return false;
    }
    return true;
}

/* virtual */
auto CardReaderBluePad::displayText(const std::string& str) -> pprx::observable<pprx::unit>
{
    // CardReaderMiuraで使用している処理。pure virtualのためダミー処理を入れている。
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::displayTextNoLf(const std::string& str) -> pprx::observable<pprx::unit>
{
    // CardReaderMiuraで使用している処理。pure virtualのためダミー処理を入れている。
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::displayIdleImage() -> pprx::observable<pprx::unit>
{
    // CardReaderMiuraで使用している処理。pure virtualのためダミー処理を入れている。
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::clearCardreaderDisplay() -> pprx::observable<pprx::unit>
{
    // CardReaderMiuraで使用している処理。pure virtualのためダミー処理を入れている。
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::connect()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto&& timeoutsec = PPConfig::instance().get<int>("bluepad.connectTimeoutSecond");

    return observableDeviceConnectState()
    .timeout(std::chrono::seconds(timeoutsec))
    .flat_map([=](DeviceConnectState state){
        if(state == DeviceConnectState::Connected){
            LogManager_AddInformationF("%s -> already connected", deviceConnectStateToString(state));
            return pprx::just(pprx::unit()).as_dynamic();
        }
        else{
            LogManager_AddInformationF("%s -> connect", deviceConnectStateToString(state));
            m_dtd.connect();
        }
        return pprx::never<pprx::unit>().as_dynamic();
    }).as_dynamic()
    .take(1)
    .on_error_resume_next([=](auto err){
        try{ std::rethrow_exception(err); }
        catch(const pprx::timeout_error&){
            throw_error_cardrw(makeErrorJson("connetTimeout"));
        }
        return pprx::error<pprx::unit>(err);
    })
    .flat_map([=](auto){
        if(!isValidAppVersion()){
            throw_error_cardrw(makeErrorJson("firmwareTooOld"));
            /* return */
        }
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::disconnect()
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    /* disconnected は同期ではなく投げっぱなしとする */
    return observableDeviceConnectState()
    .flat_map([=](DeviceConnectState state){
        if(state == DeviceConnectState::Connected){
            m_dtd.disconnect();
        }
        return pprx::just(pprx::unit());
    }).as_dynamic()
    .take(1);
}

/* virtual */
auto CardReaderBluePad::beginCardRead(const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::start<pprx::unit>([=](){
        bool bSuccess = false;
        do{
            if(!m_dtd.emv2Initialize()) break;
            
            auto info = m_dtd.emv2GetInfo();
            if(info == nil) break;
            
            auto messages = PPConfig::instance().getChild("bluepad.messages.contactless");
            setMessages(messages);
            
            if(!m_dtd.uiShowInitScreen()) break;
            if(!m_dtd.emv2SetPINOptions(PIN_ENTRY_DISABLED)) break;
            if(!m_dtd.emv2StartTransactionOnInterface(EMV_INTERFACE_MAGNETIC, 0, nil, 60)) break;
            bSuccess = true;
        } while(false);
        
        if(!bSuccess){
            throw_error_cardrw(makeErrorJson("general"));
        }
        return pprx::unit();
    }).as_dynamic()
    .flat_map([=](auto) {
        return observableICCardInserted();
    }).as_dynamic()
    .take(1);
}

/* virtual */
auto CardReaderBluePad::setICCardRemoved()
-> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::beginICCardTransaction()
-> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        NSError* error;
        
        /* emv2CancelTransactionとemv2Deinitializeは磁気カードをキャンセルするために呼び出す */
        if(!m_dtd.emv2CancelTransaction(&error)) {
            throw_error_cardrw(makeErrorJson("emv2CancelTransaction"));
        }

        if(!m_dtd.emv2Deinitialize(&error)){
            throw_error_cardrw(makeErrorJson("emv2Deinitialize"));
        }
        
        NSData* atr= m_dtd.scCardPowerOn(m_currentSlot, &error);
        if(error || (atr == nil)) {
            throw_error_cardrw(makeErrorJson("unknown"));
        }
        
        m_dtd.uiShowInitScreen();
        m_dtd.uiDrawText(@"処理中", 0, 7, FONT_10X10_JPN);
        m_bEnableErrorOnSmatCardRemoved = true;
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::endICCardTransaction()
-> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        m_dtd.uiShowInitScreen();
        m_bEnableErrorOnSmatCardRemoved = false;
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::checkMasterData(const_json_t data)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    auto hold = holdThis();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        hold->explicitCapture();
        bool bSuccess = false;
        json_t result;
        do{
            if(!m_dtd.emv2Initialize()) break;    /* emv2LoadXXXX() で内部的に呼ばれている（っぽい）が、念のため */
            /* 終了時にemv2Deinitialize()を呼ぶべきか悩む */
            if(!m_dtd.ppadEnableStatusLine(true)) break;
            
            NFCTagConverterBluePad  cnv;
            cnv.loadJMupsData(data);
            
            bool bNeedUpdateCAPK = false;
            bool bNeedUpdateConfig = false;
            {   /* バージョン確認 */
                auto bpInfo = m_dtd.emv2GetInfo();
                if(bpInfo == nil) break;
                
                {
                    auto bpVer      = (boost::format("%08X") % bpInfo.contactlessCAPKVersion).str();
                    auto dataVer    = cnv.getMasterData().get<std::string>("capk.E4.C1");
                    LogManager_AddInformationF("capk bp %s, hps %s", bpVer % dataVer);
                    if(bpVer != dataVer){
                        bNeedUpdateCAPK = true;
                    }
                }
                {
                    auto bpVer      = (boost::format("%08X") % bpInfo.contactlessConfigurationVersion).str();
                    auto dataVer    = cnv.getMasterData().get<std::string>("config.E4.C1");
                    LogManager_AddInformationF("config bp %s, hps %s", bpVer % dataVer);
                    if(bpVer != dataVer){
                        bNeedUpdateConfig = true;
                    }
                }
            }
            const bool bNeedUpdate = bNeedUpdateCAPK | bNeedUpdateConfig;
            LogManager_AddInformationF("bNeedUpdate -> %s", (bNeedUpdate ? "true" : "false"));
            result.put("bNeedUpdate", bNeedUpdate);
            bSuccess = true;
        } while(false);
        if(!bSuccess){
            throw_error_cardrw(makeErrorJson("general"));
            /* return */
        }
        return pprx::just(result.as_readonly());
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::updateMasterData(const_json_t data)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    auto hold = holdThis();
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        hold->explicitCapture();
        bool bSuccess = false;
        do{
            if(!m_dtd.emv2Initialize()) break;    /* emv2LoadXXXX() で内部的に呼ばれている（っぽい）が、念のため */
            /* 終了時にemv2Deinitialize()を呼ぶべきか悩む */
            if(!m_dtd.ppadEnableStatusLine(true)) break;
            
            NFCTagConverterBluePad  cnv;
            cnv.loadJMupsData(data);
            
            /* updateする際はcapk,config両方行う */
            {
                auto cakey = cnv.getCAPublicKeyBytes();
                LogManager_AddDebugF("contactless cakey:%s", BaseSystem::dumpHexText(*cakey));
                if(!m_dtd.emv2LoadContactlessCAPK(IOSUtilities::convert(*cakey))) break;
            }
#if defined(DEBUG)
#if 0
            /* IC contact の証明書は書き換えてはいけない */
            {
                auto cakey = cnv.getCAPublicKeyBytes();
                LogManager_AddDebugF("contact cakey:%s", BaseSystem::dumpHexText(*cakey));
                if(!m_dtd.emv2LoadContactCAPK(IOSUtilities::convert(*cakey))) break;
            }
#endif
#endif
            {
                auto config = cnv.getConfigureBytes();
                LogManager_AddDebugF("config:%s", BaseSystem::dumpHexText(*config));
                if(!m_dtd.emv2LoadContactlessConfiguration(IOSUtilities::convert(*config))) break;
            }
            if(!m_dtd.uiShowInitScreen()) break;
            bSuccess = true;
        } while(false);
        if(!bSuccess){
            throw_error_cardrw(makeErrorJson("general"));
            /* return */
        }
        return pprx::just(pprx::unit());
    });
}

/* virtual */
auto CardReaderBluePad::readContactless(const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    {
        auto&& isTraining = param.get_optional<bool>("isTraining");
        if(isTraining && *isTraining){
            m_abortedForTrainingMode = false;
            return readContactlessForTraining(param).as_dynamic();
        }
    }
    /*
     *  m_bInSeePhone はちょっとクセがあるので注意。
     *  readContactless()は、SeePhone処理につき、外部では下記のようにリトライが行われる想定。
     *  readContactless().flatmap()..... retry()
     *  この場合、m_bInSeePhone は readContactless() の開始で初期化されるが、
     *  retry() 時には m_bInSeePhoneの 初期化が行われないことに注意。
     */
    m_bInSeePhone = false;
    m_subjectContactlessTransactionFinished = pprx::subject<const_json_t>();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        bool bSuccess = false;
        do{
            if(!m_dtd.emv2Initialize()) break;
            if(!m_dtd.ppadEnableStatusLine(false)) break;
            
            auto messages = PPConfig::instance().getChild("bluepad.messages.contactless");
            setMessages(messages);
            
            auto optMessages = param.get_optional("optionalMessage");
            if(optMessages){
                setMessages(*optMessages);
            }
            
            auto currencyCode = param.get_optional<int>("currencyCode");
            if(!currencyCode){
                LogManager_AddError("currencyCode not found.");
                break;
            }
            
            int totalAmount = 0;
            {
                auto amount = param.get_optional<int>("amount");
                if(!amount){
                    LogManager_AddError("amount not found.");
                    break;
                }
                totalAmount = amount.get();
                auto taxOtherAmount = param.get_optional<int>("taxOtherAmount");
                if(taxOtherAmount){
                    totalAmount += taxOtherAmount.get();
                    LogManager_AddInformationF("total amount %d = amount %d + taxOther %d", totalAmount % amount.get() % taxOtherAmount.get());
                }
                else{
                    LogManager_AddInformationF("total amount %d = amount %d + taxOther (null)", totalAmount % amount.get());
                }
            }
            
            if(!m_dtd.emv2SetTransactionType(0, totalAmount, currencyCode.get())) break;
            if(!m_dtd.emv2SetPINOptions(PIN_ENTRY_DISABLED)) break;
            
            NSData* initData = nil;
            if(m_bInSeePhone){
                LogManager_AddInformation("SEE PHONE -> set 0xDFC401");
                auto tagDFC401 = [ppTLV tlvWithString:@"01" tag:0xDFC401];
                initData = [ppTLV encodeTags:@[tagDFC401]];
            }
            if(!m_dtd.emv2StartTransactionOnInterface(EMV_INTERFACE_CONTACTLESS, 0, initData, 30)) break;
            
            bSuccess = true;
        } while(false);
        if(!bSuccess){
            throw_error_cardrw(makeErrorJson("general"));
            /* return */
        }
        
        return m_subjectContactlessTransactionFinished.get_observable();
    });
}

auto CardReaderBluePad::readContactlessForTraining(const_json_t param)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .observe_on(pprx::observe_on_thread_pool())
    .flat_map([=](auto){
        do{
            if(!m_dtd.emv2Deinitialize()) break;
            if(!m_dtd.rfInit(CARD_SUPPORT_FELICA)) break;
            if(!m_dtd.ppadEnableStatusLine(false)) break;
            
            auto _ledctrl = [=](bool led1, bool led2, bool led3, bool led4)
            {
                const uint _led1 = led1 ? 1 : 0;
                const uint _led2 = led2 ? 1 : 0;
                const uint _led3 = led3 ? 1 : 0;
                const uint _led4 = led4 ? 1 : 0;
                LEDControl led[] =
                {
                    /* ledNumber, periodMS, numBits, bitMask */
                    { 0, 500, 1, _led1 },
                    { 1, 500, 1, _led2 },
                    { 2, 500, 1, _led3 },
                    { 3, 500, 1, _led4 }
                };
                m_dtd.uiControlLEDsEx(led, 4);
            };
            
            auto _sound = [=](int volume, int hz, int ms)
            {
                int arr[] = { hz, ms };
                m_dtd.playSound(volume, arr, sizeof(arr));

            };
            
            auto _lcd = [=](const std::string& msgId)
            {
                m_dtd.uiFillRectangle();

                auto _lcd_internal = [=](const std::string& msgId, json_t json) -> bool
                {
                    auto text_default   = json.get_optional<std::string>("defaults.text");
                    auto font_default   = json.get_optional<std::string>("defaults.font");
                    auto table          = json.get<json_t::array_t>("table");
                    
                    for(auto item : table){
                        auto _id    = item.get_optional<std::string>("id");
                        if(_id && ((*_id) == msgId)){
                            auto _text  = item.get_optional<std::string>("text");
                            auto _font  = item.get_optional<std::string>("font");
                            if(!_text){
                                _text = text_default;
                            }
                            if(!_font){
                                _font = font_default;
                            }
                            if(!_text){
                                LogManager_AddErrorF("'text' not found.(id='%s')", _id.get());
                                continue;
                            }
                            if(!_font){
                                LogManager_AddErrorF("'font' not found.(id='%s')", _id.get());
                                continue;
                            }
                            auto nfont = tableStringToFONT_XXXX()[*_font];
                            m_dtd.uiDrawText([NSString stringWithUTF8String:(*_text).c_str()], 0, 0, nfont);
                            return true;
                        }
                    }
                    return false;
                };
                auto optMessages = param.get_optional("optionalMessage");
                if(optMessages){
                    if(_lcd_internal(msgId, *optMessages)) return;
                }
                if(_lcd_internal(msgId, PPConfig::instance().getChild("bluepad.messages.contactless"))) return;
                LogManager_AddErrorF("not found.(id='%s')", msgId);
            };
            
            _lcd("EMV_UI_PRESENT_CARD");

            _ledctrl(true, false, false, false);
            while(true){
                bool bAborted = m_abortedForTrainingMode.referenceBlockWithResult<bool>([&](auto&& _value){
                    return _value;
                });
                if(bAborted) break;
                
                auto info = m_dtd.rfDetectCardOnChannel(RF_CHANNEL_FELICA, nil);
                if(info != nil){
                    m_dtd.rfClose();
                    const int baseHz = 1500;
                    _sound(20, baseHz, 700);
                    
                    _ledctrl(true, true, false, false);
                    BaseSystem::sleep(200);
                    _ledctrl(true, true, true, false);
                    BaseSystem::sleep(200);
                    _ledctrl(true, true, true, true);
                    BaseSystem::sleep(500);

                    _lcd("EMV_UI_APPROVED");
                    _sound(100, baseHz * 4, 800);
                    BaseSystem::sleep(1000);

                    json_t json;
                    json.put("cardBrand", "VISA");
                    json.put("keySerialNoForResult", "00000000000000000000");
                    json.put("nfcTradeResult", "0000");
                    json.put("outcomeParameter", "0000000000000000");
                    json.put("resultCodeExtended", "00000000");
                    json.put("status", "approved");
                    json.put("transactionResultBit", "0000000000");

                    _ledctrl(false, false, false, false);
                    if(!m_dtd.ppadEnableStatusLine(true)) break;
                    if(!m_dtd.uiShowInitScreen()) break;
                    return pprx::just(json.as_readonly()).as_dynamic();
                }
            }
            
        } while(false);
        return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("generic"))).as_dynamic();
    }).as_dynamic();
}


void CardReaderBluePad::setMessages(const_json_t messages)
{
    auto text_default   = messages.get_optional<std::string>("defaults.text");
    auto font_default   = messages.get_optional<std::string>("defaults.font");
    auto table          = messages.get<json_t::array_t>("table");
    
    auto&& strToEmvUi = tableStringToEMV_UIXXXX();
    auto&& strToFont  = tableStringToFONT_XXXX();
    
    for(auto item : table){
        auto _id    = item.get_optional<std::string>("id");
        auto _text  = item.get_optional<std::string>("text");
        auto _font  = item.get_optional<std::string>("font");
        if(!_id){
            LogManager_AddError("'id' not found.");
            continue;
        }
        if(!_text){
            _text = text_default;
        }
        if(!_font){
            _font = font_default;
        }
        if(!_text){
            LogManager_AddErrorF("'text' not found.(id='%s')", _id.get());
            continue;
        }
        if(!_font){
            LogManager_AddErrorF("'font' not found.(id='%s')", _id.get());
            continue;
        }
        
        const auto _nid = strToEmvUi[_id.get()];
        const auto _nfont = strToFont[_font.get()];
        m_dtd.emv2SetMessage(_nid, _nfont, _text.get());
    }
}

/* virtual */
auto CardReaderBluePad::adjustDateTime(const boost::posix_time::ptime& dtime)
 -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto dt = IOSUtilities::convert(dtime);
        NSError* err;
        const auto bSuccess = m_dtd.rtcSetDeviceDate(dt, &err);
        if(bSuccess){
            LogManager_AddInformationF("adjust date time to %s GMT", boost::posix_time::to_iso_extended_string(dtime));
            return pprx::just(pprx::unit()).as_dynamic();
        }
        return pprx::error<pprx::unit>(make_error_cardrw(makeErrorJson("general"))).as_dynamic();
    });
}

/* virtual */
auto CardReaderBluePad::finalizeSelf() -> pprx::observable<pprx::unit>
{
    LogManager_Function();

    m_subjectContactlessTransactionFinished.get_subscriber().on_completed();
    m_subjectPINEntryCompleteWithError.get_subscriber().on_completed();
    
    m_bEnableErrorOnSmatCardRemoved = false;
    m_abortedForTrainingMode = true;

    return observableDeviceConnectState()
    .take(1)
    .flat_map([=](DeviceConnectState state){
        if(state == DeviceConnectState::Connected){
            m_dtd.emv2CancelTransaction();
            m_dtd.emv2Deinitialize();
            m_dtd.ppadCancelPINEntry();
            m_dtd.rfClose();
            LEDControl led[] =
            {
                /* ledNumber, periodMS, numBits, bitMask */
                { 0, 500, 1, 0 },
                { 1, 500, 1, 0 },
                { 2, 500, 1, 0 },
                { 3, 500, 1, 0 }
            };
            m_dtd.uiControlLEDsEx(led, 4);
            m_dtd.ppadEnableStatusLine(true);
            m_dtd.uiShowInitScreen();
        }
        else{
            LogManager_AddInformation("device not connected -> skip dtdev finalize.");
        }
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

/* virtual */
void CardReaderBluePad::prepareObservablesSelf()
{
    LogManager_Function();

    m_subjectContactlessTransactionFinished = pprx::subject<const_json_t>();
    m_subjectPINEntryCompleteWithError      = pprx::subject<const_json_t>();
}

NSData* CardReaderBluePad::createNfcTagListForTradeResult() const
{
    return [ppTLV encodeTagList:
            @[
              @0x50,  // HPS
              @0x56,  // HPS
              @0x57, //track2
              @0xDF4F,  // HPS
              @0x5A, //pan
              @0x82,  // HPS
              @0x84,  // HPS
              @0x89,  // HPS
              @0x8A,  // HPS
              @0x8C,  // HPS
              @0x8D,  // HPS
              @0x8E,  // HPS
              @0x8F,  // HPS
              @0x95,  // HPS
              @0x9A,  // HPS
              @0x9B,  // HPS
              @0x9C,  // HPS
              @0x5F20,  //account name
              @0x5F24,  //expiration date
              @0x5F25,  // HPS
              @0x5F28,  // HPS
              @0x5F2A,  // HPS
              @0x5F2D,  // HPS
              @0x5F34,  // HPS
              @0x5F36,  // HPS
              @0x9F02,  // HPS
              @0x9F03,  // HPS
              @0x9F05,  // HPS
              @0x9F07,  // HPS
              @0x9F08,  // HPS
              @0x9F09,  // HPS
              @0x9F0D,  // HPS
              @0x9F0E,  // HPS
              @0x9F0F,  // HPS
              @0x9F10,  // HPS
              @0x9F1A,  // HPS
              @0x9F1E,  // HPS
              @0x9F1F,  // HPS
              @0x9F20,  // HPS
              @0x9F21,  // HPS
              @0x9F26,  // HPS
              @0x9F27,  // HPS
              @0x9F33,  // HPS
              @0x9F34,  // HPS
              @0x9F35,  // HPS
              @0x9F36,  // HPS
              @0x9F37,  // HPS
              @0x9F38,  // HPS
              @0x9F41,  // HPS
              @0x9F42,  // HPS
              @0x9F43,  // HPS
              @0x9F44,  // HPS
              @0x9F45,  // HPS
              @0x9F50,  // HPS
              @0x9F51,  // HPS
              @0x9F52,  // HPS
              @0x9F53,  // HPS
              @0x9F5A,  // HPS
              @0x9F5B,  // HPS
              @0x9F5C,  // HPS
              @0x9F5D,  // HPS
              @0x9F5F,  // HPS
              @0x9F60,  // HPS
              @0x9F61,  // HPS
              @0x9F62,  // HPS
              @0x9F63,  // HPS
              @0x9F64,  // HPS
              @0x9F65,  // HPS
              @0x9F66,  // HPS
              @0x9F67,  // HPS
              @0x9F69,  // HPS
              @0x9F6A,  // HPS
              @0x9F6B,  // HPS
              @0x9F6C,  // HPS
              @0x9F6E,  // HPS
              @0x9F70,  // HPS
              @0x9F71,  // HPS
              @0x9F74,  // HPS
              @0x9F7A,  // HPS
              @0x9F7C,  // HPS
              @0x9F7E,  // HPS
              @0xDF4B,  // HPS
              @0xDF810C,  // HPS
              @0xDF8116,  // HPS
              @0xDF811B,  // HPS
              @0xDF812A,  // HPS
              @0xDF812B,  // HPS
              @0xFF8105,  // HPS
              @0xFF8106   // HPS
              ]];
}

#define _make_pair_(X) {#X,X}

std::map<std::string,int> CardReaderBluePad::tableStringToEMV_UIXXXX() const
{
    return std::map<std::string,int>
    {
        _make_pair_(EMV_UI_STATUS_NOT_READY),
        _make_pair_(EMV_UI_STATUS_IDLE),
        _make_pair_(EMV_UI_STATUS_READY_TO_READ),
        _make_pair_(EMV_UI_STATUS_PROCESSING),
        _make_pair_(EMV_UI_STATUS_CARD_READ_SUCCESS),
        _make_pair_(EMV_UI_STATUS_ERROR_PROCESSING),
        _make_pair_(EMV_UI_NOT_WORKING),
        _make_pair_(EMV_UI_APPROVED),
        _make_pair_(EMV_UI_DECLINED),
        _make_pair_(EMV_UI_PLEASE_ENTER_PIN),
        _make_pair_(EMV_UI_ERROR_PROCESSING),
        _make_pair_(EMV_UI_REMOVE_CARD),
        _make_pair_(EMV_UI_IDLE),
        _make_pair_(EMV_UI_PRESENT_CARD),
        _make_pair_(EMV_UI_PROCESSING),
        _make_pair_(EMV_UI_CARD_READ_OK_REMOVE),
        _make_pair_(EMV_UI_TRY_OTHER_INTERFACE),
        _make_pair_(EMV_UI_CARD_COLLISION),
        _make_pair_(EMV_UI_SIGN_APPROVED),
        _make_pair_(EMV_UI_ONLINE_AUTHORISATION),
        _make_pair_(EMV_UI_TRY_OTHER_CARD),
        _make_pair_(EMV_UI_INSERT_CARD),
        _make_pair_(EMV_UI_CLEAR_DISPLAY),
        _make_pair_(EMV_UI_SEE_PHONE),
        _make_pair_(EMV_UI_PRESENT_CARD_AGAIN),
        _make_pair_(EMV_UI_SELECT_APPLICAITON),
        _make_pair_(EMV_UI_MANUAL_ENTRY),
        _make_pair_(EMV_UI_PROMPT_ENTER_PIN),
        _make_pair_(EMV_UI_PROMPT_WRONG_PIN),
        _make_pair_(EMV_UI_PROMPT_PIN_LAST_ATTEMPT),
        _make_pair_(EMV_UI_NA)
    };
}

std::map<std::string,FONTS> CardReaderBluePad::tableStringToFONT_XXXX() const
{
    return std::map<std::string,FONTS>
    {
        _make_pair_(FONT_6X8),
        _make_pair_(FONT_8X16),
        _make_pair_(FONT_4X6),
        _make_pair_(FONT_10X10_JPN),
        _make_pair_(FONT_6X10_JPN)
    };
}

/* virtual */
auto CardReaderBluePad::sendApdu(const_json_t data)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto sapdu = data.get<std::string>("kernelResponse.device.value.param.in_apdu_data");
        auto apdu = BaseSystem::hexTextToBytes(sapdu);
        
        bool bEncrypted = false;
        bool bVerify = false;
     
        {
            auto expr_1 = boost::regex("^0[0-9A-F]B2.*$", boost::regex::icase);
            auto expr_2 = boost::regex("^0084000000$", boost::regex::icase);
            auto expr_3 = boost::regex("^002000.*$", boost::regex::icase);
            
            if(boost::regex_match(sapdu, expr_1)){
                bEncrypted = true;
            }
            if(boost::regex_match(sapdu, expr_2)){
                bEncrypted = true;
            }
            if(boost::regex_match(sapdu, expr_3)){
                bVerify = true;
            }
        }
     
        auto convertJMupsData = [](NSData* emvData)
        {
            /* DATECSからのデータは下記のイメージ
                 struct {
                     WORD       ... ???;
                     BYTE x N   ... DATA
                     BYTE x 2   ... SW1SW2
                     BYTE x 10  ... KSN
                 }
             
             JMupsに返却するデータは "EE"タグに"DFAE0x"タグが内包されていて、最後にSW1SW2を付加する
                "EE"
                {
                    "DFAE02" ... data
                    "DFAE03" ... KSN
                }
                SW1SW2
             */
            auto src = IOSUtilities::convert(emvData);
            bytes_t ksn, sw1sw2, data;
            
            std::copy(std::end(*src) - 10    , std::end(*src)      , std::back_inserter(ksn));
            std::copy(std::end(*src) - 12    , std::end(*src) - 10 , std::back_inserter(sw1sw2));
            std::copy(std::begin(*src) + 2   , std::end(*src) - 12 , std::back_inserter(data));
            
            APDU::TLV _EE({0xEE});
            _EE.innerAppend({0xDF, 0xAE, 0x02}, data);
            _EE.innerAppend({0xDF, 0xAE, 0x03}, ksn);

            auto result = _EE.getBytes();
            std::copy(std::begin(sw1sw2), std::end(sw1sw2), std::back_inserter(result));
            return result;
        };
        
        bytes_t apduResponse;
        
        NSError* err;
        do{
            if(bVerify){
                auto sinspection = data.get<std::string>("session.transaction.inspectionMethod");
                auto inspection = boost::lexical_cast<int>(sinspection);
                LogManager_AddDebugF("inspectoin Method:%d", inspection);
                
                if ((inspection == 2) || (inspection == 4)) {
                    // オフライン平文
                    auto bpres = m_dtd.ppadVerifyPINOfflinePlainAndEncryptResponse(ALG_APDU_DUKPT, 0, &err);
                    if(err) break;
                    apduResponse = convertJMupsData(bpres);
                }
                else if ((inspection == 5) || (inspection == 6)) {
                    auto bpres = m_dtd.ppadVerifyPINOfflineEncryptedAndEncryptResponse(ALG_APDU_DUKPT, 0, &err);
                    if(err) break;
                    apduResponse = convertJMupsData(bpres);
                }
                
            }
            else if(bEncrypted){
                auto bpres = m_dtd.scEncryptedCAPDU(SLOT_MAIN, ALG_APDU_DUKPT, 0, IOSUtilities::convert(*apdu), &err);
                if(err) break;
                apduResponse = convertJMupsData(bpres);
            }
            else{
                auto bpres = m_dtd.scCAPDU(SLOT_MAIN, IOSUtilities::convert(*apdu), &err);
                if(err) break;
                apduResponse = *IOSUtilities::convert(bpres);
            }
        
            json_t result;
            result.put("result", BaseSystem::dumpHexText(apduResponse));
            return pprx::just(result.as_readonly()).as_dynamic();
        } while(false);
        
        return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
    });
}


bytes_t CardReaderBluePad::convertEncryptedApduResponseForJMups(NSData* emvData) const
{
    /* DATECSからのデータは下記のイメージ
     struct {
     WORD       ... ???;
     BYTE x N   ... DATA
     BYTE x 2   ... SW1SW2
     BYTE x 10  ... KSN
     }
     
     JMupsに返却するデータは "EE"タグに"DFAE0x"タグが内包されていて、最後にSW1SW2を付加する
     "EE"
     {
     "DFAE02" ... data
     "DFAE03" ... KSN
     }
     SW1SW2
     */
    auto src = IOSUtilities::convert(emvData);
    bytes_t ksn, sw1sw2, data;
    
    std::copy(std::end(*src) - 10    , std::end(*src)      , std::back_inserter(ksn));
    std::copy(std::end(*src) - 12    , std::end(*src) - 10 , std::back_inserter(sw1sw2));
    std::copy(std::begin(*src) + 2   , std::end(*src) - 12 , std::back_inserter(data));
    
    APDU::TLV _EE({0xEE});
    _EE.innerAppend({0xDF, 0xAE, 0x02}, data);
    _EE.innerAppend({0xDF, 0xAE, 0x03}, ksn);
    
    auto result = _EE.getBytes();
    std::copy(std::begin(sw1sw2), std::end(sw1sw2), std::back_inserter(result));
    return result;
};

auto CardReaderBluePad::waitPinInput()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();

    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        m_dtd.uiDrawText(@"暗証番号入力:\n", 0, 8, FONT_10X10_JPN);
        //m_dtd.uiDrawText(@"Please input PIN:\n", 0, 8, FONT_6X8);

        NSError* err;
        m_dtd.ppadStartPINEntry(0, 2, 60, '!', nil, &err);
        if(err){
            return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
        }
        return m_subjectPINEntryCompleteWithError.get_observable();
    });
}


/* virtual */
auto CardReaderBluePad::verifyPlainOfflinePin(const_json_t paramjson)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return waitPinInput()
    .flat_map([=](const_json_t cjson){
        auto json = cjson.clone();
        auto result = json.get<std::string>("result");
        if(result == "success"){
            NSError* err;
            auto bpres = m_dtd.ppadVerifyPINOfflinePlainAndEncryptResponse(ALG_APDU_DUKPT, 0, &err);
            if(err){
                return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
            }
            auto apduResponse = convertEncryptedApduResponseForJMups(bpres);
            json.put("data", BaseSystem::dumpHexText(apduResponse));
        }
        return pprx::just(json.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::verifyEncryptedOfflinePin(const_json_t paramjson)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return waitPinInput()
    .flat_map([=](const_json_t cjson){
        auto json = cjson.clone();
        auto result = json.get<std::string>("result");
        if(result == "success"){
            json.put("data", "0000");
        }
        return pprx::just(json.as_readonly()).as_dynamic();
    });
}

/* virtual */
auto CardReaderBluePad::verifyEncryptedOnlinePin(const_json_t paramjson)
-> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    /* TODO: オンライン暗号PINをちゃんと実装すること */
    return waitPinInput()
    .flat_map([=](const_json_t cjson){
        if(cjson.get<std::string>("result") != "success"){
            return pprx::just(cjson).as_dynamic();
        }
        
        NSError* err;
        auto nsBPData = m_dtd.ppadGetPINBlockUsingDUKPT(1, nil, PIN_FORMAT_ISO1, &err);
        if(err){
            return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
        }
        auto bpData = IOSUtilities::convert(nsBPData);

        auto pinData = bytes_t{0xdf, 0xae, 0x04, static_cast<bytes_t::value_type>(bpData->size() - 10)};
        pinData.insert(pinData.end(), bpData->begin(), bpData->end() - 10);
        
        auto ksnData = bytes_t{0xdf, 0xae, 0x05, 0x0A};
        ksnData.insert(ksnData.end(), bpData->end() - 10, bpData->end());
        
        auto data = bytes_t{0xe1, static_cast<bytes_t::value_type>(pinData.size() + ksnData.size())};
        data.insert(data.end(), pinData.begin(), pinData.end());
        data.insert(data.end(), ksnData.begin(), ksnData.end());

        auto json = cjson.clone();
        json.put("data", BaseSystem::dumpHexText(data));
        return pprx::just(json.as_readonly()).as_dynamic();
    });
}

/* virtual */
auto CardReaderBluePad::abortTransaction() -> pprx::observable<pprx::unit>
{
    return pprx::just(pprx::unit());
}

/* virtual */
void CardReaderBluePad::setLockTransactionState(bool blockd )
{
    
}

/* virtual */
auto CardReaderBluePad::txPlainApdu(const std::string& apdu)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto _apdu = BaseSystem::hexTextToBytes(apdu);

        NSError* err;
        auto bpres = m_dtd.scCAPDU(SLOT_MAIN, IOSUtilities::convert(*_apdu), &err);
        if(err){
            return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
        }
        auto apduResponse = *IOSUtilities::convert(bpres);
        json_t result;
        result.put("result", BaseSystem::dumpHexText(apduResponse));
        return pprx::just(result.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::txEncryptedApdu(const std::string& apdu)
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        auto _apdu = BaseSystem::hexTextToBytes(apdu);
        
        NSError* err;
        auto bpres = m_dtd.scEncryptedCAPDU(SLOT_MAIN, ALG_APDU_DUKPT, 0, IOSUtilities::convert(*_apdu), &err);
        if(err){
            return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
        }
        auto apduResponse = convertEncryptedApduResponseForJMups(bpres);
        json_t result;
        result.put("result", BaseSystem::dumpHexText(apduResponse));
        return pprx::just(result.as_readonly()).as_dynamic();
    }).as_dynamic();
}

/* virtual */
auto CardReaderBluePad::getIfdSerialNo()
 -> pprx::observable<const_json_t>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](auto){
        NSError* err;
        do{
            auto info = m_dtd.ppadGetSystemInfo(&err);
            auto serialBytes = IOSUtilities::convert(info.pinpadSerial);
            json_t result;
            result.put("result", BaseSystem::dumpHexText(*serialBytes));
            return pprx::just(result.as_readonly()).as_dynamic();
        } while(false);
        return pprx::error<const_json_t>(make_error_cardrw(makeErrorJson("general", static_cast<int>(err.code)))).as_dynamic();
    }).as_dynamic();
}

#pragma mark derive from DTDevicesCpp::EventListener

/* virtual */
void CardReaderBluePad::connectionState(CONN_STATES state)
{
    LogManager_Function();
    
    switch(state){
        case CONN_DISCONNECTED:
        {
            LogManager_AddInformationF("(%d) CONN_DISCONNECTED", state);
            setDeviceConnectState(DeviceConnectState::Bad);
            break;
        }

        /* この状態通知は複数の要因で発生する */
        /* (1) disconnect 状態から connect() を呼び出した場合 */
        /* (2) BPが電源断等で接続解除された場合 */
        case CONN_CONNECTING:
        {
            LogManager_AddInformationF("(%d) CONN_CONNECTING", state);
            setDeviceConnectState(DeviceConnectState::Disconnected);
            break;
        }
            
        case CONN_CONNECTED:
        {
            LogManager_AddInformationF("(%d) CONN_CONNECTED", state);
            setDeviceConnectState(DeviceConnectState::Connected);
//            m_dtd.uiShowInitScreen();
//            m_dtd.ppadEnableStatusLine(true);
            break;
        }
            
        default:
        {
            LogManager_AddInformationF("(%d) unknown state", state);
            break;
        }
    }
}

void CardReaderBluePad::deviceFeatureSupported(FEATURES feature, int value)
{
    // LogManager_Function();
}

void CardReaderBluePad::rfCardDetected(int cardIndex, DTRFCardInfo* info)
{
    LogManager_Function();
}

void CardReaderBluePad::smartCardInserted(SC_SLOTS slot)
{
    LogManager_Function();

    m_currentSlot = slot;

    json_t json;
    json.put("cardType", "contact");
    json.put("status", "approved");

    subscriberICCardInserted().on_next(json);
    setICCardSlotState(ICCardSlotState::Inserted);
}

void CardReaderBluePad::smartCardRemoved(SC_SLOTS slot)
{
    LogManager_Function();
    if(m_bEnableErrorOnSmatCardRemoved){
        /*
         * ICカード処理は ICCardInserted の購読から始まる。
         * ICCardInserted は、カード挿入時に on_next が emit されるが、
         * その後は、全体処理が終了するまで on_complete されないようにしている。
         * つまり、ICCardInserted は、カード挿入後も購読は続くので、
         * そこで error を emit すれば全体がエラーで終了する仕組みとなっている。
         */
        subscriberICCardInserted().on_error(make_error_cardrw(makeErrorJson("icCardRemoved")));
    }
    setICCardSlotState(ICCardSlotState::Removed);
}

BOOL CardReaderBluePad::pinEntryWaitRunLoop(NSError** error)
{
    LogManager_Function();
    
    return YES;
}

void CardReaderBluePad::PINEntryCompleteWithError(NSError* error)
{
    LogManager_Function();
    json_t json;
    if(error){
        switch(error.code){
            case DT_PPAD_ENO_DATA:
                json.put("result", "bypass");
                break;
            case DT_PPAD_ECANCELLED:
            case DT_PPAD_EINVALID_PIN:
                json.put("result", "retry");
                break;
            default:
                m_subjectPINEntryCompleteWithError.get_subscriber()
                    .on_error(make_error_cardrw(makeErrorJson("general", static_cast<int>(error.code))));
                return;
        }
    }
    else{
        json.put("result", "success");
    }
    m_subjectPINEntryCompleteWithError.get_subscriber().on_next(json);
}

void CardReaderBluePad::emv2OnOnlineProcessing(NSData* data)
{
    LogManager_Function();

    auto tag = [ppTLV decodeTags:data];
    auto df810c = [ppTLV findLastTag:0xDF810C tags:tag];  /* KERNEL_ID */

    if(df810c == nil){ /* 磁気カード */
        auto serverResponse = [ppTLV encodeTags:@[[ppTLV tlvWithHexString:@"30 30" tag:TAG_8A_AUTH_RESP_CODE]]];
        auto response = [ppTLV encodeTags:@[[ppTLV tlvWithHexString:@"01" tag:0xC2],[ppTLV tlvWithData:serverResponse tag:0xE6]]];
        m_dtd.emv2SetOnlineResult(response);
        return;
    }
    
    json_t json;
    json.put("status", "onlineProcessing");
    {
        const auto kernel = bytesToUint64(df810c.data);
        std::string resultCodeExtended;
        switch(kernel){
            case 1: resultCodeExtended = "1300"; break;
            case 2: resultCodeExtended = "2300"; break;
            case 3: resultCodeExtended = "3300"; break; //## with issuer update "3309"
            case 4: resultCodeExtended = "4300"; break; //## partial online "43B9"
            default: break;
        }
        resultCodeExtended += "0000";
        
        json.put("resultCodeExtended", resultCodeExtended);
        json.put("cardBrand", cardBrandFrom0xDF810C(df810c.data));
    }
    json.put("transactionResultBit", generateTransactionResultBit(tag));
    {
        //get the tags encrypted with 3DES CBC and key loaded at positon 2
        NSError* err;
        auto packetData = m_dtd.emv2GetTagsEncrypted
        (
         createNfcTagListForTradeResult(),
         TAGS_FORMAT_DATECS_SHORT,
         KEY_TYPE_DUKPT_3DES_CBC,
         0,
         0x12345678,
         &err
         );
        packetData = [packetData subdataWithRange:NSMakeRange(4, packetData.length-14)];
        json.put("nfcTradeResult", BaseSystem::dumpHexText(*IOSUtilities::convert(packetData)));
    }
    {
        auto df8129 = [ppTLV findLastTag:0xDF8129 tags:tag];  /* OUTCOME_PARAMETER_SET */
        json.put("outcomeParameter", BaseSystem::dumpHexText(*IOSUtilities::convert(df8129.data)));
    }
    {
        auto ksn = m_dtd.ppadGetDUKPTKeyKSN(0);
        json.put("keySerialNoForResult", BaseSystem::dumpHexText(*IOSUtilities::convert(ksn)));
    }
    
    LogManager_AddDebug(json.str());
    LogManager_AddInformation("wait for online pricessing (hps network access)");
    
    m_subjectContactlessTransactionFinished.get_subscriber().on_next(json);
    /*
     *  上記コードでHPSへアクセスしてCardRead処理を行う。
     *  m_dtd.emv2SetOnlineResult()の後、emv2OnTransactionFinished() は呼び出されるので、
     *  m_subjectContactlessTransactionFinished.get_subscriber() を終わらせる。
     */
    m_subjectContactlessTransactionFinished.get_subscriber().on_completed();
}

/* virtual */
auto CardReaderBluePad::onlineProccessingResult(bool bApproved) -> pprx::observable<pprx::unit>
{
    LogManager_AddInformation("back from online pricessing (hps network access)");
    auto resp = bApproved ? @"30 30" : @"30 35";
    auto serverResponse = [ppTLV encodeTags:@[[ppTLV tlvWithHexString:resp tag:TAG_8A_AUTH_RESP_CODE]]];
    auto response = [ppTLV encodeTags:@[[ppTLV tlvWithHexString:@"01" tag:0xC2],[ppTLV tlvWithData:serverResponse tag:0xE6]]];
    m_dtd.emv2SetOnlineResult(response);
    return pprx::just(pprx::unit());
}

/* virtual */
auto CardReaderBluePad::getReaderInfo() -> pprx::observable<const_json_t>
{
    LogManager_Function();
    /* TODO: 仕様決めること */
    return pprx::just(const_json_t());
}

/* virtual */
auto CardReaderBluePad::isCardInserted() -> pprx::observable<bool>
{
    LogManager_Function();
    return pprx::just(false);
}

/* virtual */
auto CardReaderBluePad::updateFirmware(const boost::filesystem::path& basedir) -> pprx::observable<pprx::unit>
{
    LogManager_Function();
    
    return pprx::just(pprx::unit())
    .flat_map([=](pprx::unit){
        json_t info;
        {
            std::ifstream is((basedir / "info.json").string());
            info.deserialize(is);
        }
        
        LogManager_AddInformation(info.str());
        
        return pprx::just(pprx::unit());
    }).as_dynamic();
}

auto CardReaderBluePad::getDisplayMsgForReader(const std::string& msgId, const bool bContactless/*= false*/) -> std::string
{
    std::string msg = "";
    std::string tablePath = bContactless ? "bluepad.messages.contactless.table" : "bluepad.messages.credit.table";
    auto table = PPConfig::instance().get<json_t::array_t>(tablePath);
    for(auto item : table) {
        auto value = item.get_optional<std::string>(msgId);
        if(value) {
            msg = item.get_optional<std::string>("text").value();
            break;
        }
    }

    return msg;
}

void CardReaderBluePad::emv2OnUserInterfaceCode(int code, int status, NSTimeInterval holdTime)
{
    LogManager_Function();
    LogManager_AddDebugF("code = %d, status = %d, holdTime = %.2f", code % status % holdTime);
    
    if(code == EMV_UI_SEE_PHONE){
        LogManager_AddInformation("SEE PHONE -> set m_bInSeePhone = true");
        m_bInSeePhone = true;
    }
}


uint64_t CardReaderBluePad::bytesToUint64(NSData* data) const
{
    /* 入力dataはビッグエンディアンを想定している */
    if(data == nil) return 0;
    const auto length = data.length;
    auto bytes = reinterpret_cast<const uint8_t* const>(data.bytes);
    if(BaseSystem::isBigEndian()){
        switch(length){
            case 1:
                return *reinterpret_cast<const uint8_t*>(bytes);
            case 2:
                return *reinterpret_cast<const uint16_t*>(bytes);
            case 4:
                return *reinterpret_cast<const uint32_t*>(bytes);
            case 8:
                return *reinterpret_cast<const uint64_t*>(bytes);
            default:
                break;
        }
    }
    uint64_t result = 0;
    for(auto i = 0; i < length; i++){
        result <<= 8;
        result |= (static_cast<uint64_t>(bytes[i]) & 0xFFLL);
    }
    return result;
}

std::string CardReaderBluePad::convertResultCodeForJMups(uint64_t resultCode, uint64_t kernel) const
{
    std::string result;
    
    if (kernel==0){
        return result;
    }
    else if ((kernel == 1) && (resultCode == EMV_RESULT_APPROVED)) result = "11000000";
    else if ((kernel == 2) && (resultCode == EMV_RESULT_APPROVED)) result = "21000000";
    else if ((kernel == 2) && (resultCode == EMV_RESULT_DECLINED)) result = "22110000";
    else if ((kernel == 3) && (resultCode == EMV_RESULT_APPROVED)) result = "31000000";
    else if ((kernel == 3) && (resultCode == EMV_RESULT_DECLINED)) result = "32110000";
    else if ((kernel == 4) && (resultCode == EMV_RESULT_APPROVED)) result = "41000000";
    else if ((kernel == 4) && (resultCode == EMV_RESULT_DECLINED)) result = "42110000";
    
    return result;
}

std::string CardReaderBluePad::cardBrandFrom0xDF810C(NSData* data) const
{
    const auto kernelId = BaseSystem::dumpHexText(*IOSUtilities::convert(data));
    
    if((kernelId == "01") || (kernelId == "03")){
        return "VISA";
    }
    else if(kernelId == "02"){
        return "MASTER";
    }
    else if(kernelId == "04"){
        return "AMEX";
    }
    return "N/A";
}

std::string CardReaderBluePad::generateTransactionResultBit(NSArray* tags) const
{
    /* これ、仕様書に記載ある？（手元の仕様書では2-5バイト目はRFUになっている） */
    bytes_t bits(5);
    
    auto t = [ppTLV findLastTag:TAG_C5_TRANSACTION_INFO tags:tags];
    if(t == nil) return "";
    
    if(t.bytes[1] & EMV_CL_TRANS_TYPE_MSD) bits[0] |= (1 << 6);
    t = [ppTLV findLastTag:TAG_C2_CVM tags:tags];
    if(t == nil) return "";
    
    switch(t.bytes[0]) {
        case EMV_CVM_NO_CVM:
            bits[1] |= (1<<0);
            break;
            
        case EMV_CVM_CONF_CODE_VERIFIED:
            bits[1] |= (1<<1);
            break;
            
        case EMV_CVM_OBTAIN_SIGNATURE:
            bits[1] |= (1<<2);
            break;
            
        case EMV_CVM_ONLINE_PIN:
            bits[1] |= (1<<3);
            break;
    }
    return BaseSystem::dumpHexText(bits);
}



void CardReaderBluePad::emv2OnTransactionFinished(NSData* data)
{
    LogManager_Function();
    
    bool bDeinitialized = false;
    
    auto tag = [ppTLV decodeTags:data];
    const auto transactionResult =
    bytesToUint64([ppTLV findLastTag:TAG_C1_TRANSACTION_RESULT
                              tags:tag].data);
    if(transactionResult == EMV_RESULT_APPROVED){
        json_t json;
        json.put("status", "approved");
        const auto interface =
        bytesToUint64([ppTLV findLastTag:TAG_C3_TRANSACTION_INTERFACE
                                  tags:tag].data);
        if(interface==EMV_INTERFACE_MAGNETIC){
            LogManager_AddInformation("EMV_INTERFACE_MAGNETIC");
            auto trackData = m_dtd.emv2GetCardTracksEncryptedWithFormat(ALG_PPAD_DUKPT_SEPARATE_TRACKS, 0);
            
            auto bytes = reinterpret_cast<const uint8_t*>(trackData.bytes);
            auto b = 1;
            
            bytes_t ksnData;
            bytes_t track2Data;
            
            while (b < trackData.length) {
                /* get the block */
                auto tid =  bytes[b++];
                auto tlen = (bytes[b + 0] << 8) | (bytes[b + 1]);
                b += 2;
                bytes_t tdata(bytes + b, bytes + b + tlen);
                b += tlen;
                /* find fields we care about and insert into nicos packet */
                if(tid == 0x00){     /* ksn data */
                    ksnData.insert(ksnData.end(), std::begin(tdata), std::end(tdata));
                }
                if(tid == 0x01){    /* tracks1 */
                }
                if(tid == 0x02){    /* tracks2 */
                    track2Data.insert(track2Data.end(), std::begin(tdata), std::end(tdata));
                }
                if(tid == 0x03){    /* track3 */
                }
                if(tid == 0x05){    /* jis2 */
                }
            }
            
            json.put("cardType", "magneticStripe");
            json.put("encryptedTrackData", BaseSystem::dumpHexText(track2Data));
            json.put("keySerialNumber", BaseSystem::dumpHexText(ksnData));
            json.put("status", "approved");
            subscriberICCardInserted().on_next(json);
        }
        else if (interface==EMV_INTERFACE_CONTACT) {
            LogManager_AddInformation("EMV_INTERFACE_CONTACT");
        }
        else if (interface==EMV_INTERFACE_CONTACTLESS) {
            LogManager_AddInformation("EMV_INTERFACE_CONTACTLESS");
            {
                auto df810c = [ppTLV findLastTag:0xDF810C tags:tag];
                const auto kernel = bytesToUint64(df810c.data);
                const auto resultCodeExtended = convertResultCodeForJMups(transactionResult, kernel);
                json.put("resultCodeExtended", resultCodeExtended);
                json.put("cardBrand", cardBrandFrom0xDF810C(df810c.data));
            }
            json.put("transactionResultBit", generateTransactionResultBit(tag));
            {
                //get the tags encrypted with 3DES CBC and key loaded at positon 2
                NSError* err;
                auto packetData = m_dtd.emv2GetTagsEncrypted
                (
                 createNfcTagListForTradeResult(),
                 TAGS_FORMAT_DATECS_SHORT,
                 KEY_TYPE_DUKPT_3DES_CBC,
                 0,
                 0x12345678,
                 &err
                 );
                packetData = [packetData subdataWithRange:NSMakeRange(4, packetData.length-14)];
                json.put("nfcTradeResult", BaseSystem::dumpHexText(*IOSUtilities::convert(packetData)));
            }
            {
                auto df8129 = [ppTLV findLastTag:0xDF8129 tags:tag];
                json.put("outcomeParameter", BaseSystem::dumpHexText(*IOSUtilities::convert(df8129.data)));
            }
            {
                auto ksn = m_dtd.ppadGetDUKPTKeyKSN(0);
                json.put("keySerialNoForResult", BaseSystem::dumpHexText(*IOSUtilities::convert(ksn)));
            }
            
            LogManager_AddDebug(json.str());
        }
        m_subjectContactlessTransactionFinished.get_subscriber().on_next(json);
    }
    else if(transactionResult == EMV_RESULT_DECLINED){
        LogManager_AddError("EMV_RESULT_DECLINED");
        broadcastError(make_error_cardrw(makeErrorJson("declined")));
    }
    else if(transactionResult ==  EMV_RESULT_END_APPLICATION){
        LogManager_AddError("EMV_RESULT_END_APPLICATION");
        if(m_bInSeePhone){
            LogManager_AddInformation("m_bInSeePhone == true -> retry");
            /*
             *  次の on_next() にて即時リトライが走行するため、
             *  ここで emv2Deinitialize() を行わないと、
             *  on_next() のリトライ処理の emv2Initialize が呼び出された後、
             *  この関数の終了時に emv2Deinitialize() が呼び出されることになる。
             *  （リトライについては設計的な問題な気もする）
             */
            bDeinitialized = true;
            m_dtd.emv2Deinitialize();
            json_t json;
            json.put("status", "seePhone");
            m_subjectContactlessTransactionFinished.get_subscriber().on_next(json);
        }
        else{
            broadcastError(make_error_cardrw(makeErrorJson("aborted")));
        }
    }
    else if(transactionResult ==  EMV_RESULT_TRY_AGAIN){
        LogManager_AddError("EMV_RESULT_TRY_AGAIN");
        /*
         *  AMEXの場合、EMV_RESULT_END_APPLICATION ではなく EMV_RESULT_TRY_AGAIN で
         *  応答してくる。
         *  この場合でも emv2OnUserInterfaceCode() は呼び出されるので
         *  m_bInSeePhone はここでは設定しないこととした。
         *  おそらく、AMEXのみならず、カードを活性化させるためのモバイルPIN入力ではなく、
         *  本人確認フロアリミットを越えてモバイルPINを要求するパターン。
         *  この場合には、END_APPLICATIONではなく、TRY_AGAINでBP側の処理が終了する。
         */
        bDeinitialized = true;
        m_dtd.emv2Deinitialize();
        json_t json;
        json.put("status", "seePhone");
        m_subjectContactlessTransactionFinished.get_subscriber().on_next(json);
    }
    else{
        LogManager_AddErrorF("unknown transactionResult = %d", transactionResult);
        broadcastError(make_error_cardrw(makeErrorJson("general")));
    }
    if(!bDeinitialized){
        m_dtd.emv2Deinitialize();
    }
}
/* virtual */
auto CardReaderBluePad::getCurrentReaderInfo()  -> const_json_t
{
    return const_json_t();
}

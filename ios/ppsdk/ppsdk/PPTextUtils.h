//
//  PPTextUtils.h
//  ppsdk
//
//  Created by tel on 2017/08/07.
//  Copyright © 2017年 Clementec Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! @enum PPTextUtilsTextAlignment
 @brief  アライメント指定子
 */
typedef NS_ENUM (NSUInteger, PPTextUtilsTextAlignment) {
    PPTextUtilsTextAlignmentBad,    /*!< 不正値（初期化値） */
    PPTextUtilsTextAlignmentNone,   /*!< アライメントなし、パディングなし */
    PPTextUtilsTextAlignmentLeft,   /*!< 左寄せ、パディングあり */
    PPTextUtilsTextAlignmentRight,  /*!< 右寄せ、パディングあり */
    PPTextUtilsTextAlignmentCenter  /*!< センタリング、パディングあり */
};

/*! @class PPTextUtils
 @brief  TextUtils の Objective-C ラッパ｀
 */
@interface PPTextUtils : NSObject

/*! 文字列を半角換算の文字数で分割する。分割後の最大行数を指定した場合は余剰文字はトランケートする。また、最終行についてはアライメント処理を行う。
 @param[in] src                     ソース文字列
 @param[in] rowCountWithHalfwidth   半角換算の文字数
 @param[in] textAlignment           アライメント指定子（最終行のみ適用）
 @param[in] maxLine                 行数の上限（指定なしの場合 0 を設定）
 @return         分割された文字列（NSString）の配列（NSArray）
 */
+ (NSArray<NSString*>*)split:(NSString*)src
       rowCountWithHalfwidth:(int)rowCountWithHalfwidth
               textAlignment:(PPTextUtilsTextAlignment)textAlignment
                     maxLine:(int)maxLine;



+ (int)countWithHalfWidth:(NSString *)src;

@end

#!/bin/bash -x

# 下記のコマンドはroot権限で行う
# sudo xcode-select --switch  /Applications/Xcode920.app

BOOST_VER="1_66_0"
BOOST_DIR="boost_${BOOST_VER}"
DEST_DIR="libboost_${BOOST_VER}"

XCODE_DIR=`xcode-select --print-path`
IOS_SDK_VERSION=`xcodebuild -showsdks | grep iphoneos | egrep "[[:digit:]]+\.[[:digit:]]+" -o | tail -1`
SDK_INCLUDE="${XCODE_DIR}/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk/usr/include"
CFLAGS="-DBOOST_AC_USE_PTHREADS -DBOOST_SP_USE_PTHREADS -g -DNDEBUG \
    -std=c++14 -stdlib=libc++ -fvisibility=default \
    -fembed-bitcode -miphoneos-version-min=8.0"

# boost を削除してから展開する
#rm -R ${BOOST_DIR}
#tar xvfz ${BOOST_DIR}.tar.gz

# boost を削除してから展開しない場合、下記のビルド成果物を削除
rm -R  ${BOOST_DIR}/iphone-build
rm -R  ${BOOST_DIR}/iphonesim-build

# ライブラリを削除
rm -R ${DEST_DIR}
rm ${DEST_DIR}.tar.gz

# jamファイルを定義（clangではなくclang++に変更するため）
cat > ${BOOST_DIR}/tools/build/src/user-config.jam <<EOF
using darwin : iphone
: ${XCODE_DIR}/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++
: <striper> <root>${XCODE_DIR}/Platforms/iPhoneOS.platform/Developer
: <architecture>arm <target-os>iphone
;
EOF

pushd ${BOOST_DIR}

./bootstrap.sh --with-libraries=chrono,date_time,regex,timer,thread,filesystem,system

./b2 \
 -j 8 \
 toolset=clang \
 cflags="${CFLAGS} -arch i386 -arch x86_64" \
 --build-dir=iphonesim-build \
 --stagedir=iphonesim-build/stage \
 architecture=x86 \
 target-os=iphone \
 link=static \
 threading=multi \
 define=_LITTLE_ENDIAN \
 stage


./b2 \
 -j 8 \
 toolset=darwin \
 cxxflags="${CFLAGS} -arch armv7 -arch armv7s -arch arm64" \
 --build-dir=iphone-build \
 --stagedir=iphone-build/stage \
 architecture=arm \
 target-os=iphone \
 macosx-version=iphone-${IOS_SDK_VERSION} \
 link=static \
 threading=multi \
 define=_LITTLE_ENDIAN \
 include=${SDK_INCLUDE} \
 stage

popd

mkdir ${DEST_DIR}

for file in `\find ./${BOOST_DIR}/iphone-build/stage/lib -maxdepth 1 -type f`; do 
    lipo -create \
        ./${BOOST_DIR}/iphone-build/stage/lib/${file##*/} \
        ./${BOOST_DIR}/iphonesim-build/stage/lib/${file##*/} \
        -output ./${DEST_DIR}/${file##*/}
done

tar cvfz ${DEST_DIR}.tar.gz ${DEST_DIR}
